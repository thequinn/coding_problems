'''
How to run this program:

  python Group_Anagram.py "eat tea tan ate nat bat tab"

'''

from typing import List

def group_anagrams(strs: List[str]) -> List[List[str]]:
  anagram_map = {}

  for entry in strs:
    # Sort each char and append each char w/ no space in cur entry.
    # str.join(list) returns a str
    anagram_key = "".join(sorted(entry))
    print("entry: ", entry, "; anagram_key:", anagram_key)

    # If the sorted anagram, anagram_id, exists in the map, append
    # the cur entry to the arr of val based on the key
    if anagram_key in anagram_map:
      anagram_map[anagram_key].append(entry)
    # Otherwise, create a new entry  for this key-val pair
    else:
      anagram_map[anagram_key] = [entry]

  return list(anagram_map.values())

if __name__ == '__main__':
  strs = "eat tea tan ate nat bat tab"
  strs = strs.split()

  res = group_anagrams(strs)
  #print("res:", type(res))

  for row in res:
    print("Before: row: ", row)
    row.sort()
    print("After : row: ", row)

  # Sort the list based on the first elem of row[]
  res.sort(key=lambda row: row[1])
  # list.sort() sort the list in place
  print("res:", res)

  for row in res:
    # Join the row list using ' '.  Return a str.
    print(' '.join(row))

