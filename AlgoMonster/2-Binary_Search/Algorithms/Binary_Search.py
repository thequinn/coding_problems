from typing import List

# Version 1: algo.monster's Sol
# => Use it as template cos the 1st if condition for testing "==" makes solving other similar problems easier.  Skip Version 2
def binary_search_2(arr: List[int], target: int) -> int:
    left, right = 0, len(arr) - 1

    # <= because left and right could point to the same element
    # < would miss it
    while left <= right:

      # double slash for integer division in python 3
        mid = (left + right) // 2
        if arr[mid] == target:
            return mid
        if arr[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1

# Version 2: QN's Sol
def binary_search(arr: List[int], target: int) -> int:
    left, right = 0, len(arr) - 1

    while left <= right:
        mid = (right + left) // 2
        if target < arr[mid]:
            right = mid - 1
        elif target > arr[mid]:  # 注意!! MUST USE  "elif" here !!!!!!
            left = mid + 1       #        Or use the if-else in v2 below
        else:
          return mid
    return -1

if __name__ == '__main__':
    #arr = [int(x) for x in input().split()]
    #target = int(input())
    arr = [1,2,3,4,5,6,7]
    target = 0

    res = binary_search(arr, target)
    #res = binary_search_2(arr, target)
    print(res)
