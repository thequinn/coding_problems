from typing import List

# Approach #-1: QN's Sol => Sometimes TLE
#                        => Use Approach #1 from AlgoMonster as template
def find_boundary(arr: List[bool]) -> int:
    if len(arr) == 1:
        if arr[0]:  return 0
        else: return -1
    left, right = 0, len(arr) - 1
    while left <= right:
        mid = (right + left) // 2
        if arr[mid]:
            # More vales of True on the left half of the arr[]
            if arr[mid - 1] and (mid - 1) >= 0:
                right = mid - 1
            else: # found the 1st True val
                return mid
        else:
            left = mid + 1
    return -1

'''
Approach #1: algo.monster's Sol
- This problem is a major 🔑 in solving future binary search-related problems. As we will see in the following modules, many problems boil down to finding the boundary in a boolean array.

思路:
- We are looking for the 1st True, which is the boundary seperating F's and T's
- Boundaries "so far":
    - bounrady index: 1st True so far
    - r: 1st index of T so far
    - l: last index of F so far
- Meaning of the while loop:

'''
def find_boundary(arr: List[bool]) -> int:
    left, right = 0, len(arr) - 1
    boundary_index = -1

    while left <= right:
        ##### 注意!! 常寫錯成 (right - left) #####
        mid = (left + right) // 2
        if arr[mid]:
            boundary_index = mid
            right = mid - 1
        else:
            left = mid + 1

    return boundary_index


if __name__ == '__main__':
    arr = [x == "true" for x in input().split()]
    print(arr)
    #arr = ["false", "false", "true", "true", "true"]
    res = find_boundary(arr)
    print(res)

