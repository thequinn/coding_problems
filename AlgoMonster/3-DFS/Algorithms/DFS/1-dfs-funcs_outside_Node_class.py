'''
Sources:
- AlgoMonster
- G4G
    https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
'''

class Node:
  count = 0

  def __init__(self, key):
    self.left = None
    self.val = key
    self.right = None

# = = = = = = = = = = = = = = = #  = = = = = = = = = = = = = = = #
# This method produce a BST, also called an ordered/sorted binary tree
def insert(root, key):
  if root is None:
    #print("root is None, new key to insert:", key)
    return Node(key)
  else:
    #print("root is NOT None, root.val: ", root.val")
    if root.val == key:
      return root
    if root.val > key:
      root.left = insert(root.left, key)
    if root.val < key:
      root.right = insert(root.right, key)
  # If omitted, we don't reassign the child back to its parent. The twigs are
  # not connected to the tree.
  return root

# = = = = = = = = = = = = = = = #  = = = = = = = = = = = = = = = #
#Note: Compare inorder() w/ dfs() implementations below !!

def print_inorder(root):
  if root:
    print_inorder(root.left)
    print(root.val)
    print_inorder(root.right)

'''
Depth First Search
- Source: AlgoMonster - DFS - DFS Fundamentals
          https://algo.monster/problems/dfs_intro
'''
def dfs(root, key):
    if root is None or root.val == key:
        return root
    return dfs(root.left, key) or dfs(root.right, key)

# = = = = = = = = = = = = = = = #  = = = = = = = = = = = = = = = #
# Method #1: Easier way to insert nodes

#r = Node(1)
#r.left = Node(2)
#r.right = Node(3)
#r.left.left = Node(4)
#r.right.left = Node(5)
#r.right.right = Node(6)
#r.right.left.left = Node(7)
#r.right.left.right = Node(8)

# - - - - - - - - - - #
# Method #2: Need to write insert() func to insert nodes
r = Node(5)
#r = insert(None, 5) # Same as last ln

r = insert(r, 3)
r = insert(r, 3)
r = insert(r, 3)
r = insert(r, 2)
r = insert(r, 4)
r = insert(r, 7)
r = insert(r, 6)
r = insert(r, 8)

# - - - - - - - - - - # - - - - - - - - - -
print("\nInorder Traversal:")
print_inorder(r)
print("- - - - - - - - - -")

node = dfs(r, 8)
print("Root:", node.val) if node else print("Root: None")


