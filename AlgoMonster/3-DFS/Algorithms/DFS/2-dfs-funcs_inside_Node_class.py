'''
Sources:
- AlgoMonster
- G4G
    https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
'''

class Node:
  count = 0

  def __init__(self, key):
    self.left = None
    self.val = key
    self.right = None

  # This method produce a BST, also called an ordered/sorted binary tree
  def insert(self, root, key):
    if root is None:
        print("root is None, key to insert is", key)
        return Node(key)
    else:
      print("root ", root.val, " is NOT None")
      if root.val == key:
        return root
      if root.val > key:
        root.left = self.insert(root.left, key)
      if root.val < key:
        root.right = self.insert(root.right, key)
    # Can be omitted if we don't reassign the root.
    # ex. ln-65, use r.insert(r, 30) instead of r = r.insert(r, 30)
    return root

  # Depth First Search Method
  # - See: 1-dfs-funcs_outside_Node_class.py
  def dfs(self, root, key):

  def inorder(self, root):
    if root:
      self.inorder(root.left)
      print(root.val)
      self.inorder(root.right)

# - - - - - - - - - - # - - - - - - - - - -
# Method #1: Easier way to insert nodes
#r = Node(1)
#r.left = Node(2)
#r.right = Node(3)
#r.left.left = Node(4)
#r.right.left = Node(5)
#r.right.right = Node(6)
#r.right.left.left = Node(7)
#r.right.left.right = Node(8)
# - - - - - - - - - - # - - - - - - - - - -
# Method #2: Need to write insert() func to insert nodes
r = Node(50)
print("After root node", r.val, " is created...")
if r is None:   print("root 50 is None\n")
else:   print("root 50 is NOT None\n")

r = r.insert(r, 30)
print("After 1st key=30 is inserted...\n")
r = r.insert(r, 30)
print("After 2nd key=30 is inserted...\n")

'''
r = r.insert(r, 30)
r = r.insert(r, 20)
r = r.insert(r, 40)
r = r.insert(r, 70)
r = r.insert(r, 60)
r = r.insert(r, 80)
'''
# - - - - - - - - - - # - - - - - - - - - -

print("Inorder Traversal:")
r.inorder(r)



