class Node:

  def __init__(self, key):
    self.left = None
    self.val = key
    self.right = None

  def inorder(self, root):
    if root:
      self.inorder(root.left)
      print(root.val)
      self.inorder(root.right)

  '''
  There are 2 ways to write this algorithm:
    Method #1: Using return vlaue
    Method #2: Using global variable
  '''

  # Method #1: Using return val (Divide & Conquer)
  # - Using return val to pass the max val back to the parent nodes, until
  #   all the way back to the main tree root.
  def findMax(self, root):
    if root is None:
      return float('-inf')  # min val in Python3

    max_lv = self.findMax(root.left)        # Must have return vals
    max_rv = self.findMax(root.right)
    return max(root.val, max_lv, max_rv)    # Compare 3 vals
    # Same as: ln-30~32
    #return max(root.val, self.findMax(root.left), self.findMax(root.right))

  # = = = = = = = = = = # = = = = = = = = = = # = = = = = = = = = =

r = Node(5)
r.left = Node(1)
r.left.left = Node(8)
r.left.right = Node(11)
r.inorder(r)
print("Result of max val:", r.findMax(r))
