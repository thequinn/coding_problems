class Node:

  def __init__(self, key):
    self.left = None
    self.val = key
    self.right = None

  def inorder(self, root):
    if root:
      self.inorder(root.left)
      print(root.val)
      self.inorder(root.right)

  '''
  There are 2 ways to write this algorithm:
    Method #2_1: Using global variable  => Not good
    Method #2_2: Using return vlaue     => Use this method


  Why NOT using "global var" is better:
  - https://stackoverflow.com/questions/72553393/cant-get-global-variable-of-dfs-in-python-to-return-the-right-value?noredirect=1#comment128166117_72553393

  - the class/blobal var (Method #2_1) is a worse way to write this, so just use (Method #2_2).  That said, see sol for using a class/global var.
  '''

  # - - - - - - - - - - # - - - - - - - - - - # - - - - - - - - - -
  '''
  # Method #2_1: Not using nested function

  Error msg:
    UnboundLocalError: local variable 'max_val' referenced before assignment

  Fix:
    In the class and outside the function, max_val is a class variable. Inside the function it is a lexical variable scoped to the function, which just happens to have the same name. Your error is because you are accessing the lexical variable before it is assigned to, so Python knows you are confused (but didn't give enough context to unconfuse you).

    To access the class var inside the func you need to access self.max_val.
  '''
  max_val = float('-inf')  # *Mark-A*
  def findMax_2_1(self, root):
    if root is None:
      return self.max_val  # Same var as in *Mark-A*

    self.max_val = max(self.max_val, root.val)
    self.findMax_2_1(root.left)
    self.findMax_2_1(root.right)

  # - - - - - - - - - - # - - - - - - - - - - # - - - - - - - - - -
  '''
  # Method #2_2: Using nested function and defining a nonlocal var
    => Code NOT Work b/c QN doesn't understand how to use "nonlocal var"

  "nonlocal" var:
  - A nonlocal variables are used in nested functions whose local scope is not
    defined. This means that the variable can be neither in the local nor the
    global scope.
  '''
  def findMax_2_2(self, root):
    max_val = float('-inf')

    def dfs(node):
      nonlocal max_val

      if node is None:
        return  # No pt in "return max_val" since you're ignoring the return
                # value in the inner func.

      max_val = max(max_val, node.val)
      #print("max_val:", max_val)
      dfs(node.left)
      dfs(node.right)

    dfs(root)
    return max_val

  # = = = = = = = = = = # = = = = = = = = = = # = = = = = = = = = =

r = Node(5)
r.left = Node(1)
r.left.left = Node(8)
r.left.right = Node(11)
r.inorder(r)

# Method 2_1
#r.findMax_2_1(r)
#print("Result of max val:", r.max_val)

# Method 2_2
print("Result of max val:", r.findMax_2_2(r))

