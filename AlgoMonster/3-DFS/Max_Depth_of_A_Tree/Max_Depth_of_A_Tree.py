'''
=====>>>>>>>注意!!!!! 非常重要!!!!!!

  This question (same as LC 104. Maximum Depth of Binary Tree) has a DIFFERENT def of tree depth from the convential one. It counts number of nodes.  So when a tree has depth of 3,

  Def of tree depth of this question:
  - A binary tree's maximum depth is the "number of nodes" along the longest path from the root node down to the farthest leaf node.

'''

class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# + + + + + + + + + # + + + + + + + + + # + + + + + + + + + #
# From here on, NOT inside "class Node"


# = = = = = = = = = = # = = = = = = = = = = # = = = = = = = = = = #
#  Approach #1: Each subtree recursive call has a return val
def tree_max_depth_QN(root: Node) -> int:
    if root is None:  return 0
    cur_d, max_d = 0, 0
    max_d = helper(root, cur_d, max_d)
    return 1+max_d

def dfs(root, cur_d, max_d):
    if root is None:  return 0
    else:
        max_dl = dfs(root.left, cur_d+1, max_d)
        max_dr = dfs(root.right, cur_d+1, max_d)
        print("cur node:", root.val, "; cur_d:", cur_d, ", max_d:", max_d, ", max_dl:", max_dl, ", max_dr:", max_dr)

        max_d = max(cur_d, max_d, max_dl, max_dr)

        return max_d
# = = = = = = = = = = # = = = = = = = = = = # = = = = = = = = = = #



# = = = = = = = = = = # = = = = = = = = = = # = = = = = = = = = = #
# Approach #2: Simplified from Approach #1 by eliminating a subtree's return val
def tree_max_depth_AM(root: Node) -> int:
    def dfs(root):
        # null node adds no depth
        if not root:    return 0

        # depth of current node's subtree = max depth of the two subtrees+1
        # provided by current node
        return max(dfs(root.left), dfs(root.right)) + 1

    return dfs(root)
# = = = = = = = = = = # = = = = = = = = = = # = = = = = = = = = = #


'''
注意: This method builds each node in DFS
'''
def build_tree(nodes, f):
    # next(): returns the next item from the iterator
    val = next(nodes)
    #print("val:", val)
    if val == 'x': return None
    left = build_tree(nodes, f)
    right = build_tree(nodes, f)
    return Node(f(val), left, right)

if __name__ == '__main__':
    # 2nd arg, int, below:
    # - Python's built-in int(value) function converts the argument value to
    #   an integer number. For example, int('42') converts the string value
    #   '42' into the integer number 42.
    #
    #root = build_tree(iter(input().split()), int)
    #
    #s = "5 4 3 x x 8 x x 6 x x"
    s = "3 9 x x 20 15 x x 7 x x"
    root = build_tree_QN(iter(s.split()), int)
    root = build_tree_AM(iter(s.split()), int)

    res = tree_max_depth(root)
    print(res)

