'''
Tutorial from:
- DFS w/ States: https://algo.monster/problems/dfs_with_states


This code consists of 2 Recursive Approaches:
  #1: Recursive - NO  Backtracking

  #2: Recursive - YES Backtracking
  --> Saving Space
  - In the recursive call in Method #1, we create a new list each time we recurse with path + [root.val]. This is not space-efficient b/c creating a new list requires allocating new space in memory and copying over each element.
  - A more efficient way is to use a single list, path[], and "push and pop" following the call stack.
  '''

from typing import List

class Node:
  def __init__(self, val, children=None):
    if children is None:
      children = []
    self.val = val
    self.children = children


'''
Method #1: Recursive - NO Backtracking
'''
def ternary_tree_paths(root):
  # dfs helper function
  def dfs(root, path, res):
    print("root.val:", root.val, ", path:", path)

    # Base Case: if there are no child nodes, this's exit route.
    if all(c is None for c in root.children):
      # append paths to results
      res.append('->'.join(path) + '->' + str(root.val))
      print("res:", res, "\n")
      return

    # Recursive Case: if there are child nodes, dfs on each child
    for child in root.children:
      if child is not None:
        print("child:", child.val)
        print("path + [str(root.val)]:", path + [str(root.val)])
        print("res:", res, "\n")
        #
        # path and [str(root.val)] are both lists.  "+": concatenate 2 lists.
        dfs(child, path + [str(root.val)], res)

  res = []
  if root: dfs(root, [], res)
  return res

'''
Method #2: Recursive - YES Backtracking
'''
def ternary_tree_paths(root):
  # dfs helper function
  def dfs(root, path, res):

    # Base Case: if there are no child nodes, this's exit route.
    if all(c is None for c in root.children):
      # append paths to results
      res.append('->'.join(path) + '->' + str(root.val))
      return

    # Recursive Case: if there are child nodes, dfs on each child
    for child in root.children:
      if child is not None:
        path.append(str(root.val))
        dfs(child, path, res)
        path.pop()

  res = []
  if root: dfs(root, [], res)
  return res



'''
How build_tree() builds a tree?
- Look at input str nums in pairs:
    even index: parent val
    odd  index: num of childs the parent has
ex.
s = " 1 3 2 1 3 0 4 0 6 0 "
|
==> "(1 3) (2 1) (3 0) (4 0) (6 0)"
      |
      => (1, 3): Node(val=1, children=[child-1, child-2, child-3])

Result Tree:
  1->2->3
  1->4
  1->6
'''
def build_tree(nodes, f):
  # next(): returns the next item from the iterator.
  val = next(nodes)
  num = int(next(nodes))
  print("val:", val, ", num:", num)

  children = [build_tree(nodes, f) for _ in range(num)]
  return Node(f(val), children)

if __name__ == '__main__':
  #root = build_tree(iter(input().split()), int)
  #
  s = "1 3 2 1 3 0 4 0 6 0"
  print("==================")
  # iter(): convert an iterable to the iterator
  root = build_tree(iter(s.split()), int)
  print("==================")

  res = ternary_tree_paths(root)
  for line in res:
    print(line)
'''

