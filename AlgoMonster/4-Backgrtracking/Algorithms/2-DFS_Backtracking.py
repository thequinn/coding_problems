'''
Source link:     https://algo.monster/problems/backtracking


Combinatorial search problems
- involve finding groupings and assignments of objects that satisfy certain conditions.
- Finding all permutations/subsets, solving sudoku, and 8-queens are classic combinatorial problems.



Combinatorial search == DFS on a tree
- In combinatorial search problems, search space is in the shape of a tree.
- The tree that represents all the possible states is called a state-space Tree (because it represent all the possible states in the search space).
- Each node of the state-space tree represents a state we can reach in a combinatorial search (by making a particular combination). Leaf nodes are the solutions to the problem (permutations in the above example).
- Combinatorial search problems boil down to DFS/backtracking on the state-space tree.
- Since the search space can be quite large, we often have to "prune" the tree, i.e. discard branches.



******* ******* *******
Difference between previous DFS problems and backtracking:
- In backtracking, we are not given a tree to traverse but rather we construct the tree/ generate the edges and tree nodes as we go. At each step of a backtracking algorithm, we write logic to find the edges and child nodes.
******* ******* *******



We summarized a three-step system to solve combinatorial search problems:

1) Identify the state(s).

  a. What state when we reach a sol
  ex. In the permutation example ln-2, we need to keep track of the letters
      we have already selected when we perform DFS.

  b. What state to decide which child nodes should be visited next and which
     ones should be pruned?
  ex. In the permutation example ln-2, we have to know what the letters are
      left that we can still use (since each letter can only be used once).

2) Draw the state-space tree.
3) DFS/backtrack on the state-space tree.



How to implement a backtracking algorithm?
||
---> Draw the tree, draw the tree, draw the tree!!!

Draw a state-space tree to visualize the problem. A small test case that's big enough to reach at least one solution (leaf node). We can't stress how important this is. Once you draw the tree, the rest of the work becomes so much easier - simply traverse the tree depth-first.

When drawing the tree, bear in mind:
1. how do we know if we have reached a solution?
2. how do we branch (generate possible children)?


# Old Template: DFS with States.  AM author updated it to the New Template next
function dfs(node, state):
    if state is a solution:
        report(state) # e.g. add state to final result list
        return

    for child in children:
        if child is a part of a potential solution:
            state.add(child) # make move
            dfs(child, state)
            state.remove(child) # backtrace

# New Template:
function dfs(start_index, path):
  if is_leaf(start_index):
    report(path)
    return
  for edge in get_edges(start_index):
    path.add(edge)
    dfs(start_index + 1, path)
    path.pop()

# - start_index: is used to keep track of the current level of the state-space
# tree we are in.
# - edge: is the choice we make to reach the next level

Notice how very similar this is to the "Ternary Tree Path" code we've seen in
"DFS with States" module. That problem has an explicit tree. For combinatorial
search problems, we have to find our own tree.

'''

from typing import List

def permutations(letters):
  def dfs(path, used, res):
    #print("path:", path, ", used:", used, ", res:", res)

    # Base Case:
    if len(path) == len(letters):
      print("path:", path)
      print("\'\'.join(path):", ''.join(path))
      res.append(''.join(path))
      print("--Base Case - res:", res)
      return

    # Recursive Case:
    for i, letter in enumerate(letters):
      # skip used letters
      if used[i]: continue
      # add letter to permutation, mark letter as used
      path.append(letter)
      used[i] = True
      dfs(path, used, res)
      # remove letter from permutation, mark letter as unused
      path.pop()
      used[i] = False

  res = []
  dfs([], [False] * len(letters), res)
  return res

if __name__ == '__main__':
  #letters = input()
  letters = "abc"

  res = permutations(letters)
  print("- - - - - - - - - - - - - - - - - - - -\nResult:")
  for line in res:
    # end=", ": multiple print() in 1 ln
    print(line, end=", ")

