'''
https://algo.monster/problems/memoization_intro

'''

# Recursion - No Memoization
def fib(n):
    if n == 0 or n == 1:
        return n
    return fib(n - 1) + fib(n - 2)

# Recursion - Yes Memoization
def fib(n, memo):
    if n in memo: # check in memo, if found, retrieve and return right away
        return memo[n]

    if n == 0 or n == 1:
        return n

    res = fib(n - 1, memo) + fib(n - 2, memo)

    memo[n] = res # save in memo before returning
    return res
