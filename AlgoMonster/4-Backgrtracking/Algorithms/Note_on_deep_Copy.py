'''
Backtracking - Combinatorial Search - Permutations - Note on deep copy
  https://algo.monster/problems/permutations


// - - - - - - - - - - //  - - - - - - - - - - //

(a) Shallow Copy vs Deep Copy:
- https://stackoverflow.com/questions/184710/what-is-the-difference-between-a-deep-copy-and-a-shallow-copy


(b) Is Python call by reference or call by value?
- https://www.geeksforgeeks.org/is-python-call-by-reference-or-call-by-value/#:~:text=Python%20utilizes%20a%20system%2C%20which,being%20passed%20to%20the%20function.
- Python utilizes a system, which is known as “Call by Object Reference” or “Call by assignment”.
-- In the event that you pass arguments like whole numbers, strings or tuples to a function, the passing is like call-by-value because you can not change the value of the immutable objects being passed to the function.
-- Whereas passing mutable objects can be considered as call by reference because when their values are changed inside the function, then it will also be reflected outside the function.

// - - - - - - - - - - //  - - - - - - - - - - //

list vs [:]
- https://stackoverflow.com/questions/4081561/what-is-the-difference-between-list-and-list-in-python

- li[:] creates a copy of the original list. But it does not refer to the same list object. Hence you don't risk changing the original list by changing the copy created by li[:].

for example:

>>> list1 = [1,2,3]
>>> list2 = list1
>>> list3 = list1[:]
>>> list1[0] = 4
>>> list2
[4, 2, 3]
>>> list3
[1, 2, 3]

'''


'''
Ex for Shallow Copy:
=>The code below is from "Note on deep copy" part:
- In the code below, append(path) actually appends a reference (memory address).  So we actually appended the same list three times.  path.append(i) mutates the list and affects all references to that list.
'''
res = []
path = []
for i in range(3):
  path.append(i)
  print("path:", path)

  res.append(path)
  print("res:", res, "\n")


'''
Permutations Ex that uses Deep Copy:
- In the exit logic here, we append a deep copy of the path res.append(path[:]).

    Note:
    - AlgoMonster (AM) author uses path[:] to stress ln-73. See explaination: "list vs [:]" above. Qn think the pt AM author is trying to make is var path is called-by-val by dfs(). It means a copy of var path is made and appended to var res.

  This creates a new list with elements being the same as current elements of path.
'''
from typing import List
def permutations(letters):
  def dfs(path, used, res):
    #print("path:", path, ", used:", used, ", res:", res)

    # Base Case:
    if len(path) == len(letters):
      print("path:", path)
      print("\'\'.join(path):", ''.join(path))
      res.append(''.join(path))
      print("--Base Case - res:", res)
      return

    # Recursive Case:
    for i, letter in enumerate(letters):
      # skip used letters
      if used[i]: continue
      # add letter to permutation, mark letter as used
      path.append(letter)
      used[i] = True
      dfs(path, used, res)
      # remove letter from permutation, mark letter as unused
      path.pop()
      used[i] = False

  res = []
  dfs([], [False] * len(letters), res)
  return res

if __name__ == '__main__':
  #letters = input()
  letters = "abc"

  res = permutations(letters)
  print("- - - - - - - - - - - - - - - - - - - -\nResult:")
  for line in res:
    # end=", ": multiple print() in 1 ln
    print(line, end=", ")

