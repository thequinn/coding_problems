'''
Don't know how to make twoSum sol work w/ threeSum using AlgoMonster's Dedup approach
- See: https://algo.monster/problems/deduplication
- Looking for a threeSum sol using twoSum which uses "Hashmap" Approach

Note:
- LC
  1. Two Sum
  167. Two Sum II
  15. 3Sum
'''

const threeSum = function(nums) {
    nums.sort(() => a - b)
    // points.sort(function(a, b){return a - b});

    for (let i in nums) {
        var

        // Skip duplicate inputs at the beginning of the array from index 0
        // - Notice!!! Trace the code w/ an ex if not understand.
        if (i > 0 && nums[i-1] == nums[i])
            continue
        const tuples = twoSum(nums, -nums[i])
    }
    return tuples
};

const twoSum = (nums, target2) => {
    //...未...
}
