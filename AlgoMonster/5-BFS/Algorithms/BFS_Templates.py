'''

***非常重要!!!
This file contains Template #1 & #2:
- The diff is that Template #1 doesn't contain "for _ in range(n):" that keeps track of "num of nodes in each level" to process.


Note:
- Must do LC 104. Maximum Depth of Binary Tree.  It will help w/ comprehension and memorization!
- Tips: new_level[] is similar as var dep in LC 104.

'''

from collections import deque
from typing import List

class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# BFS on Tree - Level Order Traversal
#  https://algo.monster/problems/bfs_intro
def bfs_basic_template(root):
  res = []  # Saves level-order-traversal result

  # ⚠️  QN ALWAYS forget to add this 1st loop!
  queue = deque([root]) # at least 1 elem in the queue to kick start bfs
  while len(queue) > 0:

    node = queue.popleft()
    res.append(node.val)  # Add every node in order (level-order)
    for child in [node.left, node.right]:

      if child is not None:
        # Early return if problem condition met
        #if OK(child):
        #  return FOUND child
        queue.append(child)

  #return NOT_FOUND
  return res

# BFS on Tree: Level Order Traversal using an extra list to add level-by-level
#  https://algo.monster/problems/binary_tree_level_order_traversal
def bfs_level_order_traversal(root: Node) -> List[List[int]]:
  res = []  # Saves level-order-traversal result

  # at least one element in the queue to kick start bfs while loop (ln-46)
  queue = deque([root]) # track unprocessed nodes
                        # - compare w/ new_level[] (ln-48)
  while len(queue) > 0:

    new_level = [] # track processed nodes

    # ⚠️  QN ALWAYS forget to add this 2nd loop!
    # n: num of nodes in this "new level" already enqueued
    # - using n instead of len(queue) ensures the for-loop ln-55 don't get
    #   affected when var queue is modified when an elem is dequeued
    n = len(queue)
    for _ in range(n):  # num of elems in the new level to dequeue
                        ### This loop is what divides the tree into levels.  It
                        ###   pops all nodes in a level before the next iter of
                        ###   of the outer loop (ln-46)

      node = queue.popleft()
      new_level.append(node.val)

      # Remember! After a node is dequeued, its childs needs to be enqueued
      for child in [node.left, node.right]:
        if child is not None:
          queue.append(child)

    res.append(new_level) # This new level of tree is done, append it to res[]

  return res

def build_tree(nodes, f):
    val = next(nodes)
    if val == 'x': return None
    left = build_tree(nodes, f)
    right = build_tree(nodes, f)
    return Node(f(val), left, right)

if __name__ == '__main__':
    # From: AlgoMonster - BFS - BFS on Tree - Binary Tree Level Order Traversal
    #
    # Same as the next 2 lns
    #root = build_tree(iter(input().split()), int)
    #
    s = "1 2 4 x 7 x x 5 x x 3 x 6 x x"
    root = build_tree(iter(s.split()), int)

    res = bfs_basic_template(root)
    print("res:", res)
    #
    #res = bfs_level_order_traversal(root)
    #for row in res:
    #    print(' '.join(map(str, row)))

