# Tmplt #1: Basic - BFS Tree vs BFS Graph

'''
BSF (Iterative) in Tree
  https://algo.monster/problems/bfs_intro
'''
from collections import deque
def bfs_tree(root: Node) -> List(List(int)):
  # At least 1 node in the queue to kick start BFS
  q = deque([root])

  # When the queue is not empty, there's more work to do
  while len(q) > 0:
    node = q.popleft()  # Dequeue

    for child in node.children:  # Enqueue childs
      q.append(child)


'''
BFS (Iterative) in Graph
  https://algo.monster/problems/graph_bfs
'''
from collections import deque
def bfs_graph(root: Node) -> List(List(tree)):
  q = deque([root])
  visited = set([root])  # Track visited neighbors

  while len(q) > 0:
    node = q.popleft()

    for neighbor in get_neighbors(node):
      if neighbor in visited:
        continue
      q.append(neighbor)
      visited.append(neighbor)
