# Tmplt #2: Track Levels (Find Distance) - BFS Tree vs BFS Graph

#
# Note!!
#   This tmplt only add a few extra ln's of code to Tmplt #1, marked in ###.
#   Compare tmplt #1 & #2!!

'''
BSF in Tree
  https://algo.monster/problems/binary_tree_level_order_traversal
'''
from collections import deque
def bfs_tree(root: Node) -> List(List(int)):
  q = deque([root])
  res = []   ### Each level of nodes is added as a list to res[],
             ### ex. [ [1], [2,3], [4,5,6] ]
  level = 0  ### Track each level in a tree

  while len(q) > 0:

    new_level = []  ### Add all nodes in the cur level
    n = len(q)      ### Track num of nodes in the level

    for _ in range(n): ### This loop is what divides the tree into levels! It pops
                       ###   all nodes in a level before the next iter of the
                       ###   outer loop (ln-19) works on the next level.
      node = popleft()

      new_level.append(node.val)  ###

      for child in node.children:
        q.append(child)

    res.append(new_level)  ###
    level += 1            ###
  return res        ###


'''
BFS in Graph
  https://algo.monster/problems/graph_bfs
'''
from collections import deque
def bfs_graph(root: Node) -> List(List(tree)):
  q = deque([root])
  visited = set([root])
  level = 0         ###

  while len(q) > 0:

    n = len(q)          ###
    for _ in range(n):  ###

      node = popleft()

      for neighbor in get_neighbors(node):
        if neighbor in visited:
          continue
        q.append(neighbor)
        visited.add(neighbor)

    level += 1  ###

