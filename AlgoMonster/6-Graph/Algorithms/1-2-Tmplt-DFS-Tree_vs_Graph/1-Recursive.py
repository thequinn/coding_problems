# Tmplt #2: Basic - DFS Tree vs DFS Graph
#   https://algo.monster/problems/graph_dfs


# DFS Recursive in Tree
def dfs(root):
  if not root:  return

  for child in root.children:
    dfs(child)             # ==> Recursive


# DFS Recursive in Graph
def dfs(root, visited):    # visited = set([root])
  if not root:  return

  for neighbor in get_neighbors(root):
    if neighbor in visited:
      continue
    visited.append(neighbor)
    dfs(neighbor, visited)  # ==> Recursive


