'''
Notice!!
- Don't understand the generator and yield statement usage here.  Skip!!


DFS using Stack:
    https://codereview.stackexchange.com/questions/247368/depth-first-search-using-stack-in-python
'''

def dfs(graph, start):
    stack = [start]
    visited = set()

    while len(stack):
        vertex = stack.pop()
        if vertex in visited:
            continue

        yield vertex

        for neighbor in graph[vertex]:
            stack.append(neighbor)
        visited.add(vertex)

