[Templates]

1. The two tmplt folders compare tree vs graph. Knowing BFS & DFS in tree is the foundation for learning the ones in graph.
- 1-1-Tmplt-BFS-Tree_vs_Graph
- 1-2-Tmplt-BFS-Tree_vs_Graph

2. The iterative approach using a stack in the following file, QN doesn't understand how to use Python generator and yield statement.
||
===> Skip! Wasted enough time on this trivial thing already!!

- AlgoMonster/6-Graph/Algorithms/1-2-Tmplt-DFS-Tree_vs_Graph/2-Iterative_w_Stack.py


// - - - - - - - - - - - - // - - - - - - - - - - - - //
[Terminology]

Unweighted Shortest Path
- BFS is best for finding the shortest distance between two nodes


Weighted Shortest Path
- We need algorithms like Dijkstra's algorithm. Dijkstra's algorithm rarely comes up in coding interviews
- Note:
    - Weighted graph:
        - Every edge is assigned a weight. Distance increases by the weight instead of 1.


Directed Acyclic Graph (DAG)



// - - - - - - - - - - - - // - - - - - - - - - - - - //
[Algorithms]

(1) DFS uses a stack, BFS uses a queue


(2) Topological Sort

- Topological Sort is only possible if it's a Directed Acyclic Graph (DAG)

{重要!!}
- Watch this video till 10:18 to learn the requirements of a valid DAG and how Topo Sort works.
    https://www.youtube.com/watch?v=L05HDfyDHjg

- Can be implemented by both of the methods below:

  (a) DFS w/ Stack
  - Techdose: https://www.youtube.com/watch?v=qe_pQCh09yU

  (b) BFS w/ Queue and an Indegree Array (= Khan's Algorithm)
  - Techdose: https://www.youtube.com/watch?v=tggiFvaxjrY

- Topo Sort - Khan's Algo (BFS + Queue)
    BitBucket/Coding_Problems/AlgoMonster/6-Graph/Algorithms/Topological_Sort/1-Topo_Sort-DFS

  (a) The 2 versions of 2-X-Topo_Sort_BFS_Queue-Khans_Algo.py has the same concept, but version 2-2 is implemented in a simpler way.

  (b) Run-time Analysis: See 2-2 version



