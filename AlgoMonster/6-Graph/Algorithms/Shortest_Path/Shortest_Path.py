
from collections import deque
from typing import List

def shortest_path(graph: List[List[int]], a: int, b: int) -> int:
    def get_neighbors(node: int):
        return graph[node]

    # BFS template
    def bfs(root: int, target: int):
        queue = deque([root])
        visited = set([root])
        level = 0
        while len(queue) > 0:
            n = len(queue)
            for n in range(n):
                node = queue.popleft()
                if node == target:
                    return level    # Return the len of shortest path
                for neighbor in get_neighbors(node):
                    if neighbor in visited:
                        continue
                    queue.append(neighbor)
                    visited.add(neighbor)
            level += 1

    return bfs(a, b)

if __name__ == '__main__':

    # Read in user input to create an adjacent list for each node in the
    # graph. (See: nested_lst_compr.py to understand the next ln)
    #
    graph = [[int(x) for x in input().split()] for _ in range(int(input()))]
    #print("graph:", graph)

    # a, b are start and end nodes of the shortest path
    a = int(input())
    b = int(input())
    #print("a:", a)
    #print("b:", b)

    res = shortest_path(graph, a, b)
    print("\nres:", res)

'''
Note:
To understand the test case below, see this tutorial's page on AlgoMonster

Test Case:

4       # 4 nodes, and the next 4 lns show their connected neighbors
1 2     # At index-0, n0 has connected neighbors of n1 & n2
0 2 3
0 1
1
0       # The next 2 lns are the start & end nodes for the shortest path
3
'''

