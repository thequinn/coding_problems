'''
Note:
To understand the test case below, see this tutorial's page on AlgoMonster

Test Case:
4       # 4 nodes, and the next 4 lns show their connected neighbors
1 2     # At index-0, n0 has connected neighbors of n1 & n2
0 2 3
0 1
1
0       # The next 2 lns are the start & end nodes for the shortest path
3
'''

# Read in user input to create an adjacent list for each node in the graph
#
graph = [[int(x) for x in input().split()] for _ in range(int(input()))]
print("graph:", graph)  # log: [[1, 2], [0, 2, 3], [0, 1], [1]]


# Equivalent Code:
numOfNodes = int(input())
graph = []
for _ in range(numOfNodes):
    neighbors_lst = input().split()
    print("\nneighbors_lst:", neighbors_lst)

    for i, e in enumerate(neighbors_lst):
        neighbors_lst[i] = int(e)
    graph.append(neighbors_lst)
print("graph:", garaph)  # Log: [[1, 2], [0, 2, 3], [0, 1], [1]]



