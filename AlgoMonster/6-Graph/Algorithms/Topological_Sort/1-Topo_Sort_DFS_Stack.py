'''
Tutorial Video:
- https://www.youtube.com/watch?v=qe_pQCh09yU

Python Code:
- https://www.lavivienpost.com/topological-sort-find-ranking-dfs-bfs/

'''

class Topo_Sort_DFS_Stack:

    def __init__(self) :
        self.adj = {};

	#Add edge, Time O(1), Space O(1)
    def addEdge(self, v, w) :
        if self.adj.get(v) == None:
            self.adj[v]= []
        if self.adj.get(w) == None:
            self.adj[w]= []
        self.adj[v].append(w)

	# Time O(V+E), Space O(V), V is number of nodes, E is number of edges
    def topoSortDfs(self) :
        stack = [];
        visited = {};

        for k in self.adj.keys():
            visited[k] = False;
        for k in self.adj.keys():
            if visited[k] == False:
                self.dfsHelper(k, visited, stack)
        res = []
        while len(stack) > 0:
            res.append(stack.pop())
        return res

    # DFS helper, Time O(V+E), Space O(V)
    def dfsHelper(self, v, visited, stack) :
        visited[v] = True
        print("visited:", visited)
        for  i in self.adj.get(v) :
            if visited[i] == False:
                self.dfsHelper(i, visited, stack);
        stack.append(v);

# - - - - - - - - - - - - - -
sort = Topo_Sort_DFS_Stack()

# There are 6 nodes: 0,1,2,3,4,5
# Add edges to the adjacency list, which is a dict here
sort.addEdge(0,2)
sort.addEdge(0,3)
sort.addEdge(3,1)
sort.addEdge(4,1)
sort.addEdge(4,2)
sort.addEdge(5,0)
sort.addEdge(5,2)

res = sort.topoSortDfs()
print("Topo Sort - DFS w/ Stack")
print(res)  # using topoSortDfs(): [5, 4, 0, 3, 1, 2] or [4, 5, 0, 3, 1, 2]
            # using topoSortBfs(): [5, 0, 3, 4, 2, 1]
