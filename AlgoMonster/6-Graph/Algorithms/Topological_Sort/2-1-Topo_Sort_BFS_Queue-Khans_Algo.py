'''
Tutorial Video:
- https://www.youtube.com/watch?v=tggiFvaxjrY&t=933s

Python Code:
- https://www.lavivienpost.com/topological-sort-find-ranking-dfs-bfs/

'''

class Topo_Sort_BFS_Queue:

    def __init__(self) :
        # Adjacency List
        self.adj = {};

	# Add edge to build adjacency list, Time O(1), Space O(1)
    def addEdge(self, v, w) :
        if self.adj.get(v) == None: self.adj[v]= []
        if self.adj.get(w) == None: self.adj[w]= []
        self.adj[v].append(w)

	# BFS Kahn’s algorithm, use indegree, Time O(V+E), Space  O(V)
    #def topoSortBfs(self, digraph) :
    def topoSortBfs(self) :

        indegree = {};
        for key in self.adj.keys():
            indegree[key] = 0
        for key in self.adj.keys():
            losers =  self.adj[key];
            for node in losers:
                indegree[node]= indegree[node]+1
        print("indegree:", indegree)

        # Add nodes w/ indegree=0 to queue.  This is the starting pt for the
        # queue to work  in while loop below.
        queue = []
        for key in self.adj.keys():
            if indegree.get(key) == 0 :
                queue.append(key);

        count = 0
        res = []
        while len(queue) != 0:
            node = queue.pop()
            res.append(node)
            count += 1;
            for neighbor in self.adj[node]:
                # Decrease indegree by 1 first
                indegree[neighbor] = indegree[neighbor]-1
                if indegree[neighbor] == 0:
                    queue.append(neighbor)

        # Check if there is a cyce
        if count != len(self.adj):
            return None

        return res

# - - - - - - - - - - - - - -
sort = Topo_Sort_BFS_Queue()

# There are 6 nodes: 0,1,2,3,4,5
# Add edges to the adjacency list, which is a dict here

sort.addEdge(0,2)
sort.addEdge(0,3)
sort.addEdge(3,1)
sort.addEdge(4,1)
sort.addEdge(4,2)
sort.addEdge(5,0)
sort.addEdge(5,2)

#digraph = [[0,2], [0,3], [3,1], [4,1], [4,2], [5,0], [5,2]]


#res = sort.topoSortBfs(digraph)
res = sort.topoSortBfs()
print("Topo Sort - BFS w/ Queue")
print(res)  # using topoSortDfs(): [5, 4, 0, 3, 1, 2] or [4, 5, 0, 3, 1, 2]
            # using topoSortBfs(): [5, 0, 3, 4, 2, 1]

