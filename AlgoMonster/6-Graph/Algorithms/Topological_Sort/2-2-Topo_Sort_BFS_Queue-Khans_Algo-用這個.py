'''
Code from Sol 3-1 in
    Bitbucket/LeetCode/207-Course_Schedule/207-Course_Schedule-1.py


// - - - - - - - - - - - //
重要!!

(1) *** Time Complexity Analysis for the main while loop ***
https://leetcode.com/problems/course-schedule/editorial/

Each queue operation takes O(1) time, and a single node will be pushed once, leading to O(n) operations for n nodes.

We iterate over the neighbors of each node that is popped out of the queue iterating over all the edges once. Since there are total of m edges, it would take O(m) time to iterate over the edges.

=> Total time: O (m + n)


(2) Time Complexity => Explains from a different perspective

https://www.interviewkickstart.com/learn/topological-sort#:~:text=The%20time%20complexity%20of%20topological,%3D%20Vertices%2C%20E%20%3D%20Edges.

- To determine the in-degree of each node, we will have to iterate through all the edges of the graph. So the time complexity of this step is O(E).

- Next, look for nodes with in-degrees equal to 0. This will require us to iterate through the entire array that stores the in-degrees of each node. The size of this array is equal to V. So, the time complexity of this step is O(V).

- For each node with an in-degree equal to zero, we remove it from the graph and decrement the in-degrees of the nodes it connected to. In the entire run of the algorithm, the number of times we have to perform the decrement operation is equal to the number of edges. So it will take O(E) time.

- Whenever we decrement the in-degree of a node, we check if the node has achieved in-degree = 0 status. When this happens, we move it to the stack. In the entire run of the algorithm, it can occur at max (V-1) times. So the run time of this operation is O(V).

- In the end, we compare the size of the resultant topologically sorted array and the size of the graph to ensure that it was not a cyclic graph. This operation is done in O(1) time.

- So, adding all the individual run times, the time complexity of topological sort is O(V+E).

*** Note ***
  This is the best case, worst case, and average-case time complexity of this algorithm since we perform the same steps regardless of how the elements are organized.

'''

from collections import defaultdict, deque

class Graph(object):
    def topoSortBfs(self, numNodes, edgeList):

        # Create an adjacency list for each node
        adjList = defaultdict(list)  # dict of list
        # Create indegree for each node to track incoming edges
        indegree = defaultdict(int) # dict of int
        for src, dst in edgeList:
            adjList[src].append(dst)
            indegree[dst] += 1
        print("adjList:", adjList)
        print("indegree:", indegree)

        # Enqueue nodes that have 0 incoming edges (indegree=0) to start
        # Khan's Algo
        queue = deque()
        for node in range(numNodes):
            if node not in indegree:
                queue.append(node)
        print("queue:", queue)

        # Keep looping until the queue is empty.
        # Whenever we dequeue a node, we append it to the topo ordered list.
        topoOrder = []
        while queue:
            src = queue.popleft()
            topoOrder.append(src)

            # Deduct indegree by 1 for all neighbors of this node.
            # If a neighbor has 0 incoming edge, append it to queue.
            for neighbor in adjList[src]:
                indegree[neighbor] -= 1
                if indegree[neighbor] == 0:
                    queue.append(neighbor)

        # Check if all nodes are handled, or there're nodes left meaning
        # there is a cycle.
        return topoOrder if numNodes == len(topoOrder) else "Graph has a cycle"

g = Graph()
numNodes = 6

# This graph is a DAG
edgeList_1 = [[0,2], [0,3], [3,1], [4,1], [4,2], [5,0], [5,2]]
res = g.topoSortBfs(numNodes, edgeList_1)
print("Topo Order: ", res)

print("- - - - - - - - - - - - - - - - -\n")
# This graph has a cycle
edgeList_2 = [[0,2], [0,3], [3,1], [4,1], [4,2], [5,0], [5,2], [1,4]]
res = g.topoSortBfs(numNodes, edgeList_2)
print("Topo Order: ", res)
