
'''
Brute Force
'''
'''
class Solution:
  def longest_substring_without_repeating_characters(self, s: str) -> int:
    n = len(s)
    longest = 0

    for start in range(n):
      print("")
      for end in range(n):
        sub = s[start: end + 1]
        #print("sub:", sub)

        # 注意!! When converting a list to a set, each elem in the list becomes
        # an individual entry in the set.
        #print("set(sub):", set(sub))
        if len(set(sub)) == len(sub):
          longest = max(longest, end + 1 - start)
    return longest
'''

'''
Sliding Window

Key Pt:
- When we encounter a char already in the window, we want to move the
  left ptr until it goes past the last occurrence of that char.
'''
class Solution:
  def longest_substring_without_repeating_characters(self, s: str) -> int:
    n = len(s)
    longest = 0
    l = r = 0
    window = set()
    while r < n:
      if s[r] not in window:
        window.add(s[r])
        print("add    s[", r, "]:", s[r], ";  window{}:", window)
        r += 1
      # When we encounter a char already in the window, we want to move the
      # left ptr until it goes past the last occurrence of that char.
      else:
        window.remove(s[l])
        print("remove s[", l, "]:", s[l], ";  window{}:", window)
        l += 1

      longest = max(longest, r - l)
      print("r:", r, ", l:", l, ", longest:", longest)
    return longest


if __name__ == '__main__':
  #str = input()    # Read from terminal
  #
  #str = "abcabcbb";   # 3
  #str = "abcbb";   # 3
  #str = "bbbbb";   # 1
  #str = "pwwkew";   # 3
  str = "abcdbea";

  sol = Solution()
  res = sol.longest_substring_without_repeating_characters(str)
  print(res)
