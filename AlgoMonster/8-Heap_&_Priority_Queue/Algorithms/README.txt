



Heaps, heapsort, and priority queues - Inside code
- Video Tutorial:
   https://www.youtube.com/watch?v=g9YK6sftDi0
   https://www.youtube.com/watch?v=hkyzcLkmoBY
- Python Code:
   https://gist.github.com/syphh/50adc4e9c7e6efc3c5b4555018e47ddd#file-heap-py
   https://www.youtube.com/watch?v=hkyzcLkmoBY -> It has code (only in video)


Converting Min Heap to Max Heap:
- https://www.youtube.com/watch?v=HCEr35qpawQ&list=PLDV1Zeh2NRsCLFSHm1nYb9daYf60lCcag&index=2


“Prim's Algorithm Minimum Spanning Tree-Tushar_Roy”
- https://www.youtube.com/watch?v=oP2-8ysT3QQ
- Watch first few mins of the video for: Priority Queue’s Change Key (Decrease Key) operation.

