'''
Binary Heap (Min/Max Heap) implementation in Python
- Concept:
   https://www.youtube.com/watch?v=g9YK6sftDi0&t=3s
   https://www.youtube.com/watch?v=hkyzcLkmoBY
- Code:
  (a) https://algo.monster/problems/heap_intro  => Code of this file from here
      - See heap-AM.js
  (b) https://gist.github.com/syphh/50adc4e9c7e6efc3c5b4555018e47ddd
      - See heap.py
'''

class HeapItem {
  constructor(item, priority = item) {
    this.item = item;
    this.priority = priority;
  }
}

class MinHeap {
  constructor() {
    this.heap = [];
  }

  push(node) {
    // insert the new node at the end of the heap array
    this.heap.push(node);
    // find the correct position for the new node
    this.bubble_up();
  }

  bubble_up() {
    let index = this.heap.length - 1;

    while (index > 0) {
      const element = this.heap[index];
      const parentIndex = Math.floor((index - 1) / 2);
      const parent = this.heap[parentIndex];

      if (parent.priority <= element.priority) break;
      // if parent > child, swap them
      this.heap[index] = parent;
      this.heap[parentIndex] = element;
      index = parentIndex;
    }
  }


  pop() {
    const min = this.heap[0];

    // Replace the root node w/ the lowest prioritized leaf node
    this.heap[0] = this.heap[this.size() - 1];
    // Remove the end node in the heap array - the lowest prioritized leaf node
    this.heap.pop();

    this.bubble_down();
    return min;
  }

  // Note: Code tracing: Con't from pop()
  //
  // Easier to understand using an ex to trace: [1,3,5,7,9,8]
  bubble_down() {
    let index = 0;
    let min = index;
    const n = this.heap.length;

    while (index < n) {
      const left = 2 * index + 1;
      const right = left + 1;
      // check if left or right child is smaller than parent
      if ((left < n && this.heap[left].priority < this.heap[min].priority) ||
        (right < n && this.heap[right].priority < this.heap[min].priority)) {
        // pick the smaller child if both childs are present
        if (right < n) {
          min = this.heap[left].priority < this.heap[right].priority ? left : right;
        }
        // only 1 child (the left child) is present
        else {
          min = left;
        }
      }
      // If min ptr isn't assigned to a l/r child in the last if statement, it
      // means the current node doesn't have childs.  So the node is the last
      // leaf child in the heap.
      if (min === index) break;
      // Otherwise, swap the current node and its smaller child
      [this.heap[min], this.heap[index]] = [this.heap[index], this.heap[min]];

      // Reset index and min
      index = min;
    }
  }

  peek() {
    return this.heap[0];
  }

  size() {
    return this.heap.length;
  }
}
