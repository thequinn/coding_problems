/*
Maximum Subarray Sum / Largest Sum COntiguour Subarray

- See /GitHub/My_Leetcode/maximumSubarray.js using Kadane's Algorithm


NOT SURE IF THE FOLLOWING CODE  IS THE BEST.  WATCH Link-1 above to understand what Kadane's algorithm is!!

*/


// Method #1
/*var maxSequence = function(arr){
  var min = 0, ans = 0, sum = 0;

  for (let i = 0; i < arr.length; ++i) {
    sum += arr[i];
    min = Math.min(sum, min);
    ans = Math.max(ans, sum - min);
  }
  return ans;
}*/

// Method #2
var maxSequence = function(arr){

  var cur = 0; // Keep total sum of the arr that has been traversed so far  
  var max = 0; // Hold max sum until that pt

  arr.forEach(function(i) {
    cur = Math.max(0, cur + i);
    max = Math.max(max, cur);
  });
  return max;
}


let re, arr;
arr = [-2, 1, -3, 4, -1, 2, 1, -5, 4];
re = maxSequence(arr);  // max sum is 6: [4, -1, 2, 1]
console.log("re: ", re);
