"""
What is the difference between __init__ and __call__?

(1) __init__ is used to initialise newly created object, and receives arguments used to do that:

ex. class Foo:
      def __init__(self, a, b, c):
        # ...

    x = Foo(1, 2, 3) # __init__

(2) __call__ implements function call operator.

ex. class Foo:
      def __call__(self, a, b, c):
        # ...

    x = Foo()
    x(1, 2, 3) # __call__


So, the __init__ method is used when the class is called to initialize the instance, while the __call__ method is called when the instance is called

"""

#---------------------------------------------------------------

# The arg, int, makes the class add() inherit from the int class
# If not inherits from int, you have to define a __init__ method for the class
class add(int):
  def __call__(self, n):
    return add(self + n)

#---------------------------------------------------------------

# ?????... 不太懂, 需先学 Inheritance ...?????
class Addable:
    def __init__(self, val):
        self.val = val

    def __call__(self, val):
        return Addable(self.val + val)

    def __eq__(self, other):
        return self.val == other

    def __add__(self, other):
        return self.val + other

    def __sub__(self, other):
        return self.val - other


def add(n):
    return Addable(n)

#---------------------------------------------------------------


print(add(1)) # 1
#print(add(1)(2)) # 3
#print(add(1)(2)(3)) # 6
#print(add(1)(2)(3)(4)) # 10
#print(add(1)(2)(3)(4)(5)) # 15
