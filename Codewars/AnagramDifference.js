const anagramDifference = (w1, w2) => {
  const stack = {};
  const re = 0;
  let counter = 0;
  
  for (var c1 of w1) {
    stack[c1] > 0 ? stack[c1]++ : stack[c1] = 1;
  }
  console.log("stack:", stack);

  for (var c2 of w2) {
    // If c2 doesn't exist in stack{}, or val of c2 is deducted to 0 already,
    // increment counter.  counter tracks num of chars needed to be removed.
    stack[c2] > 0 ? stack[c2]-- : counter++;
  }
  console.log("stack:", stack, ",  counter:", counter);
  
  let tmp = Object.values(stack).reduce((sum, curr) => sum + curr, 0);
  console.log("tmp:", tmp)

  return re + counter + tmp;
};

let re;
//re = anagramDifference("","");     // 0
//console.log(re);
re = anagramDifference("anna","banana");    // 2
console.log(re);
//re = anagramDifference("codewars","hackerrank");   // 10
//console.log(re);
