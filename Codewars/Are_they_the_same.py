
# Method 1:
def comp(array1, array2):

  # 技巧:
  # - Using "in" operator to check if a tuple's elem has None
  # - (Tuples are sequences)
  if None in (array1, array2):
    return False

  return sorted(a ** 2 for a in array1) == sorted(array2)

# Method 2:
"""def comp(a1, a2):
  return None not in (a1,a2) and [i*i for i in sorted(a1)] == sorted(a2)
"""


a = [121, 144, 19, 161, 19, 144, 19, 11]
b = [121, 14641, 20736, 361, 25921, 361, 20736, 361]
print(comp(a, b))  # T

a2 = [121, 144, 19, 161, 19, 144, 19, 11]
b2 = [132, 14641, 20736, 361, 25921, 361, 20736, 361]
print(comp(a2, b2))  # F
