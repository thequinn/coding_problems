// Method #1:
function deepCount(arr) {
  let sum = 0;
  return helper(sum, arr);
}
function helper(sum, arr) {
  if (arr.length === 0)   return 0;

  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      sum += deepCount(arr[i]);
    }
    sum++;
  }
  return sum;
}


// Method #2:
// - It shows how to eliminate using a base case!
//
/*function deepCount(arr) {
    let temp = 0;
    for(let i = 0; i < arr.length; i++) {
        if(Array.isArray(arr[i]))
            temp += deepCount(arr[i]);
        temp++;
    }
    return temp;
}*/


let re;
//re = deepCount([]);                    // 0
//console.log("re: ", re);

//re = deepCount([1, 2, 3]);             // 3
//console.log("re: ", re);

re = deepCount(["x", "y", ["z"]]);     // 4
console.log("re: ", re);

//re = deepCount([1, 2, [3, 4, [5]]]);   // 7
//console.log("re: ", re);

//re = deepCount([[[[[[[[[]]]]]]]]]);    // 8
//console.log("re: ", re);
