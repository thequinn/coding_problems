Array.prototype.square = function() {
  return this.map(a => Math.pow(a, 2));
}

Array.prototype.cube = function() {
  return this.map(a => Math.pow(a, 3));
}


// Method #1: using reduce()
Array.prototype.average = function() {
  return this.reduce((acc, curr) => {
    return acc + curr;
  }) / this.length;
}
// Method #2: using sum()
Array.prototype.average = function() { 
  return this.sum() / this.length; 
}


Array.prototype.sum = function() {
  return this.reduce((acc, curr) => acc + curr, 0);
}

Array.prototype.even = function() {
  //return this.filter(a => (a % 2) == 0);
  return this.filter(a => !(a % 2));
}

Array.prototype.odd = function() {
  //return this.filter(a => (a % 2) == 1);
  return this.filter(a => a % 2);
}


var numbers = [1, 2, 3, 4, 5];
var re;

re = numbers.square();  // must return [1, 4, 9, 16, 25]
console.log("re: ", re);
re = numbers.cube();    // must return [1, 8, 27, 64, 125]
console.log("re: ", re);
re = numbers.average(); // must return 3
console.log("re: ", re);
re = numbers.sum();     // must return 15
console.log("re: ", re);
re = numbers.even();    // must return [2, 4]
console.log("re: ", re);
re = numbers.odd();     // must return [1, 3, 5]
console.log("re: ", re);
