"""
itertools.combinations(iterable, r)
- This tool returns the length subsequences of elements from the input iterable.
- Combinations are emitted in lexicographic sorted order. So, if the input iterable is sorted, the combination tuples will be produced in sorted order.

ex.
>>> from itertools import combinations
>>>
>>> print list(combinations('12345',2))
[('1', '2'), ('1', '3'), ('1', '4'), ('1', '5'), ('2', '3'), ('2', '4'), ('2', '5'), ('3', '4'), ('3', '5'), ('4', '5')]

"""


from itertools import combinations

# Method 1:
def choose_best_sum(t, k, ls):
  return max((sum(v) for v in combinations(ls,k) if sum(v)<=t), default=None)

  ls = [sum(v) for v in combinations(ls, k) if sum(v) <= t]

  # max(iterable, *[, key, default])
  # - default: specifies an object to return if the provided iterable is empty.
  # - If the iterable is empty and default is not provided, a ValueError is
  #   raised.
  return max(ls, default=None)

#-------------------------------------------------------

# Method 2:
def choose_best_sum(t, k, ls):
  return max((sum(v) for v in combinations(ls,k) if sum(v)<=t), default=None)

#-------------------------------------------------------

xs = [100, 76, 56, 44, 89, 73, 68, 56, 64, 123, 2333, 144, 50, 132, 123, 34, 89]
print(choose_best_sum(230, 4, xs)) # 230
#print(choose_best_sum(430, 5, xs)) # 430
#print(choose_best_sum(430, 8, xs)) # None

