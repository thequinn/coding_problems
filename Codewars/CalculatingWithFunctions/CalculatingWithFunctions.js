/*
Calculating with Functions

- 這題有難度，要好好想！
*/

// Method #1: 
function expr(n, operation) {
  if (!operation) {
    console.log("if (!operation), n: ", n);
    return n;
  }
  console.log("else      , n: ", n);
  let re = operation(n);
  return re;
}

function zero(operation) { return expr(0, operation) }
function one(operation) { return expr(1, operation) }
function two(operation) { return expr(2, operation) }
function three(operation) { return expr(3, operation) }
function four(operation) { return expr(4, operation) }
function five(operation) { return expr(5, operation) }
function six(operation) { return expr(6, operation) }
function seven(operation) { return expr(7, operation) }
function eight(operation) { return expr(8, operation) }
function nine(operation) { return expr(9, operation) }

function plus(a) {
  console.log(arguments);

  // Suppose: one(plus(two()));
  // - This inner func access expr()'s 1st arg
  return function (b) {
    console.log("a:", a, ", b:", b);
    return b + a;
  }
}
function minus(a) {
  console.log(arguments);
  return function (b) {
    return b - a;
  }
}
function times(a) {
  console.log(arguments);
  return function (b) {
    return a * b;
  }
}
function dividedBy(a) {
  console.log(arguments);
  return function (b) {
    return Math.floor(b / a);
  }
}
//------------------------

let re;
// 解釋：The most outer function represents the left operand, the most inner function represents the right operand

// two() is executed first
re = one(plus(two())); // 3
console.log("re: ", re);
//re = eight(minus(three()));  // 5
//console.log("re: ", re);
//re = seven(times(five()));   // 35

//re = six(dividedBy(two()));  // 3
//re = two(dividedBy(six())); // 0
//re = six(dividedBy(nine())); // 0

