// Method #1: Same as Method #2, but in separate lines
/*String.prototype.camelCase = function() {
  let arr = this.split(' ');

  return arr.map((word, i) => 
    i !== 0 ?
      word.replace(word.charAt(0), word.charAt(0).toUpperCase())
      :
      word
  ).join('');
}*/

// Method #2
String.prototype.camelCase = function(){
  // TIP: use 'this' to capture the testing string !!!
  return this.split(" ").map(a => a.charAt(0).toUpperCase() + a.slice(1)).join("")
};

let re = '';
re = "test case".camelCase();
//re = "camel case method".camelCase();
//re = "say hello ".camelCase();
//re = " camel case word".camelCase();
//re = "".camelCase();
console.log("re:", re);
