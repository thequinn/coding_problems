// Common Denominators 

/*
最大公約數（Greatest Common Divisor，簡寫為GCD）
- 是幾個自然數公有約數中最大的一個。
- 例如，16和40公約數有：1、2、4、8，其中最大的是8，8就是16和40的最大公約數。

最小公倍數（英語：Least Common Multiple，簡寫為LCM）
- 是幾個自然數公有倍數中最小一個。
- 例如，5和6公倍數有：30、60、90、⋯⋯其中最小的是30，30就是5和6的最小公倍數。

如何找最大公约数, 最小公倍數:
- https://www.idomaths.com/zh-Hant/hcflcm.php#findhcf
*/

// Method #1: Iterative
// 
// 求最大公約數: 輾轉相除法（歐幾里德算法）
let gcd = function(a, b) {
  if(!b) { return a; }
  return gcd(b, a % b);
}
// 求最小公倍數: 公式法
let lcm = function(d1, d2) {
  return d1 * d2 / gcd(d1, d2);
}

let convertFrac = function(arr) {
  let cd = arr.reduce((a, b) => lcm(b[1], a), 1);
  // Convert from arr to str.
  return arr.map(a => `(${a[0]*(cd/a[1])},${a[1] * (cd/a[1])})`).join('');
}

// Method #2: Recrusive
// 
/*const gcd = (a, b) => b ? gcd(b, a % b) : a;
const lcm = (a, b) => a * b / gcd(a, b);

function convertFrac(arr) {
  // '_' as a func param:
  // - https://stackoverflow.com/questions/27637013/what-is-the-meaning-of-an-underscore-in-javascript-function-parameter
  // 
  const cd = arr.reduce( (a, [_, d]) => lcm(d, a), 1 );

  return arr.map(([n, d]) => `(${n * cd/d},${cd})`).join('');
}*/

var lst = [ [1, 2], [1, 3], [1, 4] ];
var re = convertFrac(lst); // (6,12)(4,12)(3,12)
console.log("re: ", re);
