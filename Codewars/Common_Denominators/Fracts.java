import static java.lang.System.out;

public class Fracts{
  private static long gcd(long m, long n){
    while(m % n != 0){
      long temp = m % n;
      m = n;
      n = temp;
    }    
    return n;
  }    
  
  private static long lcm(long m, long n){
    return m * n / Fracts.gcd(m,n);
  }    
  
  public static String convertFrac(long[][] lst){
    long mul = Fracts.lcm(lst[0][1], lst[1][1]); 
    for(int i = 2; i < lst.length; i++){
      mul = Fracts.lcm(mul, lst[i][1]);
    }

    // Convert the long array to String
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < lst.length; i++){
      // String.format()
      // - returns the formatted string by given format and arg's
      sb.append( String.format("(%d,%d)", mul*lst[i][0]/lst[i][1], mul) );
      //System.out.println(mul*lst[i][0]/lst[i][1] + ", " + mul);
    }   
    
    return sb.toString(); 
  }

  public static void main(String[] args) {
    long[][] lst = { {6, 12}, {4, 12}, {3, 12} };
    Fracts fract = new Fracts();
    System.out.println(fract.convertFrac(lst));
  }
}




