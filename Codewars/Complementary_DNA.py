

# Method 1:
def DNA_strand(dna):

  complements = {
    'A': 'T',
    'T': 'A',
    'C': 'G',
    'G': 'C'
  }

  # complements[c] is how you access an elem in set
  #
  # The arg in join() is list comprehension
  return "".join([ complements[c] for c in dna ])


# Method 2:
import string

def DNA_strand(dna):
  # string.translate()
  # - returns a string where each character is mapped to its corresponding
  #   character in the translation table.
  return dna.translate(string.maketrans("ATCG","TAGC"))

# Python 3.4 solution: you don't need to import anything :)
  # return dna.translate(str.maketrans("ATCG","TAGC"))


#--------------------------------------------

print(DNA_strand ("ATTGC")) # return "TAACG"
print(DNA_strand ("GTAT"))  # return "CATA"
