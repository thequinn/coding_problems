/*
ex. Convert C9  hex to dec?

  9 = 9 * (16 ^ 0) = 9
  C = 12 * (16 ^ 1) = 192

  192 + 9 = 201
*/

function hexStringToRGB(h) {
  return {
    r: parseInt(h.slice(1,3), 16),
    g: parseInt(h.slice(3,5), 16),
    b: parseInt(h.slice(5,7), 16)
  };
}

hexStringToRGB("#FF9933")；  // {r:255, g:153, b:51}
