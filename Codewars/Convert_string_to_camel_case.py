import re

def to_camel_case(text):
  arr = re.split(r'[^a-zA-Z]', text)

  return arr[0] + ''.join(x.capitalize() for x in arr[1:])


print(to_camel_case("the-stealth-warrior")) # returns "theStealthWarrior"
print(to_camel_case("The_Stealth_Warrior")) # returns "TheStealthWarrior"
