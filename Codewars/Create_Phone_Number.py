
# Method 1:
def create_phone_number(n):
  return "({}{}{}) {}{}{}-{}{}{}{}".format(*n)


# Method 2:
def create_phone_number(n):

  # The next 2 ln's are the same
  n = ''.join(map(str,n))
  #n = "".join([str(x) for x in n] )

  return '(%s) %s-%s'%(n[:3], n[3:6], n[6:])


print(create_phone_number([1, 2, 3, 4, 5, 6, 7, 8, 9, 0])) # "(123) 456-7890"
