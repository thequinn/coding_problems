
def maskify(cc):
  if len(cc) <= 4:  return cc

  # OK
  #return "#" * (len(cc)-4) + cc[len(cc)-4:]
  #
  # Better
  return "#" * (len(cc)-4) + cc[-4:]


print(maskify("4556364607935616")) # "############5616"
print(maskify(     "64607935616")) #      "#######5616"
print(maskify(               "1")) #                "1"
print(maskify(                "")) #                 ""
