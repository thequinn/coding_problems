/*
Data Reverse

IMPORTANT TIP!!
- (1) To Keep same order of an arr, use Array.push() to add its elems to the front of an array
ex: [a, b, c] => [a, b, c]

- (2) To reverse an arr, use Array.shift() to add its elems to the end of an arr
ex: [a, b, c] => [c, b, a]
*/

// Method #1:
/*function dataReverse(data) {
  let arr = [];
  let chunk;

  for (let i = 0; i < data.length; i += 8) {
    // Take the first 8 bits
    let chunk = data.slice(i, i+8);
    //console.log("chunk: ", chunk);   
    
    // Unshift to arr[] (add to beginning of arr[])
    //
    // If not use spread operator, ..., we get [[chunk], [chunk]]
    arr.unshift(...chunk);
  }

  //console.log(arr); 
  return arr;
}*/

// Method 2: Same as Method #1, but more concise
const dataReverse = (data, out = []) => {
    for (let i = 0; i < data.length; i += 8){
        out.unshift(...data.slice(i, i+8));
    }
    return out;
}


let data = [];

//data = [0,0,1,1, 0,1,1,0]; // Remains the same

data = [0,0,1,1, 0,1,1,0,  0,0,1,0, 1,1,1,1];
// [0,0,1,0, 1,1,1,1,  0,0,1,1, 0,1,1,0]

//data = [1,1,1,1, 1,1,1,1,  0,0,0,0, 0,0,0,0,  0,0,0,0, 1,1,1,1,  1,0,1,0, 1,0,1,0];
// [1,0,1,0, 1,0,1,0,  0,0,0,0, 1,1,1,1,  0,0,0,0, 0,0,0,0,  1,1,1,1, 1,1,1,1]

console.log(dataReverse(data));
