def descending_order(num):

    # WRONG!!
    #sortedList = sorted( list(str(num), reverse=True )
    #
    # sorted(str(num), ...) 不需 list() 來轉換: sorted(list(str(num))).
    # - 這是因為 sorted() builds a new sorted list from an iterable
    #
    sortedList = sorted(str(num), reverse=True)
    return int("".join( sortedList ))

print(descending_order(21445))  # 54421
#print(descending_order(0))  # 0
#print(descending_order(15))  # 51
