function Automaton() {
   // this defines transition function
   this.states = {
     "q1": ["q1", "q2"],
     "q2": ["q3", "q2"],
     "q3": ["q2", "q2"]
   };
}

Automaton.prototype.readCommands = function(commands) {
  //console.log("ln-11, this:\n", this);

  // No need to store current state anywhere  
  let re = commands.reduce(function(state, input) {
    //console.log("ln-15, this:\n", this);

    let tmp = this.states[state][input];
    console.log("this.states[", state, "][", input, "]:", tmp);
    return tmp;

  }.bind(this), "q1");
  /*
  重要 !!!
  - Comparing ln-11 to ln-15, the callback in commands.reduce() lost the
  context of myAuto at myAuto.readCommands(). It's how JS works.  So we need
  the callback to bind() the context again to fix the problem!!
  */

  return "q2" === re;
}

var myAuto = new Automaton();
//myAuto.readCommands(["1"]);                  // T
//myAuto.readCommands(["1", "0", "0", "1"]);   // T
var re = myAuto.readCommands(["1", "0", "0", "1", "0"]);  // F
console.log(re);
