/*
Directions Reduction

(1) How I crossed the desert the smart way.

The directions given to the man are, for example, the following (depending on the language):

["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"].

A better plan in this case is simply:

["WEST"]


(2) Task
Write a function dirReduc which will take an array of strings and returns an array of strings with the needless directions removed (W<->E or S<->N side by side).
*/

// Method #1, #2: Good
let dirReduc = function(arr) {
  let str = arr.join(' ');
  // console.log(`str:${str}`);

  let re = /NORTH\s+SOUTH|SOUTH\s+NORTH|WEST\s+EAST|EAST\s+WEST/;

  // RegExp.prototype.test()
  // - search for a match b/n a regular expression and a specified string. 
  while (re.test(str)){
    str = str.replace(re,'');
  }
  //console.log(`str:-${str}-`);


  /*let new_arr1 = str.split(" ");
  console.log(`new_arr1:${new_arr1}`);  // [ "", "", "WEST" ]

  let new_arr2 = new_arr1.filter(a => {
    // Test if a if an empty str
    let tmp = !!a;  // Same as:  let tmp = (a !== "")
    return tmp;
  });
  return new_arr2;*/
  
  //--------------------------------------------------------
  // Method #1:
  // same as: ln-33~40
  return  str.split(" ").filter(a=>!!a);

  // Method #2:
  // match()
  // - retrieves an array containing the strings matching against a regex
  //return str.match(/(NORTH|SOUTH|EAST|WEST)/g) || [];  // 好像不需[]
}

// Method #3: Easiest and Best
function dirReduc(plan) {
  var opposite = {
    'NORTH': 'SOUTH', 'EAST': 'WEST', 'SOUTH': 'NORTH', 'WEST': 'EAST'};
  return plan.reduce(function(dirs, dir){
      if (dirs[dirs.length - 1] === opposite[dir])
        dirs.pop();
      else
        dirs.push(dir);
      return dirs;
    }, []);
}

let re;
re = [];
//re = dirReduc(["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]); //["WEST"]
//re = dirReduc(["NORTH", "WEST", "SOUTH", "EAST"]); //["NORTH", "WEST", "SOUTH", "EAST"]
//re = dirReduc(["NORTH", "SOUTH", "EAST", "WEST", "EAST", "WEST"]); //[]
console.log(re);
