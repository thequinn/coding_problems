import re

# Method 1:
def disemvowel(s):
  # str.translate(table[, deletechars]);
  #
  # No need table to translate.  We only need the 2nd arg which are chars to
  # be deleted
  return s.translate(None, 'aeiouAEIOU')

# Method 2:
def disemvowel(s):
  return "".join(c for c in s if c.lower() not in "aeiou")

# Method 3:
def disemvowel(s):
  return re.sub(r"[aeiou]", "", s, flags = re.IGNORECASE)
