
// Method X: 
// - Using the line below doesn't handle '@' in last testing case.  But this method
//   is good to learn String.match() and 'g' flag.
//    
//    let arr = word.match(new RegExp(letter, "ig"));
//
/*function duplicateEncode(word){

  // Spread syntax "..."
  // - Spread syntax allows an iterable such as an array expression or string to 
  //   be expanded in places
  let result = [...word].map( function(letter) {

    // String.match():
    // - Returned val:
    // -- An Array whose contents depend on the presence or absence of the global
    //    (g) flag, or null if no matches are found.
    // --- If the g flag is used, all results matching the complete regular exp
    //     will be returned, but capturing groups will not.
    let arr = word.match(new RegExp(letter, "ig"));
    console.log("arr: ", arr);

    letter = arr.length === 1 ? "(" : ")";
    console.log("letter: ", letter);

    return letter;
  }).join("");

   //let res = [...word].map(letter =>
   //             word.match(new RegExp(letter, "ig")).length === 1 ? "(" : ")"
   //          ).join("");

  return result;
}*/

// Method #1: Best
function duplicateEncode(word) {
  return word
    .toLowerCase()
    .split('')
    .map( function (a, i, w) {
      let x = w.indexOf(a);
      let y = w.lastIndexOf(a);
      //console.log("x: ", x, ", y: ", y);
      return x === y ? '(' : ')';      
    })
    .join('');
}

// Method #2: Good
// - "TECHNIQUE": 
// -- Array.some() is Array.map()'s callback.  And they uses same arr!
/*function duplicateEncode(word) {
  let arr = word.toLowerCase().split('');

  let arr2 = arr.map(function(v1, i1) {
    // Array.some(): 
    // - tests whether at least 1 elem in the array passes the test implemented by
    //   the provided function. It returns a Boolean value.
    let re = arr.some((v2, i2) => { 
      console.log(`v1: ${v1}, i1: ${i1}, v2: ${v2}, i2: ${i2}`);
      return v1 === v2 && i1 !== i2 
    }) 
      ? ')' : '(';
    
    return re;
  });

  return arr2.join('');
}*/

// Method #2-2: 
// - Same as Method #1, but in concise version
/*function duplicateEncode(word) {
  var letters = word.toLowerCase().split('');

  return letters.map(function(c, i) {
    return letters.some(function(x, j) { 
      return x === c && i !== j }) ? ')' : '('
  }).join('')
}*/

let re = "";
//re = duplicateEncode("din");         // "((("
//re = duplicateEncode("recede");      // "()()()"
//re = duplicateEncode("Success");     // ")())())"

// This case doesn't work using Method X above 
re = duplicateEncode("(( @");        // "))(("

console.log("re: ", re);

