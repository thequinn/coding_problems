// Method #1: Iteration
/*function beggars(values, n){
  let arr = [];

  for(let i = 1; i <= n; i++) {
    // Init a val in index of i-1 to prevent "undefined" in the index
    arr[i-1] = 0;
    // Same as doing next ln in setup:
    //    new Array(n).fill(0);
    
    for(let j = i-1; j < values.length; j+=n) {
      arr[i-1] += values[j];
    }
  }
  return arr;
}*/

// Method #2-1: 
// - Recursion solution from the author
// - See Method #2-2 for the break-down of this sol
//
//const beggars = (v,n,i=0,res=[...Array(n)].map(e=>0)) => v.length ? beggars(v.slice(1),n,(i+1)%n,res.map((e,j)=>j==i ? e+v[0] : e)) : res;

// Method #2-2:
/* 
(1) res=[...Array(n)].map(e => 0)
- setup init vals for all elems in res[] to avoid undefined vals

- NOTICE !!
(i) Array(n)
-- same as: new Array(n)

(ii) If res=[...Array(n)].map(e => 0) is replaced by res=Array(n).map(e => 0), elems in the array are not initialized.
-- This is b/c usually constructors do not have a return statement. Their task is to write all necessary stuff into this , and it automatically becomes the result.

(2) 0 % 0 = NaN, so we use (i+1)%n but not i%n
*/
const beggars = (v, n, i=0, res=[...Array(n)].map(e => 0)) => {
  console.log("\nv:", v, "\t, n:", n, ", i:", i, ", res:", res);

  let tmp = v.length
    ? beggars(v.slice(1), n, (i+1)%n, res.map((e,j) => {
          console.log("i:", i, ", j:", j, ", e:", e);

          // Even if there're more than 2 elems in res[], we only map up to
          // the first n elems.  One of them is what we need.
          let newVal = j==i ? e+v[0] : e;
          console.log("newVal:", newVal);
          return newVal;
        })
    )
    : res
  
  return tmp;
};


let re;
//re = beggars([1,2,3], 1);  // [6]
//console.log("re: ", re);

re = beggars([1,2,3,4,5],2);  // [9,6]
console.log("re: ", re);
//re = beggars([1,2,3,4,5],3);  // [5,7,3]
//re = beggars([1,2,3,4,5],6);  // [1,2,3,4,5,0]
//re = beggars([1,2,3,4,5],0);  // []
//console.log("re: ", re);
