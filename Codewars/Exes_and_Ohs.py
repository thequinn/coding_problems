
def xo(s):
  s = s.lower()

  return s.count('x') == s.count('o')


print(xo("ooxx")) # => true
print(xo("xooxx")) # => false
print(xo("ooxXm")) # => true
print(xo("zpzpzpp")) # => true // when no 'x' and 'o' is present should return true
print(xo("zzoo")) # => false
