/*
File Finder

- Task:
(1) Search through a virtual filesystem of sorts to find a file, then return the path to the file.

(2) Sometimes there might not be a file in the filesystem. In that case, just throw an error. You're always guaranteed to receive a filesystem with either one or zero files.
//-----------------------------------------
/*
- Check for an empty obj:

法一：ECMA 7+
  Object.entries(obj).length === 0 && obj.constructor === Object

法二：ECMA 5+
  Object.keys(obj).length === 0 && obj.constructor === Object
*/


// Method #1:
function search(files, path = '') {
  console.log("path: ", path);

  // Using ou test case, if 'I PRANKED YOU' is what's passed as "files" in
  // 1st arg in ln-33.
  if (typeof files === 'string') {
    let tmp = path.slice(1);    // Omit the beginning "/" of path
    console.log("tmp: ", tmp);
    return tmp;
  }

  for (let folder in files) {
    try {
      // 技巧：
      // - files[folder] will return the val of a key, which is the obj's
      //   nested properties
      return search(files[folder], path + '/' + folder);
    }
    catch(e) {}
  }
  throw new Error('No files!');
}

// Method #2:
function search(files, root = true) {
  // When "files" is a string file-name, we foundan existing file.
  if(typeof files != 'object')    return true;

  for(var p of Object.keys(files)) {
    let r = search(files[p], false);

    if(r && r == true) return p;
    else if(r) return p + '/' + r;
  }

  if(root) throw new Error('No files!');
}


let files = {
  'New folder': {
    'New folder': {}
  },
  'New folder (1)': {
    'New folder': {
      'funnyjoke.txt': 'I PRANKED YOU'
    }
  },
  'New folder (2)': {}
};
console.log("\nre: ", search(files)); // New folder (1)/New folder/funnyjoke.txt
