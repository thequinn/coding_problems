
def find_uniq(arr):
  # The ex has 2 unique val's. Python allow to return multiple val's.
  a, b= set(arr)
  print("a: %i, b: %i" % (a, b))

  return a if arr.count(a) == 1 else b

arr = (2,1,2,2,2)
print(find_uniq(arr))
