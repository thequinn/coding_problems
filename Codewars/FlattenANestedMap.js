// Method #1:
/*function flattenMap(map, way, res) {
  var way = way || [],  res = res || {};

  for (var key in map) {
    //console.log("\nkey:", key, ", [key]:", [key]);
    var arr = way.concat([key]);
    //console.log("arr:", arr);

    //console.log("map[", key, "]:", map[key]);
    var objType = {}.toString.call(map[key]);
    //console.log("objType:", objType);
   
    // If curr val in objType is an Object, recurse to go deeper
    if ( objType == '[object Object]' ) {
      //console.log("res:", res);
      //console.log("Call flattenMap()");
      flattenMap(map[key], arr, res);
    }
    // If it is non-Object, we reach the end of a path
    else {
      //console.log("arr.join('/'): ", arr.join('/'));
      //console.log("map[", key, "]");
      res[arr.join('/')] = map[key];
    }
  }
  return res;
}*/

// Method #2:
function flattenMap(map) {
  var result = {};

  function recurse (cur, prop) {
    console.log("Object(cur):", Object(cur));
    console.log("cur        :", cur);
    if (Object(cur) !== cur || Array.isArray(cur)) {
      return result[prop] = cur;
    }
    for (var p in cur) {
      recurse(cur[p], prop ? prop+"/"+p : p);
    }
  }
  recurse(map, "");
  return result;
}

var input = {
  'a': {
    'b': {
      'c': 12,
      'd': 'Hello World'
    },
    'e': [1,2,3]
  }
};
/*
Output: 
{
  'a/b/c': 12,
  'a/b/d': 'Hello World',
  'a/e': [1,2,3]
};
*/
console.log( flattenMap(input) );
