
# Method 1: List Comprehension
def friend(names):
    return [name for name in names if len(name) == 4]

#=====================================

Method #1-1: Filter + Callback
def helper(name):
    return len(name) == 4

def friend(names):
    # Need to use list() b/c filter() returns an iterable obj
    return list(filter(helper, names))

#=====================================

# Mathod 1-2: filter()+ Lambda (Anonymous Callback)
def friend(x):
    return list(filter(lambda s : len(s) == 4 ,x))

#=====================================



names = ["Ryan", "Kieran", "Mark"]    # ["Ryan", "Mark"]
res = friend(names)

# printing the list using * and sep operator that seperate each item by comma
print(*res, sep = ", ")
