import math

def get_middle(s):
  # Need to use //, floor division, to avoid a decimal num
  i = (len(s) - 1) // 2

  # "or s": if len(s) <= 2
  return s[i:-i] or s

#--------------------------------------

#res = get_middle("A")        # "A"
#res = get_middle("of")        # "of"

#res = get_middle("test")        # "es"
res = get_middle("testing")     # "t"
print(res)
