/*
Greatest Common Divisor

(1) 最大公約數 (Greatest Common Divisor，簡寫為GCD）
- 是幾個自然數公有約數中最大的一個。
- 例如，16和40公約數有：1、2、4、8，其中最大的是8，8就是16和40的最大公約數。

(2) 最小公倍數（Least Common Multiple，簡寫為LCM）
- 是幾個自然數公有公倍數中最小一個。
- 例如，5和6公倍數有：30、60、90、⋯⋯其中最小的是30，30就是5和6的最小公倍數。

(3) 如何求最大公約數, 最小公倍數?
https://www.idomaths.com/zh-Hant/hcflcm.php
*/

// 輾轉相除法（歐幾里德算法）will easily work w/ Recursion here
// - Note: 不需用兩數種較小的數去除較大的數

// Method #1:
function mygcd(x, y){
  if (y == 0)   return x
  return mygcd(y, x % y)
}

// Method #2:
function mygcd(x,y){
  return y == 0 ? x : mygcd(y, x % y)
}

mygcd(30,12);   // 6
mygcd(8,9);     // 1
mygcd(1,1);     // 1
