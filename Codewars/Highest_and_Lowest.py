
# Method 1: List Comprehension
def high_and_low(numbers):
  n = [int(s) for s in numbers.split(" ")]
  return "%i %i" % (max(n),min(n))

# Method 2: map()
def high_and_low(numbers):
  n = map(int, numbers.split(' '))
  return str(max(n)) + ' ' + str(min(n))

# Method 3: ==> This sol is only good for Python 2.X
# - Error msg:
# -- return "%s %s" %(max(results), min(results))
#    ValueError: min() arg is an empty sequence
#
def high_and_low(numbers):
  results = map(int, numbers.split(" "))

  # The following 2 ln's cause the same error
  return "%i %i" %(max(results), min(results))
  #return "%s %s" %(max(results), min(results))


res = high_and_low("4 0 -214 542 -64 -12") # "542 -214"
print(res)
