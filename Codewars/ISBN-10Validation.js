/*
 
For an ISBN-10 to be valid, the sum of the digits multiplied by their position has to equal zero modulo 11.

ex. ISBN-10: 1112223339 is valid b/c 
    (((1*1)+(1*2)+(1*3)+(2*4)+(2*5)+(2*6)+(3*7)+(3*8)+(3*9)+(9*10)) % 11) == 0
*/

// Method 1-1:
/*let validISBN10 = function(isbn) {
  // if isbn has 1st 9 chars as digits, and last char as either a digit or an 'X'
  if (!/[0-9]{9}[0-9X]/.test(isbn)) { 
    return false; 
  }

  let arr = isbn.split('');

  let num = arr.reduce((acc, val, idx) => {
    console.log("idx: ", idx, "val: ", val);
    // If curr val is X, convert it to 10.  Then mul it w/ ++indx. 
    return acc + (val === 'X' ? 10 : val) * ++idx;
  }, 0);
  console.log("num: ", num);
  return num % 11 === 0;
}*/

// Method 1-2: Concise way of Method 1-1
let validISBN10 = isbn => {
  if (!/[0-9]{9}[0-9X]/.test(isbn))   return false; 

  return isbn.split('').reduce((acc, val, idx) => acc + (val === 'X' ? 10 : val) * ++idx, 0) % 11 === 0;
};


let re;

re = validISBN10('1112223339'); // true
console.log("re: ", re);
re = validISBN10('111222333X'); // false
console.log("re: ", re);
re = validISBN10('1X12223339'); // false 
console.log("re: ", re);

//re = validISBN10('12345'); // true
//re = validISBN10('123456789A'); // false
