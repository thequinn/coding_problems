/*
Kebabize

Task:
- Modify the kebabize function so that it converts a camel case string into a kebab case.

- Note:
The returned string should only contain lowercase letters
*/

// Method 1-1: 
function kebabize(str) {
  // Remove all digits in str
  var noNum = str.replace(/\d/g, '')
  console.log("noNum: ", noNum);

  // Regex ?=
  // - The ?=n quantifier matches any string that is followed by a specific string n.
  // - The ?!n quantifier matches any string that is NOT followed by a specific
  //   string n.
  //
  // Split the str w/ any capital letter into an array of strs
  var arr = noNum.split(/(?=[A-Z])/g);
  console.log("arr: ", arr);
  
  // Join all strs in arr[] w/ '-'.  Then convert all letters to lowercase.
  return arr.join('-').toLowerCase();
}
// Method 1-2:
/*function kebabize(str) {  
  return str.replace(/[0-9]/g, '').split(/(?=[A-Z])/).join('-').toLowerCase()  
}*/


/*
// Method 2:
function kebabize(str) {
  // Replace all digits w/ ""
  str = str.replace(/[\d]/g, "");
  console.log("str 1:", str);

  str = str.charAt(0).toLowerCase() + str.slice(1);
  console.log("str 2:", str);

  //String.replace(regexp|substr, newSubstr|function)
  //- The callback func will be invoked after the match has been performed.
  //- The callback's result (return value) will be used as the replacement string.  
  //- The calllback will be invoked multiple times for each full match to be replaced
  //  if the regular expression in the first parameter is global.
  //
  // Replace each capital letter, a, w/ "-"+lowercase_of_a
  return str.replace(/([A-Z])/g, a => "-"+a.toLowerCase());
}

// Method 3:
function kebabize(str) {
  return str.replace(/[^a-z]/ig, '').
         replace(/^[A-Z]/, c => c.toLowerCase()).
         replace(/[A-Z]/g, c => `-${c.toLowerCase()}`);
}
*/

let re = '';

//re = kebabize('camelsHaveThreeHumps'); // camels-have-three-humps
re = kebabize('ericHas999Books222'); // eric-has-books

console.log("re: ", re);
