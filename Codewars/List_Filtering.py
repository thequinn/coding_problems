
# Method 1:
def filter_list(l):
  # Correct
  return [elem for elem in l if type(elem) is int]

  # WRONG Syntax! List Comprehension always has [] surrounded.
  return l2.append(e) for e in l if type(e)==int

# Method 2:
def filter_list(l):
  return [i for i in l if not isinstance(i, str)]


print(filter_list([1,2,'a','b']))  # [1,2]
print(filter_list([1,'a','b',0,15]))  # [1,0,15]
print(filter_list([1,2,'aasf','1','123',123]))  # [1,2,123]
