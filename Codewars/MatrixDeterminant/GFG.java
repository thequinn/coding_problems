// https://www.geeksforgeeks.org/determinant-of-a-matrix/

// Java program to find Deteminant of a matrix 
class GFG { 
	
	// Dimension of input square matrix 
	static final int N = 3; 
	
	// n is current dimension of mat[][] 
	static void cofactor(int mat[][], int tmp[][], int p, int q, int n) { 
		int i = 0, j = 0; 	
		for (int row = 0; row < n; row++) 
			for (int col = 0; col < n; col++) 
				// Copying elements not in row and column into tmp[][]
				if (row != p && col != q) { 
					tmp[i][j++] = mat[row][col]; 
					// Move to next row 
					if (j == n - 1) {   j = 0;  i++;   } 
        }
	} 
	
	// n is current dimension of mat[][]
	static int determinant(int mat[][], int n) { 
		int D = 0;                    // Initialize result 
		int tmp[][] = new int[N][N];  // store cofactors 		
		int sign = 1;                 // sign multiplier 
	
		// Base case : if matrix contains single element 
		if (n == 1)     return mat[0][0]; 

    // Iterate for each element of first row 
		for (int f = 0; f < n; f++) { 
			// Getting Cofactor of mat[0][f] 
			cofactor(mat, tmp, 0, f, n); 
      //display(tmp, n, n);
			
      D += sign * mat[0][f]	* determinant(tmp, n - 1); 
      //System.out.println("f: " + f + "; D: " + D);

			// terms are to be added with alternate sign 
			sign = -sign; 
		} 
		return D; 
	} 
	
	static void display(int mat[][], int row, int col) { 
		for (int i = 0; i < row; i++) { 
			for (int j = 0; j < col; j++) 
				System.out.print(mat[i][j] + ", "); 
			System.out.print("\n"); 
		} 
    System.out.println();
	} 
	
	public static void main (String[] args) {
    // N = 3
    int mat[][] = { {6, 1, 1}, {4, -2, 5}, {2, 8, 7} };  // -306
    // N = 4
		//int mat[][] = { {1, 0, 2, -1}, {3, 0, 0, 5}, {2, 1, 4, -3}, {1, 0, 5, 0} }; // 30
		System.out.print("Matrix Determinant: "+ determinant(mat, N) + "\n"); 
	} 
} 

