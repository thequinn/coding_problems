// Matrix Determinant
// - NOTE: 
// -- See the link to optimize: https://www.jianshu.com/p/0fd8ac349b5e

#!/usr/bin/python

"""
How to calculate Matrix Determinant?

(1) For the 3x3 case, [ [a, b, c], [d, e, f], [g, h, i] ] or

|a b c|  
|d e f|  
|g h i|  

- determinant = a * det(a_minor) - b * det(b_minor) + c * det(c_minor)
- det(a_minor) refers to taking the determinant of the 2x2 matrix:

|- - -|
|- e f|
|- h i|  

(2)數學理論：https://www.mathsisfun.com/algebra/matrix-determinant.html
-- Important pattern in ex:  For 4×4 Matrices and Higher
>Notice: 
  Each term laternates its sign multiplier: +−+− pattern (+a... −b... +c... −d...)
"""

def minor(matrix, x, y):
  arr = []
  for i in range(1, len(matrix)):
    if i != x:
      arr_y = []
      for j in range(0, len(matrix[i])):
        if j != y: 
          arr_y.append(matrix[i][j])
          #print("arr_y: {}".format(arr_y))
    arr.append(arr_y)
    #print("arr: {}".format(arr))

  #print("before return, arr: {}\n".format(arr))
  return arr

def determinant(matrix):
  # Find which column it is to follow each sign multiplier. 
  sign = 1

  if len(matrix) < 2:       # Base Case
    #print("Base Case, matrix: {}".format(matrix))
    return matrix[0][0]
  
  while len(matrix) >= 2:   # Recursive Case
    if len(matrix) == 2:
      return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]
    else:
      tmp = 0
      for y in range(0, len(matrix[0])):
        #print("matrix[0][{}], minor(matrix, 0, {}): {}".format(y, y, minor(matrix, 0, y)))
        tmp += sign * (matrix[0][y]) * determinant(minor(matrix, 0, y))        
        sign = -sign;  # Terms are to be added with alternate sign
      return tmp
 
#lst = [[], []]                             # Not a working case
#lst = [[1,2], [3,4]]                       # -2
#lst = [ [2,5,3], [1,-2,-1], [1, 3, 4] ]    # -20
lst = [ [1, 0, 2, -1], [3, 0, 0, 5], [2, 1, 4, -3], [1, 0, 5, 0] ]    # 30
re = determinant(lst)
print(re)
