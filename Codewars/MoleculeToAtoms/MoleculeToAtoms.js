/*
var tokenExp = /([{(\[]|[})\]]|[A-Z][a-z]?)(\d*)/g

The above regex can be broken down into (See explanation later):
/
  (
    [{(\[]    |    [})\]]   |   [A-Z][a-z]?
  ) (\d*)
/g

----------------------------------------------
==> Explanation:

(x): 
- Matches 'x' and remembers the match. The parentheses are called capturing parentheses.
- ex. 
  The '(foo)' and '(bar)' in the pattern /(foo) (bar) \1 \2/ match and remember the first two words in the string "foo bar foo bar". The \1 and \2 denote the first and second parenthesized substring matches - foo and bar, matching the string's last two words.

  Note that \1, \2, ..., \n are used in the matching part of the regex. In the replacement part of a regex the syntax $1, $2, ..., $n must be used, e.g.: 'bar foo'.replace(/(...) (...)/, '$2 $1'). $& means the whole matched string.


[xyz]: 
- Character set. This pattern type matches any one of the characters in the brackets, including escape sequences.


"?": 
- Matches the preceding expression 0 or 1 time. Equivalent to {0,1}.

*/

function parseMolecule(formula) {
  var tokenExp = /([{(\[]|[})\]]|[A-Z][a-z]?)(\d*)/g
  var group, tokens, stack = [[]];

  // RegExp.exec(str):
  // - exec a search for a match in str. Returns a result array, or null.
  while (tokens = tokenExp.exec(formula)) {
    console.log("tokens: ", tokens);
  
    tokens[2] = tokens[2] || 1;
    console.log("tokens[2]: ", tokens[2]);
   
    // RegExp.test(str):
    // - exec a search for a match in str. Same as RegExp.exec(str), but 
    //   returns true or false.
    if (/^[A-Z]/.test(tokens[1])) {
      while (tokens[2]--) 
        stack.push( stack.pop().concat([tokens[1]]) );
    } 
    else if (/[{\(\[]/.test(tokens[1])) {
      stack.push([]);
    } 
    else {
      group = stack.pop();
      while (tokens[2]--) 
        stack.push(stack.pop().concat(group))
    }
  }

  return stack[0].reduce(function (count, x) {
    count[x] = (count[x] || 0) + 1;
    return count;
  }, {});
}


let formula = "";
formula = "Mg(OH)2"         // "H2MgO2", {'H': 2, 'Mg': 1, 'O': 2}
console.log( parseMolecule(formula) );

//formula = "K4(ON(SO3)2)2"   // "K4N2O14S4", {'K': 4, 'N': 2, 'O': 14, 'S': 4}
//formula = "Mg(OH)2"         // "H2MgO2", {'H': 2, 'Mg': 1, 'O': 2}
//formula = "Fe16H2O"
//formula = "FeH2O"
//formula = "H2O"             // "H2O", {'H': 2, 'O': 1}
//formula = "H"

