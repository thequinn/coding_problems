
# 技巧:
# - How to Differenciate b/t True/False/bool and 0:
# -- Using "is" keyword
#    ex. if x is False
#
def move_zeros(array):
  # The next 2 ln's are the same
  return sorted(array, key=lambda x: x==0 and type(x) is not bool)
  #return sorted(array, key=lambda x: x == 0 and x is not False)


#print(move_zeros([False,1,0,2])) # [False,1,2,0]
#print(move_zeros([False,1,0,1,2,0,1,3,"a"])) # [False,1,1,2,1,3,"a",0,0]

print(move_zeros([9, 0, 9, 0, 0, 9, 0, 0.0, 0, 0.0, 0, 0, 0]))
               # [9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
