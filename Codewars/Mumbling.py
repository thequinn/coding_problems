
# Method 1:
def accum(s):
  # enumerate(iterable, start)
  # - https://www.geeksforgeeks.org/enumerate-in-python/
  #
  # 'separator'.join(iterable)
  # - Return a string which is the concatenation of the strings in iterable.
  #
  return '-'.join(c.upper() + c.lower() * i for i, c in enumerate(s))

# Method 2:
def accum(s):

  # str.title()
  # - Return a titlecased version of the string where words start with an
  #   uppercase char and the remaining chars are lowercase.
  #
  # The 1 in enumerate(s, 1) is starting index.  So i = 1.
  return '-'.join(str.title(a * i) for i, a in enumerate(s, 1))

res = accum("abcd") # "A-Bb-Ccc-Dddd"
print("res: %s" % res)
