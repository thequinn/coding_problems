from functools import reduce
def persistence(n):
  count = 0

  while n >= 10:
    # The 2nd arg is a "Generator expression", so it should be parenthesized
    n = reduce( lambda a, b: a * b, (int(c) for c in str(n)) )
    count += 1

  return count

#-------------------------------------------------------------

from functools import reduce
import operator
def persistence(n):
  count = 0

  while n >= 10:
    # reduce()'s 3rd arg, initializer, is not needed
    n = reduce(operator.mul, [int(c) for c in str(n)], 1)
    count += 1

  return count

#-------------------------------------------------------------

print(persistence(39))  # 3
