// Method #1:
function rgb(r, g, b) {
  [r,g,b] = [r,g,b].map( a => a > 0 ? a : 0);
  [r,g,b] = [r,g,b].map( a => a > 255 ? 255 : a);

  // When (a<=15), go to "0" + a.toString(16).  This is b/c when (d<15), d
  // is converted to 1 char of Hex.
  return [r,g,b].map( a => (a > 15) ? a.toString(16) : "0" + a.toString(16)).join("").toUpperCase();
}

//--------------------------------
// Method #2:
function rgb(r, g, b){
  return toHex(r) + toHex(g) + toHex(b);
}

function toHex(d) {
  if(d < 0 ) {return "00";}
  if(d > 255 ) {return "FF";}
  
  // Break-down after next ln.
  return  ("0"+(Number(d).toString(16))).slice(-2).toUpperCase();
  //
//  let s1 = "0" + (Number(d).toString(16));
//  // Get the last 2 indices of s1.  This is b/c when (d<15), it's converted
//  // to 1 char of Hex.
//  let s2 = s1.slice(-2).toUpperCase();
//  console.log(s1, "\t, ", s2);
//  return s2;
}


let re;
//re = rgb(300, 0, -20);      // 'FF0000'
//re = rgb(173,255,47);       // 'ADFF2F'
//re = rgb(148, 0, 211)       // '9400D3'
re = rgb(153,160,10)        // '99A00A'
console.log("re:", re);
