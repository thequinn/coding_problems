// Method #1:
const factorial = n => {
  // 0! = 1;   1! = 1
  if (n <= 0)   return 1;  
  
  return n * factorial(n - 1);
};

// Method #2:
const factorial = n => n > 1 ? n*factorial(n-1) : 1;


let n = 5, re = 0;
re = factorial(n);
console.log(re);
