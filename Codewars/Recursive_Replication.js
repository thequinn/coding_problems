/*
Goal:
-The 2 methods compare when having re[] to return or not in a recursive call

- Analysis:
-- An arr is a type of JS Object, so it is always passed by ref in func.  Therefore, re[]'s contents can be modified in helper().  
-- So when to return re[] doesn't relate to pass-by-ref or pass-by val, but simply relate to return re[] at the right places

- Caution !!!!!!!!!
-- Due to computer hardware design flaws, even though both methods are correct, if we run them at the same time, one of them returns undefined.  To get correct result, completely comment out code in one method before executing.
*/


// Method #1:
function replicate_1(times, num) {
  let re = [];
  // Compare to ln-41~42
  return helper(re, times, num);
}

function helper(re, times, num) {
  // Compare to ln-47
  if (times < 1)    return re;
  
  re.push(num);    // Return len of re[]
  return helper(re, times - 1, num)
}

let re_1 = replicate_1(3, 5);
console.log("CORRECT - Method #1: ", re_1)


//-------------------------------
// Method #2:
/*function replicate2(times, num) {
  let re = [];

  // WRONG!!
  //return helper(re, times, num);
  //
  helper(re, times, num);
  return re;
}

function helper(re, times, num) {
  // Returns nothing, so if usiing ln-39, it returns nothing, either
  if (times < 1)    return;
  
  re.push(num);    // Return len of re[]
  helper(re, times - 1, num)
}

let re2 = replicate2(3, 5);
console.log("CORRECT - Method #2: ", re2)*/
