import re
from string import ascii_uppercase

#----------------------------------------------------------------

# Method 1:
def alphabet_position(text):

  # zip():
  # - returns a zip object, which is an iterator of tuples where the first item
  #   in each passed iterator is paired together, and then the second item in
  #   each passed iterator are paired together etc.
  zipped = zip(ascii_lowercase, map(str, range(1,27)))

  # class dict(iterable, **kwarg):
  # - Convert iterable to dict
  AZ = dict(zipped)
  #print(AZ)


  # remove non-alphabets
  s = re.sub('[^a-zA-Z]', '', text.lower())

  # Split s into arr. ==> split() doesn't work here!
  #
  # list(): Convert sequence types to a list
  a = list(s)
  #print(a)

  return ' '.join(AZ[i] for i in a)

#----------------------------------------------------------------

# Method 2:
def alphabet_position(text):
  # ord(c):
  # - return an int representing the unicode char
  return ' '.join(str(ord(c) - 96) for c in text.lower() if c.isalpha())

#----------------------------------------------------------------

res = alphabet_position("The sunset sets at twelve o' clock.")
print(res) # "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11"
