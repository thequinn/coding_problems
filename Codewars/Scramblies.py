
# Method 1:
from collections import Counter
def scramble(s1, s2):

  # collections.Counter()
  # - a container that stores elements as dictionary keys, and their counts
  #   are stored as dictionary values.
  #
  # 這行是指 Counter(s1) 減剩下的, 不是Counter(s2)
  x = Counter(s2) - Counter(s1)
  #print(x)

  return not(x)


# Method 2:
from collections import Counter
def scramble(s1, s2):
  x = Counter(s2) - Counter(s1)
  #print(x)

  return len(x) == 0


print(scramble('katas', 'steak')) # False
print(scramble('rkqodlw', 'world')) # True
