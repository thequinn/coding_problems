// Method #1: 
// - Using String.search() to search for reges, but NOT work for 3rd test case below
function pigIt(str){
  let regex = /[,!?@#$%^&*()]/;  

  // split str to arr of strs
    // take 1st letter of curr work, concat it w/ "ay", attach it to end of word
  let arr = str.split(' ');
  return arr.map((curr, i) => {
    if (curr.search(regex) === -1) {
      return curr.slice(1) + curr[0] + 'ay';
    }
    return curr;
  }).join(' ');
  //
  // Remember!! 
  // - Array.map() returns a new array !!!  
  // -- So try to use arr.map().join() instead of breaking into arr.map() and join().
}

// Method #2: Success
function pigIt(str) {
  return str.split(' ').map(v=>v.match(/[A-Za-z]/)?v.slice(1)+v.slice(0,1)+'ay':v).join(' ')
}

let re = "";
//re = pigIt('Pig latin is cool'); // igPay atinlay siay oolcay
//re = pigIt('Hello world !');     // elloHay orldway !
re = pigIt("Quis custodiet ipsos custodes ?"); // uisQay ustodietcay psosiay ustodescay ?
console.log("re: ", re);



