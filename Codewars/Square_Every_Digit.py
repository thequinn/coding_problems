# Method #1:
def square_digits(num):
    reurn int(''.join(str(int(n)**2) for n in str(num)))

# Method #2:
def square_digits(num):
    return int(''.join( str(n**2) for n in map(int, str(num)) ))

print(square_digits(9119)) # 811181
