
/*
Tips:
- We use pos[x, y] to store row and col of the grid.  
  ex. pos[x] is row, pos[y] is col

- Notice!
-- No need a 2D array, pos[x][y], to store current pos.  The correct way is to use a 1D array, pos[x, y], w/ 2 elems to track curr cursor.  
*/

function streetFighterSelection(fighters, pos, moves){
  var result = [];
  
  moves.forEach(function(move) {
    if (move === "up") {
      console.log("move: ", move, ", pos[0]: ", pos[0]);
      pos[0] = 0;
    }
    else if (move === "down") {
      console.log("move: ", move, ", pos[0]: ", pos[0]);
      pos[0] = 1;
    }

    // NOTE!
    // - JS has no circular array
    // ex. arr = [0, 1, 2];  arr[-1] is undefined. arr[3] is undefined.
    else if (move === "right") {
      console.log("move: ", move, ", pos[1]: ", pos[1]);
      pos[1] = (pos[1] === 5) ? 0 : pos[1] + 1;
    }
    else if (move === "left") {
      console.log("move: ", move, ", pos[1]: ", pos[1]);
      pos[1] = (pos[1] === 0) ? 5 : pos[1] - 1;
    }
    
    result.push(fighters[pos[0]][pos[1]]);
  });
  return result;
}

fighters = [
	["Ryu", "E.Honda", "Blanka", "Guile", "Balrog", "Vega"],
	["Ken", "Chun Li", "Zangief", "Dhalsim", "Sagat", "M.Bison"]
];

moves = ['up', 'left', 'right', 'left', 'left'];
// ['Ryu', 'Vega', 'Ryu', 'Vega', 'Balrog']);
console.log(streetFighterSelection(fighters, [0, 0], moves));

//moves = ["left","left","left","left","left","left","left","left"];
// ['Vega', 'Balrog', 'Guile', 'Blanka', 'E.Honda', 'Ryu', 'Vega', 'Balrog']

//moves = ["right","right","right","right","right","right","right","right"];
// ['E.Honda', 'Blanka', 'Guile', 'Balrog', 'Vega', 'Ryu', 'E.Honda', 'Blanka']

//moves = ["up","left","down","right","up","left","down","right"];
// ['Ryu', 'Vega', 'M.Bison', 'Ken', 'Ryu', 'Vega', 'M.Bison', 'Ken']

//moves = ["down","down","down","down"];
// ['Ken', 'Ken', 'Ken', 'Ken']

//moves = ["up","up","up","up"];
// ['Ryu', 'Ryu', 'Ryu', 'Ryu']

