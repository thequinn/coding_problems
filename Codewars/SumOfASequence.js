const sequenceSum = (begin, end, step) => {
  if (begin > end)    return 0;
  //
  // No need this as "Base Case" b/c the last if-statement convers it.  
  //if (begin == end)   return begin;
  
  return begin + sequenceSum(begin + step, end, step);
};


let re;
re = sequenceSum(2, 6, 2); // 12
console.log("re: ", re);
re = sequenceSum(1, 5, 1); // 15
console.log("re: ", re);
re = sequenceSum(1, 5, 3); // 5
console.log("re: ", re);
