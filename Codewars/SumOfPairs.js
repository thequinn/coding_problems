// Method X: 
// - using Array.forEach() will "FINISH looping all elems" in its arr.  So if there
//   are more than 1 pair of solutions, the last pair is used.  But this exer wants
//   the 1st pair of sol.
//
/*var sum_pairs=function(ints, target){
  let seen = {};
  let prev, re;

  // NOTICE!! 
  // - The return value of Array.forEach() is undefined !
  //
  ints.forEach(curr => {
    prev = target - curr;
    if ( seen[prev] ) { 
      console.log("prev: :", prev, "curr: ", curr);
      
      // This "return" only returns to ints.forEach() where this callback belongs
      return re = [prev, curr];
    }
    console.log("seen[" + curr + "]: true");
    seen[curr] = true;
  });

  return re;
}*/

var sum_pairs=function(ints, s){
  var seen = {}
  for (var i = 0; i < ints.length; ++i) {
    if (seen[s - ints[i]]) {
      //console.log("prev: ", s - ints[i], "curr: ", ints[i]);
      return [s - ints[i], ints[i]];
    }
    seen[ints[i]] = true;
  }
}

let arr, sum;
arr = [1,2,3,4,1,0];  sum = 2;     // [1, 1]

re = sum_pairs(arr, sum);
console.log("re: ", re);

//arr = [11, 3, 7, 5];  sum = 10;  // [3, 7]
//arr = [20, -13, 40];  sum = -7;    // undefined
