/*
Sum The Tree

Task: 
  Return the sum of all values in the tree, including the root. Absence of a node will be indicated with a null value.
*/

// Method #1:
function sumTheTreeValues(root){
  let sum = 0;

  // if root is undefined (= val not assigned) or empty
  if (root === undefined || Object.keys(root).length === 0)    
    return null;

  sum += root.value;
  root.left ? sum += sumTheTreeValues(root.left) : sum;
  root.right ? sum += sumTheTreeValues(root.right) : sum;
 
  return sum;
}

// Method #2:
function sumTheTreeValues(root) {
  if(!root) return 0;
  return root.value + sumTheTreeValues(root.left) + sumTheTreeValues(root.right);
}


let re = null;

// undefined: var is declared, but no val is assigned
var emptyNode;  
re = sumTheTreeValues(emptyNode);
console.log("re: ", re);

// empty: var is declared and val is assigned.  But the Obj's has no val.
var emptyNode2 = {};  
re = sumTheTreeValues(emptyNode2);
console.log("re: ", re);


// Sums up all children
var simpleNode = { 
  value: 10, 
  left: {value: 1, left: null, right: null}, 
  right: {value: 2, left: null, right: null}
};
re = sumTheTreeValues(simpleNode); // 13
console.log("re:", re)
  

// Handles unbalanced trees
var unbalancedNode = {
  value: 11, 
  left: {value: 0, left: null, right: null}, 
  right: {
    value: 0, 
    left: null, 
    right: {value: 1, left: null, right: null}
  }
};
re = sumTheTreeValues(unbalancedNode); // 12
console.log("re:", re)
