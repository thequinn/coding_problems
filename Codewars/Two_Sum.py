
def two_sum(numbers, target):
  for i, ei in enumerate(numbers):
    for j, ej in enumerate(numbers):
      #print("i:%i, ei:%i, j:%i, ej:%i" % (i, ei, j, ej))
      if j != 0 and ei + ej == target:
        return (i, j)

print(two_sum([1, 2, 3], 4)) # (0, 2)
print(two_sum([2,2,3], 4)) # (0,1)
