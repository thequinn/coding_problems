

# Method 1:
#
# 這個技巧要學 (控制 prev 和 curr), 因為我以前的方法不好!
def unique_in_order(iterable):
  res = []
  prev = None

  for curr in iterable:
    if curr != prev:
      res.append(curr)
      prev = curr
  return res

#----------------------------------------------------------

# Method 2:
unique_in_order = lambda l: [z for i, z in enumerate(l) if i == 0 or l[i - 1] != z]

#----------------------------------------------------------

# Method 3:
from itertools import groupby
def unique_in_order(iterable):
  # itertools.groupby(iterable, key=None)
  # - Make an iterator that returns consecutive keys and groups from the
  #   iterable.
  # - The key is a function computing a key value for each element.
  #
  # "_": ignore the value
  return [k for (k, _) in groupby(iterable)]

#----------------------------------------------------------

print(unique_in_order('AAABBCCD')) # ['A', 'B', 'C', 'D']
print(unique_in_order('AAABBCCDD')) # ['A', 'B', 'C', 'D']

print(unique_in_order('ABBCcAD'))         # ['A', 'B', 'C', 'c', 'A', 'D']
print(unique_in_order([1,2,2,3,3]))       # [1,2,3]
