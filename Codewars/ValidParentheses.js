// Method #1:
// - Best practice to write an actual parser
function validParentheses(string){
  var tokenizer = /[()]/g, // ignores characters in between; parentheses are
      count = 0,           // pretty useless if they're not grouping *something*
      token;

  // exec():
  // - exec a search for a match in a specified string. Returns arr or null.
  while (token = tokenizer.exec(string), token !== null){
    console.log("token: ", token);

    if (token == "(") {
      count++;
    } 
    else if (token == ")") {
      count--;
      if (count < 0) {
        return false;
      }
    }
  }
  return count == 0; // false
}

// Method #2: Concise version of Method #1
function validParentheses(parens){
  var indent = 0;
  
  for (var i = 0 ; i < parens.length && indent >= 0; i++) {
    indent += (parens[i] == '(') ? 1 : -1;    
  }
  
  return (indent == 0);
}

// Method #3: 
// - Same as Method #2,  but compare having the 3rd if in Method #2 to not 
//   having it in Method #2
function validParentheses(parens){
  var n = 0;
  for (var i = 0; i < parens.length; i++) {
    if (parens[i] == '(') n++;
    if (parens[i] == ')') n--;
    if (n < 0) return false;
  }
  
  return n == 0;
}


let re = null, parens = "";
parens = "()";              // true
//parens = ")(()))";          // false
//parens = "(";               // false
//parens = "(())((()())())";  // true

re = validParentheses(parens);
console.log("re: ", re);
