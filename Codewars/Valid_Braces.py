
# Method 1:
def validBraces(s):
  braces = {"(": ")", "[": "]", "{": "}"}
  stack = []

  for c in s:
    # print("c: %s" % c)
    if c in braces.keys():
      stack.append(c)
    else:
      if len(stack) == 0 or braces[stack.pop()] != c:
        return False

  #return True      # Not handle case: "("
  return len(stack) == 0


# Method 2:
def validBraces(s):
  while '{}' in s or '()' in s or '[]' in s:

    # string.replace(old, new, count)
    s = s.replace('{}','')
    print("1. " + s)

    s = s.replace('[]','')
    print("2. " + s)

    s = s.replace('()','')
    print("3. " + s)

  return s == ''



# 注意! "[()]" is valid, but "[(])" is invalid
#print(validBraces("[(])"))  # F

#print(validBraces("(){}[]"))  #T
print(validBraces("([{}])"))  #T
#print(validBraces("("))  # F
