
def parse_IPv6_1(iPv6):
  return ''.join(str(sum(int(c, 16) for c in s)) for s in iPv6.split(iPv6[4]))

'''  以下是上一行的分解:
  return ''
  .join(
    str(
      sum(
        int(c, 16) for c in s
      )
    )

    for s in iPv6.split(iPv6[4])
  )

'''

def parse_IPv6_2(iPv6):
  strList = [s for s in iPv6.split(iPv6[4])]
  print(strList)

  intList = [[sum(int(c, 16) for c in s)] for s in iPv6.split(iPv6[4])]
  print(intList)

  strList2 = [str(num[0]) for num in intList]
  print(strList2)

  res = ''.join(strList2)
  print(res)

  return res

#-----Test Case-----
s = "ABCD_1111_ABCD_1111_ABCD_1111_ABCD_1111"
# => "46" + "4" + "46" + "4" + "46" + "4" + "46" + "4" => 464464464464

res = parse_IPv6_2(s)
print("Final res:", res)
