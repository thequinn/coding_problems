from math import ceil, log, sqrt

"""
What's a Perfect Power anyway?

- Def: 
In mathematics, a perfect power is a positive integer that can be expressed as an integer power of another positive integer. 
More formally, n is a perfect power if there exist natural numbers m > 1, and k > 1 such that m^k = n.

ex:
81 = 3^4 = 9^2. But we use [3, 4] as solution. Although [3,4] and [9,2] are both correct, the Codewars tests take care of this, so if a number is a perfect power, return any pair that proves it.  So to simplify, use 

- 拆題:
核心： m^k = n ==> log(n, m) = k, m is log base
(step 1) Try all numbers, m, whose square is less than n
(step 2) If m^k equals to n, we find a perfect power
"""

def isPP(n):
  # Try all numbers, m, where m^2 is less than n. 
  for m in range(2, int(sqrt(n)) + 1):

    # - log(n, k): read as log n w/ base k 
    # - m is used as log base to generate k.  Since k might not be an int, we use   
    # round() to make k and int.
    #
    # - round(): return a floating pt num that's rounded w/ specified num of decimals
    k = int(round(log(n, m)))
    print("m:", m, ", k:", k, " n:", n)
    
    # if m^k == n, we find a perfect power 
    if m ** k == n:  
      return [m, k]
  
  return None


re = isPP(243)
print(re)

#re = isPP(81) 
# [3,4].  Although [3,4] and [9,2] are both correct, the tests take care of this, so if a number is a perfect power, return any pair that proves it.
#print(re)

#re = isPP(9) # [3,2]
#print(re)
#re = isPP(5) # None
#print(re)

