// Method #1-1:
function anagrams(word, words) {
  word = word.split('').sort().join('');
  //console.log("word:", word);

  words= words.filter((v, i) => {
    let tmp = v.split('').sort().join('');
    //console.log("v:", v, ", tmp:", tmp);

    // - WRONG !!
    // - This is b/c filter() creates a new array with all elements that
    //   pass the test implemented by callback.
    //return word == tmp ? v : null;
    //
    // - Correct:
    return word == tmp;
  });

  //console.log("words:", words);
  return words;
}

//--------------------------------------------
// Method 1-2:
String.prototype.sort = function() {
  return this.split("").sort().join("");
};

function anagrams(word, words) {
  return words.filter(function(x) {
      return x.sort() === word.sort();
  });
}

let re;

re = anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']); 
// ['carer', 'racer']

//re = anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']);  
// ['aabb', 'bbaa']

//re = anagrams('laser', ['lazing', 'lazy',  'lacer']);    // []

console.log(re);
