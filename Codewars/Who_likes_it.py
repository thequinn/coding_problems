
# Method 1:
def likes(names):

  # str.format(), ln-18, will take its passed args , format them, and place
  # them in {}
  d = {
    0: "no one likes this",
    1: "{} likes this",
    2: "{} and {} like this",
    3: "{}, {} and {} like this",
    4: "{}, {} and {others} others like this"
  }

  n = len(names)

  # min(): get the dictionary key
  # *names: unpacks a list into args
  return d[min(n,4)].format(*names, others=n-2)

#----------------------------------------------------

# Method 2:
def likes(names):
  n = len(names)

  return {
    0: 'no one likes this',
    1: '{} likes this',
    2: '{} and {} like this',
    3: '{}, {} and {} like this',
    4: '{}, {} and {others} others like this'
  }[min(4, n)].format(*names[:3], others=n-2)
  # *name[:3]: You only use first 3 names

#----------------------------------------------------

print(likes([])) # "no one likes this"
print(likes(["Peter"])) # "Peter likes this"
print(likes(["Jacob", "Alex"])) # "Jacob and Alex like this"
