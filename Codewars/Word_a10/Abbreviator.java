/*
Word a10n (abbreviation)

(1) Abbreviator.java
- using String.split() ==> Not a smart method  

(2) Word_a10n.js
- using String.replace() to handle each sub-str inplace ==> Smart!
*/

public class Abbreviator {
  
  public static String abbreviate(String s) {
    // - Split s w/ non-alphabetical chars
    // - "+" in the regex  means matching multiple alphabetical chars each time
    String[] words = s.split("[^a-zA-Z]+");
    display(words);

    // Split s w/ alphabetical chars 
    String[] separators = s.split("[a-zA-Z]+");
    display(separators);

    String result = "";

    int i = 1;
    for (String word : words) {
      result += processWord(word);
      System.out.println("for, result: (" + result + ")");

      if (i < separators.length) {
        result += separators[i++];
        System.out.println("if, result: (" + result + ")");
      }
    }
    return result;
  }

  public static String processWord(String word) {
    if (word.length() < 4) 
      return word;
    
    int n = word.length() - 2;
    return "" + word.charAt(0) + n + word.charAt(word.length() - 1);
  }

  public static void display(String[] arr) {
    for (int i = 0; i < arr.length; i++) {
      System.out.print("arr[" + i + "]: (" + arr[i] + "); ");
    }
    System.out.println();
  }

  public static void main(String[] args) {
    Abbreviator a = new Abbreviator();

    //String s = new String("elephant---rides are really fun!");
    String s = "Sky-top truly rocks!";
    
    String re = abbreviate(s);
    System.out.println("re: " + re);
  }
}
