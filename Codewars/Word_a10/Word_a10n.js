/*
Word a10n (abbreviation)

(1) Abbreviator.java
- using String.split() ==> Not a smart method  

(2) Word_a10n.js
- using String.replace() to handle each sub-str inplace ==> Smart!
*/

let abbreviate = function(str) {
  let newWord = "";
  /*
  String.replace(separator, callback):
  - (s1) Grabs each matching substr
  - (s2) Uses the callback to handle the substr  
  - (s3) Replace the substr w/ the handled substr.  
  -- So the substr is handled inplace in s.
  */
  str = str.replace(/[a-z]+/gi, word => {
    newWord = (word.length <= 3) ? 
                word : 
                `${word[0]}${word.length-2}${word[word.length-1]}`;
    return newWord;
  });

  return str;
}


let re = "";

//re = abbreviate("internationalization");  // "i18n"
//re = abbreviate("Accessibility");         // "A11y"

//re = abbreviate("elephant-ride");           // "e6t-r2e"

re = abbreviate("Sky-top truly rocks!");    // "Sky-top t3y r3s!"
console.log("re: ", re);

//re = abbreviate("elephant-rides are really fun!");  // "e6t-r3s are r4y fun!"
