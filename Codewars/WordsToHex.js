/*
Words to Hex

Task:
  Each word will represent a hexadecimal value by taking the first three letters of each word and find the ASCII character code for each character. You will then combine these values into one readable RGB hexadecimal value, ie, #ffffff.

*/

// Method #1-1:
// - same as Method #1-2, but w/o comments and logs
//
/*function wordsToHex(words) {
  return words.split(' ').map(w =>
    '#' + [0, 1, 2].map(i =>
      (w.charCodeAt(i) || '00').toString(16)
    ).join('')
  );
}*/

// Method #1-2:
function wordsToHex(words) {

  let arr = words.split(' ');
  console.log("arr:", arr);

  return arr.map(w => {

    // 技巧：
    // - using [0, 1, 2].map( i -> .....) makes the 1st arg of map() the
    //   index of [0, 1, 2]
    return '#' + [0, 1, 2].map(i => {
      console.log("\ni:", i);
     
      //----------------------------------------------
      // Break-down of ln-41:

      // Return the Unicode of the i-th char in a str
      let x = w.charCodeAt(i);
      console.log("x: ", x);

      // OR '00' b/c 1 Hex has 2 chars, and x could be NaN
      let y = x || '00';
      console.log("y: ", y);

      // Check the what object the toString(16) in ln-40 belong to
      let y_type = Object.prototype.toString.call(y);
      console.log("y_type: ", y_type);
      
      let z = y.toString(16);
      console.log("z: ", z);
      //----------------------------------------------
      
      return (w.charCodeAt(i) || '00').toString(16)
    }).join('');
  });
}

let re;
re = wordsToHex("Hello, Im Gary");
// [ '#48656c', '#496d00', '#476172' ]

//re = wordsToHex("Hello, my name is Gary and I like cheese.");
//['#48656c', '#6d7900', '#6e616d','#697300','#476172','#616e64','#490000','#6c696b','#636865']

//re = wordsToHex("0123456789");  // [ '#303132' ]

//re = wordsToHex("ThisIsOneLongSentenceThatConsistsOfWords");
// [ '#546869' ]


//re = wordsToHex("&&&&& $$$$$ ^^^^^ @@@@@ ()()()()(");
//[ '#262626', '#242424', '#5e5e5e', '#404040', '#282928' ]

console.log("re: ", re);
