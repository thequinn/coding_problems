def order(sentence):

  # sorted(iterable, key=None, reverse=False)
  # - key: specifies a function of one argument that is used to extract a
  #   comparison key from each element in iterable (ex. key=str.lower). The
  #   default value is None (compare the elements directly).
  #
  l = sorted(sentence.split(), key = lambda w: sorted(w))
  #print(l)

  return ' '.join(l)


print(order("is2 Thi1s T4est 3a")) # "Thi1s is2 3a T4est"
#print(order("4of Fo1r pe6ople g3ood th5e the2")) # "Fo1r the2 g3ood 4of th5e pe6ople"
#print(order("")) # ""
