/*
int32 to IPv4

Task:
  Complete the function that takes an unsigned 32 bit number and returns a string representation of its IPv4 address.

*/

// Method #3: Best
function int32ToIp(int32) {
  // >>>: Zero-fill right shift
  return ((int32 >>> 24) & 0xFF) + '.' +
         ((int32 >>> 16) & 0xFF) + '.' +
         ((int32 >>>  8) & 0xFF) + '.' +
         (int32        & 0xFF);
}


// Method #2-1: Best
// - Shift right by 8 bits.  Bitwise and w/ 255(10)
const int32ToIp = int32 => [24, 16, 8, 0].map(e => int32 >> e & 255).join`.`;
//
// Method #2-2: Best
// - Shift right by 8 bits.  Bitwise and w/ 0xff(16)
const int32ToIp = int32 => [24, 16, 8, 0].map(e => int32 >> e & 0xff).join`.`;


// Method #1
/*function int32ToIp(int32){
  let result = [];
  let x = 255; // 225 is in decimal. Same as x = 0xff in hex

  for(var i = 0; i < 4; ++i){
    // int32 & x: bitwise and
    result.push(int32 & x);

    // 8 bits of a binary = 1 hex
    int32 = int32 >>> 8; 
  }

  result.reverse();
  result = result.join('.');

  return result;
}*/

let re;
//re = int32ToIp(0);            // "0.0.0.0"
re = int32ToIp(5);            // "0.0.0.5"
console.log(re);

//re = int32ToIp(2149583361);   
// 1000 0000 0010 0000 0000 1010 0000 0001 => "128.32.10.1"
//console.log(re);

//re = int32ToIp(2154959208);   // "128.114.17.104"
//re = int32ToIp(2149583361);   // "128.32.10.1"

