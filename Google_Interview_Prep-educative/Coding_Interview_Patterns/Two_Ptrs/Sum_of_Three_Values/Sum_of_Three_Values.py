'''
Sum of Three Values

https://www.educative.io/interview-prep/GZjlABCO9MZzY5lX0/10370001/5803123687424000/5436644395712512


(1) Approach #1: Brute Force 

The naive approach to solving this problem is to use three nested loops. Each nested loop starts at the index greater than its parent loop. For example, if we use the iterators i, j, and k in the loops, j will start from i + 1, and k will start from j + 1. This approach will check all the possible triplets to see if they sum up to the required target value.


Time Complexity: O(n^3)
- Since we’re using three nested loops

Space Complexity: O(1), which is Constant Time Complexity
- we aren’t using any extra space to get to the final output



(2) Approach #2: Two Ptrs

The essence of this algorithm is in its smart use of the two pointers, focused on identifying three elements that achieve a specific target sum together. It starts by sorting the array, which facilitates the search process. One element is fixed as a reference point. The two pointers are set at one element ahead of the array’s fixed reference point and end of the array, respectively. Then, the algorithm dynamically adjusts the two pointers based on their combined sum’s closeness to the target. If the combined sum is less than the target, the algorithm moves the lower pointer to increase the sum. If the sum is greater, it moves the upper pointer down to decrease it. This step-by-step adjustment helps simplify the search, minimizing the potential combinations.

Now, let’s look at the workflow of the implementation of the algorithm:

First, we sort the input array, nums, in ascending order. This is because traversing an unsorted array would lead to a bad time complexity. If the input array is sorted, we can easily decide, depending on the sum of the current triplet, whether to move the low pointer toward the end, or, the high pointer toward the start. Next, we iterate over the elements in nums using the index i, 
where i < nums.length - 2 (See Note below). Against each nums[i], we find the other two integers that complete the triplet whose sum equals the target value, that is, nums[i] + nums[low] + nums[high] == target. We do this by traversing nums with the low and high pointers. In each iteration, the traversal starts with the low pointer being at nums[i+1] and the high pointer at the last element of nums. Then, depending on the current sum value, we move these pointers as follows:

If the sum of the triplet is equal to the target, we return TRUE. Otherwise, we continue.

If the sum of the triplet is less than the target, we move the low pointer forward, that is, toward the end. The aim is to increase the value of the sum so that it gets closer or equal to the target value.

If the sum of the triplet is greater than the target, we move the high pointer toward the start. The aim is to reduce the value of the sum so that it gets closer or equal to the target value.

We repeat this for each iteration until we get the required triplet.

Note: 
- As per the problem statement, each number in a triplet, nums[i], nums[low], and nums[high], should be a different element of nums, so i can’t be equal to low or high, and low can’t be equal to high. Therefore, we keep the loop restricted to nums.length - 2.

'''

def find_sum_of_three(nums, target):
    
    nums.sort()
    print("target:", target, ", nums.sort():", nums)
    
    for i in range(0, len(nums)-2):
        low = i + 1
        high = len(nums) - 1
      
        while low < high:
            print("\ni:", i, ", low:", low, ", high:", high)

            triplet = nums[i] + nums[low] + nums[high]
            print("triplet:", triplet)
      
            if triplet == target: 
                print("triplet == target:")
                return True

            elif triplet < target: 
                print("triplet < target:")
                low += 1
            
            else: 
                print("triplet > target:")
                high -= 1

        return False

def main():
    
    nums = [-1,-3,-4,1,2,5]
    target = -1

    #nums = [0,1,2,3,4,5,7,8]
    #target = 10
    
    if find_sum_of_three(nums, target):
            print("\tSum for", target, "exists")
    else:
            print("\tSum for", target, "does not exist")

if __name__ == '__main__':
    main()
