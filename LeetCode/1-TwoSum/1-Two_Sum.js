/*
2. Two Sum

- LeetCode Std Solution:
    https://leetcode.com/problems/two-sum/solution/
- The link below show JS version:
    https://leetcode.com/problems/two-sum/discuss/231428/Javascript-the-3-sample-solutions-with-notes
*/

// Approach #1: Brute Force
// - Time complexity: O(n^2)
// - Space complexity: O(1)
//
var twoSum = function(nums, target) {
  let re = [];

  for (let i = 0; i < nums.length; i++) {
    for (let j = i+1; j < nums.length; j++) {

      if (nums[i] + nums[j] === target) {

        // Same as next return method
        //re.push(i, j);
        //return re;
        //
        // Make an arr and return it directly
        return [i, j];
      }
    }
  }
}

// Approach #2: Two-pass Hash Table / Hash Map
// - 思路:
// -- Find the complement of nums[i] in Hash by checking the key.
// -- Note!!
//      Use vals of nums[] as keys, not index of nums[].  否則不好寫!
//
// - Time complexity: O(n)
// -- We traverse the list twice. Hash reduces the look up time to O(1).
//    Total time complexity is O(n)
//
// - Space complexity: O(n)
// -- map{} gets resized every time when a new item is addded to it.
// -- 解釋：
//  https://stackoverflow.com/questions/58353499/not-sure-what-space-complexity-my-map-object-uses-in-the-example/58354429#58354429
//
var twoSum = function(nums, target) {
  let map = {};
  for (let i = 0; i < nums.length; i++) {
    map[ nums[i] ] = i;
  }
  for (let i = 0; i < nums.length; i++) {
    let complement = target - nums[i];
    let j = map[complement];

    // WRONG: j is an index, so if one of the two sum has index = 0, error!
    // if (j && j != i)
    //
    if (j !== undefined && j != i)
      return [i, j];
  }
}

// Approach #3_1: One-pass Hash
// - 思路:
// -- While we iterate and inserting elements into the table, we also look back to check if current element's complement already exists in the table.
//
// - Time complexity: O(n), traverse the list w/ n elems.
// - Space complexity: O(n), n elems stored in hash tbl.
//
var twoSum = function(nums, target) {
  const map = {};

  for (let i = 0; i < nums.length; i++) {
    const cmpl = target - nums[i];  // cmpl of nums[i]

    if (cmpl in map) {  // in: Check if key in map{}
        return [i, map[cmpl]];
    }
    map[ nums[i] ] = i;
  }

  return null;
};

// Approach #3_2: One-pass Hash -> ln-94~101: ALWAYS check if key exists in {}
var twoSum = function(nums, target) {
  let map = {};

  for (let i = 0; i < nums.length; i++) {
    let cmpl = target - nums[i]; // cmpl of nums[i]

    // WRONG!!
    // - [2,7,11,15], 9 => when curNum=7, map[cmpl] returns index = 0
    //if (map[cmpl]) {
    //
    // Correct !
    if (map[cmpl] != null) {
      return [i, map[cmpl]];
    }
    // If not, add to map{}: nums[i] as key, and its "index" as val !!
    map[ nums[i] ] = i;
  }
  return null;
};

//-----Test Case-----
let nums = [2, 7, 11, 15], target = 9;  // [0, 1]
console.log( twoSum(nums, target) );
