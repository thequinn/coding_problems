'''
1. Two Sum

'''


'''
Approach #3: One-pass Hash/Dictionary

思路:
- High Level:
-- While we iterate and inserting elements into the table, we also look back to check if current element's complement already exists in the table.

# Use a hash to store the numbers already encountered
# Loop thr nums[] while checking (target - num[i]) exists in the hash
#   If yes, we found the pair
#   Else, store the cur num as key, its index as val

- Time complexity: O(n), traverse the list w/ n elems.
- Space complexity: O(n), n elems stored in hash tbl.
'''
class Solution:
  def twoSum(self, nums: List[int], target: int) -> List[int]:
    dct = {}
    for i, num in enumerate(nums):
      comp = target - num

      #if dict[comp]:  # Python 錯誤法 to test if an elem exists in dict
      if comp in dct:
        return [dct[comp], i]
      dct[num] = i

