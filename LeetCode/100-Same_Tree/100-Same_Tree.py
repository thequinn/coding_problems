'''
100. Same Tree

https://leetcode.com/problems/same-tree/solutions/642761/easy-to-understand-faster-simple-recursive-iterative-dfs-python-solution/

There're 3 Basic Conditions:
1) Both nodes NOT exist
2) 1 node exists
3) Both nodes exist
'''

'''
Approach 1: Recursive
  
  if both nodes not exist, return true
  if one node exists, return false
  if both nodes exist, 
    if val's diff, return false
    if val's same, recurse or return true
'''
class Solution:
    def recursive(self, p, q):
        if not p and not q:
            return True
        elif not p or not q:
            return False
        else:
            return p.val == q.val and self.recursive(p.left, q.left) and self.recursive(p.right, q.right)


'''
Approach 2-1: Iterative using Stack

'''
class Solution:        
    def isSameTree(self, p, q):
        # Note: Add the 2 nodes in 1 tuple
        stack = [(p, q)]

        while len(stack):
            n1, n2 = stack.pop()
            if not n1 and not n2:    pass
            elif not n1  or not n2:  return False
            else:
                if n1.val != n2.val: return False
                
                # Note: Add the 2 nodes in 1 tuple
                stack.append( (n1.left, n2.left) )
                stack.append( (n1.right, n2.right) )
        return True


'''
Approach 2-2: Iterative using Queue 
              => From Quinn. Basically the same as Stack

# - - - - - - - - - - - - - - - # - - - - - - - - - - - - - - - #
NOTES-A:

TypeError: cannot unpack non-iterable TreeNode object

- https://stackoverflow.com/questions/77842461/typeerror-cannot-unpack-non-iterable-treenode-object

- deque constructor expects "an iterable" of elements. When you create a deque, each element of that list/tuple is a separate element in the deque. 

    deque( [p, q] )    # 2 elems
    deque( [[p, q]] )  # 1 elem

'''
from collections import deque
class Solution:        
    def isSameTree(self, p, q):
        # WRONG!! CAN'T add 2 nodes in 1 tuple directly in a deque.  See NOTES-A above.
        # q = deque( [p, q] )
        q = deque( [[p, q]] )  # Compare it with Approach 2-1 using List

        while len(q) > 0:
            n1, n2 = q.popleft()
            if not n1 and not n2:    pass
            elif not n1  or not n2:  return False
            else:
                if n1.val != n2.val: return False
                
                # Note: Add the 2 nodes in 1 tuple to deque
                q.append( [n1.left, n2.left] )
                q.append( [n1.right, n2.right] )
        return True

