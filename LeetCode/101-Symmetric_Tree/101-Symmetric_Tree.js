/*
101. Symmetric Tree

Note:
Could you solve it both recursively and iteratively?

LeetCode Std Solution:
- https://leetcode.com/problems/symmetric-tree/solution/
- JS Version:
  https://leetcode.com/problems/symmetric-tree/discuss/33073/JavaScript-recursive-and-iterative-solutions
*/

// Approach 1: Recursive Pre-Order DFS
// - Solution (JS Version): 
// -- https://leetcode.com/problems/symmetric-tree/discuss/341522/JS-dfs-solution
// - 思路:
// -- Two trees are a mirror reflection of each other if:
//    (1) Their two roots have the same value.
//    (2) The right subtree of each tree is a mirror reflection of the left
//        subtree of the other tree.
//
// -- This is like a person looking at a mirror. The reflection in the
//    mirror has the same head, but the reflection's right arm corresponds
//    to the actual person's left arm, and vice versa.
//
//    The key is to recognize the tree as two different tree. For the first
//    tree, we go left -> right. For the second, we go right -> left.
//
// - Time complexity: O(n)
// -- we traverse the entire input tree once, the total run time is O(n),
//    where n is the total number of nodes in the tree.
// - Space complexity: O(n)
// -- The number of recursive calls is bound by the height of the tree. In
//    the worst case, the tree is linear and the height is in O(n).
// -- space complexity is O(1) if we ignore the recursion stack which is the
//    height of the tree.
//
var isSymmetric = function(root) {
  if (!root) { // Sanity check
    return true;
  }

  // Check if tree s & t are mirroring each other
  function isSymmetric(s, t) {
    if (!s && !t) {
      return true; // Both nodes are null, ok
    }
    if (!s || !t || s.val !== t.val) {
      return false; // Found a mismatch
    }
    // Compare the left subtree of `s` with the right subtree of `t`
    // and the right subtree of `s` with the left subtree of `t`
    return isSymmetric(s.left, t.right) && isSymmetric(s.right, t.left);
  }

  return isSymmetric(root.left, root.right);
};

// Approach 2: Iterative Pro-Order DFS
// - it's easy to convert Approach 1 to make it traverse iteratively
//   using stack
//
// - Time complexity is O(n)
// - Space complexity is the height of the tree.
function isSymmetric(p, q) {
  // Create two stacks
  var s1 = [p], s2 = [q];

  // Perform preorder traversal
  while (s1.length > 0 || s2.length > 0) {
    var n1 = s1.pop(), n2 = s2.pop();

    // Two null nodes, let's continue
    if (!n1 && !n2) continue;

    // Return false as long as there is a mismatch
    if (!n1 || !n2 || n1.val !== n2.val) return false;

    // Scan tree s from left to right
    // and scan tree t from right to left
    s1.push(n1.left); s1.push(n1.right);
    s2.push(n2.right); s2.push(n2.left);
  }
  return true;
}

// Approach 3: Iterative BFS
// - traverse both subtrees in level order, one from left to right, and 
//   the other is right to left, let's modify Approach 2.
//
// - Time complexity is O(n)
// - Space complexity is the width of the tree.
//
function isSymmetric(s, t) {
  var q1 = [s], q2 = [t];

  // Perform breadth-first search
  while (q1.length > 0 || q2.length > 0) {
    // Dequeue
    var n1 = q1.shift(), n2 = q2.shift();

    // Two null nodes, let's continue
    if (!n1 && !n2) continue;

    // Return false as long as there is a mismatch
    if (!n1 || !n2 || n1.val !== n2.val) return false;

    // Scan tree s from left to right
    // and scan tree t from right to left
    q1.push(n1.left); q1.push(n1.right);
    q2.push(n2.right); q2.push(n2.left);
  }
  return true;
}

