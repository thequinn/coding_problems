'''
101. Symmetric Tree

Explanation + Code Sol:
    https://www.youtube.com/watch?v=Mao9uzxwvmc


Intuition for Recursive Approach:
    
- If both "leftRoot" and "rightRoot" are null, 
    - return true
- If only one of "leftRoot" or "rightRoot" is null, 
    - return false
- If "leftRoot" and "rightRoot" are not null, and 
    (a) their values are equal, 
        - recursively call "isTreeSymmetric" on 
            (1) the left child of "leftRoot", and 
            (2) the right child of "rightRoot", and 
            (3) the right child of "leftRoot", and 
            (4) the left child of "rightRoot"
    (b) their values are not equal, 
        - return false
'''
class Solution:
    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        def dfs(l, r):

            # If both nodes Not exist
            if not l and not r: return True

            # If only one nodes exist
            if not l or not r: return False

            # If both nodes exist
            #
            # Extra: Using a ln break 
            #   - by putting code in "()"
            return (l.val == r.val and
                    dfs(l.left, r.right) and
                    dfs(l.right, r.left))
            #   - by using "\"
            #return l.val == r.val and \
            #       dfs(l.left, r.right) and \
            #       dfs(l.right, r.left)

        return dfs(root.left, root.right)
