'''
1011. Capacity To Ship Packages Within D Days

https://leetcode.com/discuss/general-discussion/786126/python-powerful-ultimate-binary-search-template-solved-many-problems


思路:

- Binary search probably would not come to our mind when we first meet this problem. We might automatically treat weights as search space and then realize we've entered a dead end. In fact, we are looking for the "minimal capacity" among all feasible capacities.
||
===> (1) 這題真正的需求:
     - Need the "min cap" of the ship to deliver all pkgs w/in D days.
     (2) Why use Binary Search?
     - b/c Binary Search find the num cloesest to the target num.  And the 
       sol below returns the left ptr, meaning it returns the smaller num 
       closest to the target.

- We dig out the monotonicity of this problem: if we can successfully ship all packages within D days with cap m, then we can definitely ship them all with any cap larger than m. 

(1) Now we can design a condition function, let's call it feasible, given an input cap, it returns whether it's possible to ship all packages within D days. 

This can run in a greedy way: if there's still room for the current package, we put this package onto the conveyor belt, otherwise we wait for the next day to place this package. If the total days needed exceeds D, we return False, otherwise we return True.

(2) Next, we need to initialize our boundary correctly. 
   
    Left boundary:
    (a) All pkgs take > 1 day to ship:
        - Capacity should be at least max(weights), otherwise the conveyor 
          belt couldn't ship the heaviest package. 
            
            cap >= max(weights)
    
    Right boundary:
    (b) All pkgs take 1 day to ship:

            cap <= sum(weights)
'''
class Solution:
    def shipWithinDays(self, weights: List[int], D: int) -> int:
        
        # Given an input cap of the ship, return whether it's possible to 
        # ship all pkgs within D days. This can run in a greedy way: if 
        # there's still room for the current pkg, add this pkg to the belt,
        # otherwise wait for the next day.
        def feasible(cap) -> bool:
            days = 1
            total_weight = 0

            for weight in weights:
                total_weight += weight

                if total_weight > cap: # too heavy, wait for the next 
                    # reset total to cur item's weight for next day
                    total_weight = weight
                    days += 1
                    if days > D: # cannot ship within D days
                        return False
            return True

        l, r = max(weights), sum(weights)
        while l < r:
            m = (l + r) // 2

            # Why we pass "m" in the condition func?  
            # - we're looking for m, which is "the least weight cap of the ship" that will result in all the pkgs being shipped w/in D days
            if feasible(m): r = m
            else:           l = m + 1
        return l

