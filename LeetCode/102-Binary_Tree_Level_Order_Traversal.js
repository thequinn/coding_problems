/*
102. Binary Tree Level Order Traversal

ex: Given binary tree [3,9,20,6,12,15,7],

         3
       /   \
      9     20
     / \    / \
    6  12  15  7

return its level order traversal as:

[ [ 3 ], [ 9, 20 ], [ 6, 12, 15, 7 ] ]


Solution::
- https://leetcode.com/problems/binary-tree-level-order-traversal/discuss/225081/JavaScript
*/

// Approach #1: BFS (Level-Order): Iterative using Queue
//
var levelOrder = function(root) {
  if (root == null)   return [];

  let queue = [root], res = [];

  while (queue.length) {
    //console.log("queue.length:", queue.length);
    let len = queue.length;
    let subList = [];

    while (len--) {
      let node = queue.shift();
      subList.push(node.val);

      if (node.left)    queue.push(node.left);
      if (node.right)   queue.push(node.right);
      //console.log("len:", len, ", queue.length:", queue.length);
    }
    res.push(subList);
  }
  return res;
}

//---------------------------------------------------------

// Approach #2-1: DFS (Pre-Order): Recursive
// - It implements pre-order traversal to realize level-order traversal.
//   Each time we add root.val into the list, and then look at left and
//   right child.
var levelOrder = function(root) {
  const res = [];

  const helper = (root, level=0) => {
    if (root == null)   return;

    // If res[level] is new, this elem is undefined.  Use "|| [ ]" to 
    // define an empty array
    //console.log(typeof res[level]);
    res[level] = res[level] || [];
    //console.log("res[", level, "]:", res[level])

    res[level].push(root.val);

    helper(root.left, level + 1);
    helper(root.right, level + 1);
  }

  helper(root);
  return res;
};

// Approach #2-1: DFS (Pre-Order): Recursive
// - Same as Approach #2-1, except ln57 vs. ln-82
//
var levelOrder = function(root) {
  let res = [];

  var helper = (res, root, level) => {
    if (root == null)   return;

    // If it's a new level, add an empty [] as res[]'s subarr.
    // 樹的每一層是一個 sub-arr in res[].
    // - ex. 樹有3層, level 和 res.length 都是 3
    if (level >= res.length) {
      res.push([]);
    }
    res[level].push(root.val);

    helper(res, root.left , level+1);
    helper(res, root.right, level+1);
  };

  helper(res, root, 0);
  return res;
}
