/*
103. Binary Tree Zigzag Level Order Traversal

LeetCode Std Sol:
- https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/solution/

*/


// Approach 1: BFS (Breadth-First Search)
// - https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/discuss/167512/javascript-clean-solution
//
var zigzagLevelOrder = function(root) {
  let res = [];
  helper(root, 0, res); // level 0 is root.  (level is depth)
  return res;
};

var helper = function(node, level, res){
  if(!node)     return;

  // If curr level doesn't exist, create an empty level for the curr level
  if(!res[level])     res[level] = [];

  level % 2 ? res[level].unshift(node.val) : res[level].push(node.val);

  helper(node.left, level + 1, res);
  helper(node.right, level + 1, res);
}

