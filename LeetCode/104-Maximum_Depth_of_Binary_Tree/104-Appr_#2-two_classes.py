'''
Original Java Sol:
- https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/34195/Two-Java-Iterative-solution-DFS-and-BFS

'''

class Solution:
  def __init__(self, node=None):
    #self.key = None
    self.node = node

class Node:
  def __init__(self, key):
    self.left = None
    self.val = key
    self.right = None

  def inorder(self, root):
    if root:
      self.inorder(root.left)
      print(root.val, end=" ")
      self.inorder(root.right)

  def maxDepth(self, root):
    if root is None:  return 0

    stack, node_dep = [root], [1]  # init len of node_dep[] set to 1
    max_dep = 0

    while len(stack) > 0:
      node = stack.pop()
      dep = node_dep.pop()
      max_dep = max(max_dep, dep)

      if node.left:
        stack.append(node.left)
        node_dep.append(dep+1)
      if node.right:
        stack.append(node.right)
        node_dep.append(dep+1)
    return max_dep

root = Node(1)  # Must create Node obj before creating Solution obj
sol = Solution(root)

root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.right.left = Node(5)
root.right.right = Node(6)

root.inorder(root)
max_dep = root.maxDepth(root)
print("\nmax_dep:", max_dep)
