/*
=====>>>>>>>注意!!!!! 非常重要!!!!!!

  This question (same as LC 104. Maximum Depth of Binary Tree) has a DIFFERENT def of tree depth from the convential one. It counts number of nodes.  So when a tree has depth of 3,

  Def of tree depth of this question:
  - A binary tree's maximum depth is the "number of nodes" along the longest path from the root node down to the farthest leaf node.

*/

/*
104. Maximum Depth of Binary Tree

ex. 3     ==> 看ln-11 印出的Tree structure
   / \
  9  20
    /  \
   15   7


Tree 解題技巧：
- console.log(root); in LeetCode console to print out the whole tree.
- ex:

 TreeNode {
  val: 3,
  right:
   TreeNode {
     val: 20,
     right: TreeNode { val: 7, right: null, left: null },
     left: TreeNode { val: 15, right: null, left: null } },
  left: TreeNode { val: 9, right: null, left: null } }



切記!!!
- Recursion: Think case-by-case, NOT from start to end of a while program/

注意！！
- The recursive version is trivial, so expect the interviewer to ask for
  the iterative version.
  (但我還是要學 Recurvsive Version, 因為我對遞歸不熟！！！！！)

*/


//========================================================
/***** Recursive Version *****/

// Method #1: DFS (Recursive)
// - LeetCode Premium Sol
// - 思路: ==> See: Approach_1-Explanation.pptx
// -- Intuition: By definition, the maximum depth of a binary tree is the maximum number of steps to reach a leaf node from the root node.
//
// - Time complexity : O(N), we visit each node exactly once.
//
// - Space complexity:
// -- Worst case, the tree is completely unbalanced, e.g. each node has only left child node, the recursion call would occur N times (the height of the tree), therefore the storage to keep the call stack would be O(N).
// -- Best case (the tree is completely balanced), the height of the tree would be log(N). Therefore, the space complexity in this case would be O(log(N))
//
var maxDepth = function(root) {
  if (root == null)   return 0;
  return 1 + Math.max( maxDepth(root.left), maxDepth(root.right) );
}

//========================================================
/***** Iterative Version *****/

// Method #3: BFS using Dequeue (Iterative)
// - https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/1161927/Javascript-or-queue-or-BFS
//
var maxDepth = function(root) {
  if (!root)  returnn 0;
  var queue = [root]
  var levels = 0;  // depth

  // While queue is not empty, we know a new level of tree is enqueued
  while (queue.length) {
    levels++;

    // 注意: using queue.length won't get the right for-loop
    var curLevelLen = queue.length;
    //
    // Take care of the enqueued nodes in this level
    for (var i = 0; i < curLevelLen; i++) {
      var node = queue.shift();  // remove 1st elem of queue[]
      //console.log(node);  // 移除順序 BFS Trav: 3,9,20,15,7 (ex.上方)

      if (node.left)  queue.push(node.left);
      if (node.right)  queue.push(node.right);
    }
  }
  return levels;
}

//========================================================
/***** Iterative Version *****/
// Solution Link #2:

// - https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/34195/Two-Java-Iterative-solution-DFS-and-BFS

// Method #2: DFS using Stack (Iterative) => 注意!! 移除順序: DFS Preorder
//
var maxDepth = function(root) {
  //console.log(root);  // 重要：理解JS BST 結構

  if (root == null)   return 0;

  let max = 0;
  let stack = [];   stack.push(root);
  let vals = [];    vals.push(1);  // init len set to 1

  //while (!stack.length) {   // 不要亂用 "!"
  while (stack.length > 0) {
    let node = stack.pop();
    //console.log(node); // 移除順序 DFS Preorder Trav: 3,20,7,15,9 (ex.上方)
    let val = vals.pop();

    if (val > max)   max = val;  // Update max from ln-84,88 in last iteration
    //max = Math.max(val, max);  // Same as last ln, but QN 認為較不自然

    if (node.left != null) {
      stack.push(node.left);
      vals.push(val + 1);
    }
    if (node.right != null) {
      stack.push(node.right);
      vals.push(val + 1);
    }
  }
  return max;
}


