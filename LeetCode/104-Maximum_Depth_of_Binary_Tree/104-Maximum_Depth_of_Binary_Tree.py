'''
=====>>>>>>>注意!!!!! 非常重要!!!!!!

  This question (same as LC 104. Maximum Depth of Binary Tree) has a DIFFERENT def of tree depth from the convential one. It counts number of nodes.  So when a tree has depth of 3,

  Def of tree depth of this question:
  - A binary tree's maximum depth is the "number of nodes" along the longest path from the root node down to the farthest leaf node.

'''

'''
104. Maximum Depth of Binary Tree

Explanation + Sol Code:
  https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/359949/Python-recursive-and-iterative-solution

  ex. 3     ==> 看ln-11 印出的Tree structure
   / \
  9  20
    /  \
   15   7


Tree 解題技巧：
- console.log(root); in LeetCode console to print out the whole tree.
- ex:

 TreeNode {
  val: 3,
  right:
   TreeNode {
     val: 20,
     right: TreeNode { val: 7, right: null, left: null },
     left: TreeNode { val: 15, right: null, left: null } },
  left: TreeNode { val: 9, right: null, left: null } }



切記!!!
- Recursion: Think case-by-case, NOT from start to end of a while program.

注意！！
- The recursive version is trivial, so expect the interviewer to ask for
  the iterative version.
  (但我還是要學 Recurvsive Version, 因為我對遞歸不熟！！！！！)

'''

# = = = = = = = = = = # = = = = = = = = = = # = = = = = = = = = = #
# Approach #1
#
#   Formula:
#       Depth of current node's subtree = max depth of the two subtrees + 1
#       provided by current node.  Note:  "+ 1" to add curr level
#
#       max(left, right) + 1 #
#
def tree_max_depth_AM(root: Node) -> int:
    def dfs(root):
        # null node adds no depth
        if not root:    return 0

        left = self.maxDepth(root.left)
        right = self.maxDepth(root.right)
        return max(left, right) + 1 # "+ 1" to add curr level
        #
        # The last 3 ln's above are the same as this ln
        # return max(self.maxDepth(root.left), self.maxDepth(root.right)) + 1
    return dfs(root)

#========================================================
# /***** Iterative Version *****/

'''
Approach #3-1: BFS using Deque from Python's Library (Iterative)
- AlgoMonster - Binary Tree Level Order Traversal
  https://algo.monster/problems/binary_tree_level_order_traversal

'''
from collections import deque
def maxDepth(self, root: Optional[TreeNode]) -> int:
  if root is None: return 0

  re = []
  queue = deque([root]) # at least one element in the queue to kick start bfs
  dep = 0

  # While queue is not empty, we know a new level of tree is enqueued
  while len(queue) > 0:
    n = len(queue)  # num of elems enqueued in this new level of tree
    dep+=1

    for _ in range(n):
      node = queue.popleft()  # dequeue each node in the current level

      for child in [node.left, node.right]: # enqueue non-null children
        if child is not None:
          queue.append(child)
    return dep

'''
Approach #3-2: BFS using Deque from Python's Library (Iterative)
- Deque is preferred over a list in the cases where we need quicker append and pop operations from both the ends of the container, as deque provides an O(1) time complexity for append and pop operations as compared to list which provides O(n) time complexity.

- https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/1743346/Python-BFS-Solution-Beginners-Friendly-Soln
'''
from collections import deque
class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if root is None:    return 0
        d = deque([root])
        dep = 0
        # While queue is not empty, we know a new level of tree is enqueued
        while d:
            dep += 1
            print("---->>dep:", dep)

            # Take care of the enqueued nodes in this level
            #
            # Notice!!
            # - range(len(d)) won't be changed b/c it's dequeued next in this
            # for-loop.  But it's easier to use opt-#2 for-loop (ln-144-145).
            for _ in range(len(d)): # opt-#1
                print("range(len(d)):", range(len(d)))
                node = d.popleft()
                print("node.val:", node.val, end=", ")
                if node.left:  d.append(node.left)
                if node.right: d.append(node.right)
        return dep

from collections import deque
class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if root is None:    return 0
        d = deque([root])
        dep = 0
        while d:
            dep += 1

            n = len(d)
            for _ in range(n): # Compare to Approach #3-2 ln-127, opt-#1
                node = d.popleft()
                print("node.val:", node.val, end=", ")
                if node.left:  d.append(node.left)
                if node.right: d.append(node.right)
        return dep

''' ---> QN doesn't seem to like it very much

Approach #3-3: BFS using Deque [using list to implement] (Iterative)
- https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/654160/Python3-DFS-and-BFS-O(N)
'''
class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        if not root: return 0 # edge case
        ans = 0
        queue = [root]
        # While queue is not empty, we know a new level of tree is enqueued
        while queue:
            # Using a diff queue when append a new level of tree nodes.  So
            # the queue var in for-loop below won't be affected.
            newq = []
            #  Take care of the enqueued nodes in this level
            for node in queue:
                if node.left: newq.append(node.left)
                if node.right: newq.append(node.right)
            queue = newq
            ans += 1
        return ans


#========================================================
# /***** Iterative Version *****/

'''
Approach #2-1: DFS using Stack (Iterative)
- https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/736870/python-dfs-using-stack

'''
class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        if not root:  return 0

        stack = [(root,1)]  # init depth set to 1
        max_depth = 0

        # 注意!! Empty list [] is consider Flase in Python.
        while stack:
            node, depth = stack.pop()
            if node.right:  stack.append((node.right, depth+1))
            if node.left:   stack.append((node.left, depth+1))
            max_depth = max(depth, max_depth)
        return max_depth

'''
Approach #2-2: DFS using Stack (Iterative)
- This method eliminates the "if not root" check by checking it in the while loop below.
- https://leetcode.com/problems/maximum-depth-of-binary-tree/discuss/543125/Python-DFS-Stack-Iterative
'''
class Solution:
    def maxDepth(self, root: TreeNode) -> int:

        stack, max_depth = [(root, 1)], 0

        # 注意!! Empty list [] is consider Flase in Python.
        while stack and root:
            node, depth = stack.pop()
            max_depth = max(max_depth, depth)
            if node.right: stack.append((node.right, depth + 1))
            if node.left:  stack.append((node.left, depth + 1))
        return max_depth


