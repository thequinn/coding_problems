/*
105. Construct Binary Tree from Preorder and Inorder Traversal

Concept Explanation Video:
https://www.youtube.com/watch?v=PoBGyrIWisE

Solution Code:
https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/discuss/193566/Simple-Javascript-Solution-with-comments

Help:

Depth First Traversal:
- https://www.geeksforgeeks.org/bfs-vs-dfs-binary-tree/
   - Inorder Traversal (Left-Root-Right)
   - Preorder Traversal (Root-Left-Right)
   -Postorder Traversal (Left-Right-Root)
*/

// this solution manipulates the preorder variable itself and uses the inorder variable to keep track of when to stop the recursive calls
//
var buildTree = function(preorder, inorder) {
  if (!inorder.length) {
    return null;
  }

  var root = new TreeNode(preorder.shift());
  var inorderIndex = inorder.indexOf(root.val);

  // divide the inorder list into left side
  root.left = buildTree(preorder, inorder.slice(0, inorderIndex));
  // divide the inorder list into right side
  root.right = buildTree(preorder, inorder.slice(inorderIndex+1));

  return root;
};

//------------------------------------

preorder = [1, 2, 4, 5, 3, 6]
inorder = [4, 2, 5, 1, 6, 3]

// Answer: [3,9,20,null,null,15,7]
