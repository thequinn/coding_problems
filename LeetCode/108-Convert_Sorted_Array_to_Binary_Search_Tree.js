/*
108. Convert Sorted Array to Binary Search Tree

- 思路:
-- For a sorted array, the left half will be in the left subtree, middle value as the root, right half in the right subtree.
*/

// Approach #1:
// - https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/discuss/35224/Python-optimal-solution
//
// - Time complexity: O(n)
// - Space complexity: O(n), in the case of skewed binary tree.
var sortedArrayToBST = function(nums) {

  function helper(l, r) {
    if (l > r)   return null;

    let m = Math.floor( (l + r) / 2 );
    let node = new TreeNode(nums[m]);

    node.left = helper(l, m - 1);
    node.right = helper(m + 1, r);

    return node;
  }
  
  return helper(0, nums.length - 1)
};

// Approach #2:
// - https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/discuss/146204/Javascript-Accepted-Solution
//
var sortedArrayToBST = function(nums) {
    if (!nums.length)   return null;

    var mid = Math.floor((nums.length)/2);
    var root = new TreeNode(nums[mid]);

    root.left = sortedArrayToBST(nums.slice(0, mid));
    root.right = sortedArrayToBST(nums.slice(mid+1));

    return root;
}

