/*
11. Container With Most Water

*/

/*
Approach 1: Brute Force (in Java)

LC Premium Sol:
- https://leetcode.com/problems/container-with-most-water/solution/
*/
public class Solution {
  public int maxArea(int[] height) {
    int maxarea = 0;
    for (int i = 0; i < height.length; i++)
      for (int j = i + 1; j < height.length; j++)
        maxarea = Math.max(maxarea, Math.min(height[i], height[j]) * (j - i));
    return maxarea;
  }
}

/*
Approach 2: Two Pointers

LC Premium Sol:
- https://leetcode.com/problems/container-with-most-water/solution/

This sol uses range() in for loop to speed up:
- https://leetcode.com/problems/container-with-most-water/discuss/6131/O(N)-7-line-Python-solution-72ms

Intuition:
1. The area formed b/t the lines will always be limited by the height of the shorter line.  When moving one of the two ptrs inwards, we move the ptr w/ shorter height to expect to get a larger height in next iter.
   ex. [6,2,3,4,8,7]
    Say height[0] < height[5], area of (0, 4), (0, 3), (0, 2), (0, 1) will be
    smaller than (0, 5), so no need to try them.
2. The farther the lines, the more will be the area obtained.

Why traverse backward (r ptr moving inward)?
- Because w is the width and you are calculating from the outter-most sides and moving inwards, thus the width gets smaller with every test.

Time complexity : O(n). Single pass.
Space complexity: O(1). Constant space is used.
*/
const maxArea = function(height) {
  let l = 0, r = height.length - 1;  // Two pointers
  let max_area = 0, cur_area = 0

  while (l < r) {
    cur_area = Math.min(height[l], height[r]) * (r - l);
    max_area = Math.max(max_area, cur_area);

    if (height[l] < height[r])  l++;
    else  r--;
  }
  return max_area;
}
