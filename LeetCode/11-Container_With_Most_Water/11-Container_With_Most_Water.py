'''
11. Container With Most Water

'''

# Approach #2: Two Pointer Approach
#   https://leetcode.com/problems/container-with-most-water/solutions/3701708/best-method-c-java-python-beginner-friendly/

from typing import List
class Solution:
  def maxArea(self, h: List[int]) -> int:
    l, r = 0 , len(h)-1
    cur_area, max_area = 0, 0

    # While there there is area to calculate (2 ptrs don't meet)
    while l < r:
      # Update cur area, max area
      cur_area = min(h[l], h[r]) * (r - l)
      max_area = max(cur_area, max_area)
      # If h of l ptr is smaller, move it inwards expecting to get a larger
      # height.  else, move r ptr inwards.
      if h[l] < h[r]: l += 1
      else: r -= 1
    return max_area


h = [2,3,4,5,8,6]
#h = [1,8,6,2,5,4,8,3,7]  # 49
sol = Solution()
res = sol.maxArea(h)
print("res:", res)
