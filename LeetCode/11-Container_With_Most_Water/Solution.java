/*
11. Container With Most Water

*/

// Approach #1: Brute Force
public class Solution {
  public int maxArea(int[] height) {
    int maxarea = 0;
    for (int i = 0; i < height.length; i++)
      for (int j = i + 1; j < height.length; j++)
        maxarea = Math.max(maxarea, Math.min(height[i], height[j]) * (j - i));
    return maxarea;
  }

  public static void main(String args[]) {
    Solution sol = new Solution();
    int[] h = {1,8,6,2,5,4,8,3,7};
    //int[] h = {1,3,4,5,8,6};

    int res = sol.maxArea(h);
    System.out.println("res: " + res);
  }
}


