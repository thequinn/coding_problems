/*
110. Balanced Binary Tree

Height-balanced binary tree:
- a binary tree in which the left and right subtrees of every node differ in height by no more than 1.


Explanation & Solution Code:
- https://leetcode.com/problems/balanced-binary-tree/discuss/35691/The-bottom-up-O(N)-solution-would-be-better

*/

// Approach #1: Too slow, skipped

// Approach #2: Buttom-up Approach (DFS)
//
// - 思路:
// -- Instead of calling depth() explicitly for each child node, we return the height of the current node in DFS recursion.
//
// - Time Complexity: O(N)
//
const dfsHeight = (root) => {
  if (root == null)   return 0;

  let lH = dfsHeight(root.left);  // lH: left Height
  if (lH == -1)    return -1;

  let rH = dfsHeight(root.right);  // rH: right Height
  if (rH == -1)    return -1;

  if (Math.abs(lH-rH) > 1)    return -1;
  return Math.max(lH, rH) + 1;
}

const isBalanced = (root) => {
  return dfsHeight (root) != -1;
};

