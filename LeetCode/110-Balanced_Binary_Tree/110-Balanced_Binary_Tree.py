'''
110. Balanced Binary Tree

https://leetcode.com/problems/balanced-binary-tree/solutions/981648/python-simple-dfs-explained/
'''

class Solution:
class Solution:
    def isBalanced(self, root: Optional[TreeNode]) -> bool:

        def dfs(root):
            # Base Case: => Eliminate the err msg below
            #    dfs(root.left)..."AttributeError: 'NoneType' object has no
            #    attribute 
            #    'left'".
            # - Reason: It's calling an empty tree, dfs(None) 
            # - Moving the base case to *** Mark-A *** won't help
            if not root: return 0

            l, r = dfs(root.left), dfs(root.right)
            if abs(l-r) > 1:    self.Balance = False
            return max(l, r) + 1

        # *** Mark-A ***
        self.Balance = True
        dfs(root)
        return self.Balance
