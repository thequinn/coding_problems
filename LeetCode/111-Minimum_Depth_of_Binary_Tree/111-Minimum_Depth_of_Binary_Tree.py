'''
111. Minimum Depth of Binary Tree

'''

'''
Below are 3 ways to fix the runtime error here:

UnboundLocalError: cannot access local variable 'min_dep' where it is not associated with a value
                  ^^^^^^^
                      min_dep = min(min_dep, l_dep, r_dep)




'''


'''
1. Use a nonlocal variable
- If you want to use the min_dep variable defined in the outer scope (minDepth), you can declare it as nonlocal inside the dfs function:




問題....未解決....???...???...???
- The code below doesn't solve the ex2 test case in LC description

....> Copy and paste the below sentences to ChatGPT to learn more....

The problem I' trying to solve in from Leetcode 111. Minimum Depth of Binary Tree.  I ran a test case, root =
[2,null,3,null,4,null,5,null,6], using the suggested code in 1. you provided above.  However, the test case returns an output of 1, but the expected output is 5.  What's going on?



'''
class Solution:
    def minDepth(self, root: Optional[TreeNode]) -> int:

        def dfs(node, dep) -> int:

            # To use the min_dep var defined in the outer scope, declare it
            # as nonlocal inside dfs()
            nonlocal min_dep

            if not node:    return dep

            l_dep = dfs(node.left, dep+1)
            r_dep = dfs(node.right, dep+1)
            min_dep = min(min_dep, l_dep, r_dep)
            return min_dep

        min_dep = float('inf')  # Start with infinity to find the minimum
        dfs(root, 0)
        return min_dep




'''
2. Avoid using min_dep in the recursive function
- You can rewrite your logic to compute the minimum depth without relying on an external variable like min_dep. Instead, directly return the minimum value from the dfs function:
'''
class Solution:
    def minDepth(self, root: Optional[TreeNode]) -> int:

        def dfs(node, dep) -> int:
            if not node:
                return dep
            l_dep = dfs(node.left, dep+1) if node.left else float('inf')
            r_dep = dfs(node.right, dep+1) if node.right else float('inf')
            return min(l_dep, r_dep)

        return dfs(root, 0) if root else 0



'''
3. Initialize min_dep directly in the recursive call
- Instead of maintaining a min_dep variable outside the recursive function, you can handle all calculations within the dfs function:
'''
class Solution:
    def minDepth(self, root: Optional[TreeNode]) -> int:

        def dfs(node) -> int:
            if not node:
                return 0
            if not node.left and not node.right:
                return 1
            left = dfs(node.left) if node.left else float('inf')
            right = dfs(node.right) if node.right else float('inf')
            return 1 + min(left, right)

        return dfs(root)
