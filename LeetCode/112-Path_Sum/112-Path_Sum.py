'''
112. Path Sum

'''

'''
Approach #1-1: DFS Recursive => Memorize this template!!
=> This template is used by QN's 113 Path Sum II Approach #1

- Watch NeetCode's video to learn the concept"
    https://www.youtube.com/watch?v=LSKQyOz_P8I


思路:

Notice! 
- a helper counter, curSum=0, is created to sum up node vals in a path.  When a leaf is reach, compare it with targetSum requested by the problem desc.

(a) Base Case:
- If tree NO exist

(b) Recursive Case:
- If tree exists
    1. If tree is a leaf
        - Check if cur sum equals to target sum
    2. If tree is NOT a leaf
        - Recursivly go to L & R childs

'''
class Solution:
    def hasPathSum(self, root: Optional[TreeNode], targetSum: int) -> List[List[int]]:
        
        def dfs(node, curSum):
            # => Base Case
            if not node:    return False

            # => Recursive Cases
            curSum += node.val
            # If cur node is a leaf
            if not node.left and not node.right:
                return curSum == targetSum
            # If cur node is NOT a leaf
            return dfs(node.left, curSum) or dfs(node.right, curSum)
        
        return dfs(root, 0)

'''
Approach #1-2: DFS Recursive => Use Approach #1-1 bc it can be expanded 
                                to 113. Path Sum II

Explanation + Sol Code:
    https://leetcode.com/problems/path-sum/solutions/534122/python-bfs-dfs-recursive-dfs-iterative-solution/
'''
class Solution:
    def hasPathSum(self, root: TreeNode, targetSum: int) -> bool:
        # => Base Case
        if not root:    return False

        # => Recursive Cases
        # If cur node is a leaf
        if not root.left and not root.right:
            return root.val == targetSum
        # If cur node is NOT a leaf
        return self.hasPathSum(root.left, targetSum - root.val) or \
                self.hasPathSum(root.right, targetSum - root.val)

'''
Approach #2: DFS Iterative =>未...
- See link in Approach #1-2

'''
def hasPathSum(self, root: TreeNode, sum: int) -> bool:
	stack = [(root, sum)]
	while stack:
		node, curr_sum = stack.pop()
	if not node:
		continue
	if not node.left and not node.right and curr_sum == node.val:
		return True
	stack.append((node.left, curr_sum - node.val))
	stack.append((node.right, curr_sum - node.val))
	return False



'''
Approach #3: BFS =>未...
- See link in Approach #1-2

'''
from collections import deque

def hasPathSum(self, root: TreeNode, sum: int) -> bool:
	deq = deque()
	deq.append((root, sum))
		while deq:
			node, curr_sum = deq.popleft()
			if not node:
				continue
			if not node.left and not node.right and curr_sum == node.val:
				return True
			deq.append((node.left, curr_sum - node.val))
			deq.append((node.right, curr_sum - node.val))
	return False

