'''
113-Path_Sum_II

'''


'''
Approach #1: DFS Recursive
    https://leetcode.com/problems/path-sum-ii/solutions/690275/python-simple-recursive-solution/
'''
class Solution:
    def pathSum(self, root: TreeNode, targetSum: int) -> List[List[int]]:
        
        def dfs(node, path, curSum):
            print("\npath:", path)

            # => Base Case: If Tree NOT Exist
            if not node:   return

            # => Recursive Cases: If Tree Exists
            curSum += node.val
            # If cur node is a leaf
            if (curSum == targetSum and not node.left and not node.right):
                result.append(path + [node.val])
                return
            # If cur node is NOT a leaf
            #
            # Combine lists into 1 list: extend(), +, +=
            # - https://note.nkmk.me/en/python-list-append-extend-insert/#combine-lists-extend
            next_path = path + [node.val]
            print("next_path:", next_path)
            dfs(node.left, next_path, curSum)
            dfs(node.right, next_path, curSum)

        result = []
        dfs(root, [], 0)
        return result

'''
Approach #2: BFS
    https://leetcode.com/problems/path-sum-ii/solutions/2615820/clean-fast-python3-bfs/
'''
class Solution:        
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> List[List[int]]:
        if root is None:
            return []
        q, paths = deque([(root, targetSum, [])]), []
        
        while q:
            cur, target, path = q.pop()  
            if not (cur.left or cur.right) and cur.val == target:
                paths.append(path + [cur.val])
            else:
                if cur.left:
                    q.appendleft((cur.left, target - cur.val, path + [cur.val]))
                if cur.right:
                    q.appendleft((cur.right, target - cur.val, path + [cur.val]))
                                 
        return paths
