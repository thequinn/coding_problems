/*
116. Populating Next Right Pointers in Each Node

You are given a perfect binary tree where all leaves are on the same level, and every parent has two children. The binary tree has the following definition:

struct Node {
  int val;
  Node *left;
  Node *right;
  Node *next;
}

Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.

Initially, all next pointers are set to NULL.


Follow up:

    You may only use constant extra space.
    Recursive approach is fine, you may assume implicit stack space does not count as extra space for this problem.



JavaScript BFS & DFS Solution:
https://leetcode.com/problems/populating-next-right-pointers-in-each-node/discuss/420632/JavaScript-BFS-and-DFS-Solution
*/


// The Idea - DFS
// - Pre-order traversal (It's a type of DFS)
// - Set child nodes arrangement before recursion
//
// Time complexity: O(n)
// Space complexity: O(1)
//
var connect = function(root) {
  // Base case:
  // B/c a perfect binary tree is given, if root.left is null, return the tree
  if (root == null || root.left == null)  return root;

  // ptrs of .next setup:
  root.left.next = root.right;
  root.right.next = root.next ? root.next.left : null;

  // Recursive case:
  connect(root.left);
  connect(root.right);

  return root;
}


// Note: This answer is a bit hard to understand, and it's O(n^2)
//
// The Idea - BFS
// - BFS using queue
// - As we are shifing node, connect it to the next in queue
//
// >Help:
// BFS using queue:
// - https://hackernoon.com/breadth-first-search-in-javascript-e655cd824fa4
// s1. Take the 1st node out of the queue, see if it matches your search item.
// s2. Add all of the node’s children to the temporary queue.
//
var connectBFS = function(root) {
  if (root == null)   return root;

  let queue = [root];
  while(queue.length != 0) {
    let next = [];

    while(queue.length != 0) {
      let node = queue.shift();

      //console.log("queue[0]:", queue[0])
      node.next = queue[0] || null;

      if (node.left != null) {
        next.push(node.left);
        next.push(node.right);
      }
    }

    queue = next;
  }

  return root;
};


