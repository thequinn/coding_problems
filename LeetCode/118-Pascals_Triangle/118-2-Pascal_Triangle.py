'''
118. Pascal's Triangle


4 Appraoches Explained:
https://leetcode.com/problems/pascals-triangle/solutions/5986694/4-approaches-explained-easy/
  1. Iterative Approach
  2. Recursive Approach
  3. Recursive Approach (Memoization)
  4. Dynamic Programming Approach
'''

'''
Iterative Approach

Time complexity: O(n^2)
Space complexity: O(n^2)
'''
class Solution:
    def generate(self, nRows: int) -> List[List[int]]:
        # Base case: if n is 0, return the first row of Pascal's triangle
        if nRows == 0:
            return [[1]]

        res = [[1]]

        # Generate each row of Pascal's triangle
        for i in range(2, nRows + 1):
            cur_row = [1] + [0] * (i - 2) + [1]

            # Skip first and last elem, fill in the rest of the row
            for j in range(1, i - 1):
                cur_row[j] = res[-1][j] + res[-1][j - 1]

            # Append the new row to the triangle
            res.append(cur_row)

        return res


'''
2. Recursive Approach

Time complexity:  O(n^2)
Space complexity: O(n)
'''
class Solution:
    def generate(self, n: int) -> List[List[int]]:
        # Base case: if n is 1, return the first row of Pascal's triangle
        if n == 1:  return [[1]]

        # Recursive case: generate Pascal's triangle for n-1
        prev_tri = self.generate(n-1)

        # Get the last row from the previous triangle
        last_row = prev_tri[-1]

        new_row = [1] * n
        # Fill all elems, except the 1st and last
        for i in range(1, n-1):
            new_row[i] = last_row[i-1] + last_row[i]

        prev_tri.append(new_row)
        return prev_tri

'''
3. Recursive Approach - Memoization

Time complexity:  O(n^2)
Space complexity: O(n)
'''
class Solution():
    # Create cache as an extra argument
    def generate(self, n: int, memo={}):
        if n in memo: return memo[n]
        if n == 1:  return [[1]]

        prev_tri = self.generate(n-1)
        last_row = prev_tri[-1]
        new_row = [1] * n
        for i in range(1, n-1):
            new_row[i] = last_row[i-1] + last_row[i]
        prev_tri.append(new_row)
        print("prev_tri:", prev_tri)
        memo[n] = prev_tri  # ...未... don't understand...
        return prev_tri


'''
4. Dynamic Programming Approach

Time complexity:  O(n^2)
Space complexity: O(n^2)
'''
class Solution:
    def generate(self, n):
        # Base case: if n is 1, return the first row of Pascal's triangle
        if n == 1:  return [[1]]

        # Initialize the triangle with the first row
        tri = [[1]]
        # Generate the next n-1 rows
        for _ in range(n-1):
            prev_row = tri[-1]
            new_row = [1] * (len(prev_row) + 1)
            for i in range(1, len(new_row) - 1):
                new_row[i] = prev_row[i-1] + prev_row[i]
            tri.append(new_row)
        return tri
