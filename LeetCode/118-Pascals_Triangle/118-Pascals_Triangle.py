'''
118. Pascal's Triangle

'''

'''
Approach 1-1: DP w/ 1D Array => by QN
              => Qn thought it's brute force 而不屑一顧, turned out to be DP!!

思路:

(a) High Level:
- Initialize a 1D array to store the current row.
- Iterate through numRows and update the array for each row.

(b) Low Level:
Pre-set 0-th row [1] in res[]
Iterate i=[1~numRows) times
    Append a zero on two ends of the previous list
    Iterate j=[0~i] times to calc cur_row[]
        Calc cur num from pre_row[]
    Append cur_row[] to res[]
    Reset cur_row[]
'''
class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        res = [[1]]
        pr_row, cur_row = [], []

        for i in range(1, numRows):

            # cmt-1: Using copy() won't modify res[]
            pre_row = res[i-1].copy()
            #pre_row = res[i-1] # WRONG! Modified res[[...]]

            # cmt-2:  Add 0 to two ends of res[-1]
            pre_row.insert(0, 0)
            pre_row.append(0)

            for j in range(i+1):
                cur_row.append(pre_row[j] + pre_row[j+1])
            res.append(cur_row)

            cur_row = []
            #cur_row.clean() # WRONG! Modified res[[...]]

        return res

'''
Approach 1-2: DP w/ 1D Array => Same, diff Python code syntax

NeetCode:
    https://www.youtube.com/watch?v=nPVEaB3AjUM&t=17s
'''
class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        res = [[1]]

        for i in range(1, numRows):

            # cmt-1: Assigning to tmp[] won't modify res[]
            # cmt-2: Add 0 to two ends of res[-1]
            pre_row = [0] + res[-1] + [0]

            cur_row = []
            for j in range(i+1):
                cur_row.append(pre_row[j] + pre_row[j+1])
            res.append(cur_row)
        return res


'''
Appraoch #2: Recursion

https://leetcode.com/problems/pascals-triangle/solutions/5420356/python-recursive-approach-that-beats-93/

思路:
- Define a helper function that creates pascals triangle recursively. 
- Base Case: 
    if nRows <= 1, the triangle is [1], append [1] to res[].
- Recursive Case:
    Recursivly calling helper() w/ nRows-1. 
    Define cur_row list: assign all elements with 0 except 1st and last elems since Every row of a Pascal's Triangle has both ends w/ a 1. Next, calc the rest of cur_row by summing res[-1] elems, which is the pre_row.


Note:
- The recursive call in ln-104, helper(nRows-1, res), is similar to a DFS call.  It goes all the way down to top row (the pt of the triangle), and then calc down to bottom row as each recursive call stack returns to its pre call stack:
           
           Top -> Bottom of Triangle = row:1 -> row:5
'''
class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        
        def helper(nRows, res):
            # - - - Base Case - - -
            if nRows == 1:
                res.append([1])
                return
            
            # - - -Recursive Case - - -
            helper(nRows-1, res)

            cur_row = [1] + [0] * (nRows-2) + [1]
            
            # Skip first and last element
            for x in range(1, len(cur_row)-1):
                cur_row[x] = res[-1][x-1] + res[-1][x]
            res.append(cur_row)

        res = []
        helper(numRows, res)
        return res



