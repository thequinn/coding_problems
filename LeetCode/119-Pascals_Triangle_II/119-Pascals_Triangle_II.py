'''
119. Pascal's Triangle II

'''


'''
Appraoch #1: Solved by QN
思路:
- Start cur row with 1 on both ends
- wCalc all the elems in between based on the prev row
'''
class Solution:
    def getRow(self, rowIndex: int) -> List[int]:
        if rowIndex == 0:  return [1]
        if rowIndex == 1:  return [1, 1]

        pre_row = [1, 1]
        cur_row = []
        for i in range(2, rowIndex+1):
            cur_row = [1] + (i-1) * [0] + [1]
            for j in range(1, i):
                cur_row[j] = pre_row[j-1] + pre_row[j]
            pre_row = cur_row
        return cur_row

