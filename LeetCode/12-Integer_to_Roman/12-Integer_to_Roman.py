'''
12. Integer to Roman
'''

'''
Approach #1

NeetCode: https://www.youtube.com/watch?v=ohBNdSJyLh8
'''



'''
Approach #2 => Same appraoch as Approach #1, code is a bit diff

https://leetcode.com/problems/integer-to-roman/solutions/1102775/js-python-java-c-simple-solution-w-explanation/
'''
val = [1000,900,500,400,100,90,50,40,10,9,5,4,1]
rom = ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"]

class Solution:
    def intToRoman(self, N: int) -> str:
        ans = ""
        for i in range(13):
            while N >= val[i]:
                ans += rom[i]
                print("\nN:", N, ", val[", i, "]:", val[i])
                print("rom[", i, "]:", rom[i], ", ans:", ans)
                N -= val[i]
        return ans
