/*
121. Best Time to Buy and Sell Stock

LeetCode Std Solution:
  https://leetcode.com/problems/best-time-to-buy-and-sell-stock/solution/
*/

// Method #1:  Brute Force
//
// - Time complexity : O(n^2)
// -- Loop runs n*(n−1)/2 times
//
// - Space complexity : O(1)
// -- Only two variables - maxprofit and profit
//
const maxProfit = function(prices) {
  let maxprofit = 0;

  for (let i = 0; i < prices.length - 1; i++) {
    // j starts from  i's next elem
    for (let j = i + 1; j < prices.length; j++) {
      let profit = prices[j] - prices[i];

      if (profit > maxprofit)
        maxprofit = profit;
    }
  }

  return maxprofit;
}

/* Note:
    Method #2 One Pass is really the same as Method #3 Kadane's Algorithm once you understand both and have the foundation of DP memoization which uses sub-problems to solve the main problem.
*/

// Method #2-1: One Pass
//
// 思路:
// - The points of interest are the peaks and valleys in the given graph.
// - We need to find the largest peak following the smallest valley.
// -- minprice : smallest valley
// -- maxprofit: max diff b/t selling price and minprice obtained so far.
//
const maxProfit = function(prices) {

  //let minprice = Number.MAX_VALUE;
  let minprice = prices[0];  // same as last ln

  let maxprofit = 0;

  for (let i = 0; i < prices.length; i++) {
    // If cur price < min price, a lower minprice is found
    // If cur price >= min price, a peak is found. Check if it's the highest
    //   peak that brings the max profit

    if (prices[i] < minprice)
      minprice = prices[i];

    /*
    else
      if (prices[i] - minprice > maxprofit)
         maxprofit = prices[i] - minprice;
    */
    // Same as the last 3 lns above
    else if (prices[i] - minprice > maxprofit)
      maxprofit = prices[i] - minprice;
  }

  return maxprofit;
}

// Method #2-1: One Pass -> The if-else statements in Method #2-1 can be
//                          simplified to this version
def maxProfit(self, prices: List[int]) -> int:
        minprice = prices[0]
        maxprofit = 0
        for i, e in enumerate(prices):
            minprice = min(e, minprice)
            maxprofit = max(e - minprice, maxprofit)
        return maxprofit


/*
Method #3: Kadane's Algorithm (Greedy Algorithm)
==>  LeetCode 53. Max Subarray to learn Kadane's Algorithm

- https://leetcode.com/problems/best-time-to-buy-and-sell-stock/discuss/39038/Kadane's-Algorithm-Since-no-one-has-mentioned-about-this-so-far-%3A)-(In-case-if-interviewer-twists-the-input)

- 思路：
-- 1. The logic to solve this problem is same as "max subarray problem" using
   Kadane's Algorithm.

  ***** 注意!!! *****
  All the straight forward solution should work, but if the interviewer twists
  the question slightly by giving "the differences of prices" from its prev day,
  you might end up being confused.
  Ex: for arr1 = {1, 7, 4, 11}, if he gives arr = {0, 6, -3, 7}
  |
  --> Comparing 53. Maximum Subarray to 121. Best Time to Buy and Sell Stock,
  we know 53. gives "the differences of prices" while 121. gives "the prices."

  (53. Maximum Subarray) given "the differences of prices"
  - given arr2 = {0, 6, -3, 7}, we use maxCur += arr2[i]

  (121. Best Time to Buy and Sell Stock) given "the prices"
  - given arr1 = {1, 7, 4, 11}, we use maxCur += arr1[i] - arr1[i-1], meaning
  it raises by 6 from day 1 to day 2

-- 2.
  Here, the logic is to calc the "diff" (maxCur += prices[i] - prices[i-1]) of
  the original array, and find a contiguous subarray giving maximum profit. If
  the difference falls below 0, reset it to zero.

  By reseting maxCur to 0, we have found a point i where the price[i] is lower
  than the time we bought, and that we should then try to buy at point i to see
  if we can achieve a bigger gain. Because maxCur is recording the difference,
  the difference between price[i] and itself should be 0.
*/
const maxProfit = (prices) => {
  let maxCur = 0;   // max val at cur index
  let maxSoFar = 0; // max val found so far

  // Starts from index=1
  for(let i = 1; i < prices.length; i++) {
    // Why set 1st param to 0? See 2. in 思路 above.
    maxCur = Math.max(0, maxCur += prices[i] - prices[i-1]);
    maxSoFar = Math.max(maxCur, maxSoFar);
  }

  return maxSoFar;
}

console.log( maxProfit([7,1,5,3,6,4]) );
