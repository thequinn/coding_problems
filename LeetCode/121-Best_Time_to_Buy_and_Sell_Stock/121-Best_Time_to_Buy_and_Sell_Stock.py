'''
121. Best Time to Buy and Sell Stock
'''

'''
Method #0:  Brute Force (.js)

Time complexity : O(n^2)
- Loop runs n*(n−1)/2 times

Space complexity : O(1)
- Only two variables - maxprofit and profit
'''

'''
const maxProfit = function(prices) {
  let maxprofit = 0;

  for (let i = 0; i < prices.length - 1; i++) {
    // j starts from  i's next elem
    for (let j = i + 1; j < prices.length; j++) {
      let profit = prices[j] - prices[i];

      if (profit > maxprofit)
        maxprofit = profit;
    }
  }

  return maxprofit;
}
'''



'''
Method #1: Two Pointers (Slow & Fast Ptrs)

https://www.youtube.com/watch?v=1pkOgXD63yU

思路:

Slow & Fast Ptrs:
- Slow ptr tracks the lowest price
- Fast ptr tracks the cur price

Iter over the price list using fast ptr
- If buy price is "lower than" cur price, calc profit and compare it w/ max profit
- If buy price is "higher than or equal to" cur price, it means we found a new buy price lower than the last buy price.  Set the new buy price to cur price.
'''
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        slow, fast = 0, 1
        maxP = 0  # maxProfit

        while fast < len(prices):
            if prices[slow] < prices[fast]:
                maxP = max(maxP, prices[fast] - prices[slow])
            else:
                slow = fast
            fast += 1
        return maxP

    

'''
Method #2: One Pass ==> Same as Method #2 in .js version
'''
class Solution:
    def maxProfit(self, prices: List[int]) -> int:

        # Don't set minPrice to 0, or it will never be the lowest val in prices[]
        minPrice = prices[0]
        maxProfit = 0
        
        for price in prices[1:]:
            maxProfit = max(maxProfit, price - minPrice)
            minPrice = min(minPrice, price)
            
        return maxProfit

'''
Method #3: Kadane's Algorithm (Greedy Algorithm)
==>  LeetCode 53. Max Subarray to learn Kadane's Algorithm

- https://leetcode.com/problems/best-time-to-buy-and-sell-stock/discuss/39038/Kadane's-Algorithm-Since-no-one-has-mentioned-about-this-so-far-%3A)-(In-case-if-interviewer-twists-the-input)

- 思路：
-- 1. The logic to solve this problem is same as "max subarray problem" using
   Kadane's Algorithm.

  ***** 注意!!! *****
  All the straight forward solution should work, but if the interviewer twists
  the question slightly by giving "the differences of prices" from its prev day,
  you might end up being confused.
  Ex: for arr1 = {1, 7, 4, 11}, if he gives arr = {0, 6, -3, 7}
  |
  --> Comparing 53. Maximum Subarray to 121. Best Time to Buy and Sell Stock,
  we know 53. gives "the differences of prices" while 121. gives "the prices."

  (53. Maximum Subarray) given "the differences of prices"
  - given arr2 = {0, 6, -3, 7}, we use maxCur += arr2[i]

  (121. Best Time to Buy and Sell Stock) given "the prices"
  - given arr1 = {1, 7, 4, 11}, we use maxCur += arr1[i] - arr1[i-1], meaning
  it raises by 6 from day 1 to day 2

-- 2.
  Here, the logic is to calc the "diff" (maxCur += prices[i] - prices[i-1]) of
  the original array, and find a contiguous subarray giving maximum profit. If
  the difference falls below 0, reset it to zero.

  By reseting maxCur to 0, we have found a point i where the price[i] is lower
  than the time we bought, and that we should then try to buy at point i to see
  if we can achieve a bigger gain. Because maxCur is recording the difference,
  the difference between price[i] and itself should be 0.
'''

'''
Method #3: Kadane's Algorithm (Greedy Algorithm)
'''
class Solution:
  def maxProfit(self, prices: List[int]) -> int:
    maxLocal = 0
    maxGlobal = 0
    for i in range(1, len(prices)):
      maxLocal = max(0, maxLocal + (prices[i] - prices[i-1]))
      maxGlobal = max(maxLocal, maxGlobal)
    return maxGlobal
