/*
122. Best Time to Buy and Sell Stock II
- LeetCode Std Sol:
-- https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/solution/
*/

/*
Appraoch #2: Peak Valley Approach => QN: Complicates the problem, Skip!!

- 思路: ==> 畫圖較易理解
-- The key point is we need to consider every peak immediately following a valley to maximize the profit.

QN: After tracing the code myself w/ the 座標圖 I drew, I realized this could use a Greedy Approach.  It's b/c in each transaction, we only care about findiny a local valley and next a local peak.

- Time Complexity : O(n) - Single Pass
- Space Complexity: O(1) - Constant space required
*/
let maxProfit = function(prices) {
  let i = 0, maxProfit = 0;
  let valley = prices[0], peak = prices[0];

  // i < "prices.length - 1": protect from getting index out of bound
  while (i < prices.length - 1) {
    // find valley
    while (i < prices.length - 1 && prices[i] >= prices[i + 1])
      i++;
    valley = prices[i];

    // find peak
    while (i < prices.length - 1 && prices[i] <= prices[i + 1])
      i++;
    peak = prices[i];

    maxProfit += peak - valley;
  }

  return maxProfit;
}

/*
Approach #3: One Pass (Mod of Approach #2)
- 思路:
-- Keep on adding the diff b/t the consecutive numbers of the array if the 2nd
   number is larger than the 1st one.
*/
let maxProfit = function(prices) {
  let maxProfit = 0;
  for (let i = 1; i < prices.length; i++) {
    if (prices[i] > prices[i - 1])
      maxProfit += prices[i] - prices[i - 1];
  }
  return maxProfit;
}

/*
Approach #4: DP

(a) https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/discuss/803206/Python-O(n)-by-DP-w-Visualization
- See: state machine diagram

(b) https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/discuss/564729/Java-Simple-Code-DP
- See: saikiranchalla's comment for explanation
*/

var maxProfit = function(prices) {
    // Impossible to sell stock on 1st day, set -infinity for curHold
    let [curHold, curNotHold] = [-Infinity, 0];

    for(const stockPrice of prices){
        let [prevHold, prevNotHold] = [curHold, curNotHold];

        // either keep hold, or buy in stock today at stock price
        curHold = Math.max(prevHold, prevNotHold - stockPrice );

        // either keep not-hold, or sell out stock today at stock price
        curNotHold = Math.max(prevNotHold, prevHold + stockPrice );
    }

    // Max profit must come from notHold state finally.
    return curNotHold;
};

let prices = [7,1,5,3,6,4];
let result = maxProfit(prices);
console.log("result:", result);

