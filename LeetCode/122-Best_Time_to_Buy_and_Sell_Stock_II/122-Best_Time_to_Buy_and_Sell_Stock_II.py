

'''
Approach #3: => Greedy Algorithm 

See Method #2 in this link:
  https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/discuss/803206/Python-O(n)-by-DP-w-Visualization


思路:
- Keep on adding the diff b/t the consecutive numbers of the array if the 2nd
   number is larger than the 1st one.


思路: => See the graph to visualize from the link above
    
- Max profit with long position ,'做多' in chinese, is met by collecting all price gain in the stock price sequence.

- Take care that holding multiple position at the same time is NOT allowed by description.

    Max profit w/ single long position on every gain
        = Sum of price gains (sell high - buy low)


Time Complexity: O(n), for single level for loop
Space Complexity: O(1), for array storage sapce
'''
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        maxP = 0
        for i in range(1, len(prices)):
            #if prices[i] > prices[i-1]:
            #    maxP += prices[i] - prices[i-1]
            # Same as: 
            maxP += max( (prices[i] - prices[i-1]), 0 )
        return maxP

'''
Approach #4-(a): Botoom-up DP + Iteration

(a) (必看!) The State machine diagram and Pseudo Code for 思路:
  https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/discuss/803206/Python-O(n)-by-DP-w-Visualization

Time Complexity: O(n), for single level for loop
Space Complexity: O(1), for fixed size of temporary variables
'''
class Solution:
  def maxProfit(self, prices: List[int]) -> int:

    # Can't sell stock before buy on first day, set -infinity as init value 
    # for cur_hold
    cur_hold     = -float('inf')
    cur_not_hold = 0

    for stock_price in prices:
      prev_hold     = cur_hold
      prev_not_hold = cur_not_hold

      # either keep hold, or buy in stock today at stock price
      cur_hold = max( prev_hold, prev_not_hold - stock_price )

      # either keep not-hold, or sell out stock today at stock price
      cur_not_hold = max( prev_not_hold, prev_hold + stock_price )

    # maximum profit must be in not-hold state
    return cur_not_hold
    #
    # Not sure why the Python version sol use the enxt ln
    #return cur_not_hold if prices else 0

'''
Approach #4-(b): Botoom-up DP + Iteration

(a) This sol shows original Bottom-up DP -> Optimized version in (b)
 @3:41-@8:40  https://youtu.be/USEFjOtuyA4?t=221
(b) See saikiranchalla's comment for explanation
  https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/discuss/564729/Java-Simple-Code-DP

思路:
- The action we can do on ith day is either buy (if last action is sell), or sell (if last action is buy), or do nothing (not buy, not sell).

Details:

At every iteration, you've these choices:
    
    int curBuy = Math.max(lastBuy, lastSold - A[i]);
    int curSold = Math.max(lastSold, lastBuy + A[i]);

1) Don't do anything = lastBuy or lastSold (because you're keeping there is no change in the amount of money you have)
2) Sell them for a profit = lastBuy + current (because you're selling the amount of money you've increases)
3) Buy new stock = lastSold - current (you need money to buy stock so you sell your profit to buy new stock)
'''
class Solution:
    def maxProfit(self, A: List[int]) -> int:                   
        lastBuy = -A[0]
        lastSold = 0
                                
        for i in range(len(A)):
            curBuy = max(lastBuy, lastSold - A[i])
            curSold = max(lastSold, lastBuy + A[i])
            lastBuy = curBuy
            lastSold = curSold
        
        return lastSold


'''
Approach #5: Top Down DP + Recursion
=>未: he error below exists: ...or Review Top Down DP -> look for better sol
   TypeError: Solution.maxProfit() takes 2 positional arguments but 3 were given
            

(必看!) The State machine diagram and Pseudo Code for 思路:
  https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/discuss/803206/Python-O(n)-by-DP-w-Visualization

Time Complexity: O(n), for single level for loop
Space Complexity: O(n), for 1D DP recursion call depth
'''
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        
        @cache
        def trade(day_d):
            # Base Case:
            if day_d == 0:
                # Hold on day_#0 = buy stock at the price of day_#0
                # Not-hold on day_#0 = doing nothing on day_#0
                return -prices[day_d], 0
            
            # Recursive Case:
            print("ln-14, day_d:", day_d)
            prev_hold, prev_not_hold = trade(day_d-1)
            
            hold = max(prev_hold, prev_not_hold - prices[day_d] )
            not_hold = max(prev_not_hold, prev_hold + prices[day_d] )
            
            return hold, not_hold
        
        # --------------------------------------------------
        last_day= len(prices)-1
        # Max profit must come from not_hold state on last day
        return trade(last_day)[1]
