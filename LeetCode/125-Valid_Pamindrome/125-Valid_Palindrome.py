# Approach #1: Two Ptrs - Split str to half
def isPalindrome(self, s: str) -> bool:
    # Remove non-alphanumeric characters from a str
    s = re.sub(r'[^a-zA-Z0-9]', '', s)
    s = s.lower()
    print("s after removing non-alphanumeric chars:", s)

    l, r = 0, len(s) - 1
    while l <= r:
        if s[l] != s[r]:
            return False
        l += 1
        r -= 1
    return True

# Approach #2: the Original vs the Reversed
def isPalindrome(self, s: str) -> bool:
    # Remove non-alphanumeric characters from a str
    s = re.sub(r'[^a-zA-Z0-9]', '', s)
    s = s.lower()
    print("s after removing non-alphanumeric chars:", s)

    # s[::-1]: Reverse a str in Python
    return s == s[::-1]

