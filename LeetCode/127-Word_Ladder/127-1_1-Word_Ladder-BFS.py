'''
127. Word Ladder

Key Words!
- find the length of shortest transformation sequence
-- Anytime you see "find the shortest sequence", you should immeidately think
   to use some sort of “shortest path algorithm”, such as BFS.
'''


'''
Approach: Breadth First Search

Explanation + Sol Code:
- 1. https://zxi.mytechroad.com/blog/searching/127-word-ladder/ -> 很好
- 2. dwhelper/LeetCode/

Sol Code:
- Python:
-- https://leetcode.com/problems/word-ladder/discuss/40729/Compact-Python-solution

'''

from collections import deque
class Solution(object):

  def ladderLength(self, beginWord, endWord, wordList):
    wordList = set(wordList)

    # If not import deque from collections on the top
    #queue = collections.deque([ [beginWord, 1] ])
    #
    queue = deque([ [beginWord, 1] ])  # Count beginWord for path length of 1

    while queue:
      word, length = queue.popleft()  # 注意! NOT pop"L"eft()
      print(f'\nword: {word}, length: {length}')

      if word == endWord:
        print("ln-93, length:", length)
        return length

      for i in range(len(word)):
        for c in 'abcdefghijklmnopqrstuvwxyz':
          next_word = word[:i] + c + word[i+1:]
          print("ln-99, next_word:", next_word)

          if next_word in wordList:
            wordList.remove(next_word)
            print("wordList:", wordList)
            queue.append([next_word, length + 1])
            print("queue:", queue)

    return 0  # End of: "while queue:"


#-----Test Case-----
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]
# One shortest transformation: "hit" -> "hot" -> "dot" -> "dog" -> "cog"

sol = Solution()
res = sol.ladderLength(beginWord, endWord, wordList)
print("res:", res)    # 5

