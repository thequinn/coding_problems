'''
127. Word Ladder

Key Words!
- find the length of shortest transformation sequence
-- Anytime you see "find the shortest sequence", you should immeidately think
   to use some sort of “shortest path algorithm”, such as BFS.
'''


'''
Approach: Breadth First Search  --> 用比較進階的Python Library, 以後再學!

Explanation
- Video: https://www.youtube.com/watch?v=fSNisgiV_d4
  ==> explains differences b/t Approach #1-1 anf #1-2
- Text:
-- 1. LC Std Sol: https://leetcode.com/problems/word-ladder/solution/
-- 2. https://leetcode.com/problems/word-ladder/discuss/346920/Python3-Breadth-first-search

'''
from collections import defaultdict
from collections import deque
class Solution(object):

  def ladderLength(self, beginWord, endWord, wordList):

    if endWord not in wordList or not endWord or not beginWord or not wordList:
      return 0

    # Since all words are of same length.
    L = len(beginWord)

    # Dictionary to hold combination of words that can be formed, by changing
    # one letter at a time.
    # - ex. 'hot': {'*ot': ['hot'], 'h*t': ['hot'], 'ho*': ['hot']}
    '''
    defaultdict(list)
    - You passed "list" as param to defaultdict(), and it returns an empty list.
    - The list.append() operation (in for loop next) then attaches the value to
      the new list.
    '''
    all_combo_dict = defaultdict(list)
    for word in wordList:
      print("\n------------------, word:", word)
      for i in range(L):
        '''
        all_combo_dict[] is a list, but still has key-val pairs
        - See: defaultdict Examples @ https://docs.python.org/3/library/collections.html#collections.defaultdict
        '''
        # - Key is the generic word
        # - Value is a list of words having the same intermediate generic word
        print("i:", i, ", word[:i]:", word[:i], ", (word[i+1:]:", word[i+1:])
        all_combo_dict[ word[:i] + "*" + word[i+1:] ].append(word)
        print("all_combo_dict:", all_combo_dict)


    # Queue for BFS
    queue = deque( [(beginWord, 1)] )
    # Visited to make sure we don't repeat processing same word
    visited = {beginWord: True}
    while queue:
      current_word, level = queue.popleft()
      print("\ncurrent_word:", current_word, ", level:", level)

      for i in range(L):
        # Intermediate words for current word
        inter_word = current_word[:i] + "*" + current_word[i+1:]
        print("i:", i, ", inter_word:", inter_word)

        # Next states are all the words sharing the same intermediate state
        for word in all_combo_dict[inter_word]:
          print("word:", word)
          print("all_combo_dict[inter_word]:",all_combo_dict[inter_word])

          # If we find endWor
          if word == endWord:  return level + 1

          # Otherwise, add it to the BFS Queue. Also mark it visited.
          if word not in visited:
            visited[word] = True
            queue.append((word, level + 1))  # Append a tuple ()

        # inter_word is handled, remove it from all_combo_dict[]
        all_combo_dict[inter_word] = []
        #print("-->> all_combo_dict[]:", all_combo_dict)

    return 0

#-----Test Case-----
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]

sol = Solution()
res = sol.ladderLength(beginWord, endWord, wordList)
print("\nres:", res)    # 5

