'''
127. Word Ladder

Key Words!
- find the length of shortest transformation sequence
-- Anytime you see "find the shortest sequence", you should immeidately think
   to use some sort of “shortest path algorithm”, such as BFS.
'''


'''
Approach 2: Bidirectional Breadth First Search ---> 未......

Explanation + Sol Code:
- Video: https://zxi.mytechroad.com/blog/searching/127-word-ladder/ -> 很好

LC Std Sol:
- https://leetcode.com/problems/word-ladder/solution/

'''

#-----Test Case-----
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]

sol = Solution()
res = sol.ladderLength(beginWord, endWord, wordList)
print("res:", res)    # 5

