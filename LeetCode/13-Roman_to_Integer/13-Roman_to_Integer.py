'''
13. Roman to Integer


思路:
- If cur letter is less than next letter, the cur letter is negative.
- Edge Case:
    - The last (right-most) letter is always added.

Text:
    - https://leetcode.com/problems/roman-to-integer/solutions/3421656/hash-table-concept-python3/

Video:
    https://www.youtube.com/watch?v=JlVOzbOJiv0
'''
class Solution:
    def romanToInt(self, s: str) -> int:
        m={"I":1,"V":5,"X":10,"L":50,"C":100,"D":500,"M":1000}
        n = len(s)
        ans = 0

        for i in range(n-1):
            if m[s[i]] < m[s[i+1]]:
                ans -= m[s[i]]
            else:
                ans += m[s[i]]
        return ans + m[s[-1]]
