'''
134. Gas Station
https://www.youtube.com/watch?v=lJwbPZGo05A

思路: => 最重要的一步: Identifing it's a greedy problem!
  (1) If total gas is less than total cost to complete a Loop, it won't work.
  (2) Otherwise, we guarantee there exists a sol.
'''
class Solution:
    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:
        # See: 思路(1)
        if sum(gas) < sum(cost):  return -1

        # See: 思路(2)
        total = 0  # Total gas available so far
        res = 0  # Starting pt
        for i in range(len(gas)):
            total += (gas[i] - cost[i])
            print("i:", i)
            print("gas[", i, "]:", gas[i], ", cost[", i, "]:", cost[i], \
                  ", total:", total)

            # If cur pt not enough gas to reach next pt, set next pt as starting pt
            if total < 0:
                total = 0
                res = i + 1
                print("res:", res)        return res


