/*
136. Single Number

Follor up: ---->  Must see Approach #4! ====> 簡單又好用!!
- Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

LeetCode Std Solution:
- https://leetcode.com/problems/single-number/solution/
*/

// - Approach #1:
// - 思路：If an elem is new to nums[], append it, otherwise remove it
/*
- Time complexity : O(n^2)
-- We iterate through nums[], taking O(n) time. We search the whole list,
  tmp[], to find whether there is a duplicate number, taking O(n) time
- Space complexity: O(n)
-- tmp[] used to contain at least size of n elements in nums[].
*/
var singleNumber = function(nums) {
  var tmp = [];

  for (var i = 0; i < nums.length; i++) {
    var idx = tmp.indexOf(nums[i]);

    if (idx == -1) {
      tmp.push(nums[i]);
    } else {
      tmp.splice(idx, 1);  // Remove 1 elem at idx
    }
  }
  // The only elem left in nums[] is the single num
  return tmp.pop();
};

// Approach #2: Hash Table
// - If our hash tbl doesn't have the elem, add it, otherwise remove it.
//
// - Time complexity : O(n * 1) = O(n)
//- Space complexity: O(n)
//
var singleNumber_2 = function(nums) {
  var hashTbl = {};

  for (var i = 0; i < nums.length; i++) {

    // If nums[i] key doesn't exist in hashTbl obj
    //if ( ! (nums[i] in hashTbl)) {
    if (hashTbl[nums[i]] == null) {  // Same as last ln
      hashTbl[ nums[i] ] = nums[i];
    }
    else {
      delete hashTbl[ nums[i] ];
    }
  }
  // The only elem left in nums[] is the single num
  for (var key in hashTbl) {
    return hashTbl[key];
  }
}

// Approach #3: Using Math and Set
// - Concept: 2*(a+b+c) − (a+a+b+b+c) = c
//
// - Time complexity : O(n + n) = O(n)
// - Space complexity: O(n), mySet needs space for elements in nums[]
//
var singleNumber = function(nums) {

  // Get unique values of nums[] and transform the arr to a set
  let mySet = new Set(nums);
  let sumOfSet = 0;
  mySet.forEach(num => sumOfSet += num);

  let sumOfNums = nums.reduce((acc, cur) => acc + cur);

  return (2 * sumOfSet) - sumOfNums;
}

// Approach #4: Bitwise Manupulation
// - Hint: a⊕b⊕a = (a⊕a)⊕b = 0⊕b = b
//
// - 技巧！！
// -- Bitwise XOR:
// (1）outputs true only when inputs differ
// (2) ex 1 ^ 3 = 2  (別忘記轉變十進位到二進位：01 ^ 11 = 10)
//
// - Time complexity : O(n)
// - Space complexity: O(1)
//
var singleNumber_4 = function(nums) {
  var result = nums[0];
  for (var i = 1; i < nums.length; i++) {
    console.log(result, " ^ ", nums[i], " = ", result ^ nums[i]);
    result = result ^ nums[i];
  }
  return result;
}

//var nums = [2,2,1];
var nums = [2,2,1,3,1];
//var nums = [4,1,2,1,2];
var result = singleNumber_4(nums);
console.log(result);
