/*
137. Single Number II

Note:
- The real game starts at the moment when yr-2020 Google interviewer (the problem is quite popular at Google the last six months) asks you to solve the problem in a constant space, testing if you are OK with bitwise operators.

*/

// Approach 2: HashMap
// - Iterate over nums[].  Add each num to hash{} and increment its count. When a count reaches 3, delete that entry.  At the end, hash{} has either 0/1 entry.
//
// - Time complexity : O(N) to iterate over the input array.
// - Space complexity: O(N) to keep the hashmap of N/3 elements.
//
var singleNumber = function(nums) {
  let hash = {};

  for (let n of nums) {
    if (!hash[n])
      hash[n] = 1;
    else {
      hash[n]++;
      if (hash[n] == 3)
delete hash[n]
    }
  }

  // Only has 0/1 entry in hash{}
  for (const [key, val] of Object.entries(hash)) {
    if (val == 1)  return key;
  }
};

/* Approach 3: Bitwise Operators : NOT, AND and XOR

(1) LeetCode Std Sol: ---> Its code is easier to memorize
- https://leetcode.com/problems/single-number-ii/discuss/43360/The-simplest-solution-ever-with-clear-explanation

//-------------------------------------
- 思路:

  ## 1st appearance:
  - Add num to seen_once
  - Don't add to seen_twice because of presence in seen_once

  ## 2nd appearance:
  - Remove num from seen_once
  - Add num to seen_twice

  ## 3rd appearance:
  - Don't add to seen_once because of presence in seen_twice
  - Remove num from seen_twice
//-------------------------------------

(2) More explanation:
- https://leetcode.com/problems/single-number-ii/discuss/43360/The-simplest-solution-ever-with-clear-explanation

//-------------------------------------

- Time complexity : O(N) to iterate over the input array.
- Space complexity : O(1) since no additional data structures are allocated.
*/
var singleNumber = function(nums) {
  let seen_once = 0,  seen_twice = 0;

  // 注意! for of + arr. for in + obj
  for (let num of nums) {  // 看上方思路
    seen_once = ~seen_twice & (seen_once ^ num);
    seen_twice = ~seen_once & (seen_twice ^ num);
  }

  return seen_once;
}

//-----Test Case-----//
let nums = [2,2,3,2];
console.log(singleNumber(nums));
