'''
14. Longest Common Prefix
'''

'''
NeetCode:
    https://www.youtube.com/watch?v=0sWShKIJoo4


思路:

Higher-level:
  - Only need to check up the the len of the shortest str

Lower-level:
- While iter-ing thr indices of strs[0], 
    Iter-ing all strs to compare them w/ strs[0] at index=i
        If every str's char at index=i is the same as strs[0][i]
            append the new char to res
'''
class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        res = ''
        s0_len = len(strs[0])  # 1st str len

        for i in range(s0_len):
            print("\ni:", i)

            # Compare every str w/ the 0-th str at index = i
            for s in strs[1:]:
                print("--->s:", s, ", strs[0]:", strs[0])
                print("s[i]:", s[i], ", strs[0][i]:", strs[0][i])

                # 1st if statement checks index out-of-bound
                if i == len(s) or s[i] != strs[0][i]:
                    return res

            # Append the new char to res
            res += strs[0][i]
            print("res:", res)
        
        return res




'''
Explanation + Sol Code:
    https://leetcode.com/problems/longest-common-prefix/solutions/3273176/python3-c-java-19-ms-beats-99-91/

Note: the code assumes that the input list v is non-empty, and that all the strings in v have at least one character. If either of these assumptions is not true, the code may fail.

'''
class Solution:
    def longestCommonPrefix(self, v: List[str]) -> str:
        ans = ""
        v = sorted(v)

        first = v[0]  # 1st & last strs of input arr
        last = v[-1]

        # Iterate thr the chars of the 1st and last strs in the sorted list,
        # stopping at the len of the shorter string.
        for i in range( min(len(first), len(last)) ):

            # If cur char of 1st str != cur char of last str, return ans
            if (first[i] != last[i]):
                return ans

            # Otherwise, append the cur char to ans
            ans += first[i]

        return ans



'''
=>From LC Std Sol:

Approach 1: Horizontal scanning

Explanation + Sol Code:
- https://www.youtube.com/watch?v=5nug0L9y1h4


Approach 2, 3, 4: No Need

'''
class Solution:
  def longestCommonPrefix(self, strs: List[str]) -> str:
    if len(strs) == 0:  return("")
    if len(strs) == 1:  return(strs[0])

    pref = strs[0]    # prefix
    plen = len(pref)  # prefix len

    for s in strs[1:]:
      while pref != s[0:plen]:
        pref = pref[0:(plen-1)]
        plen -= 1

        if plen == 0:   return("")

    return(pref)

