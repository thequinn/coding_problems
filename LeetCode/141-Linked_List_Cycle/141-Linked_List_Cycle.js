/*
141. Linked List Cycle
// .....題目給以下 ListNode struct 導致我以為要先寫 Linked List 程式碼來
// 測試用，其實是多餘......


Definition for singly-linked list:

  function ListNode(val) {
     this.val = val;
     this.next = null;
  }


Test:
let head = [3,2,0,-4];  // pos = 1
let head = [1];  // pos = -1


LeetCode Std Solution (拆題思路看此):
- Java: https://leetcode.com/problems/linked-list-cycle/solution/
- JS: (Only use the code.  But Chinese explanation is bad.)
-- https://leetcode.com/problems/linked-list-cycle/discuss/226855/JavaScript
*/

// Approach #1: Hash Table  ==> Use Set in JS version:
// - Time complexity : O(n)
// -- We visit each of the n elements in the list at most once. Adding a
//    node to the hash table costs only O(1) time.
// - Space complexity: O(n) 
// -- The space depends on the number of elements added to the hash table,
//    which contains at most n elements.
//
var hasCycle = function(head) {
  const nodes = new Set();

  while (head) {
    if (nodes.has(head))  
      return true;
    else  
      nodes.add(head);
    head = head.next;
  }
  return false;
};

// Approach #2: Two Pointers 
//
// Algorithm used:
// -  Tortoise-Hare Algorithm (Floyd's Cycle Detection)
//
// - Time and Space complexity: See LeetCode Std Solutions
//
var hasCycle = function(head) {
  let fast = head;
  let slow = head;
  
  // Not testing fast.next is null will cause error in ln-55:
  // - ex. If a Linked List is [1] and has no cycle
  //      -> fast.next returns null: Correct
  //      -> fast.next.next will be null.next: ERROR
  //
  //while (fast) {
  //
  while (fast && fast.next) {
    fast = fast.next.next;
    slow = slow.next;
    
    if (fast === slow)
      return true;
  }
  return false;
};


