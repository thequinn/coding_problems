'''
141. Linked List Cycle

https://leetcode.com/problems/linked-list-cycle/solutions/823960/two-approaches-in-python-3-dictionary-and-two-pointers/
'''


'''
Approach #2: Two pointers
- Floyd's cycle finding algorithm or Hare-Tortoise algorithm
    - a Two Ptrs algorithm that using 2 ptrs moving through the sequence at different speeds.

Video Visualization:
    https://leetcode.com/problems/linked-list-cycle/solutions/3911008/video-clear-visualization-using-two-pointers/

Text Explanation & Code Sol:
    https://leetcode.com/problems/linked-list-cycle/solutions/3460021/beats-92-12-20-145-top-interview-question/


思路:
1. We init two pointers, s (slow) and f (fast), both pointing to the head node.

2. Start iterating over the linked list using s and f pointers. The s pointer moves one step at a time while the f pointer moves two steps at a time.

3. If there is a cycle in the linked list, the f pointer will eventually catch up to the s pointer from behind, i.e., f will eventually become equal to s.
'''
class Solution:
    def hasCycle(self, head: Optional[ListNode]) -> bool:
        s, f = head,head

        '''
        f.next condition:
        - It prevents f pts pass the end of LL.

          ex. f pts to 4
              1 -> 2 -> 3 -> 4 -> null (end of LL)

              f = f.next.next would give the error:
                AttributeError: 'NoneType' object has no attribute 'next'
        '''
        while f and f.next:
            # Move slow pointer by 1 node and fast at 2 at each step
            s = s.next
            f = f.next.next
            # If both ptrs meet at any point, a cycle is present
            if f == s:
                return True
        return False


'''
Approach #1: Dict/Hash table => QN not understand why ln-63 not work,
                                Skip Approach #1.

    https://leetcode.com/problems/linked-list-cycle/solutions/823960/two-approaches-in-python-3-dictionary-and-two-pointers/
'''
class Solution:
    def hasCycle(self, head: Optional[ListNode]) -> bool:
        dct = {}

        while head:
            # 注意!! QN don't know why this if statement not work.
            #        Just memorize it!!
            #if nodes[head]:
            #
            if head in dct:
                return True

            else:
                dct[head] = True

            head = head.next

        return False


