'''
1411. Number of Ways to Paint N × 3 Grid

'''

'''
// Java Code => Help to understand Python code below
//
public int numOfWays(int n) {
  long a121 = 6, a123 = 6, b121, b123, mod = (long)1e9 + 7;
  for (int i = 1; i < n; ++i) {
    b121 = a121 * 3 + a123 * 2;
    b123 = a121 * 2 + a123 * 2;
    a121 = b121 % mod;
    a123 = b123 % mod;
  }
  return (int)((a121 + a123) % mod);
}


Method #1:
- https://leetcode.com/problems/number-of-ways-to-paint-n-3-grid/discuss/574923/JavaC%2B%2BPython-DP-O(1)-Space

Time Complexity : O(N)
Space Complexity:  O(1)
'''
def numOfWays(self, n):
  a121, a123, mod = 6, 6, 10**9 + 7

  for i in xrange(n - 1):
    a121, a123 = a121 * 3 + a123 * 2, a121 * 2 + a123 * 2

  return (a121 + a123) % mod

'''
Approach #2:
- https://leetcode.com/problems/number-of-ways-to-paint-n-3-grid/discuss/575485/C++Python-O(logN)-Time

Explanation based on 935. Knight Dialer:
- https://leetcode.com/problems/knight-dialer/discuss/189252/O(logN)

'''
