/*
142. Linked List Cycle II

Algorithm used:
-  Tortoise-Hare Algorithm (Floyd's Cycle Detection) 


Solution:

(1) 思路：(不用這裡的Code, 用下方的比較簡單)
-- https://leetcode.com/problems/linked-list-cycle-ii/discuss/286362/JavaScript-Solution-with-illustrations-explanation
-- Quinn's 解釋：
  用LeetCode 141. Linked List Cycle 確認cycle 中兩個ptrs meet.  再用slow ptr
  和 head ptr 一步步走，這兩個 ptrs 的相遇點就是 cycle 開頭.

  ex. head = [3,2,0,-4];  // pos = 1

(2) Code:
-- https://leetcode.com/problems/linked-list-cycle-ii/discuss/44897/Python-solution-with-comments.

*/

var detectCycle = function(head) {
  let fast = head, slow = head;

  // Checking the fast.next condition below, see LeetCode 141.
  while (fast && fast.next) {
    fast = fast.next.next;
    slow = slow.next;

    // If there is a cycle
    if (slow === fast) {
      // head and slow ptrs move step by step
      while (head) {
        if (head === slow)  return head;
        head = head.next;
        slow = slow.next;
      }
    }
  }
  return null;
};

