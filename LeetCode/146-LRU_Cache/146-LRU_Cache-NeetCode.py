'''
146. LRU Cache


Ptr Fundamental: 
 (1) item-on-the-left: ptr
 (2) item-on-the-right: addr

# - - - - - - - - - - - - #

Concept: =>  Root causes: I thought I remember the following fundamentals, but 
             I didn't.  Learn them first to speed up thinking process!

(1) Doubly LL
    https://www.youtube.com/watch?v=sDP_pReYNEc

(2) LRU Cache:      => Note: This video adds MRU to "left" end of LL
    https://www.youtube.com/watch?v=S6IfqDXWa10



>Ptr concept: => DO NOT MIX THEM WHEN TRACING CODE!!
(1) item-on-the-left: ptr
(2) item-on-the-right: addr/location

# - - - - - - - - - - - - #

NeetCode's Concept + Code: => QN make the code more human-readable
- https://www.youtube.com/watch?v=7ABFKPK2hD4


- Tips on Solving this problem: 
    - This video adds 
        - LRU to "left" end of Doubly LL
        - MRU to "right" end of Doubly LL

    - Setting of prev and nxt ptrs in insert() and remove() ->>>太重要
        - Ensure correct concepts how ptr works w/ addr assignment, otherwise 
          you won't get this tracing code correctly
        - { prev ptr pts "before" } and { nxt ptr pts "after" } 
          the node that's going to be {inserted} or {removed}. 


>insert()

1. prev ptr pts "before" the node that's going to be inserted
   nxt ptr pts "after" the node that's going to be inserted

2. Utility Nodes:
    - MRU (Most Recently Used) node
        - Always stay on the right side on the self node
    - LRU (Lease Recently Used) node
        - Always stay on the left side on the self node

3. B/c the insert() for this problem is to insert at the right-end of the Doubly 
LL, we need to set 2 ptrs, prev & nxt.  And then insert the new node in b/t.  
    - nxt ptr always pts to the MRU node
    - prev ptr always pts to where the MRU.prev pts to

'''

class Node:
    def __init__(self, key, val):
        self.key, self.val = key, val
        self.prev = self.next = None


class LRUCache:
    def __init__(self, capacity: int):
        self.cap = capacity
        self.cache = {}  # Map key to node

        # See above: Ptr concept!!!
        self.left = self.right = Node(0, 0)

        self.left.next = self.right
        self.right.prev = self.left

    # Remove node from the LL
    def remove(self, node):
        prev = node.prev # prev will be before the node gonna be removed
        nxt = node.next  # nxt will be after the node gonna be removed
        
        prev.next = nxt
        nxt.prev = prev

    # Insert node at right-end of the LL
    def insert(self, node):
        prev = self.right.prev  # prev will be before the node gonna be inserted
        nxt = self.right        # nxt will be after the node gonna be inserted

        prev.next = node
        node.prev = prev
        
        nxt.prev = node
        node.next = nxt

    def get(self, key: int) -> int:
        if key in self.cache:
            self.remove(self.cache[key])
            self.insert(self.cache[key])
            return self.cache[key].val
        return -1

    def put(self, key: int, value: int) -> None:
        if key in self.cache:
            self.remove(self.cache[key])
        self.cache[key] = Node(key, value)
        self.insert(self.cache[key])

        if len(self.cache) > self.cap:
            # Remove the least recently used node from doubly LL, and delete it from dict
            lru = self.left.next
            self.remove(lru)
            del self.cache[lru.key]
