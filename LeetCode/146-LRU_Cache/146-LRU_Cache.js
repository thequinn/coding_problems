/*
146. LRU Cache

>Video Explanation:
- https://www.youtube.com/watch?v=S6IfqDXWa10&t=9s


>LeetCode Premium Sol:
- (1) Java & Python Code: https://leetcode.com/problems/lru-cache/solution/

- (2) JS Code: See below https://leetcode.com/problems/lru-cache/discuss/178988/HashMap%2BDoubleLinkedList-in-JavaScript

-- Note:
    Don't use native API, for example Object.keys(hashMap) to count keys
    Adopt DoubleLinkedList to get O(1)
    Store the key just for deleting the object :)
*/

/*
Approach 2: Hashmap + DoubleLinkedList

- 思路:
-- The problem can be solved with a hashmap that keeps track of the keys and its values in the double linked list. That results in O(1) time for put and get operations and allows to remove the first added node in O(1) time as well.

One advantage of double linked list is that the node can remove itself without other reference. In addition, it takes constant time to add and remove nodes from the head or tail.

One particularity about the double linked list implemented here is that there are pseudo head and pseudo tail to mark the boundary, so that we don't need to check the null node during the update.


- JS Code: https://leetcode.com/problems/lru-cache/discuss/178988/HashMap%2BDoubleLinkedList-in-JavaScript

-- Note:
    Don't use native API, for example Object.keys(hashMap) to count keys
    Adopt DoubleLinkedList to get O(1)
    Store the key just for deleting the object :)
*/
var LRUCache = function(capacity) {
  this._capacity = capacity;
  this._count = 0;
  this._head = null;
  this._tail = null;
  this._hashTable = {};
};

LRUCache.prototype.get = function(key) {
  if (this._hashTable[key]) {
    const { value } = this._hashTable[key];
    const { prev, next } = this._hashTable[key];
    if (prev) { prev.next = next; }
    if (next) { next.prev = prev || next.prev; }

    if (this._tail === this._hashTable[key]) {
      this._tail = prev || this._hashTable[key];
    }

    this._hashTable[key].prev = null;
    if (this._head !== this._hashTable[key]) {
      this._hashTable[key].next = this._head;
      this._head.prev = this._hashTable[key];
    }

    this._head = this._hashTable[key];

    return value;
  }

  return -1;
};

LRUCache.prototype.put = function(key, value) {
  if (this._hashTable[key]) {
    this._hashTable[key].value = value;
    this.get(key);
  } else {
    this._hashTable[key] = { key, value, prev: null, next: null };
    if (this._head) {
      this._head.prev = this._hashTable[key];
      this._hashTable[key].next = this._head;
    }

    this._head = this._hashTable[key];

    if (!this._tail) {
      this._tail = this._hashTable[key];
    }

    this._count += 1;
  }

  if (this._count > this._capacity) {
    let removedKey = this._tail.key;

    if (this._tail.prev) {
      this._tail.prev.next = null;
      this._tail = this._tail.prev;
      this._hashTable[removedKey].prev = null;
    }

    delete this._hashTable[removedKey];

    this._count -= 1;
  }
};


/*
Approach 3: Using Map() to replace Approach 2 b/c Map() is an orderedDict
- JS Code: https://leetcode.com/problems/lru-cache/discuss/399146/Clean-JavaScript-solution

- Note:
Someone mentioned:
  This will not fly in the normal interview process. We cannot use special object like JavaScript Map object. Whats the point if we use some special in-built function/object to solve the problem? The idea is to come up with LRU using basic algorithms and data structures. Clarify with the interviewer, as an interviewer myself, I would point this out to my candidates.

So QN should learn Approach 2
*/
class LRUCache {
  constructor(capacity) {
    this.cache = new Map();
    this.capacity = capacity;
  }

  get(key) {
    if (!this.cache.has(key)) return -1;

    // - Map is an orderedDict.  And we want to move most recently used item to
    // the front of the Map(), so it eliminates the use of a linked list to
    // tell which item is used more recently in order ---> QN thinks...
    //
    // - This part is to remove the key-val pair from old pos, and then insert
    // the pair again, so it's in the front of the Map().
    //
    const v = this.cache.get(key);
    this.cache.delete(key);
    this.cache.set(key, v);
    return this.cache.get(key);
  };

  put(key, value) {
    if (this.cache.has(key)) {
      this.cache.delete(key);
    }

    this.cache.set(key, value);

    if (this.cache.size > this.capacity) {
      // Map.prototype.keys()
      // - Returns a new Iterator object that contains the keys for each
      //   element in the Map object in insertion order.
      //
      // keys().next().value returns first item's key from the Iterator obj
      this.cache.delete(this.cache.keys().next().value);
    }
  };
}
