'''
1480. Running Sum of 1d Array
'''

class Solution:
  def runningSum(self, nums: List[int]) -> List[int]:

    # Using range() to start from 1st-index of the for loop
    for i in range(1, len(nums)):
      nums[i] = nums[i-1] + nums[i]
    #print("i:", i)

    return nums
