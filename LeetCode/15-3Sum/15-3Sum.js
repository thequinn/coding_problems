/*
15. 3Sum

Problem:
- Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0

Note: The target of this problem is 0
*/

/*
Approach #1: Two Pointers
- https://leetcode.com/problems/3sum/discuss/281302/JavaScript-with-lots-of-explanatory-comments!
- 思路:
-- `i` that starts at the beginning, `k` that starts at the end, and `j`
   that races in between the two.
-- `i` is controlled by our outer for-loop and will move the slowest. In the
   meantime, `j` and `k` will take turns inching towards each other
   depending on some logic we'll set up below. once they collide, `i` will
   be incremented up and we'll repeat the process.
*/
var threeSum = function(nums) {
  var rtn = [], target = 0;

  if (nums.length < 3)    return rtn;

  nums = nums.sort((a, b) => a - b);

  for (var i = 0; i < nums.length - 2; i++) {
    if (nums[i] > target) {
      return rtn;
    }

    if (i > 0 && nums[i] == nums[i - 1]) {
      continue;
    }

    for (var j = i + 1, k = nums.length - 1; j < k;) {
      if (nums[i] + nums[j] + nums[k] === 0) {
        rtn.push([nums[i], nums[j], nums[k]]);
        j++;    k--;

        while (j < k && nums[j] == nums[j - 1]) {
          j++;
        }
        while (j < k && nums[k] == nums[k + 1]) {
          k--;
        }
      }
      else if (nums[i] + nums[j] + nums[k] > 0) {
        k--;
      }
      else {
        j++;
      }
    }
  }

  return rtn;
};

/*
Approach #2: Deduplication -> QN thinks it's hard to write it in JS
*/

