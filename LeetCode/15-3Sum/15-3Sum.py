'''
15. 3Sum

Explanation & Solution Code:
  https://www.youtube.com/watch?v=jzZsG8n2R9A

Hint:
- 167. Two Sum II is the foundation of this question
-- 解法提要:
    The pointers are initially set to the first and the last element respectively. We compare the sum of these two elements to the target.
    If it is smaller, we increment the lower pointer lo. Otherwise, we decrement the higher pointer hi. Thus, the sum always moves toward the target, and we "prune" pairs that would move it further away.
    Again, this works only if the array is sorted.

ex. [-2,-2,0,0,2,2]
'''

# Approach #1: Two Ptrs
class Solution:
    def threeSum(nums):
        nums.sort()  # Step 1: Sort the array in-place
        res = []

        # i's iteration range is [ 0, len(nums)-2 ) b/c when i=len(nums)-3, 
        # the next 2 available indices are j=len(nums)-2, k=len(nums)-1
        for i in range(len(nums) - 2):
            if i > 0 and nums[i] == nums[i - 1]:
                continue  # Skip duplicate values for i
        
            j, k = i + 1, len(nums) - 1  # Two pointers
            while j < k:
                s = nums[i] + nums[j] + nums[k]
                if s == 0:
                    res.append([nums[i], nums[j], nums[k]])
                    j += 1
                    k -= 1
                    # Skip duplicates for j and k
                    while j < k and nums[j] == nums[j - 1]:
                        j += 1
                    while j < k and nums[k] == nums[k + 1]:
                        k -= 1
                elif s < 0:
                    j += 1  # Increase sum by moving left pointer
                else:
                    k -= 1  # Decrease sum by moving right pointer

        return res
'''
Time Complexity: O(n^2)
	•	Sorting the array takes O(n log n).
	•	The two-pointer iteration for each fixed element takes O(n), and the 
        loop runs O(n) times.

Space Complexity: O(1) additional space, excluding the output list.
	• Note: The space complexity of O(1) for the optimized approach means that 
            no extra space is used apart from variables that do not scale with 
            input size.
	• Local variables
	    - The output list that stores the triplets. This space is required to store the results, but since it depends on the number of valid triplets (output size), it is excluded from the additional space calculation. Space for the output is usually not included in space complexity analysis.
	    - i, j, k: The pointers used in the loops. These are constant and independent of the size of nums.

'''




'''
Approach #2: Deduplication  ====> Trying to make the code work...未...
- https://algo.monster/problems/deduplication

思路:
- Read the Intuition part and Implementation part for details
'''
class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        res = []

        for i in range(len(nums)i - 2):
            if i > 0 and nums[i-1] == nums[i]:
                continue
            
            # Find pairs that sum up to (target - nums[i])
            # Note: tuples here is not a tuple in Python.  In math, a tuple is 
            #       an ordered sequence of values.
            tuples = self.twoSum1( nums[i+1:], target - nums[i])
            #print("tuples:", tuples)       

            # Construct triplets from the tuples
            for t in tuples:
                res.append([nums[i], t[0], t[1]])
        return res

    ''' 
    WRONG => See allPairs_TwoSum() 
    
    167. Two Sum II - Approach #1: Two Pointers 
    
    Note:
    - Using the twoSum() from below is not enough.
      It's b/c 167. finds two numbers such that they add up to a specific 
      target number.
    - See allPairs_TwoSum() to get all pairs of elem's summed up to target
    '''
    def twoSum1(self, numbers, target):
        l, r = 0, len(numbers)-1
        while l < r:
            s = numbers[l] + numbers[r]
            if s == target:    return [l+1, r+1]
            elif s < target:   l += 1
            else:              r -= 1

    def allPairs_TwoSum(numbers, target):
        result = []
        l, r = 0, len(numbers) - 1

        while l < r:
            s = numbers[l] + numbers[r]
            if s == target:
                result.append([numbers[l], numbers[r]])  # Append pair to result
                l += 1
                r -= 1

                # Skip duplicates for twoSum
                while l < r and numbers[l] == numbers[l - 1]:
                    l += 1
                while l < r and numbers[r] == numbers[r + 1]:
                    r -= 1
            elif s < target:    l += 1
            else:               r -= 1

        return result
