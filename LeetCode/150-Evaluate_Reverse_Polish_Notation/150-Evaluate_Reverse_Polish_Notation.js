/*
150. Evaluate Reverse Polish Notation
*/

/*
Approach 1: Evaluate with Stack

思路:
- If it's an operand, push to stack
- If it's an operator, pop the last 2 operands from stack, do operation, and
  push the result back to stack

Code:
- https://leetcode.com/problems/evaluate-reverse-polish-notation/discuss/134672/Clean-JavaScript-solution

*/
function evalRPN(tokens) {
  const ops = {
    '+': (a, b) => a + b,
    '-': (a, b) => a - b,
    '*': (a, b) => a * b,
    '/': (a, b) => ~~(a / b)  // "~~": a faster substitute for Math.floor()
  };

  const stack = [];

  for (let n of tokens) {

    if (ops[n] != null) {
      const b = stack.pop();
      const a = stack.pop();
      stack.push(ops[n](a, b));
    } else {
      stack.push(Number(n));
    }
  }

  return stack[0];
}

//-----Test Case-----
let nums = ["4","13","5","/","+"]; // 6
//let nums = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]; // 22

console.log(evalRPN(nums));
