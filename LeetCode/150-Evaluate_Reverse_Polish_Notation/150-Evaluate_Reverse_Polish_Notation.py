'''
150. Evaluate Reverse Polish Notation

Approach: Evaluate with Stack
思路:
- If it's an operand, push to stack
- If it's an operator, pop the last 2 operands from stack, do operation, and
  push the result back to stack

'''


'''
Approach #2-1: -> The only diff between Approach #2-1 & #2-2 is one uses a
                  func, and other one usesan obj for the calculation.

- https://leetcode.com/problems/evaluate-reverse-polish-notation/solutions/1229359/python-short-stack-solution-explained/
'''
class Solution:
    def evalRPN(self, tokens):
        def f(a, b, c):
            if c == "+": return a+b
            if c == "-": return b-a
            if c == "*": return a*b
            if c == "/": return int(b/a)

        stack = []
        for token in tokens:
            if token in "*/+-":
                stack.append(f(stack.pop(), stack.pop(), token))
            else:
                stack.append(int(token))

        return stack[0]     # Same as next ln
        #return stack[-1]



'''
Approach #2-2:


Easy fix for neg division bug in Python:
    https://leetcode.com/problems/evaluate-reverse-polish-notation/solutions/277848/standard-python-solution-easy-fix-for-neg-division-bug/

- Python integer division only work when both dividend and divisor are
  positive: +a // +b.  When either a or b is negative, it evaluates to -1
  instead of the expected 0.
- Therefore the easiest way to handle this:
        int( float( a / b) )

'''
class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        operators = {
            '+': lambda y, x: x + y,
            '-': lambda y, x: x - y,
            '*': lambda y, x: x * y,

            # Explination: see "Easy fix for neg division bug in Python"
            '/': lambda y, x: int(float(x / y))
            #'/': lambda y, x: int(operator.truediv(x, y))
        }

        stack = []
        for token in tokens:
            if token in operators:
                stack.append(operators[token](stack.pop(), stack.pop()))
            else:
                stack.append(int(token))
        return stack[-1]


    '''
Approach #1:
- https://leetcode.com/problems/evaluate-reverse-polish-notation/solutions/47444/python-easy-to-understand-solution/
'''
def evalRPN(self, tokens):
    stack = []
    for t in tokens:
        if t not in "+-*/":
            stack.append(int(t))
        else:
            r, l = stack.pop(), stack.pop()
            if t == "+":
                stack.append(l+r)
            elif t == "-":
                stack.append(l-r)
            elif t == "*":
                stack.append(l*r)
            else:
                '''
                Note:
                - The floor value for negative numbers is 1 by default.
                  (Not sure why floor() works differently in JS.)
                      ex. floor(6 / -132) = floor(-0) = 1
                '''
                #tmp = floor(l / r)                 # WRONG!!
                #tmp = int(l / r)                   # Correct
                tmp = int(operator.truediv(l, r))   # Correct. See trudiv below.

                stack.append( tmp )
    return stack.pop()

