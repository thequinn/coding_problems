'''
152-Maximum_Product_Subarray


(1) NeetCode: https://www.youtube.com/watch?v=lXVy6YWFcRM

(2) https://leetcode.com/problems/maximum-product-subarray/discuss/1608862/JAVA-or-3-Solutions-or-Detailed-Explanation-Using-Image

(3) https://leetcode.com/problems/maximum-product-subarray/solutions/48276/python-solution-with-detailed-explanation/



Intuition: => from (2)
   Since we have to find the contiguous subarray having maximum product then your approach should be combination of following three cases:

Case 1:
- All the elems are positive : Then your answer will be product of all the elems in the array.

Case 2:
- Array have positive and negative elems both:
    - If the num of negative elems is even then again your answer will be complete array because on multiplying all the negative nums it will become positive. 
    - If the num of negative elems is odd then you have to remove just one negative element and for that u need to check your subarrays to get the max product.

Case 3:
- Array also contains 0: 
    - Then there will be not much difference...its just that your array will be divided into subarray around that 0. What u have to so is just as soon as your product becomes 0 make it 1 for the next iteration, now u will be searching new subarray and previous max will already be updated.

'''

'''
Approach #1: Brute Force => TLE, by QN

'''
class Solution:
    def maxProduct(self, nums: List[int]) -> int:
        n = len(nums)
        curP, maxP = 0, -11
        for i in range(n):
            curP = 0
            for j in range(i, n):
                #print("nums[", i, "]:", nums[i], ", nums[", j, "]:", nums[j])
                
                # if encountering same num
                if j == i:  curP = nums[i]
                # if encountering diff nums
                else:       curP *= nums[j]
                
                maxP = max(maxP, curP)
        return maxP


'''
Approach #1-1: DP => See sol in (1)
- This approach sets "curMax, curMin" to 1, and resMax=max(nums). And its loop starts from index=0


學習技巧:
- Trace the code using an ex to comprehend the concept of the code
ex. Input: nums = [-1,1,0,-3,3]
    Output: [0,0,9,0,0]

Time Complexity:  O(N)
Space Complexity: O(1)

'''
def maxProduct(self, nums: List[int]) -> int:
  curMax, curMin, resMax = 1, 1, max(nums)
  
  for n in nums:
    tmp = curMax * n
    curMax = max(curMax*n, curMin*n, n)  # ex. [-1, 8]
    # Using tmp instead of curMax*n b/c curMax is recomputed in last ln
    curMin = min(tmp, curMin*n, n)       # ex. [-1, -8]
    resMax = max(resMax, curMax)
  return resMax


'''
Approach #1-2 => See 1st sol in (2)
- This version sets "curMax, curMin, resMax" to the val of the 1st elem. And its loop starts from index=1
'''
def maxProduct(self, nums: List[int]) -> int:
  curMax, curMin, resMax = nums[0], nums[0], nums[0]
  for v in nums[1:]:
      #print('v:', v)
      tmp = curMax * v
      curMax = max(curMax*v, curMin*v, v)
      curMin = min(tmp, curMin*v, v)
      resMax = max(resMax, curMax)
      print(curMax, ", ", curMin, ", ", resMax)
  return resMax



'''
Approach #1-3 => See sol in (3)

'''
class Solution(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        max_prod, min_prod, ans = nums[0], nums[0], nums[0]
        for i in range(1, len(nums)):
            x = max(nums[i], max_prod*nums[i], min_prod*nums[i])
            y = min(nums[i], max_prod*nums[i], min_prod*nums[i])            
            max_prod, min_prod = x, y
            ans = max(max_prod, ans)
        return ans


'''
Approach #2 => See 2nd sol in (2) --> Skip, no need
'''


'''
Approach #3: Two Ptrs => See 3rd sol in (2) --> Not yet learn...
'''
