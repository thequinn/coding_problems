/*
155. Min Stack

分析:
- We're told that all the MinStack operations must run in constant time, O(1).  For this reason, we can immediately rule out the use of a Binary Search Tree or Heap. While these data structures are often great for keeping track of a minimum, their core operations (find, add, and remove) are O(logn), which isn't good enough here!
- Better way: Use an array to implement a stack operation


思路 for Approach 1-1 & 1-2:
- In addition to putting a number on an underlying Stack inside our MinStack, we could also put its corresponding minimum value alongside it.
- Each stack looks like:
    Approach 1-1: {val:..., min:...}
    Approach 1-2: [val, min]


Note: Approach 1-1 and 1-2 用2種方法寫 JS class
*/

//-----------------------------------------------------------
// Approach 0: 1 Stack, vanilla approach
var MinStack = function () {
  this.stack = [];
};
MinStack.prototype.push = function (x) {
  this.stack.push(x);
};
MinStack.prototype.pop = function () {
  this.stack.pop();
};
MinStack.prototype.top = function () {
  return this.stack[this.stack.length - 1];
};
MinStack.prototype.getMin = function () {
  return Math.min(...this.stack);
};

//-----------------------------------------------------------
/*
Approach 1-1: Stack of Value/ Minimum Pairs

Solution Code:
- https://leetcode.com/problems/min-stack/discuss/204269/javascript
*/
var MinStack = function() {
  this.stack = []
};
MinStack.prototype.push = function(x) {
  let min = this.stack.length === 0 ? x : this.stack[this.stack.length - 1].min
  this.stack.push( {val: x, min: Math.min(min, x)} )
};
MinStack.prototype.pop = function() {
  if (this.stack.length > 0){
    this.stack.pop()
  }
};
MinStack.prototype.top = function() {
  if (this.stack.length > 0) {
    return this.stack[this.stack.length - 1].val
  }
};
MinStack.prototype.getMin = function() {
  if (this.stack.length > 0) {
    return this.stack[this.stack.length - 1].min
  }
};

//-----------------------------------------------------------
/*
Approach 1-2: Stack of Value/ Minimum Pairs

LC Premoum Sol:
- https://leetcode.com/problems/min-stack/solution/
*/
function last(arr) {
  return arr[arr.length - 1];
}

class MinStack {
  _stack = [];

  push(x) {
    // If the stack is empty, the min value is the first value we add
    if (this._stack.length === 0) {
      this._stack.push([x, x]);
      return;
    }

    const currentMin = last(this._stack)[1];
    this._stack.push([x, Math.min(currentMin, x)]);
  }

  pop() {
    this._stack.pop();
  }

  top() {
    return last(this._stack)[0];
  }

  getMin() {
    return last(this._stack)[1];
  }
}




