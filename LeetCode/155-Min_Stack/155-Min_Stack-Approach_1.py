'''
155. Min Stack

'''

#---------------------------------------------------------------
'''
Approach 1-1: Stack of Value/ Minimum Pairs

Solution Code:
- https://leetcode.com/problems/min-stack/discuss/49022/My-Python-solution
'''
class MinStack:

  def __init__(self):
    self.stack = []

  def push(self, x):
    curMin = self.getMin()
    if curMin == None or x < curMin:  curMin = x
    # Using tuple, (val, min), for each stack
    self.stack.append((x, curMin));

    #Same as last 4 ln's
    #self.stack.append( ( x, min(x, self.getMin()) ) )

  def pop(self):
    self.stack.pop()

  def top(self):
    if len(self.stack) == 0:
        return None
    else:
        return self.stack[len(self.stack) - 1][0]

  def getMin(self):
    if len(self.stack) == 0:
        return None
    else:
        return self.stack[len(self.stack) - 1][1]

#---------------------------------------------------------------
'''
Approach 1-2: Stack of Value/ Minimum Pairs

LC Premium Sol:
- https://leetcode.com/problems/min-stack/solution/

'''
class MinStack:

  def __init__(self):
    self.stack = []

  def push(self, x: int) -> None:
    # If the stack is empty, then the min value is the first value we add
    if not self.stack:
      # Using tuple, (val, min), for each stack
      self.stack.append((x, x))
      return

    current_min = self.stack[-1][1]
    self.stack.append((x, min(x, current_min)))

  def pop(self) -> None:
    self.stack.pop()

  def top(self) -> int:
    return self.stack[-1][0]

  def getMin(self) -> int:
    return self.stack[-1][1]
