'''
155. Min Stack

Approach 2: Two Stacks

- Video:
    https://www.youtube.com/watch?v=qkLl7nAwDPo&t=2s
- Article:
    https://leetcode.com/problems/min-stack/discuss/158351/Javascript-68ms-beats-100

'''

class MinStack:

    def __init__(self):
        self.stack = []
        self.minStack = []

    def push(self, val: int) -> None:
        self.stack.append(val)

        # The min value is either val or on the top of the stack "if our
        # minStack is non-empty"
        #
        # Note!! An empty list in Python is False
        val = min(val, self.minStack[-1] if self.minStack else val)
        self.minStack.append(val)

    def pop(self) -> None:
        self.stack.pop()
        self.minStack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        return self.minStack[-1]

# Test #2:
ms = MinStack()
ms.push(-2)
ms.pop()
print(ms.minStack)
ms.push(1)
print(ms.top())
print(ms.getMin())

'''
# Test #1:
ms = MinStack()
ms.push(3)
ms.push(-9)
ms.push(7)
ms.push(-2)
ms.pop()
print(ms.top())
print(ms.getMin())
'''

