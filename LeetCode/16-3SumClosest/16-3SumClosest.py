'''
16. 3Sum Closest

LC Std Sol: https://leetcode.com/problems/3sum-closest/solution/

'''

'''
Approach 1: Two Pointers

Time Complexity: O(n^2)
- Sorting the array takes O(nlogn), so overall complexity is O(nlogn + n^2). This is asymptotically equivalent to O(n^2)

Space Complexity: O(logn) to O(n)
- depending on the implementation of the sorting algorithm
'''
class Solution:
  def threeSumClosest(self, nums: List[int], target: int) -> int:
    diff = float('inf')
    nums.sort()

    for i in range(len(nums)):
      lo, hi = i + 1, len(nums) - 1
      while (lo < hi):
        sum = nums[i] + nums[lo] + nums[hi]
        if abs(target - sum) < abs(diff):
          diff = target - sum

        if sum < target: lo += 1
        else:            hi -= 1
      if diff == 0:    break
    return target - diff

