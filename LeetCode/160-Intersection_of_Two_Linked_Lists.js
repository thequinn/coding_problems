/*
160. Intersection of Two Linked Lists

Write a program to find the node at which the intersection of two singly linked lists begins.

Notes:
1. If the two linked lists have no intersection at all, return null.
2. The linked lists must retain their original structure after the function returns.
3. You may assume there are no cycles anywhere in the entire linked structure.
4. Your code should preferably run in O(n) time and use only O(1) memory.


LeetCode Std Sol:
- https://leetcode.com/problems/intersection-of-two-linked-lists/solution/
*/

//----------------------------------------------------
// Approach 1: Brute Force
// - For each node ai in list A, traverse the entire list B and check if any node in list B coincides with ai.
//
// - Time complexity : O(mn)
// - Space complexity : O(1)


//----------------------------------------------------
// Approach 2: Hash Table
// - Traverse list A and store the address / reference to each node in a hash set. Then check every node bi in list B: if bi appears in the hash set, then bi is the intersection node.
//
// - Time complexity : O(m+n)
// - Space complexity : O(m) or O(n)


//----------------------------------------------------
// Approach 3: Two Pointers
// - 思路:
// -- Maintain two pointers, pA pB, then let them both traverse through the lists, one node at a time. When pA reaches the end of a list, then redirect it to the head of Linked List B; similarly when pB reaches the end of a list, redirect it the head of Linked List A. If at any point pA meets pB, then pA/pB is the intersection node.
// ex. 2 lists: A = {1,3,5,7,9,11} and B = {2,4,9,11}, which are intersected at node '9'. Since B.length (=4) < A.length (=6), pB would reach the end of the merged list first. By redirecting pB to "curr" head A, we are guaranteed to reach the intersection node.
//
// Code: https://leetcode.com/problems/intersection-of-two-linked-lists/discuss/49891/Javascript-5-lines-with-inline-comment
//
// - Time complexity : O(m+n)
// - Space complexity : O(1)
//
var getIntersectionNode = function(ah, bh) {
  var a=ah, b=bh;

  while(a != b){
    a = a ? a.next : bh // move a to head of b if at end
    b = b ? b.next : ah // move b to head of a if at end
  }

  // a === b either happen at the connecting point or when they are both null.
  // For the case where a and b are both null, it means we travered to the end of both lists and there are no intersection found.
  return a;
};
