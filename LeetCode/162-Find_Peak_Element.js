/*
162. Find Peak Element

LeetCode Std Solution:
- Java Version Code + Explanation:
    https://leetcode.com/problems/find-peak-element/solution/
- JS Version Code:
    https://leetcode.com/problems/find-peak-element/discuss/133199/JavaScript-linear-scan-%2B-binary-search-(recursive-%2B-iterative)-solutions

*/

// Approach #1: Linear Scan
// - 思路:
// -- Whenever, we find a number nums[i], we only need to check if it is larger
//    than the next number nums[i+1] for determining if nums[i] is the peak.
//    (1) Case 1: [1], [1,2,3]
//        - nums[] contains only 1 num or nums[] in ascending order
//    (2) Case 2: [3,2,1], [1,2,3,2]
//        - nums[] in descending order or has a decending order
//
// - Time complexity : O(n). We traverse nums[] if size n once only.
// - Space complexity : O(1). Constant extra space is used.
//
var findPeakElement = function(nums) {
  // This loop doens't go to the last elem of nums[] b/c when nums[i] is
  // checked, nums[i+1] is also checked
  for (let i = 0; i < nums.length - 1; i++) {
    if (nums[i] > nums[i + 1])
      return i;
  }

  // This takes care of Case 1 below
  return nums.length - 1;
}

//============================================================
// 觀念: A list of nums has to be "SORTED" to use "Binary Search" !!!

// Appraoch #2: Modified Binary search (Recursive)
// - 注意: LeetCode Std Sol's explanation is unclear.
// -- Trace the code using my ex's here to understand 3 scenarios
//    (Scenerio #1) Asc      [1,2,3]
//    (Scenerio #2) Desc     [3,2,1]
//    (Scenerio #3) Asc+Desc [2,3,4,5,4,3]
//
// - Time complexity : O(log2(n))
// - Space complexity: O(log2(n))
//
// - Time complexity : O(log2(n))
// -- We reduce the search space in half at every step.
// - Space complexity : O(log2(n))
// -- We reduce the search space in half at every step. Thus, the total search
//    space will be consumed in log2(n) steps. Thus, the depth of recursion
//    tree will go upto log2(n).
//
function findPeakElement(nums) {
  return search(nums, 0, nums.length - 1);
}

function search(nums, l, r) {  // 注意: 這是 Modified Binary Search !!
  // Base Condition: When a peak is found
  if (l === r) return l;

  // Recursive Cases:
  const mid = Math.floor((l + r) / 2);
  if (nums[mid] > nums[mid + 1])
    return search(nums, l, mid);

  return search(nums, mid + 1, r);
}

//-----------------------------------------------------

// Approach #3: Modified Binary search (Iterative)
// - Time complexity : O(log2(n))
// -- We reduce the search space in half at every step. Thus, the total search
//    space will be consumed in log2(n) steps.
// - Space complexity: O(1)
// -- Constant extra space is used.
//
function findPeakElement(nums) {  // 注意: 這是 Modified Binary Search !!
  let l = 0, r = nums.length - 1;

  // THis is the condition when a peak is found
  while (l < r) {
    const mid = Math.floor((l + r) / 2);

    if (nums[mid] > nums[mid + 1]) {
      r = mid;
    } else {
      l = mid + 1;
    }
  }
  return l;
}

//---------- Testing ----------

// Case 1: nums[] contains only 1 num or nums[] in ascending order
//let nums = [1];
//let nums = [1,2];
//let nums = [1,2,3];

// Case 2: nums[] in descending order or has a decending order
//let nums = [3,2,1];
let nums = [1,2,3,2];

let res = findPeakElement(nums);
console.log("res:", res);
