'''
167. Two Sum II - Input array is sorted

Solution code:
  https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/discuss/51249/Python-different-solutions-(two-pointer-dictionary-binary-search).

Note:
- return the indexes with an additional 1 because we are manipulating 0-based indexes. The problem requires a 1-indexed sol.
'''

'''
Approach #1: Two Pointers

思路:
- The idea is to have a pointer from the beginning and one from the end.
On each iteration, move the lower pointer to the right if the sum is too small, or the upper pointer down if it is too big.
'''
def twoSum1(self, numbers, target):
    l, r = 0, len(numbers)-1
    while l < r:
        s = numbers[l] + numbers[r]
        if s == target:    return [l+1, r+1]
        elif s < target:   l += 1
        else:              r -= 1

'''
Approach #2:  One-pass Hash/Dictionary
              => Exact same code as 1. Two Sum - Approach #3
'''
def twoSum2(self, numbers, target):
    dct = {}
    for i, num in enumerate(numbers):
        comp = target-num 
        if comp in dct:
            return [dct[comp]+1, i+1]
        dct[num] = i

'''
Appraoch #3: Binary Search: less efficient ===> Not yet learn
'''
def twoSum(self, numbers, target):
    for i in xrange(len(numbers)):
        l, r = i+1, len(numbers)-1
        tmp = target - numbers[i]
        while l <= r:
            mid = l + (r-l)//2
            if numbers[mid] == tmp:
                return [i+1, mid+1]
            elif numbers[mid] < tmp:
                l = mid+1
            else:
                r = mid-1
