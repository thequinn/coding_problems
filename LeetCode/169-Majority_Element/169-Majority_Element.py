'''
169. Majority Element

https://leetcode.com/problems/majority-element/solutions/3676530/3-method-s-beats-100-c-java-python-beginner-friendly/
'''


'''
法1: Sorting
思路:
- Once the array is sorted, the majority element will always be present at index n/2, where n is the size of the array.

'''
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        nums.sort()
        n = len(nums)
        return nums[n//2]


'''
法2: Hash Map

思路:
- In each iteration, incremet the count of each elem.  The elem w/ count larger than floor(nums coount / 2) in the hash map is returned.

'''
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        res = {}
        leng = len(nums)

        for i, e in enumerate(nums):
            res[e] = res.get(e, 0) + 1

            if res[e] > leng//2:  # "//" floor operator
                return e


'''
法5: Divide & Conquer 

'''


'''
法6: Boyer-Moore Majority Voting Algorithm => Optimization: O(1) Space
   

Boyer-Moore Majority Voting Algorithm:
- When processing an elem n.  If the counter is zero, the algo stores n as the majority elem. Next, it compares n to the stored elem, and either incr the counter (if they are equal) or decr the counter (otherwise). At the end of this process, if the sequence hasa majority, it will be the elem stored by the algo.


Comprehension: => from 229. Majority Element II
- https://leetcode.com/problems/majority-element-ii/solutions/63520/boyer-moore-majority-vote-algorithm-and-my-elaboration/
- The essential concepts is you keep a counter for the majority number X. If you find a number Y that is not X, the current counter should deduce 1. The reason is that if there is 5 X and 4 Y, there would be one (5-4) more X than Y. This could be explained as "4 X being paired out by 4 Y".


Explanation & Sol Code:

(1) Fisher Coder: 
    - https://www.youtube.com/watch?v=M1IL4hz0QrE
    - Explain the theory well, but to use an example to comprehend the theory 
      better, see the comment in NeetCode's sol below.   

(2) NeetCode:
    - https://youtu.be/7pnhv842keE?t=279
    => See: @6:00, ex.  Also put the ex here:

    ex. nums = [1,2,2,3,3,3,3] (using a sorted arr to explain the theory)
          
        From the ex, we see that the majority num is 3.  The count of 3 is more
        than half of the arr.  Thus it's enough to cancel out the count of all 
        diff nums.  
        

Time Complexity: O(n)
- Boyer-Moore performs constant work exactly n times, so the algorithm runs
   in linear time.

Space Complexity: O(1)
- Boyer-Moore allocates only constant additional memory.


'''
class Solution:
    def majorityElement(self, nums:List[int]) -> int:
        candidate = 0 
        count = 0
        
        for n in nums:
            if count == 0:
                candidate = n

            if n == candidate:
                count += 1
            else:
                count -= 1
                
        return candidate

