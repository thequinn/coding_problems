/*
169. Majority Element

Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

LeetCode Std Sol:
- https://leetcode.com/problems/majority-element/solution/
-- Only use Approach #2 #5 #6

Approach #2: Hash Map
Approach #5: Divide & Conquer ==> 沒時間可以跳過, 不需在easy問題學太多種解法!!
Approach #6: Boyer-Moore Voting Algorithm

*/

// Approach #2: Hash Table ==> By Quinn
//
// Time Complexity: O(n)
// - We iterate over nums once and make a constant time HashMap insertion on
//   each iteration. Therefore, the algorithm runs in O(n)O(n) time.
//
// Space Complexity: O(n)
// - An arbitrary array of length n can contain n distinct values, but nums is
//   guaranteed to contain a majority element, which will occupy (at minimum)
//   floor(n/2)+1.  So it occupies O(n) space.
//
const majorityElement = function(nums) {
  let count = 0;  // stores the num w/ highest count
  let maxIndex = 0;

  let hash = {};

  for (let i = 0; i < nums.length; i++) {

    // If nums[i] already exists as a key in hash{}, increment its count
    // Else, add it as a new key and set its val to 1.
    //  |
    //  ==> This can be replaced by ln-40,  // 重要技巧!!
    if (! hash[nums[i]])
      hash[nums[i]] = 1;
    else
      hash[nums[i]]++;

    if (count < hash[nums[i]]) {
      count = hash[nums[i]];
      maxIndex = i;
    }
  }

  return nums[maxIndex];
};


// Approach #2-2: Hash Table ==> Improved Approach #2-1
// - https://leetcode.com/problems/majority-element/discuss/51608/Javascript-solution-(without-sort)
//
var majorityElement = function(nums) {
  var obj = {};

  for(var i = 0; i < nums.length; i++){

    // If nums[i] already exists as a key in hash{}, increment its count
    // Else, add it as a new key and set its val to 1.
    //
    obj[nums[i]] = obj[nums[i]] + 1 || 1;  // 重要技巧!!

    if (obj[nums[i]] > nums.length / 2)
      return nums[i];
  }
};

//---------- Testing ----------
let nums =  [2,2,1,1,1,2,2];
let res = majorityElement(nums);
console.log("res:", res);
