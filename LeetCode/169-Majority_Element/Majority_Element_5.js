/*
169. Majority Element

Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

LeetCode Std Sol:
- https://leetcode.com/problems/majority-element/solution/
-- Only use Approach #2 #5 #6

Approach #2: Hash Map
Approach #5: Divide & Conquer  ==> 沒時間可以跳過, 不需在easy問題學太多種解法!!
Approach #6: Boyer-Moore Voting Algorithm

*/

// Approach #5: Divide & Conquer
// - 思路:
// -- https://www.youtube.com/watch?v=JHlOll9e_ic
//
// Time Complexity: O(nlogn)
// - 這有關 Master Theorem.  看LeetCode Std Sol 的解釋.
//
// Space Complexity: O(nlogn)
// -
//
// - Although the divide & conquer does not explicitly allocate any additional
//   memory, it uses a non-constant amount of additional memory in stack frames
//   due to recursion.  Because the algorithm "cuts" the array in half at each
//   level of recursion, it follows that there can only be O(lgn) "cuts" before
//   the base case of 1 is reached.
//
//   It follows from this fact that the resulting recursion tree is balanced,
//   and therefore all paths from the root to a leaf are of length
//   O(lgn). Because the recursion tree is traversed in a depth-first manner,
//   the space complexity is therefore equivalent to the length of the longest
//   path, which is O(lgn).
//
const majorityElement = function(nums, lo = 0, hi = null) {

  const helper = function(lo, hi) {
    //console.log("lo:", lo, ", hi:", hi);

    //----->Base Case:
    if (lo == hi)   return nums[lo];

    //----->Recursive Case:
    // - Using Divide & Conquer Algorithm to recurse on left and right halves
    //   of this slice.
    let mid = lo + Math.floor( (hi-lo)/2 );
    //console.log("mid:", mid);
    let left  = helper(lo   , mid);
    let right = helper(mid+1, hi );
    //console.log("left:", left, ", right:", right);


    // If the two halves agree on the majority element, return it.
    if (left == right)    return left;
    //
    // Otherwise, count each element and return the "winner".
    let leftCount = 0, rightCount = 0;

    //for (let i = 0; i <= nums.length - 1; i++) {  // WRONG!!
    for (let i = lo; i <= hi; i++) {
      if (nums[i] == left )  leftCount++;
      if (nums[i] == right)  rightCount++;
    }
    return leftCount > rightCount ? left : right;
  };

  return helper(0, nums.length - 1);
};

//---------- Testing ----------
//let nums =  [2,2,1,1,1,2,2];
let nums = [6,5,5];

let res = majorityElement(nums);
console.log("res:", res);
