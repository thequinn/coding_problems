/*
169. Majority Element

Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

LeetCode Std Sol:
- https://leetcode.com/problems/majority-element/solution/
-- Only use Approach #2 #5 #6

Approach #2: Hash Map
Approach #5: Divide & Conquer
Approach #6: Boyer-Moore Voting Algorithm

*/

/*
法6: Boyer-Moore Majority Voting Algorithm => Optimization: O(1) Space
   

Boyer-Moore Majority Voting Algorithm:
- When processing an elem n.  If the counter is zero, the algo stores n as the majority elem. Next, it compares n to the stored elem, and either incr the counter (if they are equal) or decr the counter (otherwise). At the end of this process, if the sequence hasa majority, it will be the elem stored by the algo.


Explanation & Sol Code:

(1) Fisher Coder: 
    - https://www.youtube.com/watch?v=M1IL4hz0QrE
    - Explain the theory well, but to use an example to comprehend the theory 
      better, see the comment in NeetCode's sol below.   

(2) NeetCode:
    - https://youtu.be/7pnhv842keE?t=279
    => See: @6:00, ex.  Also put the ex here:

    ex. nums = [1,2,2,3,3,3,3] (using a sorted arr to explain the theory)
          
        From the ex, we see that the majority num is 3.  The count of 3 is more
        than half of the arr.  Thus it's enough to cancel out the count of all 
        diff nums.  
        

Time Complexity: O(n)
- Boyer-Moore performs constant work exactly n times, so the algorithm runs
   in linear time.

Space Complexity: O(1)
- Boyer-Moore allocates only constant additional memory.
*/
const majorityElement = function(nums) {
  let count = 0;
  let candidate = null;

  for (num of nums) {
    if (count == 0)
      candidate = num;

    if (num == candidate)
      count++;
    else
      count--;
  }

  return candidate;
};

//---------- Testing ----------
let nums =  [2,2,1,1,1,2,2];
let res = majorityElement(nums);
console.log("res:", res);
