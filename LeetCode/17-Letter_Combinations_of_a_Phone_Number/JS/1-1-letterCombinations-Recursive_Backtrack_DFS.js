/*
17. Letter Combinations of a Phone Number

Tip:
(1) 先畫畫 Tree 再 Code
    - 而且好向面試官解釋思路, 還有 Analysis (Time & Space)
(2) 原來 Backtracking 就是 DFS !!!


解題思路 Video (Also in my cloud): --> 必看!!
- https://www.youtube.com/watch?v=uMmFXWs_ZMY

Analysis (Time & Space): @22:20 in link above
- Time Complexity:  O(4^n)
- Space Complexity: O(n).
-- Depending on the call stack.  Levels of a call stack depends on the length
   of the input string (var digits in code below).  The tree graph also
   reflects the levels of the call stack.


Solution Code:
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/discuss/394663/Javascript-solution-44-ms-with-2-variation-easy-to-understand-using-backtrack
- 注意!!
-- 這個連結提供兩個方法, 但比較 ln-44~46 和 ln-79 可學到兩種不同的 Backtracking 寫法!!

*/

// Method #1:
function letterCombinations(digits) {
  if (!digits.length) return [];

  const dictionary = {
    2: ['a', 'b', 'c'],
    3: ['d','e','f'],
    4: ['g', 'h', 'i'],
    5: ['j', 'k', 'l'],
    6: ['m', 'n', 'o'],
    7: ['p', 'q', 'r', 's'],
    8: ['t', 'u', 'v'],
    9: ['w', 'x', 'y', 'z'],
  }
  let results = [];

  combineLetters(0, "");

  return results;

  function combineLetters(index, partial) {
    // Base Case:
    if (index === digits.length) {
      results.push(partial);
      return;
    }

    // Recursive Case:
    for (let j = 0; j < dictionary[digits[index]].length; j++) {
      partial += dictionary[digits[index]][j];
      combineLetters(index + 1, partial);
      // Current level is done, backtrack to last level
      partial = partial.substring(0, partial.length - 1);
    }
  }
}

//----------------------------------------
// Method #2: (more concise, but slower)
function letterCombinations(digits) {
  if (!digits.length) return [];

  const dictionary = {
    2: ['a', 'b', 'c'],
    3: ['d','e','f'],
    4: ['g', 'h', 'i'],
    5: ['j', 'k', 'l'],
    6: ['m', 'n', 'o'],
    7: ['p', 'q', 'r', 's'],
    8: ['t', 'u', 'v'],
    9: ['w', 'x', 'y', 'z'],
  }
  let results = [];

  combineLetters(0, "");

  return results;

  function combineLetters(index, partial) {
    // Base Case:
    if (index === digits.length) {
      results.push(partial);
      return;
    }

    // Recursive Case:
    for (let j = 0; j < dictionary[digits[index]].length; j++) {
      // - This method already include backtracking
      //
      // - DFS Trick!!!
      // -- idx + 1: go down 1 level (See video explanation @18:00)
      //    ex. input: '23', if curr level is '2', next level is '3'
      //
      combineLetters(index + 1, partial.concat(dictionary[digits[index]][j]);
    }
  }
}

//---------- Testing ----------

// To generate dictionary on the fly, use below
/*const dictionary = generateDictionary();

function generateDictionary() {
  let dictionary = {};
  let startCharCode = 97;

  for (let i = 2; i <= 9; i++) {
    let temp = [];
    for (let j = 0; j < lengthDecider(i); j++) {
      temp.push(String.fromCharCode(startCharCode + j));
    }
    dictionary[i] = temp;
    startCharCode += lengthDecider(i);
  }
  return dictionary;

  function lengthDecider(number) {
    switch (number) {
      case 7:
      case 9:
        return 4;
      default:
        return 3;
    }
  }
}*/

let digits = "23"
let res = letterCombinations(digits)
console.log(res)
