/*
17. Letter Combinations of a Phone Number

Solution:
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/discuss/8348/DFS-JavaScript-solution

*/

// Method #1:  Backtracking + Stack (push, pop)
function letterCombinations(digits) {
  var map = [
    '0',
    '1',
    'abc',
    'def',
    'ghi',
    'jkl',
    'mno',
    'pqrs',
    'tuv',
    'wxyz'
  ];
  var res = [];
  var prefix = [];

  if (digits.length) {
    traverse(0);
  }
  return res;

  function traverse(idx) {
    // Base Case:
    if (idx === digits.length) {
      return res.push(prefix.join(''));
    }

    // Recursive Case:
    var str = map[digits[idx]];
    for (var i = 0; i < str.length; i++) {
      prefix.push(str[i]);
      traverse(idx + 1);
      prefix.pop();
    }
  }
}

//---------- Testing ----------
let input = "23";
let res = letterCombinations(input);
console.log("res:", res);
