/*
17. Letter Combinations of a Phone Number

Tips:
- 必須先畫出 Tree, 會讓思緒清楚!

思路:
- This iterative method uses BFS. So it goes in level-order.

Solution:
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/discuss/8243/Javascript-iterative-solution

*/

var letterCombinations = function(digits) {
  var array = ["abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"];
  var final = [];

  for (var i = 0; i < digits.length; i++){
    console.log("\ni:", i);

    var curr = parseInt(digits.charAt(i));

    // array[]'s 1st elem is digit 2 on phone pad
    var letters = array[curr - 2];

    final = backtracking(final, letters);
  }
  return final;
};

var backtracking = function(final, letters){
  var tempfinal = [];

  console.log("final:", final, "\nletters:", letters);

  if (final.length === 0){
    for(var q = 0; q < letters.length; q++){
      tempfinal.push(letters.charAt(q));
    }
    console.log("ln-41, tempfinal:", tempfinal);
  }

  for (var j = 0; j < final.length; j++){
    for (var p = 0; p < letters.length; p++){
      tempfinal.push(final[j] + letters.charAt(p));
    }
  }
  console.log("ln-49, tempfinal:", tempfinal);

  return tempfinal;
};

//---------- Testing ----------
//let input = "23";
let input = "234";

let res = letterCombinations(input);
console.log("res:", res);
