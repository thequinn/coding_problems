/*
17. Letter Combinations of a Phone Number

Tips:
- 必須先畫出 Tree, 會讓思緒清楚!

思路:
- If you are fimilar w/ how queue works to form BSF, tracing the code will be more clear to understand the 思- If you are fimilar w/ how queue works to form BSF, tracing the code will be more clear to understand the 思路.

Solution:
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/discuss/153775/Javascript-BFS-(queue-method)
*/

// Method #1: Iterative + Queue (push, shift)
var letterCombinations = function(digits) {
  let queue = [];

  let hash = {
    "2": "abc",
    "3": "def",
    "4": "ghi",
    "5": "jkl",
    "6": "mno",
    "7": "pqrs",
    "8": "tuv",
    "9": "wxyz"
  };

  if (digits.length <= 0)   return queue;

  // To init the queueing process, push 1st node into queue.
  // - Note:
  // -- For ex. digits = "23", The 1st digit maps to "2": "abc".  So we
  //    actually enqueue 3 elem's.
  let ch = digits.charAt(0);
  for (var i = 0; i < hash[ch].length; i++) {
    queue.push(hash[ch].charAt(i));
  }
  console.log("ln-39, queue:", queue);

  // Track index of the input str, digits
  let len = 1;
  // Needs the condition, len < digits.length, to prevent index out of bound
  // in str digits
  while(queue.length > 0 && len < digits.length) {
    console.log("\n------>len:", len);

    let k = queue.length;
    while (k > 0) {
      let node = queue.shift();
      console.log("--->node:", node);

      let ch2 = digits.charAt(len);
      console.log("ch2:", ch2);
      for (var i = 0; i < hash[ch2].length; i++) {
        queue.push(node + hash[ch2].charAt(i));
      }
      console.log("ln-57, queue:", queue);

      k--;
    }

    len++;
  } // End of outer while

  return queue;
};

//---------- Testing ----------
//let input = "23";
let input = "234";

let res = letterCombinations(input);
console.log("\nres:", res);
