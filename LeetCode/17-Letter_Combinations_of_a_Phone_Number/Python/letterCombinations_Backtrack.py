"""
17. Letter Combinations of a Phone Number

"""

"""
Method #1:

LeetCode Premium Solution
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/solution/
"""
from typing import List
class Solution:
  def letterCombinations(self, digits: str) -> List[str]:
    if len(digits) == 0:    return []

    # Mapping
    letters = {"2": "abc", "3": "def", "4": "ghi", "5": "jkl",
               "6": "mno", "7": "pqrs", "8": "tuv", "9": "wxyz"}

    def backtrack(index, path):
      # Base Case:
      # - If the path is the same length as digits, we have a complete combo
      if len(path) == len(digits):
        combinations.append("".join(path))
        return # Backtrack

      # Recursive Case:
      # - Get the letters that the current digit maps to, and loop through them
      possible_letters = letters[ digits[index] ]
      print("digits[", index, "]:", digits[index], ", possible_letters:", possible_letters)
      #
      for letter in possible_letters:
        path.append(letter)
        backtrack(index + 1, path) # Move on to the next digit
        path.pop()

    combinations = []
    backtrack(0, [])
    return combinations

#--------------------------------------------------------------------
"""
Method #2: (More concise)
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/solution/
"""
import itertools as it
class Solution_2:
    def letterCombinations(self, digits: str) -> List[str]:
        m = {
            '2': 'abc',
            '3': 'def',
            '4': 'ghi',
            '5': 'jkl',
            '6': 'mno',
            '7': 'pqrs',
            '8': 'tuv',
            '9': 'wxyz', }

        possibilities = [m[xx] for xx in digits if not xx in ['0', '1']]
        return [''.join(xx) for xx in it.product(*possibilities) if xx]

#--------------------------------------------------------------------
'''
Method #3: Iterative Approach
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/discuss/326604/My-non-recursive-and-simple-python-solution
'''
class Solution:
  def letterCombinations(self, digits: str) -> List[str]:
    if not digits: return []
    digit_map = {'2': 'abc', '3': 'def', '4': 'ghi', '5': 'jkl', '6': 'mno',
                 '7': 'pqrs', '8': 'tuv', '9': 'wxyz'}
    result = ['']
    for idx in range(len(digits)):
      result = [prev + l for prev in result for l in digit_map[digits[idx]]]
    return result

#---------- Testing ----------
s = Solution()
digits = "23"

res = s.letterCombinations(digits)
print(res)

