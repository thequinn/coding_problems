
Note:
  Backtracking is DFS !!!


//-------------------------------------

JS version has the explanations

(1)
- DFS: ==> Fundamental !!
  1-1-letterCombinations-Recursive_Backtrack_DFS.js

- DFS using Stack: ==> Fun and easy
  1-2-letterCombinations-Recursive_Backtrack_DFS_w_Stack.js


(2)
- BFS:
  2-1-letterCombinations-Iterative_BFS.js

- BFS using Queue:
  2-2-letterCombinations-Iterative_BFS_w_Queue.js
