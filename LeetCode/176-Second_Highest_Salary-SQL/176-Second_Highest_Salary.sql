# LC Std Sol:
# - https://leetcode.com/problems/second-highest-salary/solution/


# Working Sol
SELECT MAX(Salary) as SecondHighestSalary
FROM Employee
WHERE SALARY NOT IN(SELECT MAX(Salary)
FROM Employee);


# Approach #1-1:
# - This solution will be judged as 'Wrong Answer' if there is no such second highest salary since there might be only one record in this table. To overcome this issue, we can take this as a temp table (See Approach #1-2).
#
SELECT DISTINCT
  Salary AS SecondHighestSalary
FROM
  Employee
ORDER BY SALARY DESC

# LIMIT OFFSET
# - https://www.sqltutorial.org/sql-limit/
# - LIMIT clause : limits the num of rows returned
# - OFFSET clause (optional): skips the offset rows before beginning
#   to return rows
LIMIT 1 OFFSET 1



# Approach #1-2: Using temp table  ---> 不懂.....
SELECT
  (SELECT DISTINCT
    Salary
  FROM
    Employee
  ORDER BY Salary DESC
  LIMIT 1 OFFSET 1) AS SecondHighestSalary
;
