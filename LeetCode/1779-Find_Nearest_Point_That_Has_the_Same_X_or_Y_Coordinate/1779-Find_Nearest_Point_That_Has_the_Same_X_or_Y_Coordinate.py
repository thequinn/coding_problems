'''
1779. Find Nearest Point That Has the Same X or Y Coordinate


'''

# https://leetcode.com/problems/find-nearest-point-that-has-the-same-x-or-y-coordinate/solutions/2028388/python-clean-and-simple/
class Solution:
    def nearestValidPoint(self, x1, y1, points):
        minIdx, minDist = -1, inf

        for i,point in enumerate(points):
            x2, y2 = point
        
            if x1 == x2 or y1 == y2:
                dist = abs(x1-x2) + abs(y1-y2)

                if dist < minDist:
                    minIdx = i
                    minDist = min(dist,minDist)
        return minIdx


# https://leetcode.com/problems/find-nearest-point-that-has-the-same-x-or-y-coordinate/solutions/1229047/python-easy-solution/
class Solution:
    def nearestValidPoint(self, x1, y1, points):
        minDist = math.inf
        ans = -1

        for i in range(len(points)):
            if points[i][0]==x or points[i][1]==y:

                manDist = abs(points[i][0]-x)+abs(points[i][1]-y)a

                if manDist<minDist:
                    ans = i
                    minDist = manDist
        return ans
        
