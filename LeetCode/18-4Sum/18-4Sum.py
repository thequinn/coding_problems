'''
18. 4Sum

Explanation & Sol Code:
- https://www.youtube.com/watch?v=29LH8QeJMzw


Generic Runtime in this type of problem: O(m^n)

Approach #1: Brute Force
- Time complexity: O(m^4), 4 nested for loops

Approach #2: Two Pointer
- Time complexity: O(m^3)
'''
class Solution:
  def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
    if not nums or len(nums) < 4: return []

    res = []
    nums.sort()
    for i in range(len(nums) - 3):
      if i>0 and nums[i] == nums[i-1]:  continue

      for j in range(i+1, len(nums) - 2):
        if j>i+1 and nums[j] == nums[j-1]:  continue

        sumA = nums[i] + nums[j]
        l, r = j+1, len(nums)-1
        #l = j+1
        #r = len(nums)-1

        while l < r:
          sumB = sumA + nums[l] + nums[r]
          if sumB == target:
            res.append( [ nums[i], nums[j], nums[l], nums[r] ] )
            l+=1
            r-=1
            while l < r and nums[l] == nums[l-1]: l+=1
            while l < r and nums[r] == nums[r+1]: r-=1
          elif sumB < target:
            l+=1
          else:
            r-=1
    return res

