'''
189. Rotate Array

- - - - - - - - - - -
YT NeetCode Video:
  https://youtu.be/BHr381Guz3Y?t=96
    - @1:36 explains the index of an element after it's shifted
    - It will help understand the following in the code:

      # Calculate the number of steps we actually need to take
      k = k % len(nums)

- - - - - - - - - - -
Note: 

1. list.reverse()
- Reverse the list in-place.

2. reversed()
- It does NOT reverse the original list, but returns a reverse iterator, which is an obj used to iterate over the sequence in reverse order.  
    - Faster than list.reverse()

'''


# Sol with O(n) extra space since nums[:] is equivalent to creating a new array
'''
Sol Code:
- https://leetcode.com/problems/rotate-array/solutions/895980/2-liner-python-solution-beats-99-8-time/
'''
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        # If k > len of nums
        # ex. nums = [1, 2], k = 5 => output = [2, 1] 
        k = k % len(nums)

        # Syntax:
        # https://stackoverflow.com/questions/509211/how-slicing-in-python-works
        nums[:] = nums[-k:] + nums[:-k]



# - - - - - - - - - - - # - - - - - - - - - - - #
# Not a Sol using O(1) extra space

'''
Sol Code #2-1: => See NeetCode's video and read Sol Code #2-2 first
- https://leetcode.com/problems/rotate-array/solutions/3218810/189-solution-step-by-step-explanation/

Notice:
- reversed(nums[:k]) copy k elements into a new list, and after reverse, it returns a new list.
- See Notes on the top
'''
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        # If k > len of nums
        k = k % len(nums)
    
        # Reverse the entire array in-place
        nums.reverse()
    
        # Reverse the first k elements
        nums[:k] = reversed(nums[:k])
    
        # Reverse the remaining elements
        nums[k:] = reversed(nums[k:])

# - - - - - - - - - - - # - - - - - - - - - - - #
# Sol for in-place with O(1) extra space
'''
Sol Code #2-2: => Two Pointers
- https://leetcode.com/problems/rotate-array/solutions/3218810/189-solution-step-by-step-explanation/
=> Code from commenter: tclay1

- See Notes on the top
'''
class Solution:
   def rotate(self, nums: List[int], k: int) -> None:
       k %= len(nums)

       nums.reverse()

       def inPlaceReverse(start, end):
           nonlocal nums
           while start < end:
               nums[start], nums[end] = nums[end], nums[start]
               start += 1
               end -= 1
       
       inPlaceReverse(0, k-1)
       inPlaceReverse(k, len(nums)-1)


'''
Sol Code #3:
- https://leetcode.com/problems/rotate-array/solutions/1729973/python3-in-place-exaplained/

Note: Use this ex to trace the code
    
    ex. [1,2,3,4,5,6,7], k = 4
'''

class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        l = len(nums)
        k %= l

        # Reverse the whole arr in place
        nums.reverse()  

        # Reverse 1st k elems
        # Note: 
        # - set i from 0 ~ k//2.  k//2 is the mid index of this sub-arr, we only
        #   need to swap left elems w/ right elems up to this point.
        # - left index range: 0 ~ k//2.  right index range depends on change of
        #   i.
        for i in range(k // 2):
            # (備註) left = i, right = (k-1) - i
            nums[i], nums[ (k-1) - i ] = nums[ (k-1) - i ], nums[i]
        
        # Reverse the rest of the elems
        # Note: 
        # - set i from k ~ (l+k)//2.  (l+k)//2 is the mid index of this sub-arr,
        #   we only need to swap left elems w/ right elems up to this pt.
        # - left index range: k ~ (k+l)//2. right index range depends on change
        #   of i.
        for i in range(k, (k+l) // 2):
            # (備註) left = i, right = (l-1) - i + k
            nums[i], nums[ (l-1) - i + k ] = nums[ (l-1) - i + k ], nums[i]


