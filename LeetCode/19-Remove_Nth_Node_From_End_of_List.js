/*
19. Remove Nth Node From End of List

Solution:
- https://github.com/BaffinLee/leetcode-javascript/blob/master/001-100/19.%20Remove%20Nth%20Node%20From%20End%20of%20List.md

  */

// Approach: Two Pointers
// - 注意! 可能会出现删掉 head 的情况!!
//
// - Time complexity : O(n)
// - Space complexity : O(1)
//
var removeNthFromEnd = function(head, n) {
  //console.log("head:", head);

  // - 如果用這一行，對於Test Case: [1], n = 1, 會在 rr.next 和 ll.next
  //   的地方出錯！！所以這題一定要用ln-22~23
  // var h = head;
  //
  var h = new ListNode(null);  //same as: var start = new ListNode(0);
  h.next = head;


  // 两个指针 a, b，初始都指向开头
  var ll = h, b = h;

  
  // 把 b 指针往后移动，直到两指针的位置相差 n。 这时候因为两指针的位置相差
  // n，a 指针的位置就是从后面数第 n+1 个
  for (var  i = 0; i < n+1; i++) {
    b = b.next;
  }

  // 同时移动两指针，直到 b 指针到了最后
  while (b !== null) {
    ll = ll.next;
    b= b.next;
  }
  // 在 a 指针那里删掉后面一个，即第 n 个
  ll.next = ll.next.next;
  
  return h.next;
 };

// Test
[1], n = 1;
[1,2,3,4,5], n = 2;

