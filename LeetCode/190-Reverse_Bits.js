/*
190. Reverse Bits

Follow up:
- If this function is called many times, how would you optimize it?


Solution:

(1) 思路:
-- https://leetcode.com/problems/reverse-bits/discuss/54738/Sharing-my-2ms-Java-Solution-with-Explanation

--s1. (把最後 1bit 由後向前挪) 
    Take right-most bit of n to add to ans.  Shift ans left by 1 bit 
--s2. Drop the last bit of n

(2) JS Code:
- https://leetcode.com/problems/reverse-bits/discuss/54754/JavaScript-bit-solution-(139ms)-greatergreatergreater-matters-(explained)

*/


var reverseBits = function(n) {
  var ans = 0;

  for (let i = 0; i < 32; i += 1) {

    // If the last digit of input n is 1, we add 1 to ans.
    // - n & 1      : get the right-most digit of n (ex. 0011 & 0001)
    // - ans |= x: ans += x
    ans |= n & 1;

    if (i < 31) {
      ans = ans << 1; // << 1: *2;
      n = n >> 1;    // >> 1: /2;
    }
  }

  // >> : arithmetic shift right
  // >>>: logical shift right
  //
  // In an arithmetic shift, the sign bit is extended to preserve the
  // signedness of the number. But 11111111111111111111111111111101, 
  // JS will give a negative num, so we need >>>
  //
  return ans >>> 0;
};


let n = 11111111111111111111111111111101;
let ans = reverseBits(n);
console.log("n:", ans.toString(2));
