/*
191. Number of 1 Bits

Follow up:
- If this function is called many times, how would you optimize it?


LeetCode Std Solution:
- https://leetcode.com/problems/number-of-1-bits/solution/

*/

// Approach #1: Loop and Flip
// - 思路: Read LeetCOde Std Solution
//
// - Time complexity: O(1)
// -- The run time depends on the number of bits in n. Because n in this
//    piece of code is a 32-bit integer, and n doesn't make the for-loop
//    bigger (see: i < 32), the time complexity is O(1).
// ===> 不懂的話參考: 
//      :whttps://stackoverflow.com/questions/58353499/not-sure-what-space-complexity-my-map-object-uses-in-the-example/58354429#58354429
//
// - Space complexity: O(1)
// -- The space complexity is O(1), since no additional space is allocated.
//
/*var hammingWeight = function(n) {
  let bits = 0, mask = 1;

  for (let i = 0; i < 32; i++) {
    if ((n & mask) != 0) {
      bits++;
    }
    mask <<= 1;
  }
  return bits;
};*/

// Approach #2: Bit Manipulation Trick
// - 思路: Read LeetCOde Std Solution
//
// - Time complexity: O(1) 
// -- The run time depends on the number of 1-bits in n. In the worst case,
//    all bits in n are 1-bits. In case of a 32-bit integer, the run time is
//    O(1).
// - Space complexity: O(1), since no additional space is allocated.
//
var hammingWeight = function(n) {
  let sum = 0;

  while (n != 0) {
    //console.log("n:", n, ", n-1:", n-1);
    sum++;
    n &= (n - 1);
  }
  return sum;
}

// Approach #3
// - https://leetcode.com/problems/number-of-1-bits/discuss/55097/JavaScript-1-Line
//
var hammingWeight = function(n) {
    return n.toString(2).split("0").join("").length;
};

let n = 0b00000000000000000000000000001011; // this binary = int 11
//console.log("n: ", n);
console.log(hammingWeight(n));
