/*
198. House Robber

解釋 + 解答:
- https://leetcode.com/problems/house-robber/discuss/156523/From-good-to-great.-How-to-approach-most-of-DP-problems.
 |
 ===>以上連結的解法, Step 4. Iterative + dp[] 是根據以下連結改進

- https://leetcode.com/problems/house-robber/discuss/189381/Java-DP-two-ways-(memoization-and-bottom-up)

//--------------------------

思路:
- This particular problem and most of others can be approached using the following sequence:

    s1. Find recursive relation

    s2. Recursive (top-down)
    s3. Recursive + memo[] (top-down)
    s4. Iterative + dp[] (bottom-up)
    s5. Iterative + N variables (bottom-up optimization)


Step 1. Figure out recurrence relationships
- See comments in  ln-1~47 in 198-House_Robber-1.js

Bonus:
  We choose the base case as f(–1) = f(0) = 0, which will greatly simplify our
  code as you can see.
*/

/*
Step 2. Recursive (top-down) [Time Limit Exceeded]
- Converting the recurrence relationship from Step 1.
*/

var rob = function(nums) {
    return helper(nums, nums.length - 1);
};

var helper = function(nums, i) {
  if (i < 0)   return 0;
  return Math.max( helper(nums, i - 2) + nums[i], helper(nums, i - 1) );
}


/*
Step 3. Recursive + memo[] (top-down)

幫助:
- https://zxi.mytechroad.com/blog/dynamic-programming/leetcode-198-house-robber/

分析:
- Look at the curr house and "look 2 houses back" thinking how the max amount of money up until now is actually a function of the max amont of money I was able to get.

- 从n层向下考虑,从大化小,每个case的考量范围缩小到: n-2, n-1, n (ex. 1,2,3), 用此思路向下延伸推得各層

Time Complexity : O(n)
Space Complexity: O(n)
*/
var rob = function(nums) {

  //let memo = {0: -1, 0: -1};  // Same as next 2 lns
  let memo = Array(nums.length);
  memo.fill(-1);

  // start from last house
  return helper(nums, nums.length - 1, memo);
};

var helper = function(nums, i, memo) {
  // Base case: --> 別忘了!!!
  if (i < 0)    return 0;

  // Memoization Step:
  // - If this step is already calculated, return the val directly
  if (memo[i] >= 0)   return memo[i];

  return memo[i] = Math.max(
    helper(nums, i - 2, memo) + nums[i],
    helper(nums, i - 1, memo)
  );
};


/*
Step 4. Iterative + dp[] (bottom-up)
- Let's get rid of the recursive stack in Step 3. to improve space complexity

幫助:
- https://zxi.mytechroad.com/blog/dynamic-programming/leetcode-198-house-robber/

分析:
- think about the first and the second house and then try to calc the max
   amount of money I was able to make up until the third house based on what I
   know so far.
- 代用 Step 1. 的公式, dp[i] = max( dp[i-2] + money[i], dp[i-1])

- Bottom-Up 的思路则和 Top-Down 相反。解决每一层向上的子问题。从第1层和第2
层的base case，可以向上推得第3层，第4层以及第n层的答案

Time Complexity : O(n)
Space Complexity: O(1).
- B/c we got rid of the recursive stack in Step 3.,  O(n) -> O(1)
*/
const rob = (nums) => {
  if (nums.length == 0) return 0;

  let dp = Array(nums.length);

  dp[0] = nums[0];
  dp[1] = Math.max(nums[0], nums[1]);

  for (let i=2; i < nums.length; i++) {
    dp[i] = Math.max(dp[i-2] + nums[i], dp[i-1]);
  }

  return dp[nums.length - 1];
}


/*
Step 5. Iterative + 2 variables (bottom-up)
- In Step 4., we use only dp[i] and dp[i-1], so going just 2 steps back. We can hold them in 2 variables instead. This optimization is met in Fibonacci sequence creation and some other problems.
*/

/* the order is: prev2, prev1, num  */
const rob = (nums) => {
  if (nums.length == 0) return 0;

  let prev1 = 0;
  let prev2 = 0;

  for (let num of nums) {
    let tmp = prev1;
    prev1 = Math.max(prev2 + num, prev1);
    prev2 = tmp;
  }

  return prev1;
};


//let nums = [1,2,3,1];
let nums = [2,7,9,3,1];
console.log(rob(nums));




