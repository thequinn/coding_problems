'''
198. House Robber

LC Premium Sol => It had direct sol
- https://leetcode.com/problems/house-robber/solution/
+
=> 補充 n = 4, 來完成以下 formula

>opt-1: Analyze from back to front of the house array
- See analysis below

  ==> DP Recursive Formula #1:
          f(n) = max( nums[n]+f(n-2), f(n-1) )

>opt-2: Analyze from front to end of the house array
- https://www.youtube.com/watch?v=73r3KWiEvyk (Formula @3:33)

  ==> DP Recursive Formula #2:
          f(n) = max( (nums[0])+f[2:n], f[1:n] )


//----------------//----------------//----------------//
技巧: (i)  Approach of this problem is to work on the simplest case first.
      (ii) Figure out recurrence relationships
//----------------//----------------//----------------//



ex. money : 2,1,1,2
------------------------------
    take? : 0,1,1,0 -> invalid
          : 0,1,0,1 -> 3    => case x
          : 1,0,1,0 -> 3    => case y
          : 1,0,0,1 -> 4    => case z

分析:
  Let us denote that:
    f(n) = Largest amount of loot(贓物) you can rob from the 1st n houses.
    money[i] = Amount of money at the ith house.

  Let us look at the cases:
    - n = 1, clearly f(1) = money[1].
    - n = 2, which f(2) = max(money[1], money[2]).

    - n = 3, two options:
      (a) Rob 3rd house, and add its amount to the 1st house's amount.
      (b) Don't rob 3rd house, and stick w/ max amount of 1st two houses.

    注意!! LC Premium Sol 沒 n = 4 case, 但需有它才產生 formula.
    - n = 4, two options:
      (a) Based on case x & z:
          - Rob 4th house, and add max amount up to (n-2)th house, f(n-2)
      (b) Based on case y:
          - Don't rob 4th house, and stick w/ max amount up to (n-1)th houses,
            f(n-1).

    -> You choose the larger of the two options (a)(b) at each step.

  Therefore, we could summarize the formula as following:
      f(n) = max( f(n-2)+money[n], f(n-1) ),   n: cur house index

Bonus:
  We choose the base case as f(–1) = f(0) = 0, which will greatly simplify our
  code as you can see.
'''

# Code @: 10:09 https://www.youtube.com/watch?v=73r3KWiEvyk
def rob(self, nums: List[int]) -> int:
  rob1, rob2 = 0, 0

  #[rob1, rob2, n, n+1, ...]
  for n in nums:
    tmp = max(n + rob1, rob2)
    rob1 = rob2
    rob2 = tmp
  return rob2
