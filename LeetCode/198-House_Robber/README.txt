
Read the method in 198-House_Robber-1.py.  It derived the DP formula:

  f(n) = max( f(n-2) + money[n], f(n-1) )

