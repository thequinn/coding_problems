/*
2. Add Two Numbers
*/

var addTwoNumbers = function(l1, l2) {
  // null node in the beginning node
  var List = new ListNode(0);

  var head = List;
  var sum = 0, carry = 0;

  while (l1! == null || l2! == null || sum > 0) {

    if (l1! == null){
      sum = sum + l1.val;
      l1 = l1.next;
    }

    if (l2! == null){
      sum = sum + l2.val;
      l2 = l2.next;
    }

    if (sum >= 10){
      carry = 1;
      sum = sum - 10;
    }

    head.next = new ListNode(sum);
    head = head.next;

    sum = carry;
    carry = 0;
  }

  // Now the linked list is constructed, it looks like: 0 -> 7 -> 0 -> 8

  // To point to the actual first node, we need this step
  return List.next;
};

