'''
20. Valid Parentheses

Sol Code:
    https://leetcode.com/problems/valid-parentheses/solutions/316753/python-4ms-faster-then-100-with-explanation/

Note:
- Since PY doesn't allow popping an empty list direct, needs to test we are operating on an empty stack before popping it.


Edge Cases:
    #1-1. ']'
    #1-2. '))'

    #2. '()'


思路:
# 1. if it's the left bracket then we append it to the stack
# 2. else if it's the right bracket and
     (a) the stack is empty - meaning no matching left bracket,
        or
     (b) the left bracket doesn't match the right bracket
# 3. finally check if the stack still contains unmatched left bracket

'''
class Solution:
    def isValid(self, s: str) -> bool:
        tbl = {'(':')', '{':'}','[':']'}
        stack = []

        for e in s:
            if e in '([{':
                stack.append(e)

            elif len(stack) == 0 or e != tbl[stack.pop()]:
                return False
            #
            # Same as last 2 lns.  Learn the if-elif technique above1
            # else:
            #     if len(stack) == 0 or e != tbl[stack.pop()]:
            #         return False

        return True


