/*
200. Number of Islands

*/


/*
Appraoch 1: BFS
- https://leetcode.com/problems/number-of-islands/discuss/597981/JavaScript-BFS
*/
const WATER = "0";
const LAND = "1";

function numIslands(grid) {
  let num = 0;
  if (!grid) return num;

  for (let iRow = 0; iRow < grid.length; iRow++) {
    for (let iCol = 0; iCol < grid[0].length; iCol++) {
      if (grid[iRow][iCol] === WATER) continue;

      num++;
      visitIsland(grid, iRow, iCol);
    }
  }

  return num;
}

function visitIsland(grid, iRowInit, iColInit) {
  const queue = [[iRowInit, iColInit]];
  grid[iRowInit][iColInit] = WATER;

  while (queue.length) {
    const queueLength = queue.length;
    for (let i = 0; i < queueLength; i++) {
      const [iRow, iCol] = queue.shift();

      // Add up, right, down, and left coordinates if they are land.

      if (iRow - 1 >= 0 && grid[iRow - 1][iCol] === LAND) {
        grid[iRow - 1][iCol] = WATER;
        queue.push([iRow - 1, iCol]);
      }

      if (iCol + 1 < grid[0].length && grid[iRow][iCol + 1] === LAND) {
        grid[iRow][iCol + 1] = WATER;
        queue.push([iRow, iCol + 1]);
      }

      if (iRow + 1 < grid.length && grid[iRow + 1][iCol] === LAND) {
        grid[iRow + 1][iCol] = WATER;
        queue.push([iRow + 1, iCol]);
      }

      if (iCol - 1 >= 0 && grid[iRow][iCol - 1] === LAND) {
        grid[iRow][iCol - 1] = WATER;
        queue.push([iRow, iCol - 1]);
      }
    }
  }
}


/*
Approach 2: BFS ==> Same as Approach 1, but more concise
- https://leetcode.com/problems/number-of-islands/discuss/480869/JavaScript-BFS
*/
function numIslands(grid) {
  let count = 0;
  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[0].length; j++) {
      if (grid[i][j] === '1') {
        count++;
        color(grid, i, j);
      }
    }
  }
  return count;
}

function color(grid, i, j) {
  let q = [[i, j]];

  while (q.length) {
    let [x, y] = q.pop();

    grid[x][y] = 0;

    // Visit cur cell's neighbors in 4 cirections
    for (let [newX, newY] of [[x + 1, y], [x, y + 1], [x - 1, y], [x, y - 1]]) {

      // If cur neighbor is in bound, and it's a land, add it to queue
      if (newX >= 0 && newX < grid.length && newY >= 0 && newY < grid[0].length && grid[newX][newY] === '1') {
          q.push([newX, newY]);
      }
    }
  }
}
