/*
200. Number of Islands

Sol Code:
- https://leetcode.com/problems/number-of-islands/discuss/716626/JavaScript-Clean-DFS

*/

var numIslands = grid => {

  function dfs(r, c) {
    // Check the bounds of i j, and if the current index is an island
    if (r < 0 || r >= grid.length || c < 0 || c >= grid[0].length || grid[r][c] == '0')
    //
    // LeetCode Prenium Sol check column's boundry.  But it doesn't
    // need to if we use the condition " grid[r][c] != '1' "
    //if (r < 0 || r >= grid.length || grid[r][c] != '1')
      return;

    // Sinking the island: (Mark it as visited in Graph algorithm)
    grid[r][c] = 0;

    dfs(r+1, c); // down
    dfs(r-1, c); // up
    dfs(r, c+1); // right
    dfs(r, c-1); // left
  }


  let islands = 0;

  for (let r = 0; r < grid.length; r++) {
    for (let c = 0; c < grid[0].length; c++) {
      if (grid[r][c] === '1') {
        islands++;
        dfs(r, c);
      }
    }
  }

  return islands;
};
