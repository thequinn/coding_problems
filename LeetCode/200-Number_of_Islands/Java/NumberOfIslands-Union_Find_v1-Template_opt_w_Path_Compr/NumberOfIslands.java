/*
200 Number of Islands

Explanation + Code + "Union-Find Template":
- https://leetcode.com/problems/number-of-islands/discuss/214981/Java-Union_Find-with-Template

===> This link consitis of Part1 and Part2 below, and they have same contents

Part1 (Text):
/Users/asun/Library/Mobile Documents/com~apple~CloudDocs/Algorithms-Anna/Graph(圖)/Union-Find_(Disjoint-Set)_DS

Part2 (Video):
(a) Union Find Intro and its code explanation:
- Video: https://www.youtube.com/watch?v=ibjEGG7ylHk
- Code: See link in (b) Code

(b) Union Find Optimized w/ Path Compression:
- Video: https://www.youtube.com/watch?v=VHRhJWacxis
- Code: https://www.youtube.com/watch?v=KbFlZYCpONw

- Note: Same Union_Find Template with question No.684,
  ex. User Master_Alcy only changed class UnionFind's constructor for this
      questtion and that's it.
*/

public class NumberOfIslands {

  public int numIslands(char[][] grid) {
    int rowMax = grid.length, colMax = grid[0].length;

    if (grid == null || rowMax == 0) // damn '[]'
      return 0;

    UnionFind uf = new UnionFind(grid, rowMax, colMax);

    for (int i = 0; i < rowMax; i++) {
      for (int j = 0; j < colMax; j++) {

        if (grid[i][j] == '1') {
          // Convert grid[][]'s 2D index to parent[]'s 1D index
          int currentIndex = i * colMax + j;

          // Check downward neighbor in grid[][], and try to union them by
          // marking parent[].
          if (i + 1 < rowMax && grid[i + 1][j] == '1')
            uf.union(currentIndex, currentIndex + colMax);
          // Check right neighbor in grid[][], and try to union them by
          // marking parent[]
          if (j + 1 < colMax && grid[i][j + 1] == '1')
            uf.union(currentIndex, currentIndex + 1);
        } // End of if

      } // End of inner for loop
    } // End of outer for loop

    return uf.count();
  }

  //---------- Testing ----------
  public static void main(String args[]) {
    char[][] grid = {
      {'1','1','1','1','0'},
      {'1','1','0','1','0'},
      {'1','1','0','0','0'},
      {'0','0','0','0','0'}
    };

    NumberOfIslands N = new NumberOfIslands();

    int count = N.numIslands(grid);
    System.out.println(count);
  }

}

