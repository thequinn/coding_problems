

/**
* Optimized Weighted quick-union by rank with path compression by halving.
* Constructor: O(N), Union: O(near 1), Find O(near 1)
*/

// (Again, the author only changed its constructor from the Union-Find Template and that's it)
public class UnionFind {
  private int[] parent; // parent of node
  private byte[] rank; // rank of subtree rooted at node, never more than 31
  private int count; // number of components

  // Only modified this constructor
  public UnionFind(char[][] grid, int row, int col) {
    int size = row * col;
    count = 0; // Only care the number of lands
    parent = new int[size];
    rank = new byte[size]; // save some bits

    // Init 1D array, parent[]. Each elem's val is the same as its index.
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < col; j++) {
        if (grid[i][j] == '1') {
          count++; // LAND
        }

        // currentIndex turn Matrix-Index into Array-Index
        int currentIndex = i * col + j;
        parent[currentIndex] = currentIndex;

        rank[currentIndex] = 0;
      }
    }
  }

  public int count() {
    return count;
  }

  private int find(int node) {
    // Find the root of the subset
    while (node != parent[node]) {
      // Path compression (optimized) by halving
      parent[node] = parent[parent[node]];

      node = parent[node];
    }
    return node;
  }

  public void union(int node1, int node2) {
    int root1 = find(node1);
    int root2 = find(node2);
    System.out.println("root1:" + root1 + "; root2:" + root2);

    // If the 2 nodes have the same root (= in same subset)
    if (root1 == root2)   return;

    // Else
    // - make root of smaller rank point to root of larger rank
    if (rank[root1] < rank[root2])
      parent[root1] = root2;
    else if (rank[root1] > rank[root2])
      parent[root2] = root1;
    // - if same rank, attach to root1 and make root1's rank one level larger
    else {
      parent[root2] = root1;
      rank[root1]++;
    }

    count--;
  }
}// End of Optimized UF
