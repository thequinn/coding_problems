class DSU {
  private int[] par = null, rnk = null;
  private int numIsl;

  public DSU(int num) {
    this.par = new int[num];
    this.rnk = new int[num];
    this.numIsl = 0;

    for (int i = 0; i < num; ++i)
      this.par[i] = i;
  }

  public int find(int x) {
    if (this.par[x] != x)
      this.par[x] = this.find(this.par[x]);
    return this.par[x];
  }

  public void union(int x, int y) {
    // Find roots of node x and y
    int xr = this.find(x), yr = this.find(y);

    if (xr == yr)   return;
    else if (this.rnk[xr] < this.rnk[yr])
      this.par[xr] = yr;
    else if (this.rnk[xr] > this.rnk[yr])
      this.par[yr] = xr;
    else {
      this.par[yr] = xr;
      this.rnk[xr]++;
    }
    this.numIsl--;
  }

  public void incNumIsl() {
    this.numIsl++;
  }

  public int getNumIsl() {
    return this.numIsl;
  }
}
