/*
200 Number of Islands

Solution Code:
- https://leetcode.com/problems/number-of-islands/discuss/479692/Intuitive-Disjoint-Set-Union-Find-in-JavaPython3


Union-Find Concept + Code Explanation:
===> The above link consitis of Part1 & Part2 below, and they have same contents

Part1 (Text):
/Users/asun/Library/Mobile Documents/com~apple~CloudDocs/Algorithms-Anna/Graph(圖)/Union-Find_(Disjoint-Set)_DS

Part2 (Video):
(a) Union Find Intro and its code explanation:
- Video: https://www.youtube.com/watch?v=ibjEGG7ylHk
- Code: See link in (b) Code

(b) Union Find Optimized w/ Path Compression:
- Video: https://www.youtube.com/watch?v=VHRhJWacxis
- Code: https://www.youtube.com/watch?v=KbFlZYCpONw

- Note: Same Union_Find Template with question No.684,
  ex. User Master_Alcy only changed class UnionFind's constructor for this
      questtion and that's it.


Analysis
- Time Complexity: O(Nα(N))≈O(N), where N is the number of vertices (and also the number of edges) in the graph, and α is the Inverse-Ackermann function.
- Space Complexity: O(N). The current construction of the graph (embedded in our dsu structure) has at most N nodes.

*/

class NumberOfIslands {
  public int numIslands(char[][] grid) {
    if (grid == null || grid.length == 0 ||
        grid[0] == null || grid[0].length == 0) return 0;

    int r = grid.length, c = grid[0].length;
    DSU dsu = new DSU(r * c);

    // union an island with its adjacent islands
    for (int i = 0; i < r; ++i) {
      for (int j = 0; j < c; ++j) {
        if (grid[i][j] == '1') {

          // add this island first
          dsu.incNumIsl();

          // union 4 adjacent islands if exist
          if (i - 1 >= 0 && grid[i - 1][j] == '1')
            dsu.union((i - 1) * c + j, i * c + j);
          if (i + 1 < r && grid[i + 1][j] == '1')
            dsu.union(i * c + j, (i + 1) * c + j);
          if (j - 1 >= 0 && grid[i][j - 1] == '1')
            dsu.union(i * c + (j - 1), i * c + (j - 1));
          if (j + 1 < c && grid[i][j + 1] == '1')
            dsu.union(i * c + j, i * c + (j + 1));
        }
      }
    }

    return dsu.getNumIsl();
  }

  //---------- Testing ----------
  public static void main(String args[]) {
    char[][] grid = {
      {'1','1','1','1','0'},
      {'1','1','0','1','0'},
      {'1','1','0','0','0'},
      {'0','0','0','0','0'}
    };

    NumberOfIslands N = new NumberOfIslands();

    int count = N.numIslands(grid);
    System.out.println(count);
  }

}

