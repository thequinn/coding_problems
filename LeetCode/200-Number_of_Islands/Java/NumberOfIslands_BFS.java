/*
200. Number of Islands

基本思路: Need to traverse the whole matrix.  So we can use DFS and BFS
 |
 --->Method: BFS (Using Flood Fill Algorithm:]\)

Video Explanation: ==> Tip: 第二部分 Video 還有用 Tree 解釋 !!
- https://www.youtube.com/watch?v=T7IEAYtb2ts

Solution Code in Java: => by alok5 in 1st comment
- https://leetcode.com/problems/number-of-islands/discuss/56338/Java-DFS-and-BFS-solution

*/

import java.lang.System.*;
import java.util.*;

public class NumberOfIslands_BFS {

  public int numIslands(char[][] grid) {
    if (grid.length == 0) {
      return 0;
    }

    int m = grid.length, n = grid[0].length;
    boolean[][] visited = new boolean[m][n];

    Queue<int[]> queue = new LinkedList<>();
    int count = 0;

    for (int i=0; i<m; i++) {
      for (int j=0; j<n; j++) {

        if (grid[i][j] == '1' && !visited[i][j]) {

          // Insert the root to the queue
          //
          // offer(): Inserts the specified element into this queue
          //
          // ex. int[] arr = new int[]{i, j}
          // - The curly braces contain values to populate the array.
          //
          queue.offer(new int[]{i, j});
          // peek(): Retrieve, but not remove, the head of the queue
          int[] tmp = queue.peek();
          System.out.println("tmp[i, j]: " + tmp[0]+ ", " + tmp[1]);

          visited[i][j] = true;

          bfs(grid, queue, visited);

          count++;
        }
      }
    }

    return count;
  }


  // Using [0,0] as center to move in 4 directions:
  // - {right, down, left, up}    順時針較好記憶
  int[][] dirs = {{0,1}, {1,0}, {0, -1}, {-1, 0}};

  private void bfs(char[][] grid, Queue<int[]> queue, boolean[][] visited) {
    int m = grid.length, n = grid[0].length;

    while (!queue.isEmpty()) {
      // poll(): Retrieves and removes the head of this queue
      int[] curr = queue.poll();
      System.out.println("\ncurr[]: " + curr[0]+ ", " + curr[1]);

      // Using curr[0] and curr[1] as center vertex, and move in 4 dirs to
      // check curr's neighboring vertices
      for (int[] dir: dirs) {
        System.out.print("dir[]:" + dir[0] + ", " + dir[1]);
        int x = curr[0] + dir[0];
        int y = curr[1] + dir[1];
        System.out.println(";    x, y:" + x + ", " + y);

        // If the neighboring vertex is out of bound, skip it
        if (x < 0 || x >= m || y < 0 || y >= n ||
            visited[x][y] || grid[x][y] == '0')
          continue;
        // Else, set the neighboring vertex as visited
        visited[x][y] = true;

        // Enqueue this visited neighboring vertex
        queue.offer(new int[]{x, y});
        int[] tmp2 = queue.poll();  // Retrieves (not remove) head of queue
        System.out.println("==>Enqueue: " + tmp2[0]+ ", " + tmp2[1]);
      }
    } // End of outer while-loop
  }


  //---------- Testing ----------
  public static void main(String args[]) {
    char[][] grid = {
      {'1','1','1','1','0'},
      {'1','1','0','1','0'},
      {'1','1','0','0','0'},
      {'0','0','0','0','0'}
    };

    NumberOfIslands_BFS N = new NumberOfIslands_BFS();

    int count = N.numIslands(grid);
    System.out.println(count);
  }

}



