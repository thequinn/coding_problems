/*
200. Number of Islands

基本思路: Need to traverse the whole matrix.  So we can use DFS and BFS.
 |
 --->Method: DFS (Using Flood Fill Algorithm)

// Method #1: DF
- Video Explanation:
  https://www.youtube.com/watch?v=o8S2bO3pmO4
- Solution Code in Java:
  https://github.com/kdn251/interviews/blob/master/company/amazon/NumberOfIslands.java

// Method #2: DFS
- Method #1 and #2 have the same code.  The only diff is that Method #1's sink() returns 1, Method #2's doesn't.

*/

// Method #1: DFS
public class NumberOfIslands_DFS {
  char[][] gridCopy;

  public int numIslands(char[][] grid) {
    // Set grid copy to the current grid
    gridCopy = grid;

    // Initialize number of islands to zero
    int numberOfIslands = 0;

    // Iterate through every index of the grid
    for (int i = 0; i < grid.length; i++) {
      for (int j = 0; j < grid[0].length; j++) {

        //attempt to "sink" the current index of the grid
        numberOfIslands += sink(gridCopy, i, j);
      }
    }

    // Return the total number of islands
    return numberOfIslands;
  }

  // 注意! sink() is actually DFS.
  int sink(char[][] grid, int i, int j) {

    // Check the bounds of i and j, and if the current index is an island
    // - Since sink() is actually a DFS, i is in vertical direction, j is in
    //   horizontal direction.  ex. i < 0: i is outside of index 0
    if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length
        || grid[i][j] == '0') {
      return 0;
    }

    // Sinking the island: (Mark it as visited in Graph algorithm)
    // - Set current index to 0, so we don't revisit it.
    grid[i][j] = '0';

    // Sink all neighbors of current index:
    // - Visit all current index's adjacent neighbors vertically / horizontally
    // -- It involves backtracking
    sink(grid, i + 1, j); // Visit down neighbor
    sink(grid, i - 1, j); // Visit up neighbor  (Backtrack upwards)
    sink(grid, i, j + 1); // Visit right neighbor
    sink(grid, i, j - 1); // Visit left neighbor(Backtrack left)

    // Once we have successfully sunk all the neighbors ln-64~68, all we need
    // to do is to return 1 to account the original island that we were
    // visiting from the caller ln-38.
    return 1;
  }

  //---------- Testing ----------
  public static void main(String args[]) {:w

    char[][] grid = {
      {'1','1','1','1','0'},
      {'1','1','0','1','0'},
      {'1','1','0','0','0'},
      {'0','0','0','0','0'}
    };

    NumberOfIslands_DFS N = new NumberOfIslands_DFS();

    int count = N.numIslands(grid);
    System.out.println(count);
  }

}

//---------------------------------------------------------
// Method #2: DFS
/*public class NumberOfIslands_DFS {

  public int numIslands(char[][] grid) {
    int count=0;
    for (int i=0; i<grid.length; i++)
      for (int j=0; j<grid[0].length; j++){
        if (grid[i][j] == '1'){
          dfsFill(grid, i, j);
          count++;
        }
      }
    return count;
  }

  private void dfsFill(char[][] grid,int i, int j){
    if (i>=0 && j>=0 && i<grid.length && j<grid[0].length && grid[i][j]=='1'){
      grid[i][j]='0';
      dfsFill(grid, i + 1, j);
      dfsFill(grid, i - 1, j);
      dfsFill(grid, i, j + 1);
      dfsFill(grid, i, j - 1);
    }
  }

  //----- Testing -----
  public static void main(String args[]) {
    char[][] grid = {
      {'1','1','1','1','0'},
      {'1','1','0','1','0'},
      {'1','1','0','0','0'},
      {'0','0','0','0','0'}
    };

    NumberOfIslands_DFS N = new NumberOfIslands_DFS();

    int count = N.numIslands(grid);
    System.out.println(count);
  }

}*/

