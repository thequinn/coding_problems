"""
200 Number of Islands

Solution Code:
- https://leetcode.com/problems/number-of-islands/discuss/479692/Intuitive-Disjoint-Set-Union-Find-in-JavaPython3

Union-Find Concept + Code Explanation:
===> The above link consitis of Part1 & Part2 below, and they have same contents

Part1 (Text):
/Users/asun/Library/Mobile Documents/com~apple~CloudDocs/Algorithms-Anna/Graph(圖)/Union-Find_(Disjoint-Set)_DS

Part2 (Video):
(a) Union Find Intro and its code explanation:
- Video: https://www.youtube.com/watch?v=ibjEGG7ylHk
- Code: See link in (b) Code

(b) Union Find Optimized w/ Path Compression:
- Video: https://www.youtube.com/watch?v=VHRhJWacxis
- Code: https://www.youtube.com/watch?v=KbFlZYCpONw

- Note: Same Union_Find Template with question No.684,
  ex. User Master_Alcy only changed class UnionFind's constructor for this
      questtion and that's it.


Analysis
- Time Complexity: O(Nα(N))≈O(N), where N is the number of vertices (and also the number of edges) in the graph, and α is the Inverse-Ackermann function.
- Space Complexity: O(N). The current construction of the graph (embedded in our dsu structure) has at most N nodes.

"""

#from array import *
from typing import List

class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        if grid == None or len(grid) == 0: return 0

        r, c = len(grid), len(grid[0])
        dsu = DSU(r * c)

        # union an island with its adjacent islands
        for i in range(r):
            for j in range(c):
                if int(grid[i][j]) == 1:

                    # add this island first
                    dsu.numIsl += 1

                    # union 4 adjacent islands if exist
                    if i - 1 >= 0 and int(grid[i - 1][j]) == 1:
                        dsu.union((i - 1) * c + j, i * c + j)
                    if i + 1 < r and int(grid[i + 1][j]) == 1:
                        dsu.union(i * c + j, (i + 1) * c + j)
                    if j - 1 >= 0 and int(grid[i][j - 1]) == 1:
                        dsu.union(i * c + (j - 1), i * c + j)
                    if j + 1 < c and int(grid[i][j + 1]) == 1:
                        dsu.union(i * c + j, i * c + (j + 1))

        return dsu.numIsl

class DSU:
    def __init__(self, num):
        self.numIsl = 0
        self.par = list(range(num))
        self.rnk = [0] * num

    def find(self, x):
        if self.par[x] != x:
            self.par[x] = self.find(self.par[x])
        return self.par[x]

    def union(self, x, y):
        xr, yr = self.find(x), self.find(y)
        if xr == yr:    return
        elif self.rnk[xr] < self.rnk[yr]:
            self.par[xr] = yr
        elif self.rnk[xr] > self.rnk[yr]:
            self.par[yr] = xr
        else:
            self.par[yr] = xr
            self.rnk[xr] += 1
        self.numIsl -= 1

#---------- Testing ----------
grid = [
      ['1','1','1','1','0'],
      ['1','1','0','1','0'],
      ['1','1','0','0','0'],
      ['0','0','0','0','0']
    ];
#print(grid)

sol = Solution()
count = sol.numIslands(grid)
print("count: %d" % count)
