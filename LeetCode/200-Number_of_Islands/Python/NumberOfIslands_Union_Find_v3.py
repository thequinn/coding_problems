"""
200: Number of Islands

Solution Code:
- https://leetcode.com/problems/number-of-islands/discuss/56519/Union-Find-in-Python

-------------------------------------------------------
Concepts + Runtime: (Part1 & Part2 have same contents)

Part1 (Text):
/Users/asun/Library/Mobile Documents/com~apple~CloudDocs/Algorithms-Anna/Graph(圖)/Union-Find_(Disjoint-Set)_DS

Part2 (Video):
(a) Union Find Intro and its code explanation:
- Video: https://www.youtube.com/watch?v=ibjEGG7ylHk
- Code: See link in (b) Code

(b) Union Find Optimized w/ Path Compression:
- Video: https://www.youtube.com/watch?v=VHRhJWacxis
- Code: https://www.youtube.com/watch?v=KbFlZYCpONw

"""

from array import *

class Solution(object):

  def numIslands(self, grid):
    if len(grid) == 0: return 0

    row = len(grid); col = len(grid[0])

    # Nested list comprehension
    self.count = sum(grid[i][j]=='1' for i in range(row) for j in range(col))
    print("self.count: %d" % self.count)

    parent = [i for i in range(row * col)]
    print(*parent, sep = ", ")


    def find(x):
      print("parent[x]: %d; x: %d" % (parent[x], x))

      # Find the root of the subset
      if parent[x] != x:
        return find(parent[x])
      return parent[x]


    def union(x,y):
      # Check if x and y are in the same subset
      print("----->union(x, y): (%d, %d)" % (x, y))
      xroot, yroot = find(x), find(y)

      # If x and y in same subset, return
      print("xroot: %d; yroot: %d" % (xroot, yroot))
      if xroot == yroot:    return

      # Else, merge 2 subsets
      # - the merging op could be done based on ranking/size of a subset.
      #   ex. merging a smaller subset to a bigger subset
      parent[xroot] = yroot
      print("parent[xroot] = yroot")
      print(*parent, sep = ", ")

      self.count -= 1
      print("self.count: %d" % self.count)


    for i in range(row):
      for j in range(col):
        print("\n--->i: %d, j: %d" % (i, j))

        if grid[i][j] == '0':   continue

        # Convert grid[][]'s 2D index to parent[]'s 1D index
        index = i*col+j

        # Check right neighbor in grid[][], and try to union them by
        # marking parent[]
        if j < col-1 and grid[i][j+1] == '1':
          union(index, index+1)

        # Check downward neighbor  in grid[][], and try to union them by
        # marking parent[]
        if i < row-1 and grid[i+1][j] == '1':
          union(index, index+col)

    return self.count


#---------- Testing ----------
grid = [
      ['1','1','1','1','0'],
      ['1','1','0','1','0'],
      ['1','1','0','0','0'],
      ['0','0','0','0','0']
    ];
#print(grid)

sol = Solution()
count = sol.numIslands(grid)
print("count: %d" % count)
