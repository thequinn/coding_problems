/*
206. Reverse Linked List

Follow up:
- A linked list can be reversed either iteratively or recursively. Could you implement both?

LeetCode Std Solution:
- https://leetcode.com/problems/reverse-linked-list/solution/
*/

// Approach #1: Iterative
// - 思路:
// -- Assume that we have linked list 1 → 2 → 3 → Ø, we would like to change
//    it to Ø ← 1 ← 2 ← 3.  While you are traversing the list, change the
//    curr node's next ptr to point to its prev elem.
// -- Since a node does not have ref to its prev node, you must store its
//    prev elem beforehand. You also need another ptr to store the next node
//    before changing the ref. Do not forget to return the new head ref at
//    the end!
//
/******************************************
> Template:
  s1. Save next node's ptr
  s2. Reassign curr node's next ptr
  s3. Update curr node and prev node's ptrs

******************************************/
// - Time complexity: O(n)
// -- Assume that n is the list's length, the time complexity is O(n).
// - Space complexity: O(1)
//
var reverseList = function(head) {
  let prev = null;            // Store prev node's ref
  let curr = head;

  while (curr !== null) {
    let nextTmp = curr.next;  // s1.
    curr.next = prev;         // s2.

    /*** 注意!!! 這兩個順序不能改, 否則有錯! ***/
    prev = curr;             // s3.
    curr = nextTmp;
  }
  return prev;
};

// Approach #2: Recursive  ==> 方法不intuitive, 不學!
// - 思路：
// 1. Using recursive call to reach end of LL (Tip: think DSF), and then work backwards
// 2. Hint: curr.next.next = curr
//
// - 思路解釋:
// -- Assume from node n(k+1) to n(m) had been reversed and you are at node
//    n(k):
//
//        n(1) → … → n(k-1) → n(k) → n(k+1) ← … ← n(m)
//
//    We want n(k+1)’s next node to point to n(k):
//
//        n(k).next.next = n(k);
//
// - 易錯： Be very careful that n(1)'s next must point to Ø, otherwies,
//   your linked list has a cycle in it. ==> Test Case: size 2.
//
// - Time complexity: O(n)
// -- Assume that n is the list's length, the time complexity is O(n)
//
// - Space complexity: O(n)
// -- The extra space comes from implicit stack space due to recursion. The
//    recursion could go up to nnn levels deep.
//
var reverseList = function(head) {
 /** Base Case **/
  //
  // WRONG!! 雖然答案通過，最好不用！
  // - (!head) 等同 (head===null && head===undefined)
  //if (!head || !head.next)
  //
  // Correct:
  if (head == null || head.next == null)
    return head;

 /** Recursive Case **/
  // Hint:
  // - revHead (head of reversed LL) always points to the same node.  We just
  //   want to use it to pt to the head of final reversed LL
  let revHead = reverseList(head.next);

  head.next.next = head;
  head.next = null;

  return revHead;
};

