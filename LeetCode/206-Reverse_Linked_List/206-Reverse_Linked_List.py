

'''
Approach #1: Iterative

Video Explanation & Sol:
    https://leetcode.com/problems/reverse-linked-list/solutions/3905957/step-by-step-visualization-of-o-n-solution/

思路:
- Since we can only traverse a Linked List (LL) from the front to the back, we build a reversed LL starting from the back and work our way to the front (Keep adding a new head before an old head).

    ex.  front/head is 1, back/tail is null

         1 -> 2 -> 3 -> 4 -> null


Runtime: O(N)

'''
# - - W/o Comments - - #
class Solution:
  def reverseList(self, head: ListNode) -> ListNode:
    head2 = None
    cur = head

    while cur:
        nxt = cur.next
        cur.next = head2
        head2 = cur
        cur = nxt
    return head2

# - - W/ Comments - - #
class Solution:
  def reverseList(self, head: ListNode) -> ListNode:

    head2 = None  # Ptr as head of reversed LL

    cur = head  # Ptr to trace the end of old LL
    '''
    Note: Using cur to pt to head, so we keep the head's LL intact.  Not
          mandetory, but it's good practice to keep the original list.
    '''

    while cur:

        # Prep for next iter:
        # - we use nxt ptr to save cur.next info
        nxt = cur.next

        #  *** This is the step that creates the reversed LL ***
        # Assign cur node of the old LL as the new head of the reversed LL
        cur.next = head2

        # Prep for next iter:
        # - Make head2 pts to head of reverse LL.
        # - Make cur pts to head of old LL.
        head2 = cur
        cur = nxt

    return head2



# Approach #2: Recursive => 這個解法很爛, 跳過!
class Solution:
  def reverseList(self, head: ListNode) -> ListNode:i

    # 注意! Inner func in Python doesn't need "self" as 1st param
    def helper(cur: ListNode, pre: ListNode) -> ListNode:
      if cur is None:  return pre

      next = cur.next
      cur.next = pre
      # Update pre and cur's ptrs for next call stack
      return helper(next, cur)

    if head == None:  return
    return helper(head, None)
