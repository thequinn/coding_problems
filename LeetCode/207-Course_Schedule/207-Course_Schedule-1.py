'''
Explanation & Sol Code:
    https://leetcode.com/problems/course-schedule/solutions/441722/python-99-time-and-100-space-collection-of-solutions-with-explanation/

'''

# Solution 1: DFS with an array storing 3 different states of a vertex
class Solution:
    # QN mod it for easy memorization
    def buildAdjList(self, n, edgesList):
        adjList = [[] for _ in range(n)]
        # ex. [0, 1]: course 1 is a prerequisite of course 0
        for curCourse, preCourse in edgesList:
            adjList[preCourse].append(curCourse)
        return adjList

    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        # build Adjacency list from Edges list
        adjList = self.buildAdjList(numCourses, prerequisites)

        # Each vertex can have 3 different states:
        # state 0   : vertex is not visited. It's a default state.
        # state -1  : vertex is being processed. Either all of its descendants
        #             are not processed or it's still in the function call stack.
        # state 1   : vertex and all its descendants are processed.
        state = [0] * numCourses

        def hasCycle(v):
            if state[v] == 1:
                # This vertex is processed so we pass.
                return False
            if state[v] == -1:
                # This vertex is being processed and it means we have a cycle.
                return True

            # Set state to -1
            state[v] = -1

            for i in adjList[v]:
                if hasCycle(i):
                    return True

            state[v] = 1
            return False

        # we traverse each vertex using DFS, if we find a cycle, stop and return
        for v in range(numCourses):
            if hasCycle(v):
                return False

        return True


# Solution 2: DFS with a stack storing all decendants being processed
class Solution:
    # QN mod it for easy memorization
    def buildAdjList(self, n, edgesList):
        adjList = [[] for _ in range(n)]
        # ex. [0, 1]: course 1 is a prerequisite of course 0
        for curCourse, preCourse in edgesList:
            adjList[preCourse].append(curCourse)
        return adjList

    def canFinish(self, numCourses: int, prereqs: List[List[int]]) -> bool:
        # build Adjacency list from Edges list
        adjList = self.buildAdjList(numCourses, prereqs)
        visited = set()

        def hasCycle(v, stack):

            if v in visited:

                # This vertex is being processed and we have a cycle
                # ex. (0,1), (1,2), (2,1), (2,3)
                if v in stack:
                    return True

                # This vertex is processed so we pass
                # ex. (0,1), (1,2), (1,3), (2,3)
                return False

            visited.add(v)
            # 注意!! stack is used to detack cycles
            stack.append(v)

            for i in adjList[v]:
                if hasCycle(i, stack):
                    return True

            # once processed, we pop it out of the stack
            stack.pop()
            return False

        # we traverse each vertex using DFS, if we find a cycle, stop and return
        for v in range(numCourses):
            if hasCycle(v, []):
                return False

        return True


# Solution 3-1: BFS + Queue == Khan's Algorithm
class Solution:
    # QN mod it for easy memorization
    def buildAdjList(self, n, edgesList):
        adjList = [[] for _ in range(n)]
        # ex. [0, 1]: course 1 is a prerequisite of course 0
        for curCourse, preCourse in edgesList:
            adjList[preCourse].append(curCourse)
        return adjList

    def topoBFS(self, numNodes, edgesList):
        adjList = self.buildAdjList(numNodes, edgesList)

        # 1. A list stores No. of incoming edges of each vertex
        inDegrees = [0] * numNodes
        for v1, v2 in edgesList:
            # v2v1 form a directed edge
            inDegrees[v1] += 1

        # 2. Enqueue vertices w/ no incoming edge (indegree=0).
        #    It's the start pt of the topo sort.
        #    Vertices in this queue have the same order as the final sort.
        queue = []
        for v in range(numNodes):
             if inDegrees[v] == 0:
                queue.append(v)

        # initialize count of visited vertices
        count = 0
        # an empty list that will contain the final topological order
        topoOrder = []

        while queue:
            v = queue.pop(0) # same as popleft()
            topoOrder.append(v)
            count += 1

            # for each descendant of current vertex, reduce its in-degree by 1
            for des in adjList[v]:
                inDegrees[des] -= 1
                # if in-degree becomes 0, add it to queue
                if inDegrees[des] == 0:
                    queue.append(des)

        if count != numNodes:
            return None  # graph has at least one cycle
        else:
            print("topoOrder:0", topoOrder)
            return topoOrder #===> Can print out topoOrder[]

    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        return True if self.topoBFS(numCourses, prerequisites) else False

'''
# Solution 3-2:  BFS + Queue == Khan's Algorithm  => This version is shorter than Sol 3-1
Sol Code:
https://leetcode.com/problems/course-schedule/solutions/742993/python-bfs-topological-sort/
'''

from collections import defaultdict, deque

class Solution(object):
    def canFinish(self, numCourses, prerequisites):

        # Create an adjacency list
        adjList = defaultdict(list)  # dict of list
        # Create indegree list to track incoming edges for a node
        indegree = defaultdict(int) # dict of int
        for dst, src in prerequisites:
            adjList[src].append(dst)
            indegree[dst] += 1
        print("adjList:", adjList)
        print("indegree:", indegree)

        # Enque nodes that have 0 incoming edges (indegree=0) to start
        # Khan's Algo
        queue = deque()
        for node in range(numCourses):
            if node not in indegree:
                queue.append(node)
        print("queue:", queue)

        # Whenever we pop a source node from queue, the indegree of its
        # destination node (neighbor) is deducted by 1
        count = 0
        topoOrder = []  # Save sorted result
        while queue:
            src = queue.popleft()
            topoOrder.append(src)
            count += 1
            for dst in adjList[src]:  # dst node same as neighbor
                indegree[dst] -= 1
                # If dst node has no incoming edge, add it to queue
                if indegree[dst] == 0:
                    queue.append(dst)

        print("topoOrder:", topoOrder)
        return count == numCourses
