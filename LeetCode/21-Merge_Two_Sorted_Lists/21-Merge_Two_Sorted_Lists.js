/*
21. Merge Two Sorted Lists

*/


// Approach 1: Recursion  --> Not yet

/*
Approach 2: Iteration

Solution:  (Same as LeetCode Premium Sol)
- https://skyyen999.gitbooks.io/-leetcode-with-javascript/content/questions/21md.html
- 思路:
-- 讀取L1目前的值與L2目前的值比較，
     如果L1.val < L2.val，將當前的L1節點加入新的連結串列(result)，然後L1指
     向下一個節點。如果L1.val > L2.val較小，則把L2當前的節點加到result，
   直到L1或L2一方為null則停止比較，並且將另外一邊剩下的節點加入result。
*/
var mergeTwoLists = function(l1, l2) {

  // 儲存結果的ListNode
  var result = new ListNode(0);  // 同: var result = new ListNode(null);

  var cur = result;  // 目前Node位置

  while (l1 !== null && l2 !== null) {
    if (l1.val > l2.val) {  // l2較小,加入result
      cur.next = l2;
      l2 = l2.next;
    } else {                // l1較小的數加入result
      cur.next = l1;
      l1 = l1.next;
    }
    cur = cur.next;
  }

  // 將l1,l2剩下的Node加到result
  if (l1 !== null)    cur.next = l1;
  if (l2 !== null)    cur.next = l2;

  return result.next;
}
