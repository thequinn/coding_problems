'''
21. Merge Two Sorted Lists

LeetCode Premium Sol:
- https://leetcode.com/problems/merge-two-sorted-lists/solution/

'''


'''
Approach 1: Recursion

Video Explanation + Code: --> 非常棒! Trace Stack 解釋
- https://www.youtube.com/watch?v=bdWOmYL5d1g

思路:
- The smaller of the 2 lists' heads plus the result of a merge on the rest of
  the elem's

Recursion Formulas to the Merge Operation:
  a) l1[0] + merge(l1[1:], l2)  l1[0] <= l2[0]
  b) l2[0] + merge(l1, l2[1:])  otherwise


Time complexity : O(n+m)
- exactly one of l1 and l2 is incremented on each loop iteration

Space complexity: O(n+m)
- The first call to mergeTwoLists() does not return until the ends of both l1 and l2 have been reached, so n+m stack frames consume O(n+m) space.
-
'''
class Solution:
  def mergeTwoLists(self, l1, l2):
    # Base Cases:
    if l1 is None:  return l2
    if l2 is None:  return l1

    # Recursive Cases:
    if l1.val <= l2.val:
      l1.next = self.mergeTwoLists(l1.next, l2)
      return l1
    else:
      l2.next = self.mergeTwoLists(l1, l2.next)
      return l2

'''
Approach 2: Iteration

思路:
- Using cur ptr to merge two LL's
  1) cur ptr: 
     - pts to the last node of the LL that's already sorted/merged 
  2) cur.next ptr: 
     - links next node that's smaller of the 2 from l1 & l2 for the merges LL.

Video Explanation + Code:
- https://youtu.be/N8WTaSSivEI


Time complexity : O(n+m)
- exactly one of l1 and l2 is incremented on each loop iteration

Space complexity : O(1)
- The iterative approach only allocates a few pointers, so it has a constant
  overall memory footprint.
'''
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:

    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
      '''
      What I missed !!!
      - 主軸思路:
        1. Creating a node "obj" to start a new LL (use as the head)
        2. Creating a ptr to pt to the head node
      - 細節思路:
        - See # Tips to utilitzing LL
      '''

      # Maintain an unchanging reference to node ahead of the return node.
      head = ListNode(-1)
      cur  = head  # Tips to utilitzing LL:
                   # - Assign cur ptr to mem location of head. So cur.next is
                   #   equivalent to head.next except head ptr doesn't move!

      while l1 and l2:
        if l1.val <= l2.val:
          # .next is used to create the sorted link
          cur.next = l1  
          # Increment pt of l1 after l1 is used
          l1 = l1.next
        else:
          cur.next = l2
          l2 = l2.next
        # Increment pt of cur after cur is used
        cur = cur.next

      # Exactly one of l1 and l2 can be non-null at this point, so connect
      # the non-null list to the end of the merged list.
      #
      # 法一:
      #if l1 is not None:  cur.next = l1
      #else: cur.next = l2
      #
      # 法二:
      cur.next = l1 if l1 is not None else l2    # "is not": check ref
                                                 # "!="    : check vals
      #cur.next = l1 if l1 is not None else cur.next = l2  # 注意! WRONG!!
      #
      # 法三:
      #cur.next = l1 or l2

      return head.next


# Approach 2: Iteration --> No comment version
class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
      head = ListNode(-1)
      cur = head

      while l1 and l2:
        if l1.val <= l2.val:
          cur.next = l1
          l1 = l1.next
        else:
          cur.next = l2
          l2 = l2.next
        cur = cur.next

      cur.next = l1 or l2
      return head.next
