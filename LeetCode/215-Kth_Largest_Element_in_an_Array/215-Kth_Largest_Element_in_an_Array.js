/*
215. Kth Largest Element in an Array


Solution Explaination + Code in Java:
- https://leetcode.com/problems/kth-largest-element-in-an-array/discuss/60294/Solution-explained

//------------------------------------

思路: ==> by modifying Quickselect (Approach #3 & #4):
- https://leetcode.com/problems/kth-largest-element-in-an-array/discuss/60297/my-1ms-Java-solution-beats-100...
||
==> Con't at Approach #3 and #4

*/

// Appraoch #1: JS Array.prototype.sort()
// - https://leetcode.com/problems/kth-largest-element-in-an-array/discuss/60294/Solution-explained
//
// - Time Complexity: O(n log n)
// -- Since it uses MergeSort, it has O(n log n)
//
// - Space Complexity: O(k)
//
var findKthLargest_1 = function(nums, k) {
  nums.sort((a, b) => b - a);
  //console.log("nums:", nums);

  return nums[k - 1];
};



// Approach #2: Heap
// - https://leetcode.com/problems/kth-largest-element-in-an-array/discuss/420314/Quickselect-and-Heap-Solutions-JavaScript
//
// => JS doesn't have a Heap library.  This appraoch is more time-consuming b/c
//    it requires to implement the heap. (Sol see link on the top)


//*****************************************************
/*
思路 by modifying Quickselect for (Appraoch #3 & #4):
- https://leetcode.com/problems/kth-largest-element-in-an-array/discuss/60297/my-1ms-Java-solution-beats-100...

(1) The idea is to use modified quick sort. We know the key point of quick sort is to divide the array into two parts, one is smaller than(or equal to) the pivot value, and the other half bigger than it(or equal).

The modified version is we only sort the part if the kth element is within it. Let's say we know the kth biggest element should appear at the index array[n-k] (where n is the size), and each time when we divide the array into two parts by using the pivot value, we check the range of each part, if index [n-k] is not within the range we won't sort it.


- Time Complexity: Best/Avg: O(n), Worst: O(n^2)
-- In Appraoch #3 To prevent worst runtime, randomly pick a pivot. (ln-97~104)
-- In Approach #4: To increase the chance of dividing the array into two parts of equal length I choose the pivot which is the median among start, end and mid element.

*/

// Approach #3: Quickselect (Recursive) Algorithm => This is the smart approach
// - https://leetcode.com/problems/kth-largest-element-in-an-array/discuss/144410/Share-my-avg.-O(n)-JavaScript-Solution-using-quick-select
//
// 注意: This Quickselect uses Lomuto's Partition Algorithm
//
var findKthLargest_3 = function(nums, k) {
  return quickSelect(nums)

  function quickSelect(nums, lo = 0, hi = nums.length - 1) {
    console.log("\nquickSelect() input nums[]:", nums);

    // Quinn: To ensure user give correct lo and hi  to call quickSelect()
    //if (lo >= hi)   return nums[hi];

    //---------------------------------------------------------
    // Lomuto's Partition

    let pivot = nums[hi];
    let i = lo, j = lo;

    // for every num between lo and hi
    for (; j < hi; j++) {
      // if the current num is less than or equal to the pivot, swap
      if (nums[j] <= pivot) {
        [ nums[i], nums[j] ] = [ nums[j], nums[i] ];  // Swap
        i++
      }
    }

    [ nums[i], nums[j] ] = [ nums[j], nums[i] ]; // Swap
    console.log("nums[]:", nums, ", i:", i, ", j:", j, ", pivot:", pivot);

    // i is the pivot after the above partition process
    //---------------------------------------------------------

    // The question wants the kth largest num in arr, so (nums.length - k) gets
    // the index of it. (Quicksort/Quickselect sorts an arr in ascending order)
    if (i < nums.length - k) {
      console.log("<, nums.length:", nums.length, ", k:", k);
      return quickSelect(nums, i + 1, hi);
    }
    else if (i > nums.length - k) {
      console.log(">, nums.length:", nums.length, ", k:", k);
      return quickSelect(nums, lo, i - 1);
    }
    else { // (i === nums.length - k) {
      console.log("===, nums.length:", nums.length, ", k:", k);
      return nums[i];
    }
  };
};

// Note:
// To make the quick sort faster, usually we can randomly pick a pivot:
/*
const quickSelect = (nums, lo, hi, k) => {
  const p = Math.floor(Math.random() * (hi - lo + 1)) + lo;
  swap(nums, p, hi);
  ...
}
*/

//-----------------------------------

// Approach #4: Quickselect (Recursive) Algorithm => This is the smart approach
//  - https://leetcode.com/problems/kth-largest-element-in-an-array/discuss/60297/my-1ms-Java-solution-beats-100...
//  |
//  ==> 不要用:Although this code is 100% faster, it's harder to memorize!
//

//---------- Testing ----------
let nums = [3,2,1,5,6,4], k = 2;          // Output: 5
//let nums = [3,2,3,1,2,4,5,5,6], k = 4;    // Output: 4
//let nums = [10, 4, 5, 8, 6, 11, 26], k = 3; // Output: 10

//let res = findKthLargest_1(nums, k);
let res = findKthLargest_3(nums, k);
console.log("res:", res);
