"""
215. Kth Largest Element in an Array

Solution Explaination + Code:
    https://leetcode.com/problems/kth-largest-element-in-an-array/solutions/3906260/100-3-approaches-video-heap-quickselect-sorting/?envType=study-plan-v2&envId=top-interview-150

"""

'''
Method 1: Sorting
'''
>class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        # Sort the input arr in ascending order, then get the
        # kth largest num from the back
        return sorted(nums)[-k]


        # See breakdowns from above
        '''
        nums_sorted = sorted(nums)
        print("nums_sorted:", nums_sorted)

        kth_largest = nums_sorted[-k]
        print(kth_largest)

        return kth_largest
        '''


'''
Method 2: Priority Queue => Using Min Heap, which is the default in Python


Characteristics of a Heap:

(1) Partial Order:
    - While a heap ensures that the root is the minimum (or maximum) element, it does not guarantee a sorted order of the entire list. Elements in a heap do not follow a strict linear ordering like in a sorted list.

(2) Complete Binary Tree:
    - Heaps are complete binary trees, which means all levels are fully filled except possibly for the last level, which is filled from left to right.

思路 (Intuition):
- We know the top is always the smallest in the heap.  If we
    (1) maintain k elems in the heap, and
    (2) removing the top when there's an elem larger than the top and append
        the larger elem to end,
  after iterating over the input list, we have the largest k elems in heap.
'''
class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        heap = nums[:k]
        heapq.heapify(heap)
        #print("heap before for-loop:", heap, "\n")

        for num in nums[k:]:
            if num > heap[0]:
                heapq.heappop(heap)
                # heappush() appends the new item to the end of the heap list
                heapq.heappush(heap, num)
            #print("num:", num, ", heap:", heap)
        return heap[0]


'''
Method 3: Divide & Conquer using Quick Select
'''

