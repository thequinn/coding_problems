'''
216. Combination Sum III
'''

'''
Approach #1: DFS (Backtracking)

LC Premium Sol:
- https://leetcode.com/problems/combination-sum-iii/solution/
'''
from typing import List
class Solution:
  def combinationSum3(self, k: int, n: int) -> List[List[int]]:

    def backtrack(remain, comb, next_start):

      if remain == 0 and len(comb) == k:
        print("comb:", comb, "; list(comb):", list(comb))

        '''
        it is important since the list object are essentially passed as reference in Python and Java. If we did not make a deep copy of the object, the combination would be reverted in other branch of the backtracking.
        '''
        #res.append( comb )      # WRONG!!
        res.append( list(comb) ) # CORRECT

        return

      elif remain < 0 or len(comb) == k:
        return

      for i in range(next_start, 9):
        comb.append(i+1)
        backtrack(remain-(i+1) , comb, i+1)
        comb.pop()

    res = []
    backtrack(n, [], 0)
    return res

'''
Approach #2: DFS (Backtracking)
- https://leetcode.com/problems/combination-sum-iii/discuss/60805/Easy-to-understand-Python-solution-(backtracking).
'''
class Solution(object):
  def combinationSum3(self, k, n):
    ret = []

    # range(): returns a sequence of numbers
    #self.dfs( range(1, 10) , k, n, [], ret)      # WRONG!!
    self.dfs( list(range(1, 10)) , k, n, [], ret) # CORRECT

    return ret

  def dfs(self, nums, k, n, path, ret):
    if k == 0 and n == 0:
      ret.append(path)
    if k < 0 or n < 0:
      return

    for i in range(len(nums)):
      print("i:", i, ", nums[i]:", nums[i], ", path[]:", path, ", [nums[i]]:", [nums[i]])
      print("nums[i+1:]:", nums[i+1:], ", k-1:", k-1, ", n-nums[i]:", n-nums[i], ", path+[nums[i]]:", path+[nums[i]], "ret:", ret)
      self.dfs(nums[i+1:], k-1, n-nums[i], path+[nums[i]], ret)
      # ||
      # ====> path+[nums[i]]: ex. [1]+[2] => [1, 2]


#-------Test Cases-------
sol = Solution()
k, n = 3, 7
res = sol.combinationSum3(k, n)
print("res:", res)
