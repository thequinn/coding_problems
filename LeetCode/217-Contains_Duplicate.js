/*
217. Contains Duplicate

LeetCode Std Solution:
- https://leetcode.com/problems/contains-duplicate/solution/
*/

// Approach #1 (Naive Linear Search) [Time Limit Exceeded]

// Approach #2 (Sorting)
// - 思路: If there are any duplicate integers, they will be consecutive
//   after sorting.
//
// - Time complexity : O(nlog⁡n).
// -- Sorting is O(nlog⁡n) and the sweeping is O(n). The entire algorithm is
//    dominated by the sorting step, which is O(nlog⁡n).
// - Space complexity : Space depends on the sorting algorithm, whether
//   auxiliary space is used.
//
var containsDuplicate = function(nums) {
  nums.sort();
  for (let i = 0; i < nums.length - 1; ++i) {
    if (nums[i] == nums[i + 1])
      return true;
  }
  return false;
}

// Approach #3 (Hash Table)
// 思路： Put each elem in hash and update count of each elem showing up
//
// - Time complexity : O(n)
// -- insert and search operation each takes constant time.  We do both
//   operations n times.
// - Space complexity : O(n)
// -- The space used by hash tbl is linear with the number of elems in it.
//
var containsDuplicate = function(nums) {
  let map = {};
  let n = 0;

  for (let i = 0; i < nums.length; i++) {
    n = nums[i];

    if (!map[n])  map[n] = 1;
    else  return true;
  }
  return false;
};

let nums;
nums = [0];         // F
//nums = [1,2,3,1]; // T
//nums = [1,2,3,4]; // F
console.log(containsDuplicate(nums));


