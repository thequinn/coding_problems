'''
217. Contains Duplicate

https://leetcode.com/problems/contains-duplicate/solutions/3672475/4-method-s-c-java-python-beginner-friendly/

'''

'''
Method #1: Sorting

思路:
- sorts the array in order and then checks for adjacent elements that are the same.

Runtime: O(n log n)
- sorting has a time complexity of O(n log n)
'''
class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        nums.sort()
        n = len(nums)

        for i in range(1, n):
            if nums[i] == nums[i-1]:
                return True
        return False

'''
Method #2: Hash Map => Notice! No need to use sort() b/c it slows down exec

思路:
- It uses a hash map to store the elements as keys and their counts as values. If a duplicate element is encountered (count greater than or equal to 1), it returns true.

Runtime: O(n)
'''
class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        seen = {}
        for i, e in enumerate(nums):
            seen[e] = seen.get(e, 0) + 1
            if seen[e] >= 2:
                return True
        return False
