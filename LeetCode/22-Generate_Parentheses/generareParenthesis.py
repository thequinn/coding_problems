"""
22. Generate Parentheses

Using DFS Tree to explain the concept + Runtime: -> 必看!!
    https://www.youtube.com/watch?v=sz1qaKt0KGQ&t=619s

"""


'''
Method #1:
    https://leetcode.com/problems/generate-parentheses/solutions/2542620/python-java-w-explanation-faster-than-96-w-proof-easy-to-understand/


思路:
- The idea is to add ')' only after valid '('.  We use two integer variables left & right to see how many '(' & ')' are in the current string

1. If left < n then we can add '(' to the current string
2. If right < left then we can add ')' to the current string


For n = 2, the recursion tree will be something like this,

								   	(0, 0, '')
								 	    |
									(1, 0, '(')
								   /           \
							(2, 0, '((')      (1, 1, '()')
							   /                 \
						(2, 1, '(()')           (2, 1, '()(')
						   /                       \
					(2, 2, '(())')                (2, 2, '()()')
						      |	                             |
					res.append('(())')             res.append('()()')

'''


'''
Method #2: -> 思路簡單, 但 上面的方法就ok
    https://www.youtube.com/watch?v=sz1qaKt0KGQ&t=619s
'''
class Solution:
    def generateParenthesis(self, n: int) -> List[str]:

        def helper(l, r, stack, ans):
            if l == r == 0:
                output.append(ans)
                return
            if l > 0:
                # Var stack is used to track # of "(" appended so far
                helper(l-1, r, stack+1, ans+"(")
            # If there're r available, and there're extra "(" appended
            if r > 0 and stack > 0:
                helper(l, r-1, stack-1, ans+")")

        output = []
        helper(n, n, 0, "")
        return output



#---------- Testing ----------
n = 2
#n = 3
sol = Solution()
res = sol.generateParenthesis(n)
print("\nresult: ")
print(*res)

