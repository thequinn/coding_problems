/*
222. Count Complete Tree Nodes

*/

// Approach 2: Binary Search
//
var countNodes = function(root) {
  if (!root)  return 0;

  // Calc depth
  var d = 0, cur = root;
  while (cur.left) {
    d++;
    cur = cur.left;
  }

  // If the tree only has 1 node, just return count of 1
  if (d == 0)  return 1;


  //---------------------------------
  var exists = function(idx) {
    var cur = root;
    var left = 0, right = 2**d - 1;
    for (var i = 0; i < d; i++) {
      var mid = ~~((left + right) / 2);
      if (idx <= mid) {
        cur = cur.left
        right = mid;
      }
      else { // if (idx > mid)
        cur = cur.right;
        left = mid;
      }
    }
    return cur != null;
  }
  //---------------------------------


  var l = 1;
  var r = 2**d - 1;  // **: exponential
  while (l <= r) {
    var mid = ~~((l + r) / 2);  // ~~: Faster Math.floor()
    if (exists(mid))  l = mid + 1;
    else  r = mid - 1;
  }
  return (2**d - 1) + l;
}
