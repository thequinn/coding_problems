'''
222. Count Complete Tree Nodes

'''

'''
Approach 1_1: Linear Time

LC Premium Sol:
- https://leetcode.com/problems/count-complete-tree-nodes/solution/

Time Complexity: O(N)
Space Complexity: O(d) = O(logN)
- to keep the recursion stack, where d is a tree depth
'''
class Solution:
  def countNodes(self, root: TreeNode) -> int:
    if not root:    return 0
    return 1 + self.countNodes(root.left) + self.countNodes(root.right)


# Approach 1_2: Same as Approach 1_1, using Ternary (Conditional) Operator
class Solution:
  def countNodes(self, root: TreeNode) -> int:
    return 1 + self.countNodes(root.right) + self.countNodes(root.left) if root else 0


'''
Approach 2: Binary Search

LC Premium Sol:
- https://leetcode.com/problems/count-complete-tree-nodes/solution/

Video: ---> 兩個Binary Search 部分講解得很清楚!
- https://www.youtube.com/watch?v=rvZfvo-r5WU

Time complexity : O(d^2) = O(log^2 N), where d is a tree depth.
Space complexity:O(1)

'''
class Solution:

  # Return tree depth in O(d) time.
  def compute_depth(self, node: TreeNode) -> int:
    d = 0
    while node.left:
      node = node.left
      d += 1
    return d


  def exists(self, idx: int, d: int, node: TreeNode) -> bool:

    # <Binary Search - II>
    # - We perform binary search "from root to last level", so we
    #   loop d times
    left, right = 0, 2**d - 1
    for _ in range(d):
      pivot = left + (right - left) // 2

      # If idx <= cur level's pivot, idx is in left sub-tree
      if idx <= pivot:
        node = node.left
        right = pivot
      else:
        node = node.right
        left = pivot
        #left = pivot + 1  # 答案一樣對
    # Return True if last level node idx exists.
    return node is not None

  def countNodes(self, root: TreeNode) -> int:
    if not root:  return 0

    d = self.compute_depth(root)
    # If the tree contains 1 node
    if d == 0:  return 1

    # <Binary Search - I>
    # - We perform binary search to check "last level" of nodes here
    # - "Last level nodes" are enumerated: 0 ~ 2**d-1 (left -> right).
    #    But since 1st node in last level always exists, set left=1.
    left, right = 1, 2**d - 1
    while left <= right:
      # The var name, pivot, here might be confusing.
      # Var pivot  is the node checked for existence
      pivot = left + (right - left) // 2

      # If pivot exists, move pivot 1 pos right to find 1st empty node
      if self.exists(pivot, d, root):
        left = pivot + 1
      else:
        right = pivot - 1

    print(f'd: {d}, 2**d-1: {2**d - 1}, left: {left}')
    # (2**d - 1): total nodes b/t depth 0 ~ d-1
    # left: total nodes in depth d (last level - 不想 level 的 def)
    return (2**d - 1) + left
