'''
224. Basic Calculator

Explanation + Sol Code:
- https://leetcode.com/problems/basic-calculator/solutions/1456850/python-basic-calculator-i-ii-iii-easy-solution-detailed-explanation/
  ||
  ===> This link's 2nd sol is almost the same as 1st sol for optimization. 


Note:
- This solution allows for "+ - * /"
- In case the code doesn't work for similar calculator problems, see "Noted for Sol Mod" in README.txt.

思路:
- While iterating thr the input str, if the char is:
  (1) a digit, save to num by updating num.
  (2) a sign "+-*/", take the previously saved sign w/ num, do the calc 
      before appending the res to stack.
  (3) "(", start a new recursive stack.
      - 2 returned vals  ex. 1+(4+2)-3
        - sum from this returned sub-stack, (4+2) from ex. 1+(4+2)-3
        - index of ), index=6 from ex. 1+(4+2)-3
  (4) ")", do the same action as (2), and then sum up cur 
      stack before return the sum.
- After the loop ends, "REMEMBER" to append the set of sign & num to stack.
  Then summ the whole stack.


***必用!!!
Use this ex to (1) design the code and (2)trace code to understand
    ex0. 1+(4+2)-3
    ex1. 1+(4+5)-3*2
    ex2. (1+(4+5)-3*2)+(6+8)


QN's Code Mod:
- ln-67,71 can be replaced by ln-68,72.  
'''

'''
See comments and print() in the next part of the code

'''
class Solution:
    def calculate(self, s):    
        def calc(ndx):

            def update(op, num):
                if op == "+": stack.append(num)
                if op == "-": stack.append(-num)
                if op == "*": stack.append(stack.pop() * num)
                # Don't use // operator bc it's floor division.
                # ex. int(a/b) = int(12.5) = 13
                #     a // b = 12
                if op == "/": stack.append( int( stack.pop() / num ) )
        
            num, stack, sign = 0, [], "+"

            while ndx < len(s):
                if s[ndx].isdigit():
                    num = num * 10 + int(s[ndx])
                elif s[ndx] in "+-*/":
                    update(sign, num)
                    sign, num = s[ndx], 0
                elif s[ndx] == "(":
                    num, j = calc(ndx + 1)
                    ndx = j - 1
                    #ndx = j
                elif s[ndx] == ")":
                    update(sign, num)
                    return sum(stack), ndx + 1
                    #return sum(stack), ndx
                ndx += 1

            update(sign, num)
            return sum(stack)

        return calc(0)

'''
Same as last sol, but w/ comments and print()
'''
class Solution2:
    def calculate(self, s):
        def calc(ndx):
            def update(op, num):
                if op == "+": stack.append(num)
                if op == "-": stack.append(-num)
                if op == "*": stack.append(stack.pop() * num)
                # (1) int() is used to handle division of 0 in Python
                # (2) Don't use // operator bc it's floor division.
                #     ex. int(a/b) = int(12.5) = 13
                #         a // b = 12
                if op == "/": stack.append( int(stack.pop() / num) )

            num = 0     # Save val already calc'ed before an operator
            stack = []
            sign = "+"

            while ndx < len(s):
                print("\nndx:", ndx, ", s[", ndx, "]:", s[ndx])
                print("num:", num, ", stack:", stack, ", sign:", sign)

                # When we encounter a digit, save to num by updating num
                # - 把之前存的num * 10 以進位, 再加上 cur digit
                if s[ndx].isdigit():
                    print("if s[ndx].isdigit():")
                    num = num * 10 + int(s[ndx])
                    print("num:", num)

                # When we encounter "+-*/", 
                # 1. use the sign/operator saved "previously" to determine the 
                #    pos/neg of num.  Then push the num to stack.
                # 2. reset num=0 since it's pushed to stack. 
                #    update sign to cur sign.
                elif s[ndx] in "+-*/":
                    print("elif s[ndx] in \"+-*/\":")
                    update(sign, num)
                    print("stack:", stack)

                    # Since num is already stored in stack, reset num to 0,
                    # and update sign to cur sign
                    sign = s[ndx]
                    num = 0
                    print("num:", num, ", sign:", s[ndx])

                # When we encounter "(", start a new recursive stack
                elif s[ndx] == "(":
                    print("elif s[ndx] == \"(\":")
                    print("--->>Calling calc(", ndx+1, ") for recursion")
                    # num and ndx2 are returned from: elif s[ndx] == ")"
                    # - num is the sum of the sub-stack from the last 
                    #   recursive call.
                    # - ndx2 is the index of ')'
                    num, ndx2 = calc(ndx + 1)
                    print("num:", num, ", ndx2:", ndx2)

                    # Move cursor to ")", end of a mini exp, ex. 1 + (4 + 5) - 3
                    ndx = ndx2
                    print("ndx:", ndx)

                # When we encounter ")", push num calc'ed previously to stack.
                # And then sum nums in the stack.
                elif s[ndx] == ")":
                    print("elif s[ndx] == \")\":")
                    update(sign, num)
                    print("stack:", stack)
                    print("return sum(stack):", sum(stack), ", ndx+1:", ndx+1)
                    return sum(stack), ndx

                ndx += 1

            print()

            # ex. (1+(4+5)-3)+4
            #   At this pt, only (1+(4+5)-3) is handled. So only 7 is in stack,
            #   need to push '+4' to stack
            update(sign, num)
            print("stack:", stack)
            
            # Final step: sum up what are in stack
            res = sum(stack)
            return res

        # Start checking input s from index 0
        return calc(0)

# - - - - - - - - - - - - - # - - - - - - - - - - - - - #
# Testing:

s = "1+(4+5)-3*2"
sol2 = Solution2()
res = sol2.calculate(s)
print("\n= = = = = =\nres:", res)
