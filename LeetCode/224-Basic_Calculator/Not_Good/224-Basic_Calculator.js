/*
224. Basic Calculator

- LeetCode Std Solution (Java & Python):
    https://leetcode.com/articles/basic-calculator/#

- So go see comments:
    /BitBucket/LeetCode/224-Basic_Calculator.py
*/

var eval_exp = function(stack) {
  let res = 0, sign;

  stack.length > 0 ? res = stack.pop() : 0;

  // - Handle left-over ')'s.  ex.  When stack:  [ ')', ')', -10 ]
  // - Evaluate the expression till we get corresponding ')'
  while (stack.length > 0 && stack[stack.length - 1] != ')') {
    sign = stack.pop();
    if (sign == '+')    res +=stack.pop();
    if (sign == '-')    res -=stack.pop();
  }
  return res;
};

var calculate = function (s) {
  let regex = /[0-9]/;
  let stack = [];
  let n = 0, operand = 0, res;

  // Loop s backwards  for (let i = s.length - 1; i >= 0; i--) {
    let ch = s.charAt(i);
    console.log("\nch: ~", ch, "~");

    // Is curr char a digit?
    if (ch.match(regex) !== null) {
      operand += Math.pow(10, n) * parseInt(ch);
      n++;
    }
    // A digit is not appended until an operator is captured b/c a num could
    // consist of more than 1 digits.
    else if (ch != ' ') {
      if (n) {
        stack.push(operand);
        n = 0;
        operand = 0;
      }

      if (ch == "(") {
        res = eval_exp(stack);
        console.log("res: ", res);
        stack.pop();
        stack.push(res);
      } else {
        stack.push(ch);
      }

      console.log("ln-61, stack: ", stack);
    }
  }
  if (n)    stack.push(operand);

  return eval_exp(stack);
};


let s = "", re = 0;

//s = "7 - 8";              // -1
//s = "7 - 8 + 9 - 10"      // -2
//s = " 2-1 + 2"            // 3
//s = "(7 - (8 + 9))"       // 10
s = "(1+(4+3)-3)+(6+8)"     // 19

re = calculate(s)
console.log(s, " = " , re)

