'''
224. Basic Calculator

LeetCode Std Solution (Java & Python):
    https://leetcode.com/articles/basic-calculator/#
'''
class Solution:

  # Calculate an interim arithmetic expr
  # ex. We evaluate (8 + 9) in (7 - (8 + 9))
  def evaluate_expr(self, stack):
    # Ternary Operator: [on_true] if [expression] else [on_false]
    res = stack.pop() if stack else 0

    # - Handle left-over ')'s.  ex.  When stack:  [ ')', ')', -10 ]
    # - Evaluate the expression till we get corresponding ')'
    while stack and stack[-1] != ')':
      print("stack: ", stack, ",\tstack[-1]: ", stack[-1])
      sign = stack.pop()
      if sign == '+':
        res += stack.pop()
      else:
        res -= stack.pop()
    return res


  # Calculate a whole arithmetic expr
  def calculate(self, s: str) -> int:
    stack = []
    n, operand = 0, 0

    # Loop s backwards
    #
    # range(start, stop, step)
    for i in range(len(s)-1, -1, -1):
      ch = s[i]
      print("\nch: ~", ch, "~")

      # A digit is not appended until an operator is captured b/c a number
      # could consist of more than 1 digits.
      if ch.isdigit():
        # A**B: A^B
        operand = (10**n * int(ch)) + operand
        print("operand: ", operand)
        n += 1
        print("n: ", n)

      # No need this case b/c we skip " "
      #elif ch == " ":

      elif ch != " ":
        if n:
          # Save the operand on the stack as we encounter some non-digit.
          stack.append(operand)
          print("ln-56, stack: ~", stack, "~")
          n, operand = 0, 0

        if ch == '(':
          res = self.evaluate_expr(stack)
          # pop '(' of the last expr that's evaluated
          stack.pop()
          # Append the evaluated result to the stack.
          # This result could be a sub-expression w/in the parenthesis
          stack.append(res)
          print("ln-66, stack: ~", stack, "~")
        # For other non-digits just push onto the stack.
        else:
          stack.append(ch)
          print("ln-70, stack: ~", stack, "~")

    # Push the last operand to stack, if any.
    # - Used to handle case like, s = "1 + 2", no parenthesis
    if n:
      stack.append(operand)

    # Evaluate any left overs in the stack.
    return self.evaluate_expr(stack)


sol = Solution()
#s = "7 - 8 + 9"            # 8
#s = " 2-1 + 2"             # 3
#s = "(7 - (8 + 9))"        # 10
s = "(1+(4+3)-3)+(6+8)"     # 19
re = sol.calculate(s)
print(s, " = " , re)
