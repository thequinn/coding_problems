/*
226. Invert Binary Tree
      |
      ==> 注意!! 這裡的 Inverted 指的是 Mirror Image!!!
*/

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */


// Approach #1: Recursive
//
// 技巧: 這只是改造的 "DFS"
//
// 思路: ==> 這 video 講解得很好! 必看!!
// - https://www.youtube.com/watch?v=vdwcCIkLUQI
//
// - Time Analysis: O(n)
// -- Since each node in the tree is visited only once, the time complexity is
//    O(n), where n is the number of nodes in the tree. We cannot do better
//    than that, since at the very least we have to visit each node to invert
//    it.
//
// - Space Analysis: O(n)
// -- Because of recursion, O(h) function calls will be placed on the stack in
//    the worst case, where h is the height of the tree. Because h∈O(n), the
//    space complexity is O(n).
//
var invertTree = function(root) {
    if (root == null)   return null;

    let left  = invertTree(root.left);
    let right = invertTree(root.right);
    root.right = left;
    root.left = right;

    return root;
};
// Same as Approach #1
// https://leetcode.com/problems/invert-binary-tree/solutions/399221/clean-javascript-iterative-dfs-bfs-solutions/
//
function invertTree(root) {
  if (root == null) return root;
  [root.left, root.right] = [invertTree(root.right), invertTree(root.left)];
  return root;
}

//-----------------------------------

// Approach #2: Iterative => Modified DFS.
//                        => Qn doesn't like it.  See .py version.
// - Time Analysis: O(n)
// -- Since each node in the tree is visited/added to the queue only once, the
//    time complexity is O(n), where n is the number of nodes in the tree.
//
// - Space Analysis: O(n)
// -- Since in the worst case, the queue will contain all nodes in one level of
//    the binary tree. For a full binary tree, the leaf level has
//    Ceiling(n/2) = O(n) leaves.
//
// QN's Approach: Queue w/ push() & shift()
//                => Add to back, remove from front (這才是Queue正常作業方式)
var invertTree = function(root) {
  if (root == null)   return null;

  let queue = [];
  queue.push(root);

  while (queue.length > 0) {
    let curr = queue.shift();  // Remove an elem from front of queue

    // Swap left and right nodes
    [curr.left, curr.right] = [curr.right, curr.left];

    if (curr.left)  queue.push(curr.left);
    if (curr.right) queue.push(curr.right);
  }

  return root;
}

