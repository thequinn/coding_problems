'''
226. Invert Binary Tree
      |
      ==> 注意!! 這裡的 Inverted 指的是 Mirror Image!!!

Sol:
    https://leetcode.com/problems/invert-binary-tree/solutions/62705/python-solutions-recursively-dfs-bfs/?orderBy=most_votes


Note:
Compare the operation funcs in list vs deque
- See Bitbucket/tutorial/PY/list_vs_deque.txt
'''

# Recursively
def invertTree(self, root):
    if root is None:    return root

    # Swap the 2 childs
    root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)a
    # Last ln is same as this block of code below
    #root.left, root.right = root.right, root.left
    #self.invertTree(root.left)
    #self.invertTree(root.right)

    return root

# BFS
from collections import deque
def invertTree(self, root):
    # Same as ln-29
    queue = collections.deque([root])

    while queue:
        # See Bitbucket/tutorial/PY/list_vs_deque.txt
        node = queue.popleft()
        if node:
            # Swap the 2 childs
            node.left, node.right = node.right, node.left

            # extend(): 
            # - Extend the right side of the deque by appending elems from 
            #   the iterable argument.
            # Iterable: 
            # - Objects like lists, tuples, sets, dictionaries, strings 
            #   that can be looped over. node.left & node.right aren't 
            #   iterables.  They need to be put in a list. 
            queue.extend([node.left, node.right])
            # The next 2 lns same as the last ln
            #queue.append(node.left)
            #queue.append(node.right)
    return root

# DFS => Skip this method b/c it's Not intuitive.
def invertTree(self, root):
    stack = [root]
    while stack:
        # See Bitbucket/tutorial/PY/list_vs_deque.txt
        node = stack.pop()
        if node:
            # Swap the 2 childs
            node.left, node.right = node.right, node.left
            stack.extend([node.right, node.left])
    return root




