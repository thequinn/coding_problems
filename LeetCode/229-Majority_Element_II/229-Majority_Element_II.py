'''
229. Majority Element II

Boyer-Moore Majority Vote algorithm:
- See: 169-Majority_Element.py

Explanation:
- https://leetcode.com/problems/majority-element-ii/solutions/63537/my-understanding-of-boyer-moore-majority-vote/
  - See the images of this link to understand solving the type II problem.
- https://leetcode.com/problems/majority-element-ii/solutions/4131226/99-7-hashmap-boyer-moore-majority-voting-explained-intuition/
  - See the text explanation of solving this problem.

  Boyer-Moore Majority Voting Solution
  
  1. Create variables to track counts and candidates for potential majority 
  elements.

  2. First Pass - Find Potential Majority Elements:
  - Iterate through the input array and identify potential majority element 
    candidates.
  - Update the candidates based on specific conditions.
  - Maintain counts for each candidate.

  3. Second Pass - Count Occurrences:
  - Iterate through the input array again to count the occurrences of the 
    potential majority elements.

  4. Compare the counts with a threshold to determine the majority elements.

  5. Return Majority Elements.

Sol Code:
https://leetcode.com/problems/majority-element-ii/solutions/63520/boyer-moore-majority-vote-algorithm-and-my-elaboration/


Time complexity: O(N)
- Since we are iterating over the array in two passes then the complexity is 
  2 * N which is O(N).

Space complexity: O(1)
- Since we are only storing constant variables then the complexity is O(1).   
'''
class Solution:
    def majorityElement(self, nums):

        # Check if a list is empty in Python 
        #if not nums:  return []  # Other sol's don't have this ln

        count1, count2, cand1, cand2 = 0,0,0,1
        
        for n in nums:
            if n == cand1:
                count1 += 1
            elif n == cand2:
                count2 += 1
            elif count1 == 0:
                cand1, count1 = n, 1
            elif count2 == 0:
                cand2, count2 = n, 1
            else:
                count1, count2 = count1-1, count2-1
        return [n for n in (cand1, cand2) if nums.count(n) > len(nums)//3]

