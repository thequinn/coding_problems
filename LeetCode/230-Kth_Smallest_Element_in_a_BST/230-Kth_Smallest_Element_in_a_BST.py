'''
230. Kth Smallest Element in a BST


Basis:

(1) Solving the problem w/ DFS In-order Traversal Recursively/Iteratively

(2) DFS In-order Traversal Template:

    Traverse the left subtree
    Visit the root              // this is where we do the work
    Traverse the right subtree

Note:
- Knowing it prevents from slowing down from understanding the sol code below and not knowing the template.
'''

'''
Approach #1: DFS In-order Traversal + Recursive

Video Explination + Sol Code: -> Good explination to time-complexity
    https://www.youtube.com/watch?v=9TJYWh0adfk

Sol Code:
    https://leetcode.com/problems/kth-smallest-element-in-a-bst/solutions/63829/python-easy-iterative-and-recursive-solution/
'''
class Solution:
    def kthSmallest(self, root, k):
        self.k = k
        self.res = None
        self.helper(root)
        return self.res

    def helper(self, node):
        if not node:    return

        # Left sub-tree
        self.helper(node.left)

        # Root
        self.k -= 1
        if self.k == 0:
            self.res = node.val
            return

        # Right sub-tree
        self.helper(node.right)

'''
Approach #2: DFS In-order Traversal + Iterative -> DFS Traversal uses a Stack
                                                   BFS Traversal uses a Queue
Video Explination + Sol Code:
    https://www.youtube.com/watch?v=5LUXSvjmGCw

Sol Code:
    https://leetcode.com/problems/kth-smallest-element-in-a-bst/solutions/63829/python-easy-iterative-and-recursive-solution/
'''
class Solution:
    def kthSmallest(self, root, k):
        stack = []

        while root or stack:

            # Going to the left subtree
            while root:
                stack.append(root)
                root = root.left

            # Root
            root = stack.pop()
            k -= 1
            if k == 0:
                return root.val

            # Working on the right sub-tree
            root = root.right
