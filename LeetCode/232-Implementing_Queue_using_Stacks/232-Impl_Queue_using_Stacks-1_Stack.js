/*
232. Implement Queue using Stacks ==> Using 1 Stack

Notes:
  (1) You must use only standard operations of a stack -- which means only push to top, peek/pop from top, size, and is empty operations are valid.
  (2) You may assume that all operations are valid (for example, no pop or peek operations will be called on an empty queue)


Solution  ==> Using 1 stack 
  https://leetcode.com/problems/implement-queue-using-stacks/discuss/64314/My-Simple-JS-Solution
*/

/**
 * Initialize your data structure here.
 */
var MyQueue = function() {
  this.store = [];
};

/**
 * Push element x to the back of queue. 
 * @param {number} x
 * @return {void}
 */
MyQueue.prototype.push = function(x) {
  this.store.push(x);
};

/**
 * Removes the element from in front of queue and returns that element.
 * @return {number}
 */
MyQueue.prototype.pop = function() {
  let elem = this.store[0];
  this.store = this.store.slice(1, this.store.length);
  return elem;
};

/**
 * Get the front element.
 * @return {number}
 */
MyQueue.prototype.peek = function() {
  return this.store[0];
};

/**
 * Returns whether the queue is empty.
 * @return {boolean}
 */
MyQueue.prototype.empty = function() {
  return this.store.length === 0;
};


// Your MyQueue object will be instantiated and called as such:
var obj = new MyQueue()

var nums = [0,1,2,3,4];
nums.forEach((num) => {
  obj.push(num);
});
console.log(obj);

var param_2 = obj.pop();
console.log(param_2);

var param_3 = obj.peek();
console.log(param_3);

var param_4 = obj.empty()
console.log(param_4);
