/*
232. Implement Queue using Stacks  ==> Using 2 Stacks

Notes:
  (1) You must use only standard operations of a stack -- which means only push to top, peek/pop from top, size, and is empty operations are valid.
  (2) You may assume that all operations are valid (for example, no pop or peek operations will be called on an empty queue)


How do you implement a queue using two stacks?
- 思路：
  (1) When calling the enqueue method, simply push the elements into the stack 1.
  (2) If the dequeue method is called, push all the elements from stack 1 into stack 2, which reverses the order of the elements.


Solution ==> Using 2 stacks
  https://leetcode.com/problems/implement-queue-using-stacks/discuss/64331/Java-solution-using-two-stacks

Solution Analysis:
-  Time: O(1) - amortized for pop/peek
-  Space: O(n)
-  n - # of elements
*/

var MyQueue = function() {
  this.pushStack = [];
  this.popStack = [];
};

// Push element x to the back of queue
MyQueue.prototype.push = function(x) {
  this.pushStack.push(x);
};

// Removes the element from in front of queue and returns that element
MyQueue.prototype.pop = function() {
  if (this.popStack.length < 1) {
    while (this.pushStack.length) {
      this.popStack.push(this.pushStack.pop());
    }
  }
  return this.popStack.pop();
};

// Get the front element
MyQueue.prototype.peek = function() {
  if (this.popStack.length < 1) {
    while (this.pushStack.length) {
      this.popStack.push(this.pushStack.pop());
    }
  }
  return this.popStack[this.popStack.length - 1];
};

MyQueue.prototype.empty = function() {

  // 重要技巧!!
  // If both are false, return false
  let tmp = !this.pushStack.length && !this.popStack.length;

  console.log(tmp);
  return tmp;
}


// Test

var obj = new MyQueue()

var nums = [1,2,3,4,5];
nums.forEach((num) => {
  obj.push(num);
});
console.log(obj);

var param_3 = obj.peek();
console.log(param_3);

var param_2 = obj.pop();
console.log(param_2);

var param_4 = obj.empty()
console.log(param_4);

