/*
234. Palindrome Linked List

Follow up:
Could you do it in O(n) time and O(1) space?

Solution:
- https://skyyen999.gitbooks.io/-leetcode-with-javascript/content/questions/234md.html

*/

// Approach #1-1 (不考慮 follow-up 的情況): Stack
// - https://leetcode.com/problems/palindrome-linked-list/discuss/148220/Javathe-clear-method-with-stack
//
var isPalindrome = function(head) {
  let tmp = head;
  let stack = [];

  // WRONG!! 雖然答案通過，最好不用！
  // - (!tmp) 等同 (tmp===null && tmp===undefined)
  // while (!tmp) {
  //
  // Correct:
  while (tmp !== null) {
    stack.push(tmp.val);
    tmp = tmp.next;
  }

  // WRONG!!
  //while (!head) {
  //
  while (head !== null) {
    if (head.val != stack.pop()) {
      return false;
    }
    head = head.next;
  }
  return true;
};

// Approach #1-2 (不考慮 follow-up 的情況): Two Pointers
// https://skyyen999.gitbooks.io/-leetcode-with-javascript/content/questions/234md.html
// - 思路:
// (1) 走訪連結陣列，使用兩個字串來處理
// (2) 一個正向字串(str = str + value)，另外一個為反向字串(str = value + str)
// (3) 最後判斷兩個字串是否相等
//
var isPalindrome = function(head) {

    var rec = "";    //反向字串
    var seq = "";   //正向字串

    // WRONG!! 雖然答案通過，最好不用！
    // - (!head) 等同 (head===null && head===undefined)
    // while (!head) {
    //
    while(head != null){
        seq += head.val;
        rec = head.val + rec;
        head = head.next;
    }
    // 反向字串與正向字串相等就是回文陣列
    return seq == rec;

};

// Method #3 (考慮 follow-up 的情況): Two Pointers
// - https://leetcode.com/problems/palindrome-linked-list/discuss/64501/Java-easy-to-understand
// - 思路:
// -- 先用快慢指針找出linked list的中點，找到後從中點之後將linked list 反轉
//    再與本來的 head 前半段比較是否相等，這邊需要一個額外的空間儲存反轉後的
//    linked list。
//
// - O(n) time and O(1) memory
//
var isPalindrome = function(head) {
  let mid = findMiddle(head);
  let rNode = reverseList(mid);

  // 比對反轉後的node與head前半段是否相等
  // -  When the node we're up to in the second list is null, we're done
  while (rNode != null) {
    if (head.val != rNode.val) {
      return false;
    }
    head = head.next;
    rNode = rNode.next;
  }
  return true;
}
// 使用快慢指針找出中點
var findMiddle = function(node) {
  let fast = node, slow = node;
  while (fast != null && fast.next != null) {
    slow = slow.next;
    fast = fast.next.next;
  }
  return slow;
}
// Reverse linked list.  來自: 206. Reverse Linked List.
var reverseList = function(head) {

  let prev = null;            // Store prev node's ref
  let curr = head;

  while (curr !== null) {
    let nextTmp = curr.next;  // Store next node in nextTmp

    curr.next = prev;         // Reverse curr.next to prev

    prev = curr;
    curr = nextTmp;
  }
  return prev;  // Head of the reversed second half of the LL
};

