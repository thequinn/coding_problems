'''
234. Palindrome Linked List

Follow up:
Could you do it in O(n) time and O(1) space?


Definition for singly-linked list.

  class ListNode:
    def __init__(self, val=0, next=None):
      self.val = val
      self.next = next


LC Premium Sol:
- https://leetcode.com/problems/palindrome-linked-list/solution/
'''

''' (不考慮 follow-up)
Approach 1: Copy into Array List and then Use Two Pointer Technique
            --> Same as Approach #1-1 and #1-2 in .js

Time Complexity : O(2N) ~ O(N)
Space Complexity: O(N)
'''
class Solution:

  def isPalindrome(self, head: ListNode) -> bool:
    vals = []
    curr = head

    while curr is not None:  # In JS: while (curr !== null)
      vals.append(curr.val)

    # Compare vals[]  w/ reversed vals[]
    return vals == vals[::-1]


''' (不考慮 follow-up)
Approach #2: Recursive ----> ...未: Don't understand.....

'''
class Solution:

  def isPalindrome(self, head: ListNode) -> bool:
    self.front_pointer = head

    def recu(curr = head):

      if curr is not None:

        if not recu(curr.next):
          return False

        if self.front_pointer.val != curr.val:
           return False

        self.front_pointer = self.front_pointer.next

      return True

    return recu()


''' (考慮 follow-up)
Approach #3: Reverse Second Half In-place
'''
class Solution:

  def isPalindrome(self, head: ListNode) -> bool:
    if head is None: return True

    # Find the end of first half and reverse second half.
    first_half_end = self.end_of_first_half(head)
    second_half_start = self.reverse_list(first_half_end.next)

    # Check whether or not there's a palindrome.
    result = True
    first_position = head
    second_position = second_half_start
    # When the node we're up to in the second list is null, we're done
    while result and second_position is not None:
      if first_position.val != second_position.val:
        result = False
      first_position = first_position.next
      second_position = second_position.next

    return result

  def end_of_first_half(self, head):
    fast = head
    slow = head
    while fast.next is not None and fast.next.next is not None:
      fast = fast.next.next
      slow = slow.next
    return slow

  # Reverse linked list.  來自: 206. Reverse Linked List.
  def reverse_list(self, head):
    previous = None
    current = head
    while current is not None:
      next_node = current.next
      current.next = previous

      previous = current
      current = next_node
    return previous  # Head of the reversed second half of the LL
