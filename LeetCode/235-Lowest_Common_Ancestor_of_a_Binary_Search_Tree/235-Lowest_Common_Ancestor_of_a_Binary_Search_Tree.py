'''
235. Lowest Common Ancestor of a Binary Search Tree
'''


'''
Approach #1: Iterative

Video + Sol Code:
- Leetcode 75 Questions (NeetCode on yt)
    https://docs.google.com/spreadsheets/d/1A2PaQKcdwO_lwxz9bAnxXnIQayCouZP6d-ENrBz_NXc/edit#gid=0
'''
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode',
                         q: 'TreeNode') -> 'TreeNode':
    cur = root

    while cur:
        # If vals of both child nodes > parent node, go to right sub-tree
        if p.val > cur.val and q.val > cur.val:
            cur = cur.right
        # If vals of both child nodes < parent node, go to left sub-tree
        elif p.val < cur.val and q.val < cur.val:
            cur = cur.left
        # Else, it's either a split (1 child node on each side of the
        # sub-tree), or one/two of the child node is the parent node
        else:
            return cur

        # No need to add a return here bc we guarantee ln-32 is gonna exec
        # at some pt


'''
Approach #2: Recursive
    https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/solutions/65183/my-python-recursive-solution/
'''
def lowestCommonAncestor(self, root, p, q):
    if not root or not p or not q:
        return None

    if (max(p.val, q.val) < root.val):
        return self.lowestCommonAncestor(root.left, p, q)
    elif (min(p.val, q.val) > root.val):
        return self.lowestCommonAncestor(root.right, p, q)
    else:
        return root
