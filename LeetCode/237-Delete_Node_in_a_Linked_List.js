/*
237. Delete Node in a Linked List

Tip！
- Linked List 题目通常不需寫出 Linked List 來測試。 題目只要修改一小部分 Linked List operation。

LeetCode Std Solution:
- https://leetcode.com/problems/delete-node-in-a-linked-list/solution/
*/

// The param node is the actual node we want to remove.  It is not val of the node like in problem ex.
//
// ex1.
//   Input: head = [4,5,1,9], node = 5
//   Output: [4,1,9]
//
// Approach: Swap with Next Node
// - 思路:
// -- The usual way of deleting a node node from a linked list is to modify the next pointer of the node before it, to point to the node after it.  Since we do not have access to the node before the one we want to delete, we cannot modify the next pointer of that node in any way. Instead, we have to replace the value of the node we want to delete with the value in the node after it, and then delete the node after it.
//
var deleteNode = function(node) {
  // 注意看思路，程式碼沒你想的那麼簡單！
  node.val = node.next.val;
  node.next = node.next.next;
};


