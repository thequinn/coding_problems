'''
238. Product of Array Except Self

    https://leetcode.com/problems/product-of-array-except-self/solutions/744951/product-of-array-except-self-python3-solution-with-a-detailed-explanation/


解題技巧:
- Solving this problem is not hard as long as you have an idea of what possible edge cases there are.


思路:

--->>>Normal Cases:
    a. All positives
Input: nums = [1,2,3,4]
Output: [24,12,8,6]

    b. Mis of positives & negatives
Negative Numbers
Input: [-1, 2, -3, 4]
Output: [-24, 12, -8, 6]


--->>>Edge Cases:
	1. One Zero in nums
Input: [1, 2, 0, 4]
Output: [0, 0, 8, 0]

=> Only 1 zero:
    - Except the elem that's 0, the rest of them have multiplcation results of 0	
    2. Zeros in nums
Input: [0, 4, 0]
Output: [0, 0, 0]

=> More than 1 zeros:
    - all the multiplcation results are 0

	3.	All Zeros
Input: [0, 0, 0]
Output: [0, 0, 0]

=> Same as case 2.

	4.	Single Element
Input: [42]
Output: [1]




'''


'''
Approach #0: By QN
'''
class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        leng = len(nums)
        ans = [0] * leng

        # Case 4.
        if leng == 1: return ans[1]

        # Track num of zeros in nums[] and do multiplication
        zero_counter = 0
        prod = 1
        for i, num in enumerate(nums):
            if num == 0: zero_counter += 1
            else:        prod *= num
       
        # Assign correct vals to ans[]
        for i, num in enumerate(nums):
            # Case 1.
            if zero_counter == 1:
                if num != 0:  ans[i] = 0
                else: ans[i] = prod
                # Case 2. & Case 3.
            elif zero_counter > 1:
                return [0] * leng
                # Normal cases
            else: ans[i] = prod // num
        return ans

'''
Approach #1: Left and Right product lists

思路:
-Each ans[i] except self = (all the products of elems to the left) * (all the products of elems to the right)

Note: Always remember to use examples to design code and then trace it


ex.
input : nums = [1, b, c, d, e]
output: res = [|bcde, a|cde, ab|de, abc|e, abcd|]

If we could come up with two arrays, one keeping track of L_Prods and one R_Prods, we could multiply each entry of them to get the desired results.

index   =  0      1      2      3      4
- - - - - - - - - - - - - - - - - - - - - - -
nums    = [1    , b    , c    , d    , e    ]
L_Prods = [1    , a    , ab   , abc  , abcd ]
R_Prods = [bcde , cde  , de   , e    , 1    ]
res     = [|bcde, a|cde, ab|de, abc|e, abcd|]



Time Complexity : O(2N) ~ O(N), b/c 2 for loops
Space Complexity: O(2N) ~ O(N), used L[] and R[]
'''
from typing import List
class Solution:
    def productExceptSelf_1(self, nums: List[int]) -> List[int]:
        leng = len(nums)

        L = [0]*leng # initialize left array
        R = [0]*leng # initialize right array

        L[0], R[-1] = 1, 1 
        res = [] # output

        for i in range(1, len(nums)):
            L[i] = L[i-1] * nums[i-1]

            # Same as next part
            R[i] = R[ (leng-1) - i ]
            #
            # 記憶! Trick to move 2nd ptr backward from end of arr
            #j = -i - 1 
            #R[j] = R[j+1] * nums[j+1]
            

        for i in range(len(nums)):
            res.append(L[i] * R[i])
        # Same as next 2 ln's
        #res = [x * y for x, y in zip(L, R)]
        
        return res

'''
Approach #2: Improved space usage from Approach #1 to solve the "Follow up" part

Follow up: Can you solve the problem in O(1) extra space complexity? (The output array does not count as extra space for space complexity analysis.)

Time Complexity : O(2N) ~ O(N)
Space Complexity: O(1), only uses res[]
'''
from typing import List
class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        res = []
        
        p = 1
        for i in range(len(nums)):
            res.append(p)
            p = p * nums[i]
        
        p = 1
        for i in range(len(nums) - 1, -1, -1):
            res[i] = res[i] * p #1
            p = p*nums[i]
        
        return res

a = [1,2,3,4]
s = Solution()
res = s.productExceptSelf_2(a)
print("res:", res)
