/*
240. Search a 2D Matrix II

思路:
1. We start to search the matrix from top right corner,
2. initialize the current position to top right corner
  a. if the target is greater than the value in current position, then the
     target can not be in entire row of current position because the row is
     sorted,
  b. if the target is less than the value in current position, then the target
     can not in the entire column because the column is sorted too.
- Note:
-- You can search from another corner (ex. in APpraoch #2).  It's like the
   matrix contains two, "binary search tree" and it has two "roots"
   correspondingly.


Solution Code for Approach #1 & #2:
- https://leetcode.com/problems/search-a-2d-matrix-ii/discuss/66140/My-concise-O(m%2Bn)-Java-solution


Time Complexity:
- We can rule out one row or one column each time, so the time complexity is
  O(m+n) or O(row+ col)
Space Complexity:
- O(1)

*/

// Appraoch #1: Search from top right corner
const searchMatrix = function(matrix, target) {
  if (matrix == null || matrix.length < 1 || matrix[0].length < 1) {
    return false;
  }

  // Start from top right corner
  let col = matrix[0].length-1;
  let row = 0;

  while(col >= 0 && row <= matrix.length-1) {
    if(target == matrix[row][col])
      return true;
    else if(target < matrix[row][col])
      col--;
    else if(target > matrix[row][col])
      row++;
  }
  return false;
}


// Approach #2: Search from bottom left corner
//
/*const searchMatrix = function(matrix, target) {
  if (matrix == null || matrix.length < 1 || matrix[0].length < 1)
    return false;

  // Start from bottom left corner
  let col = 0;
  let row = matrix.length - 1;

  while (col <= matrix[0].length - 1 && row >= 0) {
    if (target == matrix[row][col])
      return true;
    else if (target < matrix[row][col])
      row--;
    else if (target > matrix[row][col])
      col++;
  }
  return false;
}*/


//---------- Testing ----------
let A = [
  [1,   4,  7, 11, 15],
  [2,   5,  8, 12, 19],
  [3,   6,  9, 16, 22],
  [10, 13, 14, 17, 24],
  [18, 21, 23, 26, 30]
];

let target1 = 5; // T
let target2 = 20; // F

let res = searchMatrix(A, target1);
console.log("res:", res);

res = searchMatrix(A, target2);
console.log("res:", res);
