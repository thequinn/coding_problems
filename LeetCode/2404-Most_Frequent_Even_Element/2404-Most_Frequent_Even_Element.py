'''
2404. Most Frequent Even Element

Read how to use Counter class
- class collections.Counter([iterable-or-mapping])
  https://realpython.com/python-counter/#constructing-counters
'''

from collections import Counter
class Solution(object):
    def mostFrequentEven(self, nums):
        # Filter out odd numbers and count the frequency of each even number
        freq_dict = Counter(num for num in nums if num % 2 == 0)
        
        # If there is no even number, return -1
        if not freq_dict:
            return -1
        
        # If there is a tie, return the smallest one.
        # s1. Find the maximum frequency
        max_freq = max(freq_dict.values())
        # s2. Find the smallest number with the maximum frequency
        most_frequent_even = min(num for num, freq in freq_dict.items() if freq == max_freq)
        
        return most_frequent_even
