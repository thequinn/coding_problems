/*
242. Valid Anagram  => Anagram: 由調換字母順序而成的字

--------------------------------------------------
Follow up:
  What if the inputs contain unicode characters? How would you adapt your
  solution to such case?

Answer:
  Use a hash table instead of a fixed size counter. Imagine allocating a
  large size array to fit the entire range of unicode characters, which
  could go up to more than 1 million. A hash table is a more generic
  solution and could adapt to any range of characters.
*/

// Approach #1: Sorting
// - LeetCode Std Solution:
//  https://leetcode.com/problems/valid-anagram/solution/
// - 思路: After sorting, 2 words have the same strs and length
//
// - Time Complexity: O(nlogn)
// -- Depending on sorting algorithm used.  Mozilla uses merge sort
//    O(nlogn), comparing two strings costs O(n). Sorting time dominates
//    the overall time complexity.
//
// - Space Complexity: O(1)
// -- Depending on auxiliary space used by sorting algorithm.  Merge sort is
//    O(n)
// -- Note:
//  In JS, split() makes a copy of the string so it costs O(n) extra
//  space, but we ignore this for complexity analysis because:
//    1. It is a language dependent detail.
//    2. It depends on how the function is designed.
//
/*var isAnagram = function(s, t) {
  if (s2.length !== t2.length)
    return false;

  let s2 = s.split('').sort().join('');
  let t2 = t.split('').sort().join('');

  if (s2 == t2)  return true;
  return false;
};*/

// Approach #2-1: Hash Table
// - LeetCode Std Solution:
//  https://leetcode.com/problems/valid-anagram/solution/
// - Same code but in JS Version:
//  https://leetcode.com/problems/valid-anagram/discuss/66810/JavaScript-solution
//
// - split() uses O(n) memory space. (See Approach #2-2 for improvement)
//
var isAnagram = function(s, t) {
  if (s.length !== t.length)    return false;
  
  const map = {};
  s.split('').map(c => map[c] = map[c] ? map[c] + 1 : 1);
  t.split('').map(c => map[c] = map[c] ? map[c] - 1 : -1);
  
  let keyArr = Object.keys(map);
  //console.log("keyArr:", keyArr);
  return keyArr.every(k => map[k] === 0);
}

// Approach #2-2: Hash Table
//
var isAnagram = function(s, t) {
  if (s.length !== t.length)   return false;

  const map = {};
  for (let i = 0; i < s.length; i++) {
    map[s[i]] ? map[s[i]]++ : map[s[i]] = 1;
  } 
  for (let i = 0; i < t.length; i++) {
    if (map[t[i]]) map[t[i]]--;
    else return false;
  }
  return true;
};


let s = "anagram", t = "nagaram";   // T
//let s = "rat", t = "car";           // F
console.log(isAnagram(s, t));
