'''
242. Valid Anagram

'''

'''
Approach #1: Using dict
- 1st method on the page:
  https://leetcode.com/problems/valid-anagram/solutions/66499/python-solutions-sort-and-dictionary/?orderBy=most_votes
'''
def isAnagram1(self, s, t):
    dic1, dic2 = {}, {}
    for item in s:
        dic1[item] = dic1.get(item, 0) + 1
    for item in t:
        dic2[item] = dic2.get(item, 0) + 1
    return dic1 == dic2


'''
Approach #2: Using defaultdict
  https://leetcode.com/problems/valid-anagram/solutions/433680/python-3-o-n-faster-than-98-39-memory-usage-less-than-100/?orderBy=most_votes

'''
def isAnagram(self, s: str, t: str) -> bool:

    # defaultdict
    # - defaultdict is much like an ordinary dictionary. The main diff b/t defaultdict and dict is that when you try to access or modify a key that's not present in the dictionary, a default value is automatically given to that key.
    tracker = collections.defaultdict(int)

    for x in s: tracker[x] += 1
    for x in t: tracker[x] -= 1

    #return all(x == 0 for x in tracker.values())
    # Same as last ln
    return not any(tracker.values())

'''
Approach #3:
- 3rd method on the page:
  https://leetcode.com/problems/valid-anagram/solutions/66499/python-solutions-sort-and-dictionary/?orderBy=most_votes
'''
def isAnagram3(self, s, t):
    '''
    sorted()
    - sorted() returns a sorted list of the specified iterable object.
    - You can specify ascending or descending order.
    - Strings are sorted alphabetically, and numbers are sorted numerically.
    - Note: You can't sort a list that contains BOTH string and numeric values.
    '''
    return sorted(s) == sorted(t)


'''
Approach #4:
- See comment by qeatzy
  https://leetcode.com/problems/valid-anagram/solutions/66499/python-solutions-sort-and-dictionary/?orderBy=most_votes


collections.Counter()
    - https://www.hackerrank.com/challenges/collections-counter/problem
    - A counter is a container that stores elements as dictionary keys, and their counts are stored as dictionary values.

>>> from collections import Counter
>>>
>>> myList = [1,1,2,3,4,5,3,2,3,4,2,1,2,3]
>>> print Counter(myList)
Counter({2: 4, 3: 4, 1: 3, 4: 2, 5: 1})
>>>
>>> print Counter(myList).items()
[(1, 3), (2, 4), (3, 4), (4, 2), (5, 1)]
>>>
>>> print Counter(myList).keys()
[1, 2, 3, 4, 5]
>>>
>>> print Counter(myList).values()
[3, 4, 4, 2, 1

'''

from collections import Counter

def isAnagram1(self, s, t):
       return Counter(s) == Counter(t)
