'''
Notes: ...未...
    QN attemped to learn this problem, but doesn't find a sol easy enough to understand.  



2516. Take K of Each Character From Left and Right

https://leetcode.com/problems/take-k-of-each-character-from-left-and-right/solutions/2948183/python-clean-12-line-sliding-window-solution-with-explanation/
'''
class Solution:
    def takeCharacters(self, s: str, k: int) -> int:
        limits = {c: s.count(c) - k for c in 'abc'}
        if any(x < 0 for x in limits.values()):
            return -1

        cnts = {c: 0 for c in 'abc'}
        ans = l = 0
        for r, c in enumerate(s):
            cnts[c] += 1
            while cnts[c] > limits[c]:
                cnts[s[l]] -= 1
                l += 1
            ans = max(ans, r - l + 1)

        return len(s) - ans
