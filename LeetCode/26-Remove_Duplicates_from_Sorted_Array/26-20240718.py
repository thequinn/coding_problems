class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        l, r = 0, 0

        for r in range(len(nums)):
            # if l num the same as the r num, continue to go to next iter
            # if diff, swap r num to the pos on the right of the l num, update l ptr by 1, then go to next iter
            if nums[l] != nums[r]:
                nums[r], nums[l+1] = nums[l+1], nums[r]
                l += 1

        # Increment by 1 bc l index starts w/ 0
        return l+1



