/*
26. Remove Duplicates from Sorted Array


Note:

(1) Do not allocate extra space for another array, you must do this by
    modifying the input array in-place with O(1) extra memory.

(2) It doesn't matter what you leave beyond the returned length.

(3) It doesn't matter what values are set beyond the returned length.

LeetCode Std Solution:
  https://leetcode.com/problems/remove-duplicates-from-sorted-array/solution/

*/


// - Time complextiy : O(n)
// -- Assume that n is the length of array. Each of i and j traverses at
//   most n steps.
// - Space complexity : O(1)
//
var removeDuplicates = function(nums) {
  if (nums.length == 0)   return 0;

  let i = 0;  // i is slow ptr
  for (let j = 1; j < nums.length; j++) {  // j is fast ptr
    if (nums[j] != nums[i]) {
      i++;
      nums[i] = nums[j];
    }
    //console.log("nums[]:", nums);
  }

  return ++i; // Count 1st elem in nums[]
};

//let nums = [];
let nums = [1,1,2];                // 2, b/c [1,2]
//let nums = [1,2,2,2,3]             // 3, b/c [1,2,3]
//let nums = [0,0,1,1,1,2,2,3,3,4];  // 5, b/c [0,1,2,3,4]
console.log(removeDuplicates(nums));
