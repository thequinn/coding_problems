'''
Approack #1: => Skip!! This approach is ok.  But apadt Approach #2
                instead to learn the template.

Solution Code:
  https://leetcode.com/problems/remove-duplicates-from-sorted-array/discuss/11751/Simple-Python-solution-O(n)
'''

class Solution:
  def removeDuplicates(self, nums: List[int]) -> int:
    if not nums:  return 0

    # 思路:
    # Slow ptr moves when cur num is diff from the num pt'ed by the slow ptr.

    i = 0  # i: slow ptr
    for j in range(1, len(nums)):
      if nums[i] != nums[j]:  # j: fast ptr
        i = i+1
        nums[i] = nums[j]
    return i+1


'''
Approack #2: => From LC-80.

Explanation + Sol Code:
-(1) Video: https://www.youtube.com/watch?v=OZaADxYTfD4
-(2) https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/discuss/967951/Python-Two-pointers-approach-explained

思路:
- Setup:
    (1) k=1: original + 0 duplicate
    (2) slow ptr (sp), is the 1st available pos to insert the next correct val
    (3) Not care about the 1st k nums b/c we will do (sp-k) to check
- Algo:
    (1) When fp's val is the same as (sp's prev val), keep increment fp.
    (2) When diff, copy fp's val into sp's val, and then increment sp.
  Note - 命名:
      (sp's prev val) = (sp's ex val) = (sp-1)'s val => Template: (sp-k)'s val

ex. nums = [1,1,2]

'''
/***** Templates: Allowed at Most K times of Duplicates *****/

>Remove Duplicates from Sorted Array(no duplicates) :
class Solution:
  def removeDuplicates(self, nums: List[int]) -> int:

    k = 1 # Allows 0 duplicate, so k = original + 0 duplicate

    if len(nums) <= k:  return len(nums)

    # first k numbers don't matter:
    # - nums[i=0] replaced by nums[j=0]
    # - nums[i=1] replaced by nums[j=1]
    #
    i = k  # i: slow ptr
    for j in range(k, len(nums)):  # j: fast ptr
      if nums[j] != nums[i-k]:
        nums[i] = nums[j]
        i += 1
    return i

>Remove Duplicates from Sorted Array II (allow duplicates up to 2):
class Solution:
  def removeDuplicates(self, nums: List[int]) -> int:

    k = 2 # Allows 1 duplicate, so k = original + 1 duplicate

    if len(nums) <= k:  return len(nums)

    # first k numbers don't matter:
    # - nums[i=0] replaced by nums[j=0]
    # - nums[i=1] replaced by nums[j=1]
    #
    i = k  # i: slow ptr
    for j in range(k, len(nums)):  # j: fast ptr
      if nums[j] != nums[i-k]:
        nums[i] = nums[j]
        i += 1
    return i

/***** ***** ***** ***** ***** ***** ***** ***** ***** *****/

