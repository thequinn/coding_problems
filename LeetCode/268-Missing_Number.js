/*
268. Missing Number
*/

// Approach #1 Sorting
// This LeetCode Std Solution SUCKS!! Don't waste time on it!!


// Approach #2 Hash
// - This LeetCode Std Solution is brute force.  Use the following approaches


// Approach #3
// - https://leetcode.com/problems/missing-number/discuss/69916/Javascript-solution-(Beats-95)-explained
// 思路:
// - The result of XORing two duplicate numbers is always zero. 
// - Suppose 1 num is missing, we can just XOR all the numbers in the array by a counter starting in zero and ending in len-1. In this way, just the missing number will remain at the end, since all the duplicate ones will be cancelled.
//  ex. counter: 0,  1 , 2
//      nums[] : 0, [1], 2, 3 ---> No need to loop last num in nums[]
//
var missingNumber = function(nums) {
  var res = nums.length;

  for(var i = 0; i < nums.length; i++){
    res ^= i ^ nums[i];
  }
  return res;
};


// Approach #4-1
// - https://leetcode.com/problems/missing-number/discuss/210878/javascript
/* 
ex. 0, 1, 2, 4  => range is 0 ~ 4

loop {
 sum   = 0 + 1 + 2 + 4 = 7
 total = 0 + 1 + 2 + 3 = 6
}

"total" needs to add 4 to fullfill the range 0~4 for "sum"
total = 6 + 4 = 10

total - sum = 10 - 7 = 3;
*/
var missingNumber = function(nums) {
  let sum = 0, total = 0;
  
  for (let i = 0; i < nums.length; i++) {
    sum += nums[i];
    total += i;
  }
  total += nums.length;

  return total - sum;
};

// Appraoch #4-2
// - https://leetcode.com/problems/missing-number/discuss/255115/90-faster-Javascript
/*
Gauss' Formula: Sum = n * (n+1) / 2

ex. 0, 1, 2, 4  => len is 4, so range is 0 ~ 4: 0, 1, 2, 3, 4
    sum = 0 + [n * (n+1) / 2]

*/
var missingNumber = function(nums) {
  let len = nums.length;

  let sum = (len * (len+1)) / 2;
  let sum2 = nums.reduce((a,c)=> a+c); //sum of nums[]

  return sum-sum2; //difference is the answer
};


let nums = [0];
let res = missingNumber(nums);
console.log(res);
