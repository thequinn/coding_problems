/*
27. Remove Element
*/

/*:
Approach 1: Two Pointers (Slow and Fast Ptrs)
*/
var removeElement = function(nums, val) {
  let i = 0;  

  for (let j = 0; j < nums.length; j++) {
    if (nums[j] != val) {
      nums[i] = nums[j];
      i++;
    }
    console.log(nums);
  }
  return i;  
};

/*
Approach 2: Two Pointers => when elements to remove are rare
                         => QN: less intuitive, or my skill is not enough???

- For [4,1,2,3,5], val=4, the previous algorithm will do unnecessary
  copy operation of the first 4 elements.  It seems unnecessary to
  move elements [1,2,3,5] one step left as the problem description
  mentions that the order of elements could be changed.

思路：When we encounter nums[i]=val, we can swap the current element
         out with the last element and dispose the last one. This 
         reduces the array's size by 1.
Note:
- For [3,2,2,3] val=3, the last element that was swapped in could be the
  value you want to remove itself. That's why we don't increment j in
  every iter.
*/
var removeElement = function(nums, val) {

  let i = nums.length - 1;  // put slow ptr to end of nums[]
  let j = 0;

  while (j <= i) {
    if (nums[j] != val) {
      j++;
    }
    else { //(nums[j] == val) {
      nums[j] = nums[i];
      i--;  // reduce nums[] size by 1
    }
    console.log("nums:", nums, "i:", i);
  }
  return ++i;
}

 Test Case #1: 
let nums = [3,2,2,3];     let val = 3;
 Test Case #2: when elements to remove are rare
let nums = [4,1,2,3,5];   let val = 4;
console.log(removeElement(nums, val));

