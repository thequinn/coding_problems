'''
27. Remove Element

LeetCode Std Solution:
- https://leetcode.com/problems/remove-element/solution/

'''

'''
Approach 1: Two Pointers (Slow and Fast Ptrs)



思路:

(1) High-level View
- “不是要刪除的” 向左移, “要刪除的” 向右移

Usage:
- Slow ptr ptx to the index where the next “不要刪除的” to be copied to.  
- Fast ptr iter over the input arr.

(2) Low-level View:

如果 fast ptr 是 “不要刪除的”, 
  - 把這 "不要刪除的" 取代 現在 slow ptr 的位置.
  - slow & fast ptrs 都向右移.

如果 fast ptr 是 “要刪除的”
  - slow ptr 留在原處, 只有 fast ptr 向右移. 



Time complexity : O(n)
- Assume the array has a total of n elements, both i and j traverse at
   most 2n steps.

Space complexity : O(1)


ex1. if nums[fast] != val:

    val = 3
    nums = 2  3  4  2
              i  j
           2  4  3  2
                 i  j

ex2. if nums[fast] = val:

    val = 3
    nums =  3  2  4  2
           ij

            3  2  4  2
            i  j
'''
class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        slow = 0  # Slow ptr

        for fast in range(len(nums)):

            # Important! See the ex above to visualize
            if nums[fast] != val:
                nums[slow] = nums[fast]
                slow += 1

        return slow


# Approach 2: See .js =>未: 學習
