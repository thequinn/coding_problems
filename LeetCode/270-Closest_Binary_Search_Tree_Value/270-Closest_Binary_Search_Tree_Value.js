/*
270. Closest Binary Search Tree Value
*/

// Same as what QN thought -> But Approach #3 is better lol
// https://leetcode.com/problems/closest-binary-search-tree-value/discuss/425857/Javascript-solution-using-recursion
//
var closestValue = function(root, target) {
  let closestDiff = Infinity;
  let closestVal = Infinity;

  const search = (node) => {
    if (!node) return;

    if (Math.abs(node.val - target) < closestDiff) {
      closestDiff = Math.abs(node.val - target);
      closestVal = node.val;
    }

    if (target < node.val) search(node.left);
    else  search(node.right);
  }

  search(root);
  return closestVal;
};


// Approach #2, #3 -> Skipp


// Approach #3: Binary Search, O(H) time -> It explains 1st ln under while loop
//                                          in the .py version of code
// - https://leetcode.com/problems/closest-binary-search-tree-value/discuss/779685/JavaScript-Solution-Recursive-Approach
//
var closestValue = function(root, target) {
  let closest = root.val;

  return traverseBST(root);

  function traverseBST(node) {
    // base case
    if (node == null) return closest;

    closest = Math.abs(node.val - target) < Math.abs(closest - target) ? node.val : closest;

    if (closest == target) return closest;
    if (node.val > target) return traverseBST(node.left);
    if (node.val < target) return traverseBST(node.right);

    return;
  }
};

