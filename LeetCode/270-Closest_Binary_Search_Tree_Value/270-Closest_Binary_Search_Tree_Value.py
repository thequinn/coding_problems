'''
270. Closest Binary Search Tree Value

'''

class TreeNode:
  def __init__(self, val=0, left=None, right=None):
    self.val = val
    self.left = left
    self.right = right

# Approach 1: Recursive Inorder + Linear search, O(N) time -> 看不懂, 方法不好
#                                                          -> Skip
'''
class Solution_1:
  def closestValue(self, root: TreeNode, target: float) -> int:

    def inorder(r: TreeNode):
      #return (inorder(r.left) + [r.val] + inorder(r.right)) if r else []
      return inorder(r.left) + [r.val] + inorder(r.right) if r else []


    # min(iterable, *iterables, key, default)
    # - iterable - an iterable such as list, tuple, set, dictionary, etc.
    # - *iterables (optional) - any number of iterables; can be more than one
    # - key (optional) - key function where the iterables are passed and
    #   comparison is performed based on its return value
    # - default (optional) - default value if the given iterable is empty
    #
    return min(inorder(root), key = lambda x: abs(target - x))
'''

# Approach #2: Iterative Inorder, O(k) time -> Skip


'''
LC Premium Sol:
- https://leetcode.com/problems/closest-binary-search-tree-value/solution/

Video:
- https://www.youtube.com/watch?v=wued6gzSjI4
'''
# Approach #3_1: Binary Search, O(H) time
class Solution3_1:

    def closestValue(self, root: TreeNode, target: float) -> int:
        node = root
        closest = node.val
        print(f'Before while, closest: {closest}')

        while node:
          print(f'closest: {closest}, node,val: {node.val}')
          # s1. See ln-12~20 in Approach #1 for 2ns arg in min()
          # s2. See Approach #3 in .js version
          closest = min((closest, node.val), key = lambda x: abs(x - target))
          print(f'closest: {closest}')

          if node.val < target:    node = node.right
          elif node.val > target:  node = node.left
          else:                    return node.val

        return closest


class Solution3_2:

  def closestValue(self, root: TreeNode, target: float) -> int:
    closest = root.val

    while root:
      # See ln-12~20 in Approach #1 for 2ns arg in min()
      closest = min(closest, root.val, key = lambda x: abs(target - x))

      root = root.left if target < root.val else root.right

    return closest



