'''
276. Paint Fence

Tips to Analyze:
  s1. Used a simple ex to analyze
  s2. Found out its problem type is "Combinations and Permutations"
  s3. Use that type of math to create a formula


Explanation + Sol Code:
- LeetCode:  => Analysis is copied & pasted below
    https://leetcode.com/problems/paint-fence/discuss/71150/Python-solution-with-explanation
- Video:
    https://www.youtube.com/watch?v=deh7UpSRaEY&t=7s

- Analysis:
  (s1) If n == 1, there would be k-ways to paint.

  (s2) If n == 2, there would be two situations:
    2.1 You paint same color as the previous post: k*1 ways to paint, named it as "same"
    2.2 You paint differently from the previous post: k*(k-1) ways to paint this way, named it as dif
    |
    --> So, you can think, if n >= 3, you can always maintain these twos ituations: you either paint the same color as the previous one, or differently.

  (s3) If n == 3, and since there is a rule: "no more than two adjacent fence posts having the same color," we can further analyze

    Con't from 2.1, since previous two are in the same color, next one you could only paint differently.  And the number of ways to paint: same*(k-1).

    Con't from 2.2, since previous two are not the same, you can either
    - paint the same color (dif*1) ways, or
    - paint differently dif*(k-1) ways.
    |
    --> Here you can conclude, when seeing back from the next level,
        - Ways to paint the same   : dif*1 = dif
        - Ways to paint differently: same*(k-1) + dif*(k-1) = (same+dif)*(k-1)


Method #1-1: => 目前看不懂 ln-46 和 ln-51 的差別...???...???...???...
'''
class Solution:
  def numWays(self, n: int, k: int) -> int:
    if n == 0:  return 0
    if n == 1:  return k

    print("ln-X: ", k, " ", k*(k-1) )
    same, dif = k, k*(k-1) # 好像不是 swap 2 vars, 而是 multi vars assignments
    print("ln-Y: ", same, " ", dif )

    for i in range(3, n+1):
      print("ln-A: ", dif, " ", (same+dif)*(k-1) )
      same, dif = dif, (same+dif)*(k-1) # swap 2 vars
      print("ln-B: ", same, " ", dif )

    return same + dif

'''
Method #1-2:

Sol Code:
- https://leetcode.com/problems/paint-fence/discuss/117859/Python-optimized-and-not-optimized-DP-solution
'''
class Solution:
  def numWays(self, n: int, k: int) -> int:
    if n == 0:  return 0
    if n == 1:  return k

    # n=2
    same = k*1
    dif = k*(k-1)

    # n>=3
    for i in range(3, n+1):
      #same = dif*1; dif = (same+dif)*(k-1)
      tmp = dif
      dif = (same+dif)*(k-1)
      same = tmp

    return same + dif

