

'''
Approach #1: Simplied Binary Search 
             - The while loop doesn't have "=" condition
- Explanation + Template:
    https://leetcode.com/discuss/general-discussion/786126/python-powerful-ultimate-binary-search-template-solved-many-problems


思路:
This template only needs to modify 3 parts, and never need to worry about corner cases and bugs in code any more:

1. Correctly init the boundary variables left and right to specify search space. Only one rule: set up the boundary to include all possible elements.

2. Decide return value. Is it return left or return left 
- Remember this: 
    - after exiting the while loop, left is the minimal k satisfying the 
      condition function;

3. Design the condition function. 
- The most difficult and most beautiful part. Needs lots of practice.
'''
class Solution:
    def firstBadVersion(self, n: int) -> int:
        left, right = 1, n
        
        #while left <= right:  # NOT WORK for this template
        while left < right:
            mid = (left + right) // 2
            if isBadVersion(mid):   
                right = mid
            else:
                left = mid+1
        return left


# Approach #2: AlgoMonster's Template => Use Approach #1's Template instead
# - The good thing with this approach is that we don't have to modify the while loop logic in the vanilla binary search besides introducing a variable.
def firstBadVersion(self, n: int) -> int
    '''
    Boundaries "so far":
    - boundary_index: boundary b/t good and bad versions so far
    - r: 1st bad version of the bad version section so far
    - l: last good version of the good version section so far

    While loop:
    - when r != l:
        - When r and l cross each other, it means a good version is in the
          section of bad versions, or vice versa.  It's impossible to happen.
          So when it happens, it means we went thr all versions, thus break
          the loop.
    - when r == l:
        - When r and l meet each other, correct boundary found. And the next
          iter will cause r and l cross each other and break the loop.
    '''
    l, r, boundary_index = 1, n, -1
    while l <= r:
        mid = (l + r) // 2
        if isBadVersion(mid):
            boundary_index = mid
            r = mid - 1
        else:
            l = mid + 1
    return boundary_inde


