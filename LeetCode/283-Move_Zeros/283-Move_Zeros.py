'''
283. Move Zeros
'''


# https://leetcode.com/problems/move-zeroes/


'''
思路:

(1) 
Analysis:
- The problem desctiption tries to confuse you.  Actually, think of it as moving all non-zero elements to the left.

Higher View:
- When the cur ptr pts to non-zero, swap it w/ the bounrady ptr pting to 0.

Detailed view: 
- So since our goal is to move all non-zero elems to the left, we use 
    (i) ptr r to check non-zero elem. 
    (ii) ptr l to always pt to the 1st elem that's 0, like a boundary.  
- When nums[r] isn't 0, swap w/ nums[l] => A non-zero elem is moved to left. 


Video + Code:
    https://www.youtube.com/watch?v=aayNRwUN3Do

Same code as above:
    https://leetcode.com/problems/move-zeroes/solutions/1592151/python-solution-super-simple-clear-explanation/
'''    
class Solution:
    # The next 3 ln's are all the same
    # def moveZeroes(self, nums: list) -> None: 
    # def moveZeroes(self, nums: List) -> None: 
    def moveZeroes(self, nums: List[int]) -> None: 
        n = len(nums)
        i = 0
        for j in range(n):
            if (nums[j] != 0):
                nums[i], nums[j] = nums[j], nums[i]
                i += 

'''
This sol has the most votes, but is confusing => Skip!!!
    https://leetcode.com/problems/move-zeroes/solutions/562911/two-pointers-technique-python-o-n-time-o-1-space/
'''
class Solution:
    def moveZeroes(self, nums: list) -> None:
        slow = 0

        for fast in range(len(nums)):
            if nums[fast] != 0 and nums[slow] == 0:
                nums[slow], nums[fast] = nums[fast], nums[slow]
            if nums[slow] != 0:
                slow += 1


