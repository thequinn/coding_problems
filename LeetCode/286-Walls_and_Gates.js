/*
286. Walls and Gates

*****Intuition (直覺): ===> 不是指思路拆解
- Coming up with the intuition for this problem is quite simple. We know that given the 2D grid, we have to traverse through it somehow in order to mark the empty cells with their correct values. This gives us two options: BFS and DFS.

*/

/*
Approach #1: DFS

Video Explanation:
- https://www.youtube.com/watch?v=Pj9378ZsCh4

Sol Code:
- https://leetcode.com/problems/walls-and-gates/discuss/533451/Easy-and-fast-Javascript-Solution
*/
var wallsAndGates = function(rooms) {
  for(var i=0; i<rooms.length; i++){
    for(var j=0; j<rooms[0].length; j++){

      // If cur cell is a gate, use DFS to see how deep it can reach
      if(rooms[i][j] == 0){
        // - 4th param: track distance we're at in each of our recursive calls
        // - When we call dfs() on our "gate," we're at distance 0 to begin w/
        dfs(rooms, i, j, 0);
      }
    }
  }
};

var dfs = function dfs(grid, i, j, count){
  // Check (1) boundries of rows and cols (2) if the distance we have, d, is
  // worse than the distance that's in the cell, we don't want to update it,
  // b/c it's a worse distance.
  if(i<0 || i>= grid.length|| j<0|| j>=grid[0].length || count>grid[i][j])
    return;

  // Set the distance from (i, j) to the gate that we call dfs() from
  grid[i][j] = count;

  // Visit (i, j)'s neighbors
  dfs(grid, i+1, j, count+1)
  dfs(grid, i-1, j, count+1)
  dfs(grid, i, j+1, count+1)
  dfs(grid, i, j-1, count+1)
};


/*
Approach #2: BFS

思路:
- https://leetcode.com/problems/walls-and-gates/discuss/600228/JavaScript-Clean-Solution
- Since a cell may be between two gates, we want to ensure we mark it with whichever path is shorter, so we need to utilize a BFS, such that at each gate, we move outward by no more than 1 cell (up, down, left, or right). This way, whichever traversal marks a cell first, must be the shortest!

Sol Code: Sam as above
*/
var wallsAndGates = function(rooms) {
  if (!rooms || rooms.length === 0)   return;

  let queue = [];

  for (let row = 0; row < rooms.length; row++){
    for (let col = 0; col < rooms[0].length; col++){
      // If a cell is a gate, add it to the queue, so we can start searching
      // from all gates.
      if (rooms[row][col] == 0)
        queue.push([row, col]);
    }
  }

  while (queue.length){
    const [x, y] = queue.shift();  // Get cur room

    //**Note: cur room: rooms[x][y], cur room's 4 neighbors: rooms[x2][y2]

    // Visit cur room's neighbors in 4 directions
    for (let [x2, y2] of [[x + 1, y], [x, y + 1], [x - 1, y], [x, y - 1]]) {

      // If cur neighbor is (1) in bound && (2) an empty room
      // INF = 2147483647: empty room
      if (x2 >= 0 && x2 < rooms.length && y2 >= 0 && y2 < rooms[0].length && rooms[x2][y2] == 2147483647) {

        // Assign distance to this neighbor by increment cur room's val by 1.
        // This is b/c the neighbor is 1 step away from cur room
        rooms[x2][y2] = rooms[x][y] + 1;

        // Add this neighbor to queue to search the next empty cells.
        queue.push([x2, y2]);
      }

    } // End of inner-for
  }  // End of outer-while

};

