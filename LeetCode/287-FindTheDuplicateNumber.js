/*
287. Find the Duplicate Number

Task:
  Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive), prove that at least one duplicate number must exist. Assume that there is only one duplicate number, find the duplicate one.

Note:
- You must not modify the array (assume the array is read only).
- You must use only constant, O(1) extra space.
- Your runtime complexity should be less than O(n2).
- There is only one duplicate number in the array, but it could be repeated
  more than once.

LeetCode Std Solution:
- https://leetcode.com/problems/find-the-duplicate-number/solution/
- Approaches #1 & #2 do not satisfy the constraints, but they are sol's you
  might be likely to come up with during a technical interview.
*/

// Approach #1: Sorting
// - Time complexity : O(nlgn)
// -- The sort invocation costs O(nlgn) time in Python and Java, so it
//    dominates the subsequent linear scan.
// - Space complexity : O(1) or O(n)
// -- Here, we sort nums in place, so the memory footprint is constant. If
//    we cannot modify the input array, then we must allocate linear space
//    for a copy of nums and sort that instead.
// 
/*const findDuplicate = (nums) => {
  // Array.sort() sorts elems in place.  If elems are strings, no need to
  // write my comparable callback.  
  nums.sort(function(a, b) {
    return a - b;
  });
  console.log("nums:", nums);

  for (let i = 1; i < nums.length; i++) {
    if (nums[i] == nums[i-1])
      return nums[i];
  }
  return -1
};*/

// Approach #2: Set
// - 思路：
// -- In order to achieve linear time complexity, we need to be able to
//    insert elements into a DS (and look them up) in constant time.
//
// - Time complexity : O(n)
// -- Set in both Python and Java rely on underlying hash tables, so
//    insertion and lookup have amortized constant time complexities. The
// algorithm is therefore linear, as it consists of a for loop that performs
// constant work n times.
// 
// - Space complexity : O(n)
// -- In the worst case, the duplicate element appears twice, with one of
//    its appearances at array index n−1. seen will contain n−1 distinct
//    values, and will therefore occupy O(n) space.
//    
/*const findDuplicate = (nums) => {
  let seen = new Set();

  for (n of nums) {
    if (seen.has(n))
      return n;
    seen.add(n);
  }
}*/

// Approach #3: Floyd's Tortoise and Hare (Cycle Detection)
// - 思路: Our goal is to find out the entry of the loop
// - https://leetcode.com/problems/find-the-duplicate-number/discuss/134813/javascript-soution%3A-beats-99.35-runtime-56ms.-Using-linked-list
//
const findDuplicate = function(nums) {
  let slow = nums[0], fast = nums[nums[0]];
  //here fast moves two steps at a time
  while(slow!==fast){
    slow = nums[slow];
    fast = nums[nums[fast]];
  }
  //console.log("slow:", slow, ", fast:", fast);

  fast=0;
  //here fast move one step at a time
  while(slow!==fast){
    slow = nums[slow];
    fast = nums[fast];
  }
  return slow;
};

console.log( findDuplicate([1,3,4,2,2]) );  // 2
//console.log( findDuplicate([3,1,3,4,2]) );  // 3
