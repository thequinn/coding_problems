/*
3. Longest Substring Without Repeating Characters


Note: See README.txt

LeetCode Std Solution:
- https://leetcode.com/problems/longest-substring-without-repeating-characters/solution/

*/

// Approach #1: Brute Force  ==> Time Limit Exceeded
// - Time complexity: O(n^3)
// - Space complexity: O(k), k is the size of the set
//
/*var lengthOfLongestSubstring = (s) => {
  let ans = 0;

  for (let i = 0; i < s.length; i++) {
    // Notice the condition: <=
    for (let j = i + 1; j <= s.length; j++) {
      if (allUnique(s, i, j))
        ans = Math.max(ans, j - i);
    }
  }
  return ans;
};

var allUnique = (s, start, end) =>  {
  let set = new Set();

  for (let i = start; i < end; i++) {
    if (set.has(s.charAt(i)))
      return false;
    set.add(s.charAt(i));
  }
  return true;
}*/

//--------------------------------------------------------

// Approach #2: Sliding Window
// - 思路:
// -- In Approach #1, we repeatedly check a substring to see if it has duplicate character. But it is unnecessary. In substring s[i,j], where index i to j-1 is already checked to have no duplicate chars, we only need to check if s[j] is already in the substring.
// -- By using a Set as a sliding window, checking if a character in the current can be done in O(1).

// Version #2-1: Code by QN based on the 思路 in Approach #2
// - Time complexity: O(n^2)
//
/*var lengthOfLongestSubstring = (str) => {
  let ans = 0, res = 0;

  for (let i = 0; i < str.length; i++) {
    let set = new Set();
    set.add(str[i]);
    ans = 1;

    for (let j = i + 1; j < str.length; j++) {
      if (!set.has(str[j])) {
        set.add(str[j]);
        ans++;
      }
      else    break;
    }
    res = Math.max(ans, res);
  }
  return res;
};*/

// Version #2-2: LeetCode Std Solution
// - 思路: con't
// -- Sliding Window:
// --(1) It is an abstract concept commonly used in array/string problems. A window is a range of elem's in the array/string which usually defined by the start and end indices, i.e. [i,j) (left-closed, right-open)
// --(2) We use Set to store the chars in current window [i,j) (j=i initially).
// --(3) Then we slide the index j to the right. If it is not in the Set, we slide j further.
//
// - Time complexity: O(2n) = O(n)
// -- In the worst case each character will be visited twice by i and j.
// - Space complexity: O(k), k is size of the set
//
/*var lengthOfLongestSubstring = (str) => {
  let n = str.length;
  let set = new Set();
  let ans = 0, i = 0, j = 0;

  while (i<n && j<n) {
    if (!set.has(str[j])) {
      set.add(str[j++]);
      ans = Math.max(ans, j-i);
    }
    else {
      set.delete(str[i++]);
    }
  }

  return ans;
};*/

//--------------------------------------------------------

// Approach #3-1: Sliding Window Optimized
// -
// - 思路:
// -- Approach #2 requires at most 2n steps. In fact, it could be optimized to require only n steps.
// -- Instead of using a set to tell if a char exists, we could define a mapping of the chars to its index. Then we can skip the chars immediately when we found a repeated char.
// -- The reason is that if there's a duplicate at index j' in the range [i,j), we can skip all the elements in the range [i,j′] and let i  be j′+1.
//
// - Time complexity: O(n), Index j will iterate n times.
// - Space complexity: O(k), k is size of the map
//
var lengthOfLongestSubstring = function(s) {
  // keeps track of the starting index of the current substring.
  var start = 0;

  var maxLen = 0;

  // keeps track of the most recent index of each letter.
  var map = new Map();

  for (var i = 0; i < s.length; i++) {
    var ch = s[i];

    console.log("\nmap.get(",ch, "):", map.get(ch));
    console.log("before if, start:", start);
    // Found a duplicate b/c key ch is found in map{}
    // (Don't worry about the val of the key for now)
    if (map.get(ch) >= start) {

      // Reset the starting index of the substr: (last index of ch) + 1
      start = map.get(ch) + 1;
      console.log("in if, start:", start);
    }

    console.log("Before, map:", map);
    // updates last recorded index of letter to the most recent index.
    map.set(ch, i);
    console.log("After,  map:", map);

    // +1 because if your substring starts and ends at index 0, it still
    // has a length of 1.
    maxLen = Math.max(i - start + 1, maxLen); // Same as if below
    /*if (i - start + 1 > maxLen) {
      maxLen = i - start + 1;
    }*/
  }

  return maxLen;
};

// Approach #3-2: Shorter version of Approach #3-1
// - https://leetcode.com/problems/longest-substring-without-repeating-characters/discuss/2291/9-line-JavaScript-solution
//
/*const lengthOfLongestSubstring = s => {
  const map = {};
  var left = 0;

  return s.split('').reduce((max, v, i) => {
    left = map[v] >= left ? map[v] + 1 : left;
    map[v] = i;

    return Math.max(max, i - left + 1);
  }, 0)
};*/

//--------------------------------------------------------

//let str = "abcabcbb";   // 3
let str = "abcbb";   // 3
//let str = "bbbbb";    // 1
//let str = "pwwkew";   // 3

let res = lengthOfLongestSubstring(str);
console.log(res);

