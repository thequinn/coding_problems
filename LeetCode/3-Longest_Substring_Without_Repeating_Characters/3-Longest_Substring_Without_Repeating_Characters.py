'''
3. Longest Substring Without Repeating Characters

LeetCode Std Solution:
- https://leetcode.com/problems/longest-substring-without-repeating-characters/solution/

'''

# Approach #1: Brute Force  ==> Time Limit Exceeded
'''
def lengthOfLongestSubstring(self, s: str) -> int:
    def check(start, end):
        chars = [0] * 128
        for i in range(start, end + 1):
            c = s[i]
            chars[ord(c)] += 1
            if chars[ord(c)] > 1:
                return False
        return True

    n = len(s)

    res = 0
    for i in range(n):
        for j in range(i, n):
            if check(i, j):
                res = max(res, j - i + 1)
    return res
'''

#--------------------------------------------------------

# Approach #2: Sliding Window
'''
Sol Code:
    https://leetcode.com/problems/longest-substring-without-repeating-characters/solutions/1731/a-python-solution-85ms-o-n/?orderBy=most_votes
    => See commenter, coding_minion98's code instead

基本思路:
The r ptr stop to extend the window (mv right) when there is a duplicate, then the l ptr con't to contract the window (mv right) until there is NO duplicate.

細節思路:

Traversing the input s
=> Regular Case,  ex. "abcbb"
- If (1) cur char, ch, is in last_seen, and (2) l_ptr is on the left side of
  the last occurrence of the ch or l_ptr pts to same pos as r_ptr (this happens at iter=0),
    - For (2), it means the window includes 2 occurrences of the ch.
    - The older occurrence is not needed. Update l_ptr to 1 pos to the
      right of it.
=> Corner Case: "pwwp"
- If (1) cur char, ch, is in last_seen, and (2) l_ptr is on the right side of the last occurrence of the ch,
    - For (2) it means the window only includes the cur ch.  No need to update
      l_ptr

- Else, update max len
- Update r_ptr by adding/updating cur char and its index

'''

def lengthOfLongestSubstring(self, s: str) -> int:
    last_seen = {}
    l_ptr = max_len = 0

    for r_ptr, ch in enumerate(s):
        if ch in last_seen and l_ptr <= last_seen[ch]:
            l_ptr = last_seen[ch] + 1
        else:
            max_len = max(max_len, r_ptr - l_ptr + 1)
        last_seen[ch] = r_ptr
    return max_len

#--------------------------------------------------------

# 重要! This test case shows the corner case in if
#str = "pwwp";    # 2

# Regular case
#str = "abcbb";   # 3

sol = Solution()
res = sol.lengthOfLongestSubstring(str);
print("\nres: ", res)
