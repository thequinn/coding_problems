/* 300. Longest Increasing Subsequenceva
注意:
>Approach #4: LeetCode Std Solution 答案不易懂, 用以下資源

//=======================================================

>思路分析: --->解法基礎: Patience Sort
           - A Greedy Algorithm, meaning always  taking the curr best step!

(1) https://leetcode.com/problems/longest-increasing-subsequence/discuss/74824/JavaPython-Binary-search-O(nlogn)-time-with-explanation
- 思路和(2) 同, 但code 暫時看不懂

(2) https://leetcode.com/problems/longest-increasing-subsequence/discuss/223258/JavaScript-Binary-Search-Nlog(N)


- Our strategy determined by the following conditions:

tails[] tracks curr best sol.  It stores the smallest tail of all increasing subsequences with length i+1 in tails[i].

ex. say we have nums = [4,5,6,3], then all the available increasing subsequences are:

len = 1   :      [4], [5], [6], [3]   => tails[0] = 3
len = 2   :      [4, 5], [5, 6]       => tails[1] = 5
len = 3   :      [4, 5, 6]            => tails[2] = 6

*** We can easily prove that tails is a "increasing array." Therefore it is possible to do a "binary search" in tails[] to find the one needs update.


//------------------------

>思路整理: --->解法基礎: Patience Sort
           - A Greedy Algorithm, meaning always  taking the curr best step!

1. If A[i] < head, tails[0], replace the head. It means we discard the old subseq tails[], and starts a new one.

2. If A[i] > tail, tails[len-1], add A[i] to end of tails[].  It means we keep working on the curr subseq, tails[].

3. If A[i] is in b/t smallest and largest of tails[], find the celing num in tails[] and replace it with A[i].

  ex. Find ceiling of 3 in {-1,2,5,8} ?
      A: We found 2 < 3 < 5.  So ceiling is 5, replace 5 w/ 3.

//------------------------

>Theories involved:  =====>可以不看

s1. Patience Sort:

- (i)  dwhelper/Algorithms/Patience_Sort-LeetCode_300_Longest Increasing Subsequence-O(nlogn)   ==> This is NOT Tushar's video!!!
  |
   ==> 最重點!! 把 input arr 撲克牌視為 1D array: 只看左到右,目前最下一排
  |
  ==> 難懂: 換成只有一個array:
             - (i)  把 input arr 撲克牌視為 1D array: 只看左到右,目前最下一排
             - (ii) 當目前數字,E, 在於 1D array 最大和最小之中, 找到
                    floor_x < E <= ceiling_y, 然後用 E 取代 ceiling_y

             Q: Find ceiling of 3 in {-1,2,5,8} ?
             A: We found 2 < 3 < 5.  So ceiling is 5, replace 5 w/ 3.


- (ii) Algorithms-Anna/Sorting_and_Searching/Patience_Sort_(Greedy_Algorithm).pdf


s2. Binary Search:
- Algorithms-Anna/Sorting_and_Searching/Binary_Search/Binary_Search-Less_Comparisons_&_Ceiling_Floor_Pos


s3. Tushar's Video:  =====>>>>>不要看嗎講得亂七八糟,更困惑!!!!!
(1) https://www.youtube.com/watch?v=S9oUiVYEq7E
(2) /dwhelper/LeetCode/LeetCode 300 - Longest Increasing Subsequence in nlogn time - Tushar.mp4

----------------------------------------------------

*/

// Approach #4: Dynamic Programming with Binary Search
//
// - Explanation & Solution Code:
// -- https://leetcode.com/problems/longest-increasing-subsequence/discuss/223258/JavaScript-Binary-Search-Nlog(N)
//
var lengthOfLIS = function(nums) {

  if(!nums.length) return 0;

  const tails = [];
  tails[0] = nums[0];

  for(let i = 1; i < nums.length; i++) {

    // Replace current nums[i] with head if it's smaller
    if(nums[i] <= tails[0]) {
      tails[0] = nums[i];
    }
    // If current nums[i] is bigger than the largest value we've recorded
    // we can extend our tails by current nums[i]
    else if(nums[i] > tails[tails.length-1]) {
      tails.push(nums[i]);
    }
    // If nums[i] is b/t smallest and largest elem's of tails[],
    // - Use binary search (BS) to find the insertion point of current nums[i].
    //   Return r b/c we're looking to replace elem of tails[] that's > nums[i]
    //
    // 注意: There're 2 versions of BS: (1) Typical (2) Less comparison
    // - https://www.geeksforgeeks.org/the-ubiquitous-binary-search-set-1/?ref=lbp
    // -- This first 2 "Problem Statement" in the link explains how to use BS
    //    to find floor and ceiling
    else {
      let l = 0, r = tails.length-1;

      while(l < r) {
        // 技巧: JS bitwise operators, >> and <<, convert a float to an int
        //const mid = (l+r)/2 >> 0;
        //
        const mid = l + Math.floor( (r-l)/2 ); // Same as last ln
        console.log("l, r, mid:", l, r, mid);

        /*** 背起來!!! ***/
        if(tails[mid] >= nums[i]) {
          r = mid
        } else {
          l = mid + 1;
        }
      }

      tails[r] = nums[i];
    } // End of if-else

  } // End of for loop

  return tails.length;
};

//let nums = [4,10,4,3,8,9];
let nums = [3,5,6,2,5,4,19,5,6,7,12];
console.log(lengthOfLIS(nums));
