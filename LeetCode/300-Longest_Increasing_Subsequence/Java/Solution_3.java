/*
300. Longest Increasing Subsequence

注意:
>Approach #1, #2: LeetCode Std Solution 答案不易懂, 不學

//-----------------------------
思路: (同樣video 存在兩個地點)

>Approach #3: O(n^2) Time:
-(1) https://www.youtube.com/watch?v=CE2b_-XfVDk
-(2) /dwhelper/LeetCode/LeetCode 300 - Longest Increasing Subsequence - Tushar.mp4

//-----------------------------
Solution Code: (同樣 Solution Code 存在兩個地點)

- Java Version for Approach #3:

  -(1) /Users/asun/Library/Mobile\ Documents/com~apple~CloudDocs/刷题-網站-LeetCode/Solutions/bephrem1-backtobackswe  -> DP  -> LongestIncreasingSubsequence

  -(2) https://github.com/bephrem1/backtobackswe  -> DP  -> LongestIncreasingSubsequence

*/

// Approach #3:
import java.util.Arrays;

class Solution_3 {
  public int lengthOfLIS(int[] nums) {
    if (nums.length == 0) {
      return 0;
    }

    // DP part:
    // - Array to store our subproblems, default answer is 1. Each index records
    //   the answer to "what is the longest increasing subsequence ending at
    //   index i of the original array?"
    //
    int[] maxLen = new int[nums.length];
    Arrays.fill(maxLen , 1);

    // By default the best answer is a length of 1
    int max = 1;

    // Test every possible end index of a longest increasing subsequence
    for (int i = 1; i < nums.length; i++) {

      // - maxLen[i]: The best answer so far for the LIS from 0...i
      //
      // - maxLen[j] + 1:
      // -- maxLen[j]: The best answer so far for the LIS from 0...j
      //
      for (int j = 0; j < i; j++){
        if (nums[i] > nums[j]) {
          maxLen[i] = Math.max(maxLen[i], maxLen[j] + 1);
        }
      }
      // Print maxLen[] and max
      for (int n: maxLen)  System.out.print(n + ", ");
      System.out.print("\nmax:" + max + "\n");

      // Update the best answer for maxLen[0...i] so far (up to curr i).
      max = Math.max(max, maxLen[i]);
      System.out.println("---> ln-46, max:" + max + "\n");
    }

    return max;
  }

  public static void main(String args[]) {
    int arr[] = {2,2}; //1
    //int arr[] = {10,9,2,5,3,7,101,18}; //4

    Solution_3 sol = new Solution_3();
    int res = sol.lengthOfLIS(arr);

    System.out.println("\nres:" + res);
  }
}

