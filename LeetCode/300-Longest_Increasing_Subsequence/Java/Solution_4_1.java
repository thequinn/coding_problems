/*
300. Longest Increasing Subsequence

Note: 思路看 Solution_4.js

*/

// Approach #4_0: Dynamic Programming with Binary Search
//
// - Explanation & Solution Code:
// -- https://leetcode.com/problems/longest-increasing-subsequence/discuss/74824/JavaPython-Binary-search-O(nlogn)-time-with-explanation

class Solution {

  public int lengthOfLIS(int[] nums) {
    int[] tails = new int[nums.length];
    int size = 0;

    for (int x : nums) {
      int i = 0, j = size;

      while (i != j) {
        int m = (i + j) / 2;
        if (tails[m] < x)
          i = m + 1;
        else
          j = m;
      }
      tails[i] = x;
      if (i == size) ++size;
    }

    return size;
  }

}

//=======================================================
/*** Approach #4_1 太長了, 用Approach #4_0 instead! ***/

// Approach #4_1: Dynamic Programming with Binary Search
class Solution_4_1 {
  static int lengthOfLIS(int A[]) {
    if (A.length == 0)    return 0;

    int[] tailTable = new int[A.length];
    int len; // always points to an empty slot

    // Add boundary case, when array size is one
    tailTable[0] = A[0];
    len = 1; // len表示当前最长的升序序列长度

    for (int i = 1; i < A.length; i++) {
      System.out.println("\n---->i:" + i + ", len:" + len);

      // Case-1: (See ln-32 解釋)
      if (A[i] < tailTable[0]) {
        tailTable[0] = A[i]; // new smallest value
        System.out.println("ln-87, tailTable[0] = A[i]");
      }
      // Case-2: (See ln-32 解釋)
      else if (A[i] > tailTable[len-1]) {
        // A[i] wants to extend largest subsequence
        tailTable[len] = A[i];
        System.out.println("ln-93, A[i]:" + A[i] + ", tailTable[len]:" +
                            tailTable[len]);
        len++;
        System.out.println("ln-96, len:" + len);
      }
      // Case-3: (See ln-32 解釋)
      else {
        // - binarySearch() finds the ceiling index: 解釋看 Q&A ln-53
        //
        // - binarySearch(..., -1, len-1, ...)
        // -- -1:
        //  That's related to loop invariant.  There're two cases for l,
        //  either it points to a value < the target value, or -1.  r always
        //  points to a value >= the target value.
        //
        //  During the loop, the distance b/t r and l shrinks. So while
        //  r - l = 1, r is the smallest value >= the target value.
        //  If you set l to 0, l may not point to a value smaller than the
        //  target and the invariant breaks.
        // -- len-1:
        //  curr filled elems of tailTable[] is up to len-1
        System.out.println("ln-114, binarySearch()");
        tailTable[ binarySearch(tailTable, -1, len-1, A[i]) ] = A[i];
      }
      printArr(tailTable);
    }
    return len;
  }

  // - Modified Binary Search (Note: boundaries in the caller) to find ceiling
  //
  // - This version of Binary Search has less comparisons (需要學起來)
  // -- The BS code is explained in dir below: -> "Problem statements" 必看!
  // -- Algorithms-Anna/Binary_Search-Less_Comparisons_&_Ceiling_Floor_Pos
  //  |
  //  -- The first 2 "Problem Statement" in the link explains how to use BS to
  //     find floor and ceiling

  //
  static int binarySearch(int A[], int l, int r, int key) {
    System.out.println("l:" + l + ", r:" + r + ", key:" + key);

    System.out.println("Before while, r-l:" + (r-l));
    while (r - l > 1) {
    System.out.println("\nAfter while, r-l:" + (r-l));

      int m = l + (r - l) / 2;
      System.out.println("m:" + m + ", A[m]:" + A[m] + ", key:" + key);

      if (A[m] >= key) {
        r = m;
        System.out.println("ln-140, A[m] >= key" + ", r = m: " + m);
      }
      else {
        l = m;
        System.out.println("ln-144, A[m] < key" + ", l = m: " + m);
      }
    }
    System.out.println("ln-147, return r:" + r + "\n");
    return r;
  }

  static void printArr(int arr[]) {
    for (int elem: arr) {
      System.out.print(elem + " ");
    } System.out.println();
  }

  // Driver program to test above function
  public static void main(String[] args) {
    //int A[] = { 2, 5, 3, 7, 11, 8, 10, 13, 6 };
    int A[] = { 2, 8, 7, 11, 9, 10};

    System.out.println("\nLength of Longest Increasing Subsequence is " +
        lengthOfLIS(A));
  }
}

//---------------------------------------------

// Approach #4-1: "Clean"` Version
//
class Solution_4_1 {
  static int lengthOfLIS(int A[]) {
    if (A.length == 0)    return 0;

    int[] tailTable = new int[A.length];
    int len;

    tailTable[0] = A[0];
    len = 1; // len 表示当前最长的升序序列长度

    for (int i = 1; i < A.length; i++) {
      // Case-1:
      if (A[i] < tailTable[0]) {
        tailTable[0] = A[i]; // new smallest value
      }
      // Case-2:
      else if (A[i] > tailTable[len-1]) {
        tailTable[len] = A[i];
        len++;
      }
      // Case-3:
      else {
        tailTable[ binarySearch(tailTable, -1, len-1, A[i]) ] = A[i];
      }
    }
    return len;
  }

  static int binarySearch(int A[], int l, int r, int key) {
    while (r - l > 1) {
      int m = l + (r - l) / 2;
      if (A[m] >= key)
        r = m;
      else
        l = m;
    }
    return r;
  }

  // Driver program to test above function
  public static void main(String[] args) {
    //int A[] = { 2, 5, 3, 7, 11, 8, 10, 13, 6 };
    int A[] = { 2, 8, 7, 11, 9, 10};

    System.out.println("\nLength of Longest Increasing Subsequence is " +
        lengthOfLIS(A));
  }
}

