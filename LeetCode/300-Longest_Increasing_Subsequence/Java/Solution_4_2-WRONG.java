/*
300. Longest Increasing Subsequence

思路:
- See Solution_4_1.java


Solution Code from Segmentfault.com:
- https://segmentfault.com/a/1190000003819886

- 注意: 這頁的 ex 有點錯誤 (下方 Comment 有幫助更正), 但看G4G 的解釋就很好!

- Solution_4_2.java 看起來好像對,但不能處理 input A[] = [2,2]:
    (1) See: binarySearch(tailTable, -1, len - 1, A[i])
        - Solution_4-1.java: 使用Binary Search 範圍是 -1 ~ len-1.
        - Solution_4-2.java: 使用Binary Search 範圍是  0 ~ len

    (2) 兩個 binarySearch() 有點不同
        - Solution_4_2.java: 這版因以下故不能處理 case A[] = [2,2]
            (i ) Calling binarySearch() w/ range 0~len
            (ii) binarySearch()
*/

// Approach #4_2: Dynamic Programming with Binary Search
public class Solution_4_2 {

  static int binarySearch(int[] tails, int min, int max, int target) {
    while(min <= max){
      int mid = min + (max - min) / 2;

      if(tails[mid] == target)  return mid;
      if(tails[mid] < target)   min = mid + 1;
      if(tails[mid] > target)   max = mid - 1;
    }
    return min;
  }

  static int lengthOfLIS(int[] nums) {
    if(nums.length == 0)    return 0;

    // len表示当前最长的升序序列长度（为了方便操作tails我们减1）
    int len = 0;

    // tails[i]表示长度为i的升序序列其末尾的数字
    int[] tails = new int[nums.length];

    tails[0] = nums[0];

    // 根据三种情况更新不同升序序列的集合
    for(int i = 1; i < nums.length; i++){
      if(nums[i] < tails[0]){
        tails[0] = nums[i];
      } else if (nums[i] >= tails[len]){
        tails[++len] = nums[i];
      } else {
        // 如果在中间，则二分搜索
        tails[ binarySearch(tails, 0, len, nums[i]) ] = nums[i];
      }
    }
    return len + 1;
  }

  public static void main(String args[]) {
    int A[] = { 2, 5, 3, 7, 11, 8, 10, 13, 6 };

    Solution_4_2 sol = new Solution_4_2();
    int res = sol.longestIncreasingSubsequence(A);
    System.out.println("res:" + res);
  }

}
