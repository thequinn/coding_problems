'''
303. Range Sum Query - Immutable

Concept of Prefix Sum & Suffix Sum (= Range Sum):
- _模板+題型分類+範例/Prefix_Sum_\&_Suffix_Sum
   
Explanation + Sol:
  https://leetcode.com/problems/range-sum-query-immutable/solutions/3238729/303-solution-with-step-by-step-explanation/

'''

class NumArray:

    def __init__(self, nums: List[int]):
        self.prefix_sum = [0] * (len(nums) + 1)
        for i in range(len(nums)):
            self.prefix_sum[i + 1] = self.prefix_sum[i] + nums[i]

    def sumRange(self, left: int, right: int) -> int:
        return self.prefix_sum[right + 1] - self.prefix_sum[left]

