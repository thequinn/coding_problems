'''
309. Best Time to Buy and Sell Stock with Cooldown
'''

'''
Approach: State Machine

Sol Code & Explaination: ==> See: State Machine illustration
- https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/discuss/761981/PythonGo-O(n)-by-DP-and-state-machine.-w-Visualization

Note: the var names and state names below are modified from above tutorial for better understanding.

See: State_Machine .png in same dir

Formula:  cur state = pre state + action


The state machine has 3 states 

1) hold  
- To enter a hold state,
    a) pre hold state     + continue to hold
    b) pre cooldown state + buy action

2) noHold 
- To enter a noHold state,
    a) pre hold state + sell action

3) cooldown 
- To enter a cooldown state,
    a) pre noHold state + No action needed (bc a noHold state is caused be a 
       sell action and followed by 1 cooldown state
    b) pre cooldown state

'''
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        cooldown = 0
        noHold = 0
        hold  = -float('inf')
        
        for p in prices:
            pre_cooldown, pre_noHold, pre_hold = cooldown, noHold, hold

            cooldown = max(pre_cooldown, pre_noHold)
            noHold = pre_hold + p
            hold = max(pre_hold, pre_cooldown - p)
        
        # The state of final trading day must be either noHold or cooldown state
        return max(noHold, cooldown)



'''
Approach: DP + Tree Structure

NeetCode: => QN: hard to comprehend...maybe bc my level is too low now...???
- Video: https://www.youtube.com/watch?v=I7j0F7AHpb8
- Code: https://github.com/neetcode-gh/leetcode/blob/main/python%2F0309-best-time-to-buy-and-sell-stock-with-cooldown.py

  The DP technique uses Caching:
    dp = {}  # key = (day, T/F); val = max_profit

    Day:
        If Buy  -> i + 1
        If Sell -> i + 2  (Need 1 extra day of cooldown)

    State: Buying or Selling?
        True : Buying
        False: Selling
        Staying in the same state: cooldown (= not doing anything)      
'''
class Solution:
    def maxProfit(self, prices: List[int]) -> int:

        dp = {}  # key = (day, T/F); val = max_profit

        def dfs(i, buying):
            # If input arr is empty
            if i >= len(prices):  return 0

            ''' If already in cache '''
            if (i, buying) in dp:
                return dp[(i, buying)]

            ''' If not in cache '''
            # 
            if buying:
                buy = dfs(i+1, not buying) - prices[i]
                # This ln is redundant in if-else statements, can move above if
                cooldown = dfs(i + 1, buying)
                dp[(i, buying)] = max(buy, cooldown)
            else: 
                sell = dfs(i + 2, not buying) + prices[i]
                cooldown = dfs(i + 1, buying)
                dp[(i, buying)] = max(sell, cooldown)
            return dp[(i, buying)]

        return dfs(0, True)
