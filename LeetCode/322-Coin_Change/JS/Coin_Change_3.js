/*
322. Coin Change

Note: LeetCode Std Sol 很爛,雖然它rating很高,但很難懂!

-------------------------------------------------

解法 + 優化進程:
- Recursion DFS
- DP Top-Down Memorization ==> 使用 DP Top-Down Memo 來優化 DFS
- DP Bottom-Up
- Optimization for DP Bottom-Up

-------------------------------------------------

思路 for Approach #1~#3: (同樣video 存在兩個地點)
-(1) https://www.youtube.com/watch?v=jgiZlGzXMBw
-(2) /dwhelper/LeetCode/LeetCode 322 - Coin Change.mp4

-------------------------------------------------

Solution Code: (同樣 Solution Code 存在兩個地點)

- Java Version for Approach #1~#3:
  -(1) /Users/asun/Library/Mobile\ Documents/com~apple~CloudDocs/刷题-網站-LeetCode/Solutions/bephrem1-backtobackswe  -> DP  -> ChangeMaking
  -(2) https://github.com/bephrem1/backtobackswe  -> DP  -> ChangeMaking

- JS Version for Appraoch #3, #4:
  - https://leetcode.com/problems/coin-change/discuss/133487/Clean-JavaScript-solution

*/

// Approach #3: DP:Bottom-Up
// - 思路:
// -- Need the answers to prev sub-problems, so we can build the final result
//
const coinChange = (coins, amount) => {
  // dp[i]: the least amount of coins that can make the value equals to the i
  const dp = Array(amount + 1).fill(Infinity);
  dp[0] = 0;

  for (let i = 1; i <= amount; i++) {
    for (const coin of coins) {
      if (i - coin >= 0) {
        dp[i] = Math.min( dp[i], dp[i - coin] + 1);
      }
    }
  }
  return dp[amount] === Infinity ? -1 : dp[amount];
};
