/*
322. Coin Change

Note: LeetCode Std Sol 很爛,雖然它rating 很高,但很難懂!

-------------------------------------------------

解法 + 優化進程:
- Recursion DFS
- DP Top-Down Memorization ==> 使用 DP Top-Down Memo 來優化 DFS
- DP Bottom-Up
- Greedy + DFS + Pruning

-------------------------------------------------

思路 for Approach #1~#3: (同樣video 存在兩個地點)
-(1) https://www.youtube.com/watch?v=jgiZlGzXMBw
-(2) /dwhelper/LeetCode/LeetCode 322 - Coin Change.mp4

-------------------------------------------------

Solution Code: (同樣 Solution Code 存在兩個地點)

- Java Version for Approach #1~#3:
  -(1) /Users/asun/Library/Mobile\ Documents/com~apple~CloudDocs/刷题-網站-LeetCode/Solutions/bephrem1-backtobackswe  -> DP  -> ChangeMaking
  -(2) https://github.com/bephrem1/backtobackswe  -> DP  -> ChangeMaking

- JS Version for Appraoch #3, #4:
  - https://leetcode.com/problems/coin-change/discuss/133487/Clean-JavaScript-solution

*/

// Approach #4: DFS + Pruning ==> Not real Greedy Algorithm
//
// - Time & Space Analysis:
// -- https://leetcode.com/problems/coin-change/discuss/406501/C++-Greedy-+-Pruning.-20-ms.-Beats-97.40.-(No-DP)
//
// - Time Complexity:
// -- O(NlogN) for sorting, and O( N^(S/N) ) for search in worst case (every
//    coin value is relatively close such as {1e3, 1e3+1, 1e3+2, ..., etc} and
//    S is relatively large such as 1e9), where N is coins' number and S is the
//    target value. Other than this extreme case, this algorithm is fast.
//
// - Space Complexity:
// -- Since memory is only spent on fucntions during DFS search, the space
//    complexity is the same with time complexity and will be O( N^(S/N) ) for
//    the same worst case.
//
const coinChange = (coins, amount) => {

  const find = (amount, accuCoins, coinIndex) => {
    const coin = coins[coinIndex];
    let count = ~~(amount / coin);  // "~~": faster Math.floor()

    // Base Case:
    if (coinIndex === coins.length - 1) {
      if (amount % coin === 0) {
        res = Math.min(res, accuCoins + count);
      }
      return res;
    }

    // Recursive Case:

    // - (accuCoins + i) < res: for pruning, avoid unnecessary calc
    //
    // - i--: backtrack 1 coin change to increase amount
    //
    for (let i = count; i >= 0 && (accuCoins + i) < res; i--) {

      // 1st param: Take the max num of coin change
      // 2nd param: Update accumulated num of coin change
      find(amount - coin * i, accuCoins + i, coinIndex + 1);
                                             // 注意: ++coinIndex 不對!
    }
  };

  coins.sort((a, b) => b - a);  // Sort in decending order
  console.log("coins[]:", coins);

  let res = Infinity;

  find(amount, accumulatedCoins=0, coinIndex=0);
  return res === Infinity ? -1 : res;
};

//---------- Testing ----------
//let coins = [1, 2, 5];    let amount = 11;    // 3
//let coins = {2};          let amount = 3;     // -1
//let coins = {2,3,4};      let amount = 7;     // 2
let coins = [186,419,83,408];   let amount = 6249;    // 20

let min = coinChange(coins, amount);
console.log("\nmin:", min);
