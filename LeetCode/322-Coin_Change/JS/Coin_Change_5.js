/*
322. Coin Change

Note: LeetCode Std Sol 很爛,雖然它rating 很高,但很難懂!

-------------------------------------------------

解法 + 優化進程:
- Recursion DFS
- DP Top-Down Memorization ==> 使用 DP Top-Down Memo 來優化 DFS
- DP Bottom-Up
- DFS + Pruning
- Greedy (非最優解)

-------------------------------------------------

思路 for Approach #1~#3: (同樣video 存在兩個地點)
-(1) https://www.youtube.com/watch?v=jgiZlGzXMBw
-(2) /dwhelper/LeetCode/LeetCode 322 - Coin Change.mp4

-------------------------------------------------

Solution Code: (同樣 Solution Code 存在兩個地點)

- Java Version for Approach #1~#3:
  -(1) /Users/asun/Library/Mobile\ Documents/com~apple~CloudDocs/刷题-網站-LeetCode/Solutions/bephrem1-backtobackswe  -> DP  -> ChangeMaking
  -(2) https://github.com/bephrem1/backtobackswe  -> DP  -> ChangeMaking

- JS Version for Appraoch #3, #4:
  - https://leetcode.com/problems/coin-change/discuss/133487/Clean-JavaScript-solution

  */

/* Approach #5: Greedy ==> 非最優解 (See test case below)
- To make clear what goes wrong with this algorithm, consider

    coins = {42, 40, 1},    amount = 1 * (42) + 2 * (40) + 1 * (1) = 123

  The correct answer is clearly count = 4 but the above greedy algorithm would
  instead gives us count = 41 because 123 = 2 * 42 + 39 = 2 * (42) + 39 * (1).

  This algorithm is incapable of discovering that the optimal answer occurs
  when you have exactly 1 coin of value 42 because it only considers having the
  max possible number of that coin.
*/
const coinChange = function(coins, amount) {
  // Sort coins in decending order (大 -> 小)
  coins.sort((a, b) => b-a);

  let count = 0;

  for (let i = 0; i < coins.length; i++) {
    count = count + Math.floor(amount / coins[i]);

    // Get remaining after deducting the coin change
    // amount = amount - count * coins[i]  // Same as next ln
    amount = amount % coins[i];

    if (amount == 0)    return count;
  }
  return -1;
}

//---------- Testing ----------
let coins = [1, 2, 5];    let amount = 11;    // 3: DP & Greedy

//let coins = [1, 40, 42];    let amount = 123; // 4 : Using DP => Optimal Sol
                                                // 41: Using Greedy

let min = coinChange(coins, amount);
console.log("\nmin:", min);
