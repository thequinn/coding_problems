/*
322. Coin Change

Note: LeetCode Std Sol 很爛,雖然它 rating 很高,但很難懂!

-------------------------------------------------

解法 + 優化進程:
- Recursion DFS
- DP Top-Down Memorization ==> 使用 DP Top-Down Memo 來優化 DFS
- DP Bottom-Up
- Greedy + DFS + Pruning

-------------------------------------------------

思路 for Approach #1~#3: (同樣video 存在兩個地點)
-(1) https://www.youtube.com/watch?v=jgiZlGzXMBw
-(2) /dwhelper/LeetCode/LeetCode 322 - Coin Change.mp4

-------------------------------------------------

Solution Code: (同樣 Solution Code 存在兩個地點)

- Java Version for Approach #1~#3:
  -(1) /Users/asun/Library/Mobile\ Documents/com~apple~CloudDocs/刷题-網站-LeetCode/Solutions/bephrem1-backtobackswe  -> DP  -> ChangeMaking
  -(2) https://github.com/bephrem1/backtobackswe  -> DP  -> ChangeMaking

- JS Version for Appraoch #3:
  - https://leetcode.com/problems/coin-change/discuss/133487/Clean-JavaScript-solution

- JS Version for Appraoch #4:
   - https://leetcode.com/problems/coin-change/discuss/584332/JavaScript-Greedy-then-DFS-beats-99.26-solution

*/

// Approach #1: Recursion (DFS) ==> Brute Force [Time Limit Exceeded]
// - 思路:
// -- It's about using DFS - branching and exploring all possibilities
class Solution_1 {
  public int coinChange(int[] coins, int amount) {
    if (amount < 1)   return 0;
    return helper(coins, amount);
  }

  private int helper(int[] coins, int remainder) {
    // Base Cases:
    if (remainder < 0)    return -1;
    if (remainder == 0)   return 0;

    // Recursive Case:

    int min = Integer.MAX_VALUE; // Any random val > the input "amount"

    // - Try each coin as the last coin in the change that we
    // make for the amount.   ex. amount=11, last_coin=2 --> 11-2=7
    for (int coin: coins) {
      int changeRes = helper(coins, remainder - coin);

      // If making change was possible (changeRes >= 0) and the change result
      // beats our present min, add 1 to that smallest val
      if (changeRes >= 0 && changeRes < min) {
        min = 1 + changeRes;
      }
    }

    // If no answer is found then the sub prob ans is arbitrarily made to be
    // -1, otherwise the sub prob's answer is "min"
    return (min == Integer.MAX_VALUE) ? -1 : min;
  }
}

