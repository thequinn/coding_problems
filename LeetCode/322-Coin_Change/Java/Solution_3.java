/*
322. Coin Change

Note: LeetCode Std Sol 很爛,雖然它rating很高,但很難懂!

-------------------------------------------------

解法 + 優化進程:
- Recursion DFS
- DP Top-Down Memorization ==> 使用 DP Top-Down Memo 來優化 DFS
- DP Bottom-Up
- Greedy + DFS + Pruning

-------------------------------------------------

思路 for Approach #1~#3: (同樣video 存在兩個地點)
-(1) https://www.youtube.com/watch?v=jgiZlGzXMBw
-(2) /dwhelper/LeetCode/LeetCode 322 - Coin Change.mp4

-------------------------------------------------

Solution Code: (同樣 Solution Code 存在兩個地點)

- Java Version for Approach #1~#3:
  -(1) /Users/asun/Library/Mobile\ Documents/com~apple~CloudDocs/刷题-網站-LeetCode/Solutions/bephrem1-backtobackswe  -> DP  -> ChangeMaking
  -(2) https://github.com/bephrem1/backtobackswe  -> DP  -> ChangeMaking

- JS Version for Appraoch #3, #4:
  - https://leetcode.com/problems/coin-change/discuss/133487/Clean-JavaScript-solution

*/

// Approach #3: DP:Bottom-Up
// - 思路:
// -- Need the answers to prev sub-problems, so we can build the final result
//
import java.util.Arrays;
class Solution_3{

  public int coinChange(int[] coins, int amount) {
    int[] dp = new int[amount+1]; // amount=7, index:0~7
    Arrays.fill(dp, amount+1);  // amount+1 is just a val > all vals' in dp[]

    dp[0] = 0; // making change w/ min coins for 0 will always be 0 coins

    // Solve every subproblem from 1 to amount. i is curr amount to get changed
    for (int i = 1; i <= amount; i++) {
      System.out.println("\n------------>i:"+i);

      // coins[j]: curr change
      for (int j = 0; j < coins.length; j++) {
        System.out.println("--->j:"+j);

        // (any index i) <  (curr index i) has the most optimal change cached
        Sytem.out.println("coins[j]:"+coins[j]+", i:"+i);
        if (i - coins[j] >= 0) {
          dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
        }
      }
    }

    // If we do not have an answer, dp[amount]=amount+1.  Thus, will return -1
    // If we do, dp[amount] has the change < amount
    return dp[amount] > amount ? -1 : dp[amount];
  }
}

// Note: 看以下 Clean Version

//------------------------------------------------

// Clean Version (only ln-92 is diff)
import java.util.Arrays;
class Solution_3{

  public int coinChange(int[] coins, int amount) {
    int[] dp = new int[amount+1];
    Arrays.fill(dp, amount+1);

    dp[0] = 0;

    for (int i = 1; i <= amount; i++) {
      for (int j = 0; j < coins.length; j++) {

        if (i - coins[j] >= 0) {
          dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
        }
      }
    }

    return dp[amount] > amount ? -1 : dp[amount];
  }
}


