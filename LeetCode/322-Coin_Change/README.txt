
a. Java dir has explanation and solution code:

  1. Recursion DFS
  2. Top-Down Memorization ==> 使用 DP Top-Down Memo 來優化 DFS
  3. Bottom-Up



b. JS has explanation and solution code:

  3. DP Bottom-Up

  4. (錯) Greedy + DFS + Pruning  --> (改正) DFS + Pruning
     ==> LeetCode 的 Discussions 充斥著一堆自稱 Greedy + DFS + Pruning 的答案,
         都不是正確的 Greedy Algorithm !!!

  5. (非最優解) Greedy
     ==> 以下連結解釋為何 Greedy Algorithm 無法找出"最優解":

          https://leetcode.com/problems/coin-change/discuss/322329/Example-explaining-why-a-Greedy-algorithm-does-not-work


     Note:
     ex. Coin Change in Greedy Algorithm
     - https://www.geeksforgeeks.org/greedy-algorithm-to-find-minimum-number-of-coins/

