'''
324. Wiggle Sort II

'''

'''
Approach #1: Sorting
- https://leetcode.com/problems/wiggle-sort-ii/solutions/3242335/324-time-99-2-and-space-99-16-solution-with-step-by-step-explanation/
'''
class Solution:
    def wiggleSort(self, nums: List[int]) -> None:
        nums.sort()
        #print("nums:", nums)
        mid = ( len(nums) -1) // 2
        #print("mid:", mid)

        #nums[::2] = nums[mid::-1]
        #nums[1::2] = nums[:mid:-1]
        #
        # 1. Notice! This ln can't be seperated into the above 2 lns.
        # 2. See the breakdowns below.
        nums[::2], nums[1::2] = nums[mid::-1], nums[:mid:-1]

        '''
        nums[mid: :-1] - Starting from index=mid, go backwards
        nums[ :mid:-1] - Ending in index=mid, go backwards

        nums[::2]  - even indices
        nums[1::2] - odd indices
        '''
