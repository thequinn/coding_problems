/*
328. Odd Even Linked List

Given a singly linked list, group all odd nodes together followed by the even nodes.

Input: 2->1->3->5->6->4->7->NULL
Output: 2->3->6->7->1->5->4->NULL

注意!!
1. We're talking about the node number and not the value in the nodes.
2. You should try to do it in place. The program should run in O(1) space complexity and O(nodes) time complexity.

//--------------------------

LeetCode Std Solution: (See the visualization => 一定畫圖 trace code 才理解)
- 思路: Put the odd nodes in a linked list and the even nodes in another. Then link the evenList to the tail of the oddList.
- https://leetcode.com/problems/odd-even-linked-list/solution/

*/

//- Time complexity : O(n). There are total n nodes and we visit each node once.
//- Space complexity: O(1). All we need is the four pointers.
//
var oddEvenList = function(head) {
  if (head == null)   return null;

  let odd = head;
  let even = head.next, evenHead = even;

  // 注意: 一定要畫圖 trace code 才理解
  //
  // even !== null    : when LL has only 1 node
  // even.next != null: when LL has only 2 nodes
  while (even !== null && even.next !== null) {
    odd.next = even.next;   odd = odd.next;
    even.next = odd.next;   even = even.next;
  }
  // Link the evenList to the tial of the oddList
  odd.next = evenHead;

  return head;
};
