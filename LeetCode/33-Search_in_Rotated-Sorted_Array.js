/*
33. Search in Rotated Sorted Array

思路:
(1) 数组从任意位置劈开后，至少有一半是有序的
    ex. [4 5 6 7 1 2 3] ，从 7 劈开，左边是有序的
(2) 然后看 target 在不在这一段里，如果在，那么就把另一半丢弃。
    如果不在，那么就把这一段丢弃。

Solution Code:
- https://leetcode.com/problems/search-in-rotated-sorted-array/discuss/186585/Javascript-Solution-with-detailed-explanation

---------------------------------------------

注意!!
- https://leetcode.wang/leetCode-33-Search-in-Rotated-Sorted-Array.html
-- 以上連結列出3個解法,它們也是LeetCode 上最高票的3個解法. (前2個解法不易懂!)
-- 這裡的程式碼是法3,但比 LeetCode 上原來的解法容易懂.


*/

// - 时间复杂度：O(log n)
// - 空间复杂度：O(1)
//
var search = function(A, target) {
  var left = 0, right = A.length - 1;

  // Just a straight binary search.
  while (left <= right) {
    var middle = Math.floor((right + left) / 2);

    // We have found our target.
    if (A[middle] === target) {
      return middle;
    }

    // The clever part starts here:
    if (A[left] <= A[middle]) {
      // If the middle element is greater than the element to the left
      // of it, then that means that the bottom half is strictly increasing
      // from left to middle, i.e. it is sorted and we can just do a normal
      // binary search.

      // Is the target in this range?
      if (A[left] <= target && target < A[middle]) {
        // 'recurse' down this side
        right = middle - 1;
      } else {
        // 'recurse' down the other side
        left = middle + 1;
      }
    } else {
      // This means that the *top* half must be sorted, because
      // there can only be one place in the entire array where
      // the order is not sorted, and it's on the bottom half.

      if (A[middle] < target && target <= A[right]) {
        // 'recurse' down this side
        left = middle + 1;
      } else {
        // 'recurse' down the other side
        right = middle - 1;
      }

    }
  }

  return -1;
};

//---------- Testing ----------
let nums = [4,5,6,7,0,1,2], target = 0;  // Output: 4
//let nums = [4,5,6,7,0,1,2], target = 3;  // Output: -1

let res = search(nums, target);
console.log("res:", res);
