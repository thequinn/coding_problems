
/*
334. Increasing Triplet Subsequence

----------------------------------------------------------------
Def: Subsequence
- a sequence that can be derived from another sequence by deleting some elements without changing the order of the remaining elements.

  ex-a. The sequence {A,B,D} is a subsequence of {A,B,C,D,E,F}

- Subsequences should not be confused with substrings.

  ex-b. The substring {A,B,C,D} can be derived from the {A,B,C,D,E,F} by deleting substring {E,F}


- More subsequence ex:

  The list of all subsequences for the word "apple":
  - "a", "ap", "al", "ae", "app", "apl", "ape", "ale", "appl", "appe", "aple", "apple", "p", "pp", "pl", "pe", "ppl", "ppe", "ple", "pple", "l", "le", "e", "".

----------------------------------------------------------------
ex1: Input: [10,20,3,2,1]
     Output: F

// 重要: ex2-1, ex2-2, 用到 Def: Subsequence, see ex-a above
//
ex2-1: Input: [1,2,0,4]
     Output: T

ex2-2: Input: [1,2,0,-1,4]
     Output: T

*** 重要!! ***
ex3: ===> Must know def of subsequence & animation from LeetCode Premium Sol!
     Input: [10,20,3,2,1,1,2,0,4] ---> 重點為 [1,2,0,4]

================================================================

LeetCode Premium Sol:

- 思路:
-- LeetCode Std Sol 後半段

- Time complexity : O(N) where N is the size of nums.
-- We are updating first_num and second_num as we are scanning nums.
- Space complexity : O(1)
-- We are not consuming additional space other than variables for two numbers.

*/

var increasingTriplet = function(nums) {
  let first_num = Infinity;
  let second_num = Infinity;

  for (const n of nums) {
    if (n <= first_num)       // NOT <, but <=
      first_num = n;
    else if (n <= second_num) // Not <, but <=
      second_num = n;
    else
      return true;
  }
  return false;
};


let nums = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
console.log(increasingTriplet(nums));

