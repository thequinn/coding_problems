/*
337. House Robber III

Explanation + Sol Code:  (same videos, 2 locations)
- https://www.youtube.com/watch?v=HllsaLY2Fy8
- dwhelper/LeetCode in MacBook
*/


// Approach: DP + DFS
const rob = function(root) {

  function decision(node) {
    // Base Case (Exit Strategy):
    if (!node)
      // [result robbing the node, result NOT robbing the node]
      return [0, 0];

    // [x, y]: [result Robbing left child, result NOT robbing left child]
    let [leftRob, leftNot] = decision(node.left);
    let [rightRob, rightNot] = decision(node.right);

    // If rob cur node, can't rob its children
    let robDecision = node.val + leftNot + rightNot;
    // If NOT rob cur node, can rob its children in any kinds of combos
    let notRobDecision = Math.max(
      leftRob+rightRob, leftRob+rightNot,
      leftNot+rightRob, leftNot+rightNot
    );

    return [robDecision, notRobDecision]
  }

  // B/c we have 2 elems passing back from [] from ln-35,
  // we need to spread it using "..."
  return Math.max(...decision(root));
}
