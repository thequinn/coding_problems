'''
337. House Robber III

This link shows how to evlove from brute force to optimization
  https://leetcode.com/problems/house-robber-iii/discuss/1612349/C%2B%2BPython-Simple-Solutions-w-Explanation-or-Optimization-from-Brute-Force-to-DP-to-Optimized-DP


'''

'''
Approach #1: Brute Force (Time Limit Exceeded)

Explaination & Sol Code:
  https://www.youtube.com/watch?v=FRP5l83XoZo
||
===> Note: This video uses a Java Map for 'Memoization' 優化 - save the nodes'
           val's calculated already.  But QN can't find a way to save type of
           TreeNode as key in a map of dict in Python.  So go to Approach #3.
'''
class Solution(object):
  def rob(self, root: Optional[TreeNode]) -> int:
    # if root is null, it's an empty tree meaning there are no houses
    if (not root):  return 0

    # if root is not null, start recursivly robbing

    # --> Opt. #1
    # Rob the cur node => skip the childs and rob the grandchilds
    sum = root.val
    if (root.left):
      sum += self.rob(root.left.left) + self.rob(root.left.right)
    if (root.right):
      sum += self.rob(root.right.left) + self.rob(root.right.right)

    # --> Opt. #2
    # Not rob the cur node => rob the childs
    next_sum = self.rob(root.left) + self.rob(root.right)

    # Take the max of the two options
    res = max(sum, next_sum)

    return res


'''
# Approach #2: DP => The sol link's Approach #2 & #3 are the same in Python version

'''


'''
Approach #3:

Explaination + Sol Code:
  https://leetcode.com/problems/house-robber-iii/discuss/946176/Python-very-short-dfs-explained
'''

class Solution:
  def rob(self, root):

    def dfs(node):
      if not node: return [0, 0]

      # we have node and L and R are left and right children

      L = dfs(node.left)
      R = dfs(node.right)

      return
      [
        # opt1: rob cur node, then we can't rob the childs, but the grandchilds
        node.val + L[1] + R[1],
        # opt2: skip cur node, then we can rob the childs
        max(L) + max(R)
      ]

    return max(dfs(root))

