/*
34. Find First and Last Position of Element in Sorted Array

- 思路:
-- Because the array is sorted, we can modify Binary Search to locate the
   leftmost and rightmost indices.  ex. [1,2,5,5,5,9]
*/

// Approach #1: JavaScript built-in methods
function searchRange_1(nums, target) {
  return [nums.indexOf(target), nums.lastIndexOf(target)];
}

//-----------------------------------------------------
// Approach #2: Linear Scan - by Quinn
var searchRange_2 = function(nums, target) {
  let left = -1, right = -1;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === target) {
      left = i;
      break;
    }
  }

  // If last for loop reaches end and not found a target, return error [-1,-1]
  if (left === -1)    return [-1, -1];

  // Condition i >= left takes care of the edge cases in Testing Part
  for (let i = nums.length - 1; i >= left; i--) {
    if (nums[i] === target) {
      right = i;
      break;
    }
  }

  return [left, right];
};

//-----------------------------------------------------
// Approach #3: Modified Binary Search (Iterative)
//              => This code "while (i<j)" vs Binary Search "while (i<=j)"
//
// - Explanation + Solution Code:
// -- https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/discuss/14699/Clean-iterative-solution-with-two-binary-searches-(with-explanation)
//
// - When we use mid=(i+j)/2, the mid is rounded to the lowest leteger. In
// other words, mid is always biased towards the left, meaning:
//
// -- mid=(i+j)/2 only works when searching for leftmost target pos but
//     not rightmost target pos.  So we could have i=mid when mid=(i+j)/2,
//     but we NEVER have j=mid.
//
//     Scenerio:
//     - After leftmost target pos is found, i=mid. If still use
//       mid=(i+j)/2, i will get stuck and we NEVER have j=mid.(追蹤代碼)
//
//     Solution:
//     - To keep the search range moving, make sure the new i is set to
//       sth different than mid, otherwise i might get stuck.
//
const searchRange_3= function(A, target) {
  let i = 0, j = A.length - 1;
  let res = [];

  /***** Search-#1 for 1st pos of target *****/

  while (i < j) {  // When i, j pt to same index, 1st pos of target is found
    let mid = Math.floor( (i + j) / 2 );

    if (A[mid] < target)    i = mid + 1;
    else    j = mid;
  }
  if (A[i] != target)   return [-1,-1];
  else    res.push(i); // i is set

  /***** Search-#2 for last pos of target *****/

  j = A.length-1;  // Need not to set i to 0 the second time.

  while (i < j) {  // When i, j pt to same index, last pos of target is found
    let mid = Math.floor( (i + j) / 2 + 1);  // Make mid biased to the right

    if (A[mid] > target)  j = mid - 1;
    else  i = mid;  // So that this won't make the search range stuck.
  }
  res.push(j); // j is set

  return res;
}

//-----------------------------------------------------
// Approach #4: Modified Binary Search (Recursive)
// - https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/discuss/14707/9-11-lines-O(log-n)
//

//---------- Testing ----------

// Edge Cases:
//let nums = [1], target = 1;             // Output: [0, 0]
//let nums = [1,3], target = 1;           // Output: [0, 0]

// Regular cases:
let nums = [5,7,7,8,8,10], target = 8;  // Output: [3,4]
//let nums = [5,7,7,8,8,10], target = 6;  // Output: [-1,-1]

console.log("Input nums[]:", nums, ", target:", target);

//let res = searchRange_1(nums, target);
//let res = searchRange_2(nums, target);
let res = searchRange_3(nums, target);

console.log("\nres:", res);
