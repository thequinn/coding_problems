'''
340. Longest Substring with At Most K Distinct Characters

LeetCode Premium Solution:
- s1. 3. Longest Substring Without Repeating Characters
- s2. 159. Longest Substring with At Most Two Distinct Characters
- s3. Can read the explanation from sol 340, but I haven't read its sol code

Solution Code: --> Easy to learn for beginner
- https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/discuss/80052/10-line-Python-Solution-using-dictionary-with-easy-to-understand-explanation

'''

class Solution:
    def lengthOfLongestSubstringKDistinct(self, s: str, k: int) -> int:
        sub = {}
        substr_start = 0 # left ptr of sliding window
        longest = 0

        # - Keep extending the r ptr until the num of elems of the dict is
        #   larger than k.  Then we found a substr candidate.
        # - When it happend, remove 1st elem in the dict and contract the
        #   window (move left ptr to the right)
        for i, c in enumerate(s): # i: right ptr of sliding window
          sub[c] = i
          if len(sub) > k:
            # min(sub.values()): 1st elem of the dict needs to be removed.  So
            # this index +1 will be the start of the new substr
            substr_start = min(sub.values())
            del sub[ s[substr_start] ]

            # start of index for the new substr
            substr_start += 1
          else: # can be omitted
            longest = max(longest, i - substr_start + 1)
        return longest



