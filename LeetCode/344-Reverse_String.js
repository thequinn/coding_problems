
// Approach #2: Two Pointers
// - for-loop's condition is i < j
//
// - Time complexity: O(N) to swap N/2 element
// - Space complexity: O(1), it's a constant space solution
//
var reverseString = function(s) {
  for (let i = 0, j = s.length - 1; i < j; i++, j--) {
    // JS swap formula: b = [a, a = b][0];
    s[j] = [ s[i], s[i]=s[j] ][0];
  }
  return s;
};


let s = ["h","e","l","l","o"];
console.log(reverseString(s));
