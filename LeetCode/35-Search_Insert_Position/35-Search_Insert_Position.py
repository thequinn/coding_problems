'''
35. Search Insert Position

https://leetcode.com/discuss/general-discussion/786126/python-powerful-ultimate-binary-search-template-solved-many-problems

right boundary:
- The input target might be larger than all elements in nums and therefore needs to placed at the end of the array. That's why we should initialize 
right = len(nums) instead of right = len(nums) - 1.
'''

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        left, right = 0, len(nums)

        while left < right:
            mid = left + (right - left) // 2
            if nums[mid] >= target:
                right = mid
            else:
                left = mid + 1
        return left
