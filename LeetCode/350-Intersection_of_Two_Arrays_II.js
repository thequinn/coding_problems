/*
350. Intersection of Two Arrays II


Note: (為把題目解釋清楚，必看下方 Test Cases!!)

(1) Each element in the res should appear as many times as it shows in
   both arrays.
(2) The res can be in any order.


Follow up Solution: 
- https://leetcode.com/problems/intersection-of-two-arrays-ii/discuss/282372/Java-solution-with-all-3-follow-up-questions

(1) What if the given array is already sorted? How would you optimize your
    algorithm?
Ans: If given array is not sorted, use HashMap.  otherwise, use Two Pointers

(2) What if nums1's size is small compared to nums2's size? Which algorithm
    is better?
Ans: Let's say nums1 is K size. Then we should do binary search for every
     element in nums1. Each lookup is O(log N), and if we do K times, we
     have O(K log N).
     If K is small enough, O(K log N) < O(max(N, M)). Otherwise, we have to
     use the previous two pointers method.

(3) What if elements of nums2 are stored on disk, and the memory is limited
    such that you cannot load all elements into the memory at once?
Ans: This one is open-ended. But you have to think of divide and conquer. We can always let the memory take care of a segment of our nums2 or nums1.

Code Solution: 
- It has 3 Python solutions, but each method below has the JS version.
- https://leetcode.com/problems/intersection-of-two-arrays-ii/discuss/82247/Three-Python-Solutions
*/


// Method #1: Two Pointers
// - https://leetcode.com/problems/intersection-of-two-arrays-ii/discuss/82312/Javascrip-beats-97-2-pointer
//
// - Array.sort(compareFunc)
// -- If compareFunction is not supplied, all non-undefined array elements
//    are sorted by converting them to strings and comparing strings in 
//    UTF-16 code units order. 
// -- ex, "banana" comes before "cherry". 
//    ex. In a numeric sort, 9 comes before 80, but b/c numbers are
//        converted to strings, "80" comes before "9" in the Unicode order.
//
var intersect = function(nums1, nums2) {
  let p1 = 0, p2 = 0;
  let res = [];

  nums1.sort(cm);  // Array.sort() sort in-place
  nums2.sort(cm);

  while(p1 < nums1.length && p2 < nums2.length) {
    if(nums1[p1] == nums2[p2]) {
      res.push(nums1[p1]);
      p1++;   p2++;
    } else if(nums1[p1] > nums2[p2]) {
      p2++;
    } else {
      p1++;
    }
  }
  return res;
};
var cm = function(a, b) {
    return a - b;
};

// Method #2-1: Using Map/HashMap obj
// - https://leetcode.com/problems/intersection-of-two-arrays-ii/discuss/242050/JavaScript-Solution-HashMap
//
// - Map object 
// -- holds key-value pairs and remembers the original
//   insertion order of the keys. Any value (both objects and primitive
//   values) may be used as either a key or a value.
//
var intersect = function(nums1, nums2) {
  const map = new Map();  // Using Map obj
  const res = [];

  for (const n of nums1) {
    if (map.has(n)) {
      map.set(n, map.get(n) + 1)
    } else {
      map.set(n, 1);
    }
    //console.log("map:", map);
  }

  for (const n of nums2) {
    // 注意！
    // - W/o 2nd condition, all elems in nums2[] matching nums1[] will
    //   be added to res[]
    if (map.has(n)) {
      res.push(n);
      map.set(n, map.get(n) - 1);
    }
  }
  return res;
};

// Method #2-2: Using JS object literial, {}
// - https://leetcode.com/problems/intersection-of-two-arrays-ii/discuss/310695/JavaScript-Solution-beat-99.64-Easy-Understanding
//
// - Time Complexity: O(N + M)
// -- O(N) for iterate one of the array to create a hashmap and O(M) to
//    iterate the other array.
//
var intersect = function(nums1, nums2) {
  const map = {}; // Using JS obj literial, {}
  const res = [];

  for (const n of nums1){
    map[n] ? map[n]++ : map[n] = 1;
    console.log("map:", map);
  }

  for (const n of nums2) {
    if (map[n] && map[n] > 0) {
      res.push(n);
      map[n]--;
    }
  }
  return res;
};

//let nums1 = [1,2,2,1], nums2 = [2,2]; // [2,2]
let nums1 = [4,9,5], nums2 = [9,4,9,9,8,4]; // [4,9]
//let nums1 = [2,4,9,5], nums2 = [9,5,7,4,9,8,4];
console.log(intersect(nums1, nums2));
