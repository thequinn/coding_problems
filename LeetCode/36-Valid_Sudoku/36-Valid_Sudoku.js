/*
36. Valid Sudoku

Explanation & Sol Code:
- https://leetcode.com/problems/valid-sudoku/discuss/124036/Readable-Javascript-Solution-w-Comments

- Time complexity : O(1)
-- All we do here is just one iteration over the board with 81 cells.

- Space complexity: O(1).

*/

var isValidSudoku = function(board) {
  // create an empty set for each row/col/square
  const rowRules = new Array(9).fill().map(() => new Set())
  const colRules = new Array(9).fill().map(() => new Set())
  const mixedRules = new Array(9).fill().map(() => new Set())

  // iterate through each cell on the board
  for (let row = 0; row < 9; row++) {
    for (let col = 0; col < 9; col++) {
      const curr = board[row][col];

      // some tricky math to get the index of the 3x3 squares
      const mixedIdx = Math.floor(row / 3) * 3 + Math.floor(col / 3);
      console.log("row, col, mixedIdx:", row, col, mixedIdx);

      if (curr === ".")   continue  // ignore dots

      // if the current number already exists in the set, board is invalid
      const a = rowRules[row].has(curr);
      const b = colRules[col].has(curr);
      const c = mixedRules[mixedIdx].has(curr);
      if (a || b || c)    return false;

      // add the number to the appropriate set
      rowRules[row].add(curr);
      colRules[col].add(curr);
      mixedRules[mixedIdx].add(curr);
    }
  }

  return true;
};

let board_1 = [
  ["5","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
];

let board_2 = [
  ["8","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
];

console.log( isValidSudoku(board_1) ); // T
//console.log( isValidSudoku(board_2) ); // F
