'''
36. Valid Sudoku

Explaination + Sol Code:
- https://leetcode.com/problems/valid-sudoku/solutions/3277043/beats-96-78-short-7-line-python-solution-with-detailed-explanation/?envType=study-plan-v2&envId=top-interview-150

思路:
- Divide the board into the 3 types of tuples in temp[] below.  After 
  processing all the cells, the method checks if the length of "res" is equal
  to the length of the set of "res".

- temp[] inside the nested for loop
  - If the element is not a dot ('.'), which means it's a valid number, the 
    method adds three tuples to the "res" list:
     1. The first tuple contains the row index (i) and the element itself.
     2. The second tuple contains the element itself and the column index (j).
     3. The third tuple creates 3x3 sub-grid.  Row and col indices are 0~2 for 
       each sub-grid.
'''

class Solution(object):
    def isValidSudoku(self, board):
        res = []
        for i in range(9):
            for j in range(9):
                element = board[i][j]
                if element != '.':
                    temp = [
                        (i, element), 
                        (element, j), 
                        (i // 3, j // 3, element)
                    ]
                    #print(temp)
                    res += temp
        return len(res) == len(set(res))
        # Same as last ln
        '''
        leng1 = len(res)
        leng2 = len(set(res))
        print("leng1:", leng1, ", leng2:", leng2)
        return leng1 == leng2
        '''
