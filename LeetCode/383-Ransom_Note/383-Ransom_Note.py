'''
383. Ransom Note

'''

class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:

        print("ransomNote:", ransomNote, ", magazine:", magazine)
        print("set(randomNote):", set(ransomNote))

        for i in set(ransomNote):
            print("ransomNote.count(", i, "):", ransomNote.count(i))
            print("magazine.count(", i, "):", magazine.count(i))

            if ransomNote.count(i) > magazine.count(i):
                return False
        return True
