/*
384. Shuffle an Array

Solution:
- https://leetcode.com/problems/shuffle-an-array/discuss/85975/Intuitive-Javascript-Solution
*/

var Solution = function(nums) {
  this.nums = nums || [];
};

Solution.prototype.reset = function() {
  return this.nums;
};

// Fisher-Yates algorithm
// - On each iteration of the algorithm, we generate a random integer between the current index and the last index of the array. 
// - Then, we swap the elements at the current index and the chosen index - this simulates drawing (and removing) the element from the hat, as the next range from which we select a random index will not include the most recently processed one. 
//
Solution.prototype.shuffle = function() {

  // 法1:
  // - WRONG!! B/c JS is pass-by-ref for objects, this.nums will be altered,
  // and Solution.prototype.reset() won't work.
  //const a = this.nums; 
  //
  // 法2:
  const a = this.nums.slice();

  for (let i = 0; i < a.length; i++) {
    // Getting a random num between i ~ a.length.  a.length is exclusive.
    let rand = Math.floor(Math.random() * (a.length - i)) + i;

    // Swap 2 values
    [ a[i], a[rand] ] = [ a[rand], a[i] ];
  }

  return a;
};

/**
 * Your Solution object will be instantiated and called as such:
 */
//let nums = ["Solution","shuffle","reset","shuffle"]
let nums = [[[1,2,3]],[],[],[]]
var obj = new Solution(nums)

var param_2 = obj.shuffle()
console.log("param_2:", param_2);

var param_1 = obj.reset()
console.log("param_1:", param_1);
