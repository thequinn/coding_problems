/*
387. First Unique Character in a String

Note: You may assume the string contain only lowercase letters.

Solution:
- https://leetcode.com/problems/first-unique-character-in-a-string/discuss/86356/JavaScript-solution
*/

// Method #1: Array
var firstUniqChar = function(s) {
  for(let i = 0; i < s.length; i++){
    //console.log("s[i]:", s[i]);
    //console.log("s.indexOf(s[i]):", s.indexOf(s[i]));
    //console.log("s.lastIndexOf(s[i]):", s.lastIndexOf(s[i]));
    if ( s.indexOf(s[i]) === s.lastIndexOf(s[i]) ){
      return i;
    } 
  }
  return -1;
};

// Method #2: Hask Map
// 
// (1) Map object:
// - A Map object can iterate its elements in insertion order
// - You use Map when keys are unknown until runtime
//
// (2) Object object / {}:
// - Since ES2015, insertion order is preserved, except in the case of keys
//   that parse as integers (eg "7" or "99"), where behavior varies between
//   browsers.
// - ex. const obj = { "foo": "foo", "1": "1", "bar": "bar" }
//       ==> key order: 1, foo, bar
//
var firstUniqChar = function(s){
  var map=new Map();
  
  for(let i = 0; i < s.length; i++){
    //console.log("i:", i);
    if(map.has(s[i]))   map.set(s[i], 2);
    else                map.set(s[i], 1);
  }

  for(let i = 0; i < s.length; i++){
    if(map.has(s[i]) && map.get(s[i]) === 1){
      return i;
    }
  }
  return -1;
};

let s = "leetcode";
//let s = "loveleetcode";
console.log(firstUniqChar(s));
