'''
39. Combination Sum

Video:
    https://www.youtube.com/watch?v=GBKI9VSKdGg

Code:
    https://leetcode.com/problems/combination-sum/solutions/1494899/backtracking-and-dp-solutions-python-with-explanation/
'''

'''
Approach #1: Backtracking
'''
class Solution(object):
    def combinationSum(self, candidates, target):

        res = [] # to hold valid combination sequences that sum to target; res will be updated by backtracking

        # backtracking helper
        def backtrack(i, cur, total):
            if total == target:
                res.append(cur[:])
                return
            if i >= len(candidates) or total > target:
                return

            # choose to add one candidates[i]
            cur.append(candidates[i])
            backtrack(i, cur, total + candidates[i])
            # backtracking to not including candidates[i]
            cur.pop()
            backtrack(i + 1, cur, total)

        # backtracking
        backtrack(0, [], 0)
        return res

'''
Approach #2: DP
'''
class Solution(object):
    def combinationSum(self, candidates, target):

        candidates.sort()
        dp ={i: [] for i in range(target + 1)}
        print("dp-1:", dp)
        dp[0].append([])
        print("dp-2:", dp)

        for i in range(1, target + 1):
            for j in range(len(candidates)):
                if candidates[j] > i:
                    break
                for k in range(len(dp[i - candidates[j]])):
                    temp = dp[i - candidates[j]][k][:]
                    if temp and temp[-1] > candidates[j]:
                        continue
                    temp.append(candidates[j])
                    dp[i].append(temp)
        # print(dp)
        return dp[target]
