'''
39. Combination Sum

Note:
- Combinations (組合) vs Permutation (排列)
    => 記憶: 組合 不需 排列

- From the problem:
  The same number may be chosen from candidates an unlimited number of times. Two combinations are unique if the frequency of at least one of the chosen numbers is different.
||
==> What it says is that it wants unique combinations
     ex. [2,2,3] and [3,2,2] are comsidered the same combos and diff permutation
'''

# Approach #1-1:
# - See template:
#    -  Dropbox/Templates/Backtracking_&_DFS/Backtrack_Summary-General_Solution_for_10_Questions.py
#
class Solution:
  def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:

    def backtrack(remain, comb, start):
      if remain == 0:
        results.append(list(comb))
        return
      elif remain < 0:
        return

      for i in range(start, len(candidates)):
        comb.append(candidates[i])
        backtrack(remain - candidates[i], comb, i)
        comb.pop()  # Backtrack

    results = []
    backtrack(target, [], 0)
    return results

'''
Approach #1-2: -> Exactly the same as Approach #1-1, except QN replaced
                  ln-43~47 and ln-50~52 to match Approach #2
'''
class Solution:
  def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:

    def backtrack(cur, comb, start):
      if cur > target:
        return
      if cur == target:
        results.append(list(comb))
        return

      for i in range(start, len(candidates)):
        comb.append(candidates[i])
        backtrack(cur+candidates[i], comb, i)
        comb.pop()  # Backtrack

    results = []
    backtrack(0, [], 0)
    return results


'''
Approach #2: -> Same concept as Approach #1, except Approach #2 combines
                add-dfs-remove into 1 ln,
                    dfs(i, path+[candidates[i]], cur+candidates[i])
- See template:
  - Dropbox/Templates/Backtracking_&_DFS/DFS_Templates-6_Classic_Backtracking_Problems.py
'''
from typing import List
class Solution:
  def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
    # if candidates[] is empty
    if not candidates: return []

    res = []

    # if you sort the candidates beforehand, you can return early in the for
    # loop as soon as you see a candidate bigger than the remaining target.
    candidates.sort()

    def dfs(idx, path, cur):
      if cur > target:
        return
      if cur == target:
        res.append(path)
        return
      for i in range(idx, len(candidates)):
        dfs(i, path+[candidates[i]], cur+candidates[i])

    dfs(0, [], 0)
    return res

#-------Test Cases-----
sol = Solution()
candidates = [2,3,6,7]
target = 7

res = sol.combinationSum(candidates, target)
print("res:", res)

