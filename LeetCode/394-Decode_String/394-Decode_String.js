/*
394. Decode String

The problem seems straightforward at first glance. But the trick here is that there can be nested encoded strings:

  ex1. s = "3[a]2[bc]" => "aaabcbc"

  ex2. s = "3[a2[c]]" => "3[acc]" => "accaccacc"

If you have solved similar problem such as Evaluate Polish Notation or Simplify Path , it is clear that Stack Data Structure is best suited to implement such problems. We could implement a stack data structure or recursively build the solution by using an internal call stack.
*/

/*
Approach 1: Using Stack

Sol Code:
- https://leetcode.com/problems/decode-string/discuss/87586/Javascript-solution-using-stack-idea

思路:

There could be one of the following cases :
(1) There is another nested pattern k[string k[string]]
(2) There is a closing bracket k[string]

Since we have to start decoding the innermost pattern first, continue iterating over the string s, pushing each character to the stack until it is not a closing bracket ]. Once we encounter the closing bracket ], we must start decoding the pattern.

As we know that stack follows the Last In First Out (LIFO) Principle, the top of the stack would have the data we must decode.


Time Complexity : O(k * n),  ex. s = 5[a2b], k is 5
Space Complexity: O(k * n), We're pushing each decoded string to stack k times.

*/
var decodeString = function(s) {
  if (!s || s.length === 0)   return '';

  var digits = [];

  // ln-93 or ln-99: Array.pop() returns "undefined" if arr is empty.
  var strings = [''];

  for (let i = 0; i < s.length; i++) {
    let c = s.charAt(i);  console.log("\nc:", c);

    if (!isNaN( s.charAt(i)) ) {      // c is a number
    //if (/\d/.test( s.charAt(i) )) { // same as above

      // Collect all digits until a non-digit appears
      let n = '';
      while (!isNaN(s.charAt(i))) {
        n += s.charAt(i);
        i++;
      }
      i--;

      console.log("n:", n);
      digits.push(parseInt(n));
    }
    else if (s.charAt(i) === '[') {
      // ln-93 or ln-99: Array.pop() returns "undefined" if arr is empty.
      strings.push('');
    }
    else if (s.charAt(i) === ']') {

      /*
      Part-A: Takes care of
      (1) Regular case: 2[bc] in s = "3[a]2[bc]"
      (2) Nested case : 2[c]  in s = "3[a2[c]]"
      */
      let chars = strings.pop();
      let count = digits.pop();

      let substr = '';
      for (let j = 0; j < count; j++) {
        substr += chars;
      }

      /*
      Part-B:
      (1) Regular case: 2[bc] in s = "3[a]2[bc]"
      - It takes the top-most str from strings[], which is 'a' and combine
        w/ substr, 'bcbc'
      (2) Nested case : 2[c]  in s = "3[a2[c]]"
      - It takes the top-most str from strings[], which is 'a' and combine
        w/ substr, "cc"
      */
      let tmp = strings.pop();
      strings.push(tmp + substr);
      console.log("(", tmp, "), (", substr, ")");
    }
    else {  // c is an alphabet
      // Combined the top-most str from strings[] and concat w/ c
      let tmp1 = strings.pop();
      let tmp2 = s.charAt(i);
      let tmp3 = tmp1 + tmp2;
      console.log("(", tmp1, "), (", tmp2, "), (", tmp3, ")");
      strings.push(tmp3);

      //strings.push(strings.pop() + s.charAt(i)); // Same as right above it
    }

    console.log("digits[] :", digits);
    console.log("strings[]:", strings);
  }

  return strings[0];
};


// Approach 2: Using Recursion ===> 應該不用


//-----Test Case-----
//s = "11[a]2[bc]";  // "aaabcbc"
s = "3[a2[c]]";  // "accaccacc"

console.log(decodeString(s));
