'''
394. Decode String

'''

'''
Approach #1: Stack
  https://leetcode.com/problems/decode-string/solutions/1400105/98-faster-with-and-without-stack-cleane-concise/


思路:

1. var res to store each section of the str.  
   var num to store the k val for num of repetition.
   stack[] to store res and num until ']' is encountered

2. Iter thr the input string, s. If a char is a ...
    a) digit: multiply num by 10 and add the new num and the digit back to num.
    b) '['  : temporarily store the cur res and num in stack[]
    c) ']'  : pop res and num from stack[]
    d) else (an alphbet): concat it to res
'''
class Solution:
    def decodeString(self, s: str) -> str:
        res, num = "", 0
        stack = []
        print("s:", s, "\n")

        for ch in s:
            print("\nc:", c)
            if ch.isdigit():
                num = num * 10 + int(ch)   
                print("1. num:", num) 

            elif ch == "[":
                stack.append(res)
                stack.append(num)
                print("2. stack:", stack)

                res, num = "", 0

            elif ch == "]":
                pnum = stack.pop()
                pstr = stack.pop()
                res = pstr + (pnum * res)
                print("3-1. pnum:", pnum, ", pstr:", pstr)
                print("3-2. res:", res)

            else:
                res += ch
                print("res:", res)
    
        return res

'''
Approach #2: Recursion
  https://leetcode.com/problems/decode-string/solutions/1400105/98-faster-with-and-without-stack-cleane-concise/


思路:

'''
class Solution:
    def decodeString(self, s: str) -> str:       
        
        def dfs(s,p):
            res = ""
            i,num = p,0

            while i < len(s):
                asc = ( ord(s[i]) - 48 )

                # Same as: if s[i].isdigit():
                if 0 <= asc <= 9:  num = num * 10 + asc
                
                elif s[i]=="[":
                    local, pos = dfs(s, i + 1)
                    res += local * num
                    i = pos
                    num = 0
                
                elif s[i] == "]":  return res, i

                else:  res+=s[i]
                
                i+=1
            
            return res,i
    
        return dfs(s,0)[0]
