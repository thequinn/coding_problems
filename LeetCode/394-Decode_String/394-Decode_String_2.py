'''
394. Decode String

# - - - - - - - - - - - - # - - - - - - - - - - - - # 
Tips:
- When solving problems w/ nested brackets, you cane use
  1. Recursion 
  2. Recursion + Stack => Easier

Ex. 394. Decode String
    https://youtu.be/qB0zZpBJlh8?t=226

# - - - - - - - - - - - - # - - - - - - - - - - - - # 
Vid Explanation + Code:
- NeetCode: https://www.youtube.com/watch?v=qB0zZpBJlh8


# - - - - - - - - - - - - # - - - - - - - - - - - - # 
非常重要!!
- When we use a stack - a type of list, pop() will be used.  We always have to check if the stack/list is empty.  It's bc if you call pop() on an empty list, it will raise an IndexError.

'''
class Solution:
    def decodeString(self, s: str) -> str:

        stack = []
        for i in range(len(s)):
            # When ']' is not encountered, keep appending to stack
            if s[i] != "]":
                stack.append(s[i])
            # When  ']' is encountered
            else:
                # Get everything up to '['
                substr = ""
                while stack[-1] != "[":
                    substr = stack.pop() + substr
                stack.pop() # Pop the left bracket

                # Get the digit(s) before '['
                k = ""
                while stack and stack[-1].isdigit():
                    k = stack.pop() + k

                # k * the above substr -> Append it to stack    
                stack.append(int(k) * substr)

            # join(): takes all items in an iterable and joins them into 
            #         1 str
            # Using an empty str so we can call join() from Python String 
            return "".join(stack)
