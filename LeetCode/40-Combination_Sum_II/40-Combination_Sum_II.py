'''
40. Combination Sum II

'''
# Approach #1: DFS
# - from DFS_Templates-6_Classic_Backtracking_Problems.py in iCloud/刷題/LeetCode
#
from typing import List
class Solution:
  def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
    res = []
    candidates.sort()

    def dfs(idx, path, cur):
      if cur > target: return
      if cur == target:
        res.append(path)
        return
      for i in range(idx, len(candidates)):
        # avoid repetitive use of same number
        if i > idx and candidates[i] == candidates[i-1]:
          continue
        # i+1 will move to next elem
        dfs(i+1, path+[candidates[i]], cur+candidates[i])

    dfs(0, [], 0)
    return res

#-------Test Cases-------
sol = Solution()

candidates = [2,5,2,1,2]  # Output: [ [1,2,2], [5] ]
target = 5

res = sol.combinationSum2(candidates, target)
print("res:", res)
