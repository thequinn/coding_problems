'''
409. Longest Palindrome
- https://leetcode.com/problems/longest-palindrome/solutions/813721/python-3-solution-using-set/
- 思路:
    - the code aims to count how many characters have an odd frequency.

'''


'''
Appraoch #1: => - To help understand mark-###
                - (a) Trace ex1. dccaccd; ex2. dccbbaccd
                  (b) See Appraoch #2
'''
def longestPalindrome_set(s):
    ss = set()
    for letter in s:
        if letter not in ss:
            ss.add(letter)
        else:
            ss.remove(letter)
    if len(ss) != 0:
        return len(s) - len(ss) + 1  # mark-###
    else:
        return len(s)

'''
Commenter nbgraham about mark-### above
- it makes more sense to have a length count that you increment by two when removing each pair of letters, because that pair corresponds to the 2 letters in the palindrome

'''
def longestPalindrome(self, s: str) -> int:
    unpairedLetters = set({})
    palindromeLength = 0

    for letter in s:
        if letter in unpairedLetters:
            # found a pair
            palindromeLength += 2
            unpairedLetters.remove(letter)
        else:
            unpairedLetters.add(letter)

    if len(unpairedLetters) > 0:
        # can use at most one unpaired letter, in the middle of the palindrome
        palindromeLength += 1

    return palindromeLength
