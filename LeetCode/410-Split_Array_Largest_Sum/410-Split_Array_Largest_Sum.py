'''
410. Split Array Largest Sum

https://leetcode.com/discuss/general-discussion/786126/python-powerful-ultimate-binary-search-template-solved-many-problems


Goal:
- Return the "minimized" largest sum of a sub- array.

Note!! 
- QN notices both 410. Split Array Largest Sum and 875. Koko Eating Bananas uses Binary Search to look for the "MINIMUM"


思路: ->See the link above

(1) Left boundary is max(nums):
    - a sub-arr has to be able to contain at least 1 elem. The reason we 
      want max val so it "min" the largest sum of the rest of sub-arrs.

(2) Right boundary is sum(nums):
    - when m = 1, a sub-arr contains the same elems of the original arr.  
      Thus, the sum of all elems.
'''
class Solution:
    def splitArray(self, nums: List[int], k_splits: int) -> int:     
           
        # If feasible() returns true, cur threshold satisfies k_splits. 
        def feasible(threshold) -> bool:
            count = 1
            total = 0
            
            # Keep moving to the next sub-arr if cur sub-arr sum is larger 
            # than threshold and cub-arr count is less than k splits.
            for num in nums:
                total += num
                if total > threshold:
                    total = num
                    count += 1
                    if count > k_splits:
                        return False
            return True

        left = max(nums)
        right =  sum(nums) 

        # Vars left and right are modified in every iter, so they could be 
        # anywhere. When left is no longer smaller than right, cur mid is 
        # the minimized largest sum of the split.
        while left < right:

            # We set diff mids as a threshold to find the minimized largest
            # sum of the split
            mid = left + (right - left) // 2
           
            # - Reminder: See "Goal" & "Notice" above
            # - When var mid is the sum that splits nums[] into k_splits 
            #   sub-arrs, we move R ptr towards left side to lower the next
            #   mid val in the next iter.  We do it bc See "Goal"
            # - When  mid allows Koko to eat all bananas w/in h hrs, 
            #   we move right ptr towards left side to lower the next mid 
            #   val in the next iter. We do it bc See "Goal." 

            # See comments for feasible()
            if feasible(mid):
                right = mid     
            else:
                left = mid + 1
        return left
