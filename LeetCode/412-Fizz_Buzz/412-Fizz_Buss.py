'''
412. Fizz Buzz

LC Std Sol:
- https://leetcode.com/problems/fizz-buzz/solution/

'''

'''
Approach 1: Naive Approach

Time Complexity: O(N)
Space Complexity: O(1)
'''
class Solution:
  def fizzBuzz(self, n):
    ans = []
    for num in range(1,n+1):
      divisible_by_3 = (num % 3 == 0)
      divisible_by_5 = (num % 5 == 0)

      if divisible_by_3 and divisible_by_5:
        ans.append("FizzBuzz")
      elif divisible_by_3:
        ans.append("Fizz")
      elif divisible_by_5:
        ans.append("Buzz")
      else: # Not divisible by 3 or 5, add the number
        ans.append(str(num))
    return ans


'''
Approach 2 ==> Skip!  Bad!
'''


'''
Approach 3: Hash it!

Time Complexity: O(N)
Space Complexity: O(1)
'''
class Solution:
  def fizzBuzz(self, n):
    ans = []
    tbl = {3: "Fizz", 5: "Buzz"}

    for num in range(1, n+1):
      s = ""

      # If num is divisible by key, add corresponding str mapping to s
      for key in tbl.keys():
        if num % key == 0:
          s += tbl[key]

      if not s:  # "not str": Check empty str
        s = str(num)

      # Append the current answer str to the ans list
      ans.append(s)

    return ans
