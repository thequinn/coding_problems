/*
412. Fizz Buzz

LeetCode Std Solution:
- https://leetcode.com/problems/fizz-buzz/solution/
//
JS Solution:
  https://leetcode.com/problems/fizz-buzz/discuss/435799/JavaScript-Solutions-wVideo-Explanation-Three-Different-Methods

*/

// Method #1: Brute Force
// -
// - 思路:
//
// - Time complexity: O(N)
// - Space complexity: O(1)
var fizzBuzz = function(n) {
  let result = [];

  for (let i = 1; i <= n; i++) {
    if (i % 15 === 0) {
      result.push('FizzBuzz');
    } else if (i % 5 === 0) {
      result.push('Buzz');
    } else if (i % 3 === 0) {
      result.push('Fizz');
    } else {
      result.push(i.toString());
    }
  }

  return result;
};

// Method #2-1: String Concatenation
// - If our case change to: 3 ---> "Fizz" , 5 ---> "Buzz", 7 ---> "Jazz", b/c the mapping increases, the condition would grow exponentially.
// - Instead of checking for every combination of these conditions, check for divisibility by given numbers i.e. 3, 5 as given in the problem. If the number is divisible, concatenate the corresponding string mapping Fizz or Buzz to the current answer string.
//
// - Time complexity: O(N)
// - Space complexity: O(1)
//
var fizzBuzz = function(n) {
    var result = [], str;

    for (let i = 1; i <= n; i++)
        str = "";

        if (i%3 === 0)  str = 'Fizz';
        if (i%5 === 0)  str += 'Buzz';
        // if str is empty
        if (!str)       str += i;

        result.push(str);
    }
    return result;
};

// Method #2-2: String Concatenation
var fizzBuzz = function(n) {
  let res = Array(n).fill(0).map((a, i) =>
    (++i % 3 ? '' : 'Fizz') + (i % 5 ? '' : 'Buzz') || '' + i
  );

  return res;
};


// Method #3: Hash
// - What if you have too many mappings?  Use hash table.
// - Time Complexity : O(N)
// - Space Complexity : O(1)
//
var fizzBuzz = function(n) {
  let result = [];

  let mappings = {
    3: 'Fizz',
    5: 'Buzz'
  };

  for (let i = 1; i <= n; i++) {
    let str = '';

    for (let key in mappings) {
      if (i % parseInt(key, 10) === 0) {
        str += mappings[key];
      }
    }
    // if str is empty
    if (!str)     str += i;

    result.push(str);
  }
  return result;
};
