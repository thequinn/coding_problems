'''
415. Add Strings

'''

'''
Method #1: Convert char into unicode using ord()

LC Std Sol:
- https://leetcode.com/problems/add-strings/solution/

Time Complexity: O(max(n1, n2))
- n1 and n2 are length of num1 and num2.  We do max(n1, n2) iterations at most

Space Complexity: O(max(n1, n2))
- the length of the new string is at most max(n1, n2) + 1
'''
class Solution_1:

  def addStrings(self, num1: str, num2: str) -> str:
    res = []
    carry = 0

    # 注意! Get last indices of num1 and num2 b/c we calc from right-most digits
    p1 = len(num1) - 1
    p2 = len(num2) - 1

    while p1 >= 0 or p2 >= 0:
      # ord(): returns an integer representing the Unicode.
      x1 = ord(num1[p1]) - ord('0') if p1 >= 0 else 0
      x2 = ord(num2[p2]) - ord('0') if p2 >= 0 else 0

      value = (x1 + x2 + carry) % 10
      carry = (x1 + x2 + carry) // 10

      res.append(value)
      p1 -= 1  # Python doesn't have p1--
      p2 -= 1

    # After the while loop, check if there is any carry left
    if carry: res.append(carry)

    # String.join()
    # - returns a string by joining all the elem's of an "iterable", separated
    #   by a string separator.
    #
    # The param inside print() is "Generator Expression"
    # res[::-1] will reverse res[]
    print(str(x) for x in res[::-1])
    #
    #return ''.join( str(x) ) for x in res[::-1]   # 注意! WRONG !!!
    return  ''.join( str(x)   for x in res[::-1] )

'''
Method #2: Convert char into int using int()

Sol Code:
- https://leetcode.com/problems/add-strings/discuss/436345/Python3-Easy-Code-with-One-Loop-guides-provided

Note:
- This code taught me how to (1) reverse a stringin Python
- But since the code reverse the input strings and at the end reverse the result string again, it's slower.
'''
class Solution_2:

  def addStrings(self, num1: str, num2: str) -> str:
    res = ''

    # a[::-1]: starts from the end towards the 1st taking each elem
    num1 = num1[::-1]
    num2 = num2[::-1]

    carry = i = 0
    l1, l2 = len(num1), len(num2)

    # Checking carry in the while loop eliminates extra ln in ln-41, Method #1
    while i < max(l1, l2) or carry:
      dig1  = int(num1[i]) if i < l1 else 0
      dig2  = int(num2[i]) if i < l2 else 0

      value = (dig1 + dig2 + carry) % 10
      carry = (dig1 + dig2 + carry) // 10

      res += str(value)
      i += 1

    return res[::-1]


#-----Test Cases-----

#sol = Solution_1()
sol = Solution_2()

num1 = '333'
num2 = '18'

print(sol.addStrings(num1, num2))
