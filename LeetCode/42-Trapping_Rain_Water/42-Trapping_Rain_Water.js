/*
42. Trapping Rain Water

LeetCode Std Sol:
- https://leetcode.com/problems/trapping-rain-water/solution/


思路 (Video): ==> It has 3 parts, 雖然有點長,但奠定很強基礎!

- Part 1: https://www.youtube.com/watch?v=HmBbcDiJapY
-- 思路講解開始 @4:03
-- ***** 解題key @4:42 ***** ===>> 太重要了!!!!!

  ex. height = [0,1,0,2,1,0,1,3,2,1,2,1];
                      ^   ^   ^
                      l   i   r
    - At index=5, the water accumulated is the "min height" of its heighest
      left building and highest right building.

- Part 2: https://www.youtube.com/watch?v=VZpJxINSvfs&list=RDVZpJxINSvfs&start_radio=1
-- @1:27 講解 height[i] 比左右 buildings 高, 這樣會有 negative water.

  ex. height = [0,1,0,2,1,0,1,3,2,1,2,1];
                      ^       ^     ^
                      l       i     r
- Part 3: https://www.youtube.com/watch?v=XqTBrQYYUcc&t=100s

*/

// Approach 1: Brute Force => 必須先懂
//                         => See: Trapping_Rain_Water-Approach_1.png
//
// - Time complexity: O(n^2)
// -- For each element of array, we iterate the left and right parts.
// - Space complexity: O(1) extra space.


// Approach 2: Dynamic Programming => Optimized from Approach 1
//                                 => See: Trapping_Rain_Water-Approach_2.png
// - Time Complexity: O(n)
// - Space Complexity: O(n), Additional space for leftMax[] and rightMax[]
//
var trap = function(height) {
  if (height == null)   return 0;

  let size = height.length;
  let leftMax = [], rightMax = [];

  leftMax[0] = height[0];
  for (let i = 1; i < size; i++) { // Left-most elevation can't accu water
    leftMax[i] = Math.max(height[i], leftMax[i - 1]);
  }

  rightMax[size - 1] = height[size - 1];
  for (let i = size - 2; i >= 0; i--) { // Right-most elevation can't accu water
    rightMax[i] = Math.max(height[i], rightMax[i + 1]);
  }

  //console.log("leftMax[]:", leftMax);
  //console.log("rightMax[]:", rightMax);
  let totalWater = 0, minBuildingH = 0, waterH = 0;
  for (let i = 1; i < size - 1; i++) { // Right-most & left-most cant accu water

    // lower building (elevation) decides height of water to accu
    minBuildingH = Math.min(leftMax[i], rightMax[i]);

    waterH = minBuildingH - height[i];
    totalWater += waterH;

    //console.log(minBuildingH, ", ", waterH, ", ", totalWater);
  }

  return totalWater;
};


//Approach 3: Using stacks .....未.....

//Approach 4: Using 2 pointers  .....:w
//未.....


//-----Test Cast-----
let height = [0,1,0,2,1,0,1,3,2,1,2,1];  // 6
console.log("result: ", trap(height));
