/*
43. Multiply Strings

Requirement:
1) The length of both num1 and num2 is < 110.
2) Both num1 and num2 contain only digits 0-9.
3) Both num1 and num2 do not contain any leading zero, except the number 0 itself.
4) You must not use any built-in BigInteger library or convert the inputs to integer directly.

*/

// Solution from QN => It does NOT fullfill Requirement 4)
//                     But have to know how to convert strInt to int: ln-17~20
//
var multiply = function(num1, num2) {
  // Convert input to int
  let n1 = 0, n2 = 0;
  for (let i = 0; i < num1.length; i++) {
    n1 = 10 * n1 + Number(num1[i]);
  }
  for (let i = 0; i < num2.length; i++) {
    n2 = 10 * n2 + Number(num2[i]);
  }
  console.log("n1:", n1, ", n2:", n2);

  // multiply them, then convert result to str
  return String(BigInt(n1) * BigInt(n2));
};

//-----Test Case-----

//let num1 = "123", num2 = "456";  // 56088

/*
When an integer in JS is too big to fit in a 32 bit value, it will lose some precision.  We can use BigInt to fix it. So w/o using BigInt, the multiplication result below is "121932631112635260"

BigInt is a built-in object that provides a way to represent whole numbers larger than 253 - 1
*/
let num1 = "123456789", num2 = "987654321"; // "121932631112635269"

let result = multiply(num1, num2);
console.log("result:", result);
