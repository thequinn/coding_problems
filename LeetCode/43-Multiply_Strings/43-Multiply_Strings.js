/*
43. Multiply Strings

Explanation + Code Sol: See Solution.java

*/

// Sol Code in JS:
// - https://leetcode.com/problems/multiply-strings/discuss/444777/JavaScript%3A-Readable-Code-For-Humans.-Intuitive-simple.
//
var multiply = function(num1, num2) {
    let answer = Array(num1.length + num2.length ).fill(0), string = ""
    for(let i = num1.length-1; i>=0; i--) {
        for(let j = num2.length-1; j>=0; j--) {
            let mul = Number(num1[i])* Number(num2[j])
            let p1 = i + j
            let p2 = i + j + 1
            let sum = mul + answer[p2]
            answer[p1] += Math.floor(sum/10)
            answer[p2] = sum % 10
        }
    }
    for(let p of answer) {if(!(string.length === 0 && p === 0)) {string += p}}
    return string.length === 0 ? "0" : string
};

//-----Test Case-----
//let num1 = "123", num2 = "456";  // 56088
let num1 = "123456789", num2 = "987654321"; // "121932631112635269"

let result = multiply(num1, num2);
console.log("result:", result);
