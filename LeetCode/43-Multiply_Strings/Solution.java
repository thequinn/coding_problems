/*
43. Multiply Strings

Requirement:
1) The length of both num1 and num2 is < 110.
2) Both num1 and num2 contain only digits 0-9.
3) Both num1 and num2 do not contain any leading zero, except the number 0 itself.
4) ***** You must not use any built-in BigInteger library or convert the inputs to integer directly.


>拆題：
(1) Remember how we do multiplication?
  Start from right to left, perform multiplication on every pair of digits, and add them together.

  See process graph:
  - (1) 解釋圖.png   or
  - (2) https://leetcode.com/problems/multiply-strings/discuss/17605/Easiest-JAVA-Solution-with-Graph-Explanation)

  From the following draft, we can immediately conclude:

    `num1[i] * num2[j]` will be placed at indices `[i + j`, `i + j + 1]`

(2) m位的数字乘以n位的数字的结果最大为m+n位：
  ex: 999*99 < 1000*100 = 100000，最多为3+2 = 5位数。


>Solution:
(1) Explanation + Code in Java:
  https://leetcode.com/problems/multiply-strings/discuss/17605/Easiest-JAVA-Solution-with-Graph-Explanation
(2) Video
  https://www.youtube.com/watch?v=CnEFY5Y3Z68

*/

class Solution {
  public static String multiply(String num1, String num2) {
    int m = num1.length(), n = num2.length();
    int[] pos = new int[m + n];

    for(int i = m - 1; i >= 0; i--) { // From right to left
      for(int j = n - 1; j >= 0; j--) {

        int mul = (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
        int p1 = i + j, p2 = i + j + 1;
        System.out.println("\ni:" + i + ", j:" + j + ", p1:" + p1 + ", p2:" + p2);

        //....
        int sum = mul + pos[p2];  // pos[p2] 就是iteration “要進位的數” ln-42
        System.out.println("mul:" + mul + ", sum:" + sum + ", pos[p2]:" + pos[p2]);

        System.out.println("前: pos[p1]:" + pos[p1] + ", pos[p2]:" + pos[p2]);
        pos[p1] += sum / 10;  // 要進位的數
        pos[p2] = (sum) % 10; // 有結果的數 (現在就可紀錄)
        System.out.println("後: pos[p1]:" + pos[p1] + ", pos[p2]:" + pos[p2]);
      }
    }

    // Remove leading 0's
    StringBuilder sb = new StringBuilder();
    for(int p: pos)
      if(!(sb.length() == 0 && p == 0)) sb.append(p);

    return sb.length() == 0 ? "0" : sb.toString();
  }

  public static void main(String args[]) {
    String num1 = "123", num2 = "45";  // 5535
    //String num1 = "123", num2 = "456";  // 56088
    //String num1 = "123456789", num2 = "987654321"; // "121932631112635269"

    String result = multiply(num1, num2);
    System.out.println("result:" + result);
  }
}
