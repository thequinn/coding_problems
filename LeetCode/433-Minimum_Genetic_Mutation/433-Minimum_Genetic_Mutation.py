'''
433. Minimum Genetic Mutation

解釋 + Sol by 负雪明烛:
- https://blog.csdn.net/fuxuemingzhu/article/details/82903720


解题方法
- 基本和127. Word Ladder 一模一样 只不过把26个搜索换成了4个搜索，
  所以代码只用改变搜索的范围，以及最后的返回值就行了。
- 很显然这个问题是BFS的问题，同样是走迷宫问题的4个方向，
  代码总体思路很简单，就是利用队列保存每个遍历的有效的字符串，
  然后对队列中的每个字符串再次遍历，保存每次遍历的长度即可。
  每个元素进队列的时候，保存了到达这个元素需要的步数，
  这样能省下遍历和记录当前bfs长度部分代码。

时间复杂度是O(NL), 其中N是Bank中的单词个数，L是基因的长度
空间复杂度是O(N)

'''

class Solution(object):
    def minMutation(self, start, end, bank):
        bfs = collections.deque()
        bfs.append((start, 0))
        bankset = set(bank)

        while bfs:
            gene, step = bfs.popleft()

            if gene == end:             # v visited?
                return step

            for i in range(len(gene)):  # v's neighbors visited?
                for x in "ACGT":
                    newGene = gene[:i] + x + gene[i+1:]
                    if newGene in bank and newGene != gene:
                        bfs.append((newGene, step + 1))
                        bank.remove(newGene)
        return -1


#-----Test Cases-----
start = "AACCGGTT"
end   = "AACCGGTA"
bank  = ["AACCGGTA"]  # return: 1

start = "AACCGGTT"
end   = "AAACGGTA"
bank  = ["AACCGGTA", "AACCGCTA", "AAACGGTA"]  # return: 2
#The 2nd str in bank[], "AACCGCTA" has no path to reach "end", which is "AAACGGTA".  So it doesn't count as an answer.  The reason is that it takes 2 mutations to "end".

start = "AAAAACCC"
end   = "AACCCCCC"
bank  = ["AAAACCCC", "AAACCCCC", "AACCCCCC"]  # return: 3

