
/*
QN's Sol: using global var, totalPaths
 |
 |--> Waiting for Q&A:...未

    Test case passed under "Run Code" but not under "Submit"

    https://leetcode.com/problems/path-sum-iii/discuss/820788/Test-case-passed-under-%22Run-Code%22-but-not-under-%22Submit%22

*/

let totalPaths = 0;

const pathSum = (root, targetSum) => {
  if (root == null)   return 0;

  // DFS: Root
  let pathFromRoot = pathSumFromNode(root, targetSum, 0);

  // DFS: Left and Right
  let pathOnLeft  = pathSum(root.left , targetSum);
  let pathOnRight = pathSum(root.right, targetSum);

  return totalPaths;
}

const pathSumFromNode = (root, targetSum, currSum) => {
  if (root == null)   return 0;

  // DFS: Root
  currSum += root.val;
  if (currSum == targetSum)   totalPaths++;

  // DFS: Left and Right
  pathSumFromNode(root.left , targetSum, currSum);
  pathSumFromNode(root.right, targetSum, currSum);
}


//-----Test Case-----
[5,4,8,11,null,13,4,7,2,null,null,5,1]
22

Output:   3
Expected: 3
