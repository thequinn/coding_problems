

/*
437. Path Sum III

思路:
- s1. Helper Func: Traverse each node using DFS preorder traversal

  s2. Use the node as root forming a subtree.  Recursively try all paths
      downwards to see if each path forms a target sum.


非常重要!!
- Recursion 技巧
    記住, 用 local var (ln-40), 不用 global var (ln-11)

*/

let totalPaths = 0;

const pathSum = (root, targetSum) => {
  if (root == null)   return 0;

  // DFS: Root
  let pathFromRoot = pathSumFromNode(root, targetSum, 0);

  // DFS: Left and Right
  let pathOnLeft  = pathSum(root.left , targetSum);
  let pathOnRight = pathSum(root.right, targetSln-40
  return pathFromRoot + pathOnLeft + pathOnRight;
}

const pathSumFromNode = (root, targetSum, currSum) => {
  if (root == null)   return 0;

  // DFS: Root
  currSum += root.val;
  //
  let totalPaths = 0; // 非常重要!! 記住用 local var, 不用 global var (ln-19)
  if (currSum == targetSum)   totalPaths++;

  // DFS: Left and Right
  totalPaths += pathSumFromNode(root.left , targetSum, currSum);
  totalPaths += pathSumFromNode(root.right, targetSum, currSum);

  return totalPaths;
}
