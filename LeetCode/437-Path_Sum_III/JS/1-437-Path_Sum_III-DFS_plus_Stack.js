/*
437. Path Sum III

*/

// Approach #A-1: Recursive Backtracking + for-loop  --> Syntax Structure #A-1
//
// 思路: ==> QN 所理解以下的code
// - 1. This is DFS Post-order Traversal. (Can use Pre/In/Post-order)
//
// - 2. Inside getSum(): Traversing in post-order (left-right-root)
//
//    2-0. stack[] records each parent/curr node of the path.  It will be
//         summed up in the for loop later
//
//    2-1. The 1st getSum() checks if curr_root -> curr_node (left child) has
//         any sub-tree that satisfies "sum"
//
//    2-2. The 2nd getSum() checks if curr_root -> curr_node (right child) has
//         any sub-tree that satisfies "sum"
//
//    2-3. After the 2 getSum(), the for loop checks if curr_root -> curr_node
//         (which is the parent of the left & right children) has any sub-tree
//         that satisfies "sum"
//
// - 3. Inside getSum(): Backtrack
//    3-1. Before the curr recursive call returned to its caller, the callee
//         needs to remove itself from stack.  So the callee's path doesn't
//         include this node.
//
// 思路 + Sol Code:
// - https://www.youtube.com/watch?v=x87RihNvRaY
// - dwheloer/LeetCode/LeetCode 437 - Path Sum III - jayati tiwari.mp4
//
// Time Complexity: O(n^2)
// - In every getSum() we are traversing a stack that stores the nodes
//   traversed till that point, which roughly makes it O(N^2)
//
const pathSum = (root, sum) => {

  let count = 0;
  let stack = [];

  const getSum = (root, sum) => {
    if (root == null)   return 0;

    stack.push(root.val);

    // DFS: Post-order traversal - traverse left & right children
    getSum(root.left , sum);
    getSum(root.right, sum);

    // DFS: Post-order traversal - work on the roots themselves
    let tmp = 0;
    for (let i = stack.length - 1; i >= 0; i--) {
      tmp += stack[i];
      // 重要: shows what each path is consisted of in this recur-backtrack alg
      // - Here are the paths in the prob ex:
      //    3,3,5,0; -2,3,5,0; 3,5,0; 1,2,5,0; 2,5,0; 5,0; 11,-3,10; -3,10; 10
      console.log("stack[i]:", stack[i], "tmp:", tmp);

      if (tmp == sum)   count++;
    }

    // Backtrack:
    // - Remove last node added to the stack earlier (Let's call it node-x).
    //   This is b/c when this recursive call is returned to its caller, the
    //   path being considered won't include node-x.
    // - In the case of problem 427, one example is that the left node has to
    //   be removed in order to add the right node, so we have a new path.
    stack.pop();
  };

  getSum(root, sum);
  return count;
}

//-------------------------------------------

// Approach #A-2: Same 思路 as Approach #A-1  --> Syntax Structure #A-2
// - https://leetcode.com/problems/path-sum-iii/discuss/91914/C%2B%2B-13ms-recursive-using-a-stack
//
let count = 0;
let stack = [];

const pathSum = (root, sum) => {
  if (!root)  return 0;

  stack.push(root.val);

  // DFS: Post-order traversal - Add num of paths from left and right childs
  let count = pathSum(root.left, sum) + pathSum(root.right, sum);

  // DFS: Post-order traversal - Add num of paths from root to curr node
  let tmp = 0;
  for (let i = stack.length - 1; i >= 0; i--) {
    tmp += stack[i];
    if (tmp == sum)   count++;
  }

  stack.pop(); // Backtrack

  return count;
};


