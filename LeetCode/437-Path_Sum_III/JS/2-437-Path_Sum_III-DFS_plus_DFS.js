/*
437. Path Sum III

- 思路(1)(2) 相輔相成. 思路(1)(2)的 videos  及答案是同樣的, 只是答案用不同
  Double Recursion Syntax Structures. (第二個方法較易懂, 先看它)

- 思路 Summary:
-- Each time find all the path start from current node.  Then move start node
   to the child and repeat.
   - (1st layer DFS):
     - 轉換不同的 root. Use recursive traverse to go through each node (can be
       any order: pre, in, post all fine).
   - (2nd layer DFS):
     - 在每個 root 找所有返回路徑.
       If a path sum equals to the target: self.numOfPaths += 1

-------------------------------------

- 思路(1):
    - https://www.youtube.com/watch?v=LXcpMokpKes     or
    - /dwhelper/LeetCode/Leetcode 437 - Path Sum III - codebix.mp4
       |
       ==> 重要!! It shows 正確/錯誤 examples!!

- 思路(2):
    - https://www.youtube.com/watch?v=NTyOEYYyv-o     or
    - /dwhelper/LeetCode/LeetCode 437 - Path Sum III 【公瑾讲解】.mp4

-----------------------------------------------------

- Time & Space Analysis:
-- https://leetcode.com/problems/path-sum-iii/discuss/141424/Python-step-by-step-walk-through.-Easy-to-understand.-Two-solutions-comparison.-%3A-)


- Time Complexity: O(N^2) for the worst case,
                   O(NlogN) for balanced binary tree
-- Time complexity depends on the two DFS.
-- 1st layer DFS will always take O(n), due to here we will take each node out, there are in total n TreeNodes.  2nd layer DFS will take range from O(n) (single sided tree) to O(logn)(balanced tree). This is due to here we get all the paths from a given node. The length of path is proportional to the tree height. Therefore, the total time complexity is O(nlogn) to O(n^2).

- Space Complexity: O(1), due to there is no extra cache.
-- However, for any recursive question, we need to think about stack overflow,
   namely the recursion should not go too deep.
-- Assume we have n TreeNodes in total, the tree height will vary from O(n)
   (single sided tree) to O(logn)(balanced tree).
-- The two DFS will go as deep as the tree height.

*/


//==============================================

// Approach #1: From 思路(1)  --> Double Recursion Syntax Structure #1
// - https://www.youtube.com/watch?v=LXcpMokpKes
//
// 注意!!
// - This sol is translated from 437-Path_Sum_III-DFS.py
// -- The Python version passed all tests.
// -- The JS version doesn't pass the test case below when doing "Submit," but it passes the case when doing "Run Code." So it's LeetCode's problem!
//
//    Test Case: [5,4,8,11,null,13,4,7,2,null,null,5,1],  22
//
let totalPaths = 0;

const pathSum = (root, sum) => {
  if (!root)    return 0;

  // Traverse whole sub-tree based on curr root
  traverseAllPaths(root, sum, 0);
  // Switch root
  pathSum(root.left , sum);
  pathSum(root.right, sum);

  return totalPaths;
};

const traverseAllPaths = (root, sum, currSum) => {
  if (!root)    return;

  currSum += root.val;
  if (sum == currSum)   totalPaths += 1;

  traverseAllPaths(root.left , sum, currSum);
  traverseAllPaths(root.right, sum, currSum);
};

//==============================================

// Approach #2-1: From 思路(2) --> Double Recursion Syntax Structure #2-1
// - https://github.com/yuzhoujr/leetcode/blob/master/tree/Yu/437.py
//  |
//  ===> Double Recursion Syntax Structure #1-2 的程式碼其實和這個一樣, 只是吧
//       findPath() 提出來.
//
const pathSum = (root, sum) => { // Switch roots
  if (!root)    return 0;

  const dfs = (root, sum) => {   // Traverse whole sub-tree based on curr root
    let count = 0;

    if (!root)    return 0;
    if (root.val == sum)    count +=1;

    // "sum - root.val":
    // - Pass left-over amount to child node.  So when in the child's
    //   recursive call, if its val == the left-over val, an ans is found.
    count += dfs(root.left , sum - root.val);
    count += dfs(root.right, sum - root.val);
    return count;
  };

  return dfs(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
};

//-----------------------------
// Approach #2-2: From 思路(2) --> Double Recursion Syntax Structure #2-2
// - https://leetcode.com/problems/path-sum-iii/discuss/91884/Simple-AC-Java-Solution-DFS
//
const pathSum = (root, sum) => {  // Switch roots
  if (root == null)   return 0;

  return findPath(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
};

const findPath = (root, sum) => { // Traverse whole sub-tree based on curr root
  let res = 0;

  if (root == null)       return res;
  if (sum == root.val)    res++;

  res+= findPath(root.left , sum - root.val);
  res+= findPath(root.right, sum - root.val);
  return res;
};


//##########################################################
// 1. 從這裡開始有優化.
// 2. 注意!!
//    - 以下這個解法裡的 ”Double Recursion Syntax Structure #3“ 比之前
//      的都要簡潔.只用了一個 DFS func 就處理兩種狀況!!
//
//##########################################################

// Approach #3: 和思路 (1) (2) 同, Optimized w/ DP:Memo:
//                                  --> Double Recursion Syntax Structure #3
// - https://leetcode.com/problems/path-sum-iii/discuss/377535/Javascript-Read-this-solution-if-you've-given-up-hope-and-just-want-to-see-some-progress
//
// Optimization:
// - Using hash table/Set to ensure you don't start from a node more than once
//      |
//      ==> 注意!!
//          - 這個方法QN看不懂!!!  一個function裡有4個recursive calls, 太複雜!
//            QN 不贊成這麼使用, 跳過!!
//
var pathSum = (root, sum) => {
  let count = 0;
  let startedFrom = new Set();

  dfs(root, sum, true); // 'true': you can't visit root node twice
  return count;


  function dfs(node, remaining, isStartingFromThisNode) {
    if (!node)   return;

    if (isStartingFromThisNode) {
      // Only start from this node "once" throughout the whole solution.
      // Doing 2 dfs() calls from each node means each node of depth h gets
      // visited 2^h times, which is undesirable.
      if (startedFrom.has(node))    return;
      startedFrom.add(node);
    }

    if (remaining - node.val === 0)    count++;

    dfs(node.left, remaining-node.val, false);
    dfs(node.right, remaining-node.val, false);

    dfs(node.left, sum, true);
    dfs(node.right, sum, true);
  }
}


