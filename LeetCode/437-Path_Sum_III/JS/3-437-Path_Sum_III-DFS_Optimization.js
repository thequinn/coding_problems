/*
437. Path Sum III   ===> Similar problem: LeetCode 560. Subarray Sum Equals K

//---------- Test Cases ----------
[10,5,-3,3,2,null,11,3,-2,null,1]       ---> ok
8

[5,4,8,11,null,13,4,7,2,null,null,5,1]  ---> 容易錯
22

[1]                                     ---> 容易錯
0
*/

// Approach #I-1: Backtracking, DP + Memo
//
// 思路: ==> QN 所理解以下的code
// - 1. This is DFS Pre-order Traversal. (Can use Pre/In/Post-order)
//
// 思路 + Sol Code:
// -(1) https://leetcode.com/problems/path-sum-iii/discuss/141424/Python-step-by-step-walk-through.-Easy-to-understand.-Two-solutions-comparison.-%3A-)
// - (2) https://leetcode.com/problems/path-sum-iii/discuss/91915/My-detailed-explanation-to-the-HashMap-method
//
// Time Complexity : O(n)
// - O(n) as we just traverse once
//
// Space Complexity: O(n).
// - For a balanced tree we use O(log n) space, otherwise as bad as O(n)
//
const pathSum = (root, target) => {
  let result = 0;

  // - Create a hash tbl using an object.
  // - Init { 0: 1 } b/c root node is 0 and target 0.  And null node is diff
  //   from node with value 0
  let cache = { 0: 1 };

  dfs(root, target, 0, cache);
  return result;


  function dfs(root, target, currPathSum, cache) {
    if (!root)    return;


    /*-----------------------------*/
    /* DFS: Pre-order traversal: Work on the root itself */

    // Update the prefix sum by adding the current val
    currPathSum += root.val;

    // Get the number of valid path, ended by the current node
    let oldPathSum = currPathSum - target;
    result += cache[oldPathSum] || 0;

    // - Update hash tbl w/ the current sum
    // - See ln-85.  It explains why it can't be put right behind ln-50
    cache[currPathSum] = (cache[currPathSum] || 0) + 1;


    /*-----------------------------*/
    /* DFS: pre-order traversal - traverse left & right children */
    dfs(root.left , target, currPathSum, cache);
    dfs(root.right, target, currPathSum, cache);


    /*-----------------------------*/
    /* Backtrack */
    // - When move to a different branch, the currPathSum is no longer
    //   available, hence remove one.
    cache[currPathSum] -= 1;
  }
};

// Approach #I-2: 思路同 Approach #I-1, 寫法較簡短
const pathSum = (root, target) => {
  let cache = {0: 1};
  return dfs(root, 0, target, map);

  const dfs = (root, target, currPathSum, cache) => {
    if (root == null)   return 0;

    currPathSum += root.val;

    // This ln has to be put after numPathtoCurr is assigned a val in ln-91
    // b/c the key, oldPathSum, of cache{} could be the same as currPathSum.
    //
    //cache[currPathSum] = (cache[currPathSum] || 0) + 1; // WRONG!

    let oldPathSum = currPathSum - target;
    let numPathtoCurr = cache[oldPathSum] || 0;

    cache[currPathSum] = (cache[currPathSum] || 0) + 1;   // Correct

    let res = numPathToCurr +
              dfs(root.left , target, currPathSum, cache) +
              dfs(root.right, target, currPathSum, cache);
  };

  cache[currPathSum] -= 1;

  return res;
};


