

/*
437. Path Sum III

思路:
- s1. Helper Func: Traverse each node using DFS preorder traversal

  s2. Use the node as root forming a subtree.  Recursively try all paths
      downwards to see if each path forms a target sum.


非常重要!!
- Recursion 技巧
    記住, 用 local var (ln-40), 不用 global var (ln-11)

*/
const pathSum = (root, target) => {
  // - Create a hash tbl using an object.
  // - Init { 0: 1 } b/c root node is 0 and target 0.  And null node is diff
  //   from node with value 0
  //let hash = { 0: 1 };
  //
  let hash = new Map();

  return dfs(root, target, 0, hash);
}

const dfs = (node, target, currPathSum, hash) => {
  if (node == null)    return 0;


  // Part #1: Work on root

  /*------------------------------------*/
  /* DFS s1: DFS Preorder traversal - Root -*/

  // Update the prefix sum by adding the current val
  currPathSum += node.val;

  // Get the number of valid paths from hash
  let oldPathSum = currPathSum - target;
  let totalPaths = hash[oldPathSum] || 0;

  // If currPathSum equals to target, one more path is found
  // 注意!! This doesn't increment individual entry of hash tbl
  if (currPathSum == target)    totalPaths++;
  /*------------------------------------*/



  // Part #2: Increment hash{} -> Recurse -> Decrement hash{} (Backtrack)

  // - Update hash tbl w/ the current sum
  hash[currPathSum] = (hash[currPathSum] || 0) + 1;

  /*------------------------------------*/
  /* DFS s2: DFS Preorder traversal - L & R */
  dfs(node.left , target, currPathSum, hash);
  dfs(node.right, target, currPathSum, hash);
  /*------------------------------------*/

  // Backtrack
  // - When move to a different branch, the currPathSum is no longer
  //   available, hence remove one.
  hash[currPathSum] -= 1;
};
