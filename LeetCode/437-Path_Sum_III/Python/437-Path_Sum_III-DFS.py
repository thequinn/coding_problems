"""
437. Path Sum III


"""

# From 思路(1)
#
class Solution(object):
  totalPaths = 0

  def pathSum(self, root: TreeNode, sum: int) -> int:
    if not root:    return 0

    self.traverseAllPaths(root, sum, 0)
    self.pathSum(root.left , sum);
    self.pathSum(root.right, sum);

    return self.totalPaths;

  def traverseAllPaths(self, root, sum, currSum):
    if not root:    return

    currSum += root.val
    if sum == currSum:
        self.totalPaths += 1

    self.traverseAllPaths(root.left , sum, currSum)
    self.traverseAllPaths(root.right, sum, currSum)


# From 思路(2)
#
class Solution(object):
  def pathSum(self, root, sum):
    # Edge Case:
    if not root:  return 0

    # Process
    def dfs(root, sum):
      count = 0

      if not root:  return 0
      if root.val == sum:  count += 1

      count += dfs(root.left, sum - root.val)
      count += dfs(root.right, sum - root.val)
      return count

    # Recursion
    return dfs(root, sum) + self.pathSum(root.left, sum) + self.pathSum(root.right, sum)

