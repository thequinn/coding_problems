

Note!!

1. 原題: CTCI 4.12 Paths with Sum ==> 它的基本解法最易懂, 而且有優化解答!
   -- 思路: Recursive "Only", Downwards

2. 2020/08/26: 剛買LeeCode Premium, 需要看這題的 LeetCode Std Solution....

===========================================================================


1.
有些 JS dir 裡程式碼是 Python dir 轉換過來的.
但 Python dir 裡的答案才是最完整的.



2.
注意!!
(1) Trace code 時, 用樹狀圖 trace recursive calls, but not a 1-D stack!!

(2) 1-437-Path_Sum_III-DFS_plus_Stack.js ==> 最易懂, 沒找到優化解答
    - Approach #A uses 1 recursion and 1 for loop (stack)
    -- 思路: Recursive + Backtracking, Upwards

----------------------------------------------

>>要做出這題有兩部分:

(1) 思路:
- s1. 看 2-437-Path_Sum_III-DFS_plus_DFS.js 裡的思路 (1) (2).
- s2. 看下方:  (i)  思路: ==> QN 所理解以下的code
               (ii) Trace code
(2) 理解程式碼:
- s1. Trace: 1-437-Path_Sum_III-DFS_plus_Stack.js  (理解完這個,後面就比較簡單)
- s2. Trace: 2-437-Path_Sum_III-DFS_plus_DFS.js
             (i)  Approach #1 優先
             (ii) Approach #2

(3) 優化程式碼:
- s1. 3-437-Path_Sum_III-DFS_Optimization.js
  |
  ==> (3) 如果實在看不懂, 先學相似題: LeetCode 560. Subarray Sum Equals K


3.
以下網頁要看.加強基礎:  ==> 2 locations

-(1) https://leetcode.com/problems/path-sum-iii/discuss/534823/Summary-of-4-solutions-(-keypoints-in-both-English-and-Chinese-)

-(2) Summary of 4 solutions ( key points in both English and Chinese ) - LeetCode Discuss

