'''
438. Find All Anagrams in a String


Text Explanation & Python Code:
-> Note: The sol might look simple, but the sol below is better and simpler
- https://leetcode.com/problems/find-all-anagrams-in-a-string/solutions/1737985/python3-sliding-window-hash-table-explained/


Vid Explanation + Python Code:
- https://www.youtube.com/watch?v=G8xtZy0fDKg

'''
def findAnagrams(self, s: str, p: str) -> List[int]:

    # str p is longer than str s, there is no way an anagram of p exists in s
    if len(p) > len(s): return []

    # Add freq of each char of p to 2 dicts
    # Note: This part can be replaced by class collections.Counter(). But some
    #       interviewers might not allow it since it does the work for you.
    pCount, sCount = {}, {}
    for i in range(len(p)):
        # get() init a field in dict to 0 in in case it's not init yet
        pCount[p[i]] = 1 + pCount.get(p[i], 0)
        sCount[s[i]] = 1 + sCount.get(s[i], 0)

    # If sCount and pCount have the same hash, the len(p) chars form an
    # anagram in sCount in the first len(p) chars.
    res = [0] if sCount == pCount else []

    # Sliding window range: l and r ptrs
    l = 0
    # Increment r ptr
    for r in range(len(p), len(s)):

        # Update entry r ptr pting to s
        sCount[s[r]] = 1 + sCount.get(s[r], 0)

        # Remove entry l ptr pting to s
        sCount[s[l]] -= 1

        # If an entry's value is 0, remove it.  Must do bc when comparing
        # sCount==pCount, if 1 hash has a entry of 0 and the other doesn't,
        # the 2 hashes won't be equal.
        if sCount[s[l]] == 0:
            sCount.pop(s[l])

        # This and increment of r ptr(ln-33) => slides the whole window right
        # Notice: l & r currently include the correct range of the cur window
        l += 1

        if sCount == pCount:
            res.append(l)

    return res


