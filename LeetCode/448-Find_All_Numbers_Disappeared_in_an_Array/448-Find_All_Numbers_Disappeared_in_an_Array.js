/*
448. Find All Numbers Disappeared in an Array



*/

// Approach #1: Using Set w/ Extra Space => Passed
// - 思路:
// -- Using Set to remove duplicate elements from arr. Iterate from [1 to N] and
//   add to result list if i is not in the marked set.
//
// - Solution Code:
// -- https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/discuss/93153/Step-by-step-detailed-explanation-(Python-Solution)
//
/*var findDisappearedNumbers = function(nums) {
  let res = [];
  let marked = new Set(nums);

  // 題目給: nums[] where 1 ≤ a[i] ≤ n (n = size of array)
  for (let i = 1; i <= nums.length; i++) {
    if (!marked.has(i))
      res.push(i);
  }
  return res;
}*/

// Approach #2: Using Array w/o Extra Space => Passed
//
// - 思路 Video:
// -- https://www.youtube.com/watch?v=CTBEcmzLAuA
// -- 1. Iterate nums[] and mark the pos implied by every element as negative.
//    2. In 2nd iteration, we simply need to report the positive numbers.
//
// - Solution Code:
// -- https://terriblewhiteboard.com/find-all-numbers-disappeared-in-an-array-leetcode-448/
//
let findDisappearedNumbers = function(nums) {
  let result = [];

  for (let i = 0; i < nums.length; i++) {
    let j = Math.abs(nums[i]) - 1;
    nums[j] = Math.abs(nums[j]) * -1;
  }

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] > 0) {
      // "+1" b/c we "-1" in ln-41
      result.push(i + 1);
    }
  }

  return result;
};

//---------- Testing ----------
let nums = [4,3,2,7,8,2,3,1];  // [5,6]
let res = findDisappearedNumbers(nums);
console.log("res:", res);
