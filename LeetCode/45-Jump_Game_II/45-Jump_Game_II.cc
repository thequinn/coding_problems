/*
45. Jump Game II

https://leetcode.com/problems/jump-game-ii/solutions/1192401/easy-solutions-w-explanation-optimizations-from-brute-force-to-dp-to-greedy-bfs/
*/


/*
Approach #1: Brute Force - Recursion

Time Complexity : O(N!) 
- At each index i we have N-i choices and we recursively explore each of them till end. So we require O(N*(N-1)*(N-2)...1) = O(N!)

Space Complexity : O(N)
*/
class Solution {
public:
    int jump(vector<int>& nums, int pos = 0) {
	    if(pos >= size(nums) - 1) return 0;      

        // initialising to max possible jumps + 1
	    int minJumps = 10001;  

        // explore all possible jump sizes from current position
	    for(int j = 1; j <= nums[pos]; j++)  
            // 1 is added in the 2nd arg or min() bc j is init'ed to 1
		    minJumps = min(minJumps, 1 + jump(nums, pos + j));        
	    return minJumps;
    };
};



/*
Approach #2: Top-Down DP: Recursive Memoization

Time Complexity : O(N^2)
Space Complexity : O(N)
*/
int jump(vector<int>& nums) {
	// init all to max possible jumps+1 denoting dp[i] hasn't been computed yet
    vector<int> dp(size(nums), 10001); 	
    return solve(nums, dp, 0);
}
// recursive solver to find min jumps to reach end
int solve(vector<int>& nums, vector<int>& dp, int pos) {
	
    // when we reach end, return 0 denoting no more jumps required
    if(pos >= size(nums) - 1) return 0;    	

    // number of jumps from pos to end is already calculated, so just return it
    if(dp[pos] != 10001) return dp[pos];    
	
    // explore all possible jump sizes from current position. Store & return min jumps required
	for(int j = 1; j <= nums[pos]; j++)
		dp[pos] = min(dp[pos], 1 + solve(nums, dp, pos + j));        
	return dp[pos];
}



/*
Approach #3: Bottom-Up DP - Iterative Tabulation

Time Complexity : O(N^2)
Space Complexity : O(N)
*/
int jump(vector<int>& nums) {
	int n = size(nums);
	vector<int> dp(n, 10001);
    
    // start from last index. No jumps required to reach end if already here
	dp[n - 1] = 0;  
	
    // same as above. For each index, explore all jump sizes and use the one requiring minimum jumps to reach end
	for(int i = n - 2; i >= 0; i--) 
		for(int jumpLen = 1; jumpLen <= nums[i]; jumpLen++)
            // min(n-1, i + jumpLen) for bounds handling
            dp[i] = min(dp[i], 1 + dp[min(n - 1, i + jumpLen)]);  
    return dp[0];
}



/*
Approach #4: Greedy BFS

Video:
- NeetCode :    https://www.youtube.com/watch?v=dJ7sWiOoK7g
- Greg Hogg:    https://www.youtube.com/watch?v=CsDI-yQuGeM => 不容易懂

Text: 
- See link on the top of the file

*/
int jump(vector<int>& nums) {
	int n = size(nums), i = 0, maxReachable = 0, lastJumpedPos = 0, jumps = 0;
	while(lastJumpedPos < n - 1) {  // loop till last jump hasn't taken us till the end
		maxReachable = max(maxReachable, i + nums[i]);  // furthest index reachable on the next level from current level
		if(i == lastJumpedPos) {			  // current level has been iterated & maxReachable position on next level has been finalised
			lastJumpedPos = maxReachable;     // so just move to that maxReachable position
			jumps++;                          // and increment the level
	// NOTE: jump^ only gets updated after we iterate all possible jumps from previous level
	//       This ensures jumps will only store minimum jump required to reach lastJumpedPos
		}            
		i++;
	}
	return jumps;
}
