'''
450. Delete Node in a BST

QN's Notes from Algorithm-Anna/Tree-BST:
/Users/as/Library/CloudStorage/Dropbox/Algorithms-Anna/Tree-BST/Ex-Python

NeetCode:
https://www.youtube.com/watch?v=LFzAoJJt92M


Let's del 9 from the tree

	      15
	    /    \
	  9      20
	 / \     / \
	5  14   19  30 
      /  \
     10   18
'''
class Solution:
    def deleteNode(self, root: Optional[TreeNode], key: int) -> Optional[TreeNode]:
        
        # If tree is empty, return null
        if not root:  return root


        # Key is not found - key is larger than root
        if key > root.val:
            root.right = self.deleteNode(root.right, key)
        # Key is not found - key is smaller than root
        elif key < root.val:
            root.left = self.deleteNode(root.left, key)

        # Key is found
        else:  
            # If root has only 1 child or no child
            if not root.left:  return root.right
            elif not root.right: return root.left

            # If both childs exist
            # 1) Find the min node from right subtree (use tree ex above)
            minNode = root.right  # Peserve root ptr to use later
            while minNode.left:  minNode = minNpde.left
            # 2) Replace the val of the root for del w/ the min node's val
            root.val = minNode.val
            # 3) Del the min node in the right subtree
            root.right = self.deleteNode(root.right, minNode.val)
        
        return root
        

