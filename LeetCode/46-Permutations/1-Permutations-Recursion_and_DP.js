/*
46. Permutations

思路 Video:
- https://www.youtube.com/watch?v=TnZHaH9i6-0&t=610s

Solution Code:
- https://leetcode.com/problems/permutations/discuss/18308/JavaScript-using-DP

*/

// Approach #1: Recursion (Not DFS)
var permute = function(nums) {
  const res = [];
  backtrack(nums, res);
  return res;
};

function backtrack(nums, res, n = 0) {
  if (n === nums.length - 1) {
    res.push(nums.slice(0));
    return;
  }
  for (let i = n; i < nums.length; i++) {
    [nums[i], nums[n]] = [nums[n], nums[i]]; // Swap 2 vars
    backtrack(nums, res, n + 1);
    [nums[i], nums[n]] = [nums[n], nums[i]]; // Swap the 2 vars back: backtrack
  }
}

// Approach #2: DP
// - Sol Code:
//  https://leetcode.com/problems/permutations/discuss/18308/JavaScript-using-DP
// - Explanation:
//  https://leetcode.wang/leetCode-46-Permutations.html
//
var permute = function(nums, n = 0) {
  if (n >= nums.length)   return [[]];

  const res = [];
  const prevs = permute(nums, n + 1);  // permutations of elements after n
  //console.log("\nprevs:", prevs);

  for (let prev of prevs) {
    //console.log("\n---->prev:", prev);
    for (let i = 0; i <= prev.length; i++) {
      //console.log("-->i:", i);

      let p = prev.slice(0);
      //console.log("p:", p);

      //console.log("nums[", n, "]:", nums[n])
      p.splice(i, 0, nums[n]);  // successively insert element n
      //console.log("p:", p);

      res.push(p);
      //console.log("res:", res);
    }
  }

  return res;
};

//---------- Testing ----------
let arr = [1,2,3];
let res = permute(arr); // [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
console.log("res:", res);
