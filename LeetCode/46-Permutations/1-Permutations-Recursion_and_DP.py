"""
46. Permutations

Explanation: (Code & Concepts are the same in .js and .py files)
- Permutations-Recursion_and_DP.js

Explanation Video: It shows a tree structure and swapping
- https://www.youtube.com/watch?v=TnZHaH9i6-0&t=610s
=> Note: Watch Time Complexity part!

Solution Code:
- https://leetcode.com/problems/permutations/discuss/18284/Backtrack-Summary%3A-General-Solution-for-10-Questions!!!!!!!!-Python-(Combination-Sum-Subsets-Permutation-Palindrome)
- Note:
-- This link has "Backtrack Summary: General Solution for 10 Questions"

"""

"""
重要 Issue:
- Solutions have diff ideas about whether "ans.append(nums[:])" below makes a
  shallow or deep copy.

(1) both append() and [:] make a shallow copy, but it's not the focus here.

(2) What ans.append(nums[:]) does behind the scenes is equivalent to:
        ans[len(ans):] = [ nums[:] ]
- See: BitBucket/tutorial-cs/res.append(nums[:])/

(3) Online resources don't explain clearly.  The only reasonable explaination I could think of is:
- After nums[start] and nums[i] swap, ans.append(nums[:]) does a shallow copy (1st-level copy) of the cur contents in nums[]. When nums[start] and nums[i] swap back, it doesn't affect what's already copied.

記憶!!!
- ans.append(nums[:]) gives the desired ans here.
    [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 2, 1], [3, 1, 2]]
- ans.append(nums) acts differently, and will result in
    [[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]
"""

class Solution(object):

  def permute(self, nums):
    def backtrack(start, end):

      # Base Case:
      if start == end:
        ans.append(nums[:])

      # Recursive Case:
      for i in range(start, end):
        nums[start], nums[i] = nums[i], nums[start] # Swap 2 vars
        backtrack(start+1, end)
        nums[start], nums[i] = nums[i], nums[start] # Swap them back: backtrack

    ans = []
    backtrack(0, len(nums))
    return ans

'''
Approach #2: DP
- Explanation & Sol Code:
    https://leetcode.com/problems/permutations/solutions/339502/python-dp-solution-beat-99-with-comments/

思路:
We can have come up with the formula

dp[i:j] =[j + dp[i:j-1]] + [dp[i:j-1] + j] + [dp[i:k] + j + dp[k:j]]

In the example [1,2,3], we want to calculate dp[0:3] (i is 0 and j is 3 here), we can do this by calculate dp[0:2] first. dp[0:2] needs us to calculate
dp[0:1] which is [1] here.
'''
import collections

class Solution:
    def permute(self, nums):
        # dp[0] = [1]
        # dp[1] = [[1, 2], [2, 1]]
		# edge case
        if not nums:
            return []
        dic = collections.defaultdict(list)
        dic[0] = [[nums[0]]]
        for i in range(1, len(nums)):
		   # Case one:  the new element append to the right of the list
            for l in dic[i-1]:
                dic[i].append([nums[i]] + l)
		    # Case two:  the new element append to the left of the list
            for l in dic[i-1]:
                dic[i].append(l + [nums[i]])
		    # Case three:  the new element somewhere between the list
            for lst in dic[i-1]:
                for x in range(1, len(lst)):
                    dic[i].append(lst[:x] + [nums[i]] + lst[x:])
        return dic[len(nums)-1]

