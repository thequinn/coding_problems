/*
46. Permutations

Explanation Video + JS Code:
1) https://www.youtube.com/watch?v=KukNnoN-SoY

*/

//Approach #1: DFS
var permute = function(nums, set=[], answer=[]) {
  if (!nums.length)   answer.push(...set);

  for (let i = 0; i < nums.length; i++) {
    const newNums = nums.filter((n, index) => index != i);

    set.push(nums[i]);
    permute(newNums, set, answers);
    set.pop();
  }

  return answers;
}

//---------- Testing ----------
const arr = [1,2,3]
let res = permute(arr)  // [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
console.log(res)
