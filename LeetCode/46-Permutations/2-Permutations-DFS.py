"""
46. Permutations

Explanation Video + JS Code:
1) https://www.youtube.com/watch?v=KukNnoN-SoY

Explanation Video + Python Code:
2) https://www.youtube.com/watch?v=oCGMwvKUQ_I&t=268s
- Note:
    It explains why we use ans.append(nums[:]), but not use ans.append(nums)
    但是 Russel Gu 的留言說沒關係, 而且有9個人按讚


Solution Code:
- https://leetcode.com/problems/permutations/discuss/172632/Python-or-DFS-%2B-BFS-tm
"""

#----------------------------
# Approach #1-1: Exactly same code as Appraoch #1-2, but diff Python structure
#
# Note:
# - This is diff from the "recursive" method in
#       Permutations-Backtrack_Template_for_10_Questions.py
class Solution:
  def permute(self, nums):
    self.res = []
    self.dfs(nums, [])
    return self.res

  def dfs(self, nums, tmp):
    if len(nums) == len(tmp):
      self.res.append(tmp[:])
      return

    for i in range(len(nums)):
      if nums[i] in tmp:
        continue

      tmp.append(nums[i])
      self.dfs(nums, tmp)
      tmp.pop()

#----------------------------
# Approach #1-2: DFS ==> A lot slower than Approach #1-1, even though they have exactly same code but one has dfs() nested in permute().
#
class Solution(object):
  def permute(self, nums):

    def dfs(nums, tmp):
      if len(tmp) == len(nums):
        self.res.append(tmp[:])  # See Note in ln-10

      for i in range(len(nums)):
        if nums[i] in tmp:  continue

        tmp.append(nums[i])
        print(*tmp)
        dfs(nums, tmp)
        tmp.pop()

    self.res = []
    dfs(nums, [])
    return self.res

#----------------------------
# Approach #2: "Diff" appproach from #1.
#              => using an extr arr, used[], to trace if an elem is used
# - 重要!!
#   - ln-90 allows ln-79 to test used[i] directly, instead of using ln-59.
#
class Solution:
  def permute(self, l: List[int]) -> List[List[int]]:

    def dfs(used, res, tmp):
      if len(tmp) == len(l):
        # [:] makes a shallow copy, otherwise we'd append same list over & over
        res.append(tmp[:])
        return

      for i, c in enumerate(l):
        if used[i]:  continue

        # add letter to permutation, mark letter as used
        tmp.append(c)
        used[i] = True

        dfs(used, res, tmp)

        # remove letter from permutation, mark letter as unused
        tmp.pop()
        used[i] = False

    res = []
    used = [False] * len(l)
    dfs(used, res, [])
    return res

#----------------------------
# Approach #3:
def permute(self, nums):
    res = []
    self.dfs(nums, [], res)
    return res

def dfs(self, nums, path, res):
    if not nums:
        res.append(path)
        # return # backtracking
    for i in xrange(len(nums)):
        self.dfs(nums[:i]+nums[i+1:], path+[nums[i]], res)

#---------- Testing ----------
arr = [1,2,3]
sol = Solution()
res = sol.permute(arr) # [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
print(*res)
