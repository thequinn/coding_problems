"""
46. Permutations

Explanation + Solution Code:
- https://leetcode.com/problems/permutations/discuss/172632/Python-or-DFS-%2B-BFS-tm

"""

# Approach #1: BFS
from collections import deque
class Solution:
  def permute(self, nums):
    q = deque()
    for num in nums:
      q.append([num])
    print("Init q:", end = '')
    print(*q)

    # This code block equals to ln-37~55m which helps to trace the code.
    #
    # min(iterable, *[key, default])
    # - https://thepythonguru.com/python-builtin-functions/min/
    # - Find the smallest item in an iterable
    # - key (optional):
    # -- key function where the iterables are passed and comparison is
    #    performed based on its return value
    # -- ex. key=len below: compare based on its length
    """
    while len(min(q, key=len)) < len(nums):
      temp = q.popleft()
      for num in nums:
        if num in temp: continue
        q.append(temp[:] + [num])
    """

    # This code block equals to ln-30~34.  It helps to trace the code.
    minList = min(q, key=len)
    print("minList:", end = '')
    print(minList)

    while len(minList) < len(nums):
      temp = q.popleft()
      print("\npopleft():", end = '')
      print(temp)

      for num in nums:
        print("num: %d" % num)
        if num in temp: continue
        q.append(temp[:] + [num])
        print("q:", end = '')
        print(*q)

      minList = min(q, key=len)
      print("minList:", end = '')
      print(minList)


    return list(q)


# Approach #2: BFS - Acting like using deque() in Python
#                  - This code is more generic when converting to JS
from collections import deque
class Solution:
  def permute(self, nums):
    q = [[num] for num in nums]

    # min(q, key=len)
    # - Output min by len of list ==> Print it out....未....???...???...???
    # - https://stackoverflow.com/questions/27486309/how-does-iter-and-key-in-python-max-min-function-work
    while len( min(q, key=len) ) < len(nums):
      temp = q.pop(0)
      for num in nums:
        if num in temp: continue
        q.append(temp[:] + [num])
    return q


#---------- Testing ----------
arr = [1,2,3]
sol = Solution()
res = sol.permute(arr) # [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
print(*res)
