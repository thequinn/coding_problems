
(1) 解法一:
- The solution in
    Permutations-Recursion.js  (same as Permutations-Recursion.py)
uses "Recursive" method.  However, its 思路 is diff from the DFS 思路.

Note:
- 這個解法是用 String Permutations 的思路 (Tracing tree 看 notes)
-- https://www.youtube.com/watch?v=TnZHaH9i6-0&t=610s


(2) 解法二:
- Permutations-DFS.py
- Permutations-BFS.py

Time & Space Complexity: ==> Video @23:30
- https://www.youtube.com/watch?v=GCm7m5671Ps
