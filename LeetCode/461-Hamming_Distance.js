/*
461. Hamming Distance

Solution:
- https://leetcode.com/problems/hamming-distance/discuss/94704/Javascript-one-line-solution
*/

var hammingDistance = function(x, y) {
   let arr = (x ^ y).toString(2)
  console.log(arr);

  let s = arr.replace(/0/g, '');
  console.log(s);

  return s.length;
};

console.log( hammingDistance(3, 1) );

