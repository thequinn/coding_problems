/*
48. Rotate Image

*/


/*
Approach #3: Rotate four rectangles in one single loop

Video Explaiation + Java Code:
- https://www.youtube.com/watch?v=gCciKhaK2v8

LC Std Sol:
- https://leetcode.com/problems/rotate-image/solution/
*/
class Solution {
  public void rotate(int[][] matrix) {
    int n = matrix.length;
    for (int i = 0; i < n / 2 + n % 2; i++) {
      for (int j = 0; j < n / 2; j++) {
        int[] tmp = new int[4];
        int row = i;
        int col = j;
        for (int k = 0; k < 4; k++) {
          tmp[k] = matrix[row][col];
          int x = row;
          row = col;
          col = n - 1 - x;
        }
        for (int k = 0; k < 4; k++) {
          matrix[row][col] = tmp[(k + 3) % 4];
          int x = row;
          row = col;
          col = n - 1 - x;
        }
      }
    }
  }
}


// Approach #2: No need => Skip


/*
Approach #1: Transpose and then reverse

Video Explaiation + Java Code:
- https://www.youtube.com/watch?v=gCciKhaK2v8

LC Std Sol:
- https://leetcode.com/problems/rotate-image/solution/
*/
class Solution {
  public void rotate(int[][] matrix) {
    int n = matrix.length;

    // transpose matrix
    for (int i = 0; i < n; i++) {
      for (int j = i; j < n; j++) {
        int tmp = matrix[j][i];
        matrix[j][i] = matrix[i][j];
        matrix[i][j] = tmp;
      }
    }
    // reverse each row
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n / 2; j++) {
        int tmp = matrix[i][j];
        matrix[i][j] = matrix[i][n - j - 1];
        matrix[i][n - j - 1] = tmp;
      }
    }
  }
}
