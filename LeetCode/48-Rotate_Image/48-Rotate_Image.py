'''
48. Rotate Image

'''

'''
Approach #3: Rotate four rectangles in one single loop

Video Explaiation + Java Code:
- https://www.youtube.com/watch?v=gCciKhaK2v8

LC Std Sol:
- https://leetcode.com/problems/rotate-image/solution/
'''
class Solution:
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        n = len(matrix[0])
        for i in range(n // 2 + n % 2):
            for j in range(n // 2):
                tmp = [0] * 4
                row, col = i, j
                # store 4 elements in tmp
                for k in range(4):
                    tmp[k] = matrix[row][col]
                    row, col = col, n - 1 - row
                # rotate 4 elements
                for k in range(4):
                    matrix[row][col] = tmp[(k - 1) % 4]
                    row, col = col, n - 1 - row

'''
Approach #2: No need => Skip
'''


'''
Approach #1: Transpose and then reverse

Video Explaiation + Java Code:
- https://www.youtube.com/watch?v=gCciKhaK2v8

LC Std Sol:
- https://leetcode.com/problems/rotate-image/solution/
'''
class Solution:
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        n = len(matrix[0])
        # transpose matrix
        for i in range(n):
            for j in range(i, n):
                matrix[j][i], matrix[i][j] = matrix[i][j], matrix[j][i]

        # reverse each row
        for i in range(n):
            matrix[i].reverse()
