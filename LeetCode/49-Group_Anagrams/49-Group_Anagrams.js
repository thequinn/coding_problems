/*
49. Group Anagrams

Anagram: 就是两个词所用的字母及其个数都是一样的，但是字母的位置不一样。
*/

// Approach #1: Hash
// - https://leetcode.com/problems/group-anagrams/discuss/388356/JavaScript
//
var groupAnagrams = function(strs) {
  const anagrams = {};

  for (const str of strs) {
    // sort() sorts elems of an array in place and returns the sorted array
    const sortedStr = str.split('').sort().join('');
    console.log(str, ", ", sortedStr);

    if (!anagrams[sortedStr]) {
      anagrams[sortedStr] = [];
    }

    anagrams[sortedStr].push(str);
  }

  // Object.values()
  // - returns an array of a given object's own enumerable property values,
  //   in the same order as that provided by a for...in loop
  return Object.values(anagrams);
};

// Approach #2: Categorize by Count
// - 思路: https://leetcode.com/problems/group-anagrams/solution/
// - Code: https://leetcode.com/problems/group-anagrams/discuss/235518/Javascript
// 
var groupAnagrams = function(strs) {
  const map = new Map();

  for(const str of strs) {
    const keys = Array(26).fill(0);

    // Count num of appearances of each letter in str
    for(const c of str){
      // 'a' has ASCII code 97.
      keys[c.charCodeAt(0) - 97] += 1;
    }
    const key = keys.join("#");
    console.log("\nkey:", key);

    let val = [];

    // If the key exist in map{}, get the existing val of the key
    if (map.has(key)) {
      // Map.get(key): returns an [] w/ the value associated to the key
      val = map.get(key)
      console.log("in if, val:", val);
    }

    // Add the currstr to val[]
    val.push(str);
    console.log("after push(), val:", val);

    // Map.set(): sets the value for the key in the Map object.
    map.set(key, val);
    console.log("map:", map);
  }

  // Array.from():
  // - creates a new, shallow-copied Array instance from an array-like or
  //   iterable object.
  return Array.from(map.values());
};

let strs = ["eat", "tea", "tan", "ate", "nat", "bat"];
let res = groupAnagrams(strs);
console.log(res);
