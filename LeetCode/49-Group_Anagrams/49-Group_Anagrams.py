'''
49. Group Anagrams
'''


'''
Sol by Quinn

Intuition:
- When we encounters a new str, check its sorted form in dct.
    - If it doesn't exist, add the sorted str as key.  As for the key's val,
      use the len of the dct. Next, append the str as a list to the ans[].
    - If it exists, get the val of the str in its sorted form from dct.
       This is the index where the s will be appended in ans[].  Append the s.
'''
class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        dct = {}
        ans = []

        for i, s in enumerate(strs):
            s2 = ''.join(sorted(s))
            if s2 not in dct:
                dct[s2] = len(dct)
                ans.append([s])
            else:
                # Will be used as index of s in its sorted form in as[]
                val = dct[s2]
                ans[val].append(s)
        return ans


'''
https://leetcode.com/problems/group-anagrams/solutions/2384037/python-easily-understood-hash-table-fast-simple/

1. Since the problem description says you can return the answer in any order, a
dct of arr's can be used.

2. When a new str shows up,
    - if its sorted form doesn't exist in the dct,
        add a new entry:
            key is sorted str, val is empty list.
    - else:
        append to existing entry:
            key is sorted str, append the new str to the entry
'''

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        tbl = {}

        for s in strs:

            # sorted() returns a list. ''.join() converts list to str.
            sorted_s = ''.join(sorted(s))
            print("\ns:", s, ", sorted_s:", sorted_s)

            if sorted_s not in tbl:
                tbl[sorted_s] = []
            tbl[sorted_s].append(s)


        # dictionary.values():
        # - returns a view object that displays a list of all the values in
        #   the dictionary.
        return list(tbl.values())


