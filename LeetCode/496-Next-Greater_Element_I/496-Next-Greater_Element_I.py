'''
496. Next Greater Element I
'''


'''
Approach #1: Bruth Force, but Good
- From QN & others
- https://leetcode.com/problems/next-greater-element-i/solutions/523044/python-straightforward-approach-easy-to-understand/


Steps:
    iter over nums1[]
        if n1 is in nums2[], iter over nums2[] from where n1's val is
        in nums2[]
            if there is a next greater number, append it to ans[]
            else, append -1 to ans[]
'''
class Solution:
    def nextGreaterElement(self, nums1: List[int], nums2: List[int]) -> List[int]:

        ans = []
        ndx = -1
        for i, n1 in enumerate(nums1):
            # Signal if cur num in nums1[] is handled for the next greater elem
            flag = False
            if n1 in nums2:
                ndx = nums2.index(n1)
                for j, n2 in enumerate(nums2[ndx+1:]):
                    if n1 < n2:
                        ans.append(n2)
                        flag = True
                        break
                if not flag:
                    ans.append(-1)
        return ans


'''
Approach #2: Monotonic Stack

Vid: https://www.youtube.com/watch?v=68a1Dc_qVq4


Intuition:

- Monotonic Stacks are valuable for analyzing sequences or arrays, especially when determining the next or previous larger or smaller element for each array element. If a problem requires sequential step-by-step comparison, using a Monotonic Stack is likely beneficial.

- A clear indication that a Monotonic Stack could be useful is when the problem involves finding the "next greater element" or "next smaller element" in an array. Problems like calculating maximum areas in histograms can also be efficiently solved with Monotonic Stacks. The key is recognizing patterns that necessitate sequential comparisons.


Explanation & Sol Code:
    (1) https://www.youtube.com/watch?v=68a1Dc_qVq4
    (2) https://labuladong.gitbook.io/algo-en/ii.-data-structure/monotonicstack


'''
