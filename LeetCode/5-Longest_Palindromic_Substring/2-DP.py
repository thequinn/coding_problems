'''
Longes Palindromic Subsring



Approach #2: DP

Explaination + Sol Code:
- https://leetcode.com/problems/longest-palindromic-substring/solutions/759291/straight-forward-short-and-clean-python-dp-with-detailed-simple-explanation/

Video Explaination => 注意!! 不要使用 Techdose 介紹的 G4G code, 爛爆!!!
                   => 看這 vid 的 concept 就好
- https://www.youtube.com/watch?v=UflHuQj6MVA



Notes:
- Matrix usage: Only half of the matrix will be used


# - - - - - - - - - - - - - - - # 
Notice!! => DON"T USE THE INFO BELOW! 爛爆!!! 
- Sol Code from G4G
    https://www.geeksforgeeks.org/longes-palindromic-subsring/

'''

class Solution:
    def longestPalindrome(self, s):
        n = len(s)
        
        if n < 2: return s
        
        dp, ans = [[0]*n for _ in range(n)], {}
        
        for i in range(n-1, -1, -1):
            for j in range(i, n):
                if s[i] == s[j] and ((j - i + 1) <= 3 or dp[i + 1][j - 1]):
                    dp[i][j] = True
                    ans[j-i+1] = s[i:j+1]
                else:
                    dp[i][j] = False
       
        print("ans:", ans)
        return ans[max(ans)]



sol = Solution()

s1 = "Hello"
l = sol.longestPalSubstr(s1)
print("Length is:", l)

s2 = "acacb"
l = sol.longestPalSubstr(s2)
print("Length is:", l)



