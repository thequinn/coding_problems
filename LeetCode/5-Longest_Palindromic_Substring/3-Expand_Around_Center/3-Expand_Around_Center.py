'''
Longest Palindromic Substring

'''


'''
Approach #3: Expand Around Center

'''

class Solution:
    def longestPalindrome(self, s):
        longest = ""
    
        for i in range(len(s)):
            print("\ni:", i)

            # Check odd-length palindromes
            print("odd_palindrome = self.expandAroundCenter(s, i, i)")
            odd_palindrome = self.expandAroundCenter(s, i, i)
            print("odd_palindrome:", odd_palindrome)

            if len(odd_palindrome) > len(longest):
             longest = odd_palindrome
        
            # Check even-length palindromes
            print("even_palindrome = self.expandAroundCenter(s, i, i+1)")
            even_palindrome = self.expandAroundCenter(s, i, i+1)
            print("even_palindrome:", even_palindrome)

            if len(even_palindrome) > len(longest):
                longest = even_palindrome
    
        return len(longest)

    def expandAroundCenter(self, s, left, right):
        print("ln-32, s:", s, ", left:", left, ", right:", right)

        while left >= 0 and right < len(s) and s[left] == s[right]:
            print("ln-35, s[", left, "]:", s[left], ", s[", right, "]:", s[right])
            left -= 1
            right += 1
        
        return s[left+1:right]  # Return cur palindrome or None



# Example usage
sol = Solution() 
s1 = "babad"    # "bab"
s2 = "abccdd"   # "cc"
print("Longest palindrome:", sol.longestPalindrome(s1))
#print("Longest palindrome:", sol.longestPalindrome(s2))

