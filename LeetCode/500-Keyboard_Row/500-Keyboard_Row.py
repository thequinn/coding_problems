'''
500. Keyboard Row

'''

'''
Approach #1: QN's Bruth Force

思路:
- Put the 3 keyboard rows as an array of str
- Create a func, checkWords(), that checks if a word has all its letters in \     a row
- Iterate over the 3 rows while calling the checkWords()
 ||
 ||===> Note "HowTo Make My Life Easier ???"
        - If not create a seperate func, checkWords(), it's harder for brain 
          to think because not it has 3 layers of loop.  

'''
class Solution:
    def findWords(self, words: List[str]) -> List[str]:

        rows = ["qwertyuiop", "asdfghjkl", "zxcvbnm"]

        def checkWords(row):
            for word in words:
                validWord = 1
                for ch in set(word.lower()):
                    if ch not in row:
                        validWord = 0
                        break
                if validWord == 1:
                    res.append(word)
        res = []
        for row in rows:
            checkWords(row)
        return res


'''
Approach #2

https://leetcode.com/problems/keyboard-row/solutions/97913/easy-understand-solution/

思路:
- Make every line a set of letter.
- Then I check every word if this word set is the subset if any line set.

Note:
- Check this link to learn the techniques to use set.
- https://stackoverflow.com/questions/20726010/how-to-check-if-a-string-contains-only-characters-from-a-given-set-in-python

'''
def findWords(self, words):
    line1, line2, line3 = set('qwertyuiop'), set('asdfghjkl'), set('zxcvbnm')
    ret = []
    for word in words:
      w = set(word.lower())
      if w <= line1 or w <= line2 or w <= line3:
        ret.append(word)
    return ret
