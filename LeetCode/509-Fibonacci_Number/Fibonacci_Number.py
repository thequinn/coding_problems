"""
509. Fibonacci Number

LC Std Sol:
- https://leetcode.com/problems/fibonacci-number/solution/

"""

#========================================================
# Method #1: Brute Force => Recursion w/o DP
'''
Time Complexity: O(2^N), Exponential time
- The amount of operations needed, for each level of recursion, grows
  exponentially as the "depth" approaches N.
- 非常重要: 要了解這個 Recursion Tree
                              (5)
                              / \
                   (4)                   (3)
                   / \                   / \
            (3)           (2)      (2)         (1)
            / \           / \      / \
         (2)   (1)     (1)   (0) (1)  (0)
         / \
       (1)(0)

Space Complexity: O(N)
- We need space proportionate to N to account for the max size of the stack, in
  memory.
- This has the potential to be bad in cases that there isn't enough physical
  memory to handle increasingly growing stack, leading to a StackOverflowError.
'''
class Solution:
  def fib(self, N: int) -> int:
    if N <= 1: return N
    return self.fib(N-1) + self.fib(N-2)

#========================================================
# Method #3-1: Top-Down w/ Memoization => 這個方法和 70. Climb Stairs 同, 但要
#                                         fill 0's 到 memo[] 較麻煩
# - https://leetcode.com/problems/fibonacci-number/discuss/253291/clean-python-solution-in-both-recursive-and-dp-bottom-up-approach
#
class Solution:

  def fib(self, N: int) -> int:
    # Create a list w/ 0's filled.  W/o it, "if memo[N]: " returns error!
    memo = [0] * (N+1)

    return self.helper(N, memo)

  def helper(self, N, memo):
    # 1. 這行不可省略, 因為 memo[0] = 0, which returns false.
    # 2. 移到 " if memo[N]: return memo[N]" 之後也可以
    #if N <= 1: return N
    #
    if memo[N]: return memo[N]
    #
    # 這行不可省略, 因為 memo[0] = 0, which returns false
    if N <= 1: return N

    memo[N] = self.helper(N-1, memo) + self.helper(N-2, memo)
    return memo[N]

#-----------------------------
# Method #3-2: Top-Down w/ Memoization => From: LC Std Sol, 方法比較好!
'''
Time Complexity: O(N)
- Each number, starting at 2 up to and including N, is visited, computed and then stored for O(1)O(1) access later on.

Space Complexity: O(N)
- The size of the stack in memory is proportionate to N.
'''
class Solution:
  def fib(self, N: int) -> int:
    # Pre-fill obj w/ 0's, 因此可省略 “if N <= 1: return N” in helper() below
    self.memo = {0: 0, 1: 1}

    return self.helper(N)

  def helper(self, N: int) -> {}:
    #if N <= 1: return N

    # - dict.keys() returns a Dictionary view object.  It can be iterated over.
    # - Func's supported by Dictionary view objects:
    #     ex. x in dictview: return True if x is in the Dict view obj
    if N in self.memo.keys():
      return self.memo[N]

    self.memo[N] = self.helper(N-1) + self.helper(N-2)
    return self.memo[N]

#========================================================
# Method #2-1: Bottom-Up w/ DP => 1. LC Std Sol 名字寫錯
#                                 2. 這個方法是依據 70. Climb Stairs, 2-1.js
'''
Time Complexity: O(N)
- Each number, starting at 2 up to and including N, is visited, computed and
  then stored for O(1)O(1) access later on.

Space Complexity: O(N)
- The size of the data structure is proportionate to N.
'''
class Solution:
  def fib(self, N: int) -> int:
    # 因為 “ self.dp = {0:0, 1:1}”, 所以這行可省略
    #if N <= 1: return N

    self.dp = {0:0, 1:1}

    for i in range(2, N+1):
      self.dp[i] = self.dp[i-1] + self.dp[i-2]
    return self.dp[N]

#========================================================
# Method #4: Bottom-Up w/ DP + Optimization => LC Std Sol 名字寫錯
'''
Time Complexity: O()

Space Complexity: O()
'''
class Solution:
  def fib(self, N) -> int:
    # 這行不能少 => Compare to Method #2-1
    if (N <= 1) return N

    a, b = 0, 1
    c = 0

    # range() is exclusive and we want to include N, we need to put N+1.
    for i in range(2, N+1):
      c = a+b
      a = b
      b = c
    return c

