// Approach #??: Recursion
// - https://www.geeksforgeeks.org/longest-palindromic-subsequence-dp-12/
// - 思路: See the link (Downloaded as attachment)
//
var longestPalindrome = function(s) {
  return helper(s.split(''), 0, s.length - 1);
};

var helper = function(seq, i, j) {
 // Base Case:
  // Base Case 1: If there is only 1 character
  if (i == j)   return 1;
  // Base Case 2: If there are only 2 characters and both are same
  if (seq[i] == seq[j] && i + 1 == j)   return 2;

 // Recursive Case:
  // If the first and last characters match
  if (seq[i] == seq[j]) {
    var num = helper(seq, i+1, j-1) + 2;
    //console.log("num:", num, ", i:", i, ", j:", j);
    return num;
  }
  // If the first and last characters do not match
  return Math.max( helper(seq, i, j-1), helper(seq, i+1, j) );
}


//let s = "babab"; // "bab" or "aba"
//let s = "cbbd"; // "bb"
let s = "GEEKSFORGEEKS";

let res = longestPalindrome(s);
console.log(res);
