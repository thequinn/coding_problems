
/*
523. Continuous Subarray Sum

LeetCode Premium Sol:
- https://leetcode.com/problems/continuous-subarray-sum/solution/

*/

// Approach #1: Brute force => Too slow, skipped

// Approach #2: Better brute force => Slow, but basis to know it!
// - Time complexity : O(n^2)
// - Space complexity: O(1)
//
/*const checkSubarraySum = (nums, k) => {

  for (let i = 0; i < nums.length; i++) {
    let sum = nums[i];  // Don't forget to reset sum!

    for (let j = i+1; j < nums.length; j++) {
      sum += nums[j];

      // if (sum == k): ex. nums=[0, 0], key=0
      if (sum == k ||sum % k == 0) {
          return true;
      }
    }
  }
  return false;
}*/

/*
Approach #3: Sliding Window w/ HashMap  ==> 這題只是要背數學證明, 其實很簡單!

- Video 解釋: https://www.youtube.com/watch?v=3kD0nuwyPj8 --> 核心 @4:48
  |
  --> This approach is from LeetCode Premium Sol, but QN prefers 以上解釋


- What the question wants in 1 sentence:
A: Find i and j such that sum(arr[i:j)) % k = 0;


- 思路:
Part-1:
  If we do a linear scan thr input nums[], we can do an accumulative sum.  With that, we can efficiently calc sub-arr sum of any starting & ending indices.

  acc[i] = sum(arr[0:i))  // acc[i] is the sum or accumulation b/t indices 0~i
  sum(arr[i:j)) = acc[j] - acc[i]

Note: ==> (不太清楚這思路以上作者為何這樣用, 但這裡解釋這數學符號)
  In Math, [x:y) means x inclusive, y exclusive.  So it means x ~ y- 1

* * * * * * * * * * * * ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Part-2:
  How to determine the substraction of 2 nums, acc[j] - acc[i], to be multiple of k?

s1.
因為:
  acc[j] = nj = a*k+b,
  acc[i] = ni = c*k+d, where 0 < b, d < k

所以:
  sum(arr[i:j)) = acc[j] - acc[i] = (a-c)*k + (b-d)

s2.
因為: ------>>> 核心!!!
  sum(arr[i:j)) is a multiple of k, only if b-d=k, meaning when b-d=0
                                                   ==> b=d (same remainders)
所以:
  When two nums have the same remainders, they are both multiples of k

s3.
總結: ------>>> 核心!!!
  Therefore, if nums[i]%k == nums[j]%k, we found “multiple of k” b/t i,j
                                                 ^-^-^-^-^-^-^-^-^-^-^-^
* * * * * * * * * * * * ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

In code:
  What we should really store in map{}:  key: remainder, val: index of cur num
  So sum(arr[i:j)) is a multiple of k if nums[i]%k == nums[j]%k

補充 @ ln-108 code:
  Assume that the given sum at the i-th index equals to rem. Now, if any subarray follows the i-th element, which has a sum equal to the integer multiple of k, say extending upto the j-th index, the sum up to the j-th index will be:

  (rem + n*k), where n is some integer > 0

We can observe that (rem + n*k) %k = rem %k, which is the same value as stored corresponding to the i-th index.

//==============================================

- Time complexity : O(n).
-- Only one traversal of the array nums is done.

- Space complexity: O(min(n,k)).
-- The HashMap can contain upto min(n,k) different pairings.
*/
const checkSubarraySum = (nums, k) => {
  let r = 0; // remainder of mod

  // Edge Case:
  // - In the case of nums = [1, 5] k = 6, at index=1, (1+5)%k = 0. So the
  //   remainder= 0, and its key=1.  Since it must be comply with
  //   "if (i-map[sum] > 1)" below to return 2.
  let map = {0: -1}; // key: remainder, val: index of nums

  for (let i = 0; i < nums.length; i++) {
    r += nums[i]; // 解釋: See 補充 ln-79

    if (k != 0)   r %= k;

    // If the remainder exists as the key
    //
    // 注意: nums=[5,0,0]: map[5]=0, eval'ed to false in if statement
    //if (map[r]) { // WRONG
    //
    //if (map[r] != undefined) {  // 這個方法比較不準確
    if (map.hasOwnProperty(r)) {
      // Check if the sliding window has the size of at least 2
      if (i - map[r] > 1)   return true;
    }
    else {
      // Store curr num's remainder as key, index as val
      map[r] = i;
    }
  }

  return false;
}

//let nums = [23,2,4,6,7], key = 6;
let nums = [5,0,0], key = 0;
console.log(checkSubarraySum(nums, key));
