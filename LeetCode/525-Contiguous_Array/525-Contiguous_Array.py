'''
525. Contiguous Array

Goal:
- return the max len of a sub-arr w/ an equal num of 0 and 1.

Cruicial Concept Needed: => See Video Explanation next
- Prefix Sum

# - - - - - - - -  - - - - - # - - - - - - -  - - - - - - #

- Video Explanation: => 講得很好!
    https://www.youtube.com/watch?v=ZIdNMCJ6MNc

- Text Explanation:
    https://leetcode.com/problems/contiguous-array/solutions/529791/mind-course-the-way-you-think-and-solve-it-from-o-n2-to-o-n/


# - - - - - - - -  - - - - - # - - - - - - -  - - - - - - #

思路:

(1) Use var sum to track the sum up to the cur elem in nums[]
    - When we encounter a 1, increment sum by 1
    - When we encounter a 0, decrement sum by 1
(2) Set a prefixSum{} to track the 1st appearance of a var sum as a key. A 
    key's value is the index of the cur elem in nums[].
    - If sum NOT exist as a key in prefixSum{}, add it
    - If sum YES exist, No need to update it in prefixSum{}.  Update cur_maxLen and maxLen.

(1) We want even num of 0's and 1's, so
    - when we encounter a 1, increment sum by 1
    - when we encounter a 0, decrement sum by 1

(2) Use a dict to track prefix sums.  It only needs to track the 1st appearance of a new prefix sum and it's corresponding index.  The key is the new prefix sum, and it's val is the corresponding index.  
    - If sum NOT exist as a key in prefixSum{}, add it
    - If sum YES exist, No need to update its index in prefixSum{}.  Update cur len and max len.

(3) The init prop of prefix sum dict is set to {0: -1}.  See comment in code
'''

class Solution:
    def findMaxLength(self, nums: List[int]) -> int:
        max_len = 0
        cur_sum = 0
        prefixSum = {0: -1}

        # key: cur sum that'll be used as key in dict prefix sum 
        # val: the corresponding index of a prefix sum when it "1st" appears
        #
        # - Set init key-val pair to {0: -1} b/c we only need to update 
        #   maxLen when we find a pair of 0 and 1 consecutively. 
        # - Trace code w/ ex. nums = [0,1,0,1].  
        #   - If we set prefixSum = {}, When we run the code, 
        #     prefixSum = {0: 1, -1: 0}, final result of maxLen returns
        #     2, but the correct one is 4.  
        for ndx, elem in enumerate(nums):
            if elem == 0:   cur_sum -= 1
            else:           cur_sum += 1

            if cur_sum not in prefixSum.keys():
                prefixSum[cur_sum] = ndx
            else:
                cur_len = ndx - prefixSum[cur_sum]
                max_len = max(max_len, cur_len)
        return max_len
