/*
53. Maximum Subarray


Solutions:

- Cubic to Kadane's Algorithm (DP)  ==>  GREAT!!!
-- Approach #1~#3 here
-- https://www.youtube.com/watch?v=2MmGzdiKR9Y
--- Kadane's Algorithm @ 7:30

- Easy Python Solution  ==>  EASIEST!
-- Approach #4 here
-- https://leetcode.com/problems/maximum-subarray/discuss/20396/Easy-Python-Way

- Divide and Conquer ==> The code is too long, Skip!!
-- https://leetcode.com/problems/maximum-subarray/discuss/254481/JavaScript-3-solutions(Aggregate-Kadane's-Algorithm-Divide-and-Conquer)

*/

// Approach #1: Brute Force
// - Analysis: Quadratic O(n^2)
var maxSubArray = function(nums) {
  let maxSum = Number.NEGATIVE_INFINITY;

  for (let left = 0; left < nums.length; left++) {
    let windowSum = 0;

    for (let right = left; right < nums.length; right++) {
      windowSum += nums[right];

      maxSum = Math.max(maxSum, windowSum);
    }
  }

  return maxSum;
};


/*
Approach #2: DP (Kadane's Algorithm, Greedy Algorithm)

Greedy Algorithm
- Pick the locally optimal move at each step, and that will lead to the globally optimal solution.
(簡短: Each local optimal move leads to global optimal solution.)

DP is NOT about memorizing what the table (ex. nums[]) is about.  It's about knowing the "Sub-Problems"!
- Understand the prev sub-prob helps determine best answer to our curr sub-prob

ex. Suppose we have nums[], if we end at index=2, what's my best contiguous
    subarray sum?

      nums = [1, -3, 4, -1, 2];

    Here are the combos:
      nums = [       4]  ==> 這兒
      nums = [   -3, 4]
      nums = [1, -3, 4]


* * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * * * * * *

Video: https://www.youtube.com/watch?v=2MmGzdiKR9Y
- @15:53 全題核心!!!
-- "Do I
    (1) start a new sub-arr: (from the cur index)
    or
    (2) perform an extension: (extend the prev opt sub-arr + cur index) ?

* * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * * * * * *


思路:
- Every curSum[i] signifies that "ending at index i, the max sum we can get in a contiguous sub-array"
- 注意!!  See break-down steps in ex here and s1, s2 below

 ex. index   :   0, 1, 2, 3, 4, 5, 6, 7, 8
     nums[]  : [-2, 1,-3, 4,-1, 2, 1,-5, 4]

     curSum[]: [-2, 1,-2, 4, 3, 5, 6, 1, 5]
     maxSum[]: [-2, 1, 1, 4, 4, 5, 6, 6, 6]

- Time Complexity: Linear O(N) since it's one pass along the array.
- Space Complexity: Linear O(1) since it's a constant space solution.
*/
var maxSubArray = function(nums) {
  let maxEndingHere = nums[0];
  let maxSum = nums[0];

  for (let i = 1; i < nums.length; i++) {
    // s1. Get "local" best res
    //     - Greedy Alg always "pick the locally optimal move“ at each step
    maxEndingHere = Math.max(maxEndingHere + nums[i], nums[i]);
    // s2. Get "global" best res
    //     - Compare the "local optimal sum" to the "global sum"
    maxSum = Math.max(maxSum, maxEndingHere);
  }
  return maxSum;
}


/*
Appraoch 3: Kadane's Algorithm => Same as Approach #2 (See the code)
- https://leetcode.com/problems/maximum-subarray/discuss/20396/Easy-Python-Wayclass Solution:

思路:
- If the sum of a subarray is positive, it's possible to make the next value
  bigger, so we keep doing it until it turns to negative.
- If the sum is negative, it has no use to the next element, so we break.
- It is a game of sum, not the elements.

*/
class Solution:
  def maxSubArray(self, nums: List[int]) -> int:
    for i in range(1, len(nums)):
        if nums[i-1] > 0:
            nums[i] += nums[i-1]
    return max(nums)

//-----Test Case-----
//let nums = [-1];  // Corner Case: -1
let nums = [-2,1,-3,4,-1,2,1,-5,4];  // [-2,1,-2,4,3,5,6,1,5]
console.log(maxSubArray(nums));


