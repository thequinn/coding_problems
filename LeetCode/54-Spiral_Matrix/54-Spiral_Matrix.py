'''
54. Spiral Matrix


'''



'''
法2-2: => QN watched the vid from 法2-1 first.  But want to learn 法2-2 since
          it's more intuitive. 

NeetCode's Video:
    https://www.youtube.com/watch?v=BJnMZNwUk1M


The next 2 links are the code below...maybe watch tthe video after watching NeetCode's.

Code below:
- https://leetcode.com/problems/spiral-matrix/solutions/3502600/python-java-c-simple-solution-easy-to-understand/

Vid Explanation:
- https://www.youtube.com/watch?v=aqVW8IuXUF0
'''
class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        if not matrix:
            return []

        rows, cols = len(matrix), len(matrix[0])
        top, bottom, left, right = 0, rows-1, 0, cols-1
        result = []
        
        while len(result) < rows * cols:
            for i in range(left, right+1):
                result.append(matrix[top][i])
            top += 1
            
            for i in range(top, bottom+1):
                result.append(matrix[i][right])
            right -= 1
            
            if top <= bottom:
                for i in range(right, left-1, -1):
                    result.append(matrix[bottom][i])
                bottom -= 1
            
            if left <= right:
                for i in range(bottom, top-1, -1):
                    result.append(matrix[i][left])
                left += 1
        
        return result


'''
法2-1:
- https://www.youtube.com/watch?v=fcn8qkRcFVM
- this method is more like what a person would think of in an interview
'''
class Solution:
    def spiralOrder(sel, matrix: List[List[int]]) -> List[int]:
        m, n = len(matric), len(matrix[0])
        ans = []
        i, j = 0, 0

        UP, RIGHT, DOWN, LEFT = 0, 1, 2, 3
        direction = RIGHT
        
        # Boundaries to signal end of a direction
        UP_WALL, RIGHT_WALL, DOWN_WALL, LEFT_WALL = 0, n, m, -1

        while len(ans) != m*n:
            print("\nUP_WALL:", UP_WALL, ", RIGHT_WALL:", RIGHT_WALL, ", DOWN_WALL:", DOWN_WALL, ", LEFT_WALL:", LEFT_WALL)

            if direction == RIGHT:
                while j < RIGHT_WALL:
                    ans.append(matrix[i][j])
                    j += 1
print("RIGHT, ans:", ans)
                # Set (i,j) to the starting location of the downwards direction.
                # In ex.1, it's 6 at (1, 2).
                i, j = i+1, j-1
                # Shrink RIGHT_WALL bc "if direction == RIGHT" might be 
                # revisited. In ex.1, it's 4 -> 5.
                RIGHT_WALL -= 1
                print("i:", i, ", j:", j, ", RIGHT_WALL:", RIGHT_WALL)                          direction = DOWN

            elif direction == DOWN:
                while i < DOWN_WALL:
                    ans.append(matrix[i][j])
                    i += 1
                print("DOWN, ans:", ans)
                # Set (i, j) to the starting location of the left direction.
                i, j = i-1, j-1
                # Shrink DOWN_WALL bc "elif direction == DOWN" might be 
                # revisited based on size of matrix.
                DOWN_WALL -= 1
                print("i:", i, ", j:", j, ", DOWN_WALL:", DOWN_WALL)                            direction = LEFT

            elif direction == LEFT:
                while j > LEFT_WALL:
                    ans.append(matrix[i][j])
                    j -= 1
                i, j = i-1, j+1
                LEFT_WALL += 1
                direction = UP

            else:
                while i > UP_WALL:
                    ans.append(matrix[i][j])
                    i -= 1
                i, j = i+1, j+1
                UP_WALL += 1
                direction = RIGHT

        return ans




'''
法2: => QN doesn't understand the syntax of the code bc need to be better at 
        Python first.
- https://leetcode.com/problems/spiral-matrix/solutions/20571/1-liner-in-python-ruby/?envType=study-plan-v2&envId=top-interview-150

Note:
- Compare Python 2 vs Python 3 version below to understand unpacking operator. 
'''
# Python 2 Recursive
class Solution:
    def spiralOrder(self, matrix):
        return matrix and list(matrix.pop(0)) + \
                self.spiralOrder(zip(*matrix)[::-1])  

# Python 3 Recursive
class Solution:
    def spiralOrder(self, matrix):
        return matrix and [*matrix.pop(0)] + \
                self.spiralOrder([*zip(*matrix)][::-1]) 

# Python 2 Iterative (From comments of the sol)
class Solution(object):
    def spiralOrder(self, matrix):
        res = []
        while matrix:
            res.extend(matrix.pop(0))
            matrix[:] = zip(*matrix)[::-1]
        return res

# Python 3 Iterative (From comments of the sol)
class Solution:
    def spiralOrder(self, matrix):
        res = []
        while matrix:
            res.extend(matrix.pop(0))
            matrix = [*zip(*matrix)][::-1]
        return res

