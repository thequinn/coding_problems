/*
543. Diameter of Binary Tree

思路:
- The path is the depth of the left subtree plus the depth of the right
  subtree, so that is all you have to know.

*/

// Approach #1: Intuitive Brute Force
//
// 思路:
// - https://www.youtube.com/watch?v=Hvyqh606zsI    or
// - /dwhelper/LeetCode/543 - Diameter of Binary Tree - thecodingworld.mp4

// Sol Code:
// - https://leetcode.com/problems/diameter-of-binary-tree/discuss/101120/Java-easy-to-understand-solution
//
/*
注意! ==> Can use as brute force approach, but not good for interview.
- This approach is intuitive but it's a recursion w/in recursion,  ex. LeetCode 437. Path Sum III, Approach #3 in 2-437-Path_Sum_III-DFS_plus_DFS.js
  Bad performance b/c of the overlapping subproblems when calculate depth.

DiameterOfBinaryTree is called on every node. In each call, it traverses all descendants of that node to get the depth.

for root node, it is n => n + 1 - 2^0
for all nodes on 2nd level, it is 2 * (n - 1) / 2 => n - 1 => n + 1 - 2^1
for all nodes on 3rd level, it is 4 * (n - 3) / 4 => n - 3 => n + 1 - 2^2
for all nodes on 4th level, it is 8 * (n - 7) / 8 => n - 7 => n + 1 - 2^3
...
for all nodes on last level, it is n + 1 - 2^(h-1). h is max tree depth.
Add them up, the result is (n+1) * h - (1 + 2 + 4 ... + 2^(h-1)). In worst case, the latter part is n (all nodes in the tree), so time complexity is O(n*logn) when tree is balanced, and O(n^2) when tree is unbalanced.

*/
//.....未.....未.....這是Java Code .....未.....要轉換成 JS.....未.....
//
public class Solution {
  public int diameterOfBinaryTree(TreeNode root) {
    if(root == null)    return 0;

    int dia = depth(root.left) + depth(root.right);
    int ldia = diameterOfBinaryTree(root.left);
    int rdia = diameterOfBinaryTree(root.right);
    return Math.max(dia,Math.max(ldia,rdia));

  }
  public int depth(TreeNode root){
    if(root == null)    return 0;

    return 1 + Math.max(depth(root.left), depth(root.right));
  }
}


// Approach #2: Better Approach
//
// 思路 & Sol Code:
// - https://www.youtube.com/watch?v=0VnOfu2pYTo      or
// - LeetCode 543 - Diameter of Binary Tree - 公瑾讲解.mp4
//
// Time Complexity: O(n) b/c we traverse each node of the tree once
// Space Complexity: O(n) b/c the recursion has its own stack of O(n)
//
function diameterOfBinaryTree(root) {
  let max = 0;  // Max diameter

  function maxDepth(root) {
    // If our root(num) is null then there is no path. return 0/null
    if (root === null)  return 0;

    // 重要!!
    // - Don't forget to assign the recursive calls to vars left and right!!
    let left = maxDepth(root.left);   // get left depth
    let right = maxDepth(root.right); // get right depth

    // Store and update var max? -->必看: Watch Video @6:56
    // - 1st arg, max:
    // -- This is the last recursive call's max.  The max is the max val by
    //    comparing the curr root node's left diameter and right diameter.
    max = Math.max(max, left + right);

    // Get the max depth from the curr root node: -->必看: Watch Video @5:30
    // - Think from the curr node's left and right children,
    //   1. Compare left and right depth, and get the max of it.
    //   2. "+1" b/c we need to count the edge from the left/right child to 
    //      its parent, which is the curr node.
    return Math.max(left, right) + 1;
  }


  // Since we don't know if the path will go through the root or not we will
  // have to get the max between:
  //    1. path that visits the root, or
  //    2. the path that doesn't go through the root.)
  maxDepth(root);

  return max;
};
