'''
543. Diameter of Binary Tree

Explanation + Sol Code
(1) NeetCode: -> Confusing! Don't use!
    https://www.youtube.com/watch?v=bkxqA8Rfv04
(2)
    https://leetcode.com/problems/diameter-of-binary-tree/solutions/1515564/python-easy-to-understand-solution-w-explanation/


Diameter of Tree
- Maximum depth of the left side of the tree, plus the maximum depth of the right side of the tree. 

    diameter = depth(root.left) + depth(root.right)
'''
def depth(node: Optional[TreeNode]) -> int:
	return 1 + max(depth(node.left), depth(node.right)) if node else 0 

# Same code as last 2 ln's
def depth(node: Optional[TreeNode]) -> int:
	if not node:    return 0  
	
    left = depth(node.left) 
	right = depth(node.right)  
    
    # take the maximum, and +1 for the current node
	return 1 + (left if left > right else right) 



'''
Approach #1 => Incorrect, it helps to understand why Approach #2 works

To find the diameter of a Binary Tree, we need to find the maximum depth of a given tree. For any given tree, we can find the maximum depth of the left side of the tree, the maximum depth of the right side of the tree, and +1 to the maximum of the two depths.

'''
class Solution:
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:
	    # Implement depth
	    def depth(node: Optional[TreeNode]) -> int:
		    return 1 + max(depth(node.left), depth(node.right)) if node else 0
		return depth(root.left) + depth(root.right)  



'''
Approach #2
- See the link on the top for explaination
'''
class Solution:
    def __init__(self):
	    self.diameter = 0  # stores the maximum diameter calculated
	
    def depth(self, node: Optional[TreeNode]) -> int:
        """
        This function needs to do the following:
        1. Calc the max depth of the left & right sides of the given node.
        2. Determine the diameter at the given node, check if its the max.
        """
        # Calculate maximum depth
        left = self.depth(node.left) if node.left else 0
        right = self.depth(node.right) if node.right else 0

        # Calculate diameter
        if left + right > self.diameter:
            self.diameter = left + right

        # Make sure the parent node(s) get the correct depth from this node
        return 1 + (left if left > right else right)
    
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        # if not root:    return 0
        self.depth(root)  # root is guaranteed to be a TreeNode object
        return self.diameter
