public class Jump_Game {

  public static void main(String args[]) {
    int[] A1 = {2,3,1,1,4};  // T
    int[] A2 = {3,2,1,0,4};  // F

    boolean res;

    /*Solution_1 sol_1 = new Solution_1();
    res = sol_1.canJump(A1);
    System.out.println("\nres:" + res + "\n");
    res = sol_1.canJump(A2);
    System.out.println("\nres:" + res);
    */

    /*Solution_2 sol_2 = new Solution_2();
    res = sol_2.canJump(A1);
    System.out.println("\nres:" + res + "\n");
    res = sol_2.canJump(A2);
    System.out.println("\nres:" + res);
    */

    /*Solution_3 sol_3 = new Solution_3();
    res = sol_3.canJump(A1);
    System.out.println("\nres:" + res + "\n");
    res = sol_3.canJump(A2);
    System.out.println("\nres:" + res);
    */

    Solution_4 sol_4 = new Solution_4();
    res = sol_4.canJump(A1);
    System.out.println("\nres:" + res + "\n");
    res = sol_4.canJump(A2);
    System.out.println("\nres:" + res);
  }
}

