/*
55. Jump Game

LeetCode Std Solution:
- https://leetcode.com/problems/jump-game/


非常重要!!
- This is a dynamic programming question. Usually, solving and fully understanding a dynamic programming problem is a 4 step process:

    1. Start with the recursive backtracking solution.
       --> See Approach #1

    2. Optimize by using a memoization table (top-down[2] dynamic programming)
       --> See Approach #2

    3. Remove the need for recursion (bottom-up dynamic programming)
       --> See Approach #3

    4. Apply final tricks to reduce the time / memory complexity
       --> See Approach #4

  All solutions presented below produce the correct result, but they differ in run time and memory requirements.


非常重要!!
- We call a position in the array a "good index" if starting at that position, we can reach the last index. Otherwise, that index is called a "bad index".
- The problem then reduces to whether or not index 0 is a "good index".

*/

// Approach #1: Backtracking
/*
思路:
- We start from the first position and jump to every index that is reachable.
  We repeat the process until last index is reached. When stuck, backtrack.

觀察: ==> Backtracking Algorithm (Approach #1)
- This is the inefficient solution where we try every single jump pattern that
  takes us from the first position to the last.

//---------------------------------------------

Time complexity : O(2^n)
- There are 2^n (upper bound) ways of jumping from the first pos to
    the last, where n is the length of array nums.

- Proof: ==> LeetCode Std Sol's Proof is too complicated
  A simpler proof for the O(2ⁿ) complexity of the first approach is as follows. The worst possible case is when all the integers are larger than n-1, i.e. you can get to the end from any index of the array. It also means that any possible path (series of jumps) you try will be valid.
  There are 2^n-2 such possible series of jumps: we always take the first index, then take or not take the 2nd index (2 possibilities), and so on... until finally we always take the last index. The first approach tries out all these possible paths, hence O(2ⁿ) complexity in the worst case.


Space complexity : O(n)
- Recursion requires additional memory for the stack frames.

*/
public class Solution_1 {
  public boolean canJumpFromPos(int pos, int[] nums) {
    System.out.println("\n------>pos:" + pos);

    if (pos == nums.length - 1) {
      return true;
    }

    // Get the smaller jump len so we don't get a len pass nums[]'s last index
    int furthestJump = Math.min(pos + nums[pos], nums.length - 1);
    System.out.println("furthestJump:" + furthestJump);

    for (int nextPos = pos + 1; nextPos <= furthestJump; nextPos++) {
      System.out.println("nextPos:" + nextPos);

      if (canJumpFromPos(nextPos, nums)) {
        return true;
      }
    }
    return false;
  }

  public boolean canJump(int[] nums) {
    return canJumpFromPos(0, nums);
  }
}

/*
Optimzation:
- Check the nextPos from right to left. Intuitively, this means we always
   try to make the biggest jump such that we reach the end ASAP.
- The theoretical worst case performance is the same,
   but in practice, for silly examples, the code might run faster.

- The change required is:

  Old:
    for (int nextPos = pos + 1; nextPos <= furthestJump; nextPos++)

  New:
    for (int nextPos = furthestJump; nextPos > pos; nextPos--)

ex1:
  Starting from i=0, take max len and reach i=1, take max len and reach i=6. By doing so, we determine that 0 is a GOOD index in 3 steps.

        index : 0,1,2,3,4,5,6
        nums[]: 1,5,2,1,0,2,0

ex2:
  It illustrates the worst case, where this optimization has no effect. i=6 cannot be reached from any pos, but all combinations will be tried.
  The first few steps of the backtracking algorithm for ex2 are: i: 0 -> 4 -> 5 -> 4 -> 0 -> 3 -> 5 -> 3 -> 4 -> 5 -> etc.

        index : 0,1,2,3,4,5,6
        nums[]: 1,5,2,1,0,2,0
*/


