/*
55. Jump Game

LeetCode Std Solution:
- https://leetcode.com/problems/jump-game/


非常重要!!
- This is a dynamic programming question. Usually, solving and fully understanding a dynamic programming problem is a 4 step process:

    1. Start with the recursive backtracking solution.
       --> See Approach #1

    2. Optimize by using a memoization table (top-down[2] dynamic programming)
       --> See Approach #2

    3. Remove the need for recursion (bottom-up dynamic programming)
       --> See Approach #3

    4. Apply final tricks to reduce the time / memory complexity
       --> See Approach #4

  All solutions presented below produce the correct result, but they differ in
  run time and memory requirements.


  非常重要!!
- We call a position in the array a "good index" if starting at that position, we can reach the last index. Otherwise, that index is called a "bad index".
- The problem then reduces to whether or not index 0 is a "good index".

*/

// Approach #2: Dynamic Programming: Top-Down Approach
/*

觀察: ==> 用 DP:Top-Down Approach 優化 Backtracking Algorithm (Approach #1)
- Top-down Dynamic Programming can be thought of as optimized backtracking.

  - It relies on the observation that once we determine that a certain index is
  good / bad, this result will never change. This means that we can store the
  result and not need to recompute it every time.

  - Therefore, for each position in the array, we remember whether the index is
  good or bad. Let's call this array memo and let its values be either one of:
  GOOD, BAD, UNKNOWN. This technique is called memoization[3].

  ex.
  nums = [2, 4, 2, 1, 0, 2, 0]

  Memoization table for input nums[]: (G: GOOD pos, B: BAD pos)
    Index: 0,1,2,3,4,5,6
    nums : 2,4,2,1,0,2,0
    memo : G,G,B,B,B,G,G

  We can see that we cannot start from indices 2, 3 or 4 and eventually reach
  last index (6), but we can do that from indices 0, 1, 5 and (trivially) 6.

- Steps:
(s1) Initially, all elements of the memo table are UNKNOWN, except for the last one, which is (trivially) GOOD (it can reach itself)
(s2) Modify the backtracking algorithm such that the recursive step first checks if the index is known (GOOD / BAD)
  a. If it is known then return True / False
  b. Otherwise perform the backtracking steps as before
(s3) Once we determine the value of the current index, we store it in the memo table

//---------------------------------------------

Time complexity: O(n^2)
- For every element in the array, say i, we are looking at the next nums[i] elements to its right aiming to find a GOOD index. nums[i] can be at most n, where n is the length of array nums.

Space complexity : O(2n) = O(n).
- First n originates from recursion. Second n comes from the usage of the memo table.

*/
enum Index {
  GOOD, BAD, UNKNOWN
}

public class Solution_2 {
  Index[] memo;

  public boolean canJumpFromPos(int pos, int[] nums) {
    // Base Case:
    if (memo[pos] != Index.UNKNOWN) {
      return memo[pos] == Index.GOOD ? true : false;
    }

    // Recursive Case:
    int furthestJump = Math.min(pos + nums[pos], nums.length - 1);

    for (int nextPos = pos + 1; nextPos <= furthestJump; nextPos++) {
      if (canJumpFromPos(nextPos, nums)) {
        memo[pos] = Index.GOOD; // Update result in memo tbl
        return true;
      }
    }

    memo[pos] = Index.BAD; // Update result in memo tbl
    return false;
  }

  public boolean canJump(int[] nums) {
    memo = new Index[nums.length];

    // Init memo tbl to empty (unknown values)
    for (int i = 0; i < memo.length; i++) {
      memo[i] = Index.UNKNOWN;
    }
    // The last elem can reach itself
    memo[memo.length - 1] = Index.GOOD;

    return canJumpFromPos(0, nums);
  }
}


