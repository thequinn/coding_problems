/*
55. Jump Game

LeetCode Std Solution:
- https://leetcode.com/problems/jump-game/


非常重要!!
- This is a dynamic programming question. Usually, solving and fully understanding a dynamic programming problem is a 4 step process:

    1. Start with the recursive backtracking solution.
       --> See Approach #1

    2. Optimize by using a memoization table (top-down[2] dynamic programming)
       --> See Approach #2

    3. Remove the need for recursion (bottom-up dynamic programming)
       --> See Approach #3

    4. Apply final tricks to reduce the time / memory complexity
       --> See Approach #4

  All solutions presented below produce the correct result, but they differ in run time and memory requirements.


非常重要!!
- We call a position in the array a "good index" if starting at that position, we can reach the last index. Otherwise, that index is called a "bad index".
- The problem then reduces to whether or not index 0 is a "good index".

*/

// Approach #3: Dynamic Programming: Bottom-Up Approach
/*

觀察: ==> 用 DP:Bottom-Up Approach 優化 DP:Top-Down Approach (Approach #2)
- Top-down to bottom-up conversion is done by eliminating recursion.

  - In practice, this achieves better performance as we no longer have the
    method stack overhead and might even benefit from some caching.

  - More importantly, this step opens up possibilities for future optimization:
    -- The recursion is usually eliminated by trying to reverse the order of
       the steps from the top-down approach.

思路:
- The observation to make here is that we only ever jump to the right. This
  means that if we start from the right of the array, every time we will query
  a position to our right, that position has already beeen determined as being
  GOOD or BAD. This means we don't need to recurse anymore, as we will always
  hit the memo table.

//---------------------------------------------

Time complexity: O(n^2)
- For every element in the array, say i, we are looking at the next nums[i]
  elements to its right aiming to find a GOOD index. nums[i] can be at most n,
  where n is the length of array nums.

Space complexity: O(n)
- This comes from the usage of the memo table.

*/
enum Index {
  GOOD, BAD, UNKNOWN
}

public class Solution_3 {

  public boolean canJump(int[] nums) {
    Index[] memo = new Index[nums.length];

    // Init memo tbl to empty (unknown values)
    for (int i = 0; i < memo.length; i++) {
      memo[i] = Index.UNKNOWN;
    }
    // The last elem can reach itself
    memo[memo.length - 1] = Index.GOOD;

    for (int i = nums.length - 2; i >= 0; i--) {
      int furthestJump = Math.min(i + nums[i], nums.length - 1);

      for (int j = i + 1; j <= furthestJump; j++) {
        if (memo[j] == Index.GOOD) {
          memo[i] = Index.GOOD;
          break;
        }
      }
    }

    return memo[0] == Index.GOOD;
  }
}
