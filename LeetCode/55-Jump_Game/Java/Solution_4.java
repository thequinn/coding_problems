/*
55. Jump Game

LeetCode Std Solution:
- https://leetcode.com/problems/jump-game/


非常重要!!
- This is a dynamic programming question. Usually, solving and fully understanding a dynamic programming problem is a 4 step process:

    1. Start with the recursive backtracking solution.
       --> See Approach #1

    2. Optimize by using a memoization table (top-down[2] dynamic programming)
       --> See Approach #2

    3. Remove the need for recursion (bottom-up dynamic programming)
       --> See Approach #3

    4. Apply final tricks to reduce the time / memory complexity
       --> See Approach #4

  All solutions presented below produce the correct result, but they differ in run time and memory requirements.


非常重要!!
- We call a position in the array a "good index" if starting at that position, we can reach the last index. Otherwise, that index is called a "bad index".
- The problem then reduces to whether or not index 0 is a "good index".

*/

// Approach #4: Greedy
/*

觀察: ==> 用 Greedy Algorithm 優化 DP:Bottom-Up Approach (Approach #3)
- Once we have our code in the bottom-up state, we can make one final, important observation (See Approach #3's code):

  - From a given position, when we try to see if we can jump to a GOOD
    position, we only ever use one position - the first one (see the break
    statement in Approach #3, ln-85 in Solution_3.java).  In other words, the
    left-most one.
  - If we keep track of this left-most GOOD position as a separate variable,
    we can avoid searching for it in the array. Not only that, but we can stop
    using the array altogether.

思路:
- Iterating right-to-left, for each position we check if there is a potential jump that reaches a GOOD index:

  currPosition + nums[currPosition] >= leftmostGoodIndex

- If we can reach a GOOD index, then our position is itself GOOD. Also, this new GOOD position will be the new leftmost GOOD index. Iteration continues until the beginning of the array. If first position is a GOOD index then we can reach the last index from the first position.

  ex.
  nums = [9, 4, 2, 1, 0, 2, 0].

  Memoization table for input nums[]: (G: GOOD, B: BAD, U: UNKNOWN)
    Index: 0,1,2,3,4,5,6
    nums : 9,4,2,1,0,2,0
    memo : U,G,B,B,B,G,G

  Let's assume we have iterated all the way to position 0 and we need to decide
  if index 0 is GOOD. Since index 1 was determined to be GOOD, it is enough to
  jump there and then be sure we can eventually reach index 6. It does not
  matter that nums[0] is big enough to jump all the way to the last index. All
  we need is one way (重讀: 非常重要!! ln-26).

//---------------------------------------------

Time complexity: O(n)
- We are doing a single pass through the nums array, hence nnn steps, where nnn is the length of array nums.

Space complexity: O(1)
- We are not using any extra memory.

*/
public class Solution_4 {
  public boolean canJump(int[] nums) {
    int lastPos = nums.length - 1;

    for (int i = nums.length - 1; i >= 0; i--) {
      if (i + nums[i] >= lastPos) {
        lastPos = i;
      }
    }
    return lastPos == 0;
  }
}
