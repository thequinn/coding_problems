'''
55. Jump Game

Recursion, Memoization, Greedy
- https://www.youtube.com/watch?v=PfGypLRcoVA

'''

'''
Appraoch #1: Brute Force - Recursion

(1) Recursion, Memoization, Greedy
- https://www.youtube.com/watch?v=PfGypLRcoVA

(2) (必看!) NeetCode using Tree to explain Recursion Basics + Memoization:
- https://www.youtube.com/watch?v=Yan0cv2cLy8

Time : O(max(nums) ^ n)
Space: O(n)

# See Comment Free Version in next section
'''
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        n = len(nums)
        
        def canReach(i):

            # Base Case:
            # - If max jump len, i, can reach the end of nums[] at index = n-1
            if i == n - 1:  return True

            # Recursive Case:
            # - The for loop checks all jump lengthes in the range of 1 ~ num[i]
            # - Can it reach the end of nums[] from i by taking jump length of 
            #   (i + jump)? 
            for jump in range(1, nums[i] + 1):
                if canReach(i + jump):    return True

            # Not a single jump length (i + jump) works
            return False

        return canReach(0)

# Comment Free Version
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        n = len(nums)
        
        def canReach(i):
            # Base Case:
            if i == n-1:    return True

            # Recursive Case:
            for jump in range(1, nums[i]+1):
                if canReach(i+jump):    return True

            return False

        return canReach(0)


'''
Approach #2: Top-Down DP - Recursive Memoization

(1) Recursion, Memoization, Greedy
- https://www.youtube.com/watch?v=PfGypLRcoVA

(2) (必看!) NeetCode using Tree to explain Recursion Basics + Memoization:
- https://www.youtube.com/watch?v=Yan0cv2cLy8

Time : O(n ^ 2)
Space: O(n)
'''
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        n = len(nums)
        memo = {n-1: True}
        
        def canReach(i):
            # Base Case:
            if i in memo:    return memo[i]

            # Recursive Case:
            for jump in range(1, nums[i]+1):
                if canReach(i+jump):    
                    memo[i] = True
                    return True

            memo[i] = False
            return False

        return canReach(0)


'''
Appraoch #3: Bottom-Up DP - Iterative Tabulation 
                            =>...未: Read another explanation to understand

Python 3 (Brute-force, DP, Greedy)
- https://leetcode.com/problems/jump-game/solutions/2225627/python-3-brute-force-dp-greedy/

Time : O(N^2)
Space: O(N)
'''
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        n = len(nums)
        dp = [False] * n
        dp[-1] = True

        # Iterate backwards
        for i in range(n-1, -1, -1):
            print("\ni:", i, ", dp[]:", dp)

            # If cur max jump len nums[i] is 0, we can't jump further. 
            # Immediately skip to the next iteration.
            if nums[i] == 0:    
                print("nums[i] == 0 => con't")
                continue

            # Check diff combo's of jump len
            lowerBound = i + 1
            upperBound = min(i + nums[i], n-1) + 1
            print("lower:", lowerBound, ", upper:", upperBound)
            for j in range(lowerBound, upperBound):
                print("--->j:", j)

                # if dp[j] is True, it implies the pach reaches the last elem. 
                # We need to set dp[i] to True
                if dp[j]:
                    print("dp[", j, "]:", dp[j], " => set dp[", i, "] to True")
                    dp[i] = True
                    break   # Why break the inner loop ...???

        print("\ndp[0]:", dp[0])
        return dp[0]

'''
Approach #4: Greedy => See Jump_Game-Greedy.py bc it's Bottom-up

Intuition: => This approach start at the end

Video by NeetCode:
  @5:06, https://www.youtube.com/watch?v=Yan0cv2cLy8

Text:
(1) Python 3 (Brute-force, DP, Greedy)
- https://leetcode.com/problems/jump-game/solutions/2225627/python-3-brute-force-dp-greedy/

Explanation: 
If we look closely at the problem, we don't necessarily have to visit all the paths to verify whether a valid path exists. As long as we can jump to the element that ultimately leads to the final element, then we should be able to verify whether the valid path exists.

The idea is to iterate through nums from right to left, and for each iteration, we'll check whether i + nums[i] >= index of the element that leads to the final element (I'll call it valid_idx for brevity's sake). If that's true, then it implies the current element leads to the final element. Hence, we'll update valid_idx to store i.

After we finish iterating through nums, we'll check whether valid_idx == 0. If that's true, then it implies the first element leads to the final element. Therefore, we'll return true as an answer. Otherwise, we'll return false as an answer.


=> If need more explanation, see (2)

(2) [Python] Simple solution with thinking process -- Runtime O(n)  
- https://leetcode.com/problems/jump-game/solutions/596454/python-simple-solution-with-thinking-process-runtime-o-n/


Time : O(n)
Space: O(1)
'''
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        N = len(nums)
        valid_idx = N - 1
        
        for i in range(N - 2, -1, -1):
            if i + nums[i] >= valid_idx:
                valid_idx = i
        
        return valid_idx == 0

'''
   '''
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        last_position = len(nums)-1

        # Iterate backwards from second to last item until the first item
        for i in range(len(nums)-2, -1, -1):
            # If this index has jump count which can reach to or beyond the
            # last position
            if (i + nums[i]) >= last_position:
                # Since we just need to reach to this new index
                last_position = i

        return last_position == 0


'''
https://www.youtube.com/watch?v=PfGypLRcoVA

Time : O(n)
Space: O(1)

Note: Same code as above
'''
