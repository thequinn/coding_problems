"""
56. Merge Intervals

思路:
- The idea is to sort the intervals by their starting points. Then, we take the first interval and compare its end with the next interval'ss start.
(1) As long as they overlap, we update the end to be the max end of the overlapping intervals.
(2) If we find a non overlapping interval, we can add the previous "extended" interval and start over.

Video + Sol Code:
    https://www.youtube.com/watch?v=44H3cEC2fFM
"""


# Approach #1-1:
class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        # Sort each interval based on its start elem, i[0]
        # sort(): sorts an array in-place
        # sorted(): gives a new array
        intervals.sort(key = lambda i: i[0])

        # Fill output[] w/ the 1st elem to avoid edge case
        output = [intervals[0]]

        for start, end in intervals[1:]:
            lastEnd = output[-1][1]

            # There is an overlap: 
            # - If cur interval's start falls between (<) or ends right 
            #   after(=) prev interval, there is an overlap
            if start <= lastEnd:
                output[-1][1] = max(lastEnd, end)
            # No overlap.  Simply append the cur interval to output[]
            else:
                output.append([start, end])
        return output


# Approach #1-2: ==> Same as Approach #1-1
# - LeetCode Std Sol:
# -- https://leetcode.com/problems/merge-intervals/solution/
#
# - Time complexity : O(nlogn)
# -- Other than the sort invocation, we do a simple linear scan of the list,
#    so the runtime is dominated by the O(nlgn) complexity of sorting.
# - Space complexity : O(1) or O(n)
# -- If we can sort intervals in place, we do not need more than constant
#    additional space. Otherwise, we must allocate linear space to store a copy
#    of intervals and sort that.
#
class Solution:

  def merge(self, intervals: List[List[int]]) -> List[List[int]]:
    # sort(): sorts an array in-place
    # sorted(): gives a new array
    intervals.sort(key = lambda x: x[0])

    merged = []
    for interval in intervals:
      # if the list of merged intervals is empty or if the current
      # interval does not overlap with the previous, simply append it.
      if not merged or merged[-1][1] < interval[0]:
        merged.append(interval)
      else:
        # otherwise, there is overlap, so we merge the current and previous
        # intervals.
        merged[-1][1] = max(merged[-1][1], interval[1])

    return merged


#---------- Testing ----------

# Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6]
in1 = [[1,3],[2,6],[8,10],[15,18]] # Output: [[1,6],[8,10],[15,18]]

in2 = [[1,7],[2,6]]  # Output: [[1,7]]

in3 = [[1,4],[4,5]]  # Output: [[1,5]]

sol = Solution()
res = sol.merge(in1)
print("\nres: ", res)
