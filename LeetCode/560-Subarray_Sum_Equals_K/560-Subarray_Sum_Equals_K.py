'''
560. Subarray Sum Equals K


Review Prefix Sum Concept, and why 1st elem of prefix sums array is 0
    /Users/as/Library/CloudStorage/Dropbox/刷题-網站-LeetCode/_模板+題型分類+範例/Prefix_Sum_\&_Suffix_Sum



⚠️  ⚠️  ⚠️
Video Explaination:
=> NeetCode's explaination is NOT good !!!
- https://www.youtube.com/watch?v=fFVZt-6sgyo
⚠️  ⚠️  ⚠️

'''

'''
Approach #1: 
  (Bruth Force) Prefix Sum + Nested Loops => TLE, but MUST learn this ex to
                                         be familiar w/ Prefix Sum concept
                                         as foundation!
- Time complexity : O(n2)
- Space complexity : O(n)

Explanation + Sol: 
- https://leetcode.com/problems/subarray-sum-equals-k/solutions/803317/java-solution-with-detailed-explanation/

- Key Concept:
    k is sumOfSubarray
    sumOfSubarray = prefix_sums[end] - prefix_sums[start];

- Algorithm applying Prefix Sums Throey
  - Iter thr the input arr, and calc the prefix sums arr
  - Create a nested for loop to iter thr the ps[] using two ptrs
    - From the prefix sum theory, we know k is the sum of subarr
    - If ps[end] - ps[start] == k, sum of subarr is found 

'''
class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        leng = len(nums)  # len of nums[]
        leng2 = leng + 1 # len of ps[] (prefix sums arr)
        count = 0  # track num of sum of subarr

        # Calc prefix sums
        ps = [0] * leng2
        for i in range(1, leng2):
            ps[i] = ps[i-1] + nums[i-1]
        print("ps[]:", ps)
        
        # Get num of sum of subarr equals to k
        for start in range(leng2):
            for end in range(start + 1, leng2):
                # calc the sum of subarr
                if ps[end] - ps[start] == k:
                    count += 1
        return count


# Driver code
nums = [1,1,1]
k = 2
sol = Solution()
res = sol.subarraySum(nums, k)
print("res:", res)



'''
Approach #2-1: Prefix Sum + Optimized by Hashmap => ⚠️  This is a DP sol!

- Time complexity : O(n).
- Space complexity : O(n).


# - - - - - - - - - - - - - - # - - - - - - - - - - - - - - #
Explanation + Sol: Same link as in Approach #1:
- https://leetcode.com/problems/subarray-sum-equals-k/solutions/803317/java-solution-with-detailed-explanation/

- Key Concept:
  1. We judge when the array is traversed, no need 2nd loop
  2. Obviously, when sum[end] is calculated, all its possible sum[start] are 
  already in the map.

    prefix_sums[end] - prefix_sums[start] == k

    prefix_sums[end] - k == prefix_sums[start]


# - - - - - - - - - - - - - - # - - - - - - - - - - - - - - #
***Why init dict/hashmap as {0: 1}?

ex. a  = [1,2,1,3] and k = 3

A:  ps1 = [1,3,4,7]   => Not include 0 at index=0
    ps2 = [0,1,3,4,7] => Include 0 at index=0
    
    There're 3 sub-arrs haing sum equals to k
    - 0->3, 1->4, 4->7

    ps1 will produce 1 less count, so we manually init d = {0: 1}

'''
from typing import List
class Solution:
    def subarraySum(self, nums:List[int], k: int) -> int:
        count = 0
        prefix_sum = 0
        dct = {0: 1}

        for i in range(len(nums)):
            # Calc cur prefix sum
            prefix_sum = prefix_sum + nums[i]

            # sum[end] - k == sum[start]. So check if sum[end] in dct.
            prefix_sum_start = prefix_sum - k
            if (prefix_sum_start) in dct:
                count += dct[prefix_sum_start]
            dct[prefix_sum] = count
        return count


'''
Approach #2-2: More concise code
- https://leetcode.com/problems/subarray-sum-equals-k/solutions/341399/python-clear-explanation-with-code-and-example/
'''
class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        count = 0
        prefix_sum = 0
        d = {0: 1}

        for num in nums:
            prefix_sum += num
            count += d.get(prefix_sum - k, 0)
            d[prefix_sum] = d.get(prefix_sum, 0) + 1

        return count


