'''
57. Insert Interval

'''

'''
NeetCode:
- https://youtu.be/A8NUOmlwOlM?t=297
- 思路:
  @4:57 gives the key pt to solve this problem 
  - It tells if a cur interval is overlapping with the new interval (cur 
  interval is from intervals[])
  - Not overlapping  
    (1) If the end val of the new interval less than the start val of the cur interval
    (2) If the start val of the new interval more than the end val of the cur interval
  - Overlapping
    - If not fullfull (1) and (2), there is an overlap
'''

class Solution:
    def insert(
        self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        res = []

        for i in range(len(intervals)):
            # If there is no overlap
            # - the new interval is before the range of other intervals
            if newInterval[1] < intervals[i][0]:
                res.append(newInterval)
                return res + intervals[i:]
            # If there is no overlap
            # - the new interval is after the range of other intervals
            elif newInterval[0] > intervals[i][1]:
                res.append(intervals[i])
            # If there is an overlap
            else:  
            # Same as next ln
            #elif newInterval[0] <= intervals[i][1] or newInterval[1] >= intervals[i][0]:
                newInterval = [
                    min(newInterval[0], intervals[i][0]),
                    max(newInterval[1], intervals[i][1]),
                ]
        res.append(newInterval)
        return res


'''
Same concept, diff code to implement it

Sol Code:
- https://leetcode.com/problems/insert-interval/solutions/844494/python-o-n-solution-explained/?orderBy=most_votes
- Note: Use the code from commenter, grc1618.  The original sol is confusing using i -= 1 inside our loop, before break.

'''
class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
    if not intervals: return [newInterval]
    res = []
    for i, interval in enumerate(intervals):
        if interval[1] < newInterval[0]:
            res.append(interval)
        else:
            if interval[0] > newInterval[1]:
                return res + [newInterval] + intervals[i:]
            else:
                newInterval[0] = min(newInterval[0], interval[0])
                newInterval[1] = max(newInterval[1], interval[1])

    return res + [newInterval]

