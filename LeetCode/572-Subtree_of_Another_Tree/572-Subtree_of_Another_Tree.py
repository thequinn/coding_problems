'''
572. Subtree of Another Tree

'''

'''
Approach#1:

Video Explanation + Sol Code:
    https://www.youtube.com/watch?v=E36O5SWp-LE

Sol Code:
    https://leetcode.com/problems/subtree-of-another-tree/solutions/265239/python-easy-to-understand/


思路:
Note: Order of if statements matters here

(1) If s NOT exist, there's no way t exists in a non-existing tree, s
(2) If s exists, t might exist in s
(3) If both s and t exist, t might exist in s
    - For (2) & (3):
        - If t is a subtree of s, return True
        - Else, recursively visit the left and right childs of s

'''

class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        # Condition (1) above
        if not s:   return False
        # Condition (2) (3) above
        if self.isSameTree(s, t):   return True
        return self.isSubtree(s.left, t) or self.isSubtree(s.right, t)

    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        if p and q:
            return p.val == q.val and \
                self.isSameTree(p.left, q.left) and \
                self.isSameTree(p.right, q.right)
        return p is q

# = = = = = = = = = = # = = = = = = = = = = #
'''
The rest of the 4 BFS approaches are from the link below:
    https://leetcode.com/problems/subtree-of-another-tree/solutions/1176589/clean-code-5-python-methods-bfs-preorder-postorder-inorder-level-bfs-dfs/
'''

'''
Approach #2: BFS with Level
'''
class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        if t is None: return True
        def same(p, q):
            if not p and not q: return True
            if not p or not q or p.val != q.val: return False
            return same(p.left, q.left) and same(p.right, q.right)    
        
        stack = collections.deque([s])
        pre = []
        while stack:
            for _ in range(len(stack)):
                s = stack.popleft()
                pre.append(s)
                if s.left: stack.append(s.left)
                if s.right: stack.append(s.right)
        
        same_tree = partial(same, q=t)
        
        # ex. root = [3,4,5,1,2], subRoot = [4,1,2]
        #   pre = [3,4,5,1,2], 
        #   reversed(pre) let same_tree() check the right-most subtree first 
        #
        # The next 2 ln's are both correct.  The arg w/ reversed(pre) is faster.
        tmp = map(same_tree, reversed(pre))  
        # tmp = map(same_tree, pre)
 
        if any(tmp):    return True
        return False

'''
Approach #3: BFS with Preorder
'''
class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        if t is None: return True

        def same(p, q):
            if not p and not q: return True
            if not p or not q or p.val != q.val: return False
            return same(p.left, q.left) and same(p.right, q.right)        

        stack = []
        pre = []
        while stack or s:
            while s:
                pre.append(s) # if s.val == t.val else None # seems the pruning is not work at all
                stack.append(s)
                s = s.left
            s = stack.pop()
            s = s.right
        same_tree = partial(same, q=t)
        if any(map(same_tree, reversed(pre))): return True
        # for s in pre:
        #     if same(s, t): return True
        return False


'''
Approach #4: BFS with Inorder
'''
class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        if t is None: return True

        def same(p, q):
            if not p and not q: return True
            if not p or not q or p.val != q.val: return False
            return same(p.left, q.left) and same(p.right, q.right)        

        stack = []
        pre = []
        while stack or s:
            while s:
                #pre.append(s) # if s.val == t.val else None # seems the pruning is not work at all
                stack.append(s)
                s = s.left
            s = stack.pop()
			pre.append(s)
            s = s.right
        same_tree = partial(same, q=t)
        if any(map(same_tree, reversed(pre))): return True
        # for s in pre:
        #     if same(s, t): return True
        return False

'''
Approach #: BFS with Postorder
'''
class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        if t is None: return True
        def same(p, q):
            if not p and not q: return True
            if not p or not q or p.val != q.val: return False
            return same(p.left, q.left) and same(p.right, q.right)        
     
        # check post order
        stack = [s]
        pre = []
        while stack:
            s = stack.pop()
            pre.append(s) # reversed pre will be true postorder
            if s.left: stack.append(s.left)
            if s.right: stack.append(s.right)
        same_tree = partial(same, q=t)
        if any(map(same_tree, reversed(pre))): return True
        return False
