'''
58. Length of Last Word

'''


'''
Approach #1: By QN

思路:
- Remote right trailing spaces.  
- Iter backwards until reaching a space
'''
class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        # Strip R end of str
        s = s.rstrip()

        leng = 0
        for c in s[::-1]:
            if c != ' ':  leng += 1
            else:         return leng
        # For testcase: s = "a"
        return leng

'''
Approach #2:
    https://leetcode.com/problems/length-of-last-word/solutions/4954087/97-43-easy-solution-with-explanation/?envType=study-plan-v2&envId=top-interview-150


Approach:
  1. Strip trailing whitespaces from the input string using the strip() method.
  2. Split the string into words using the split() method.
  3. If there are no words after stripping whitespaces, return 0.
  4. Otherwise, return the length of the last word


Time complexity:
  1. Stripping trailing whitespaces takes linear time, so it's O(n) where n is the length of the input string.
  2. Splitting the string into words also takes linear time, so it's also O(n).
  3. The overall time complexity is O(n).

Space complexity:
  We store the list of words, which could take up to O(n) space if all characters in the input string are non-whitespace.
'''
class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        words = s.strip().split()
        
        if not words:  return 0
        return len(words[-1])


