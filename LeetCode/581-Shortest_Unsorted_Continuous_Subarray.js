/*
581. Shortest Unsorted Continuous Subarray

LeetCode Std Solution:
- https://leetcode.com/problems/shortest-unsorted-continuous-subarray/solution/

Appraoches worth learning:
- Appraoch #3: Using Sorting
- Approach #4: Using Stack           ==> Nick White says boring
- Approach #5: W/o Using Extra Space ==> Very similar w/ Approach #3
*/

// Approach 3: Using Sorting
// - 思路:
// -- By comparing sorted nums[] and unsorted nums[], we can determine the
//    leftmost and rightmost elems which mismatch.  The subarr lying b/t them
//    is the required shorted unsorted subarr.
//
// - 思路 + Solution Code: ==> 注意!!  LeetCode Std Sol 的code難懂, 用下方連結
// -- https://www.youtube.com/watch?v=N3hrzgluUvE
// -- dwhelper/LeetCode/
//
// - Time complexity : O(nlogn).  Sorting takes nlogn time.
// - Space complexity : O(n).  We are making copy of original array.
//
var findUnsortedSubarray = function(nums) {

  // sort(): Sorts elems of an arr in place and returns the sorted arr, so we
  //         need to make a copy of nums[]
  let snums = [...nums]
  snums.sort((a,b) => a-b);  // Sort in ascending order

  let start = end = -1;
  for (let i = 0; i < nums.length; i++) {

    // When a mismatch is found and start is not set, set start and end to i
    if (nums[i] != snums[i] && start == -1) {
      start = end = i;
    }
    else if (nums[i] != snums[i]) {
      end = i;
    }
  }
  console.log("start:", start, ", end:", end);

  if (start != -1 || end != -1)
    return end - start + 1;
  return 0;
};


// Approach #5: W/o Using Extra Space
// - 思路:
// -- https://www.youtube.com/watch?v=p-O7FExDH1M&t=308s
//
// - Time complexity : O(n). Four O(n) loops are used.
// - Space complexity : O(1).  Constant space is used.
//
var findUnsortedSubarray = function(nums) {
  let min = Infinity, max = -Infinity;

  /***** 注意!! Run test nums_2[] to understand ln-65~89 *****/

  // Find where discontinuity occurs for min
  let flag = false;
  for (let i = 1; i < nums.length; i++) {
    // Once "curr val < prev val", you enter "the zone" of the unsorted sub-arr
    if (nums[i] < nums[i-1]) {
      flag = true;
      console.log("ln-70, flag = true");
    }
    // Now you're in "the zone," find the min val of the sub-arr
    if (flag) {
      console.log("ln-74, min:", min, ", nums[i]:", nums[i], ", i:", i);
      min = Math.min(min, nums[i]);
    }
  }
  console.log();

  // Find where discontinuity occurs for max
  flag = false;
  for (let i = nums.length - 2; i >= 0; i--) {
    if (nums[i] > nums[i+1]) {
      flag = true;
      console.log("ln-85, flag = true");
    }
    if (flag) {
      console.log("ln-88, max:", max, ", nums[i]:", nums[i], ", i:", i);
      max = Math.max(max, nums[i]);
    }
  }
  console.log("min:", min, ", max:", max);

  // Find min and max in discontinuity range to determin the shortest subarray
  // ex.  0  1  2  3   4  5  6   7
  //     [2, 6, 4, 8, 10, 9, 3, 15] => l is @ index 2, r is @ index 6
  let l, r;
  // If min < curr val, index of curr val is the start pt  to sort the sub-arr
  for (l = 0; l < nums.length; l++) {
    if (min < nums[l])    break;
  }
  for (r = nums.length - 1; r >= 0; r--) {
    if (max > nums[r])    break;
  }
  console.log("l:", l, ", r:", r);

  return r - l < 0 ? 0 : r - l + 1;
}

//---------- Testing ----------

//let nums_1 = [2, 6, 4, 8, 10, 9, 15]; // 5: [6, 4, 8, 10, 9]
//let res = findUnsortedSubarray(nums_1);

// 注意!! Run this test to understand ln-65~89
//
let nums_2 = [2, 6, 4, 8, 10, 9, 3, 15];
let res = findUnsortedSubarray(nums_2);

//let nums_3 = [1,2,3,4]; // 0
//let res = findUnsortedSubarray(nums_3);

console.log("\nres:", res);
