/*
617. Merge Two Binary Trees

*/

/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

// Approach #1: Recursive: DFS Preorder/Inorder/Postorder Traversal
// - LeetCode Std Solution:
// -- https://leetcode.com/problems/merge-two-binary-trees/solution/
//
// - Time & Space Complexity: O(m)
// -- WRONG !!  LeetCode Std Solution's explanation
// -- CORRECT:  See comment by basi4869 under the LeetCode Std Sol:
//    |
//    ==> m stands for "the number of overlapping nodes b/y the two trees", not
//        the "min" num of nodes b/t the two, as stated in LeetCode Std Sol.
//
//        ex. Tree 1 extends to the left for 100 times, Tree 2 extends to the
//            right for 100 times. The algorithm only takes 1 recursion (QN
//            thinks it's 2 recursive calls) to complete the merge, which is
//            the number of overlapping nodes b/t the two.
//
// - Approach #1-1: Preorder Traversal
const mergeTrees = (t1, t2) => {
  if (!t1)    return t2;
  if (!t2)    return t1;

  t1.val += t2.val;
  // 注意!! Don't forget to assign returned left & right branchs (after stacks
  // returned) back to curr node, ln-28~29.
  t1.left = mergeTrees(t1.left, t2.left);
  t1.right = mergeTrees(t1.right, t2.right);
  return t1;
};
//
// - Approach #1-2: Inorder Traversal
var mergeTrees = function(t1, t2) {
  if (t1 == null)   return t2;
  if (t2 == null)   return t1;

  // 注意!! Don't forget to assign returned left & right branchs (after stacks
  // returned) back to curr node, ln-50, ln-52.
  t1.left = mergeTrees(t1.left, t2.left);
  t1.val += t2.val;
  t2.right = mergeTrees(t1.right, t2.right);
  return t1;
};

//------------------------------

// Approach #2: Iterative: BSF w/ Queue
//                         (這題用Stack 也可, 但我覺得用 BFS+Queue 較好記!)
//
// - https://leetcode.com/problems/merge-two-binary-trees/discuss/443033/617-Merge-Two-Binary-Trees-Py-All-in-One-By-Talse
//
// - Time & Space Complexity: O(m)
//
// - Approach #2-1:  ==> 不用看, 用 Approach #2-2 較易懂又快!
const mergeTrees = (t1, t2) => {
  if ( !(t1 && t1) )   return t1 || t2;

  const queue = [];
  queue.push([t1, t2]);

  while (queue.length != 0) {
    const t = queue.pop();

    // If nothing to add
    if (t[0] == null || t[1] == null) {
      continue;
    }

    // Else
    t[0].val += t[1].val;

    if (!t[0].left && t[1].left)      t[0].left = new TreeNode(0);
    if (!t[0].right && t[1].right)    t[0].right = new TreeNode(0);

    queue.push( [t[0].left, t[1].left] );
    queue.push( [t[0].right, t[1].right] );
  }

  return t1;
}
//
// Approach #2-2:
const mergeTrees = (t1, t2) => {

  // If only one tree is available, return the available one
  if ( !(t1 && t1) )   return t1 || t2;

  const queue = [];
  queue.push([t1, t2]);

  while (queue.length != 0) {
    const t = queue.pop();

    // If t2 is null, we keep t1 intact and move on to next item in the queue
    //
    //if (t[0] == null || t[1] == null)  // No need to check t[0] == null
    if (t[1] == null)                    // QN improved the last ln in sol code
      continue;

    // Else: If t2 is not null, merge t2 to t1
    t[0].val += t[1].val;

    if (t[0].left == null)    t[0].left = t[1].left;
    else    queue.push( [t[0].left, t[1].left] );

    if (t[0].right == null)   t[0].right = t[1].right;
    else    queue.push( [t[0].right, t[1].right] );
  }

  return t1;
}
