/*
62. Unique Paths

思路:
- First of all you should understand that we need to do n + m - 2 movements : m - 1 down, n - 1 right, because we start from cell (1, 1).

- Secondly, the path it is the sequence of movements( go down / go right),
therefore we can say that two paths are different
when there is i-th (1 .. m + n - 2) movement in path1 differ i-th movement in path2.

- So, how can we build paths? Let's choose (n - 1) movements(number of steps to the right) from (m + n - 2), and rest (m - 1) is (number of steps down).

I think now it is obvious that count of different paths are all combinations (n - 1) movements from (m + n-2)

以下 Video 解釋更清楚 + Combination Math:
- https://www.youtube.com/watch?v=M8BYckxI8_U

//--------------------------------

Explanation + Sol Code:
- https://leetcode.com/problems/unique-paths/discuss/22981/My-AC-solution-using-formula

*/

// Appraoch: Using Combination Math Formula
const uniquePaths = function(m, n) {
  if (m < n)   return uniquePaths(n,m);

  // n+m-2 steps needed to reach final pos
  // - m-1 down, n-1 right, b/c we start from cell (1, 1)
  let N = n + m - 2;

  let k = m - 1; // number of steps that need to go down

  // Calculate the total possible paths:
  // - Combination(N, k)
  //   = n! / (k! * (n - k)!)
  //   = ( (n - k + 1) * (n - k + 2) * ... * n ) / k!
  // - 用 C(100,10) 證明
  //
  let res = 1;
  for (let i = 1; i <= k; i++)
    res = res * (N - k + i) / i;

  return res;
};

