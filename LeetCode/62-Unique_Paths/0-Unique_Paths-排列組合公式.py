"""
62. Unique Paths

- Video:
-- https://www.youtube.com/watch?v=M8BYckxI8_U

- Explanation + Sol Code:
(1) Python version in user hxuanz's comment
  https://leetcode.com/problems/unique-paths/discuss/22981/My-AC-solution-using-formula

(2) Code in Method #5:
  https://leetcode.com/problems/unique-paths/discuss/1581998/C%2B%2BPython-5-Simple-Solutions-w-Explanation-or-Optimization-from-Brute-Force-to-DP-to-Math
"""

def uniquePaths(self, m, n):
  return reduce(lambda res, i: res * (n - 1 + i) / i, range(1, m), 1)
