'''
62. Unique Paths

Explanation + Sol Code:
  https://leetcode.com/problems/unique-paths/discuss/1581998/C%2B%2BPython-5-Simple-Solutions-w-Explanation-or-Optimization-from-Brute-Force-to-DP-to-Math/


...未....Not yet review runtime...未......未......未......未...

//------------------------------------------
- 思路拆解 for Method #1 & #2:

1. At each cell we can either move down or move right.
2. Choosing either of these moves could lead us to an unique path. So we
   consider both of these moves.
3. If the series of moves leads to a cell outside the grid's boundary, we can
   return 0 denoting no valid path was found.
4. If the series of moves leads us to the target cell (m-1, n-1), we return 1
   denoting we found a valid unique path from start to end.


  Formula: (i,j) 的結果來自它的上一步的和: 上 + 左

          dp[i][j] = dp[i-1][j] + dp[i][j-1];

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- 思路拆解 for Method #3:

dp[i][j] will denote the number of unique paths from cell (0, 0) to cell (i, j). (Note this differs from memoization appraoch where dp[i][j] denoted number of unique paths from cell (i, j) to the cell (m-1,n-1))

In this case, we first establish some base conditions first.

1. We start at cell (0, 0), so dp[0][0] = 1.
2. Since we can only move right or down, there is only one way to reach a cell
   (i, 0) or (0, j). Thus, we also initialize dp[i][0] = 1 and dp[0][j]=1.
   => Meaning all cells in the 1st row and 1 col have val of 1.
3. For every other cell (i, j) (where 1 <= i <= m-1 and 1 <= j <= n-1), we can
   reach here either from the top cell (i-1, j) or the left cell (i, j-1).

   The result for number of unique paths to arrive at (i, j) is the summation
   of both, i.e, dp[i][j] = dp[i-1][j] + dp[i][j-1].


'''


'''
Method #1: Brute Force Recursion  ==> Same as DFS
'''
def uniquePaths(self, m, n, i=0, j=0):
  # Out of bound
  if i >= m or j >= n:       return 0
  # Base case: Destination reached
  if i == m-1 and j == n-1:  return 1
  # Recursive case:
  return self.uniquePaths(m, n, i+1, j) + self.uniquePaths(m, n, i, j+1)

# = = = = = = = = = # = = = = = = = = = # = = = = = = = = =
'''
Method #2A-1: DP - Memoization => Using nested func
'''
class Solution:
  def  uniquePaths(self, m:int, n:int) -> int:

    # Init a 2D array
    dp = [ [0 for cols in range(n)] for rows in range(m) ]
    # 注意!! Don't use it! Python2 uses it, but it doesn't work here!!
    #dp = [ [-1]*n ] *m

    '''
    注意!! Nested func doesn't need 'self' as a param
    '''
    def dfs(i, j):
      # Check arr out  of bound
      if i >= m or j >=n:       return 0

      # Destination reached
      if i == m-1 and j == n-1: return 1

      # Check Memoized arr to see if the val is already calculated
      if dp[i][j] != -1:  return dp[i][j]

      # Assign the result to dp[i][j] for Memoization
      dp[i][j] = dfs(i+1, j) + dfs(i, j+1)
      return dp[i][j]

    return dfs(0, 0)

'''
Method #2A-2: DP - Memoization ==> Not using a nested func, but a helper func

'''
class Solution:
  def uniquePaths(self, m:int, n:int) -> int:
    dp = [ [0 for cols in range(n)] for rows in range(m)]
    return self.dfs(dp, m, n, 0, 0)

  def dfs(self, dp, m:int, n:int, i:int, j:int) -> int:
    if i >= m or j >= n:    return 0
    if i == m-1 and j == n-1: return 1
    # Check memoization for optimization
    if dp[i][j] > 0:   return dp[i][j]
    # Assign the result to dp[i][j] for Memoization
    dp[i][j] = self.dfs(dp, m, n, i+1, j) + self.dfs(dp, m, n, i, j+1)
    return dp[i][j]

'''
Method #2B: DP - Memoization: using @cache decorator in Python3 instead of
                              dp[][] in Method #2A
'''
class Solution:
    def uniquePaths(self, m, n):
        @cache
        def dfs(i, j):
            if i >= m or j >= n:      return 0
            if i == m-1 and j == n-1: return 1
            return dfs(i+1, j) + dfs(i, j+1)
        return dfs(0, 0)
# = = = = = = = = = # = = = = = = = = = # = = = = = = = = =

'''
Method #3: DP - Tabulation (= Bottom-Up Approach)
=> Look at concept at the top of this file
'''
class Solution:
    def uniquePaths(self, m, n):
        dp = [ [1]*n for i in range(m) ]
        for i, j in product(range(1, m), range(1, n)):
            dp[i][j] = dp[i-1][j] + dp[i][j-1]
        return dp[-1][-1]
'''
...未... not yet understand Method #3


...未... Method #4
...未... Method #5

'''
