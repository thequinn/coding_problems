/*
62. Unique Paths

Notice from QN:
-  Although LeetCode's ex shows m is col, n is row.  It doesn't matter in
code.  m could be row and n could be col.

//------------------------------------------
- 思路拆解：
1. At each cell we can either move down or move right.
2. Choosing either of these moves could lead us to an unique path. So we
   consider both of these moves.
3. If the series of moves leads to a cell outside the grid's boundary, we can
   return 0 denoting no valid path was found.
4. If the series of moves leads us to the target cell (m-1, n-1), we return 1
   denoting we found a valid unique path from start to end.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 太重要了!!!
  |
  ====> QN 簡化以上思路結果為formula: (做到 Approach #3 才懂)

        (i,j) 的結果來自它的上一步的和: 上 + 左

          dp[i][j] = dp[i-1][j] + dp[i][j-1];

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Method #1: Brute Force Recursion  ==> Same as DFS
- Same as DFS, but too expensive b/c Time limit exceed for bigger value

- Java:
    https://leetcode.com/problems/unique-paths/discuss/182143/Recursive-memoization-and-dynamic-programming-solutions
- C++ & Python:
    https://leetcode.com/problems/unique-paths/discuss/1581998/C%2B%2BPython-5-Simple-Solutions-w-Explanation-or-Optimization-from-Brute-Force-to-DP-to-Math
*/
var uniquePaths = function(m, n) {    // m is col, n is row
  return helper(m-1, n-1);
};
var helper = function(m, n) {
  if (m < 0 || n < 0)         return 0;
  else if (m == 0 || n == 0)  return 1;
  else
    // return 給(m,n): (m,n) 的上 + (m,n) 的左
    return helper(m-1, n) + helper(m, n-1);
};

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Method #2: Top-Down Memoization
- Runtime 0 ms, Time complexity: O(m x n), Space complexity: O(m x n)

- Java:
    https://leetcode.com/problems/unique-paths/discuss/182143/Recursive-memoization-and-dynamic-programming-solutions
- C++ & Python:
    https://leetcode.com/problems/unique-paths/discuss/1581998/C%2B%2BPython-5-Simple-Solutions-w-Explanation-or-Optimization-from-Brute-Force-to-DP-to-Math
*/
var uniquePaths = function(m, n) {    // m is col, n is row
  // Tip !!
  // Init board: Create snf pre-fill a 2D array in JavaScript
  let memo = Array(n).fill(null).map(() => {  // Fill memo[][] w/ 0's
    return Array(m).fill(0);
  });
  //console.log("memo:", memo);

  return helper(m-1, n-1, memo);
};
var helper = function(m, n, memo) {
  if (m < 0 || n < 0)         return 0;
  else if (m == 0 || n == 0)  return 1;
  // Memoization
  else if (memo[n][m] > 0)    return memo[n][m];
  else {
    memo[n][m] = helper(m-1, n, memo) + helper(m, n-1, memo);
    return memo[n][m];
  }
};

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Method #3: Bottom-Up (Tabulation)
- Runtime 1 ms, Time complexity O(m x n), Space complexity O(m x n)

- Java:
    https://leetcode.com/problems/unique-paths/discuss/182143/Recursive-memoization-and-dynamic-programming-solutions
- C++ & Python:
    https://leetcode.com/problems/unique-paths/discuss/1581998/C%2B%2BPython-5-Simple-Solutions-w-Explanation-or-Optimization-from-Brute-Force-to-DP-to-Math
*/
var uniquePaths = function(m, n) {    // m is col, n is row

  // Create and pre-fill dp[][] w/ 0's
  var dp = Array(n).fill(null).map(() => {
    return Array(m).fill(0);
  });

  // Base Cases mentioned in 思路拆解 above
  for (let i = 0; i < n; i++)   // Fill left-most col w/ 1's
    dp[i][0] = 1;
  for (let i = 0; i < m; i++)   // Fill top-most  row w/ 1's
    dp[0][i] = 1;
  //console.log("dp:", dp);

  // Starting from (1,1)
  for (let i = 1; i < n; i++) {
    for (let j = 1; j < m; j++) {
      // (i,j) 的結果來自它的上一步的和: 上 + 左
      // --> 比較 ln-107, Approach #3, 1-Unique_Paths-Recursive_to_DP-mateatomico.js
      dp[i][j] = dp[i-1][j] + dp[i][j-1];
    }
  }
  return dp[n-1][m-1]; // 注意
};

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Method #4: Bottom-Up Optimized
 - Space complexity improves to O(n)

 - Java:
    https://leetcode.com/problems/unique-paths/discuss/116646/Very-Simple-JavaScript-Solution-in-100th-Percentile-(56ms)
- C++ & Python:
    https://leetcode.com/problems/unique-paths/discuss/1581998/C%2B%2BPython-5-Simple-Solutions-w-Explanation-or-Optimization-from-Brute-Force-to-DP-to-Math

 - The insight with my implementation is that we don't need to allocate the
   full m x n matrix. One row will suffice.
   -(1) Init the first row the same way you did before and simply mutate the
        values of that first row with the new values of the second row.
   -(2) You can iterate this process until you have calcluated the final row
        of the matrix. This works b/c we will only ever need "col k" to calc
        the values of "col k+1"
*/
var uniquePaths = function(m, n) {

  // Assigning 1's is simply a shortcut that skips some later computation
  // as matrix[i][0] will never change in this iterative process.
  let dp = new Array(n).fill(1);

  // Starting from (1,1)
  for (let r = 1; r < m; r++) {
    for (let c = 1; c < n; c++) {
      dp[c] = dp[c] + dp[c - 1];
    }
  }
  return dp[n - 1];  // 注意
};

// Now, you may wonder whether we can further reduce the memory usage to just O(1) space since the above code seems to use only two variables (cur[j] and cur[j - 1]). However, since the whole row cur needs to be updated for m - 1 times (the outer loop) based on old values, all of its values need to be saved and thus O(1)-space is impossible. However, if you are having a DP problem without the outer loop and just the inner one, then it will be possible.
