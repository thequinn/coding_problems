/*
QN (Approach #1) vs LeetCode user mateatomico (Approach #2-A)
- The diff is ln-38 ln-42 vs ln-66:
  (1) ln-38 ln-42 decide whether to do a recursive call before the recursive
      call build a new stack
  (2) ln-66 decides whether to do a recursive call after the recursive call
      builds a new stack
*/


// Approach #1: Recursive ==> by Quinn
//
// 思路:
// - 從 (0,0) 出發 to call helper()
// - In helper()
//   - Base Case:
//      - If reach destination, return 1 as one more route found
//   - Recursive Case:
//      - If can go right, call helper() to go right
//      - If can go down, call helper() to go down
//      - Add the 2 returned results and return it
//
const uniquePaths = function(m, n) {
  return helper(m, n, 0, 0);
}

const helper = function(m, n, x, y) {
  console.log("\nx:", x, ", y:", y);

  // Base Case:
  // - Destination reached?  Return 1
  if (x == m-1 && y == n-1) {
    console.log("Destination reached!");
    return 1;
  }

  // Recursive Cases:
  let pathRight = 0, pathDown = 0;

  if (x+1 < m) {  // See comment on the top
    console.log("pathRight:", pathRight);
    pathRight = helper(m, n, x+1, y);
  }
  if (y+1 < n) {  // See comment on the top
    console.log("pathDown:", pathDown);
    pathDown = helper(m, n, x, y+1);
  }
  return pathRight + pathDown;
};

//=========================================================

// Approach #2-A: Recursive ==> See 1-Unique_Path-Recursive_to_DP-直接思路.js
//                              (含 Recursive -> DP)
// - https://leetcode.com/problems/unique-paths/discuss/511059/javascript-from-naive-recursive-to-dp-bottom-up
//
// - 注意!
// -- 這個解法最左上角不是(0,0)開始, 而是(1,1) ==> Approach #2-B 是(0,0)開始
//
const uniquePaths = (m, n) => {
  return helper(m, n, 1, 1);
};

const helper = (m, n, r, c) => {
  console.log("m:", m, ",n:", n, ", r:", r, ", c:", c);

  // Base Case:
  // - Destination reached?  Return 1
  if(r === m && c === n) return 1;

  // Corner Case:
  // - Out of bound? Return 0
  if(r > m || c > n) return 0;  // See comment on the top

  const pathsRight = helper(m, n, r, c + 1);
  const pathsDown = helper(m, n, r + 1, c);

  return pathsRight + pathsDown;
};

//---------------------------------------------------------
// Approach #2-B: Recursive ==> 改成最左上角是(0,0)開始
//
const uniquePaths = (m, n) => {
    return helper(m, n, 0, 0);
};

const helper = (m, n, r, c) => {
  console.log("m:", m, ",n:", n, ", r:", r, ", c:", c);

  // Base Case:
  // - Destination reached?  Return 1
  if(r === m-1 && c === n-1) return 1;

  // Corner Case:
  // - Out of bound? Return 0
  if(r > m-1 || c > n-1) return 0;

  const pathsRight = helper(m, n, r, c + 1);
  const pathsDown = helper(m, n, r + 1, c);

  return pathsRight + pathsDown;
};

//---------- Testing ----------

//let m = 3, n = 2;  // Output:3
let m = 3, n = 3;  // Output: 6
//let m = 23, n = 12;  // Output: 193536720

let res = uniquePaths(m, n);
console.log("\nres:", res);
