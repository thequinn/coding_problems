/*
62. Unique Paths

思路: --> 這個思路有 2-Unique_Path-Recursive_to_DP-直接思路-mateatomico.js 幫助
- 從 (0,0) 出發 to call helper()
- In helper()
  - Base Case:
     - If reach destination, return 1 as one more route found
  - Recursive Case:
     a. If can go right, call helper() to go right
     b. If can go down, call helper() to go down
     c. The sum of the 2 recursive calls is returned

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 太重要了!!!
  |
  ====> QN 簡化以上思路結果為formula: (做到 Approach #3 才懂)

        (r,c) 的結果來自它的下一步的和 - 向右和向下:

          dp[r][c] = dp[r + 1][c] + dp[r][c + 1];

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~ 極為重要!!! ---> QN 花好幾天直到 “code tracing + 畫樹” 才懂, 因為關乎到從
                   Recursive 改成 DP Top-Down

   - In c., it is 極為重要 to know a. b. are leaves, and their sum are
     returned to their parent.  Then the sum modify the parent's, (x, y).
     - See comments: ln-90~109

   - 要了解需 “畫樹”!!!
   ----> ex. m = 3, n = 2;  // Output:3

                         (0,0)
                       /       \
                      /         \
                     /           \
                 (1,0)           (0,1)
                /     \         /     \
               /       \       /       \
           (2,0)       (1,1)  (x)      (1,1)
           /   \       /   \           /   \
         (x)  (2,1) (2,1)  (x)      (2,1)  (x)
*/
// Approach #1: Recursive ==> by Quinn
/*const uniquePaths = (m, n) => {
  return helper(m, n, 0, 0);
}

const helper = function(m, n, x, y) {
  console.log("\nx:", x, ", y:", y);

  // Base Case:
  if (x == m-1 && y == n-1) {
    console.log("Destination reached!");
    return 1;
  }

  // Recursive Cases:
  let pathRight = 0, pathDown = 0;
  if (x+1 < m) {  // Decide whether to do a recursive call before the recursive
                  // call build a new stack
    console.log("pathRight:", pathRight);
    pathRight = helper(m, n, x+1, y);
  }
  if (y+1 < n) {  // Decide whether to do a recursive call before the recursive
                  // call build a new stack
    console.log("pathDown:", pathDown);
    pathDown = helper(m, n, x, y+1);
  }
  return pathRight + pathDown;
};*/


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Approach #2: DP Top Down Memoization ==> by Quinn
/*const uniquePaths = (m, n) => {
  // Create a pre-filled 2D array:
  //let memo = Array(m).fill(Array(n).fill(-1));  // WRONG!!
  let memo = Array(m).fill().map(() => Array(n).fill(-1));  // Correct

  // Print memo[][]
  //memo.forEach(r => r.forEach(c => console.log(c)))

  return helper(m, n, 0, 0, memo);
}

const helper = function(m, n, x, y, memo) {
  console.log("\nx:", x, ", y:", y);

  // Base Case:
  if (x == m-1 && y == n-1) {
    console.log("Destination reached!");
    return 1;
  }

  //-----------------------
  // Recursive Cases:

  let pathRight = 0, pathDown = 0;

  //console.log("memo[x][y]:", memo[x][y]);
  if (memo[x][y] === -1) {
    if (x+1 < m) {
      console.log("pathRight:", pathRight);

       // 這只是看下步 (x+1, y) 可否行.  這部分的 Recursive Case 目標是 (x, y)
      pathRight = helper(m, n, x+1, y, memo);

      //memo[x+1][y] = pathRight;   // WRONG!!! See ln-25 ~ 極為重要!!!
    }
    if (y+1 < n) {
      console.log("pathDown:", pathDown, memo);

      // 這只是看下步 (x, y+1) 可否行.  這部分的 Recursive Case 目標是 (x, y)
      pathDown = helper(m, n, x, y+1, memo);

      //memo[x][y+1] = pathRight;   // WRONG!!! See ln-25 ~極為重要!!!
    }
    memo[x][y] = pathRight + pathDown;  // 給目標 (x, y) 結果
  }
  return memo[x][y];
};*/


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Approach #3: DP Bottom-Up ==> by Quinn
/*const uniquePaths = (m, n) => {

  // Pre-fill dp[][] w/ 1
  // - Here I am just filling up the whole matrix for convenience. I could
  //   have just implemented filling up right-most column and bottom row, but
  //   that would require more code
  let dp = Array(m).fill().map(() => Array(n).fill(1));
  dp.map(arr => console.log(arr));

  // Starting from (m-2, n-2), ending at (0,0)
  for (let r = m-2; r >= 0; r--) {
    console.log("r:", r);
    for (let c = n-2; c >= 0; c--) {
      console.log("c:", c);

      // (r,c) 的結果來自它的下一步的和 - 向右和向下
      // --> 比較 ln-132, Method #3, 1-Unique_Paths-Recursive_to_DP-daniel_stoian.js
      dp[r][c] = dp[r+1][c] + dp[r][c+1];
    }
  }
  return dp[0][0];
}*/

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Approach #4: DP Space Optimized  ==> by Quinn
//
// - 解釋: Method #4, 1-Unique_Paths-Recursive_to_DP-daniel_stoian.js
const uniquePaths = (m, n) => {
  const dp = new Array(n+1).fill(1);

  for (let r = m-2; r >= 0; r--) {
    for (let c = n-2; c >= 0; c--) {
      // We'll only need "col v+1" to calc the values of "col v"
      dp[c] += dp[c+1];
    }
  }
  return dp[0];
}


//---------- Testing ----------

let m = 3, n = 2;  // Output:3
//let m = 3, n = 3;  // Output: 6
//let m = 23, n = 12;  // Output: 193536720

let res = uniquePaths(m, n);
console.log("\nres:", res);
