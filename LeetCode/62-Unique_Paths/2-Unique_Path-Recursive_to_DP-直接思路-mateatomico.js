/*
62. Unique Paths

- Here's the solution's progression:
        Brute force recursive
        Optimize by memoizing
        Turn around and make it dp bottom up tabular
        Further optimize space

思路 + Solution Code (The author uses 1-base arr instead of 0-base one):
- https://leetcode.com/problems/unique-paths/discuss/511059/javascript-from-naive-recursive-to-dp-bottom-up


- Problem can be decomposed into the following sub-problems:

    can I go right?
    can I go down?

  and the following base cases to get a response to those questions:

    have I gone outside bounds?
        return 0. This is not a valid path.

    have I reached destination?
        return 1. This is a valid path.

  Then I can add up the numbe of valid paths (see code below)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 太重要了!!!
  |
  ====> QN 簡化以上思路結果為formula: (做到 Approach #3 才懂)

        (r,c) 的結果來自它的下一步的和 - 向右和向下:

          dp[r][c] = dp[r + 1][c] + dp[r][c + 1];

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~ 極為重要!!!
      - 要了解需 “畫樹”!!!
      ----> ex. m = 3, n = 2;  // Output:3

                         (0,0)
                       /       \
                      /         \
                     /           \
                 (1,0)           (0,1)
                /     \         /     \
               /       \       /       \
           (2,0)       (1,1)  (x)      (1,1)
           /   \       /   \           /   \
         (x)  (2,1) (2,1)  (x)      (2,1)  (x)
*/


// Approach #1: Brute force recursive
/*const uniquePaths = (m, n) => {
    return helper(m, n, 1, 1);
};

const helper = (m, n, row, col) => {
  if(row === m && col === n) return 1;

  // Decides whether to do a recursive call after the recursive call builds a
  // new stack
  if (row > m || col > n) return 0;

  const pathsDown = helper(m, n, row + 1, col);
  const pathsRight = helper(m, n, row, col + 1);

  return pathsRight + pathsDown;
};*/

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Approach #2: Memoized
/*const uniquePaths = (m, n) => {
  // Create a pre-filled 2D array:
  //let memo = Array(m).fill(Array(n).fill(-1));  // WRONG!!
  let memo = Array(m).fill().map(() => Array(n).fill(-1));  // Correct

  return helper(m, n, 1, 1, memo);
};

const helper = (m, n, r, c, memo) => {
  console.log("r:", r, ", c:", c);

  if (r === m-1 && c === n-1) return 1;
  if (r > m-1 || c > n-1) return 0;

  console.log("memo[r][c]:", memo[r][c]);
  if (memo[r][c] === -1) {
    const pathsDown = helper(m, n, r + 1, c, memo);
    const pathsRight = helper(m, n, r, c + 1, memo);

    memo[r][c] = pathsRight + pathsDown;
  }

  return memo[r][c];
};*/

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Approach #3: DP Bottom up tabular
// - Note:
// -- The author, mateatomico, uses a 1- base array instead of a 0-base one.
//    (starting index is (1,1))
/*const uniquePaths = (m, n) => {

  // Pre-fill dp[][] w/ 1
  // - Here I am just filling up the whole matrix for convenience. I could
  //   have just implemented filling up right-most column and bottom row, but
  //   that would require more code
  let dp = Array(m+1).fill().map(() => Array(n+1).fill(1));
  //dp.map(arr => console.log(arr));

  // Starting from (m-1, n-1), ending at (1,1)
  for(let r = m - 1; r > 0; r--){
    for(let c = n - 1; c > 0; c--){
      // (r,c) 的結果來自它的下一步的和 - 向右和向下
      // --> 比較 ln-142, Method #3, 1-Unique_Paths-Recursive_to_DP-daniel_stoian.js
      dp[r][c] = dp[r + 1][c] + dp[r][c + 1];
      console.log("dp[", r, "][", c, "]:", dp[r][c]);
    }
  }
  return dp[1][1];  // 注意
};*/

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Approach #4: DP Space Optimized (there might be even further space optimization!!)
//
// - 解釋: Method #4, 1-Unique_Paths-Recursive_to_DP-daniel_stoian.js
const uniquePaths = (m, n) => {
  const dp = new Array(n + 1).fill(1);

  for(let r = m - 1; r > 0; r--){
    for(let c = n - 1; c > 0; c--){
      // We'll only need "col v+1" to calc the values of "col v"
      dp[c] += dp[c + 1];
    }
  }
  return dp[1];  // 注意
};


//---------- Testing ----------

let m = 3, n = 2;  // Output:3
//let m = 3, n = 3;  // Output: 6
//let m = 23, n = 12;  // Output: 193536720

let res = uniquePaths(m, n);
console.log("\nres:", res);
