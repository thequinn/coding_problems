

Best and Simplest to understand verswion:

  1-Unique_Paths-Recursive_to_DP-BEST.py

//======================================================




思路方法 3 個:

(a) Focus on methods in 1.

(b) Understand the two main diff approaches in 1. & 2.

1. (r,c) 的結果來自它的上一步的和: 上 + 左

      dp[r][c] = dp[r-1][c] + dp[r][c-1];

2. (r,c) 的結果來自它的下一步的和 - 向右和向下:

      dp[r][c] = dp[r+1][c] + dp[r][c+1];


//======================================================

-0. 排列组合數學公式

//------------------------------------------------------

-1. (r,c) 的結果來自它的上一步的和: 上 + 左

      dp[r][c] = dp[r-1][c] + dp[r][c-1];

  - File Name:
      1-Unique_Paths-Recursive_to_DP-daniel_stoian.js

//------------------------------------------------------

-2. (r,c) 的結果來自它的下一步的和 - 向右和向下:

      dp[r][c] = dp[r+1][c] + dp[r][c+1];

  - File Name:
      2-Unique_Path-Recursive-直接思路-QN.js
      2-Unique_Path-Recursive-直接思路-mateatomico.js

-2-1. 用來比較兩個 Recursive 寫法, 差別只在於 Decides whether to do a recursive
  call before or after a new recursive stack is built

  - Approach #1:
      if (x+1 < m) { ... }
      if (y+1 < n) { ... }

  - Approach #2-A,B
      if(r > m || c > n)    return 0;

  - File Name:
      2-1-Unique_Path-Recursive_to_DP-直接思路-QN_vs_mateatomico.js


//------------------------------------------------------



