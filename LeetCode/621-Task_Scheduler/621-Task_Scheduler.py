'''
621. Task Scheduler

Video + Code:
    https://www.youtube.com/watch?v=s8p8ukTyA2I


// - - - - - - - - - - - - - - - - //

Note for the confusing part of the problem:
- there is a non-negative integer n that represents the cooldown period between two same tasks (the same letter in the array), that is that there must be at least n units of time between any two same tasks.

Example 1:
Input: tasks = ["A","A","A","B","B","B"], n = 2
Output: 8

Explanation:
A -> B -> idle -> A -> B -> idle -> A -> B
There is at least 2 units of time between any two same tasks.

QN's Explanation:
n = 2 means there're 2 units of cooldown periods time that needs to be in b/t 2 same tasks.  So for "A -> B -> idle -> A", there is a task B and an idle time to form 2 units of cooldown time.

'''
class Solution:
    def leastInterval(self, tasks: List[str], n: int) -> int:

        # Counter: a dict subclass for counting hashable objects
        # ex. ["A","A","A","B","B","B"] => {"A": 3, "B":3}
        count = Counter(tasks)

        # Track count of each letter
        maxHeap = [-cnt for cnt in count.values()]
        print("maxHeap:", maxHeap)

        # Transform a list into a heap, in-place, in linear time.
        heapq.heapift(maxHeap)


        q = deque()  # [-cnt, time the task is available again]

        time = 0  # cur time

        # While maxHeap or q are not empty
        while maxHeap or q:
            print("maxHeap:", maxHeap)
            print("q:", q)

            time += 1
            print("time:", time)

            if maxHeap:
                print("if maxHeap:")

                # 1 count of cur letter is used
                cnt = 1 + heapq.heappop(maxHeap)
                print("cnt:", cnt)
                if cnt:
                    print("if count:")
                    q.append([cnt, time + n])
                    print("q:", q)

            # If q is not empty, and there is a task available at cur time
            if q and q[0][1] == time:

                #poppedItem = q.popleft()[0]
                #print("poppedItem:", poppedItem)
                #
                heapq.heappush(maxHeap, q.popLeft()[0])

        return time


