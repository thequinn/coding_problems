/*
63. Unique Paths II

LeetCode STD Solution:
  https://leetcode.com/problems/unique-paths-ii/solution/

- Complexity Analysis:
(1) Time Complexity: O(M×N)
-- The rectangular grid given to us is of size M×N and we process each
   cell just once.
(2) Space Complexity: O(1)
-- We are utilizing the grid as the DP array. Hence, no extra
   space.
*/

var uniquePathsWithObstacles = function(grid) {
  let m = grid.length;
  let n = grid[0].length;

  //---------- Base Case ----------//

  // If the starting cell has an obstacle, then simply return as there would
  // be no paths to the destination.
  if (grid[0][0] == 1)    return 0;

  // Number of ways of reaching the starting cell = 1.
  grid[0][0] = 1

  // Fill the first col
  for (let row = 1; row < m; row++ ) {
    // (grid[row][0] == 0 && grid[row-1][0] == 1) returns T/F
    // Use Multiplication by 1 to convert T/F to 1/0
    grid[row][0] = (grid[row][0] == 0 && grid[row-1][0] == 1) * 1;
  }
  console.log("grid:", grid);

  // Fill the first row 
  for (let col = 1; col < n; col++ ) {
    grid[0][col] = (grid[0][col] == 0 && grid[0][col-1] == 1) * 1;
  }
  console.log("grid:", grid);
 
  //---------- Recursive Case ----------//
  
  for (let row = 1; row < m; row++) {
    for (let col = 1; col < n; col++) {
      if (grid[row][col] == 0)
        grid[row][col] = grid[row-1][col] + grid[row][col-1];
      else
        grid[row][col] = 0;
    }
  }
  
  console.log("grid:", grid);
  return grid[m-1][n-1];
};

let grid = [[0, 1]];
//let grid = [[0,0,0],[0,1,0],[0,0,0]];
console.log( uniquePathsWithObstacles(grid) ) ;
