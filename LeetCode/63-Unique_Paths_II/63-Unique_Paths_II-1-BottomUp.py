"""
63. Unique Paths II

LeetCode STD Solution:
  https://leetcode.com/problems/unique-paths-ii/solution/

- Complexity Analysis:
(1) Time Complexity: O(M×N)
-- The rectangular grid given to us is of size M×N and we process each
   cell just once.
(2) Space Complexity: O(1)
-- We are utilizing the grid as the DP array. Hence, no extra
   space.

"""

class Solution(object):
  def uniquePathsWithObstacles(self, grid):
  
    """
    :type grid: List[List[int]]  
    :rtype: int
  
    """

    m = len(grid)
    n = len(grid[0])

    #---------- Base Case ----------#

    # If the starting cell has an obstacle, then simply return as there
    # would be no paths to the destination.
    if grid[0][0] == 1:
      return 0

    # Number of ways of reaching the starting cell = 1.
    grid[0][0] = 1

    # Filling the values for the first column
    # - See in LeetCode Std Solution above: 1st anime
    for i in range(1,m):
      # ==  : Comparison operators are used to compare two values
      # and : Logical operators are used to combine conditional statements
      #
      #print("grid[i][0]:", grid[i][0], ", grid[i-1][0]:", grid[i-1][0])
      grid[i][0] = int(grid[i][0] == 0 and grid[i-1][0] == 1)
      #print("grid[i][0]:", grid[i][0])

    print("grid:", grid)

    # Filling the values for the first row
    # - See in LeetCode Std Solution above: 2nd anime
    for j in range(1, n):
      grid[0][j] = int(grid[0][j] == 0 and grid[0][j-1] == 1)

    print("grid:", grid)

    #---------- Recursive Case ----------#

    # Starting from cell(1,1) fill up the values
    # No. of ways of reaching cell[i][j] = cell[i - 1][j] + cell[i][j - 1]
    # i.e. From above and left.
    for i in range(1,m):
      for j in range(1,n):
        if grid[i][j] == 0:
          grid[i][j] = grid[i-1][j] + grid[i][j-1]
        else:
          grid[i][j] = 0

    print("grid:", grid)
    
    # Return value stored in rightmost bottommost cell. That is the destination
    return grid[m-1][n-1]

grid = [[0,1]]
#grid = [[0,0,0],[0,1,0],[0,0,0]]
sol = Solution()
print (sol.uniquePathsWithObstacles(grid))
