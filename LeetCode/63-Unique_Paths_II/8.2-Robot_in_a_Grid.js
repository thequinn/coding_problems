/*
Robot in a Grid

Note:
- See better solutions @ /GitBucket/LeetCode/
    62-UniquePaths.js
    63-UniquePaths_II.js
*/


var findPaths = function(grid) {
  var paths = [];
  var endRow = grid.length - 1;
  var endCol = grid[0].length - 1;

  var recurse = function(row, col, currPath) {
    console.log("\nrow:", row, ", col:", col);
    console.log("currPath:", currPath);
    
    // Base Case
    if (row === endRow && col === endCol) {
      console.log("===>Base Case");
      
      // 結論：Array.concat() and ... Spread operator do the same thing
      //
      // (1) 注意 How Array.concat() works:
      // - Take [row, col] inside another [] and add to end of currPath[]
      //
      //let tmp = currPath.concat( [[row, col]] );  // Same as ln-32
      //
      // (2) 注意 How ... Spread syntax works:
      // - Expand an iterable "in place" ==> NOTHING is returned !!!
      // - The code below: In [], expand the elems from currPath[], and the  
      //   add [row, col] as an elem at the end.
      //
      //let tmp = [...currPath, [row, col] ];  // Same as ln-26

      //paths.push( tmp );
      

      // Combining ln-36 & ln-39, they are equal to:
      paths.push( [...currPath, [row, col]] );
    } 
    // Recursive Case
    else if (row <= endRow && col <= endCol) {
      console.log("===>Recursive Case");
      
      // Can move down?
      if (row < endRow && grid[row+1][col] !== 'x') {
        // 法一：
        //recurse(row + 1, col, currPath.concat( [[row, col]] ) );
        
        // 法二：
        let tmp = [...currPath, [row, col]];
        //console.log("Move down-----tmp:", tmp, "row:", row, ", col:", col);
        recurse(row + 1, col, tmp);*/
        
        // 法三：Don't reassign currPath b/c it's used in next if statement!
        //recurse(row + 1, col, currPath = [...currPath, [row, col]] );
      }
      // Can move right?
      if (col < endCol && grid[row][col+1] !== 'x') {
        // 法一：
        //recurse(row, col + 1, currPath.concat([[row, col]]));
        
        // 法二：
        let tmp = [...currPath, [row, col]];
        //console.log("Move right----tmp:", tmp, "row:", row, ", col:", col);
        recurse(row, col + 1, tmp);

        // 法三：OK to reassign currPath
        //recurse(row, col + 1, currPath = [...currPath, [row, col]]);
      }
    }
  };

  recurse(0, 0, []);
  return paths;
};

/* TEST */

var grid = [
  ['0', '0', '0', '0'],
  ['0', 'x', '0', 'x'],
  ['x', '0', '0', '0'],
];

console.log("\nre:", findPaths(grid));
