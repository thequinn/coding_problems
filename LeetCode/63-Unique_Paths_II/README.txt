This problem is the same as:  CtCI 8.2 Robot in a Grid 


解法：

- Recursion (No DP)
  CtCI 8.2-Robot_in_a_Grid.js

- Bottom-Up 
  63-Unique_Paths_II-1-BottomUp.js
  63-Unique_Paths_II-1-BottomUp.py

- Bottom-Up Optimized
  63-Unique_Paths_II-1-BottomUp_Optimized.java

