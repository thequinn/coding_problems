'''
658. Find K Closest Elements


未....

- Approach #2: Two Ptrs
    https://leetcode.com/problems/find-k-closest-elements/solutions/3314784/658-solution-with-step-by-step-explanation/

'''

'''
Approach #1-2: Binary Search => QN: Approach #1-1 衍生 this approach

注意!!!
- 以下思路內容證明這算法, 但整個回想較複雜. 把這算法當公式背起來!

Video:
    https://www.youtube.com/watch?v=bhPQq7vqLEA
Article:
    https://leetcode.com/problems/find-k-closest-elements/solutions/106426/JavaC++Python-Binary-Search-O(log(N-K)-+-K)/

思路:
Assume A[mid] ~ A[mid + k] is sliding window:
=> QN's understanding:


(1) x is closer to right side of the window: 
    - When x is on the right side of the window, or inside the window but closer to the right boundary, move left ptr towards the right.

  case 1: x - A[mid] > A[mid + k] - x
  -------A[mid]---------------------A[mid + k]----x------
  
  case 2: x - A[mid] > A[mid + k] - x
  -------A[mid]------------------x---A[mid + k]----------



(2) x is closer to the left side of the window: 
- When x is on the left side of the window, or inside the window but closer to the left boundary, move right ptr towards the left.

  case 1: x - A[mid] < A[mid + k] - x
  -------x----A[mid]-----------------A[mid + k]----------

  case 2: x - A[mid] < A[mid + k] - x
  -------A[mid]----x-----------------A[mid + k]----------

'''
class Solution:
    def findClosestElements(self, A, k, x):a
        
        left = 0
        right = len(A) - k  # Start the window from size=1

        # See: "Why Not adding "=" condition to while loop?" in this file.
        while left < right:
            mid = (left + right) // 2

            if x - A[mid] > A[mid + k] - x:
                left = mid + 1
            else:   
                # Not "right = mid+1" b/c this else condition is "<="
                right = mid

        return A[left : left + k]

'''
Approach #1-1: Binary Search

Explanation + Sol:
    https://leetcode.com/problems/find-k-closest-elements/solutions/915047/finally-i-understand-it-and-so-can-you/
'''
class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int):
        low, high = 0, len(arr) - k
        
        # Why Not adding "=" condition to while loop?
        # - As long as k is not given as 0 from the prob description, we 
        # expect the window size to be > 0.  
        #  (1) We expect the window size: range mid ~ (mid+k) 
        #  (2) So if low == high, window size is 0 => error!
        while low < high:
            mid = (high+low) // 2
            
            if x <= arr[mid]:      
                high = mid

            elif arr[mid + k] <= x:  
                # "+1" b/c its if condition uses arr[mid+k] to compare. 
                # It means the range is mid ~ mid+k.  But the window size 
                # we want is mid ~ mid+(k-1).
                low = mid + 1

            else:  # arr[mid] < x < arr[mid+k]

                # If x is closer to mid,similar to the 1st case, move high 
                # to mid. Else, move low to mid+1.
                middist = abs(x - arr[mid])
                midkdist = abs(x - arr[mid + k])

                if middist <= midkdist:  high = mid
                else:                    low = mid + 1
                    
        return arr[low : low + k] 


class Solution:
    def findClosestElements(self, A, k, x):
        left, right = 0, len(A) - k

        # Why Not adding "=" condition to while loop?
        # - As long as k is not given as 0 from the prob description, we 
        # expect the window size to be > 0.  
        #   - We expect the window size: range mid ~ (mid+k), but not 0
        while left < right:
            mid = (left + right) // 2

            if x <= A[mid]:        right = mid

            elif A[mid + k] < x:   left = mid + 1 

            else:  # if A[mid] < x <= A[mid+k]:
                mid_dist = abs(x - A[mid])
                mid_k_dist = abs(x - A[mid + k])

                # If x is closer to mid,similar to the 1st case, move right 
                # to mid. Else, move left to mid+1.
                if mid_dist <= mid_k_dist:  right = mid
                else:                       left = mid + 1

        return A[left : left + k]



'''
Approach #2: Two Pointers

Explanation + Sol:
    https://leetcode.com/problems/find-k-closest-elements/solutions/3314784/658-solution-with-step-by-step-explanation/


Intuition (from link above):

1. Initialize two pointers "left" and "right" to point to the 
   beginning and end of the array respectively.

2. While the difference between "right" and "left" is greater 
   than or equal to "k":

  a. If the absolute difference between "x" and the element at 
   "left" is greater than or equal to the absolute diff b/t
    "x" and the element at "right", increment "left".

  b. Else, decrement "right".

3. Return a sorted subarray of "arr" containing the elements 
   b/t "left" and "right" (inclusive).

'''
class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int):
        left, right = 0, len(arr) - 1
        
        while right - left >= k:
            if abs(x - arr[left]) > abs(x - arr[right]):
                left += 1
            else:
                right -= 1
        
        return sorted(arr[left:right+1])


// - - - - - - - - - - - - - - // 
'''
Notice:
    - QN: Not sure if the approach of using a Heap / Priority Queue is a good idea to solve the problem.  Maybe it's b/c I'm not familiar with Heap / Priority Queue.

'''


'''
Approach 3: Using Min Heap (Heap/Priority Queue)
'''
class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int):
        maxHeap = []    

        # Since the input arr is sorted, we push its 1st k elems into 
        # the heap
        for i in range(k):
            heappush(maxHeap, arr[i])       
        # print("maxHeap:", maxHeap, "\n")

        # Loop from index=k of the input arr.  
        for i in range(k, len(arr)):
            cur_dist = abs(arr[i]-x)
            min_dist = abs(maxHeap[0]-x)

            # If cur dist < min dist, replace the min dis in the heap w/ 
            # the cur dist
            if cur_dist < min_dist:
                #heappop(maxHeap)
                #heappush(maxHeap, arr[i])        
                #
                # Same as last 2 lns
                #heappushpop(maxHeap, arr[i])
            # print("maxHeap:", maxHeap)

        return sorted(maxHeap)


'''
Approach #4:  Using Python's default Min Heap as Max Heap (Heap/Priority Queue)
             => This is VERY AKWARD to understand, SKIP until 
                "maybe" after my brain is used to converting a 
                min heap to max heap.
               
    https://leetcode.com/problems/find-k-closest-elements/solutions/1145950/python-heap/
'''
class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        print("arr:", arr)
        if len(arr) < k:    return []

        heap = []
        for ndx, val in enumerate(arr):
            dist = abs(val - x)
            print("\nndx:", ndx, ", val:", val, ", dist b/t (x=", x, ", val=", val, ") = ", dist)

            if len(heap) < k:
                print("in if")
                # Push a tuple to heap
                heappush(heap, (-1 * dist, val))

            # When heap have k items, we can start considering whether to use
            # pop & push combo to replace an item closer to x
            else:
                print("in else")
                # If root of max heap > the dist, replace the root w/ the 
                # cur val in the heap
                if -1 * heap[0][0] > dist:
                    print("in nested if")
                    heappop(heap)
                    heappush(heap, (-1 * dist, val))
                    #
                    # Same as the last 2 lns
                    # heappushpop(): Push item on the heap, then pop and return
                    #                the smallest item from the heap.
                    #heappushpop(heap, (-1 * dist, val))
            print("heap:", heap)

        return sorted([val for _, val in heap])

