/*
66. Plus One

注意：
- JS can't handle very long str numbers, 
-- ex. [6,1,4,5,3,9,0,1,9,5,1,8,6,7,0,5,5,4,3]

*/

// Method #1:
// - https://leetcode.com/problems/plus-one/discuss/24297/Simple-Direct-JavaScript-Solution
//
// 思路: 
// - First, add 1 anyway. If there is a carry-over, the new digit will
//   also add 1. If the current digit is less than 9 then return the array.
//
var plusOne = function(digits) {
  for(var i = digits.length - 1; i >= 0; i--){
    digits[i]++;
    
    // Case #1:
    // - Handle mist significant bit, and possibily its neighboring bits to
    //   the left > 9
    if(digits[i] > 9)   digits[i] = 0;
    // Case #2:
    // - Handle least significant bit < 9
    else                return digits;
  }
  // Con't to handle Case #1:
  // - We only need to add carry-over(進位) once
  digits.unshift(1);
  return digits;
};

// Method #2: Improve of Method #1
// - https://leetcode.com/problems/plus-one/discuss/24150/A-few-lines-of-JavaScript
/*var plusOne = function(digits) {
  for (let i = digits.length - 1; i >= 0; i--) {
    // Case #1:
    // - Handle mist significant bit, and possibily its neighboring bits to
    //   the left > 9
    digits[i] = (digits[i] + 1) % 10;
    console.log("ln-A:", digits);

    if (digits[i]) {
      console.log("ln-B:", digits);
      return digits;
    }
  }
  digits.unshift(1);
  return digits;
};*/

// Method #3:
// - https://leetcode.com/problems/plus-one/discuss/354173/One-line-JavaScript-hook
//
// - BigInt(val):
// -- built-in object that provides a way to represent whole numbers larger
//   than 2^53 - 1, which is the largest number JS can reliably represent
//   with the Number primitive.
// -- val: May be a string or an integer.
//
var plusOne = function(digits) {
    // Wonder what "1n" do below?  See MDN BigInt Description.
    return ( BigInt(digits.join('')) + 1n ).toString().split('');
};

//let digits = [1,2,3];  // [1,2,4]

//let digits = [9];  // [1,0]
let digits = [9,9];  // [1,0,0]

//let digits = [6,1,4,5,3,9,0,1,9,5,1,8,6,7,0,5,5,4,3];
// [6,1,4,5,3,9,0,1,9,5,1,8,6,7,0,5,5,4,4]

console.log("--------------\nresult:", plusOne(digits));
