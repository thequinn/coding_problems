'''
67. Add Binary

'''

'''
Approach 2-1: Bit Manipulation

LC Premium Sol:
- https://leetcode.com/problems/add-binary/solution/

'''
class Solution:
  def addBinary(self, a: str, b: str) -> str:
    x, y = int(a, 2), int(b, 2)

    while y:
      answer = x ^ y
      carry = (x & y) << 1
      x, y = answer, carry

    # [2:] b/c bin(x) returns 0b100
    return bin(x)[2:]

'''
Approach 2-1: Bit Manipulation --> More concise Python syntax
'''
class Solution:
    def addBinary(self, a, b) -> str:
        x, y = int(a, 2), int(b, 2)
        while y:
            x, y = x ^ y, (x & y) << 1
        return bin(x)[2:]

