'''
680. Valid Palindrome II

'''

'''
Approach #1: Brute Force [Time Limit Exceeded]

LC Std Sol:
- https://leetcode.com/problems/valid-palindrome-ii/solution/

思路:
- For each index i in the given string, let's remove that character, then check if the resulting string is a palindrome. If it is, (or if the original string was a palindrome), then we'll return true

Time Complexity:  O(N^2) ==> 未...Asking in comment part.....
Space Complexity: O(N)

'''
class Solution(object):
    def validPalindrome(self, s):
        for i in xrange(len(s)):
            t = s[:i] + s[i+1:]
            if t == t[::-1]: return True

        return s == s[::-1]

'''
Appraoch 2: Two Pointers

Explanation + Sol Code in Java: --> Code is a bit diff from the next link
- https://www.youtube.com/watch?v=hvI-rJyG4ik

Explanation + Sol Code in Python:
- https://leetcode.com/problems/valid-palindrome-ii/discuss/632647/Python-Readable-and-Intuitive-Solution-(Generalizable-to-n-deletes)
- 思路:
-- Simply checking if character at left matches corresponding right until it doesn't. At that point we have a choice of either deleting the left or right character. If either returns Palindrome, we return true. To generalize this to more than one deletes, we can simply replace the flag "deleted" to be a counter initialized to how many characters we are allowed to delete and stop allowing for recursive calls when it reaches 0.

'''
class Solution:

  def validPalindrome(self, s: str) -> bool:

    def verify(s, left, right, deleted):
      while left < right:
        if s[left] != s[right]:
          if deleted:
            return False
          else:
            return verify(s, left+1, right, True) or verify(s, left, right-1, True)
        else:
          left += 1
          right -= 1
      return True

    return verify(s, 0, len(s)-1, False)
