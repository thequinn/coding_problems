'''
69. Sqrt(x)

https://leetcode.com/discuss/general-discussion/786126/python-powerful-ultimate-binary-search-template-solved-many-problems

思路:
- Search for "minimal" val of "mid" that satisfies the condition mid^2 > x, 
  then mid-1 is the answer to the question.
  - When the while loop exits, "left" needs to -1 to be the correct val 
    that matches "mid".  ex. Input: x = 8; Output: 2

Notice:
- Set right = x + 1 instead of right = x to deal with special input cases
  like x = 0 and x = 1.
'''
class Solution:
    def mySqrt(self, x: int) -> int:

        def condition(val):
           return val * val > x

        l, r = 0, x+1
        while l < r:
            mid = (l + r) // 2

            if condition(mid):  r = mid
            else:               l = mid + 1
        return l - 1
