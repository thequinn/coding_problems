'''
思路:
- Create a dict of words and its freq
- Create an iteratble obj that has tuples of (count, word)
- Sort the iterable based on words freq and then alphabetical order
'''

'''
Approach #1-1: Using a Heap
    https://leetcode.com/problems/top-k-frequent-words/solutions/1657648/simple-4-lines-using-heap-with-explanation/
'''
class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        # have a dict of word and its freq
        counts = collections.Counter(words)

        #- get a array wchich will have a tuple of word and count
        #- Python's default is min heap.  So we negate var count to store
        #  largest num in the root.
        heap = [(-count, word) for word, count in counts.items()]

        # creating heap in place, by deualt it will sort by freq then word
        heapq.heapify(heap)

        return [heapq.heappop(heap)[1] for _ in range(k)]


'''
Approach #1-2: Using a Heap
https://leetcode.com/problems/top-k-frequent-words/solutions/533190/python-3-heap/
'''
class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        dct = {}
        for word in words:
            dct[word] = dct.get(word, 0) + 1

        hp = [] # heap
        for key, val in dct.items():
            # negate var v to store largest val in Python's default min heap
            heapq.heappush(hp, (-val, key))

        res = []
        for _ in range(k):
            # poppedItem = heapq.heappop(hp)[1]
            # print("poppedItem:", poppedItem)
            # res.append(poppedItem)
            #
            # Same as the last 3 lns above
            res.append(heapq.heappop(hp)[1])

        return res


'''
Approach #2: Using sorted() and specify its param key to decide the order
             - sorted() returns a sorted list of the specified iterable object.

    https://leetcode.com/problems/top-k-frequent-words/solutions/449970/python-use-dictionary-and-lambda-beat-94/
'''
class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:

        dict = {}
        for word in words:
            dict[word] = dict.get(word, 0) + 1
        #
        # Same as last 2 lns, but not sure why it doesn't work
        #dict = { w: dict.get(w, 0) + 1 for w in words}

        # - arg "key": A Function to execute to decide the order.
        #
        # - Use lambda to sort the dict
        #   1. -dict[word]: in desc order based on words freq. W/o the
        #         negative sign, the default is in aesc order.
        #   2. x: if freq are equal, sort dict in asc order based on
        #         lexicographical order
        res = sorted(dict, key = lambda x: (-dict[x], x))

        return res[:k]


