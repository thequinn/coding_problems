/*
7. Reverse Integer


Note:
- Assume we are dealing with an envir which could only store integers w/in the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
  ||
  =====> valid range: 最小： Math.pow(2, 31) * -1: -2147483648
                      最大： Math.pow(2, 31) - 1 : +2147483647
*/

// Method #1: for Non-JS b/c only JS has built-in Array.reverse()
// - Use 1st link to learn 思路, 2nd link for code
// (1) https://leetcode.com/problems/reverse-integer/solution/
// (2) https://leetcode.com/problems/reverse-integer/discuss/235701/JavaScript-Solution-4-Different-ways
//
var reverse = function(x) {
  const THRESHOLD =  Math.pow(2, 31);
  let rev = 0;
  let y = Math.abs(x);

  while (y != 0) {
    let pop = y % 10;
    y = x / 10 | 0;   // 技巧: ex. 1.2 | 0 = 1
    //console.log("y:", y);

    rev = rev * 10 + pop;
  }

  if ((rev < THRESHOLD * -1) || (rev > THRESHOLD - 1)) {
    return 0;
  }
  return x < 0 ? rev * -1 : rev;
}

// Method #2: Suitable for JS b/c JS has nuilt-in Array.reverse()
// - https://leetcode.com/problems/reverse-integer/discuss/4185/My-simple-solution-in-Javascript
//
var reverse = function(x) {
  var sign = (x>0) ? 1 : -1;
  x=Math.abs(x);

  var str=x.toString().split("").reverse().join("");
  var result=sign * Number(str);

  // 注意:
  // - Takes care of int overflow b/c the prob specifies the input has
  //   32-bit signed integer range: [−231,  231 − 1]
  if (result < Math.pow(2, 31) * -1 || result > Math.pow(2, 31) - 1)
  //if (result < -2147483648 || result > 2147483647)  // Same as last ln
    return 0;
  else
    return result;
};


//let x = 120;              // 21
//console.log(reverse(x));
let x1 = 123;               // 321
console.log(reverse(x1));
let x2 = -123;              // -321
console.log(reverse(x2));
