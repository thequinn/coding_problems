'''
7. Reverse Integer
'''

class Solution:
  def reverse(self, x):
    # 注意: "-x" as param when input is negative case
    return -1 * self.reverseUtil(-x) if x < 0 else self.reverseUtil(x)

  def reverseUtil(self, x):
    result = 0
    while x != 0:
      digit = x % 10
      result = result * 10 + digit
      x = int(x / 10)

    # The prob specifies input is 32-bit signed integer range: [−231,  231 − 1]
    # - overflow, ex. 9646324351 always becomes 900000000
    return 0 if result > pow(2, 31) - 1 or result < -pow(2, 31) else result
