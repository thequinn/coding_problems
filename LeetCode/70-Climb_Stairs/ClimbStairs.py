'''
70. Climbing Stairs

- NeetCode: =>必看! 教如何拆解 TOp-down & Bottom-up DP problems
    https://www.youtube.com/watch?v=Y0lT9Fck7qI

- (以下 Code 來自這個連結)
    https://leetcode.com/problems/climbing-stairs/solutions/3708750/4-method-s-beat-s-100-c-java-python-beginner-friendly/




Top-down DP vs Bottom-up DP
- Top-Down DP: Recursion + Memoization
  - The recursion is DFS
- Bottom-up DP: Iteration + Tabulation
  - The iteration runs backwards (end of arr to front)

Top-down DP & Bottom-up DP actually solve a problem in the same direction
- The 2 DP approaches solve a problem in opposite directions.  This involves the directions you break down a problem into sub-problems. 
    - ex. See 思路 of the 2 DP approaches in 746. Min Cost Climbing Stairs
- However, their code both solve the smallest sub-problems first, meaning solving from bottom-up of a tree.  (Tracing the 2 versions of code will show.)    

'''


# - - - - - - - - - - - - #  - - - - - - - - - - - - #
# 法一: (Top-Down) Basic Recursive + No DP -> TLE
'''
https://leetcode.com/problems/climbing-stairs/solutions/1531764/python-detail-explanation-3-solutions-easy-to-difficult-recursion-dictionary-dp/

Top-Down without Memoization
- Time Limit Exceeded b/c we don't cache answers

Time Complexity: O(2^n)
- It's b/c of the size of recursion tree is 2^n
- ex. when n=5, its recursion tree is:

                              (5)
                              / \
                   (4)                   (3)
                   / \                   / \
            (3)           (2)      (2)         (1)
            / \
         (2)   (1)

Using the recursive tree to generate the recursion math formula:

n=5: count = cs(4) + cs(3) = 5 + 3 = 8
n=4: count = cs(3) + cs(2) = 3 + 2 = 5
n=3: count = cs(2) + cs(1) = 2 + 1 = 3
n=2: count = 2
n=1: count = 1

It leads to the 思路: How many ways are there to reach the n-th (last) step?


Recursion - Based on the recursive tree above =>QN+Braeden Mock Interview
'''
class Solution:
    def climbStairs(self, n: int) -> int:
        if n == 1 or n == 2:    return n

        left = self.climbStairs(n-1) 
        right = self.climbStairs(n-2)
        return left + right

class Solution:
    def climbStairs(n):
        # only 1 step option is availble
        if n == 1:    return 1
        # 2 possible options: two 1-stpes or one 2-steps
        if n == 2:  return 2

        # using the recursion formula above
        return self.climbStairs(n-1) + self.climbStairs(n-2)





// = = = = = = = = = = = = = = // = = = = = = = = = = = = = = // 
# 法二: Top-Down DP: Recursive Memo

'''
Inner/Nested func

'''
class Solution:
    def climbStairs(self, n: int) -> int:
        memo = {1:1, 2:2}

        def dfs(n, memo):  # DFS: Depth First Search

            '''
            - You attempt to access dp[n] directly, which will raise a KeyError
              if n is not yet in dp.
            - To avoid this, use if n in dp: instead of if dp[n]:, so that it 
              checks if n is a key in dp before accessing it.              
            '''
            #if dp[n]: => WRONG!! See explanation above
            #
            if n in memo:   
                return memo[n]

            left = dfs(n-1, memo)
            right = dfs(n-2, memo)
            return left + right

        return helper(n, memo)

'''
No inner func - Using a class var

Class var
- defined directly in the class scope, outside of any methods. This means it is shared among all instances of the Solution class. All instances of Solution will access and modify the same memo dictionary.
'''
class Solution:

    # class var
    memo = {1:1, 2:2} 

    def climbStairs(self, n: int) -> int:
        if n not in self.memo:
            self.memo[n] = self.climbStairs(n-1) + self.climbStairs(n-2)
        return self.memo[n]

'''
No inner func - using an instance var

Instance var
- defined inside the __init__ constructor method and would be unique to each instance of the class.
'''
class Solution:

    def __init__(self):
        # Instance var
        self.dic = {1:1, 2:2}

    def climbStairs(self, n):
        if n not in self.dic:
            self.dic[n] = self.climbStairs(n-1) + self.climbStairs(n-2)
        return self.dic[n]






// = = = = = = = = = = = = = = // = = = = = = = = = = = = = = // 
'''
法三: Bottom-up DP: Iterative Tabulation: 
               ||
               = = =>>>TRUE Dynamic Programming Approach!


Top-down DP:
- This tree and math breakdown are from the Recursion (DP) appraoch above.
->思路: How many ways are there to reach the n-th (last) step? 
        => We think this way bc it's Top-down approach

                              (5)
                              / \
                   (4)                   (3)
                   / \                   / \
            (3)           (2)      (2)         (1)
            / \
         (2)   (1)

Using the recursive tree to generate the recursion math formula:

n=5: count = cs(4) + cs(3) = 5 + 3 = 8
n=4: count = cs(3) + cs(2) = 3 + 2 = 5
n=3: count = cs(2) + cs(1) = 2 + 1 = 3
n=2: count = 2
n=1: count = 1


Bottom-up DP:
- For the Bottom-up DP appraoch, we store the distinct ways in a dynamic table.
->思路: To reach the n-th (last) step, it depends on the previous 2 steps
        => We think this way bc it's Bottom-up approach

dp = [ cs(1), 
       cs(2), 
       cs(3) = cs(2) + cs(1), 
       cs(4) = cs(3) + cs(2),
       cs(5) = cs(4) + cs(3),
       ...
       cs(n) = cs(n-1) + cs(n-2)
     ]

dp = [ 1, 2, 3, 5, dp(i-1)+dp(i-2]) ]
return dp[n]


'''

# # 法三-1:Bottom-up, O(n) space
class Solution:
    def climbStairs(self, n):
        # Edge Cases
        if n == 1 or n == 2:  return n
        
        dp = [0] * n
        dp[0], dp[1] = 1, 2

        for i in range(2, n):
            dp[i] = dp[i-1] + dp[i-2]
        
        return dp[n-1]


# 法四: Bottom-up, constant space
def climbStairs3(self, n):
    if n == 1 or n == 2:  return n

    a, b = 1, 2

    for i in range(2, n):
        tmp = b
        b = a+b
        a = tmp
    return b




#res = climbStairs(3)  # 3
res = climbStairs(5) # 8
print("res:", res)
