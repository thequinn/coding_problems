/*
70. Climbing Stairs

Task:
- Each time you can either climb 1 or 2 steps.
  How many distinct ways can you climb to the top of n steps of a stair case?
- Note: Given n will be a positive integer.

Solution:
- https://leetcode.com/problems/climbing-stairs/discuss/163347/Python-3000DP-or-tm
*/

/*-------------------------------------------------------------
  Recursion -  No Dynamic Programming Involved.
  -------------------------------------------------------------*/
// Method #1-3 from 70-Climb_Stairs-1-Recursion.js
var climbStairs = function(n) {
  if (n === 1)    return 1;
  if (n === 2)    return 2;
  return climbStairs(n - 1) + climbStairs(n - 2);
}


/*---------------------------------------------------------------
  Top-Down with Memoization
  - Top-Down的思路，我们从n层向下考虑, 从大化小，思路就和DFS Tree
    中的分制一样
  ---------------------------------------------------------------*/
var climbStairs = function(n) {
  var memo = [];  // Using Array (Same as next ln)
  //var memo = {};    // Using JS Object = Python List/Hash Table

  // Same as ln-39~40.  But better use ln-39~40 b/c helper() is a recursive
  // func and it works better w/ base cases.
  //memo[1] = 1,  memo[2] = 2;

  return helper(n, memo);
};
var helper = function(n, memo) {
  if (n === 1)    return 1;   // Same as ln-33
  if (n === 2)    return 2;

  if (memo[n] > 0) {
    return memo[n];
  }

  // 这里就是Top-Down的思路，从大化小，思路就和DFS Binary Tree中的分制一样
  memo[n] = helper(n-1, memo) + helper(n-2, memo);
  return memo[n];
};


/*----------------------------------------------------------------------
  Bottom-Up
  - Bottom-Up 的思路则相反。我们不从n层向下考虑，而是解决我们每一层向上的
    子问题。当我们有了第1层和第2层的base case，我们则可以直接从base case
    向上推得第3层，第4层以及第n层的答案

  - 技巧: Think from a recursion tree such as below:

                              (5)
                              / \
                   (4)                   (3)
                   / \                   / \
            (3)           (2)      (2)         (1)
            / \
         (2)   (1)

  ----------------------------------------------------------------------*/
var climbStairs = function(n) {
  var dp = [];  // Using Array (Same as next ln)
  //var dp = {};    // Using JS Object = Python Dictionary/Hash/Map

  //dp[0] = 1;  // Needed if the problem input allows non-positive int
  dp[1] = 1;  dp[2] = 2;

  // Fill the rest of dp[] from i = 3 to n
  for (let i = 3; i <= n; i++) {
    dp[i] = dp[i-1] + dp[i-2];
  }
  return dp[n];
}


/*----------------------------------------------------------------------
  Bottom-Up 优化 (Constant Space) -> Similar to Fibonacci Growth
  -  其实在每一次更新当前的数的时候，我们最终返回的是最后一次更新的数，
     而我们的dependency是n-1 和n-2中的值，我们根本不需要一个数组去储存，
     直接用两个函数即可。

  - 技巧: Think from a recursion tree such as below:

              .   ↑
              .   ↑
              .
            (3) = (2) + (1)
            (2) = (1) + (0)

                             (5)
                              / \
                   (4)                   (3)
                   / \                   / \
            (3)           (2)      (2)         (1)
            / \
         (2)   (1)

  ----------------------------------------------------------------------*/
var climbStairs = function(n) {
  if (n === 1)   return 1;
  if (n === 2)   return 2;

  let a = 1, b = 2;
  let c;  // cur elem needed to be computed
  for (let i = 3; i <= n; i++) {
    c = a + b;
    a = b;  b = c;
  }
  return c;
};

// This case is not needed if n is NOT given as a positive int
//console.log(climbStairs(0));  // 1

console.log(climbStairs(1));  // 1
console.log(climbStairs(2));  // 2
//console.log(climbStairs(3));  // 3
console.log(climbStairs(4));  // 5

