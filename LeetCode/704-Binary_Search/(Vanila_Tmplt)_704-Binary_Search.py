


'''
'''
Iterative Approach
'''

'''
https://leetcode.com/problems/binary-search/solutions/1884068/python-javascript-easy-solution-with-very-clear-explanation/?orderBy=most_votes
'''
import math
class Solution:
    def search(self, nums: List[int], target: int) -> int:

        left, right = 0, len(nums)-1

        while left <= right:
            mid = (left + right) // 2    # "//" is Floor division
            #mid = math.floor( (left + right) / 2 )  # Same as above

            if nums[mid] == target:   return mid
            elif nums[mid] < target:  left=mid+1
            else:                     right=mid-1

        return -1

# - - - - - - - - - - - # - - - - - - - - - - - # - - - - - - - - - - - # 
'''
Recursive Approach:  
'''
    
'''
https://leetcode.com/problems/binary-search/solutions/1743624/python-simple-python-solution-using-binary-search-with-two-approach/
'''
class Solution:
	def search(self, nums: List[int], target: int) -> int:

		def BinarySearch(array, start, end, target):
			if start <= end:  # Check base case
				mid = (start + end) // 2 # "//" is Floor division
 
				if array[mid] == target:    return mid
				elif array[mid] < target:   start = mid + 1
				else:                       end = mid - 1

				return BinarySearch(nums, start, end, target)

			return -1

		start, end = 0, len(nums)-1
		result = BinarySearch(nums, start, end, target) 
		return result

'''
https://www.geeksforgeeks.org/python-program-for-binary-search/
'''
class Solution:
    def search(self, nums: List[int], target: int) -> int:

        def binary_search(nums, low, high, target):
            if high >= low:  # Check base case
                mid = (high + low) // 2
 
                if nums[mid] == target:  return mid
                elif nums[mid] > target:
                    return binary_search(nums, low, mid - 1, target)
                else:
                    return binary_search(nums, mid + 1, high, target)
            else:  # Element is not present in the array
                return -1

        low, high = 0, len(nums) - 1
        return binary_search(nums, low, high, target) 
