/*
704. Binary Search

Next Level:
- LeetCode 287. Find the Duplicate Number
*/

// Method #1: Recursive Version
let search = function (arr, target) {
  return helper(arr, target, 0, arr.length - 1);
}
let helper = function(arr, target, start, end) {
    // Base Condtion
    if (start > end) return false;

    let mid = Math.floor((start + end) / 2);
    //console.log("mid:", mid);

    // Recursive Condition
    if (arr[mid] === target)
      return true;
    if (arr[mid] > target)
      return helper(arr, target, start, mid-1);
    else
      return helper(arr, target, mid+1, end);
}

// Iterative Version
// https://leetcode.com/problems/binary-search/solutions/1884068/python-javascript-easy-solution-with-very-clear-explanation/?orderBy=most_votes
//
const search = (nums, target) => {
  let left = 0;
  let right = nums.length - 1;

  while (left <= right) {

    // Multiply and Divide using Bit Shifting:
    // (1) X * 2 = 1 bit shift left
    // (2) X / 2 = 1 bit shift right
    //
    //let mid = (left + right) / 2 | 0;  // Logical OR w/ 0 to convert to int
    let mid = (left + right) >> 1;
    //let mid = Math.floor((left + right) / 2);

    if (nums[mid] === target) { return mid; }
    if (nums[mid] > target)   { right = mid - 1; }
    else { left = mid + 1; }
  }
  return -1;
};

let arr = [-1,0,3,5,9,12];
console.log( search(arr, 9) );
