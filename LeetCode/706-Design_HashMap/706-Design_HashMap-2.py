'''
706. Design HashMap

'''

'''
Approach 1: Using Array --> Not good enough for interviews

Sol Code:
- https://leetcode.com/problems/design-hashmap/discuss/169659/Python-straightforward-solution-one-line-for-each-function
'''
class MyHashMap(object):

    def __init__(self):
        # - 1000001: better to be a prime number, less collision
        # - Using list[] b/c it's not supposed to use a built-in hash table
        #   libraries, i.e. dictionary{}
        self.table = [-1] * 1000001

    def put(self, key, value):
        self.table[key] = value


    def get(self, key):
        return self.table[key]


    def remove(self, key):
        self.table[key] = -1


'''
Approach 2: Using Hash with Chaining
- using just arrays, direct access table
- using linked list for chaining

Sol Code:
- https://leetcode.com/problems/design-hashmap/discuss/185347/Hash-with-Chaining-Python
'''
class ListNode:
    def __init__(self, key, val):
        self.pair = (key, val)
        self.next = None

class MyHashMap:
    def __init__(self):
        # m: number of slots in HashTable
        # Better use a prime #, less collision
        self.m = 1001;
        # h: array that stores all key-val pairs
        self.h = [None] * self.m

    def put(self, key, value):
        index = key % self.m
        if self.h[index] == None:
            self.h[index] = ListNode(key, value)
        else:
            cur = self.h[index]
            while True:
                # If key found, update its val
                if cur.pair[0] == key:
                    cur.pair = (key, value)
                    return
                # If key not found and reach the end of the linked list, break
                if cur.next == None: break
                cur = cur.next
            # Add the new key-val pair as a new node to the linked list
            cur.next = ListNode(key, value)

    def get(self, key):
        index = key % self.m
        cur = self.h[index]
        while cur:
            if cur.pair[0] == key:
                return cur.pair[1]
            cur = cur.next  # bool(None) returns False
        return -1

    def remove(self, key):
        index = key % self.m
        cur = prev = self.h[index]
        # If cur not exist
        if not cur: return
        # If cur's key matches the item to be removed
        if cur.pair[0] == key:
            # Remove cur by re-assigning it to its next
            self.h[index] = cur.next
        else:
            cur = cur.next
            while cur:
                if cur.pair[0] == key:
                    prev.next = cur.next
                    break
                else:
                    cur, prev = cur.next, prev.next
