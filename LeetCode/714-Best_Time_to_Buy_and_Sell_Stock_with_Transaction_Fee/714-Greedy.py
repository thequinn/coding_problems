'''
Greedy Approach => Less intuitive, but fast runtime

思路:

(1) https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/1112529/java-intuitive-o-n-solution-greedy/

  Info collected:
  1) From problem statement:
     - We want to min transaction fees and max profits
  2) From previous "Buy & Sell Stock" problems, we know that for unlimited 
     transactions, the max profit we can attain is by 
        a) keeping track of the min price and 
        b) greedilly selling when we reach a profitable state

  Details for 2) 
  a) If prices[i] < min price, then it's optimal to update min price, since 
     less profit can be made by buying a more expensive stock
  b) when prices[i] - fee > min, then we can make a profit even with the 
     transaction fee, then update the min price to this current price.


(2) Good drawing showing how this greedy alg works:
https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/1684685/python-greedy-and-dp/

- Confusing part:

    minPrice = curPrice - fee  # ln-71

  Scenerio:
  - If the next day's price con't to raise, greedy approach will sell again. However, We want to combine the 2 transaction into 1 to avoid incurring the fee twice.  

    cash += curPrice - (minPrice + fee)  # ln-69
    
    Since the code last ln pays for the fee in every iter, deducting the fee to adjust for the right minPrice (avoid incurring fee) for the next iter is needed.


ex. index  =  0, 1, 2
    prices = [1, 4, 5], k=2

Up to this pt, profit=0

i=1: minPrice=1, curPrice=4
    profit = 0 + 4 - (1 + 2) = 1
    minPrice = curPrice -2 = 4-2 = 2

i=2: minPrice=2, curPrice=5
    profit = 1 + 5 - (2 + 2)) = 2

'''
class Solution:
    def maxProfit(self, prices: int, fee: int) -> int: 
        minPrice = prices[0]
        cash = 0

        for curPrice in prices:
            # Keep track of min price
            if curPrice < minPrice:
                minPrice = curPrice
            
            # If you can make profit, take it
            elif curPrice > (minPrice + fee):
                cash += curPrice - (minPrice + fee)
                # See: 思路 (2)
                minPrice = curPrice - fee
            
        return cash


