'''
714. Best Time to Buy and Sell Stock with Transaction Fee


(1) Most Complete Solutions:
- Brute Force Recursion -> Memoization -> Tabulation -> Space Optimisation -> Two Integer Variables

https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/3668244/detailed-explanation-fastest-cakewalk-solution-for-everyone/

- Note: (2)'s video explains the concept.  But its sol code can be simplified to (1)'s code.  2 lines in if-else case of (2) can be simplified to the ln below from (1):
    
        int move_on = maxProfitHelper(idx + 1, canBuy, fee, prices);


(2) Video Explanation: Recursion-> Memoization-> Tabulation

https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/3670803/video-explanation-recursion-memoization-tabulation-c-simple-and-easy/


(3) From Top down DP to Bottom up DP - O(1) space 
    https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/1439821/python-from-top-down-dp-to-bottom-up-dp-o-1-space-easy-to-understand/


'''


'''
Approach #1: Pure Recursion - TLE 

Time complexity: O(2^n)
- Since for every index we can only explore 2 paths, either buy-move_on or 
  sell-move_on. And we are doing this on every index starting from 0 to n - 1.

Space complexity: O(n)
- Since at any point of time the maximum depth of recursion will be the size of the array (idx goes from 0 to n - 1)

'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        
        def dfs(idx: int, canBuy: bool):
            # Base Case:
            if idx == len(prices):  return 0

            # 4 Recursive Cases: buy - move_on (= not buy)
            #                    sell - move_on (= not sell)
            buy, sell = -1, -1
            if canBuy:
                buy = -prices[idx] + dfs(idx+1, not canBuy)
            else:
                sell = prices[idx] - fee + dfs(idx+1, not canBuy)
            move_on = dfs(idx+1, canBuy)

            # Find max profit from them
            return max(buy, sell, move_on)

        return dfs(0, True)

# - - - - - - - - - - - - # - - - - - - - - - - - - # 
'''
Approach #2: Memoization (Top-Down DP Approach)
- Since we have two parameter where idx goes from 0 to n and canBuy switches between true (1) and false (0) we define dp of size n + 1 by 2.
- If we use a dict, no need to set the size.

Time complexity: O(n)
Space complexity: O(n)
- n space for the dp cache and n space for the recursion stack
'''
# Using an object {} as cache
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        memo = {}
        
        def dfs(idx: int, canBuy: bool):
            # Base Case:
            if idx == len(prices):  return 0

            # Check if result is already cached/memoized.
            key = (idx, canBuy)
            if key in memo:   return memo[key]

            # 4 Recursive Cases: buy - move_on (= not buy)
            #                    sell - move_on (= not sell)
            buy, sell = -1, -1
            if canBuy:
                buy = -prices[idx] + dfs(idx+1, not canBuy)
            else:
                sell = prices[idx] -fee + dfs(idx+1, not canBuy)
            move_on = dfs(idx+1, canBuy)

            # Find max profit from them and cache it
            memo[key] = max(buy, sell, move_on)
            return memo[key]

        return dfs(0, True)

# Using a 2D array [][] as cache
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)

        # Create a 2D list
        memo = [[0] * 2 for i in range(n+1)]   # 法一
        #memo = [[0] * 2] * (n+1)              # 法二

        def dfs(idx: int, canBuy: bool):
            # Base Case:
            if idx == len(prices):  return 0

            # Check if result is already cached/memoized.
            if memo[idx][canBuy] > 0: 
                return memo[idx][canBuy]

            # 4 Recursive Cases: buy - move_on (= not buy)
            #                    sell - move_on (= not sell)
            buy, sell = -1, -1
            if canBuy:
                buy = -prices[idx] + dfs(idx+1, not canBuy)
            else:
                sell = prices[idx] -fee + dfs(idx+1, not canBuy)
            move_on = dfs(idx+1, canBuy)

            # Find max profit from them and cache it
            memo[idx][canBuy] = max(buy, sell, move_on)
            return memo[idx][canBuy]

        return dfs(0, True)

# - - - - - - - - - - - - # - - - - - - - - - - - - # 
'''
Approach #3: Tabulation (Bottom-Up DP Approach)
- In memoization we go top-down. In tabulation we go bottom-up. So in tabulation we first calculate the base case and then go in the backward direction to reach the initial state.
- # Recursion is used to go thr all possible combos.  In Tabulation (Bottom-up DP) sol, we use a nested for loop to do the same - iter thr dp[i][canBuy]

Time complexity: O(2n)
- Since at max for every index we will caluclate the result twice once for canBuy = false (0) and for canBuy = true (1)

Space complexity: O(n)
- n  space for the dp obj.
- 2n space for the dp arr.
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)

        # Create a 2D list
        dp = [[0, 0] for _ in range(n + 1)]    # 法一
        #dp = [[0] * 2 for _ in range(n + 1)]  # 法二
        #dp = [[0] * 2] * (n+1)                # 法三

        # - Iter backwards bc it's a bottom-up approach
        # - The nested for loop iter thr all possible combos of dp[i][canBuy].  
        # - Think of it as a tree structure: "(2) Video Explanation" on the top
        #
        for i in range(n-1, -1, -1):
        # WRONG! 
        # - n-1 is used in range() in when iter backward
        # - -1 is used in list, lst[-1] to mean last elem.
        #for i in range(-1, -1, -1):
            for canBuy in range(2):

                # Reset the 2 vars in every iter.  So We get correct max profit
                buy, sell = -1, -1 
                
                if canBuy:
                    buy = -prices[i] + dp[i+1][not canBuy]
                else:
                    sell = prices[i] - fee + dp[i+1][not canBuy]
                move_on = dp[i+1][canBuy]

                # Notes: This ln is here bc the 2D indices need to be cached
                # Find max profit from them and cache the result
                dp[i][canBuy] = max(buy, sell, move_on)
        
        # i=0 and canBuy=T is the initial state
        return dp[0][1]


'''
Approach #4: Space Optimized Tabulation
- In Approach #3, observe the inner loop body, 
    
    dp[idx][...] always depend on dp[idx+1][...]

  So instead of storing the entire dp[][] tbl, just keep track of 2 arrays cur[] and next[].

    dp[ idx   ][ canBuy = T/F ]  -->  cur[ canBuy = T/F ] 
    dp[ idx+1 ][ canBuy = T/F ]  -->  nxt[ canBuy = T/F ] 


Time complexity: O(2n)
- Since at max for every index we will caluclate the result twice once for canBuy = false (0) and for canBuy = true (1)

Space complexity: O(1)
- We are just taking 4 spaces in total, 2 for cur[] and 2 for nxt[]
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)
        cur, nxt = [0,0], [0,0]

        for i in range(n-1, -1, -1):
            for canBuy in range(2):
                buy, sell = -1, -1

                if canBuy:
                    buy = -prices[i] + nxt[not canBuy]
                else: 
                    sell = prices[i] - fee + nxt[not canBuy]
                move_on = nxt[canBuy]
                
                # Find max profit from them and cache it
                cur[canBuy]  = max(buy, sell, move_on)

            # Save cur[] to nxt[] for next iteration
            nxt = cur

        # See cmt in Approach #4
        # - We use nxt[] here bc in ln-216 cur is assigned to nxt
        # - Index=1 here bc init state is canBuy=T.  Approach #4 is bottom-up DP
        return nxt[1]


'''
Approach #5: Getting Rid of the inner loop
- In Approach #4, the inner loop is extra - it is just iterating over 2 values. This is adding an additional time complxity. Why not remove it completely and do both the operation in the same loop. 

Time complexity: O(n)
- We are just traversing prices array once

Space complexity: O(1)
- We just take 4 spaces, 2 for cur and 2 for nxt
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)
        cur, nxt = [0,0], [0,0]

        for i in range(n-1, -1, -1):

            # For canBuy = T: get max from buy and move_on
            #
            # cur[1] is dp[i][T]
            # nxt[0] is dp[i+1][F]
            cur[1] = max( -prices[i] + nxt[0], nxt[1] )
            # For canBuy = F: get max from sell and move_on
            cur[0] = max ( prices[i] - fee + nxt[1], nxt[0])

            # Save cur to nxt for next iteration
            nxt = cur

        return nxt[1]

'''
Approach #6: Code w/ 4 Integers
- In Approach #6, cur[] and nxt[] each holds 2 vals.  We can use 4 ints instead.  


Time complexity: O(n)
- Traverse prices array once.

Space complexity: O(1)
- 2 integer spaces. 
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)
        c0, c1, n0, n1 = 0, 0, 0, 0

        for i in range(n-1, -1, -1):
            c1 = max(-prices[i] + n0, n1)
            c0 = max(prices[i] - fee + n1, n0)
            n0 = c0
            n1 = c1
        return n1
 
'''
Approach #7: Code w/ 2 Integers
- In Approach #6, since the space consumption rematins the same, let's optimize more.
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)
        c0, c1, n0, n1 = 0, 0, 0, 0

        for i in range(n-1, -1, -1):
            n1 = max(-prices[i] + n0, n1)
            n0 = max(prices[i] - fee + n1, n0)
        return n1
 

