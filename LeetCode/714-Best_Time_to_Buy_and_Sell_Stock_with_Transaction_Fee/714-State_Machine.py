'''
714. Best Time to Buy and Sell Stock with Transaction Fee

Approach: State Machine
'''


'''
Approach #1-1: State Machine + DP 

Explanation + Sol Code:
  https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/108871/2-solutions-2-states-dp-solutions-clear-explanation/

思路:

Note: Draw a state machine first

Given any day i, its max profit status boils down to one of the two status:

(1) buy status:
buy[i] represents the max profit at day i in buy status, given that the last action you took is a buy action at day K, where K<=i. And you have the right to sell at day i+1, or do nothing.
(2) sell status:
sell[i] represents the max profit at day i in sell status, given that the last action you took is a sell action at day K, where K<=i. And you have the right to buy at day i+1, or do nothing.

Let's walk through from base case.

Base case:
We can start from buy status, which means we buy stock at day 0.
buy[0]=-prices[0];
Or we can start from sell status, which means we sell stock at day 0.
Given that we don't have any stock at hand in day 0, we set sell status to be 0.
sell[0]=0;

Status transformation:
At day i, we may buy stock (from previous sell status) or do nothing (from previous buy status):
buy[i] = Math.max(buy[i - 1], sell[i - 1] - prices[i]);
Or
At day i, we may sell stock (from previous buy status) or keep holding (from previous sell status):
sell[i] = Math.max(sell[i - 1], buy[i - 1] + prices[i]);

Finally:
We will return sell[last_day] as our result, which represents the max profit at the last day, given that you took sell action at any day before the last day.

We can apply transaction fee at either buy status or sell status.



Solution I -- pay the fee when buying the stock => Use Solution II instead
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)
        if n <= 1:  return 0
        
        buy = [0] * n
        sell = [0] * n
        buy[0] = -prices[0] - fee

        for i in range(1, n):
            # keep the same as day i-1, or buy from sell status at day i-1
            buy[i] = max( buy[i-1], sell[i-1] - prices[i] - fee )
            # keep the same as day i-1, or sell from buy status at day i-1
            sell[i] = max( sell[i-1], buy[i-1] + prices[i] )
        
        return sell[-1]
'''
Solution II -- pay the fee when selling the stock:
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        n = len(prices)
        if n <= 1:  return 0
        
        buy = [0] * n
        sell = [0] * n
        buy[0] = -prices[0]

        for i in range(1, n):
            # keep the same as day i-1, or buy from sell status at day i-1
            buy[i] = max( buy[i-1], sell[i-1] - prices[i] )
            # keep the same as day i-1, or sell from buy status at day i-1
            sell[i] = max( sell[i-1], buy[i-1] + prices[i] - fee )
        
        return sell[-1]



'''Approach #1-2: DP => Simplified from Solution II, Approach #1-1
https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/3667440/beats-100-c-java-python-beginner-friendly/

Intuition
- It's easy to simplify Approach #1-2 by keeping track of 
    the minimum cost to buy a stock at each day, and 
    the maximum profit that can be achieved by selling the stock at each day.
  => Basically just to track the last buy and sell 

Time Complexity: O(n)
Space Complexity: O(1)
'''
class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        buy = float('-inf')
        sell = 0

        for price in prices:
            buy = max(buy, sell - price)
            sell = max(sell, buy + price - fee)

        return sell




