'''
72. Edit Distance



Levenshtein Distance Algorithm

- Levenshtein distance is a string metric for measuring the difference between two sequences. The Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other.

-In approximate string matching, the objective is to find matches for short strings in many longer texts, in situations where a small number of differences is to be expected. The short strings could come from a dictionary, for instance. Here, one of the strings is typically short, while the other is arbitrarily long. This has a wide range of applications, for instance, spell checkers, correction systems for optical character recognition, and software to assist natural-language translation based on translation memory.



Explanation for Levenshtein Distance Algorithm:
- https://www.youtube.com/watch?v=Dd_NgYVOdLk  => Best video found!!
==> See my paper notes


Explanation + Python Code:
    1. Recursion
    2. Top-Down Memoization (Recursion)
    3. Bottom Up DP (Iteration) ===  Levenshtein Distance Algorithm
- https://leetcode.com/problems/edit-distance/solutions/159295/python-solutions-and-intuition/

'''


'''
Approach #1: Recursion => TLE 

思路 for Recursion:
- We'll either match the currently indexed characters in both strings, or mismatch. In the first case, we don't incur any penalty, and we can continue to compare the rest of the strings by recursing on the rest of both strings. In the case of a mismatch, we either insert, delete, or replace. To recap:

 Base case: 
    word1 = "" or word2 = "" => return length of other string
 Recursive cases: 
    word1[0] == word2[0] => recurse on word1[1:] and word2[1:]
    word1[0] != word2[0] => recurse by inserting, deleting, or replacing


 - Visualization:
  
   md("horse", "hello")
	 md("orse", "ello")
		md("orse", "llo")
			md("orse", "lo")
			md("rse", "llo") <- 
			md("rse", "lo")
		md("rse", "ello")
			md("rse", "llo") <-
			md("se", "ello")
			md("se", "llo") <<-
		md("rse", "llo")
			md("rse", "llo") <-
			md("se", "llo") <<-
			md("se", "lo")
'''
class Solution:
    def minDistance(self, word1, word2):
        if not word1 and not word2:
            return 0

        if not word1:
            return len(word2)
        if not word2:
            return len(word1)

        if word1[0] == word2[0]:
            return self.minDistance(word1[1:], word2[1:])
        #else:
        insert = 1 + self.minDistance(word1, word2[1:])
        delete = 1 + self.minDistance(word1[1:], word2)
        replace = 1 + self.minDistance(word1[1:], word2[1:])
        
        return min(insert, replace, delete)

'''
Approach #2: Top-Down Memoization (Recursion)
'''
class Solution:
    def minDistance(self, word1, word2, i, j, memo):
        if i == len(word1) and j == len(word2):
            return 0

        if i == len(word1):
            return len(word2) - j
        if j == len(word2):
            return len(word1) - i

        if (i, j) not in memo:
            if word1[i] == word2[j]:
                ans = self.minDistance2(word1, word2, i + 1, j + 1, memo)
            else: 
                insert = 1 + self.minDistance2(word1, word2, i, j + 1, memo)
                delete = 1 + self.minDistance2(word1, word2, i + 1, j, memo)
                replace = 1 + self.minDistance2(word1, word2, i + 1, j + 1, memo)
                ans = min(insert, delete, replace)
            memo[(i, j)] = ans
        return memo[(i, j)]a

'''
Approach #3: Bottom Up DP (Iteration) ===  Levenshtein Distance Algorithm

'''
class Solution:
    def minDistance(self, word1, word2):
        m = len(word1)
        n = len(word2)
        table = [[0] * (n + 1) for _ in range(m + 1)]

        for i in range(m + 1):
            table[i][0] = i
        for j in range(n + 1):
            table[0][j] = j

        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if word1[i - 1] == word2[j - 1]:
                    table[i][j] = table[i - 1][j - 1]
                else:
                    table[i][j] = 1 + min(table[i - 1][j], table[i][j - 1], table[i - 1][j - 1])
        return table[-1][-1]
