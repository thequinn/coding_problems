/*
724. Find Pivot Index

思路:
- ex. [1,7,3,6,5,6], index=3 is the pivot

  The prob wants the sum on the left of the pivot is equal to the sum on the left.  It means don't don't take index=3 into consideration.
  So in each iteration at curr elem, when (sum - nums[i] - leftSum) == leftSum, we found the pivot.
*/

var pivotIndex = function(nums) {
  let leftSum = 0;
  let sum = nums.reduce((accu, curr) => accu+curr, 0);
  for (let i = 0; i < nums.length; i++) {
    if ((sum - nums[i] - leftSum) == leftSum)
      return i;
    leftSum += nums[i];
  }
  return -1;
};

//-----Test Case-----
let nums = [1,7,3,6,5,6]; // 3
//let nums = [1,2,3]; // -1
