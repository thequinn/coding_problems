"""
726. Number of Atoms

- LeetCode Std Solution
  https:#leetcode.com/problems/number-of-atoms/solution/
"""

import re
import collections

"""
# Approach #1: Recursion

- Write a function parse that parses the formula from index i, returning a map count from names to multiplicities (the number of times that name is recorded).

- We will put i in global state: our parse function increments i throughout any future calls to parse.

(1) When we see a '(', we will parse whatever is inside the brackets (up to the closing ending bracket) and add it to our count.

(2) Otherwise, we should see an uppercase character: we will parse the rest of the letters to get the name, and add that (plus the multiplicity if there is one.)

(3) At the end, if there is a final multiplicity (representing the multiplicity of a bracketed sequence), we'll multiply our answer by this.
"""
class Solution(object):

  def countOfAtoms(self, formula):

    def parse():
      N = len(formula)

      # collections.Counter()
      # - A dict subclass for counting hashable objects
      count = collections.Counter()
      print("count: ", count)

      """ Char: NOT ")"  =========================================== """
      while (self.i < N and formula[self.i] != ')'):

        if (formula[self.i] == '('):
          print("\n\"(\" ------------------")
          print("self.i: ", self.i)

          self.i += 1
          # items():
          # - returns a view object. The view object contains the key-value pairs
          #   of the dictionary
          for name, v in parse().items():
            print("parse().item() is called in for-loop-------------")
            # Add to end of count{}
            # +=: same as extend() in Mutable Sequence Types.(append vs extend)
            count[name] += v
            print("count[", name, "]: ", count[name])
            print("count: ", count)

        else:
          print("\nelse ------------------")
         
          # Parse elem name
          i_start = self.i
          print("self.i: ", self.i, "\t, i_start: ", i_start)
          # If the elem name has lowercase letter
          self.i += 1
          while (self.i < N and formula[self.i].islower()):
            self.i += 1

          name = formula[i_start: self.i] # Specify range of index in list/array
          print("name: ", name)

          # Parse multiplicity which has more than 1 digit
          i_start = self.i
          print("self.i: ", self.i, "\t, i_start: ", i_start)
          while (self.i < N and formula[self.i].isdigit()):
              self.i += 1
          print("self.i: ", self.i)
         
          multiplicity = int(formula[i_start: self.i] or 1)
          print("formula[i_start: self.i]: (", formula[i_start: self.i], ")")
          print("multiplicity: ", multiplicity)

          # Add to end of count{}
          # +=: same as extend() in Mutable Sequence Types.(append vs extend)
          count[name] += multiplicity
          print("count: ", count)

      """ Char: ")" =========================================== """
      print("\n\")\" ------------------")
      self.i += 1
      
      # Parse multiplicity which has more than 1 digit
      i_start = self.i
      print("self.i: ", self.i, "\t, i_start: ", i_start)
      while (self.i < N and formula[self.i].isdigit()):
        self.i += 1
      print("self.i: ", self.i)

      # If no multiplicity, set it to 1
      multiplicity = int(formula[i_start: self.i] or 1)
      print("multiplicity: ", multiplicity)
     
      # Update elem's inside "()" to count{}
      for name in count:
        count[name] *= multiplicity
      print("count: ", count)

      return count


    # Not belong to the inner func of countOfAtoms()
    self.i = 0
    ans = []    # []: list/array
    count = parse()
    print("\n############################################### \ncount: ", count)

    for name in sorted(count):
      ans.append(name)
      multiplicity = count[name]
      if multiplicity > 1:
        ans.append(str(multiplicity))
    return "".join(ans)


formula = "K4(ON(SO3)2)2"   # "K4N2O14S4", {'K': 4, 'N': 2, 'O': 14, 'S': 4}
#formula = "Mg(OH)2"         # "H2MgO2", {'H': 2, 'Mg': 1, 'O': 2}
#formula = "Fe16H2O"
#formula = "FeH2O"
#formula = "H2O"             # "H2O", {'H': 2, 'O': 1}
#formula = "H"

sol = Solution()
re = sol.countOfAtoms(formula)
print("\nre: ", re)
