"""
726. Number of Atoms

- LeetCode Std Solution
  https:#leetcode.com/problems/number-of-atoms/solution/
"""

import re
import collections

# Approach #3: Regular Expressions
# - Whenever parsing is involved, we can use regular expressions, a language 
#   for defining patterns in text.
#
class Solution(object):

  def countOfAtoms(self, formula):

    # The regex can be broken down by capture group "()":
    # 1. ([A-Z][a-z]*) match an uppercase character followed by any number of
    #    lowercase chars, then ((\d*)) match any number of digits.
    # OR 
    # 2. (\() match a left bracket 
    # OR 
    # 3. (\)) match a right bracket, then (\d*) match any number of digits.
    # 
    parse = re.findall(r"([A-Z][a-z]*)(\d*)|(\()|(\))(\d*)", formula)
 
    # class collections.Counter([iterable-or-mapping])
    # - A Counter is a dict subclass for counting hashable objects.
    #
    # collections.Counter()
    # - A dict subclass for counting hashable objects
    stack = [collections.Counter()]
    print("stack: ", stack)

    # m1, m2: multiplicity, which is from (\d*) in regex
    for name, m1, left_open, right_open, m2 in parse:
      print("\nname: ", name, "\tm1: ", m1, "\tleft_open: ", left_open, 
            "\tright_open: ", right_open, "\tm2: ", m2)
      
      # If we parsed a name and multiplicity ([A-Z][a-z]*)(\d*), we will add it
      # to our current count.
      if name:
        # Add a new key-val pair to stack. 
        # -1: last elem of list/array.  
        # +=: same as extend() in Mutable Sequence Types. (append vs extend)
        stack[-1][name] += int(m1 or 1)
        print("stack[-1][", name, "]: ", stack[-1][name])
        print("stack: ", stack)

      # If we parsed a left bracket, we add a new count to the stack, 
      # representing the nested depth of parentheses.
      if left_open:
        stack.append(collections.Counter())
        print("stack: ", stack)

      # If we parsed a right bracket (and possibly another multiplicity), we 
      # multiply our deepest level count, top = stack.pop(), and add those 
      # entries to our current count. 
      if right_open:
        top = stack.pop()
        print("top: ", top)
        print("stack: ", stack)
        for k in top:
          stack[-1][k] += top[k] * int(m2 or 1)
        print("stack: ", stack)

    return "".join(name + (str(stack[-1][name]) if stack[-1][name] > 1 else '')
                     for name in sorted(stack[-1]))


#formula = "K4(ON(SO3)2)2"   # "K4N2O14S4", {'K': 4, 'N': 2, 'O': 14, 'S': 4}
formula = "Mg(OH)2"         # "H2MgO2", {'H': 2, 'Mg': 1, 'O': 2}
#formula = "Fe16H2O"
#formula = "FeH2O"
#formula = "H2O"             # "H2O", {'H': 2, 'O': 1}
#formula = "H"

sol = Solution()
re = sol.countOfAtoms(formula)
print("\nre: ", re)
