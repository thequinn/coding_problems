"""
726. Number of Atoms

- LeetCode Std Solution
  https:#leetcode.com/problems/number-of-atoms/solution/
"""

import re
import collections

# Approach #2: Stack
class Solution(object):

  def countOfAtoms(self, formula):
    N = len(formula)

    # collections.Counter()
    # - A dict subclass for counting hashable objects
    stack = [collections.Counter()]
    print("stack: ", stack)

    i = 0
    while i < N:

      if formula[i] == '(':
        print("\"(\" ============")
        stack.append(collections.Counter())
        print("stack: ", stack)
        i += 1

      elif formula[i] == ')':
        print("\")\" ============")
        top = stack.pop()
        print("top: ", top)
        i += 1

        # Parse multiplicity which has more than 1 digit
        i_start = i
        print("i: ", i, "\t, i_start: ", i_start)
        while i < N and formula[i].isdigit(): 
          i += 1
        print("i: ", i)

        # If no multiplicity, set it to 1
        multiplicity = int(formula[i_start: i] or 1)
        print("multiplicity: ", multiplicity);

        # items():
        # - returns a view object. The view object contains the key-value pairs
        #   of the dictionary
        for name, v in top.items():
          # Push (+=) the key-val pair (name: multiplicity) to stack.
          # "+=" is same as extend() in Mutable Sequence Types.
          stack[-1][name] += v * multiplicity
        print("stack: ", stack)

      else:
        print("else ============")

        # Parse elem name
        i_start = i
        print("i: ", i, "\t, i_start: ", i_start)
        # If the elem name has lowercase letter
        i += 1  
        while i < N and formula[i].islower(): 
          i += 1
        print("i: ", i)

        name = formula[i_start: i]  # Specify range of index in list/array
        print("name: ", name)

        # Parse multiplicity which has more than 1 digit
        i_start = i
        print("i: ", i, "\t, i_start: ", i_start)
        while i < N and formula[i].isdigit(): 
          i += 1
        print("i: ", i)

        # If no multiplicity, set it to 1
        multiplicity = int(formula[i_start: i] or 1)
        print("multiplicity: ", multiplicity)

        # Push (+=) the key-val pair (name: multiplicity) to stack.
        # - "+=" is same as extend()
        stack[-1][name] += multiplicity
        print("stack: ", stack)

    return "".join(name + (str(stack[-1][name]) if stack[-1][name] > 1 else '')
                 for name in sorted(stack[-1]))


#formula = "K4(ON(SO3)2)2"   # "K4N2O14S4", {'K': 4, 'N': 2, 'O': 14, 'S': 4}
formula = "Mg(OH)2"         # "H2MgO2", {'H': 2, 'Mg': 1, 'O': 2}
#formula = "Fe16H2O"
#formula = "FeH2O"
#formula = "H2O"             # "H2O", {'H': 2, 'O': 1}
#formula = "H"

sol = Solution()
re = sol.countOfAtoms(formula)
print("\nre: ", re)
