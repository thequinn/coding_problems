726. Number of Atoms

- LeetCode Std Solution
  https:#leetcode.com/problems/number-of-atoms/solution/

(1) Appraoch 1:  Recursion. ==> Hard to understand !!

/BitBucket/LeetCode/726-Number_of_Atoms-Recursion.py
- Don't understand this solution:


(2) Approach 2: Stack ==> Easy
/BitBucket/LeetCode/726-Number_of_Atoms-Stack.py

(3) Approach 3: Regex ==> Easy & Best Practice
/BitBucket/LeetCode/726-Number_of_Atoms-Regex.py
