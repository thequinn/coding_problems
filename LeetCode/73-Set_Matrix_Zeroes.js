/*
73. Set Matrix Zeroes
*/

// Approach #1: Brute Force - Additional Memory Approach
// - https://leetcode.com/problems/set-matrix-zeroes/solution/
//
// - Time complexity: O(m * n), m and n are num of rows and cols
// - Space complexity: O(m + n), 2 sets
//
/*var setZeroes = function(matrix) {
  let set_rows = new Set();
  let set_cols = new Set();

  // Iterate over matrix[][], if an entry at [i, j] is 0, record it to sets
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (matrix[i][j] === 0) {
        set_rows.add(i);
        set_cols.add(j);
      }
    }
  }

  // Iterate over matrix[][] again and using the rows and cols sets, update
  // the elements
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (set_rows.has(i) || set_cols.has(j)) {
        matrix[i][j] = 0;
      }
    }
  }
};*/

// This method is a waste of time.  Skip it!
// Approach #2: Brute Force - O(1) space by manipulating the original array
// - https://leetcode.com/problems/set-matrix-zeroes/solution/
//
// - 思路:
// -- (1) Iterate over the original array and if we find an entry, say cell[i][j] to be 0, then we iterate over row i and column j separately and set all the non zero elements to some high negative dummy value (say -1000000).
// -- (2) we iterate over the original matrix and if we find an entry to be equal to the high negative value, we set the value in the cell to 0.
//
// - Time complexity: O( (m * n) * (m + n))
// -- This approach avoids using extra space, but is very inefficient since in worst case for every cell we might have to zero out its corresponding row and column. Thus for all (M×N) cells zeroing out (M+N) cells.
// 
// - Space complexity: O()
//
/*var setZeroes = function(matrix) {
  let R =  matrix.length;
  let C =  matrix[0].length;
  const MODIFIED = -1000000;

  // Iterate over matrix[][], if an entry at [i, j] is 0, record it to sets
  for (let i = 0; i < R; i++) {
    for (let j = 0; j < C; j++) {
      if (matrix[i][j] === 0) {

        // We modify the corresponding rows and column elements in place.
        // Note, we only change the non zeroes to MODIFIED
        for (let k = 0; k < C; k++) {
          if (matrix[i][k] != 0) {
            matrix[i][k] = MODIFIED;
          }
        }
        for (let k = 0; k < R; k++) {
          if (matrix[k][j] != 0) {
            matrix[k][j] = MODIFIED;
          }
        }

      }
    }
  }

  // Iterate over matrix[][] again and using the rows and cols sets, update
  // the elements
  for (let i = 0; i < R; i++) {
    for (let j = 0; j < C; j++) {
      if (matrix[i][j] == MODIFIED) {
        matrix[i][j] = 0;
      }
    }
  }
};*/

// Approach #3: O(1) Space, Efficient Solution
// - https://leetcode.com/problems/set-matrix-zeroes/discuss/26129/Javascript-solution-sharing
//
var setZeroes = function(matrix) {
  var solution = [];
  
  for(var i=0; i < matrix.length; ++i){ // step 1
    for(var j=0; j < matrix[i].length; ++j){
      if(matrix[i][j]===0){
        solution.push(i);
        solution.push(j);
      }
    }
  }
  console.log("solution:", solution);

  
  for(var k=0; k < solution.length; k+=2){ // step 2

    console.log("\nsolution[",k,"]:", solution[k]);
    console.log("matrix[",solution[k],"]:", matrix[solution[k]]);
    // 知道row, 所以row不要動,col動
    for(j=0; j < matrix[solution[k]].length; ++j){
      matrix[solution[k]][j] = 0;
    }
    console.log("matrix[",solution[k],"]:", matrix[solution[k]]);

    console.log("\nsolution[",k+1,"]:", solution[k+1]);
    // 知道col, 所以col不要動,row動
    for(i=0; i < matrix.length; ++i){
      matrix[i][solution[k+1]] = 0;
      console.log("matrix[",i,"][",solution[k+1],"]:", matrix[i][solution[k+1]]);
    }
  }
};

let matrix = [[1,1,1],[1,0,1],[1,1,1]];
setZeroes(matrix);
console.log("\nmatrix:", matrix);
