'''
733. Flood Fill -> DFS Sol
https://leetcode.com/problems/flood-fill/

'''


# - - - - - - - - - - # - - - - - - - - - - # - - - - - - - - - - #
# Using a BFS Iterative

'''
Method #1
- https://leetcode.com/problems/flood-fill/discuss/187701/Python-nice-and-clean-DFS-and-BFS-solutions
'''
class Solution:
  def floodFill(self, img: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:
    oldColor, m, n = img[sr][sc], len(img), len(img[0])
    if oldColor != newColor:
      q = collections.deque([(sr, sc)])
      while q:
        i, j = q.popleft()
        img[i][j] = newColor
        for x, y in ((i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)):
          if 0 <= x < m and 0 <= y < n and img[x][y] == oldColor:
            q.append((x, y))
    return img
