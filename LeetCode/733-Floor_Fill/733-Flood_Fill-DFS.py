'''
733. Flood Fill -> DFS Sol
https://leetcode.com/problems/flood-fill/

Video Explaination & Sol Code:
- https://www.youtube.com/watch?v=RwozX--B_Xs&t=324s

'''


# - - - - - - - - - - # - - - - - - - - - - # - - - - - - - - - - #
# Using a DFS Recursion

'''
Method #1-1: This is Modified from Method #1-2 by QN
'''
class Solution:
    def floodFill(self, image, sr, sc, newColor):
        def dfs(i, j):
            # Base Case:
            if image[i][j] == newColor:  return image

            # Recursive Case:
            image[i][j] = newColor
            for x, y in ((i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)):
                if 0 <= x < m and 0 <= y < n and image[x][y] == oldColor:
                    dfs(x, y)

        oldColor, m, n = image[sr][sc], len(image), len(image[0])
        dfs(sr, sc)
        return image
'''
Method #1-2: -> See ln-44 comment:
                # Moving the Base case in ln-19 Method #1-1 is probably more
                # efficient
- https://leetcode.com/problems/flood-fill/discuss/187701/Python-nice-and-clean-DFS-and-BFS-solutions
'''
class Solution:
    def floodFill(self, image, sr, sc, newColor):
        def dfs(i, j):
            image[i][j] = newColor
            for x, y in ((i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)):
                if 0 <= x < m and 0 <= y < n and image[x][y] == oldColor:
                    dfs(x, y)

        oldColor, m, n = image[sr][sc], len(image), len(image[0])
        # Moving the Base case in ln-19 Method #1-1 is probably more efficient
        if oldColor != newColor:
            dfs(sr, sc)
        return image

'''
Method #2
- https://leetcode.com/problems/flood-fill/discuss/626424/Python-sol-by-DFS-and-BFS-w-Comment
'''
class Solution:
  def floodFill(self, img: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:

    m, n = len(img), len(img[0])

    def dfs( r, c, src_color, new_color):
      # Reject for invalid coordination, repeated traversal, or different color
      if r < 0 or c < 0 or r >= m or c >= n or img[r][c] == new_color or img[r][c] != src_color:
        return

      img[r][c] = new_color # update color

      # DFS to 4-connected neighbors
      dfs( r-1, c, src_color, new_color )
      dfs( r+1, c, src_color, new_color )
      dfs( r, c-1, src_color, new_color )
      dfs( r, c+1, src_color, new_color )


    dfs(sr, sc, src_color = img[sr][sc], new_color = newColor)
    return img


# - - - - - - - - - - # - - - - - - - - - - # - - - - - - - - - - #
# Using a Stack Iterative

'''
Sol from commenter, constantstranger:
- https://leetcode.com/problems/flood-fill/discuss/187701/Python-nice-and-clean-DFS-and-BFS-solutions
'''
class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, color: int) -> List[List[int]]:
        if color == image[sr][sc]:
            return image
        scolor, stack = image[sr][sc], [(sr, sc)]
        while stack:
            r, c = stack.pop()
            image[r][c] = color
            stack += (
                (r + i, c + j)
                 for i, j in ((-1,0),(1,0),(0,1),(0,-1)) if
                     0 <= r + i < len(image) and
                     0 <= c + j < len(image[0]) and
                     image[r + i][c + j] == scolor )
        return image
