'''
735. Asteroid Collision

Simplest & Easiest to Understand Explanation & Sol: => Must watch the vid!
- https://leetcode.com/problems/asteroid-collision/solutions/3790225/ex-amazon-explains-a-solution-with-a-video-python-javascript-java-and-c/

思路:
- To cause a collosion, a previous asteroid has to be moving towards the right (+), and a current asteroid has to be moving towards the left (-)
- Note: We save pre asteroids in a stack


非常重要!!
- When we use a stack - a type of list, pop() will be used.  We always have to check if the stack/list is empty.  It's bc if you call pop() on an empty list, it will raise an IndexError.

'''

# The following 2 version have same code, but one with print() for testing

# Testing code
'''
Use Testcase: [5,-10,12,-5]
'''
class Solution:
    def asteroidCollision(self, asteroids: List[int]) -> List[int]:
        res = []

        for a in asteroids:
            print("\na:", a)
            while res and a < 0 < res[-1]:
                print("Right after while")
                # If cur asteroid's weight > pre asteroid's weight, 
                if -a > res[-1]:
                    print("if")
                    res.pop()
                    continue
                    # If cur and pre asteroids have same weight
                elif -a == res[-1]:
                    print("elif")
                    res.pop()
                print("Before break")
                break
            else:
                res.append(a)
                print("res:", res)

            print("After while, inside for")

        return res


# Clean Code
class Solution:
    def asteroidCollision(self, asteroids: List[int]) -> List[int]:
        
        stack = []

        for a in asteroids:

            # To cause a collosion, a previous asteroid has to be moving 
            # towards the right (+), and a current asteroid has to be
            # moving towards the left (-).

            while stack and a < 0 < stack[-1]:

                # Keep checking the stack for collision when cur asteroid's 
                # weight is larger than the pre asetroid on the top of stack
                if -a > stack[-1]: # -a here is the same as abs(a)
                    stack.pop()
                    continue

                # If cur and pre asteroids have same weight, break the while
                # loop and go to the else portion of the while-else to 
                # append cur asteroid.
                elif -a == stack[-1]:
                    stack.pop()

                break 
            
            else: # This is a while-else statement
                stack.append(a)

        return stack
