'''
739. Daily Temperatures


非常重要!!
- When we use a stack - a type of list, pop() will be used.  We always have to check if the stack/list is empty.  It's bc if you call pop() on an empty list, it will raise an IndexError.
'''

'''
Monitonic Stack Approach: => tmp: abbr as temperature


Monotonic Stack:
- a stack whose elements are monotonically increasing or decreasing.
  - For this prob, we need a "Decreasing" Monotonic Stack to track # of daysto find the next tmp higher.


Vid Explanation & Code:
-Easy to understand
    Cracking FANG  : https://www.youtube.com/watch?v=qJGwESM6b7Y
- OK after watching Cracking FANG's vid
    NeetCode: https://www.youtube.com/watch?v=cTBiBSnjO3c

思路: @3:10 link above
1. Iterate over each index and val pair (cur_day, cur_tmp) in tmps[]
2. While we cur tmp > what's at the top of the stack
    - Pop from the stack, and 
    - Calc the diff in the days for the tmp at the top of the stack and the 
      cur day, which will tell us how many days it took for that previous 
      day until we saw a larger tmp.
3. Always append the cur_day & cur_tmp pair to the stack in each iter for 
   future eval.
'''
class Solution:
    def dailyTemperatures(self, tmps: List[int]) -> List[int]:

        stack = []
        ans = [0] * len(tmps)

        for cur_day, cur_tmp in enumerate(tmps):

            # "while stack": while stack is not empty
            # - Python doesn't allow access to an empty stack
            while stack and cur_tmp > stack[-1][1]:

                pre_day, pre_tmp = stack.pop()
                ans[pre_day] = cur_day - pre_day

            # Always add the cur_day & cur_tmp to stack for future eval
            stack.append( (cur_day, cur_tmp) )

        return ans
         


