'''
746. Min Cost Climbing Stairs


(0) Must see NeetCode's tree drawing explanation:
    @4:00 & @10:40 https://www.youtube.com/watch?v=ktmzAZWkEZ0


(1) A Beginner's Guide on DP validation & How to come up with a Recursive solution [Python 3]
    https://leetcode.com/problems/min-cost-climbing-stairs/solutions/773865/a-beginner-s-guide-on-dp-validation-how-to-come-up-with-a-recursive-solution-python-3/


(2) 4 ways | Step by step from Recursion -> top down DP -> bottom up DP -> fine tuning

    https://leetcode.com/problems/min-cost-climbing-stairs/solutions/476388/4-ways-step-by-step-from-recursion-top-down-dp-bottom-up-dp-fine-tuning/


(3) 94% FASTER 97% Space Efficient Python solutions Different approaches
    https://leetcode.com/problems/min-cost-climbing-stairs/solutions/2260902/94-faster-97-space-efficient-python-solutions-different-approaches/

'''


# - - - - - - - - - - - - # - - - - - - - - - - - - #
'''
Top-down DP vs Bottom-up DP
- Top-Down DP: Recursion + Memoization
  - The recursion is DFS
- Bottom-up DP: Iteration + Tabulation
  - The iteration runs backwards (end of arr to front)

Top-down DP & Bottom-up DP actually solve a problem in the same direction
- The 2 DP approaches solve a problem in opposite directions.  This involves the directions you break down a problem into sub-problems. 
    - ex. See 思路 of the 2 DP approaches in 746. Min Cost Climbing Stairs
- However, their code both solve the smallest sub-problems first, meaning solving from bottom-up of a tree.  (Tracing the 2 versions of code will show.)  

'''
# - - - - - - - - - - - - # - - - - - - - - - - - - #




'''
(1) A Beginner's Guide on DP validation & How to come up with a Recursive solution [Python 3]
    https://leetcode.com/problems/min-cost-climbing-stairs/solutions/773865/a-beginner-s-guide-on-dp-validation-how-to-come-up-with-a-recursive-solution-python-3/


..............................................................

Section 1: DP Validation

DP Criteria => Suitable for both Top-down Recursive DP & Bottom-up Iterative DP:- To check if we can apply DP, we look at 2 distinct properties of DP:

Note: - 746- explains Top-down DP 思路 
      - 746-3-DP.py explains Bottom-up DP 思路


In its essence, DP is just doing brute force recursion smartly, so we should first check if the question can be done recursively (whether we can break it down into smaller subproblems)

Then, to check if we can apply DP, we look at 2 distinct properties of DP:

    1. Overlapping subproblems
    - Does finding the solution require us to calculate the same subproblem multiple times?
        
        -> See NeetCode's tree analysis: 
            @4:00 https://www.youtube.com/watch?v=ktmzAZWkEZ0

    2. Optimal Substructure Property
    - Can the optimal solution be constructed from the optimal solutions of its subproblems? (Are the subproblems independent from each other?
        
        -> See Thought Process below for Top-down subprob dependency
        -> See NeetCode's drawing explanation for Bottonm-up subprob dependency
            @10:40 https://www.youtube.com/watch?v=ktmzAZWkEZ0



>Thought Process

We can use recursion to solve the problem because we can break it into smaller subproblems:

    - If we want to know the min cost to reach stair #n, It will be tremendously helpful to know the min cost to reach step #n-1 and step #n-2 (because we can reach step #n in one step from them)



Now that we know we can use recursion, should we use DP? Let's check if the question contains the two properties that I mentioned above

    1. Overlapping Subproblems:
        - minCost(n) will be the short form of min cost to reach stair #n
        - We know that minCost(n) depends on minCost(n-1) and minCost(n-2), then by the same logic minCost(n-1) depends on minCost(n-2) and minCost(n-3).
        - Notice that we have the same expression twice (bolded), this shows that we have overlapping subproblems!

    2. Optimal Substructure Property
        - let's take cost = [4,2,3] for example, what's minCost(3)?
        - To reach stair #3, we have to start from either stair #1 or #2, since we want to minimize cost so we choose stair #2 (2 < 4) and climb one step to reach #3
        - To reach stair #2, since we can choose to start at #2 (step with index 1), it's already the optimal solution.
        - We see that the optimal solution of one subproblem leads up to the optimal solution of the problem, therefore this property is present!


After we have both properties fulfilled, we know that we can tackle this problem using DP with confidence!


..............................................................

Section 2: How to come up with a recursive solution?

The key to solving any DP problem is to identify a recursive solution, then we can identify the overlapping subproblems and then apply memoization (top-down) or tabulation (bottom-up) methods to optimize based of the recursive solution



Below is a strategy we can use to find a recursive solution

    1. Identify what variable is changing between each subproblem
    2. Create the recursive function based on #1 and clearly defines its meaning
    3. State transition formula (Find a formula that connects a problem to its subproblems)
    4. Define the base cases of recursion



Walkthrough

    1. Changing Variables:
        - From the problem, we can see that there are 2 changing variables
        - index of the current stair (stair #n)
        - minimum cost to reach the current stair

    2. Create function:
        - We can associate the 2 variables together by creating a function minCost(n), where n is the index of the current stair, and minCost(n) represents the min cost to reach stair #n
        - This is the same function that we came up with in section 1 based on pure intuition

    3. Get State transition formula:
        - Get a formula to connect a problem to its subproblems
            => We first check what is moving the problem forward:
                
        - By reading the problem, we see that the key info is that you can either climb one or two steps.
        - Based of this info, we know that minCost(n) has strong relationship to minCost(n-1) and minCost(n-2), bc you can reach n from both n-1 and n-2 stairs.
        - We see that we should pay the cost at each stair and get minimum cost, so the relationship between them becomes clear as shown below:
        
            minCost(n) = cost[n] + min(minCost(n-1), minCost(n-2))

    4. Define base cases:
        - This part is easy, from the problem statement we know we can start from either stair index 0 or index 1
        - So base cases would be the first 2 stairs. 

            minCost(0) = cost[0] and minCost(1) = cost[1]



With all the above info, it's easy to construct a recursive solution as shown below.  Use ex. cost = [1,2,3,2,5] 

'''

'''
Now, after the above tutorial on DP, we must see a Tree analysis:

(0) Must see NeetCode's tree drawing explanation:
    @4:00 & @10:40 https://www.youtube.com/watch?v=ktmzAZWkEZ0




'''


# Recursion
class Solution:
    def minCostClimbingStairs(self, cost: List[int]) -> int:
        
        def minCost(n):
            # Base Case:
            if n < 2:   return cost[n]

            # Recursive Case:
            return cost[n] + min( minCost(n-1), minCost(n-2) )

        totalSteps = len(cost)
        # @10:38  https://www.youtube.com/watch?v=ktmzAZWkEZ0
        # - Explains the diff b/t ln-135 vs ln-141
        return min( minCost(totalSteps -1), minCost(totalSteps-2) )


'''
重要!!
    - If you trace the Top-down DP and Bottom-up DP versions of code, you'll realize Top-down DP merely setup the recursive stacks in a top-down order.  The returned values of each subproblems are from bottom-up.
    - Try tracing the Top-down DP and Bottom-up DP code using the ex below to comprehend:  
        ex. cost = [1,2,3,2,5] 
'''


# Top-down DP - Recursive Memoization
class Solution:
    def minCostClimbingStairs(self, cost: List[int]) -> int:
        
        def minCost(n):
            # Base Case:
            if n < 2:   return cost[n]

            # DP Memoization:
            if dp[n] != 0:    return dp[n]

            # Recursive Case:
            dp[n] = cost[n] + min( minCost(n-1), minCost(n-2) )
            #
            # ...???...If not "return dp[n]", get an error:
            #   TypeError: '<' not supported between instances of 'int' 
            #   and 'NoneType'
            return dp[n] 

        totalSteps = len(cost)
        dp = [0] * totalSteps
        return min( minCost(totalSteps -1), minCost(totalSteps-2) )


'''
Unfortunately, even with Top-down DP code, what input is too large, we still get TLE (Time Limit Exceeded).  But this is a great problem to learn Top-down DP.

ex.  cost = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0]

'''



'''    
..............................................................

Section 3: => See 746-2-Bottom_up_DP.py to learn Bottom-up DP: Iterative Tabulation

'''




