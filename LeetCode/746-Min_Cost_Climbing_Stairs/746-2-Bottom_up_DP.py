'''
746. Min Cost Climbing Stairs

DP Criteria => Suitable for both Top-down Recursive DP & Bottom-up Iterative DP:- To check if we can apply DP, we look at 2 distinct properties of DP:

    1. Overlapping subproblems
    - Does finding the solution require us to calculate the same subproblem multiple times?
        
        -> See NeetCode's tree analysis: 
            @4:00 https://www.youtube.com/watch?v=ktmzAZWkEZ0

    2. Optimal Substructure Property
    - Can the optimal solution be constructed from the optimal solutions of its subproblems? (Are the subproblems independent from each other?
        
        -> See NeetCode's drawing explanation:
            @10:40 https://www.youtube.com/watch?v=ktmzAZWkEZ0

'''



'''
Approach: Bottom-Up DP: Iterative Tabulation 

Vid:
(1) NeetCode => 必看 Bottom-up DP Iterative 概念 
                - 解釋 (1) when a prob depends on its subprobs,
                       (2) why solving it backwards

https://www.youtube.com/watch?v=ktmzAZWkEZ0


Article:

(2) 4 ways | Step by step from Recursion -> top down DP -> bottom up DP -> fine tuning
        => Step 5 shows fine tuning NeetCode's sol

https://leetcode.com/problems/min-cost-climbing-stairs/discuss/476388/4-ways-or-Step-by-step-from-Recursion-greater-top-down-DP-greater-bottom-up-DP-greater-fine-tuning

(3) c# bottom-up approach => QN不懂, 需要進階才懂...
https://leetcode.com/problems/min-cost-climbing-stairs/solutions/5926019/c-bottom-up-approach/


'''


'''
Bottom-up DP: Iterative Tabulation
- From (1) NeetCode's link above

Use ex. cost = [10, 15, 20] 
'''
class Solution:
    def minCostClimbingStairs(self, cost: List[int]) -> int:

        # Prob desc isn't clear that the end of cost[] isn't top of the stairs
        # ex. cost = [10, 15, 20] 0 => 0 at index 3 is the top of the stairs
        cost.append(0)

        n = len(cost)
        for i in range(n-3, -1, -1):
            # Climb 1 step or 2 steps
            #cost[i] = min( cost[i] + cost[i+1]), cost[i] + cost[i+2] )
            #
            # The last ln can be simplified as:
            cost[i] += min( cost[i+1], cost[i+2] )

        return min(cost[0], cost[1])

'''
重要!!
    - If you trace the Top-down DP and Bottom-up DP versions of code, you'll realize Top-down DP merely setup the recursive stacks in a top-down order.  The returned values of each subproblems are from bottom-up.
    - Try tracing the Top-down DP and Bottom-up DP code using the ex below to comprehend:  
        ex. cost = [1,2,3,2,5] 
'''



'''
Optimization of Bottom-up DP
- See: 
    Step 5 - Optimization 3 - Fine Tuning - Reduce O(n) space to O(1)
  from (2) link above

'''
# ...未...
