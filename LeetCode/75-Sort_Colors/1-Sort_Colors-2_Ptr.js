/*
75. Sort Colors


解題思路:


Solution Code:
- https://leetcode.com/problems/sort-colors/discuss/26688/JavaScript-three-different-solutions-(one-pass-counting-sort-library's-sort-function)

*/

// Approach #1: One Pass
// Time Analysis:
// - Counting Sort: Linear Time O(n)
// - One Pass Solution: Constant Time O(1)
var sortColors = function(nums) {
  var temp;
  var zero = 0;
  var two = nums.length - 1;
  for(var i=0; i<=two; i++){
    while(nums[i] === 2 && i !== two){

      temp = nums[two];
      nums[two] = nums[i];
      nums[i] = temp;

      two--;
    }
    while(nums[i] === 0 && i !== zero){
      temp = nums[zero];
      nums[zero] = nums[i];
      nums[i] = temp;

      zero++;
    }
  }
};
