"""
75. Sort Colors


解題思路:
- The idea is to sweep all 0s to the left and all 2s to the right, then all 1s are left in the middle.
- It is hard to define what is a "one-pass" solution but this algorithm is bounded by O(2n), meaning that at most each element will be seen and operated twice (in the case of all 0s). You may be able to write an algorithm which goes through the list only once, but each step requires multiple operations, leading the total operations larger than O(2n).


Solution Code:
- https://leetcode.com/problems/sort-colors/discuss/26605/JavaScript-One-Pass-with-Explaination

"""

# Approach #1: One Pass
# Time Analysis:
# - Counting Sort: Linear Time O(n)
# - One Pass Solution: Constant Time O(1)
function sortColors (nums) {
  let low = 0, high = nums.length - 1;

  for (let i = 0; i <= high;i++) {
    if (nums[i] === 0) {
      if (i !== low) {
        [nums[i], nums[low]] = [nums[low], nums[i]];
      }
      low++;
    } else if (nums[i] == 2) {
       [nums[i], nums[high]] = [nums[high], nums[i]];
       high--;
       i--;
    }
  }
};

