'''
75. Sort Colors

'''

'''
https://leetcode.com/problems/sort-colors/solutions/3009143/beats-98-easy-explained-solution-for-beginners/

Intuition:

- Left pointer is where the next 0 will be at
- Right pointer is where the next 2 will be at

1. "Iterate" through the list from left to right (Before reaching the latest "2" that has been swapped)

2. If the number is a 2, swap with the right pointer number and decrement it by one (As the next 2 will be to the left of the swapped "2")'

3. Elif the number is a 0, similarly, swap with the left pointer number and increment it by one.

4. Else (If the number is a 1), just "skip" the number
'''
class Solution:
    def sortColors(self, nums: List[int]) -> None:
        left = count = 0
        right = len(nums)-1
        while count <= right:
            if nums[count] == 2:
                nums[count], nums[right] = nums[right], nums[count]
                right -= 1
            elif nums[count] == 0:
                nums[count], nums[left] = nums[left], nums[count]
                left += 1
                count += 1
            else:
                count += 1
