
The solution using "Counting Sort" takes too much work to write the code and is worse than the "One Pass" solution.

Time Analysis:
- Counting Sort: Linear Time O(n)
- One Pass Solution: Constant Time O(1)
