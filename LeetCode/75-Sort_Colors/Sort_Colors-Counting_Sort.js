/*
75. Sort Colors

解題思路:
- See /Algorithms-Anna/Sorting/Counting_Sort/

Solution Code: --> The code below ln-31~35 uses nested for loop, but
                   /Algorithms-Anna/Sorting/Counting_Sort/ doesn't.
- https://leetcode.com/problems/sort-colors/discuss/26688/JavaScript-three-different-solutions-(one-pass-counting-sort-library's-sort-function)

*/

// Approach #2: Counting Sort (Two Pass)
//              => Counting Sort doesn't work for negative numbers
// Time Analysis:
// - Counting Sort: Linear Time O(n)
// - One Pass Solution: Constant Time O(1)
//
var sortColors = function(nums) {
  var k = 0;
  var count = [];

  // 2-0+1: Need an auxiliary count[] to store 0 ~ maxNum in input nums[]
  for(var i=0; i<2-0+1; i++){
    count[i] = 0;
  }
  console.log("count[]:", count);

  // Go thr nums[], and then update count[] to track number of times each
  // number shows up.
  for(var j=0; j<nums.length; j++){
    count[nums[j]-0]++;
  }
  console.log("count[]:", count);

  for(var m=0; m<count.length; m++){
    for(var n=0; n<count[m]; n++){
      nums[k++] = m + 0;
    }
  }
};

//---------- Testing ----------
let nums = [2,0,2,1,1,0]; // Output: [0,0,1,1,2,2]
sortColors(nums);
console.log("nums[]:", nums);
