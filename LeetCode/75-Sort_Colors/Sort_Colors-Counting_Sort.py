"""
75. Sort Colors


解題思路:
- See Counting Sort in /Algorithms-Anna/Sorting/

Solution Code:
- https://leetcode.com/problems/sort-colors/discuss/26605/JavaScript-One-Pass-with-Explaination

"""

# Approach #2: Counting Sort (Two Pass)
#              => Counting Sort doesn't work for negative numbers
# Time Analysis:
# - Counting Sort: Linear Time O(n)
# - One Pass Solution: Constant Time O(1)
#
def sortColors1(self, nums):
    c0 = c1 = c2 = 0
    for num in nums:
        if num == 0:
            c0 += 1
        elif num == 1:
            c1 += 1
        else:
            c2 += 1
    nums[:c0] = [0] * c0
    nums[c0:c0+c1] = [1] * c1
    nums[c0+c1:] = [2] * c2
