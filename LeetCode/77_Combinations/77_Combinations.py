'''
77. Combinations

Combinations is typical application for backtracking.



'''

'''
Approach #1: DFS Recursive Backtracking
- https://leetcode.com/problems/combinations/solution/
'''
class Solution:
  def combine(self, n: int, k: int) -> List[List[int]]:
    def backtrack(first = 1, curr = []):
      # if the combination is done
      if len(curr) == k:
        output.append(curr[:])
      for i in range(first, n + 1):
        # add i into the current combination
        curr.append(i)
        # use next integers to complete the combination
        backtrack(i + 1, curr)
        # backtrack
       curr.pop()

      output = []
      backtrack()
      return output

'''
Approach #2: DFS Recursive Backtracking => 未看...
- (1) https://leetcode.com/problems/combinations/discuss/26990/Python-easy-to-understand-DFS-solution
- (2) General Backtracking Templates in Python:
      - https://leetcode.com/problems/combinations/discuss/429526/General-Backtracking-questions-solutions-in-Python-for-reference-%3A
'''
class Solution(object):
    def combine(self, n, k):
        ret = []
        self.dfs(list(range(1, n+1)), k, [], ret)
        return ret

    def dfs(self, nums, k, path, ret):
        if len(path) == k:
            ret.append(path)
            return
        for i in range(len(nums)):
            self.dfs(nums[i+1:], k, path+[nums[i]], ret)

'''
Approach #3: DFS Iterative => 未看...

Sol Code: ==> From FLAG_TO's comment
- https://leetcode.com/problems/combinations/discuss/27029/AC-Python-backtracking-iterative-solution-60-ms

思路:
- Two conditions for back track:
(1) the stack length is already k
(2) the current value is too large for the rest slots to fit in since we are using ascending order to make sure the uniqueness of each combination.
'''
class Solution:
  def combine(self, n, k):
    stack = []
    res = []
    l, x = 0, 1
    while True:
      # Base Case:
      if l == k:
        res.append(stack[:])

      # Recursive Case:
      '''
      Two conditions for back track:
      - (1) the stack length is already k
      - (2)n-x+1 < k-l:
      -- One combination has k numbers, and currently we already have l numbers, so we need to find another k-l numbers. Since we insert the numbers into stack in the ascending order, we want to make sure that there are enough larger numbers to be inserted. From x to n, there are n-x+1 numbers that are larger than the numbers in the current stack. So if n-x+1 < k-l, it means that not enough larger numbers to be inserted, so we track back.
      '''
      if l == k or n-x+1 < k-l:
        # if list is empty
        if not stack:  return res
        x = stack.pop() + 1
        l -= 1
      else:
        stack.append(x)
        x += 1
        l += 1
