"""
78. Subsets

LeetCode Std Solution:
- https://leetcode.com/problems/subsets/solution/
- 注意!! See LeetCode Std Sol's explanation with image, very good!!
  ||
  ----> 為了容易理解記憶, check out the video and text explanations below:

(1) Video Explanation + Code: @7:16
- https://www.youtube.com/watch?v=rtFHxiQAICA&t=451s

(2) Text Explanation and Code (See the Python code in comment part)
- https://leetcode.com/problems/subsets/discuss/122645/3ms-easiest-solution-no-backtracking-no-bit-manipulation-no-dfs-no-bullshit

思路:

While iterating through all numbers, for each new number, we can either pick it or not pick it
1, if pick, just add current number to every existing subset.
2, if not pick, just leave all existing subsets as they are.
We just combine both into our result.

For example, {1,2,3} intially we have an emtpy set as result [ [ ] ]
Considering 1, if not use it, still [ ], if use 1, add it to [ ], so we have [1] now
Combine them, now we have [ [ ], [1] ] as all possible subset

Next considering 2, if not use it, we still have [ [ ], [1] ], if use 2, just add 2 to each previous subset, we have [2], [1,2]
Combine them, now we have [ [ ], [1], [2], [1,2] ]

Next considering 3, if not use it, we still have [ [ ], [1], [2], [1,2] ], if use 3, just add 3 to each previous subset, we have [ [3], [1,3], [2,3], [1,2,3] ]
Combine them, now we have [ [ ], [1], [2], [1,2], [3], [1,3], [2,3], [1,2,3] ]

"""

# Approach #1: Recursion (on LeetCode Std Sol)
#              ==> Should be called DP (Bottom Up) or BFS Approach
#
# Complexity Analysis:
# - Time/Space Complexity:  O(N×2^N)
# -- Time and space complexity are the same.  Here, we loop through each number in the nums array once, which is O(n). For each number we go through, we double the size of result, thus the next for loop will take twice as long, hence O(2^n). Multiplying them together since they are nested, we have O(n2^n) for both time and space.
# -- Or you could see it this way.  For a given number, it could be present or absent (i.e. binary choice) in a subset solution. As as result, for N numbers, we would have in total 2^N choices (solutions).
#   ==> The inner loop is not always O(2^N). You end up running a total of
#       (2^0 + 2^1 + .. 2^(N-1)) = 2^N commands.
#
# Version A:
from typing import List
class Solution:
  def subsets(self, nums: List[int]) -> List[List[int]]:
    n = len(nums)
    output = [[]]
    print(*output)

    for num in nums:
      # 注意! Python uses "+" operator to concatenate 2 lists
      # ex. In 1st iteration, curr is [] and [num] is 1. So curr+[num] is [1]
      #     - ln-22 equals to ln-23~31
      output += [curr + [num] for curr in output]
      """
      tmp = [curr + [num] for curr in output]
      print("\ntmp:", end = '')
      print(*tmp)

      output += tmp
      print("output:", end = '')
      print(*output)
      """
    return output

# Version B:
class Solution:
    def subsets(self, nums):
        all_subsets = [[]]

        for num in nums:
            for idx in range(len(all_subsets)):
                all_subsets.append(all_subsets[idx]+[num])

        return all_subsets


#---------- Testing ----------
nums = [1,2,3]
s = Solution()
res = s.subsets(nums)
print(*res)

