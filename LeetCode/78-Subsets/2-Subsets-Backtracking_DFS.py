"""
78. Subsets

(1) LeetCode Std Solution:
https://leetcode.com/problems/subsets/solution/

Note:
- Approach #2's code in LeetCode Std Solution does unnecessary computation.
  See the link below, which has general sol's for 10 backtracking problems:

https://leetcode.com/problems/permutations/discuss/18284/Backtrack-Summary%3A-General-Solution-for-10-Questions!!!!!!!!-Python-(Combination-Sum-Subsets-Permutation-Palindrome)

(2) Video Explanation & Code
https://www.youtube.com/watch?v=VdnvmfzA1pw

Note:
- This explanation sorts input nums[], but it's no needed.

#-----------------------------

Tips for Backtracking Approach:
- 假設 input set = [1,2,3], 本來想是用 a pair of nested for loops, 以 Matrix 方式為思路(畫圖) 產生各種不同 subsets.  但這不對, 而是用 Tree 方式為思路(畫圖) 產生各種不同 subsets.


#----------------------------------------------------

Def: Power Set
(1) A Power Set is a set of all the subsets of a set.

ex. For the set {a,b,c}:

    The empty set {} is a subset of {a,b,c}
    And these are subsets: {a}, {b} and {c}
    And these are also subsets: {a,b}, {a,c} and {b,c}
    And {a,b,c} is a subset of {a,b,c}

    Power Set of {a,b,c}:
      P(S) = { {}, {a}, {b}, {c}, {a, b}, {a, c}, {b, c}, {a, b, c} }

(2) Calc num of sets:
-- If a set has n members, the Power Set will have 2^n members

ex. For the set S={1,2,3,4,5}, the power set has |P(S)| = 2^n = 25 = 32 members

"""

# Approach #2: Backtracking
# - See LeetCode Std Sol's explanation with image, very good!!
#
# ...???......???......
# 不確定是否對, 看reply to my question to p.1 ugolin's comment at https://leetcode.com/problems/subsets/solution/      or      reply to p.2 rollerzz's analysis
# ...???......???......
#
# - Time complexity: O(N * 2^N)
# -- N is from for loop, 2^N is from the calling stacks
#
# - Space complexity: Same as Time complexity
#
class Solution:

  def subsets(self, nums):

    def backtrack(start, end, tmp):
      # Using tmp[:] b/c we need a deep copy to be appended
      ans.append(tmp[:])

      for i in range(start, end):
        tmp.append(nums[i])
        backtrack(i+1, end, tmp)
        tmp.pop()

   ans = []
   backtrack(0, len(nums), [])
   return ans

#---------- Testing ----------
nums = [1,2,3]
s = Solution()
res = s.subsets(nums)
print(*res)

