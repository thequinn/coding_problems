"""
78. Subsets

Note:
- Bit Manipulation Approach only works for "Combination" problems.  It doesn't work for "Permutation" problems!!


LeetCode Std Solution:
- https://leetcode.com/problems/subsets/solution/

Video Explanation & Code: ----> Starting @15:33
- https://www.youtube.com/watch?v=CUzm-buvH_8

"""
