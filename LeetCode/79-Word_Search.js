/*
79. Word Search

思路:
- Since it's obvious that there will be a lot of repetitive work, such as checking up, down, left, right for lots of characters, we can use recusion to simplify our code.

  (1) There are 3 base cases, and will need to check:
    (a) Are we getting out of boundary? if yes, get out.
    (b) Are we getting a wrong character? If yes, get out.
    (c) Did we find every character from the work? If yes, great, we have found
        this word.

  (2) Otherwise keep exploring characters for all directions.


Solution + Explanation:
- https://leetcode.com/problems/word-search/discuss/408943/JavaScript-Recursive-Solution-w-Explanation

*/

var exist = function(board, word) {
  let result = false;

  var check = function(r, c, i) {
    if (!result) {  // 注意: !result is T here

      //------ Base Cases:
      if (r < 0 || c < 0 || r >= board.length || c >= board[0].length)
        return; // out of boundary

      if (board[r][c] != word[i])  // wrong character
        return;

      if (i == word.length - 1) { // found a correct path
        result = true;
        return;
      }

      //------ Recursive Case:
      board[r][c] = null;   // mark our path so we dont go back and forth
      check(r+1,c,i+1)      // try all directions from curr index in board[][]
      check(r-1,c,i+1)
      check(r,c+1,i+1)
      check(r,c-1,i+1)
      board[r][c] = word[i] // backtrack: reset our board
    }
  }

  for (let i=0;i<board.length;i++) {
    for (let j=0;j<board[0].length;j++) {
      if (board[i][j] == word[0]) {
        check(i, j, 0)
        if (result) return result;
      }
    }
  }

  return result;
};

//---------- Testing ----------
//let board = [["a"]];
//word = "a";

let board = [
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
];

//word = "AB";
word = "ABC";   // T
//word = "ABCCED"; // T
//word = "ABB";   // F

console.log("-----> res:");
console.log(exist(board, word));

