'''
799. Champagne Tower
'''

'''
Approach #1: Pascal's Triangle Simulation
  https://leetcode.com/problems/champagne-tower/solutions/634967/java-pascal-s-triangle-simulation-o-r-c-time-and-o-r-c-space/

Time:  O(R * C) 
Space: O(R * C) 
'''
class Solution {
    public double champagneTower(int poured, int query_row, int query_glass) {
        double[][] triangle = new double[query_row + 1][query_glass + 2];
        triangle[0][0] = poured;
        
        for (int i = 0; i < query_row; i++) {
            for (int j = 0; j <= query_glass; j++) {
                double rest = Math.max(triangle[i][j] - 1.0, 0);
                triangle[i+1][j] += rest / 2.0;
                triangle[i+1][j+1] += rest / 2.0;
            }
        }
        
        return Math.min(triangle[query_row][query_glass], 1.0);
    }
}


'''
Approach #2: NeetCode => Not sure why the case below not passed ...???
  https://www.youtube.com/watch?v=LQ8TuG_QADM


Case Not Passed:
    
    input: poured = 100000009
'''
class Solution:
    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:
        pre_row = [poured]  # Flow

        for row in range(1, query_row + 1):
            cur_row = [0] * (row + 1)
            
            for i in range(row):
                extra = pre_row[i] - 1
                if extra > 0:
                    cur_row[i] += 0.5 * extra
                    cur_row[i+1] += 0.5 * extra
            pre_row = cur_row
        
        return pre_row[query_glass
