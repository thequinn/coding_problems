/*
8. String to Integer (atoi)

Sol Code:
- https://leetcode.com/problems/string-to-integer-atoi/discuss/162293/JavaScript-99-proper-and-clean-solution
*/

var myAtoi = function(str) {
    let i = 0;
    let res = 0;
    let isNegative = false;
    
    // 1. Skip leading spaces
    //while (str[i] === ' ')    i += 1;
    str = str.trim();  // Same as last ln
    
    // 2. Skip +/-
    const maybeSign = str[i];
    if (maybeSign === '+' || maybeSign === '-') {
        isNegative = maybeSign === '-';
        i += 1;
    }

    // 3. Process numbers and stop once an invalid character is found
    for (; i < str.length; i += 1) {
      // WRONG !!!
      //const code = str[i] - 48;  // '0' is 48 in ASCII Table
      //console.log("str[i]:", str[i], ", code:", code);
      //
      // CORRECT:
      // charCodeAt() returns an integer between 0 and 65535
      const code = str.charCodeAt(i) - 48; // '0' is 48
      //console.log("str.charCodeAt(i):", str.charCodeAt(i))
      //console.log("code:", code);
      
      if (code < 0 || code > 9)   break;

      res *= 10;
      res += code;
    }
    
    if (isNegative)   res = -res;
    
    // Make sure the res is in the range of [-231, 231 - 1]
    return Math.max(-(2**31), Math.min(2**31 - 1, res));
};

let s;
//s = "42";  // 42
s = "   -42";  // -42
//s = "4193 with words";  //4139
//s = "words and 987";  // 0

//s = "-91283472332";  //Output: -2147483648
// The number "-91283472332" is out of the range of a 32-bit signed integer.
// Thefore INT_MIN(−2^31) is returned.

console.log(myAtoi(s));
