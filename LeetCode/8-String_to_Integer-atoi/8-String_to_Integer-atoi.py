'''
8. String to Integer (atoi)

'''

'''
Method #2: => Faster than Method #1
- https://leetcode.com/problems/string-to-integer-atoi/solutions/1688546/python3-5-lines-faster-100-explained/


思路:

1. Iterate over the string until all leading white spaces are exhausted.

2. Check whether the next character is a sign. For "+" we assign +1 to a 
   variable sign, and -1 otherwise.

3. Iterate the next characters and accumulate our integer in res until the 
   first non-digital symbol is met.

4. Convert the string res into an integer, multiply by sign and clamp the 
   result: max(-2e31, min(2e31-1, result)).


Time: O(N) - for scan
Space: O(1) - nothing is strored
'''
class Solution:
    def myAtoi(self, s: str) -> int:
        leng, i, sign, sign_count, res = len(s), 0, +1, 0, ''
        
        while i < leng and s[i] == ' ':
            i += 1
        while i < leng and s[i] in ('-', '+'): # Same: ['-', '+']
            sign_count += 1
            if sign_count > 1:  return 0

            if s[i] == '-':  sign = -1
            else:            sign = +1
            i += 1
        while i < leng and s[i].isdigit():
            res += s[i]
            i += 1
        
        # - - - - - - - - - - - - # - - - - - - - - - - - - #
        # See breakdown in next code block:
        res = max( -2**31, min(sign * int(res  or 0), 2**31 -1) )
        return res    
        # - - - - - - - - - - - - # - - - - - - - - - - - - #
        '''
        # If no digits were read, then the integer is 0
        has_digits = int(res or 0)

        # Add the correct sign to the int
        has_digits = sign * has_digits

        # Check the range is w/in [-231, 231 - 1]
        check_upper_bound = min(has_digits, 2**31 - 1)
        check_lower_bound = max(-2**31, check_upper_bound)

        return check_lower_bound
        '''    

'''
Method #1: => Based on JS Sol Code
- 8-String_to_Integer-atoi.js, or
- https://leetcode.com/problems/string-to-integer-atoi/discuss/162293/JavaScript-99-proper-and-clean-solution
'''
class Solution:
    def myAtoi(self, s: str) -> int:     
        res, i, isNegative = 0, 0, False

        # 1. Trim spaces from both ends of a string
        s2 = s.strip()
        
        # Return 0 instead of False bc the problem wants a return of int
        if s2 == '': return 0
        
        #  2. Skip +/- 
        maybeSign = s2[i]
        if maybeSign == '+' or maybeSign == '-':
            isNegative = maybeSign == '-'
            i += 1

        # 3. Process numbers and stop once an invalid character is found
        for j in range(i, len(s2)):
            # '0' is 48 in ASCII Table
            #
            # WRONG! Don't use str or char to substract an int
            #ascii_code = s2[j] - 48 
            #
            # ord() returns an int representing Unicode char
            ascii_code = ord(s2[j]) - 48  
            
            if ascii_code < 0 or ascii_code > 9: break
            
            res *= 10
            res += ascii_code

        if isNegative:  res = -res

        return max( -pow(2, 31), min(pow(2, 31) - 1, res) )
  


