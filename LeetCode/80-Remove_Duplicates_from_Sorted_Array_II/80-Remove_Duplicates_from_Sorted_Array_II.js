/*
80. Remove Duplicates from Sorted Array II
*/

// Approach #1: Two Ptrs  (Same as Approach #2)
//
// Explanation + Sol Code:
// -(1) Video: https://www.youtube.com/watch?v=OZaADxYTfD4
// -(2) https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/discuss/967951/Python-Two-pointers-approach-explained
//
// 思路：-->  同思路: 26-Remove_Duplicates_from_Sorted_Array
// - if slow ptr's val (cur val) is diff from the (fast ptr - 2)'s val,
//   copy fast ptr's val into slow ptr, then increment slow ptr's index
//
var removeDuplicates = function(nums) {
  let k = 2;  // Allows 1 duplicate, so k = original + 1 duplicate

  let left = k;  // left: slow ptr
  for (let right = k; right < nums.length; ++right) {  // right: fast ptr
    if (nums[left - k] != nums[right]) {
      nums[left++] = nums[right];
    }
  }
  return left;
}

/*
// Approach #2: Two Ptrs
// - Solution
// -- https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/discuss/27987/Short-and-Simple-Java-solution-(easy-to-understand)
 ||
 ==> If not understand, look at .py version

***** Templates *****

>Remove Duplicates from Sorted Array(no duplicates) :

public int removeDuplicates(int[] nums) {
    int i = 0;
    for(int n : nums)
      # if (i < 2): nums[i] is replaced by n
      # if (n > nums[i - 1]): can use != instead of >
      if(i < 1 || n > nums[i - 1])
        nums[i++] = n;
    return i;
}

>Remove Duplicates from Sorted Array II (allow duplicates up to 2):

public int removeDuplicates(int[] nums) {
   int i = 0;
   for (int n : nums)
     # if (i < 2): nums[i] is replaced by n
     # if (n > nums[i - 1]): can use != instead of >
     if (i < 2 || n > nums[i - 2])
       nums[i++] = n;
   return i;
}

***** ********** *****/

var removeDuplicates = function(nums) {
  let i = 0;
  nums.forEach(n => {
    if (i < 2 || n > nums[i - 2])
      nums[i++] = n
  });
  return i;
}

let nums = [1,1,2];                 // 3, b/c [1,1,2]  (LeetCode 答案有錯!)
//let nums = [1,1,1,2,2,3];         // 5, b/c [1,1,2,2,3]
//let nums = [0,0,1,1,1,1,2,3,3];   // 7, b/c [0,0,1,1,2,3,3]
console.log(removeDuplicates(nums));

