'''
Approach #1: Two Ptrs  (Same as Approach #2)

Explanation + Sol Code:
-(1) Video: https://www.youtube.com/watch?v=OZaADxYTfD4
-(2) https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/discuss/967951/Python-Two-pointers-approach-explained

思路:
- Setup:
    (1) k=2: original + 1 duplicate
    (2) slow ptr (sp), is the 1st available pos to insert the next correct val
    (3) Not care about the 1st k nums b/c we will do (sp-k) to check
- Algo:
    (1) When fp's val is the same as (sp's ex val), keep increment fp.
        -> see "Note - 命名" below
    (2) When diff, copy fp's val into sp's val, and then increment sp.
  Note - 命名:
      (sp's prev val) = (sp's ex val) = (sp-1)'s val => Template: (sp-k)'s val

ex. nums1 = [1,1,1,2]

    nums2 = [1,1,2,3] // nums2[] shows it doesn't matter what the 1st 2 nums
                      // are.  i.e.they could be [1,1] or [1,2].

    nums3 = [1,2,2,3] // nums3[] supports nums2[] more by observing vals in
                      // 2nd and 3rd nums.  When nums[1] and nums[2] are the
                      // same, nums[2] is replaced by itself, and it's b/c
                      // 1 duplicate is allowed.
'''

/***** Templates: Allowed at Most K times of Duplicates *****/

>Remove Duplicates from Sorted Array(no duplicates) :
class Solution:
  def removeDuplicates(self, nums: List[int]) -> int:

    k = 1 # Allows 0 duplicate, so k = original + 0 duplicate

    if len(nums) <= k:  return len(nums)

    # first k numbers don't matter:
    # - nums[i=0] replaced by nums[j=0]
    # - nums[i=1] replaced by nums[j=1]
    #
    i = k  # i: slow ptr
    for j in range(k, len(nums)):  # j: fast ptr
      if nums[j] != nums[i-k]:
        nums[i] = nums[j]
        i += 1
    return i

>Remove Duplicates from Sorted Array II (allow duplicates up to 2):
class Solution:
  def removeDuplicates(self, nums: List[int]) -> int:

    k = 2 # Allows 1 duplicate, so k = original + 1 duplicate

    if len(nums) <= k:  return len(nums)

    # first k numbers don't matter:
    # - nums[i=0] replaced by nums[j=0]
    # - nums[i=1] replaced by nums[j=1]
    #
    i = k  # i: slow ptr
    for j in range(k, len(nums)):  # j: fast ptr
      if nums[j] != nums[i-k]:
        nums[i] = nums[j]
        i += 1
    return i

/***** ***** ***** ***** ***** ***** ***** ***** ***** *****/


'''
Approach #2: Two Ptrs => This solution template is not intuitive.
                         QN: Skip!!
'''
