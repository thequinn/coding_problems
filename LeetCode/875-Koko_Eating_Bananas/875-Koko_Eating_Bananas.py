'''
875. Koko Eating Bananas

# Goal: 
# - find "min" speed Koko eats so she can eat all bananas w/in h hr 

Note!! 
  - QN notices both 410. Split Array Largest Sum and 875. Koko Eating Bananas uses Binary Search to look for the "MINIMUM"


Explaination + Sol Code:
    https://leetcode.com/problems/koko-eating-bananas/solutions/769702/python-clear-explanation-powerful-ultimate-binary-search-template-solved-many-problems/
'''


class Solution:
    def minEatingSpeed(self, piles: List[int], H: int) -> int:

    
        def feasible(speed) -> bool:
            return sum( (pile - 1) // speed + 1 for pile in piles ) <= H  

        left = 1  # Min pile to eat is 1
        right = max(piles)



        while left < right:
            
            # Var mid is the speed Koko eats bananas per hr
            mid = (left + right) // 2
            
            # - Reminder: See "Goal" & "Notice" above
            # - When speed mid allows Koko to eat all bananas w/in h hrs, 
            #   we move right ptr towards left side to lower the next mid 
            #   val in the next iter. We do it bc See "Goal."  
            if feasible(mid): right = mid
            else:             left = mid + 1

        return left
