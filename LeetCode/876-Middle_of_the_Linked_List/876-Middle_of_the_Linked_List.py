'''
876. Middle of the Linked List


'''

'''
Approach #1:  Simple straight-forward method

思路:
Step 1. Traverse to the end of the Linked List while adding each node to a list
Step 2. Return the mid element from the list
'''
class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:

        cur, lst = head, []
        while cur:
            lst.append(cur)
            cur = cur.next

        length = len(lst)
        return lst[len(lst) // 2]  # "//": floor operator


'''
Approach #2: Two Pointers, Tortoise and Hare Algorithm

https://leetcode.com/problems/middle-of-the-linked-list/solutions/3198346/python-two-pointer-slow-fast-pointer-clean-simple-solution/

思路:
Increase slow ptr by 1 and fast ptr by 2. When fast ptr reach the end, 
slow ptr is at the mid of the LL.
    ||
    ===> Similar Q: Detect a LL cycle: 141. Linked List Cycle
'''
class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
       s = f = head
       while f and f.next:
           s = s.next
           f = f.next.next
       return s
