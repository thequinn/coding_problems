/*
88. Merge Sorted Array

Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:
- The number of elements initialized in nums1 and nums2 are m and n respectively.
- You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2.

ex.
  nums1 = [1,2,3,0,0,0], m = 3
  nums2 = [2,5,6],       n = 3  ==> Output: [1,2,2,3,5,6]

思路:
- If you simply consider one element at a time from the two arrays and make a decision and proceed accordingly, you will arrive at the optimal solution.
- No need to loop through the entire array. You just need to keep going from
  end of the 2 lists until n >= 0.
*/

// Solution:
// - https://leetcode.com/problems/merge-sorted-array/discuss/29680/Short-Javascript-Solution
//
var merge = function (nums1, m, nums2, n) {
  // Trace the index of nums[] for sorted position backwards
  let len = nums1.length - 1;

  m--;  n--;
  while (n >= 0) {
    if (nums1[m] > nums2[n]) {
      nums1[len--] = nums1[m--];
    }
    else {
      nums1[len--] = nums2[n--];
    }
  }

  return nums1;
};

let nums1 = [1,2,3,0,0,0], m = 3;
let nums2 = [2,5,6],       n = 3;

merge(nums1, m, nums2, n);
console.log(nums1); // [1,2,2,3,5,6]

