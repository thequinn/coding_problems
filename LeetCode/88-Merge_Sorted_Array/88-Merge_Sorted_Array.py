'''
88. Merge Sorted Array


ex. n1 = [4,5,6,0,0,0], n2 = [1,2,3]


Intuition:
-Since we have free space (the zeros) at the end of nums1, that is the direction into which we must "spread" the numbers of nums1 and nums2. That is why this solution compares and swaps the numbers starting from the end of both given sorted arrays and moves through them backwards. 
- Starting from the beginning leads only to a mess of over-swapping. We would start from the beginning and move forwards only if, for example, the free space was at the beginning of nums1.

Complexity for both approaches:
- O(m + n) time complexity, where m and n are the given lengths of the sorted arrays, since in the worst case the algorithm would need to swap every element of both arrays once (for this case as an example: [4, 5, 6, 0, 0, 0], [1, 2, 3])
-O(1) space complexity, since no extra memory is necessary


LC Premium Sol: =>  VERY BAD!
- https://leetcode.com/problems/merge-sorted-array/solution/
'''


'''
Approach #1-1: => Need extra code to check corner case
- NeetCode:
  https://www.youtube.com/watch?v=P1Ic85RarKY
- https://leetcode.com/problems/merge-sorted-array/solutions/29503/beautiful-python-solution/

ex. n1 = [4,5,6,0,0,0], n2 = [1,2,3]
'''
class Solution_1:
    def merge(self, nums1, m, nums2, n):
        last = m + n - 1

        # Traverse backward
        while m > 0 and n > 0:
            if nums1[m-1] >= nums2[n-1]:
                nums1[last] = nums1[m-1]
                m -= 1
            else:
                nums1[last] = nums2[n-1]
                n -= 1
            last -= 1

        # Corner Case:
        # - If there'are leftover in nums1, it's done.
        # - But if happens in nums2, copy the leftover into nums1 
        if n > 0:
            nums1[:n] = nums2[:n]

'''
Approach #2: => No need extra code to check corner case
             => Less intuitive, QN likes Approach #1 more.

- https://leetcode.com/problems/merge-sorted-array/solutions/29503/beautiful-python-solution/
  - See commenters, tju_xu and mgjp

- https://leetcode.com/problems/merge-sorted-array/solutions/1176400/best-python-solution-faster-than-99-one-loop-no-splicing-no-special-case-loop/
    - See ChiaN-Yang's sol

ex. n1 = [4,5,6,0,0,0], n2 = [1,2,3]
'''
class Solution_2:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        while n > 0:
            # Moved a >= 0 from while loop to if here.  It eliminates the final 
            # while loop to handle corner case.
            if nums1[m-1] >= nums2[n-1] and m > 0:
                nums1[m+n-1] = nums1[m-1]
                m -= 1
            else:
                nums1[m+n-1] = nums2[n-1]
                n -= 1


#-----Test Cases-----
nums1 = [4,5,6,0,0,0]
nums2 = [1,2,3]
m, n = 3, 3

#sol_1 = Solution_1()
#sol_1.merge(nums1, m, nums2, n)
sol_2 = Solution_2()
sol_2.merge(nums1, m, nums2, n)

print("\nFinal nums1[]:", nums1)
