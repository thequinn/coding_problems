/*
91. Decode Ways

基本思路:
- 這題跟 “Find all combinations (all possible candidates)” 相同
- 這裡, 題目要
○ Find num of ways to decode a string, ex. '226'
  § We get 3 ways to encode: (2,2,6), (2,26), (22,6)
  § Since we can take 1 or 2 char each time, we build a recursive formula:

      count = decode(index+1) decode(index+2)
      |
      ---> 看 19-Decode_Ways.py 的解法是直接用這個公式, 因為 Python 的語法


LeetCode Premium Sol:
- https://leetcode.com/problems/decode-ways/solution/

*/

/*** 注意: 可以直接跳到 Approach 0-3 ***/
/*
Approach 0-1: Brute Force  --> Time Limit Exceeded for case like
                             s = '111111111111111111111111111111111111111'
                         --> QN added this approach (similar to Approach 1)
Video Explanation:
- https://www.youtube.com/watch?v=YcJTyrG3bZs&t=602s

Note:
  - Use LeetCode Premium Sol's Tree graph to visualize.  Even though it's explained in a BFS fashion on video tutorials and in text, it's only for easier explanation.  The code is actually executed in DFS fashion.


Time Complexity: O(2^n)
- Watch end of video: https://www.youtube.com/watch?v=qli-JCrSwuk
Space Complexity: ................未.........
- Ask in the comments sec of last link................未.........


*/
class Solution {
  public int numDecodings(String s) {
    if (s == null || s.length() == 0)   return 0;
    return decode(0, s);
  }

  private int decode(int index, String s) {
    // If you reach the end of the string, return 1 for success.
    if (index == s.length())    return 1;

    // If the string starts with a zero, it can't be decoded
    if (s.charAt(index) == '0') return 0;

    /*
    The reason LC Premium Sol has this condition is that we have only 1 char
    left to process. Since we have already checked that that char is not '0' we
    know for sure that it is valid and we can return 1.

    If we don't return 1 then we will run into "out of bounds" error when we
    take the substring from index+2 from Case #2 below (we are trying to make
    a substring of length 2 but we have only 1 char).

    We can remove the statement by replacing a check for out of bounds before
    calling the recursive function on substring of length 2, ln-65

        Add: index + 2 <= s.length()
        To:  if (Integer.parseInt(s2) <= 26) {...}
        變成:if (index + 2 <= s.length() && Integer.parseInt(s2) <= 26) {...}

    if (index == s.length()-1) {
      return 1;
    }
    */

    // Case #1: handle 1 digit
    int count = decode(index+1, s);
    //
    // Case #2: handle 2 digits
    // (1) If at least 2 digits are left for us to exam
    // (2) If 10 <= substring <= 26
    //     - No need to check "10 <=" b/c we checked if s starts w/ 0 above
    if (index + 2 <= s.length() && Integer.parseInt(s.substring(index, index+2)) <= 26) {
       count += decode(index+2, s);
     }

    return count;
  }
}

/*
Approach 0-2: Brute Force => Used LC Premium Sol
                             - Used ln-38-55 and mod 1st if condition in ln-65
*/
class Solution {
  public int numDecodings(String s) {
    if (s == null || s.length() == 0)   return 0;
    return decode(0, s);
  }

  private int decode(int index, String s) {
    // If you reach the end of the string, return 1 for success.
    if (index == s.length())    return 1;

    // If the string starts with a zero, it can't be decoded
    if (s.charAt(index) == '0') return 0;

    // We already check cur ch is not '0' last ln, now we check if it's the
    // last ch
    if (index == s.length()-1) return 1;

    // Case #1: handle 1 digit
    int count = decode(index+1, s);
    //
    // Case #2: handle 2 digits
    // - If 10 <= substring <= 26
    //   - No need to check "10 <=" b/c we checked if s starts w/ 0 above
    if (Integer.parseInt(s.substring(index, index+2)) <= 26) {
       count += decode(index+2, s);
     }

    return count;
  }
}

/*
Approach 0-3: Brute Force => QN mod 1st 3 if conditions in decode() to make
                             the analyzation easier to memorize
*/
class Solution {
  public int numDecodings(String s) {
    if (s == null || s.length() == 0)   return 0;
    return decode(0, s);
  }

  private int decode(int index, String s) {

    // Base Cases (Exit Strategies):
    //
    // ***處理上個recursion stack 傳來的數據 => See "Case #1" below
    // - If you reach the end of s, return 1 for success.
    if (index == s.length())    return 1;
    //
    // ***處理上個recursion stack 傳來的數據 => See "Case #2" below
    // - If you reach the last ch of s, and it's not '0', return 1 for success
    // 注意: 1st condition prevent "out of bound" in Case #2 below
    if (index == s.length() - 1 && s.charAt(index) != '0')   return 1;


    // Edge case:
    // - If cur ch is '0', it can't be decoded
    if (s.charAt(index) == '0') return 0;

    //-----------------------------------------
    // Recursive Cases:
    //
    // Case #1: handle 1 digit
    int count = decode(index+1, s);
    //
    // Case #2: handle 2 digits
    // - If 10 <= substring <= 26
    //   - No need to check "10 <=" b/c we checked if s starts w/ 0 above
    if (Integer.parseInt(s.substring(index, index+2)) <= 26) {
       count += decode(index+2, s);
     }

    return count;
  }
}

//--------------------------------------------------------

/*
Approach 1: (Recursive) Top-Down with Memoization

Time Complexity: O(N)
- Memoization helps in pruning the recursion tree and hence decoding for an index only once. Thus this solution is linear time complexity.

Space Complexity: O(N)
- The dictionary used for memoization would take the space equal to the length of the string. There would be an entry for each index value. The recursion stack would also be equal to the length of the string.
*/
class Solution {
  HashMap<Integer, Integer> memo = new HashMap<>();

  private int recursiveWithMemo(int index, String str) {
    if (index == str.length())  return 1;

    if (str.charAt(index) == '0')   return 0;

    if (index == str.length()-1)    return 1;

    // Memoization is needed since we might encounter the same sub-string.
    if (memo.containsKey(index)) {
      return memo.get(index);
    }

    // Case #1: handle 1 digit
    int ans = recursiveWithMemo(index+1, str);
    //
    // Case #2: handle 2 digits
    if (Integer.parseInt(str.substring(index, index+2)) <= 26) {
      ans += recursiveWithMemo(index+2, str);
    }

    // Save for memoization
    memo.put(index, ans);

    return ans;
  }

  public int numDecodings(String s) {
    if (s == null || s.length() == 0)   return 0;
    return recursiveWithMemo(0, s);
  }
}

/*
Approach 2: (Iterative) Bottom-UP DP

Intuition for Memo:
- Using the sub-problems to solve the larger problems asked.  So we always start w/ the base cases.

Video Explanation & Sol Code:
- https://www.youtube.com/watch?v=cQX3yHS0cLo&t=327s

Time Complexity : O(N). We iterate the length of dp array which is N+1.
Space Complexity: O(N). The length of the DP array.
*/
class Solution {
  public int numDecodings(String s) {
    if(s == null || s.length() == 0)    return 0;

    //----------------------------------------------------
    // Part-1: Subproblems (Base Cases): i=0, i=1 --> See: Intuition for Memo

    // dp[] to store the subproblem results
    // - dp[x]: stores num of ways to decode a str of length x
    // - "+ 1": Account for the num 0 if we have no string
    int[] dp = new int[s.length() + 1];

    // Num of ways to decode of a str of length 0
    dp[0] = 1;

    // Ways to decode a string of size 1 is 1. Unless the string is '0'.
    // '0' doesn't have a single digit decode.
    dp[1] = s.charAt(0) == '0' ? 0 : 1;

    //----------------------------------------------------
    // Part-2

    // Starting at 2 b/c we already solved the subproblem of i=0 and i=1,
    // ln-147,151. The index i of dp is the i-th char of s, that is char at
    // index i-1 of s.  We start from s.charAt(1) b/c we already tested s.
    // charAt(0) in ln-151.
    for(int i = 2; i < dp.length; i += 1) {
      // Check if successful single digit decode is possible.
      if(s.charAt(i-1) != '0') {
        dp[i] += dp[i-1];
      }

      // Check if successful two digit decode is possible.
      int twoDigit = Integer.valueOf(s.substring(i-2, i));
      if(twoDigit >= 10 && twoDigit <= 26) {
        dp[i] += dp[i-2];
      }
    }

    return dp[s.length()];
  }
}

/*
Approach 3: (Iterative) Bottom-Up DP w/ Optimization

........未......
*/
