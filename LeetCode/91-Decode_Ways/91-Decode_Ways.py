

# Method #0-1: LC Premium Sol => 可直接去 Method #0-2
class Solution:

  def numDecodings(self, s: str) -> int:
    # If s is null
    if not s:  return 0
    return self.helper(0, s)

  def helper(self, index, s) -> int:
    if index == len(s): return 1
    if s[index] == '0': return 0

    if index == len(s)-1: return 1

    ans = self.helper(index+1, s) \
        + (self.helper(index+2, s) if (int(s[index : index+2]) <= 26) else 0)
    return ans
    '''
    Not sure why can't do this:

    return ans = self.helper(index+1, s) \
        + (self.helper(index+2, s) if (int(s[index : index+2]) <= 26) else 0)
    '''
#--------------------------------
# Method 0-2: QN mod 2nd if condition in Method #0-1 to make it clearer
class Solution:

  def numDecodings(self, s: str):
    if not s or len(s) == 0:  return 0
    return self.decode(0, s)

  def decode(self, index: int, s: str):
    if index == len(s):   return 1
    if index == len(s)-1 and s[index] != '0':   return 1

    if s[index] == '0':  return 0


    '''
    WRONG!! 第二個 recursive call 因為又 if statement, 要用()包住
    count = self.decode(index+1, s) + \
             self.decode(index+2, s) if int(s[index: index+2]) <= 26 else 0
    '''
    count = self.decode(index+1, s) + \
            (self.decode(index+2, s) if int(s[index: index+2]) <= 26 else 0)

    return count

#===================================================================
'''
Approach 1: (Recursive) Top-Down with Memoization

'''
class Solution:
  def numDecodings(self, s: str) -> int:
    # If s is null
    if not s:  return 0

    self.memo = {}
    return self.helper(0, s)

  def helper(self, index, s) -> int:
    # If you reach the end of the string, return 1 for success.
    if index == len(s): return 1

    # If the string starts with a zero, it can't be decoded
    if s[index] == '0': return 0

    # We already check cur ch is not '0', now we check if it's the last ch
    if index == len(s)-1: return 1


    # Memoization is needed since we might encounter the same sub-string.
    if index in self.memo:
      return self.memo[index]

    ans = self.helper(index+1, s) \
          + (self.helper(index+2, s) if (int(s[index : index+2]) <= 26) else 0)

    # Save for memoization
    self.memo[index] = ans

    return ans

#===================================================================
'''
Approach 2: (Iterative) Bottom-UP DP ....未.....
'''

#-----Test Cases-----
s = "27"  # 1
