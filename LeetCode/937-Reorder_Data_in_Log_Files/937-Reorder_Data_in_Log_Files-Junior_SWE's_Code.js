/*
937. Reorder Data in Log Files

Video Explanation:  ==> Easier to watch it first
- https://www.youtube.com/watch?v=Nj2Bpw1KKds

Sol Code:
- https://leetcode.com/problems/reorder-data-in-log-files/discuss/408131/Clean-JavaScript-solution


Time Complexity: O(N*log(N))
- where N is the number of elements we have in our array. Under the hood, the array sort used depends on the sorting algorithm used in JS.
-- There is no draft requirement for JS to use a specific sorting algorthim. As many have mentioned here, Mozilla uses merge sort.However, In Chrome's v8 source code, as of today, it uses QuickSort and InsertionSort, for smaller arrays.

Space Complexity: O(log(N))
- since quick sort must utilize recursive calls to apply sorting.

*/
const reorderLogFiles = (logs) => {

  const body = s => s.slice(s.indexOf(' ') + 1); // get body after identifier

  // Check in "JS RegExp Character classes":
  // - \d: Matches any digit [0-9]
  //
  // test(): search for a match b/t a regex and a specified string
  const isNum = s => /\d/.test(s);
  }

  // If bodies are the same then compare identifier
  const compare = (a, b) => {
    console.log("a:", a, ", b:", b);

    // strA.localeCompare(strB):
    // - Returns a number indicating whether a reference string comes before,
    //   after or is the same as the given string in sorted order.
    // - strA > strB: return positive
    //        <     : return negative
    //        =     : return 0
    const n = body(a).localeCompare(body(b));

    // If body(a) and body(b) are not equivalent, return the result
    if (n !== 0)  return n;
    // If not, compare a and b including their identifiers
    return a.localeCompare(b);
  };

  //-------------------------------------------------
  const digitLogs = [], letterLogs = [];
  for (const log of logs) {
    if (isNum(body(log)))
      digitLogs.push(log);
    else letterLogs.push(log);
  }
  console.log("letterLogs:", letterLogs);

  return [...letterLogs.sort(compare), ...digitLogs];
};

//-----Test Case-----
let logs1 = [
  "dig1 8 1 5 1",
  "let1 art can",
  "dig2 3 6",
  "let2 own kit dig",
  "let3 art zero"
];
// ['let1 art can','let3 art zero','let2 own kit dig','dig1 8 1 5 1',dig2 3 6']

let logs2 = [
  "a1 9 2 3 1",
  "g1 act car",
  "zo4 4 7",
  "ab1 off key dog",
  "a8 act zoo"
];
// ['g1 act car','a8 act zoo','ab1 off key dog','a1 9 2 3 1','zo4 4 7']

let logs3 = ["w 7 2", "l 1 0", "6 066", "o aay", "e yal"];
// ['o aay', 'e yal', 'w 7 2', 'l 1 0', '6 066' ]


