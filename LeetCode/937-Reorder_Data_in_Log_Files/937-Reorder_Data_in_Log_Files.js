/*
937. Reorder Data in Log Files

ex.

Input: logs = [
  "dig1 8 1 5 1",
  "let1 art can",
  "dig2 3 6",
  "let2 own kit dig",
  "let3 art zero"
]

Output: [
  "let1 art can",
  "let3 art zero",
  "let2 own kit dig",
  "dig1 8 1 5 1",
  "dig2 3 6"
]

注意!!
(1) Blackbird Studios' Scoring App interview:
    - QN failed to write it was determinded as "too junior"
(2) 必看 LeetCode Std Sol.
    - 此答案教你如何寫 Custom Comparator, 改進寫程式“最基礎”功力!!

The JS code implemented below is by QN.  It's based on the 1st sol (Java version) in LeetCode Std Sol:
- https://leetcode.com/problems/reorder-data-in-log-files/solution/
*/
var reorderLogFiles = function(logs) {

  function compare(logA, logB) {

    // Split at the1st occurrance of the separator
    let idA = logA.slice(0, logA.indexOf(" "));
    let contentA = logA.slice(logA.indexOf(" ")+1);
    //console.log("idA:", idA, ", contentA:", contentA);

    let idB = logB.slice(0, logB.indexOf(" "));
    let contentB = logB.slice(logB.indexOf(" ")+1);
    //console.log("idB:", idB, ", contentB:", contentB);

    // Is 1st char of logA content a digit?
    // Note: parseInt(str, radix): radix does not default to 10! 記得設定!
    let isDigitA = Number.isInteger( parseInt(contentA.charAt(0), 10) );
    let isDigitB = Number.isInteger( parseInt(contentB.charAt(0), 10) );
    //console.log("isDigitA:", isDigitA, ", isDigitB:", isDigitB);

    // Case 1: Both logs are letter-logs
    if (!isDigitA && !isDigitB) {
      // 法二: --> Slower
      /*let cmp = contentA.localeCompare(contentB)
      // If contentA and contentB are diff
      if (cmp != 0)   return cmp;
      // If they're the same, compare their id's
      let cmp2 = idA.localeCompare(idB);
      return cmp2;*/

      // 法一: --> Faster
      // If contentA and contentB are diff
      if (contentA < contentB)  return -1;
      if (contentA > contentB)  return 1;

      if (idA < idB)  return -1;
      if (idA > idB)  return 1;
      return 0;
    }

    // Case 2: One of the logs is digit-log
    if (!isDigitA && isDigitB)    return -1;
    if (isDigitA && !isDigitB)   return 1;
    // Case 3: Both logs are digit-logs
    return 0;
  }

  //-------------------------------------------
  // Don't need the checks
  //if (logs.length === 0 || !logs)
    //return [];

  logs.sort(compare);
  return logs;
};



let logs1 = [
  "dig1 8 1 5 1",
  "let1 art can",
  "dig2 3 6",
  "let2 own kit dig",
  "let3 art zero"
];
// ['let1 art can','let3 art zero','let2 own kit dig','dig1 8 1 5 1',dig2 3 6']

let logs2 = [
  "a1 9 2 3 1",
  "g1 act car",
  "zo4 4 7",
  "ab1 off key dog",
  "a8 act zoo"
];
// ['g1 act car','a8 act zoo','ab1 off key dog','a1 9 2 3 1','zo4 4 7']

let logs3 = ["w 7 2", "l 1 0", "6 066", "o aay", "e yal"];
// ['o aay', 'e yal', 'w 7 2', 'l 1 0', '6 066' ]

