/*
937. Reorder Data in Log Files

ex.

Input: logs = [
  "dig1 8 1 5 1",
  "let1 art can",
  "dig2 3 6",
  "let2 own kit dig",
  "let3 art zero"
]

Output: [
  "let1 art can",
  "let3 art zero",
  "let2 own kit dig",
  "dig1 8 1 5 1",
  "dig2 3 6"
]

注意!!
(1) Blackbird Studios' Scoring App interview:
    - QN failed to write it was determinded as "too junior"
(2) 必看 LeetCode Std Sol.
    - 此答案教你如何寫 Custom Comparator, 改進寫程式“最基礎”功力!!

LeetCode Std Sol:
- https://leetcode.com/problems/reorder-data-in-log-files/solution/
*/

import java.util.*;
class Solution {

  public String[] reorderLogFiles(String[] logs) {

    // Override compare() in Comparator Interface
    Comparator<String> myComp = new Comparator<String>() {

      @Override
      public int compare(String log1, String log2) {

        // String.split(regex, limit):
        // - https://www.geeksforgeeks.org/split-string-java-examples/
        // - Below, we split each log into two parts: <identifier, content>
        String[] split1 = log1.split(" ", 2);
        String[] split2 = log2.split(" ", 2);
        System.out.println("\nln-39, split1[1] : split2[1] = " + split1[1] + " : " + split2[1]);

        boolean isDigit1 = Character.isDigit(split1[1].charAt(0));
        boolean isDigit2 = Character.isDigit(split2[1].charAt(0));
        //System.out.println(isDigit1 + " : " + isDigit2);

        // case 1). both are letter-logs
        if (!isDigit1 && !isDigit2) {
          System.out.println("ln-48, both are letter-logs");

          // first compare the content
          int cmp = split1[1].compareTo(split2[1]);

          if (cmp != 0)   return cmp;

          // logs of same content, compare the identifiers
          System.out.println("ln-55, split1[0] : split2[0] = " + split1[0] + " : " + split2[0]);
          return split1[0].compareTo(split2[0]); // 比較 2 strs alphabetically
        }

        // case 2). one of logs is digit-log
        if (!isDigit1 && isDigit2) {
          System.out.println("ln-61, letter-log vs digit-log");
          return -1;  // the letter-log comes before digit-logs
        }
        else if (isDigit1 && !isDigit2) {
          System.out.println("ln-65, digit-log vs letter-log");
          return 1;
        }
        // case 3). both logs are digit-logs
        else {
          System.out.println("ln-70, both are digit-logs");
          return 0;
        }
      }
    };

    //---------------------------------

    Arrays.sort(logs, myComp);
    return logs;
  }

  public static void main(String[] args) {
    String[] logs = {        // Output: See top of the file
      "dig1 8 1 5 1",
      "let1 art can",
      "dig2 3 6",
      "let2 own kit dig",
      "let3 art zero"
    };

    Solution sol = new Solution();
    String[] res = sol.reorderLogFiles(logs);

  }
}

