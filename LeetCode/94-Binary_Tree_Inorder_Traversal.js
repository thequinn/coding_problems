/*
94. Binary Tree Inorder Traversal

Follow up: Recursive solution is trivial, could you do it iteratively?

//-------------------------------
注意:

Tree Traversals have 2 main kinds:
- (1) Depth First Traversal
- (2) Breadth First Traversal

(1) Depth First Traversal:
  (a) Inorder (Left, Root, Right) : 4 2 5 1 3
  (b) Preorder (Root, Left, Right) : 1 2 4 5 3
  (c) Postorder (Left, Right, Root) : 4 5 2 3 1

技巧: In, Pre, Post 指的是 Root "何時" 印出 !!!

(2) Breadth First / Level Order Traversal: 1 2 3 4 5

     1
    / \
   2   3
  / \
 4   5

*/

//--------------------------
// Approach #1-1: Recursive
// - Time complexity : O(n) b/c the recursive function is T(n)=2⋅T(n/2)+1
// - Space complexity: The worst case space required is O(n) 例如.tree 是一條斜線, and in the average case it's O(log⁡n) where n is number of nodes.
//
var inorderTraversal = function(root) {
  let res = [];
  return helper(root, res);
};

var helper = (root, arr) => {
  if (!root)    return arr;

  helper(root.left, arr);
  arr.push(root.val);
  helper(root.right, arr);
  return arr;
}

// Approach #1-2: Recursive => 作者直接加 2nd arg.
var inorderTraversal = function(root, arr = []) {
  if (root) {
    inorderTraversal(root.left, arr);
    arr.push(root.val);
    inorderTraversal(root.right, arr);
  }

  return arr;
};

//--------------------------
// Appraoch #2: Iterative using Stack
// // https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/31394/JavaScript-solution-with-iteration
//
//- This approach is actually the same as BFS, just using a stack instead of a queue. So you get both O(n) time and space complexity.
//
function inorderTraversal(root) {
  const stack = [], res = [];

  while (root || stack.length) {
    if (root) {
      stack.push(root);
      root = root.left;
    }
    else {
      root = stack.pop();
      res.push(root.val);
      root = root.right;
    }
  }

  return res;
}

//--------------------------
// Test Case: [1,2,3,null,null,6]
// Expected : [2,1,6,3]
