'''
953. Verifying an Alien Dictionary

'''


'''
Sol by Quinn

思路:

Higher Level:
- Iter over words[].  Compare a pair of words each time.  If two words are in correct order, compare the next pair.

Details
- How to compare 2 words:
  - when ch1's order > ch2's order, return F
  - when ch1's order = ch2's order, con't to compare next set of chars
  - when ch1's order < ch2's order, return T

- Edge case: When 2 strs are partially the same and in diff lengthes.
  ex. 'Apple' and "App"
    - Compare till len of shorter str + 1
    - s1[3] = 'l', s2[3] = '∅' => 'l' > '∅'

'''
from typing import List
class Solution:
    def isAlienSorted(self, words: List[str], order: str) -> bool:

        # Convert the order str into hash {ch: idx of the order str}
        dct = {}
        for i, ch in enumerate(order):
            dct[ch] = i

        for i in range(len(words)-1):
            print("words[", i, "]:", words[i], ", words[", i+1, "]:", words[i+1])
            flag = False
            len_limit = min(len(words[i]), len(words[i+1]))

            for j in range(len_limit):
                print("\nwords[", i, "][", j, "]:", words[i][j])
                print("words[", i+1, "][", j, "]:", words[i+1][j])
                val1 = dct[words[i][j]]
                val2 = dct[words[i+1][j]]

                # Read print() in the following cases
                if val1 > val2:
                    return False
                if val1 < val2:
                    flag = True
                    break

            # ex.  "Apple" and "App" => Compare chars at index=3
            if flag == False and len(words[i]) > len(words[i+1]):
                return False
        return True

'''
Method #1:
- https://leetcode.com/problems/verifying-an-alien-dictionary/discuss/203175/Python-straightforward-solution

思路:
- Hash indexes of each character for better runtime
- Compare every adjacent word
- If any letter of former word is in higher order, return False
- If current letter of former word is in lower order, forget the rest of word
- If lenght of former word is longer and latter word is substring of former, return False (apple & app etc.)
- Return True
'''
# See comments and print() below
class Solution:
    def isAlienSorted(self, words, order):
        ind = {c: i for i, c in enumerate(order)}
        for a, b in zip(words, words[1:]):
            if len(a) > len(b) and a[:len(b)] == b:
                return False
            for s1, s2 in zip(a, b):
                if ind[s1] < ind[s2]:
                    break
                elif ind[s1] > ind[s2]:
                    return False
            return True

class Solution:
    def isAlienSorted(self, words, order):
        # Create mapping for the order of the alphabet
        tbl = {c: i for i, c in enumerate(order)}

        # See breakdowns explanations below:
        #for a, b in zip(words, words[1:]):
        #
        # (1) Not work!
        # - https://stackoverflow.com/questions/55409214/zip-object-usage-in-python-for-loop
        #zipped_words = zip(words, words[1:])
        #for (a, b) in list(zipped_words):
        # (2) Works
        zipped_words = list(zip(words, words[1:]))
        print("zipped_words:", zipped_words)
        for a, b in zipped_words:
            print("a:", a, ", b:", b)

            # Edge Case: ex. 'Apple' and 'App'
            if len(a) > len(b) and a[:len(b)] == b:
                return False

            #for s1, s2 in zip(a, b):
            #
            zipped_chs = list(zip(a, b))
            print("zipped_chs", zipped_chs)
            for c1, c2 in zipped_chs:
                if tbl[c1] < tbl[c2]:   break
                elif tbl[c1] > tbl[c2]: return False
        return True

'''
Method #2:
- https://leetcode.com/problems/verifying-an-alien-dictionary/discuss/203185/JavaC%2B%2BPython-Mapping-to-Normal-Order
'''
class Solution:
  def isAlienSorted(self, words, order):
    m = {c: i for i, c in enumerate(order)}

    words = [[m[c] for c in w] for w in words]

    return all(w1 <= w2 for w1, w2 in zip(words, words[1:]))



#-----Test Cases-----

#words = ["hello","leetcode"]           # T
#order = "hlabcdefgijkmnopqrstuvwxyz"

words = ["woo","word","world","row"]    # F
order = "worldabcefghijkmnpqstuvxyz"

#words = ["apple","app"]                # F
#order = "abcdefghijklmnopqrstuvwxyz"

sol = Solution()
print("order:", order)
res = sol.isAlienSorted(words, order)
print("res:", res)
