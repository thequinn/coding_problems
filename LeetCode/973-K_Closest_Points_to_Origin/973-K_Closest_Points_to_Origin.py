'''
973. K Closest Points to Origin

Sol Code:
https://leetcode.com/problems/k-closest-points-to-origin/solutions/3556983/python3-maxheap-short-beats-97/

// - - - - - - - - - - - - - - - - //
思路:

1. Python's default is min heap.  We negate a num (dst) before pushing to the heap. Thus the pt farther away from the origin will be on the top of the heap.
    ex.[[3,3],[5,-1],[-2,4]] => Run the sol code and see the print-out

2. After popping out the top of the heaps, we have larger nums left.  However, if we negate these nums back, they are the smaller nums (dst)

'''

class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        maxHeap = []

        for ndx, pt in enumerate(points):
            dist = pt[0] ** 2 + pt[1] ** 2  # **: power operator
            print("\nndx:", ndx, ",pt[]:", pt, ", dist:", dist)

            if len(maxHeap) < k:
                heappush(maxHeap, (-dist, ndx))
                print("if - maxHeap:", maxHeap)
            else:
                # heappushpop(): Push item on the heap, then pop and return
                # the smallest item from the heap.
                poppedItem = heappushpop(maxHeap, (-dist, ndx))
                print("poppedItem:", poppedItem )
                print("else - maxHeap:", maxHeap)
        print("maxHeap:", maxHeap)

        res = [points[ndx] for _, ndx in maxHeap]
        print("\nres:", res)
        return res



 



