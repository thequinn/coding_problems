
See notes:
  BitBucket/_Notes-Ptrs+DSA/Using_Min_Heap_as_Max_Heap.txt



(1) Heap / Priority Queue
1. It's a binary tree
2. Python's default heap is a min heap



(2) Using Python's defalut Min Heap as a Max Heap

1. When do we need a Max Heap?
   - We need to return smallest vals in an collection.
     ex. k smallest nums, k cloest pts
   - To do that, we use max heap to quickly get rid of the larger vals

2. Using a Min Heap as a Max Heap
   -  Negate the nums when stored in the min heap.



Note: Use an ex and then draw it out



ex. Return the smallest num from nums[10, 8, 7]

            min         max
            ---------------
            -10, -8, -7
            ---------------

            => Negate the nums when pushing them into the heap.
               Pop the top 2 elems, and the last elem is the smallest


