/*
974. Subarray Sums Divisible by K

Note:
- Must comprehend the math theory behind.
  See the math proof in 523-Continuous_Subarray_Sum.js

Solution Code:
- Java Version:
-- https://leetcode.com/problems/subarray-sums-divisible-by-k/discuss/217980/Java-O(N)-with-HashMap-and-prefix-Sum
- JS Version:
-- https://leetcode.com/problems/subarray-sums-divisible-by-k/discuss/816163/javascript-prefix-sum-solution

Time Complexity: O(N)
Space Complexity: O(K)
*/

var subarraysDivByK = function(nums, k) {
  let map = {0: 1}; // x%k=0 means x是k的倍數
  let r = 0; // remainder
  let count = 0;

  for (let i = 0; i < nums.length; i++) {
    r += nums[i];

    // 這題不用check, b/c 題目說: 2 <= k <= 10000
    //if (k != 0)   r = r % k;
    r = r% k;

    // -1 % 5 = -1, but we need the positive mod 4. “-1到0”和“1到0“距離一樣.
    if (r < 0)    r = r + k;

    // if (map[r] != undefined) {  // 不夠check
    if (map.hasOwnProperty(r)) {
      // 注意: 不是像 523. 存 i as key's val
      //
      // - count加上map[r]的次數, 因為每碰到一個已存在的r,要加上從index:0~cur_i
      // 的r 出現次數.  所以count++不對.
      count += map[r];

      map[r]++;
    }
    else  map[r] = 1;
  }

  return count;
};

//-----Test Case-----
A = [4,5,0,-2,-3,1], K = 5; // 7
console.log(subarraysDivByK(A, K));

