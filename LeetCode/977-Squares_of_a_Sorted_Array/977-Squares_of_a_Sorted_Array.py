'''
977. Squares of a Sorted Array

https://leetcode.com/problems/squares-of-a-sorted-array/solutions/3189378/python-two-pointer-clean-simple-o-n-solution/

'''

'''
Approach #1: Using sort()
'''
class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        # Remember to use list() bc map() returns a map obj (an iterator)
        res = list(map(lambda x: x**2, nums)) 
        res.sort()
        return res

'''
Approach #2: Two Ptrs

https://leetcode.com/problems/squares-of-a-sorted-array/solutions/283978/python-two-pointers/

思路:
- Create 2 ptrs pting on 2 ends of A[], and iterate backwards from end of len(A)
- If abs(left ptr) > abs(right ptr), square left ptr and place it at end of res[], increment left ptr
- Else, do the same to right ptr, decrement right ptr
'''
class Solution:
    def sortedSquares(self, A: List[int]) -> List[int]:
        res = [None] * len(A)
        left, right = 0, len(A)-1

        for i in range(len(A)-1, -1, -1):
            if abs(A[left]) >= abs(A[right]):
                res[i] = A[left] ** 2
                left += 1
            else:
                res[i] = A[right] ** 2
                right -= 1
        return res
