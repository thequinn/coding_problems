/*
98. Validate Binary Search Tree

ex1:   2
      / \
     1   3    Input: [2,1,3]
              Output: true
ex2:   5
      / \
     1   4
        / \
       3   6  Input: [5,1,4,null,null,3,6]
              Output: false

LeetCode Std Solution:  (第一個答案可看，第二個爛，第三個還可以)
- https://leetcode.com/problems/validate-binary-search-tree/solution/


-------------------------------------------------------------------------

觀念：
DFS: Pre-order, In-order, Post-order
BFS: Level-order
*/

/*
Approach 1: Recursion DFS

Video + Python Sol Code:
https://www.youtube.com/watch?v=s6ATEkipzow

我出錯於此:
- one should keep both upper and lower limits for each node while traversing the tree.
- ex. [5, 1, 4, null, null, 4, 8]
    - node.val=4 violates a valid binary search tree
    - See video explaination @5:30


JS Sol Code:
https://leetcode.com/problems/validate-binary-search-tree/discuss/207438/javascript

- Time complexity: O(n), since we visit each node exactly once.
- Space complexity: O(n), since we keep up to the entire tree.
*/
var isValidBST = function(root) {

  // Number.MIN_SAFE_INTEGER
  // - min safe integer in JS (-(2^53 - 1))
  return dfs(root, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER);

  function dfs(root, min, max){
    // 2 ways to think:
    // (1) a leaf node is reached
    // (2) a leaf node is an empty binary search tree.  An empty binary search
    //     tree is still a valid one binary search tree.
    if (!root)    // The leaf node is reached
      return true;

    if (root.val >= max || root.val <= min)
      return false;

    //console.log("root.val:", root.val, ", min:", min, ", max:", max);
    return dfs(root.left, min, root.val) && dfs(root.right, root.val, max)
  }
};

//------------------------------------------------

// Approach 2: Iteration
// ==> 這解法沒什麼人用， 跳過。


// Approach 3-1: DFS In-order Traversal (Left -> Node -> Right)
// - LeetCode Std Solution (JS Version):
// https://leetcode.com/problems/validate-binary-search-tree/discuss/357687/JavaScript-inorder-traverse-solution
//
// - 思路：
// -- Left -> Node -> Right order of inorder traversal means for BST that
//    each element should be smaller than the next one.
// ex2:  [4]
//       / \
//      2   5
//     / \
//    1  [6]   ==> In-order Traversal: 1,2,[6],[4],5
//                 Not valid BST b/c 6<4 in inorder traversal
//
// - Time complexity: O(n)
// -- in the worst case when the tree is BST or the "bad" element is a
//    rightmost leaf.
// - Space complexity: O(n), to keep a stack.
//
var isValidBST = function(root) {
  let arr = [];

  var traverse = function(root) {
    if (!root)    return;
    traverse(root.left);
    arr.push(root.val);
    traverse(root.right);
  };

  traverse(root);

  for (let i = 0; i < arr.length-1; i++) {
    if (arr[i] >= arr[i+1])    return false;
  };

  return true;
}

// Approach 3-2 ==> Improved Approach 3-1
// - https://leetcode.com/problems/validate-binary-search-tree/discuss/158094/Python-or-Min-Max-tm     (這連結最下方的解法)
// - 思路:
// -- 在上面的算法里进行了优化，每次只需要将当前root.val和上次储存的 last
//    比对即可知道是否满足条件。
//
var isValidBST = function(root) {
  let last = -Infinity, flag = true;
  inorder(root);
  return flag;

  function inorder(root) {
    if (!root)    return;

    inorder(root.left);

    if (last >= root.val)   flag = false;
    last = root.val;

    inorder(root.right);
  }
};
