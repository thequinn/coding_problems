'''
986. Interval List Intersections

https://www.youtube.com/watch?v=ZjxhxTiahBQ

Tips: 
- Draw out examples to visualize diff scenarios

Time: O(N+M), where N = len(l1), M = len(l2) => O(2N) =>O(N)
Space: O(N), otherwise O(1)
'''

class Solution:
    def intervalIntersection(self, firstList: List[List[int]], secondList: List[List[int]]) -> List[List[int]]:

        if not firstList or not secondList:
            return [] 

        p1 = p2 = 0
        res = []
        while p1 < len(firstList) and p2 < len(secondList):
            start1, end1 = firstList[p1]
            start2, end2 = secondList[p2]

            # No intersection
            if start1 > end2:    p2 += 1
            # No intersection
            elif start2 > end1:    p1 += 1
            # Yes intersection
            else:
                # Image the start and end squeeze in as tight as possible
                res.append( [ max(start1, start2), min(end1, end2)] )
            
                # There might be more overlaps for the same interval
                if end1 > end2:    p2 += 1
                else:              p1 += 1
        return res
