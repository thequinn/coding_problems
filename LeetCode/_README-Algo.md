# 学渣的逆袭：Starting Date: 2018/04/10 :milky_way:
# 承認学渣的開始: 2019,08,31~present

"+": 學習次數,學到一題的 主要解法"們"
"x": 複習次數 (不包含 學習次數)

|--------|--------|--------|--------|------|--------|--------|--------|
*** 複習次數?  不是以次數為準, 而是 1Easy/15min, 1Medium/30min ***
|--------|--------|--------|--------|------|--------|--------|--------|


<< 每期刷題長度追蹤 >>

s0. Codewars
------> 2019/08/30 ~ 2019/09/17
- Did some problems to be familiar with the language you are using!!


s1. Cracking the Coding Interviews
------> 2019/09/23 ~ 2019/09/28
- Only did some problems, and then moved to LeetCode.

s2. LeetCode

2019/10/02 ~ 2019/10/18: Easy
2019/11/21 ~ 2019/12/03: Medium => 很艱難!
2020/01/15 ~ 2020/05/22: Medium => 很艱難!
2020/08/26 ~ 2020/09/28 (34天): Medium => 變簡單! 改變做題策略: 一個題型做好幾題
2020/10/19 ~ 2020/10/22 (4天) : Medium => 測試策略: 為因應面試, 3天複習1 main topic, 每天複習1 subtopic
2020/11/01 ~ 2020/12/26 (56天): Stick to the strategy
2021/04/13 ~                  : (2nd time)


|--------|--------|--------|--------|--------|--------|--------|--------|
:turtle:  :bike: :speedboat:  :blue_car:  :tram:  :bullettrain_side:  :helicopter:  :airplane:  :rocket:
| Site | 強化 | Difficulty | Title & Problem Link | Date | Category | 基礎: ; 關聯: ; 延伸: | Notes |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Hard | [] | 2022/03/ | | 基礎: ; 關聯: ; 延伸: | |
| LeetCode | | Medium | [] | 2022/03/ | | 基礎: ; 關聯: ; 延伸: | |
| LeetCode | | Easy| [] | 2022/03/ | | 基礎: ; 關聯: ; 延伸: | |
| Codewars | | kyu | [] | 2022/03/ | | 基礎: ; 關聯: ; 延伸: | |
|--------|--------|--------|--------|--------|--------|--------|--------|




|# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #|
>>>2022
|========|========|========|========|========|========|========|========|
*****  必須複習這週刷的題 ***** !!!!!



| LeetCode | | Medium | [62. Unique Paths] | (+0, x3) 2022/03/02-04 | Math, DP, Combinatorics | 基礎: ; 關聯:; 延伸: | 未: Approach #3-#5 in 1-Unique_Paths-Recursive_to_DP-BEST.py |

| LeetCode | :anchor: :star: | Medium | [337. House Robber III] | (+1 x2) 2022/03/02 | Recursion, DP | 基礎:152, 198; 關聯:740, 2140; 延伸: | |

| LeetCode | :anchor: :star: | Medium | [337. House Robber III] | (+1 x1) 2022/03/01 | Recursion, DP | 基礎:152, 198; 關聯:740, 2140; 延伸: | |
| LeetCode | :anchor: :star: | Medium | [213. House Robber II] | (+1 x0) 2022/03/01 | Recursion, DP | 基礎:152, 198; 關聯:740, 2140; 延伸: | |

| LeetCode | :anchor: :star: | Medium | [198. House Robber] | (+3 x2) 2022/02/28 | Recursion, DP | 基礎:152; 關聯:740, 2140; 延伸:  | |

| LeetCode | :anchor: :star: | Medium | [198. House Robber] | (+3 x0) 2022/02/28 | Recursion, DP | | 未: See 198/Readme.txt |


| LeetCode | | Medium | [152. Maximum Product Subarray] | (+1 x0) 2022/02/24 | Array, DP, Two Ptrs | 基礎:53; 關聯:198, 238, 713; 延伸: | 未: Two Ptrs approach |
|========|========|========|========|========|========|========|========|

| LeetCode | | Medium | [714. Best Time to Buy and Sell Stock with Transaction Fee] | (x2) 2022/01/03 | | 基礎: 122. ; 關聯: 309. ; 延伸: | 確認理解如何畫 State Machine 來表達這類問題! |

| LeetCode | | Medium | [714. Best Time to Buy and Sell Stock with Transaction Fee] | (x1) 2022/01/01 | | 基礎: 122. ; 關聯: 309. ; 延伸: | 未: 需確認理解如何畫 State Machine 來表達這類問題! |
| LeetCode | | Medium | [309. Best Time to Buy and Sell Stock with Cooldown] | (x1) 2022/01/01 | | 基礎: 122. ; 關聯: 714. ; 延伸: | |
| LeetCode | | Medium | [122. Best Time to Buy and Sell Stock II] | (x4) 2022/01/01 | Array, Greedy |  基礎: 121. ; 關聯: 309. 714. ; 延伸:| Focus on Approach #3, #4(DP) |


|# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #|
>>>2021

|========|========|========|========|========|========|========|========|
| LeetCode | | Medium | [122. Best Time to Buy and Sell Stock II] | (x3) 2021/12/28 | Array, Greedy |  基礎: 121. ; 關聯: 309. 714. ; 延伸:| 需複習: Approach #4: DP |
| LeetCode | | Easy | [121. Best Time to Buy and Sell Stock] | (x4) 2021/12/28 | Array, DP(Kadane's Algorithm) | 基礎: ; 關聯: ; 延伸: 122. | |
| LeetCode | :star: | Easy | [53. Maximum Subarray] | (x4) 2021/12/27 | Array, DP(Kadane's Alg), Divide & Conquer | | |
|========|========|========|========|========|========|========|========|
| LeetCode | | Easy | [121. Best Time to Buy and Sell Stock] | (x3) 2021/12/04 | Array, DP(Kadane's Algorithm) | 基礎: ; 關聯: ; 延伸: | Confused of setting to 0 in maxCur = Math.max(0, maxCur += prices[i] - prices[i-1]); in .js |
| LeetCode | :star: | Easy | [53. Maximum Subarray] | (x3) 2021/12/04 | Array, DP(Kadane's Alg), Divide & Conquer | | |
|========|========|========|========|========|========|========|========|
| LeetCode | | Medium | [80. Remove Duplicates from Sorted Array II] | (x3_2) 2021/11/30 | | 基礎: 26. Remove Duplicates from Sorted Array ; 關聯: ; 延伸: | |
| LeetCode | | Easy| [26. Remove Duplicates from Sorted Array] | (x4_2) 2021/11/30 | Array, Two Pointers | 基礎: ; 關聯: 80. Remove Duplicates from Sorted Array II ; 延伸: | |
| LeetCode | | Medium | [80. Remove Duplicates from Sorted Array II] | (x3_1) 2021/11/29 | | 基礎: 26. Remove Duplicates from Sorted Array ; 關聯: ; 延伸: | Added the right template to solve k duplicates |
| LeetCode | | Easy| [26. Remove Duplicates from Sorted Array] | (x4_2) 2021/11/29 | Array, Two Pointers | 基礎: ; 關聯: 80. Remove Duplicates from Sorted Array II ; 延伸: | Added template to solve k duplicates |
|========|========|========|========|========|========|========|========|
| LeetCode | | Medium | [80. Remove Duplicates from Sorted Array II] | (x2_2) 2021/11/26 | | 基礎: 26. Remove Duplicates from Sorted Array ; 關聯: ; 延伸: | (1)***QN designed an algorithm based on Two Ptrs.*** (2) Needs to learn the template from solution.  |

| LeetCode | | Medium | [80. Remove Duplicates from Sorted Array II] | (x2_1) 2021/11/25 | | 基礎: 26. Remove Duplicates from Sorted Array ; 關聯: ; 延伸: | QN 的解法有bug -> Fixed on 2021/11/26 |
| LeetCode | | Easy| [26. Remove Duplicates from Sorted Array] | (x4_1) 2021/11/25 | Array, Two Pointers | 基礎: ; 關聯: 80. Remove Duplicates from Sorted Array II ; 延伸: | x4: QN recalled 解法, Two Ptrs.  |
|========|========|========|========|========|========|========|========|


|# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #||# # # # # # # #|
>>>2020

|========|========|========|========|========|========|========|========|
| LeetCode | | Medium | [122. Best Time to Buy and Sell Stock II] | (x2) 2020/10/02 | Array, Greedy |  基礎: ; 關聯: ; 延伸:| 需複習: Approach #4: DP |
| LeetCode | | Easy | [121. Best Time to Buy and Sell Stock] | (x2) 2019/10/02 | Array, DP(Kadane's Algorithm) | 基礎: ; 關聯: ; 延伸: | 未複習 Kadane's Algorithm 的code |

| LeetCode | | Medium | [80. Remove Duplicates from Sorted Array II] | (x1_2) 2021/10/01 | | 基礎: 26. Remove Duplicates from Sorted Array ; 關聯: ; 延伸: | |
| LeetCode | | Easy| [26. Remove Duplicates from Sorted Array] | (x3) 2021/10/01 | Array, Two Pointers | 基礎: ; 關聯: 80. Remove Duplicates from Sorted Array II ; 延伸: | |
|========|========|========|========|========|========|========|========|
| LeetCode | | Medium | [122. Best Time to Buy and Sell Stock II] | (x2) 2020/09/28 | Array, Greedy |  基礎: ; 關聯: ; 延伸:| 需複習: Approach #4: DP |
| LeetCode | | Easy | [121. Best Time to Buy and Sell Stock] | (x2) 2019/09/28 | Array, DP(Kadane's Algorithm) | 基礎: ; 關聯: ; 延伸: | 未複習 Kadane's Algorithm 的code |

| LeetCode | | Medium | [80. Remove Duplicates from Sorted Array II] | (x1_1) 2021/09/29 | | 基礎: 26. Remove Duplicates from Sorted Array ; 關聯: ; 延伸: | |
| LeetCode | | Easy| [26. Remove Duplicates from Sorted Array] | (x3) 2021/09/29 | Array, Two Pointers | 基礎: ; 關聯: 80. Remove Duplicates from Sorted Array II ; 延伸: | |
|========|========|========|========|========|========|========|========|


| LeetCode | | Medium | [47. Permutations II] | 2021/05/ | Recursive Backtracking | 基礎: ; 關聯: ; 延伸: | 未:要做答案裡 2-Permutations-Backtrack_Template_for_10_Questions.py 的類似10題! |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [31. Next Permutation] | (x1 +2) 2021/05/04 | Array | 基礎: ; 關聯: ; 延伸: | Not relate to Recursive Backtracking and DFS questions like Permutations. |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [17. Letter Combinations of a Phone Number] | 2021/05/03 | String, Backtracking, DFS, Recursion | 基礎: ; 關聯: 39. Combination Sum; 22. Generate Parentheses; 延伸: | |
| LeetCode | | Medium | [40. Combination Sum II] | 2021/05/03 | Array, Backtracking | 基礎: 39. Combination Sum; 關聯: ; 延伸: | 可以不做 17. Letter Combinations of a Phone Number; 未: Backtracking template x6 |
|========|========|========|========|========|========|========|========|

| LeetCode | | Medium | [39. Combination Sum] | 2021/05/02 | Array, Backtracking | 基礎: 216. Combination Sum III; 關聯: 17. Letter Combinations of a Phone Number
; 延伸: 40. Combination Sum II; 254. Factor Combinations | 未: Backtracking template x6 |
| LeetCode | | Medium | [216. Combination Sum III] | 2021/05/02 | | 基礎: ; 關聯: ; 延伸: 39. Combination Sum | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [77. Combinations] | (x2) 2021/05/01 | Backtracking | 基礎: 關聯: 46. Permutations; 39. Combination Sum; 延伸: | 未: Approach #2, #3|
| LeetCode | | Medium | [46. Permutations ] | (x2, +6+2) 2021/04/29, 2021/05/01 | Backtracking | 基礎: 關聯: 77. Combinations; 39. Combination Sum; 延伸: 31. Next Permutation, 47. Permutations II | 567. Permutation in String is under categories of Two Pointer, Sliding Window |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [18. 4Sum] | (x1) 2021/04/25 | Array, Two Pointers, Hash Table | | |
|========|========|========|========|========|========|========|========|

| LeetCode | | Medium | [16. 3Sum Closest] | (x1) 2021/04/22 | Array, Two Pointers | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [15. 3Sum] | (+2 x1)2021/04/18-2021/04/20 | Array, Two Pointers| 基礎: Two Sum, Two Sum II; 關聯: 3Sum Closest, 4Sum, 3Sum Smaller; 延伸: | |
| LeetCode | | Medium (Easy) | [11. Container With Most Water] | (x2) 2021/04/18 | Array, Two Pointers |  基礎: ; 關聯: ; 延伸: 42. Trapping Rain Water | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [340. Longest Substring with At Most K Distinct Characters] | (x1) 2020/04/17 | String, Hash Table, Two Pointers, Sliding Window | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: | Medium | [3. Longest Substring Without Repeating Characters] | (+2 x2) 2021/04/13~2021/04/16 | String, Hash Table, Two Pointers, Sliding Window | Working:法2, 未:法3 |
|========|========|========|========|========|========|========|========|

策略 v.1-2: (2nd time, after 3.5 mo of job)
1. 今天開始按照 "主題"->"次要主題" 分類, 每 "次要主題" 做~5題
  - 主題: (a) Arrays & Strings (b) Trees & Graphs (c) Dynamic Programming (d) Linked List, Sorting & Searching
2. 4wk + 1wk break / mini-sprint -> 1 main sprint/3mo -> 3 sprints, 一層層疊在基礎上, 一層層拉高水平能力
   (a) Arrays & Strings: 1wk  (b) Trees & Graphs: 1wk
   (c) DP & Greedy: 1wk     (d) Sorting & Searching: 1wk
3. 題目選擇用 Google Interview in LC Explore
4. 目標速度: 1M/25min, 4M/day
5. 每六日 複習 一~五刷過的題


|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [207. Course Schedule] | 2020/12/23 ~ 2020/12/26 | DFS, BFS, Graph, Topological Sort | 基礎: ; 關聯: 210. Course Schedule II, 261. Graph Valid Tree, 310. Minimum Height Trees ; 延伸: 630. Course Schedule III | 不懂 Approach 2, 未做 Appraoch 3 |

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [48. Rotate Image] | (+1) 2020/12/10 | Array | | QN thinks this prob has no pt to learn. |
| LeetCode | | Medium (Easy) | [11. Container With Most Water] | (+1) 2020/12/10 | Array, Two Pointers |  基礎: ; 關聯: ; 延伸: 42. Trapping Rain Water | |
| LeetCode | | Medium | [127. Word Ladder] | (+4 x0) 2020/12/10 | BFS | 基礎: 79. Word Search, 139. Word Break, 140. Word Break II ; 關聯: 433. Minimum Genetic Mutation ; 延伸: | 未: Approach 2; 先做相似題 433. |

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [127. Word Ladder] | (+3 x0) 2020/12/02 | BFS | 基礎: 79. Word Search, 139. Word Break, 140. Word Break II ; 關聯: 433. Minimum Genetic Mutation ; 延伸: | 未: 不理解 Appraoch #1; 未: Approach 2; 先做相似題 433. |

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [127. Word Ladder] | (+2 x0) 2020/11/16 | BFS | | 未: 不理解 Appraoch #1 ; 未: Approach 2 |

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [127. Word Ladder] | (+2 x0) 2020/11/03 | BFS | | 未: Approach 2 |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [127. Word Ladder] | (+1 x0) 2020/11/02 | BFS | | |
| LeetCode | | Medium | [222. Count Complete Tree Nodes] | (+3 x0) 2020/11/02 | Binary Search (Divide & Conquer), Tree | | Reviewed Binary Search & found good video to explain LC Premium Sol's Approach 2|
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [222. Count Complete Tree Nodes] | (+1 x0) 2020/11/01 | Binary Search (Divide & Conquer), Tree | | 未: LC 的Approach 2 有點長, 找到LC Discussion 中較好的Approach 2 版本, 但還看不懂 |
| LeetCode | | Easy| [270. Closest Binary Search Tree Value] | 2020/11/01 | Binary Search, Tree | 延伸: 222. Count Complete Tree Nodes | |
|--------|--------|--------|--------|--------|--------|--------|--------|

策略 v.1-2: (1st time)
1. 今天開始按照 主題->次要主題 分類, 每 次要主題 做~5題
  - 主題: (a) Arrays & Strings (b) Trees & Graphs (c) Dynamic Programming (d) Linked List, Sorting & Searching
2. 3wk + 1wk break / mini-sprint -> 1 main sprint/3mo -> 3 sprints, 一層層疊在基礎上, 一層層拉高水平能力
   (a) Strings & Arrays: 1wk  (b) Trees & Graphs: 1wk
   (c) DP & Greedy: 0.5wk     (d) Sorting & Searching: 0.5wk
3. 題目選擇用 Google Interview in LC Explore
4. 目標速度: 1M/25min, 4M/day

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: | Easy | [276. Paint Fence] | (+1, x1) 2020/10/22 | DP | 基礎: 70. 509.; 關聯: 198. House Robber (E) | |
| LeetCode | :anchor: :star:  | Medium | [91. Decode Ways] | (+3 x2) 2020/10/22 | String, DP | 基礎: 70. Climbing Stairs, 276. Paint Fence; 延伸: 62. Unique Paths | (+2)Traced code to review 法#0; Reviewed 法#1; 未: 方法 #2 #3 |

| LeetCode | :star: | Hard | [1411. Number of Ways to Paint N × 3 Grid] | (+1 x0) 2020/10/23 | DP | 未: Python code 沒完全看懂 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: :star: | Medium | [337. House Robber III] | (+1 x0) 2020/10/21 | Tree, DFS, DP | 基礎: 198. House Robber | 未: LC Std Sol 答案還沒看, 好像寫得有點複雜, 找其他解答 |
| LeetCode | :anchor: :star: | Easy | [198. House Robber] | (+2 x0) 2020/10/21 | Recursion, DP | | 做Greedy probs -> Find out if it's a greedy problem or DP? |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: :star: | Easy | [198. House Robber] | (+1 x0) 2020/10/20 | Recursion, DP | 基礎: 70. 509. ; 關聯: 337. House Robber III (M), 152. Maximum Product Subarray, 256. Paint House (M); 延伸: 1473. Paint House III (H) | Went thr the code too fast, need to revisit tomorrow |

| LeetCode | :anchor: | Easy | [276. Paint Fence] | 2020/10/20 | DP | 基礎: 70. 509.; 關聯: 198. House Robber (E) | |

| LeetCode | :anchor: :star:  | Medium | [91. Decode Ways] | (+2 x1) 2020/10/20 | String, DP | 基礎: 70. Climbing Stairs, 276. Paint Fence; 延伸: 62. Unique Paths | 未: 方法 #2 #3 |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: | Easy| [509. Fibonacci Number] | (x1) 2020/10/19 | Array, Recursion, DP | | |
| LeetCode | :anchor: :star: | Easy | [70. Climbing Stairs] | (x1) 2019/10/19 | Recursion, DP | 關聯: 509. Fibonacci Number, 276. Paint Fence | |
|--------|--------|--------|--------|--------|--------|--------|--------|

策略 v.1:
1. 今天開始按照 "主題"->"次主題" 分類, 每 "次主題" 做5~10題
2. 1 sprint/mo -> 3 sprints, 一層層疊在基礎上, 一層層拉高水平能力
   Strings & Arrays: 1wk, Trees & Graphs: 2wk, DP & Greedy: 1wk

|--------|--------|--------|--------|--------|--------|--------|--------|

| Codewars | :anchor: | 7kyu | [Decending Order] | 2020/10/18 | List, String | 1. string.split() can't split a str w/o a separator. 2. sort() vs sorted()|
| Codewars | :anchor: | 7kyu | [Credit Card Mask] | 2020/10/18 | List, Slice | |
| Codewars | :anchor: | 7kyu | [List Filtering] | 2020/10/18 | List | |
| Codewars | | 7kyu | [Square Every Digit] | 2020/10/18 | | |  |

| Codewars | | 6kyu | [Find the unique number] | 2020/10/17 | | | |
| Codewars | | 7kyu | [Mumbling] | 2020/01/10 | String | | |
| Codewars | :anchor: | 7kyu | [Get the Middle Character] | 2020/10/17 | String, Slice | Get floor num in Python, ex. 5//2=2 | |
| Codewars | :anchor: | 7kyu | [Highest and Lowest] | 2020/10/17 | String | | |
| Codewars | :anchor: | 7kyu | [Friend or Foe?] | (x1) 2020/10/17 | String | | |
| Codewars | :anchor: | 6kyu | [Weird IPv6 hex string parsing] | 2020/10/17 | String, Lists | 好像有點 tricky, 可能對 List Comprehension 不熟 => 複習以前做過的題 | | |

| Codewars | :anchor: | 6kyu | [Weird IPv6 hex string parsing] | 2020/10/15 | String, Lists | 未: review list comprehension types of syntax | | |
| Codewars | | 6kyu | [Remove the parentheses] | 2020/10/15 | String | | | |
|--------|--------|--------|--------|--------|--------|--------|--------|

| LeetCode | | Medium | [150. Evaluate Reverse Polish Notation] | 2020/09/28 | Stack | 基礎: 20; 相關: 394| |
| LeetCode | :anchor: :star: | Medium | [394. Decode String] | 2020/09/28 | Stack, DFS | | |
| LeetCode | :star: | Medium | [286. Walls and Gates] | 2020/09/28 | DFS, BFS | 基本: 200 | |
| LeetCode | :anchor: :star: | Medium | [200. Number of Islands] | (x1) 2020/09/28 | DFS, BFS, Disjoint Set Union Find | Reviewed: BFS, 未: Disjoint Set Union Find | |

|--------|--------|--------|--------|--------|--------|--------|--------|

| LeetCode | :anchor: :star:  | Medium | [91. Decode Ways] | (+1 x0) 2020/9/27 | String, DP | 基礎: 70. Climbing Stairs, 276. Paint Fence; 延伸: 62. Unique Paths | Only did brute force approach.  未: Approach 1 & 2 on LC Premium Sol |
| LeetCode | :anchor: :star: | Medium | [200. Number of Islands] | (x1) 2020/09/27 | DFS, BFS, Disjoint Set Union Find | Reviewed: DFS; 未: BFS, Disjoint Set Union Find | |

| LeetCode | :anchor: :star: | Medium | [394. Decode String] | 2020/09/25 | Stack, DFS | | |
| LeetCode | :star:| Medium | [286. Walls and Gates] | 2020/09/25 | DFS, BFS | 未.....   基本: 200 | |
| LeetCode | :anchor: :star: | Medium | [200. Number of Islands] | (x1) 2020/09/25 | DFS, BFS, Disjoint Set Union Find | 未: Disjoint Set Union Find | |

| LeetCode | | Medium | [974. Subarray Sums Divisible by K] | 2020/09/24 | Array, Hash Table | 基本: 523, 560 | |

| LeetCode |  :rat: :star: | Medium | [560. Subarray Sum Equals K] | (x2) 2020/09/23 | Array, Hash Table | 關聯: 523 | |
| LeetCode | :star: | Medium | [523. Continuous Subarray Sum] | (x2) 2020/09/23 | Math, DP | 關聯: 523; 延伸: 974 | |


| LeetCode | :star: | Medium | [523. Continuous Subarray Sum] | (x1) 2020/09/22 | Math, DP | (1) 有用到560. (2) 這題只是要背數學證明,其實很簡單! | |

| LeetCode | | Easy | [724. Find Pivot Index] | 2020/09/21 | Array | | |
| LeetCode |  :rat: :star: | Medium | [560. Subarray Sum Equals K] | (x1) 2020/09/21 | Array, Hash Table | Help: Do 1. Two Sum first | |
| LeetCode | | Easy | [1. Two Sum] | (x1) 2020/09/21 | Array, Hash Table | |
| LeetCode | :anchor: :star: | Medium | [43. Multiply Strings] | (x1) 2020/09/21 | Math, String | 未:reproduce | |

|--------|--------|--------|--------|--------|--------|--------|--------|

***** 今天開始按照題型分類, 每類型做5~10題 *****

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [227. Basic Calculator II] | 2020/ | Stack, String | | |

| LeetCode | :anchor: | Easy| [937. Reorder Data in Log Files] | 2020/09/18 ~ 2020/09/19 | String | 題目細節在短時間分析有點複雜,所以Amazon愛考 | |

| LeetCode | 未 | Hard| [42. Trapping Rain Water] | 2020/09/15 | Array, Two Pointers, Stack | 基礎: 11. Container With Most Water; 關聯: ; 延伸:  | 未: 對現階段的我太複雜 |

| LeetCode | :star: | Medium | [238. Product of Array Except Self] | 2020/09/14 | Array | Learned Approach #2 | |

|--------|--------|--------|--------|--------|--------|--------|--------|

| LeetCode | | Medium | [238. Product of Array Except Self] | 2020/09/11 | Array | Approach #2 不太懂 | |
| LeetCode | :star: | Easy | [53. Maximum Subarray] | (x2) 2019/09/11 | Array, DP(Kadane's Alg), Divide & Conquer | | |
| LeetCode | :star: | Medium | [134. Gas Station] | 2020/09/11 | Greedy | | |
 |
| LeetCode | | Easy| [7. Reverse Integer] | 2020/09/10 | String, Array | | |
| LeetCode | | Easy| [344. Reverse String] | (2) 2020/09/10 | Two Pointers, String | | |
| LeetCode | | Easy| [110. Balanced Binary Tree] | 2020/09/10 | Tree, DFS | | |
 |
| LeetCode | :anchor: | Easy| [110. Balanced Binary Tree] | 2020/09/09 | Tree, DFS | | |
| LeetCode | :anchor: :star: | Easy | [104. Maximum Depth of Binary Tree] |  (x2) 2020/09/09 | Tree, DFS, Recursion | 需複習 Iterative traverse w/ (1) DFS Stack (2) BFS Queue | |
 |
| LeetCode | :anchor: :star: | Easy | [104. Maximum Depth of Binary Tree] |   (x2) 2020/09/08 | Tree, DFS, Recursion | | |
| LeetCode | :rat: | Medium | [523. Continuous Subarray Sum] | 2020/09/08 | Math, DP | 複習(x1)時發現原來根本沒懂!! | |
 |
| LeetCode | :rat: | Medium | [523. Continuous Subarray Sum] | 2020/09/07 | Math, DP | Learning HashMap 解法 | |
| LeetCode | :star: | Medium | [137. Single Number II] | 2020/09/07 | Hash Table, Bit Manipulation | | |

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: | Medium | [137. Single Number II] | 2020/09/06 | Hash Table, Bit Manipulation | Learning Bitwise 解法| |
| LeetCode | | Easy| [136. Single Number] | 2020/09/06 | Hash Table, Bit Manipulation | | |
 |
| LeetCode | | Easy | [36. Valid Sudoku] | 2020/09/05 | Hasn Table | | |
| LeetCode | | Easy | [136. Single Number] | (x2) 2019/09/05 | Hash Table, Bit Manipulation | | |
| LeetCode | :anchor: :star: :rat: | Medium | [300. Longest Increasing Subsequence] | (x2) 2020/09/05 | Binary Search, DP | | |
 |
| LeetCode | :anchor: :star: :rat: | Medium | [300. Longest Increasing Subsequence] | (x2) 2020/09/04 | Binary Search, DP | Reviewing Approach #4 | |
 |
| LeetCode | :anchor: :star: :rat: | Medium | [300. Longest Increasing Subsequence] | (x2) 2020/09/03 | Binary Search, DP | Reviewed Approach #3 | |
| LeetCode |  | Medium | [334. Increasing Triplet Subsequence] | 2020/09/03 | Array, String | 完成 | |
 |
| LeetCode | | Medium | [334. Increasing Triplet Subsequence] | 2020/08/31 | Array, String | 未完成.... | |
| LeetCode | :rat: :star: | Medium | [560. Subarray Sum Equals K] | 2020/08/31 | Array, Hash Table | **重要: Basis of 437. Path Sum III | |

未: Add 437. CTCI optimization code to this folder......

|--------|--------|--------|--------|--------|--------|--------|--------|
| Review | :anchor: :star: :rat: | | [437. Path Sum III] | 2020/08/30 | Tree | | |

| LeetCode | :anchor: :star: :rat: | Medium | [437. Path Sum III] | (x3) 2020/08/28 | Tree | "CTCI  4.12 Paths with Sum": 1st 解法 | |

| LeetCode | :anchor: :star: :rat: | Medium | [437. Path Sum III] | (x2) 2020/08/27 | Tree | 學習 "CTCI  4.12 Paths with Sum" 2 解法 | |
| LeetCode | | Medium | [122. Best Time to Buy and Sell Stock II] | 2020/08/27 | Array, Greedy | | |

| LeetCode | :anchor: :star: :rat: | Medium | [437. Path Sum III] | (x2) 2020/08/26 | Tree | | |
| LeetCode | | Easy| [26. Remove Duplicates from Sorted Array] | (x2) 2020/08/26 | Array, Two Pointers | | |
|--------|--------|--------|--------|--------|--------|--------|--------|

遺忘曲線記憶法:
(1) 週日: 第二,三天重做(複習) 第一天的題 ==> 第一,二次複習
(2) 週末: 第七天重做(複習) 周日所有的題  ==> 第三次複習

|--------|--------|--------|--------|--------|--------|--------|--------|

*** 目前進度: 2019/09~2020/08: 刷了4.5個月 ***
(現在開始加入 ”遺忘曲線記憶法“, 所以紀錄會有重複題目)

心得: ==> 整理自以下Notes
- 不需學太多種解法, 一種普通快, 一種改良快即可
- Greedy can't give optimal sol every time, but DP can

|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: :star: :rat: | Easy| [437. Path Sum III](https://leetcode.com/problems/path-sum-iii/) | 2020/05/16 ~ 2020/05/22 | Tree, Double Recursion, Backtracking and DP | 原題: CTCI 4.12 Paths with Sum; Double Recursion 很難!! |
| LeetCode | :anchor: :star: :rat: | Easy| [543. Diameter of Binary Tree](https://leetcode.com/problems/diameter-of-binary-tree/) | 2020/05/13 ~ 2020/05/15, 2020/05/24 | Tree | 要 update 現在最大質 is tricky!! |
| LeetCode | :anchor: | Easy| [617. Merge Two Binary Trees](https://leetcode.com/problems/merge-two-binary-trees/) | 2020/05/13 | Tree | |
| LeetCode | :rat: | Easy| [581. Shortest Unsorted Continuous Subarray](https://leetcode.com/problems/shortest-unsorted-continuous-subarray/solution/) | 2020/05/12 | | |
| LeetCode | :anchor: :rat: | Easy| [ 448. Find All Numbers Disappeared in an Array](https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/) | 2020/05/12 | Array |  Approach #2 需要點技巧 |
| LeetCode | :anchor:  | Easy| [169. Majority Element](https://leetcode.com/problems/majority-element/) | 2020/05/09 ~ 2020/05/11 | Array, Divide & Conquer, Bit Manipulation | (1)Approach #6 使用 Boyer-Moore Voting Algorithm (2)未: Bit Manipulation. Easy Problem 不需學太多種解法, 一種普通快, 一種改良快即可 |

| LeetCode | :anchor: | Easy | [226. Invert Binary Tree](https://leetcode.com/problems/invert-binary-tree/) | 2020/05/07 | Tree | |
| LeetCode | :anchor: :star: :rat: | Medium | [300. Longest Increasing Subsequence](https://leetcode.com/problems/longest-increasing-subsequence/) | 2020/04/30 ~ 2020/05/06 | Binary Search, DP | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: :star: | Medium | [322. Coin Change](https://leetcode.com/problems/coin-change/) | 2020/04/14 ~ 2020/04/29 | DP, Greedy (Not Optimal) | Greedy can't give optimal sol every time, but DP can |

| LeetCode | :anchor: | Medium | [62. Unique Paths](https://leetcode.com/problems/unique-paths/) | 2020/04/07 ~ 2020/04/14 | Array, DP | 第一次複習.  雖然花更多時間學,但更深入了解 Recursive -> DP (4階段) |
| LeetCode | :anchor: :star: | Medium | [55. Jump Game](https://leetcode.com/problems/jump-game/) | 2020/04/04 ~ 2020/04/06 | Array, Backtracking, DP, Greedy | |
| LeetCode | :anchor: :star: | Medium | [240. Search a 2D Matrix II](https://leetcode.com/problems/search-a-2d-matrix-ii/) | 2020/04/04 | Binary Search, Divide & Conquer | 未: Divide & Conquer method |
| LeetCode | :star: | Medium | [33. Search in Rotated Sorted Array](https://leetcode.com/problems/search-in-rotated-sorted-array/) | 2020/04/03 | Array, Binary Search | |
| LeetCode | :star: | Medium | [56. Merge Intervals](https://leetcode.com/problems/merge-intervals/) | 2020/04/01~2020/04/02 | Array, Sort | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: | Medium | [34. Find First and Last Position of Element in Sorted Array](https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/) | 2020/03/24~25, 2020/03/30~31 | Array, Binary Search | |
| LeetCode | :star: | Medium | [162. Find Peak Element](https://leetcode.com/problems/find-peak-element/) | 2020/03/23 ~ 2020/03/24 | Array, Binary Search | |
| LeetCode | :star: | Medium | [215. Kth Largest Element in an Array](https://leetcode.com/problems/kth-largest-element-in-an-array/) | 2020/03/21 ~ 2020/03/23 | Divide  & Conquer, Heap | Not yet learn Hoare's partition, but probably not necessary to learn everything |
| LeetCode | :star: | Medium | [347. Top K Frequent Elements](https://leetcode.com/problems/top-k-frequent-elements/) | 2020/03/20 | Hash Table, Heap | |
| LeetCode | :star: | Medium | [75. Sort Colors](https://leetcode.com/problems/sort-colors/) | 2020/03/17 ~ 2020/03/20 | Array, Two Pointers, Sort | 不理 Counting Sort |

| LeetCode | :anchor: :star: | Medium | [78. Subsets](https://leetcode.com/problems/subsets/) | 2020/03/12 ~ 2020/03/16 | Array, Recursive Backtracking, Bit Manipulation | 未學 Bit Manipulation Approach in LeetCode Std Sol |
| LeetCode | :star: | Medium | [79. Word Search](https://leetcode.com/problems/word-search/) | 2020/03/10 ~ 2020/03/11 | Array, Recursive Backtracking | |
| LeetCode | :anchor: :star: | Medium | [46. Permutations](https://leetcode.com/problems/permutations/) | 2020/03/03~2020/03/08 (6) | Recursive Backtracking | 未:要做答案裡 2-Permutations-Backtrack_Template_for_10_Questions.py 的類似10題! |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: :rat: | Medium | [22. Generate Parentheses](https://leetcode.com/problems/generate-parentheses/) | 2020/02/21 ~ 2020/03/02 (11) | String, Recursive Backtracking| 不懂 Approach #3, LeetCode Std Sol |
| LeetCode | :anchor: :star: | Medium | [17. Letter Combinations of a Phone Number](https://leetcode.com/problems/letter-combinations-of-a-phone-number/) | 2020/02/18 ~ 2020/02/20 | Recursive Backtracking, DFS, BFS | High Frequency for FLAG.  Very fundamental "Recursive Backtracking" question!!! |
| LeetCode | :star: :rat: | Medium | [200. Number of Islands](https://leetcode.com/problems/number-of-islands/) | 2020/02/10 ~ 2020/02/18 | DFS, BFS, Disjoint Set Union Find | |
| LeetCode | | Medium | [116. Populating Next Right Pointers in Each Node](https://leetcode.com/problems/populating-next-right-pointers-in-each-node/) | 2020/02/09 | Tree, DFS, BFS | BFS sol not yet understood |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [105. Construct Binary Tree from Preorder and Inorder Traversal](https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/) | 2020/01/15, 2020/02/08 | Array, Tree, DFS | Tip! 這個答案看半天看不懂, 就換個看看.  有時是答案爛, 不是題目難解!! |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | :anchor: | 5kyu | [Best travel](https://www.codewars.com/kata/55e7280b40e1c4a06d0000aa/) | 2020/01/15 | itertools | |
| Codewars | :anchor: :anchor: | 5kyu | [A Chain adding function](https://www.codewars.com/kata/539a0e4d85e3425cb0000a88/) | 2020/01/15 | Inheritance, __call__, __init__ | ???...這題不太懂, 須先學 Inheritance |
| Codewars | :anchor: | 5kyu | [Moving Zeros To The End](https://www.codewars.com/kata/52597aa56021e91c93000cb0/) | 2020/01/15 | Array | 技巧: Differenciate b/t False and 0: if x is False |
| Codewars | | 5kyu | [Scramblies](https://www.codewars.com/kata/55c04b4cc56a697bb0000048/) | 2020/01/15 | Collection.Counter() | 用減法配合 Counter() 的技巧 |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 6kyu | [Convert string to camel case](https://www.codewars.com/kata/517abf86da9663f1d2000003/) | 2020/01/14 | Regex | |
| Codewars | :anchor: | 6kyu | [Are they the "same"?](https://www.codewars.com/kata/550498447451fbbd7600041c/) | 2020/01/14 | Tuple | Using "in" to check elems in tuple |
| Codewars | :anchor: | 6kyu | [Create Phone Number](https://www.codewars.com/kata/525f50e3b73515a6db000b83/) | 2020/01/14 | Callback | 學習使用 str.format() |
| Codewars | | 6kyu | [Sort the odd](https://www.codewars.com/kata/578aa45ee9fd15ff4600090d/) | 2020/01/14 | Callback, List Comprehension | |
| Codewars | | 6kyu | [Valid Braces](https://www.codewars.com/kata/5277c8a221e209d3f6000b56/) | 2020/01/14 | Dictionary, String | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 6kyu | [Two Sum](https://www.codewars.com/kata/52c31f8e6605bcc646000082/) | 2020/01/13 | |
| Codewars | :anchor: | 6kyu | [Array.diff](https://www.codewars.com/kata/523f5d21c841566fde000009/) | 2020/01/13 | Set | |
| Codewars | :anchor: | 6kyu | [Unique In Order](https://www.codewars.com/kata/54e6533c92449cc251001667/) | 2020/01/13 | | Method 1 的技巧要學 (控制 prev 和 curr), 因為我以前的方法不好! |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | :anchor: | 6kyu | [Who likes it?](https://www.codewars.com/kata/5266876b8f4bf2da9b000362/) | 2020/01/12 | Dictionary, str.format() | |
| Codewars | :anchor: | 6kyu | [Your order, please](https://www.codewars.com/kata/55c45be3b2079eccff00010f/) | 2020/01/12 |  String, Lambda| |
| Codewars | :anchor: | 6kyu | [Persistent Bugger](https://www.codewars.com/kata/55bf01e5a717a0d57e0000ec/) | 2020/01/12 | Callback, Lambda | |
| Codewars | | 6kyu | [Replace With Alphabet Position](https://www.codewars.com/kata/546f922b54af40e1e90001da/) | 2020/01/12 | String, List, Dictionary | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 6kyu | [Title Case](https://www.codewars.com/kata/5202ef17a402dd033c000009/) | 2020/01/11 | List, Slice | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 7kyu | [Credit Card Mask](https://www.codewars.com/kata/5412509bd436bd33920011bc/) | 2020/01/10 | List, Slice | |
| Codewars | | 7kyu | [List Filtering](https://www.codewars.com/kata/53dbd5315a3c69eed20002dd/) | 2020/01/10 | List | |
| Codewars | :anchor: | 7kyu | [Sum of two lowest positive integers](https://www.codewars.com/kata/558fc85d8fd1938afb000014/) | 2020/01/10 | String, Slice | |
| Codewars | | 7kyu | [Growth of a Population](https://www.codewars.com/kata/563b662a59afc2b5120000c6/) | 2020/01/10 | | |
| Codewars | | 7kyu | [Jaden Casing Strings](https://www.codewars.com/kata/5390bac347d09b7da40006f6/) | 2020/01/10 | | |
| Codewars | | 7kyu | [Isograms](https://www.codewars.com/kata/54ba84be607a92aa900000f1/) | 2020/01/10 | | |
| Codewars | | 7kyu | [Square Every Digit](https://www.codewars.com/kata/546e2562b03326a88e000020/) | 2020/01/10 | | |
| Codewars | | 7kyu | [Complementary DNA](https://www.codewars.com/kata/554e4a2f232cdd87d9000038/) | 2020/01/10 | String | |
| Codewars | | 7kyu | [Exes and Ohs](https://www.codewars.com/kata/55908aad6620c066bc00002a/) | 2020/01/10 | | |
| Codewars | | 7kyu | [You're a square!](https://www.codewars.com/kata/54c27a33fb7da0db0100040e/) | 2020/01/10 | | |
| Codewars | | 6kyu | [Find the unique number](https://www.codewars.com/kata/585d7d5adb20cf33cb000235/) | 2020/01/10 | | |
| Codewars | | 7kyu | [Decending Order](https://www.codewars.com/kata/5467e4d82edf8bbf40000155/) | 2020/01/10 | List, String| |
| Codewars | | 7kyu | [Shortest Word](https://www.codewars.com/kata/57cebe1dc6fdc20c57000ac9/) | 2020/01/10 | String | |
| Codewars | | 7kyu | [Mumbling](https://www.codewars.com/kata/mumbling/) | 2020/01/10 | String | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 7kyu | [Vowel Count](https://www.codewars.com/kata/vowel-count/) | 2020/01/08 | String, Utilities | |
| Codewars | :anchor: | 7kyu | [Get the Middle Character](https://www.codewars.com/kata/get-the-middle-character/) | 2020/01/08 | String, Slice | |
| Codewars | :anchor: | 7kyu | [Highest and Lowest](https://www.codewars.com/kata/highest-and-lowest/) | 2020/01/08 | String | |
| Codewars | :anchor: | 7kyu | [Disemvowel Trolls](https://www.codewars.com/kata/disemvowel-trolls/) | 2020/01/08 | String, Regex | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 7kyu | [Friend or Foe?](https://www.codewars.com/kata/friend-or-foe/) | 2020/01/07 | String | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: | Medium | [103. Binary Tree Zigzag Level Order Traversal](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/) | 2020/01/06 | Stack, Tree, BFS | 鍛鍊 BFS and Queue/Stack 基本功 |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [94. Binary Tree Inorder Traversal]() | 2020/01/02 | Hash Table, Stack, Tree | |
| LeetCode | | Easy | [Intersection of Two Linked Lists](https://leetcode.com/problems/intersection-of-two-linked-lists/) | 2020/01/02 | Linked List, Two Pointers | 方法容易, 但要知道這方法 |
| LeetCode | :anchor: | Medium | [328. Odd Even Linked List](https://leetcode.com/problems/odd-even-linked-list/) | 2020/01/02 | Linked List | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :anchor: | Medium | [2. Add Two Numbers](https://leetcode.com/problems/add-two-numbers/) | 2020/01/01 | Linked List, Math | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: :rat: | Medium | [5. Longest Palindromic Substring](https://leetcode.com/problems/longest-palindromic-substring/) | 2019/12/04, 12/28-29, 2020/01/01 | String, DP | Fucking hard!! 耐心 + Coding Tracing |
|--------|--------|--------|--------|--------|--------|--------|--------|
| | | | []() | | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| 2019/10~2019/12 要把Web Dev Projects 收口完成，故每日一题刷題 | | | []() | | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: | Medium | [3. Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/) | 2019/12/03 | Array, String, Hash Table, Two Pointers, Sliding Window | 第3個方法有點花腦筋! |
| LeetCode | | Medium | [49. Group Anagrams](https://leetcode.com/problems/group-anagrams/) | 2019/12/01 | Array, String, Hash Table | 第二個方法有點煩人,但學了新 JS funcs |
| LeetCode | :star: | Medium | [73. Set Matrix Zeroes](https://leetcode.com/problems/set-matrix-zeroes/) | 2019/11/30 | Array | 有點花腦筋! |
| LeetCode | | Medium | [15. 3Sum](https://leetcode.com/problems/3sum/) | 2019/11/27 | Array, Two Pointers | |
| LeetCode | | Easy | [412. Fizz Buzz](https://leetcode.com/problems/fizz-buzz/) | 2019/11/26 | Math | |
| LeetCode | :anchor: | Easy | [155. Min Stack](https://leetcode.com/problems/min-stack/) | 2019/11/26 | Design | |
| LeetCode | | Medium | [384. Shuffle an Array](https://leetcode.com/problems/shuffle-an-array/) | 2019/11/26 | Design | |
| LeetCode | :star: | Easy | [198. House Robber](https://leetcode.com/problems/house-robber/) | 2019/11/23, 11/25 | Recursion, DP | |
| LeetCode | | Easy | [88. Merge Sorted Array](https://leetcode.com/problems/merge-sorted-array/) | 2019/11/22 | Array, Two Pointers | |
| LeetCode | :anchor: | Easy | [268. Missing Number](https://leetcode.com/problems/missing-number/) | 2019/11/22 | Bit Manipulation | Skill: XOR to remove duplicates |
| LeetCode | :anchor: | Easy | [190. Reverse Bits](https://leetcode.com/problems/reverse-bits/) | 2019/11/21 | Bit Manipulation | Skills: &, |, >>, <<. 解法難懂,但學會了好用 |
|--------|--------|--------|--------|--------|--------|--------|--------|
| 2019/10~2019/12 要把Web Dev Projects 收口完成，故每日一题刷題 | | | []() | | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Easy | [461. Hamming Distance](https://leetcode.com/problems/hamming-distance/) | 2019/10/18 | Bit Manipulation | |
| LeetCode | | Easy | [191. Number of 1 Bits](https://leetcode.com/problems/number-of-1-bits/) | 2019/10/18 | Bit Manipulation | |
| LeetCode | | Easy | [108. Convert Sorted Array to Binary Search Tree](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/) | 2019/10/18 | Tree, DFS, Recursion | |
| LeetCode | | Medium | [102. Binary Tree Level Order Traversal](https://leetcode.com/problems/binary-tree-level-order-traversal/) | 2019/10/17 | Tree, BFS | |
| LeetCode | | Easy | [101. Symmetric Tree](https://leetcode.com/problems/symmetric-tree/) | 2019/10/16 | Tree, DFS, BFS, Recursion | |
| LeetCode | :star: | Medium | [98. Validate Binary Search Tree](https://leetcode.com/problems/validate-binary-search-tree/) | 2019/10/15~10/16  | Tree, DFS, Recursion | 先學如何code traverse in DFS(preorder, inorder, postorder), BFS(level-order) |
| LeetCode | :star: | Easy | [104. Maximum Depth of Binary Tree](https://leetcode.com/problems/maximum-depth-of-binary-tree/) | 2019/10/14 | Tree, DFS, Recursion | |
| LeetCode | :star: | Easy | [234. Palindrome Linked List](https://leetcode.com/problems/palindrome-linked-list/) | 2019/10/13 | Linked List, Two Pointers | |
| LeetCode | :anchor: | Easy | [206. Reverse Linked List](https://leetcode.com/problems/reverse-linked-list/) | 2019/10/13 | Linked List | |
| LeetCode | | Easy | [21. Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists/) | 2019/10/12| Linked List | |
| LeetCode | :rat: | Medium | [19. Remove Nth Node From End of List](https://leetcode.com/problems/remove-nth-node-from-end-of-list/) | 2019/10/12| Linked List, Two Pointers | |
| LeetCode | | Easy | [237. Delete Node in a Linked List](https://leetcode.com/problems/delete-node-in-a-linked-list/) | 2019/10/12| Linked List | |
| LeetCode | :anchor: | Easy | [1. Two Sum](https://leetcode.com/problems/two-sum/) | 2019/10/12 | Array, Hash Table | |
| LeetCode | :anchor: | Easy | [66. Plus One](https://leetcode.com/problems/plus-one/) | 2019/10/11 | Array, Math Carry-Over | |
| LeetCode | | Easy | [350. Intersection of Two Arrays II](https://leetcode.com/problems/intersection-of-two-arrays-ii/) | 2019/10/10 | Hash Table, Two Pointers, Binary Search, Divide Divide and Conquer Conquer | 題目給的測試數據會誤導方向！|
| LeetCode | | Easy | [136. Single Number](https://leetcode.com/problems/single-number/) | 2019/10/09 | Hash Table, Bit Manipulation | |
| LeetCode | | Easy | [217. Contains Duplicate](https://leetcode.com/problems/contains-duplicate/) | 2019/10/09 | Array, Hash Table | |
| LeetCode | | Easy | [189. Rotate Array](https://leetcode.com/problems/rotate-array/) | 2019/10/09 | Array | |
| LeetCode | :muscle: | Medium| [8. String to Integer (atoi)](https://leetcode.com/problems/string-to-integer-atoi/) | 2019/10/08 | String, Math | |
| LeetCode | | Easy | [125. Valid Palindrome](https://leetcode.com/problems/valid-palindrome/) | 2019/10/08 | String, Two Pointers | |
| LeetCode | | Easy | [242. Valid Anagram](https://leetcode.com/problems/valid-anagram/) | 2019/10/08 | String, Hash Table, Sort | |
| LeetCode | | Easy | [387. First Unique Character in a String](https://leetcode.com/problems/first-unique-character-in-a-string/) | 2019/10/08 | String, Hash Table | |
| LeetCode | | Easy | [7. Reverse Integer](https://leetcode.com/problems/reverse-integer/) | 2019/10/07 | String | |
| LeetCode | | Easy | [344. Reverse String](https://leetcode.com/problems/reverse-string/) | 2019/10/06 | String | |
| LeetCode | :star: | Medium | [80. Remove Duplicates from Sorted Array II](https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/) | 2019/10/05 | Array, Two Pointers | 要有耐心，這只是國中數學! |
| LeetCode | | Easy | [26. Remove Duplicates from Sorted Array](https://leetcode.com/problems/remove-duplicates-from-sorted-array/) | 2019/10/05 | Array, Two Pointers | |
| LeetCode | :anchor: | Easy | [27. Remove Element](https://leetcode.com/problems/remove-element/) | 2019/10/04 | Array, Two Pointers | |
| LeetCode | :anchor: | Easy | [283. Move Zeroes](https://leetcode.com/problems/move-zeroes/) | 2019/10/03 | Array, Two Pointers | 不在行要把array elems in-place 移來移去的題目。多練習，做類似題：27. Remove Element |
| LeetCode | | Easy | [232. Implement Queue using Stacks](https://leetcode.com/problems/implement-queue-using-stacks/) | 2019/10/02 | Stack, Queue | Other sites called: Queue Using Two Stacks |
|--------|--------|--------|--------|--------|--------|--------|--------|
| 2019/10~2019/12 要把Web Dev Projects 收口完成，故每日一题刷題 | | | []() | | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| | | | []() | | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: | Easy | [53. Maximum Subarray](https://leetcode.com/problems/maximum-subarray/) | (x1) 2019/09/28 | Array, DP(Kadane's Algorithm), Divide & Conquer | |
| LeetCode | | Easy | [121. Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/) | 2019/09/28 | Array, DP(Kadane's Algorithm) | 未學 Kadane's Algorithm 的code |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [287. Find the Duplicate Number](https://leetcode.com/problems/find-the-duplicate-number/) | 2019/09/27 | Two Pointers, Binary Search(難懂) | 未學 Tortoise-Hare Algorithm 的code |
| LeetCode | :star: | Medium | [142. Linked List Cycle II](https://leetcode.com/problems/linked-list-cycle-ii/) | 2019/09/27 | Two Pointers:  Tortoise-Hare Algorithm (Floyd's Cycle Detection), Linked List | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Easy | [141. Linked List Cycle](https://leetcode.com/problems/linked-list-cycle/) | 2019/09/26 | Two Pointers, Linked List | 寫Linked List來測試是多餘!! |
| LeetCode | | Easy | [704. Binary Search](https://leetcode.com/problems/binary-search/) | 2019/09/26 | Recursion, Binary Search | |
| CtCI | | | [8.3 Magic Index]() | 2019/09/26 | Recursion, Binary Search | Not clear, see LeetCode 704. Binary Search, instead |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [63. Unique Paths II](https://leetcode.com/problems/unique-paths-ii/) | 2019/09/25 | Recursion, DP | |
| CtCI | | | [6.2 Robot in a Grid]() | 2019/09/25 | DP | Same as LeetCode 63. Unique Paths II |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | | Medium | [62. Unique Paths](https://leetcode.com/problems/unique-paths/) | 2019/09/24 | Recursion, DP | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :star: | Easy | [70. Climbing Stairs](https://leetcode.com/problems/climbing-stairs/) | 2019/09/23 | Recursion, DP | |
| CtCI | | | [8.1 Triple Steps]() | 2019/09/23 | Recursion, DP | Learn the DP part in LeetCode 70. Climbig Stairs |
|--------|--------|--------|--------|--------|--------|--------|--------|
| | | | []() | | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars| | 6kyu | [Words to Hex](https://www.codewars.com/kata/words-to-hex/) | 2019/09/17 | String, Unicode, Hex | 先做了前兩題就簡單些 |
| Codewars | | 5kyu | [Convert A Hex String To RGB](https://www.codewars.com/kata/convert-a-hex-string-to-rgb/) | Hex to Dec | | |
| Codewars | | 5kyu | [RGB To Hex Conversion](https://www.codewars.com/kata/rgb-to-hex-conversion/) | 2019/09/17 | Dec to Hex Conversion | |
| Codewars | | 5kyu | [Where my anagrams at?](https://www.codewars.com/kata/where-my-anagrams-at/) | 2019/09/17 | String | |
| Codewars | | 6kyu | [Anagram Difference](https://www.codewars.com/kata/anagram-difference/) | 2019/09/17 | String | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 6kyu | [Number of anagrams in an array of words](https://www.codewars.com/kata/number-of-anagrams-in-an-array-of-words/) | 不做 | | Bad instructions.  Skip. |
| Codewars | | 7kyu | [Anagram Detection](https://www.codewars.com/kata/anagram-detection/) | 2019/09/17 | String | |
| Codewars | :anchor: | 5kyu | [Flatten a Nested Map](https://www.codewars.com/kata/flatten-a-nested-map/) | 2019/09/17 | Recursion | Foundation: Codewars, FileFinder |
| Codewars | :anchor: | 6kyu | [File Finder](https://www.codewars.com/kata/file-finder/) | 2019/09/17 | Recursion | |
| Codewars | :anchor: | 6kyu | [Mutual Recursion](https://www.codewars.com/kata/mutual-recursion/) | 2019/09/17 | Recursion: Mutual | Shows a recursion is easy once a math formula is derived |
| Codewars | :anchor: :muscle: | 6kyu | [English Beggars](https://www.codewars.com/kata/english-beggars/train/javascript) | 2019/09/17 | Recursion | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | :anchor: | 6kyu | [Array Deep Count](https://www.codewars.com/kata/array-deep-count/) | 2019/09/16 | Recursion | |
| Codewars | | 6kyu | [Design a Simple Automaton (Finite State Machine)](https://www.codewars.com/kata/design-a-simple-automaton-finite-state-machine/) | 2019/09/16 | Recursion | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars |  | 7kyu | [Recursive Replication](https://www.codewars.com/kata/recursive-replication/) | 2019/09/15 | Recursion | |
| Codewars | :star: | 7kyu | [Greatest Common Divisor](https://www.codewars.com/kata/greatest-common-divisor/) | 2019/09/15 | Recursion | Using 輾轉相除法（歐幾里德算法）to find GCD |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars |  | 7kyu | [Recursion #2 - Fibonacci](https://www.codewars.com/kata/recursion-number-2-fibonacci/) | 2019/09/14 | Recursion |  |
| Codewars |  | 7kyu | [Recursion #1 - Factorial](https://www.codewars.com/kata/recursion-number-1-factorial/) | 2019/09/14 | Recursion |  |
| Codewars |  | 7kyu | [Sum of a sequence](https://www.codewars.com/kata/sum-of-a-sequence/) | 2019/09/14 | Recursion |  |
|--------|--------|--------|--------|--------|--------|--------|--------|
| LeetCode | :muscle: | Hard | [726. Number of Atoms](https://leetcode.com/problems/number-of-atoms/) | 2019/09/12~2019/09/13 | Parsing | LeetCode Std Sols: recursion(Bad), stack, regex |
| Codewars | :muscle:  | 5kyu  | [Molecule to atoms](https://www.codewars.com/kata/molecule-to-atoms/) | NOT YET | Parsing | 先做LeetCode 746.學regex to parse |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | :muscle: | 5kyu | [Calculating with Functions](https://www.codewars.com/kata/calculating-with-functions/) | 2019/09/11 | | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 2kyu | [Evaluate Mathematical Expression](https://www.codewars.com/kata/evaluate-mathematical-expression/) | NOT YET |  | 先做 LeetCode 43. 學stack來控制簡單算式 |
| Codewars | | 3kyu | [Calculator](https://www.codewars.com/kata/calculator/) | NOT YET |  | 先做 LeetCode 43. 學stack來控制簡單算式|
| LeetCode | :star: | Hard | [224. Basic Calculator](https://leetcode.com/problems/basic-calculator/) | 2019/09/10 | Stack, Math | 需要做 Approach 2: Stack and No String Reversal |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | :arrow_up: | 5kyu |[Maximum subarray sum](https://www.codewars.com/kata/maximum-subarray-sum/) | 2019/09/28 | Array, DP(Kadane's Algorithm), Divide & Conquer Conquer | Same as LeetCode 53. Maximum Subarray |
| Codewars | | 5kyu | [Valid Parentheses](https://www.codewars.com/kata/valid-parentheses/) | 2019/09/09 | | |
| Codewars | | 5kyu | [Sum of Pairs](https://www.codewars.com/kata/sum-of-pairs/train) | 2019/09/09| |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 5kyu | [ISBN-10 Validation](https://www.codewars.com/kata/isbn-10-validation/) | 2019/09/08 | |
| Codewars | | 6kyu | [Array Helper](https://www.codewars.com/kata/array-helpers/train/javascript) | 2019/09/08 | |
| Codewars | | 6kyu | [Duplicate Encoder](https://www.codewars.com/kata/duplicate-encoder/) | 2019/09/08 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 6kyu | [Word a10n (abbreviation)](https://www.codewars.com/kata/5375f921003bf62192000746) | 2019/09/07 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewats | | 6kyu | [Kebabize](https://www.codewars.com/kata/kebabize/) | 2019/09/06 | |
| Codewars | | 6kyu | [Multiplication Tables](https://www.codewars.com/kata/multiplication-tables) | 2019/09/06 | |
| Codewars | | 6kyu | [Street Fighter 2 - Character Selection](https://www.codewars.com/kata/street-fighter-2-character-selection) | 2019/09/06 | |
| Codewars | | 6kyu | [Sum The Tree](https://www.codewars.com/kata/sum-the-tree/) | 2019/09/06 | |
| Codewars | | 6kyu | [Data Reverse](https://www.codewars.com/kata/data-reverse/) | 2019/09/06 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 5kyu | [What's a Perfect Power anyway?](https://www.codewars.com/kata/whats-a-perfect-power-anyway/) | 2019/09/05 | |
| Codewars | | 5kyu | [int32 to IPv4](https://www.codewars.com/kata/int32-to-ipv4/) | 2019/09/05 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 5kyu | [Direction Reduction](https://www.codewars.com/kata/directions-reduction) | 2019/09/04 | |
| Codewars | | 4kyu | [Multiplying numbers as strings](https://www.codewars.com/kata/multiplying-numbers-as-strings) | NOT YET |  | 先做 LeetCode 43. 學乘法 |
| LeetCode | | Medium | [43. Multiply Strings](https://leetcode.com/problems/multiply-strings/) | 2019/09/04 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 5kyu | [Simple Pig Latin](https://www.codewars.com/kata/simple-pig-latin/javascript) | 2019/09/03 | |
| Codewars | |4kyu | [Matrix Determinant](https://www.codewars.com/kata/matrix-determinant) | 2019/09/02~09/03 | | [Need to work thr 优化8次和其他解法](https://www.jianshu.com/p/0fd8ac349b5e) |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 5kyu | [The Hashtag Generator](https://www.codewars.com/kata/52449b062fb80683ec000024) | 2019/09/02 | |
| Codewars | | 6kyu | [CamelCase Method](https://www.codewars.com/kata/camelcase-method) | 2019/09/02 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | | 5kyu | [Common Denominators](https://www.codewars.com/kata/common-denominators) | 2019/09/01 | |
| Codewars | | 6kyu | [Build Towers](https://www.codewars.com/kata/build-tower/) | 2019/09/01 | |
|--------|--------|--------|--------|--------|--------|--------|--------|
| Codewars | :star: | 6kyu | [The Supermarket Queue](https://www.codewars.com/kata/the-supermarket-queue) | 2019/08/30~08/31 |  |
|--------|--------|--------|--------|--------|--------|--------|--------|

------------------------------------------------------------------------------------------------

| 118 |  [Pascal's Triangle](https://leetcode.com/problems/pascals-triangle/description/) | 2018/06/26 | | Easy |
| 12 |  [Integer to Roman](https://leetcode.com/problems/integer-to-roman/description/) | 2018/05/07 | | Easy |
| 13 | [Roman to Integer](https://leetcode.com/problems/roman-to-integer/description/) | 2018/05/07 | | Easy |
| 242 |  [Valid Anagram](https://leetcode.com/problems/valid-anagram/description/) | 2018/05/01 | | Easy |
| 125 | [Valid Palindrome](https://leetcode.com/problems/valid-palindrome/description/) | 2018/04/28 | | Easy |
| 136 | [Single Number](https://leetcode.com/problems/single-number/description/) | 2018/04/28 | | Easy |
| 104 | [Maximum Depth of Binary Tree](https://leetcode.com/problems/maximum-depth-of-binary-tree/description/) | 2018/04/14 | | |
|  |   | | Havn't added iterative approach| Easy |
| 53 | [Maximum Subarray](https://leetcode.com/problems/maximum-subarray/description/) | 2018/04/14 | | Easy |
|  |   | 2018/04/26 | Added divide-and-conquer approach | |
| 20 | [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/description/) | 2018/04/14 | | Easy |
| 70 | [Climbing Stairs](https://leetcode.com/problems/climbing-stairs/description/) | 2018/04/12 | 2018/04/12 Added Appr #2-#4| Easy |
|  |   | | But haven't verified them yet! | |
| 1 | [Two Sum](https://leetcode.com/problems/two-sum/description/) | 2018/04/10 |  | Easy |
|  |  | 2018/04/12 | added Appr #2 & #3 | |

