'''
Edit distance (Word Ladder Problem)

https://www.tryexponent.com/questions/3891/edit-distance

'''


'''
ChatGPT Sol:

Explanation:
	1.	Edge Case Check: If endWord is not in wordList, return None.
	2.	BFS Setup: Use a queue to store the current word and the number of steps taken so far.
	3.	Word Transformation: For each word, generate all possible one-letter transformations and check if they exist in the word list.
	4.	Remove Used Words: Avoid revisiting words by removing them from the set once added to the queue.
	5.	Terminate on Match: If current_word == endWord, return the current step count.
	6.	Return None: If the queue is exhausted without finding a match, return None.
'''
from collections import deque

def word_ladder(wordList, beginWord, endWord):
    if endWord not in wordList:  # If endWord is not in the list, return None
        return None
                
    queue = deque([(beginWord, 0)])  # Queue stores tuples of (word, steps)
    wordList = set(wordList)  # Convert to a set for O(1) lookup

    while queue:
        current_word, steps = queue.popleft()

        # If the current word matches the endWord, return the step count
        if current_word == endWord:  return steps

        # Try changing each letter of the current word
        for i in range(len(current_word)):
            for ch in 'abcdefghijklmnopqrstuvwxyz':

                # Generate a new word by replacing one character
                new_word = current_word[:i] + ch + current_word[i + 1:]  
                
                # If the new word is in the word list, add it to the queue and
                # remove from wordList
                if new_word in wordList:
                    queue.append((new_word, steps + 1))
                    wordList.remove(new_word)  # Avoid revisiting the word      

    return None  # If no path is found, return None


# With print()
def word_ladder(wordList, beginWord, endWord):
    if endWord not in wordList:  # If endWord is not in the list, return None
        return None
                
    queue = deque([(beginWord, 0)])  # Queue stores tuples of (word, steps)
    wordList = set(wordList)  # Convert to a set for O(1) lookup
    print("queue:", queue)
    print("wordList:", wordList, "\n") 

    while queue:
        current_word, steps = queue.popleft()
        print("\n>>>>>current_word:", current_word, ", steps:", steps)

        # If the current word matches the endWord, return the step count
        if current_word == endWord:
            return steps

        # Try changing each letter of the current word
        for i in range(len(current_word)):
            print("\ni:", i)
            for ch in 'abcdefghijklmnopqrstuvwxyz':

                # Generate a new word by replacing one character
                
                #print("type of current_word[:i]:", type(current_word[:i]))
                print("current_word[:i]:", current_word[:i], ", ch:", ch, \
                        ", current_word[i + 1:]:", current_word[i + 1:])

                new_word = current_word[:i] + ch + current_word[i + 1:]  
                print("new_word:", new_word)
                
                # If the new word is in the word list, add it to the queue and
                # remove from wordList
                if new_word in wordList:
                    print("----->>>", new_word, " found in wordList set")
                    queue.append((new_word, steps + 1))
                    print("queue:", queue)
                    wordList.remove(new_word)  # Avoid revisiting the word      
                    print("wordList:", wordList) 

    return None  # If no path is found, return None

# Testing Code
beginWord = 'hit'
endWord = 'dot'
wordList = ["hit", "hot", "dot"]
print(word_ladder(wordList, beginWord, endWord))
# output: 2 # hit -> hot -> dot

beginWord = 'hit'
endWord = 'cog'
wordList = ["hit", "hot", "dot", "dog", "cog"]
#print(word_ladder(wordList, beginWord, endWord))
# output: 4 # hit -> hot -> dot -> dog -> cog

beginWord = 'word'
endWord = 'word'
wordList = ['word', 'ward']
#print(word_ladder(wordList, beginWord, endWord))
# output: 0 

beginWord = 'hit'
endWord = 'cog'
wordList = ["hit", "hot", "dot", "dog"]
#print(word_ladder(wordList, beginWord, endWord))
# output: None  # because no 'cog' in list
