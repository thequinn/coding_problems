'''
Flatten a Dictionary

https://www.tryexponent.com/practice/prepare/flatten-a-dictionary

ex1 = {
    "Key1": "1",
    "Key2": {
        "a": "2",
        "b": "3",
        "c": {
            "d": "3",
            "e": {
                "": "1"
            }
        }
    }
}

output: {
    "Key1" : "1",
    "Key2.a" : "2",
    "Key2.b" : "3",
    "Key2.c.d" : "3",
    "Key2.c.e" : "1"
}
'''


'''
Version #2:
=> tryexponent.com's sol 
=> Using Boolean tbl technique to handle 2 vars, initKey and curKey, in if-else statement (See: 技巧 section below)


思路:
- We divide the problem into 2 parts, then the problem is simplified:
    (i) For-loop + Recursion: 
        - For loop: handle layers of key-val pairs
        - Recursion: handle nested vals
    (ii) Handle diff conditions of parent & child keys (See next sec)


技巧: boolean tbl technique to handle 2 vars in if-else statement

(1) Using ex1. to analyze, there are 2 edge cases:
      Case 1: When initKey is "", we can't use "." to concat Key1. 
      Case 2: When curKey is "", excude it, still need to handle it's val

(2) Write out the logic in plain texts observing ex1:

if initKey is empty:
    (a) if curKey is Empty:     newKey = "" or curKey
    (b) if curKey NOT empty:    newKey = curKey    => Same operations, combine

if initKey NOT empty:
    (c) if curKey is Empty:     newKey = initKey
    (d) if curKey NOT Empty:    newKey = initKey + "." + curKey


(3) Boolean Tbl Technique:
- Based on (2), fill in the Result part of the Boolean Tbl

  initKey  |  curKey  |  Result
- - - - - - - - - - - - - - - - - - - - - - - - - - 
      T    |    T     | (d) newKey = initKey + "." + curKey    
- - - - - - - - - - - - - - - - - - - - - - - - - - 
      T    |    F     | (c) newKey = initKey
- - - - - - - - - - - - - - - - - - - - - - - - - - 
      F    |    T     | (a) (b) newKey = curKey
- - - - - - - - - - - - 
      F    |    F     |

(4) To simplify the if-else statement from version 1 to 2 b/t the lines, writing each if condition following the up-to-down order is crucial.
       

'''
from typing import Dict, Union
DeepNestedDict = Dict[str, Union[str, 'DeepNestedDict']]

def flatten_dictionary(dct: DeepNestedDict) -> Dict[str, str]:

    def dfs(initKey, dct):
        for curKey, curVal in dct.items():
            # No need it here
            #newKey = ""

            # - - - - - - - - - - - - - # - - - - - - - - - - - - - # 
            # 技巧: boolean tbl technique to handle 2 vars in if-else statement
            if initKey and curKey:  newKey = initKey + "." + curKey
            elif initKey:           newKey = initKey
            else:                   newKey = curKey
            # - - - - - - - - - - - - - # - - - - - - - - - - - - - # 

            # Base case: if cur node is a leaf node
            if isinstance(curVal, str):
                res[newKey] = curVal
                continue
            # Recursive case: if cur node has child(s)
            dfs(newKey, curVal)
            #
            '''Same as above code
            if isinstance(curVal, str):
                res[newKey] = curVal
            else:  dfs(newKey, curVal)
            '''


'''
Version #1: => Skip after I absorb Version #2's Boolean Tbl technique
=> Sol by QN
=> Using if-else statement to handle 2 vars 
 
'''
from typing import Dict, Union
DeepNestedDict = Dict[str, Union[str, 'DeepNestedDict']]
def flatten_dictionary(dct: DeepNestedDict) -> Dict[str, str]:

    def dfs(dct, initKey):
        
        newKey = ""
        for curKey, val in dct.items():
            newKey = ""

            # - - - - - - - - - - - - - # - - - - - - - - - - - - - # 
            # When initKey is the top-most node.  ex. Key1 in the ex
            if initKey == ".":  
                # When initKey is "".  ex. {'': {'a': '1'}, 'b': '3'}
                if curKey == "":  newKey = "."
                else:  newKey = curKey
            else:    
                # When a leaf node's key is "".
                if curKey == "":       
                    newKey = initKey      
                else:
                    newKey = initKey + "." + curKey
            # - - - - - - - - - - - - - # - - - - - - - - - - - - - # 

            # Base Case: val is str.  It means a leaf node is reached.
            if isinstance(val, str):
                res[newKey] = val
            # Recursive Case: val is {}
            else: 
                dfs(val, newKey)

    res = {}
    dfs(dct, ".")
    return res


# Test Case
ex1 = {
    "Key1": "1",
    "Key2": {
        "a": "2",
        "b": "3",
        "c": {
            "d": "3",
            "e": {
                "": "1"
            }
        }
    }
}

print(flatten_dictionary(ex1))
'''
output: {
    "Key1" : "1",
    "Key2.a" : "2",
    "Key2.b" : "3",
    "Key2.c.d" : "3",
    "Key2.c.e" : "1"
}
'''
