'''
Pramp Link:
  https://www.tryexponent.com/practice/prepare/root-of-number
[Root of Number]

Many times, we need to re-implement basic functions without using any standard library functions already implemented. For example, when designing a chip that requires very little memory space.

In this question we’ll implement a function root that calculates the n’th root of a number. The function takes a nonnegative number x and a positive integer n, and returns the positive n’th root of x within an error of 0.001 (i.e. suppose the real root is y, then the error is: |y-root(x,n)| and must satisfy |y-root(x,n)| < 0.001).

Don’t be intimidated by the question. While there are many algorithms to calculate roots that require prior knowledge in numerical analysis (some of them are mentioned here), there is also an elementary method which doesn’t require more than guessing-and-checking. Try to think more in terms of the latter.

Make sure your algorithm is efficient, and analyze its time and space complexities.


# - - - - - - - - - - - - #  - - - - - - - - - - - - # 
Examples:

input:  x = 7, n = 3
output: 1.913

input:  x = 9, n = 2
output: 3


=>QN's Notes:
  - Since it's the wording is confusing to me, I made the tests below to help.

# Test cases
# Test cases
print(root(9**10, 10))  # Output should be close to 9
print(root(9, 2))  # Output should be close to 3
print(root(7, 3))  # Output should be close to 1.913


# Edge cases: 

# n-th root of 0 is always 0
print(root(0, 2))  # 0

# n-th root of a negative number is Error
# - Remember to read the constraints given by a problem description

# - - - - - - - - - - - - #  - - - - - - - - - - - - # 


Constraints:

[time limit] 5000ms

[input] float x

0 ≤ x
[input] integer n

0 < n
[output] float
'''

# Recursive Sol by QN =>未: Review Binary Search's moving L/R boundaries...???
def root(x, n):
  def binary_search(l, r):
    mid = (l+r) / 2
    if abs(x - mid**n) < 0.001**3:
      return mid
    if mid**n > x:
      return binary_search(l, mid)
    return binary_search(mid, r)
  
  if x == 0:  return 0
  return binary_search(0, x)

# Recursive Sol from Interview Peer, Soel
def root(x, n):
  def binary_search(l, r):
    mid = (l+r) / 2
    if abs(x - mid**n) < 0.001:
      return mid
    if mid ** n > x:
      return binary_search(l, mid)
    return binary_search(mid, r)
  
  if x == 0:  return 0
  return binary_search(0, 10000000000)

# Test cases
print(root(9**10, 10))  # Output should be close to 9
print(root(9, 2))  # Output should be close to 3
print(root(7, 3))  # Output should be close to 1.913
print(root(0, 2))  # 0
