'''
Top K Elements

Description:
- Given an array of integers arr, and an integer k, return the k most frequent elements. You may return the answer in any order.


Data Structure to use:
- Using Python's Heap Queue, heapq - a min heap, which is a priority Queue



The time complexity of this function can be analyzed as follows:

Building the frequency dictionary takes (O(n)) time, where (n) is the length of the array.
Inserting an item into the heap is (O(\log k)), and you do this at most once for each unique element. If (m) is the number of unique elements, this part takes (O(m \log k)).
Extracting the top (k) elements from the heap takes (O(k \log m)), since each extraction is (O(\log m)) and you perform (k) extractions.
Assuming (m) is at most (n), the overall time complexity is (O(n + m \log k + k \log m)), which simplifies to (O(n + k \log k)) when (m = k).

The space complexity is (O(m)), which is the space required to store the frequency dictionary and the heap, and assuming (m = k), you get (O(k)).

'''
from heapq import heappush, heappop

def top_k_frequent(arr, k):
    char_freq = {}

    # Iter over the arr to add (ch, freq fo the ch) to the heap
    for ch in arr:
        char_freq[ch] = char_freq.get(ch, 0) + 1

    # Add (-freq, ch) to create a "reverse" min heap, which is a max 
    # and set a limit of k elems
    max_heap = []
    for ch, freq in char_freq.items():
        # Add to max_heap
        heappush(max_heap, (-freq, ch))
    # Get the k most freq elems by removing the top k elems from the heap
    res = []
    for i in range(k):
        freq, ch = heappop(max_heap)
        res.append(ch)
    return res
