#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h> 
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define BUFFERLENGTH 1024


// record packet
typedef struct {
  char   	data[BUFFERLENGTH];
  int   	length;  //32 bits
  short 	type;    //16 bits
} Records;


void fillRecords(Records *rPtr, short type, char *data);


int main(int argc, char *argv[])
{

    Records *recordsPtr = malloc(sizeof(Records));

    int sockfd = 0, n = 0, ok=1;
    char buff[BUFFERLENGTH];

    // Note: 
    // serv_addr is not the server clients connect to.
    // serv_addr is this current local machine, client.c
    struct sockaddr_in serv_addr; 

    struct servent* se;

    if(argc != 3)
    {
        printf("\n Usage: %s server_ip server_port\n",argv[0]);
        return 1;
    } 

    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    } 

    //memset(&serv_addr, '0', sizeof(serv_addr)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2])); 

    if (inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    } 
    
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("\n Error : Connect Failed \n");
       return 1;
    } 

    while (1) {
	memset(buff, '\0', sizeof(buff));	

      	printf("Enter your message: ");
    	fgets(buff, 1024, stdin);

	//Note:
        //add '\n' to detect the end of "exit\n" in buff[]
        if (strcmp(buff, "exit\n") == 0) { 
	  printf("Exiting client........\n");
	  break;	  
        }

        fillRecords(recordsPtr, 12, buff);

        //if use sizeof(recordsPtr), we get size of the ptr
        n = send(sockfd, recordsPtr, sizeof(Records), 0); //send to server
    	if (n < 0) {
          perror("send()");
          exit(1);
    	}
    
    } 
   
    printf("Exit client\n");
    close(sockfd);
    exit(0);
}

void fillRecords(Records *rPtr, short type, char *data)
{
  strcpy(rPtr->data, data); 
  rPtr->length  = strlen(data);
  rPtr->type = type;
}
