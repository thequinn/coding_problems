#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SENDER_PORT_NUM 5793
#define SENDER_IP_ADDR  "192.168.1.94"

#define BUFFERLENGTH 1024

// record packet
typedef struct {
  char   	data[BUFFERLENGTH];
  int   	length;  //32 bits
  short 	type;    //16 bits
} Records;


void printRecords(Records *rPtr);
void printHex(const char *s);

int main(int argc, char *argv[])
{
    Records *recordsPtr = malloc(sizeof(Records));

    int listenfd=0, connfd=0, n=0, yes=1;

    // IPv4 AF_INET sockets
    struct sockaddr_in serv_addr, client_addr;
    struct sockaddr_in serv_addr_bkup; 

    socklen_t len = sizeof(serv_addr); 
    char s_addr[INET_ADDRSTRLEN], buff[1024];
 
    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    inet_pton(AF_INET, SENDER_IP_ADDR, &(serv_addr.sin_addr)); 

    // Use htons to convert host byte order to network byte order
    serv_addr.sin_port = htons(SENDER_PORT_NUM);  

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

    // http://www.beej.us/guide/bgnet/output/html/multipage/syscalls.html#bind
    // When you re-run the server, sometimes you get a msg, "Address already in
    //   use."  It's b/c the connected socket wasn't completely disconnected. 
    // Use the following code to fix.  
    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
    	perror("setsockopt");
    	exit(1);
    } 

    // Get server IP and Port #
    if (getsockname(listenfd, (struct sockaddr *)&serv_addr_bkup, &len) == 0) {
       	inet_ntop(AF_INET, &(serv_addr_bkup.sin_addr), s_addr, INET_ADDRSTRLEN); 
        printf("Server IP:%s\n", s_addr);

        printf("Server Port: %d\n", ntohs(serv_addr.sin_port));
    }
    else {
        //return server addr
	inet_ntop(AF_INET, &(serv_addr.sin_addr), s_addr, INET_ADDRSTRLEN); 
        printf("Server IP:%s\n", s_addr);

        printf("Server Port: %d\n", ntohs(serv_addr.sin_port));
    }

    listen(listenfd, 10);

    connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 
    
    while (1) {	
      //memset(buff, '\0', sizeof(buff)); 

      //if use sizeof(recordsPtr), we get size of the ptr
      n = recv(connfd, recordsPtr, sizeof(Records), 0); //recv from client
      if (n < 0) {
        perror("recv()");
        exit(1);
      }
      if (n == 0) { //detect if client has closed its connection
	printf("Client closed its connection\n");
	break;
      }

      //printf("(%s)\n", recordsPtr->data);
      printRecords(recordsPtr); //print client msg

    }

    printf("Exit server\n");
    close(connfd);    
    exit(0);             
}

void printRecords(Records *rPtr)
{
  printf("\nRecords Info:\n");
  printf("type:   %d\n", rPtr->type);
  printf("length: %d\n", rPtr->length);
  printf("data in char: %s", rPtr->data);
  printf("data in hex: ");
  printHex(rPtr->data);
}

void printHex(const char *s)
{
  while(*s)
    printf("%02x", (unsigned int) *s++);
  printf("\n");
}
