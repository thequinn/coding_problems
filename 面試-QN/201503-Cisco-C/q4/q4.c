#include "stdio.h"
#include "stdlib.h"

int program = 0, version = 0, process = 0;
unsigned char flags = 0;

#define GOT_PROG 0x01
#define GOT_VERS 0x02
#define GOT_PROC 0x04

void parse(char *data);

void main()
{
  char s1[] = "1 2 3";
  char s2[] = "1 * 3";

  char s3[] = "* 2 3";
  char s4[] = "1 2 *";

  char s5[] = "1   2 3";
  
  parse(s5);
}

void parse(char *data)
{
    // Use malloc() to allocate memory in heap
    char *tmp = malloc(sizeof(tmp));  
   

    /* advance past whitespace */
    
    //replace (int) to (unsigned char)

    //while(isspace((int)*data)) data++;
    while(isspace((unsigned char)*data)) {
      data++;
    }
    
    if(*data != '*') {
        program = strtoul(data,&tmp,0);
        flags|=GOT_PROG;
    } 
    printf("1-flags:%o  %x\n", flags, flags);
    printf("1-tmp:%s, data:%s\n", tmp, data);


     if(*tmp == '\0') return;
    data=++tmp;
    if(*data != '*') {
        version = strtoul(data,&tmp,0);
        flags|=GOT_VERS;
    } else {
        tmp++;
    }
    printf("2-flags:%o  %x\n", flags, flags);
    printf("2-tmp:%s, data:%s\n", tmp, data);


    if(*tmp == '\0') return;
    data=++tmp;
    if(*data != '*') {
        process = strtoul(data,&tmp,0);
        flags|=GOT_PROC;
    }
    printf("3-flags:%o  %x\n", flags, flags);
    printf("3-tmp:%s, data:%s\n", tmp, data);
}
