#include "stdio.h"
#include "stdlib.h"

#define num_elements 8

void fill();
void swap();
void printList();

int **head;
int list[num_elements];

void main()
{
  int count=0;

  // allocat memory space on heap for **head (ptr to ptr)
  head = malloc(sizeof(void *) * num_elements);

  fill(list);
  printf("\nBefore swap():\n");
  printList();

  swap(list);
  printf("\nAfter swap():\n");
  printList();
}

void printList()
{
  int count = 0;

  *head = list; // move *head to top of list[]
  for (count; count < num_elements; count++) {
	printf("%d ", *(*head+count));
  } 
  printf("\n");
}

void fill()
{
  int i = 0;

  for (i; i<num_elements; i++) {
    
    // *head is a ptr pointing at the ith location of **head
    *head = &list[i];  
    list[i] = i; //same as: **head = i;

    *head++; // adv to next index of pointer array
  }
}

void swap() 
{
  int i=0, tmp=0; 
  int swap_times = num_elements / 2;
  int len = sizeof(list) / 4;

  *head = list; // move *head to top of list[]
  while (i < swap_times) {   
  
    //swap 2 elements using pointer indirection
    tmp = *(*head+i);
    *(*head+i) = *(*head+len-(i+1));
    *(*head+len-(i+1)) = tmp;

    i++;
  }
}
