#include "stdio.h"
#include "stdlib.h"

void AllocateMemory(int **buffer, size_t size)
{
    *buffer = NULL;
    *buffer = malloc(size);

    if (*buffer == NULL) {
      free(*buffer);
      printf("Error!\n");
    }

    //mallinfo(): retrieve the size of a memory allocated by malloc()
    printf("Memory allocated: %d bytes\n",mallinfo(*buffer));
}

int main()
{
    int *int_ptr = NULL;
    
    AllocateMemory(&int_ptr, sizeof(int_ptr));

    *int_ptr = 3;
    printf("%d\n", *int_ptr);

    return;
}

