#include <stdio.h>
#include <stdlib.h>

void print_triangle(int n)
{
  int i, j;

  int **tri;

  tri = malloc(n * sizeof *tri);
  for (i=0; i<n; i++) {
    //allocate colum in triangle shape, hench i+1
    tri[i] = malloc((i+1) * sizeof *tri[i]);
  }

  //a triangle is 2D, so need 2 for loops
  for (i=0; i<n; i++) {
    for (j=0; j<=i; j++) {

      //if the number is 1st or last in a row, they are all 1
      if (i==j || j==0) {
        if (i==j) {
          tri[i][j] = 1;
          printf("%d\n",tri[i][j]);
        }
        else {
          tri[i][j] = 1;
          printf("%d ",tri[i][j]);
          //printf("tri[%d][%d]:%d\n",i,j,tri[i][j]);
        }
      }
      else {
        tri[i][j] = tri[i-1][j-1] + tri[i-1][j];
        printf("%d ",tri[i][j]);

        //printf("<tri[%d][%d]:%d>\n",i,j,tri[i][j]);
      }      


    }
  }

}

int main()
{
  int rows=4;

  print_triangle(rows);

  return 0;
}


