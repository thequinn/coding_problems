#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Question:
//
// int is 8 bit, input = 0xAB, swap odd bits with even bits.
//
// ex.  Input:     b0 b1   b2 b3    ....    b6 b7
//      Output:    b1 b0   b3 b2    ...     b7 b6



//Swap odd and even bits
int swapBits(int x)
{
  //Declare binary numbers in C
  //- If you use GCC, you can use GCC extension (which is included in 
  //C++14 standard)
  int m = 0b10101010;
  int n = 0b01010101;
  
  return ( ((x << 1) & m) | ((x >> 1) & n) );
}

//Convert int to binary string
const char *int_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;

    //By observing the for loop, it uses 1 bit to & and then >> 1 bit and
    //append to b as output.thr all available bits of x, 
    for (z = 128; z > 0; z >>= 1) //128 = 0b10000000 => 8 bits
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

int main()
{ 
  //All 3 can be used in swapBits()
  int input = 122;  //122 = 0x74 = 0b01111010;
  //int input = 0x7A; 
  //int input = 0b01111010;
  
  int output;
          
  output = swapBits(input);

  printf("Result in Dec: %d\n", output);
  printf("Result in Hex: %x\n", output);
  printf("Result in Bin: %s\n", int_to_binary(output)); //%s: binary string
}
