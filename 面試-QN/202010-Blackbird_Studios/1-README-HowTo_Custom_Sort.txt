
非常重要!!!

- jessecurry's comments to my sorting team scores and team names in "separate"
  functions:
-- https://github.com/blackbird-studios/coding-exercise-thequinn/pull/2

votes.sort((t1, t2) => {
  if (t1.points > t2. points) return 1;
  if (t1. points < t2. points) return -1;

  // If the points are the same between both teams, sort alphabetically
  if (t1.name > t2. name) return 1;
  if (t1. name < t2. name) return -1;
});
