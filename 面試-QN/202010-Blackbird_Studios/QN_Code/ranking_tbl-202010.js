/*
 * Author    : Anna Sun
 *
 * File      : ranking-table.js
 * Dependency: npm install prompt-sync
 * Execution : node ranking-table.js
 * Exit      : exit
 *
 * Input files:
 *  - sample-input0.txt: empty file
 *  - sample-input1.txt: modified to have a very long team name.
 *  - sample-input2.txt: modified so the bottom teams have same pts after calc.
 *  - sample-input3.txt: similar to sample-input2.txt, but teams with same pts are not at the bottom.
 *
 * Output file format:
 *  - out-inputFileName.  ex. out-sample-input1.txt
 */

const prompt = require('prompt-sync')({sigint: true});
const fs = require('fs');

const readFile = path => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      resolve(data);
    })
  });
};

const writeFile = (path, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, err => {
      resolve('Write Success');
    });
  });
};

const parse = (s) => {
  let points = {};  // Store each team's final total pts

  // Split the string using separators: (1) new-line (2) comma
  let teams = s.split(/[\n|,]/);

  // Parse each pair of teams into tbl{} in each iteration
  let tbl = {};
  for (let i = 0; i < teams.length; i += 2, tbl = {}) {

    if (teams[i].length == 0)  continue;

    for (let j = i; j < i+2; j++) {
      let team = teams[j].trim().split(' ');

      // Parse a team name longer than 1 word
      let teamName = team[0];
      for (let k = 1; k < team.length - 1; k++) {
        teamName += ' ' + team[k];
      }
      // Parse the team's score
      tbl[teamName] = team[team.length - 1];
    }

    compareScores(points, tbl);
  }

  return points;
};

// Compare scores of 2 teams and assign pts based on the result
// -  winning: 3, tie: 1, loss: 0
const compareScores = (points, tbl) => {
  let teams = [], scores = [];

  // Parse the names and scores of the 2 teams into 2 arrs
  for (const [key, val] of Object.entries(tbl)) {
    teams.push(key);
    scores.push(val);
  }

  // Compare their scores and assign points to each team based on the result
  if (scores[0] == scores[1]) {
    points[ teams[0] ] ? points[ teams[0] ] += 1 : points[ teams[0] ] = 1;
    points[ teams[1] ] ? points[ teams[1] ] += 1 : points[ teams[1] ] = 1;
  }
  else if (scores[0] > scores[1]) {
    points[ teams[0] ] ? points[ teams[0] ] += 3 : points[ teams[0] ] = 3;
    points[ teams[1] ] ? points[ teams[1] ] += 0 : points[ teams[1] ] = 0;
  }
  else {
    points[ teams[0] ] ? points[ teams[0] ] += 0 : points[ teams[0] ] = 0;
    points[ teams[1] ] ? points[ teams[1] ] += 3 : points[ teams[1] ] = 3;
  }
};

// Sort points in ascending order
const sortPts = (points) => {
  let arr = [];

  for (let team in points) {
    arr.push( [team, points[team]] );
  }

  arr.sort((a, b) => {
    return b[1] - a[1];
  });

  return arr;
};

const sortNamesWithSamePts = (sortedPts) => {
  let arr = [];
  let preScore = -999;

  let mark = -1;  // To make the start when a team's score is same as the last
  let startIndex, endIndex;  // Mark the start & end of teams w/ same pts

  for (let i = 0; i < sortedPts.length; i++) {

    // If current team's score != previous team's score
    if (sortedPts[i][1] != preScore) {

      // If mark is 1, preTeam is the last team whose name needs to be sorted.
      if (mark == 1) {
        mark = -1;
        endIndex--;

        // Sort all team names stored in arr[]
        sortNames(arr);

        // Replace the result in sortedPts[]
        arr.forEach(e => {
          sortedPts.splice(startIndex++, 1, e);
        });
      }
      arr = [];
      arr.push(sortedPts[i]);
    }
    // If current team's score == previous team's score
    else {
      if (mark == -1) {
        mark = 1;
        startIndex = i - 1;
      }
      arr.push(sortedPts[i]);
    }

    preScore = sortedPts[i][1];
  }

  // If for loop ended, but mark == 1, it means the last chunk of teams have
  // same scores, but we haven't update their sorted names in sortedPts[]

  sortNames(arr);

  arr.forEach(e => {
    sortedPts.splice(startIndex++, 1, e);
  });
};

const sortNames = (arr) => {
  arr.sort((a, b) => {
    if (a < b)  return -1;
    if (a > b)  return 1;
    return 0;
  });

  return arr;
};

const giveRankings = (scores) => {
  let arr = [];
  let preScore = -999, rank = 0;
  let s = "";

  for (let i = 0; i < scores.length; i++) {

    // if curreut score is different from previous score, update rank
    if (scores[i][1] != preScore) {
      rank = arr.length + 1;
    }

    s = String(rank) + ". " + scores[i][0] + ", " + String(scores[i][1]);
    isPlural(scores[i][1]) ? s += ' pts\n' : s += ' pt\n';

    arr.push(s);
    preScore = scores[i][1];
  }

  return arr.join('');
};

// score = 1: singular point
// score = 1 or score > 1: plural points
const isPlural = (score) => {
  if (score != 1)  return true;
  return false;
};

//-------------------------------------------------------------------------

const writePath = './out-';

while (true) {
  const readPath = prompt('Enter input file name: ');

  if (readPath == 'exit')  break;

  readFile('./' + readPath)
    .then(val => {

      // If input file doesn't exist or is empty, return val to move control
      // to next then()
      if (typeof val === 'undefined')
        return val = 'Input file not exist!';
      if (!val)   return val;

      // Parse the read input and assign correct points to each team
      let points = parse(val);

      let sortedPts = sortPts(points);

      sortNamesWithSamePts(sortedPts);

      let rankings = giveRankings(sortedPts);
      return rankings;
    }, reason => {
      console.error(reason);
    })
    .then(val => {
      return writeFile(writePath + readPath, val);
    }, reason => {
      console.error(reason);
    })
}
