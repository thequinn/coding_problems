/*
How to run the code: 

    node filename.js
*/

var rankTbl = [{}]
//var outputTbl = []

// Get file name from stdin using prompt-sync module
/*const prompt = require('prompt-sync')();
var file = prompt('Please enter file name: ');
file = './test_files' + file . 
console.log(`${file} processed . . .`);
*/
// Tmply using this file name here
file = './test_files/sample-input2.txt'

// Use Node.js readline Module to read a local file
const readLine = require('readline')
const fs = require('fs')

var rl = readLine.createInterface({
    input: fs.createReadStream(file), 
    output: process.stdout, 
    terminal: false
});

// The 'line' event is emitted whenever the input stream receives an end-of-line// input (\n, \r, or \r\n).
rl.on('line', text => {

    const [score1, name1, score2, name2] = parseLine(text)

    // Calc num of pts a team gets after a match
    // - Calc the pts for both teams
    calcPts(score1, score2, name1, name2)

    // Update rankTbl
    // - Check if a team already exist in rankTbl.  If not, add it.  
        
    // Using my customized compare function to sore rankTbl[{}], and 
    // output to an external file
    //compareVals(, order = 'asc')
})


/*const compareVals = (key, order = 'asc') => {

}*/

const calcPts = (score1, score2, name1, name2) => {
    
    if (score1 === score2) {
        rankTbl[name1] += 1
        rankTbl[name2] += 1
    }
    else if  (score1 > score2) {
        rankTbl[name1] += 3
    }
    else { // (score1 < score2) 
        rankTbl[name2] += 3
    }
    console.log("rank_tbl:", rankTbl)
}

const parseNameAndScore = (team) => {
    var t = team.trim().split(' ')

    var score = t.pop()
    // Handle a team name w/ 2 words
    var name = t.join(' ')
    console.log(name, ": ", score)
    
    return [score, name]
}

const parseLine = (text) => {
    var teams = text.split(',')
    console.log("\nteam1:(", teams[0], "), team2:(", teams[1], ")")

    const [score1, name1] = parseNameAndScore(teams[0])
    const [score2, name2] = parseNameAndScore(teams[1])

    return [score1, name1, score2, name2]
}


