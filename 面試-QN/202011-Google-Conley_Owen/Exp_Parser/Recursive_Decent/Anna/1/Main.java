/*This code only clean up math expression and then do parsing like:

(1) What the original parse function does:

    Input:  33+ 2*11/11-4"

    Output: strArr[0]:33
            strArr[1]:+
            strArr[2]:2*11/11
            strArr[3]:-
            strArr[4]:4
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
    public static void main(String[] args) throws IOException {
        Calculator calc = new Calculator();

        //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //String input = br.readLine();

        //String input = "3+2-";
        //String input = "-3+2";

        String input = "33+ 2*11/11-4";
        //String input = "33+33+33+2*11/11-4";
        //String input = "33+33+33+2*11/11-4-4-4";

        //String input = "2*11/11-4+33";
        //String input = "2*11/11-4+33+33+33";

        //String input = "2*11*11-4+2*11*11+33";

        if (input==null) { System.out.println("Null input!");   }

        //remove un-needed chars
        input = calc.cleanup(input);
        System.out.println("cleanup():" + input);

        //parse into str array in such ex: "1", "+", "2*3/4*5"
        calc.parse(input);
        calc.printTokens();

        System.out.println("");
        int result = 0;
        result = calc.multi_div("11*22/22", result);
        System.out.println("Result of 11*22/22: "+result);
    }


}
