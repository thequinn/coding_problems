public class Calculator
{
    String[] strArr = new String[32];
    int numTokens = 0;

    public void parse(String s){
        String strToken="";
        char currCh;
        int tmp=0;

        int ok=0; //1: parsing parts like 2*3*2/5

        //ex: 33+ 2*11*2 -5
        //1: 2nd '+' or '-' op presents, meaning parts like 2*11*2 is done parsing
        int ok2=0;

        //if first char is a number
        for(int i=0; i<s.length(); i++){
            currCh=s.charAt(i);

            if (currCh>='0' && currCh<='9'){
                strToken += currCh; //concat char

                if (i==s.length()-1){ //parse last token
                    strArr[numTokens++] = strToken;
                }
            }
            if (currCh=='+' || currCh=='-'){

                //ex: 33+ 2*11/11-4
                //check if current op is 2nd '+' or '-'
                if ((tmp=numTokens-2)>=0) {
                    if (strArr[tmp].compareTo("+") == 0 || strArr[tmp].compareTo("-") == 0) {
                        ok2 = 1;
                    }
                }

                if (ok==1){ //just past last char of ex: 2*3*2/5
                    strArr[numTokens-1] = strToken; //the str before curr op
                    strToken="";
                    ok=0;

                    //ok=0 signals end of parsing parts like 2*3*2/5, now calculate it
                    //multi_div(strArr[numTokens-1], result);

                    strArr[numTokens++] = "" + currCh; //curr op

                    if (ok2==1){ //2nd '+' or '-' shows up
                        ok2=0;
                    }

                }
                else {
                    strArr[numTokens++] = strToken; //the number before curr op
                    strToken = "";

                    strArr[numTokens++] = "" + currCh; //current op
                }
            }
            if (currCh=='*' || currCh=='/'){
                if (ok==0) {
                    numTokens++;
                    strToken += currCh;
                    ok = 1; //on parsing parts like 2*3*2/5
                }
                else {
                    strToken += currCh;
                }
            }
        }

    }

    /*
    (1) What the original parse() function does:

    Input:  33+ 2*11/11-4"

    Output: strArr[0]:33
            strArr[1]:+
            strArr[2]:2*11/11
            strArr[3]:-
            strArr[4]:4

     (2) Modified to add the code below to handle blocks like: 2*11/11

            mul_div("2*11/11", result);

         Modified to add the code below to handle blocks like: 33+11-4

            add_sub("33+11-4", result);

     (3) I was thinking about calling mul_div() inside add_sub(), so the program
         is recursive decent (mutually recursive).  But it seems more reasonable
         to merge the 2 funcs to parse() for the strings currently evaluating.

         See next update for this part of code

     */
   public int add_sub(String s, int result){
       int index, n;

       //Base case: if length<=0
       if (s.length()<=0)   return result;

       //Recursive Case: if length>0
       else {   //ex: 33+(2*11/11)-4 => (2*11/11) is taken care of in mul_div()
           if ( (index=s.indexOf((int)'+')) > -1 ){
               //get number before '+' op
               n = Integer.parseInt(s.substring(0, index));

               //handle the substr after '+' op
               result = n + add_sub(s.substring(index+1), result);
           }
           else if ( (index=s.indexOf((int)'-')) > -1 ){
               n = Integer.parseInt(s.substring(0, index));
               result = n - add_sub(s.substring(index+1), result);
           }
           else {
               return n = Integer.parseInt(s);
           }
       }

       return result;
   } 

    //Helpful Tips:
    //  use debugger to trace to understand the flowing direction of this recursion
    public int mul_div(String s, int result){
        int index, n;

        //Base case:        if length<=0
        if (s.length()<=0)  return result;

        //Recursive case:   if length>0
        else {  //ex: 11*22/22
            if ( (index = s.indexOf((int) ('*')) ) > -1) { //if '*' found
                //get number before '*' op
                n = Integer.parseInt(s.substring(0, index));

                //handle the substr after '*' op
                result = n * (mul_div(s.substring(index + 1), result));
            }
            else if ( (index = s.indexOf((int) ('/')) ) > -1) { //if '/' found
                n = Integer.parseInt(s.substring(0, index));
                result = n / mul_div(s.substring(index + 1), result);
            }
            else {//if a number is the last component of the substring
                if ((s.indexOf((int) '*') == -1) || (s.indexOf((int) '/') == -1)) {
                    return n = Integer.parseInt(s);
                }
            }
        }

        return result;
    }

    public void printTokens() {
        System.out.println("\nPrint all tokens:");
        for (int i = 0; i < numTokens; i++)
            System.out.printf("strArr[%d]:%s\n", i, strArr[i]);
    }

    public void errCheck(String s){
        //if 1st char is not a number
        char c=s.charAt(0);
        if (! (c>='0' && c<='9') ){
            System.out.println("Invalid input!");
            return;
        }

        //if last char is not a number
        c=s.charAt(s.length()-1);
        if (! (c>='0' && c<='9') ){
            System.out.println("Incomplete input!");
            return;
        }
    }
    //remove all chars other than '0-9' and '+-*/'
    public String cleanup(String s){
        String regex = "[^0-9/*+-]"; //exclude 0-9, /*+- math operators
        //put '-' at the end of regex so it isn't treated as a range like 0-9

        String s2 = s.replaceAll(regex, "");

        return s2;
    }
}
