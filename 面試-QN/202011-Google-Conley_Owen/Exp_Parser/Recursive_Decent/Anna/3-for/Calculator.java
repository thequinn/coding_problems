public class Calculator
{
    String[] strArr = new String[32];
    int numTokens = 0;

    /*
    (1) What the original parse() function does:

    Input:  33+ 2*11/11-4"

    Output: strArr[0]:33
            strArr[1]:+
            strArr[2]:2*11/11
            strArr[3]:-
            strArr[4]:4

     (2) Modified to add the code below to handle blocks like: 2*11/11

            mul_div("2*11/11", result);

         Modified to add the code below to handle blocks like: 33+11-4

            add_sub("33+11-4", result);

     (3)

     */

    public int parse(String s, int result){
        int index;
        if ( (index=s.indexOf((int)'+'))>-1 || (index=s.indexOf((int)'-'))>-1 ){
            result = add_sub(s, result, 0, 0);
        }
        else { //if ( (index=s.indexOf((int)'*'))>-1 || (index=s.indexOf((int)'/'))>-1 ){
            result = mul_div(s, result, 0, 0);
        }

        return result;
    }

    public int add_sub(String s, int result, int index, int ok){
        int n;
        char c;

        //Base case: if length<=0
        if (s.length()<=0)   return result;

        //Recursive Case: if length>0
        else {
          c = s.charAt(index);

          if (!Character.isDigit(c)){ //if curr char is a digit
            if (ok==1){
                return n=Integer.parseInt(s);
            }

          }
          else {        //if curr char is +, -, *, /
              ok=1; //first number has past

              if (c == '+') {
                  //get number before '+' op
                  n = Integer.parseInt(s.substring(0, index));

                  //handle the substr after '+' op
                  result = n + add_sub(s.substring(index + 1), result, index, ok);
              } else if (c == '-') {
                  n = Integer.parseInt(s.substring(0, index));
                  result = n - add_sub(s.substring(index + 1), result, index, ok);
              } else if (c == '*' || c == '/') {
                  result = mul_div(s, result, index);

                  //if next op is '+' or '-'
                  c = s.charAt(++index);
                  if (c == '+' || c == '-') {
                      String s2 = "" + result;
                      String s3 = s2.concat(s.substring(index));
                      return add_sub(s3, result, index, ok);
                  }
              } /*else {
                  System.out.println("-----" + s);
                  return n = Integer.parseInt(s);
              }*/
          }
        }

        return result;
    }

    //Helpful Tips:
    //  use debugger to trace to understand the flowing direction of this recursion
    public int mul_div(String s, int result, int index){
        int n;
        char c;

        //Base case:        if length<=0
        if (s.length()<=0)  return result;

        //Recursive case:   if length>0
        else {
            c = s.charAt(index);

            if (c=='*'){
                //get number before '*' op
                n = Integer.parseInt(s.substring(0, index));

                //handle the substr after '*' op
                result = n * mul_div(s.substring(index + 1), result, index);

                //if next op is '+' or '-'
                c = s.charAt(++index);
                if (c=='+' || c=='-'){
                    String s2 = "" + result;
                    String s3 = s2.concat(s.substring(index));
                    return add_sub(s3, result, index);
                }
            }
            else if (c=='/'){
                n = Integer.parseInt(s.substring(0, index));
                result = n / mul_div(s.substring(index + 1), result, index);

                //if next op is '+' or '-'
                c = s.charAt(++index);
                if (c=='+' || c=='-'){
                    String s2 = "" + result;
                    String s3 = s2.concat(s.substring(index));
                    return add_sub(s3, result, index);
                }
            }
            else {//if a number is the last component of the substring
                return n = Integer.parseInt(s);
            }
        }

        return result;
    }
/*
    public void printTokens() {
        System.out.println("\nPrint all tokens:");
        for (int i = 0; i < numTokens; i++)
            System.out.printf("strArr[%d]:%s\n", i, strArr[i]);
    }
*/

    public void errCheck(String s){
        //if 1st char is not a number
        char c=s.charAt(0);
        if (! (c>='0' && c<='9') ){
            System.out.println("Invalid input!");
            return;
        }

        //if last char is not a number
        c=s.charAt(s.length()-1);
        if (! (c>='0' && c<='9') ){
            System.out.println("Incomplete input!");
            return;
        }
    }

    //remove all chars other than '0-9' and '+-*/'
    public String cleanup(String s){
        String regex = "[^0-9/*+-]"; //exclude 0-9, /*+- math operators
        //put '-' at the end of regex so it isn't treated as a range like 0-9

        String s2 = s.replaceAll(regex, "");

        return s2;
    }

    //check if the input is valid math expression
    //public void validate(String s){

}
