public class Calculator
{
    String[] strArr = new String[32];
    int numTokens = 0;

    public void parse_old(String s){
        String strToken="";
        char currCh;
        int tmp=0;

        int ok=0; //1: parsing parts like 2*3*2/5

        //ex: 33+ 2*11*2 -5
        //1: 2nd '+' or '-' op presents, meaning parts like 2*11*2 is done parsing
        int ok2=0;

        //if first char is a number
        for(int i=0; i<s.length(); i++){
            currCh=s.charAt(i);

            if (currCh>='0' && currCh<='9'){
                strToken += currCh; //concat char

                if (i==s.length()-1){ //parse last token
                    strArr[numTokens++] = strToken;
                }
            }
            if (currCh=='+' || currCh=='-'){

                //ex: 33+ 2*11/11-4
                //check if current op is 2nd '+' or '-'
                if ((tmp=numTokens-2)>=0) {
                    if (strArr[tmp].compareTo("+") == 0 || strArr[tmp].compareTo("-") == 0) {
                        ok2 = 1;
                    }
                }

                if (ok==1){ //just past last char of ex: 2*3*2/5
                    strArr[numTokens-1] = strToken; //the str before curr op
                    strToken="";
                    ok=0;

                    //ok=0 signals end of parsing parts like 2*3*2/5, now calculate it
                    //multi_div(strArr[numTokens-1], result);

                    strArr[numTokens++] = "" + currCh; //curr op

                    if (ok2==1){ //2nd '+' or '-' shows up
                        ok2=0;
                    }

                }
                else {
                    strArr[numTokens++] = strToken; //the number before curr op
                    strToken = "";

                    strArr[numTokens++] = "" + currCh; //current op
                }
            }
            if (currCh=='*' || currCh=='/'){
                if (ok==0) {
                    numTokens++;
                    strToken += currCh;
                    ok = 1; //on parsing parts like 2*3*2/5
                }
                else {
                    strToken += currCh;
                }
            }
        }

    }

    /*
    (1) What the original parse() function does:

    Input:  33+ 2*11/11-4"

    Output: strArr[0]:33
            strArr[1]:+
            strArr[2]:2*11/11
            strArr[3]:-
            strArr[4]:4

     (2) Modified to add the code below to handle blocks like: 2*11/11

            mul_div("2*11/11", result);

         Modified to add the code below to handle blocks like: 33+11-4

            add_sub("33+11-4", result);

     (3) I was thinking about calling mul_div() inside add_sub(), so the program
         is recursive decent (mutually recursive).  But it seems more reasonable
         to merge the 2 funcs to parse() for the strings currently evaluating.

         See next update for this part of code

     */

    public int parse(String s, int result){
        //find 1st non-digit, so we know either add_sub() or mul_div() should start
        int index = firstNonDigit(s, 0);

        //if input str only has 1 number, return immediately
        if (index==s.length()){
            return Integer.parseInt(s);
        }

        char c = s.charAt(index);
        if (c=='+' || c=='-'){
            result = add_sub(s, result, '#');
        }
        else if (c=='*' || c=='/'){
            result = mul_div(s, result);
        }
        else {
            System.out.println("c is not an op, -------c:"+c);
            result = -999999999;
        }

        return result;
    }

    public int firstNonDigit(String s, int index){
        int i=0;
        char c;

        while (index<s.length()) {
            if (! Character.isDigit(s.charAt(index)) ){
                break;
            }
            index++;
        }
/*
        //if index out of bound
        if (index==s.length()) {
            index--;
        }
*/
        return index;
    }

    public int nextAddSub(String s, int index){
        int i=0;
        char c;

        while (index<s.length()) {
            if (s.charAt(index)!='+' || s.charAt(index)!='-'){
                index++;
            }
            else    return index;
        }
        if (index == s.length())    index = -1;
        return index;
    }

    public int nextMulDiv(String s, int index){
        int i=0;
        char c;

        System.out.println("nextMulDiv() s:"+s);
        while (index<s.length()) {
            if (s.charAt(index)!='*' || s.charAt(index)!='/'){
               index++;
            }
            else    return index;
        }
        if (index == s.length())    index = -1;
        return index;
    }

    //calculate the length/amount of digits of a number
    int numDigits(int n) {
        if (n < 10) return 1;
        return 1 + numDigits(n / 10);
    }

    //lastOp:
    //- (1) memorize the '+' or '-' before a block of ex: 22*11/11 in add_sub()
    //- (2) memorize the '+' or '-' after a block of ex: 22*11/11 in mul_div()
    public int add_sub(String s, int result, char lastOp){
        int n, index, newIndex;
        char c;

        System.out.println("\nadd_sub() s:"+s);

        //Base case: if length<=0
        if (s.length()<=0)   return result;

        //Recursive Case: if length>0
        else {   //ex: 33+(2*11/11)-4 => (2*11/11) is taken care of in mul_div()

            //find 1st non-digit char
            index = firstNonDigit(s, 0);

            //check for index out of bound
            //if the following condition true, the last number is reached
            if (index==s.length()){
                return n = Integer.parseInt(s);
            }
            c = s.charAt(index);
            System.out.println("add_sub() op c:"+c);

            if (c=='+'){
                //lastOp = '+';

                //get number before '+' op
                n = Integer.parseInt(s.substring(0, index));
                if (lastOp=='-')    n *= -1;

                //handle the substr after '+' op
                result = n + add_sub(s.substring(index + 1), result, '#');
            }
            else if (c=='-'){
                //lastOp = '-';
                n = Integer.parseInt(s.substring(0, index));
                if (lastOp=='-')    n *= 1;

                result = n - add_sub(s.substring(index+1), result, '#');
            }
            else if (c=='*' || c=='/'){

                //when it returns, the next available op would be either '+' or '-'
                result = mul_div(s, result);
                System.out.println("add_sub() result after mul_div():"+result);

                index = nextAddSub(s,0); //find 1st '+' or '-' of the substring
                c = s.charAt(index);
                System.out.println("add_sub() c:"+c);

                if (c=='+' || c == '-') {
                    String s2 = Integer.toString(result);
                    s2 = s2.concat(s.substring(index));

                    if (lastOp=='+')
                        result += add_sub(s2, result, lastOp);
                    if (lastOp=='-')
                        result -= add_sub(s2, result, lastOp);
                }

            }
            else {
                return n = Integer.parseInt(s);
            }
        }

        return result;
    }

    public int mul_div(String s, int result){
        int index, n;
        char c;

        System.out.println("\nmul_div() s:"+s);

        //Base case:        if length<=0
        if (s.length()<=0)  return result;

        //Recursive case:   if length>0
        else {  //ex: 11*22/22

            //find 1st non-digit char
            index = firstNonDigit(s, 0);
            c = s.charAt(index);

            if (c=='*') {
                //get number before '*' op
                n = Integer.parseInt(s.substring(0, index));
                System.out.println("* n:"+n);

                //handle the substr after '*' op
                result = n * (mul_div(s.substring(index + 1), result));
            }
            else if (c=='/'){
                n = Integer.parseInt(s.substring(0, index));
                System.out.println("/ n:"+n);
                result = n / mul_div(s.substring(index+1), result);
            }
            else if (c=='+'){
                n = Integer.parseInt(s.substring(0, index));
                System.out.println("+ n:"+n);

                System.out.println("+ substr:"+s.substring(index+1));
                result = add_sub(s.substring(index+1), result, '+');

                //find next '*' or '/'
                index = nextMulDiv(s.substring(index+1), 0);
                System.out.println("-----mul_div() + index:"+index);
            }
            else if (c=='-'){
                n = Integer.parseInt(s.substring(0, index));
                System.out.println("- n:"+n);

                System.out.println("- substr:"+s.substring(index+1));
                result = add_sub(s.substring(index+1), result, '-');

                //find next '*' or '/'
                index = nextMulDiv(s.substring(index+1), 0);
                System.out.println("-----mul_div() - index:"+index);
            }
            else { //if a number is the last component of the substring
                n = Integer.parseInt(s);

                System.out.println("else n:"+n);
                return n;
            }
        }

        return result;
    }

/*    public void printTokens() {
        System.out.println("\nPrint all tokens:");
        for (int i = 0; i < numTokens; i++)
            System.out.printf("strArr[%d]:%s\n", i, strArr[i]);
    }
*/
    public void errCheck(String s){
        //if 1st char is not a number
        char c=s.charAt(0);
        if (! (c>='0' && c<='9') ){
            System.out.println("Invalid input!");
            return;
        }

        //if last char is not a number
        c=s.charAt(s.length()-1);
        if (! (c>='0' && c<='9') ){
            System.out.println("Incomplete input!");
            return;
        }
    }
    //remove all chars other than '0-9' and '+-*/'
    public String cleanup(String s){
        String regex = "[^0-9/*+-]"; //exclude 0-9, /*+- math operators
        //put '-' at the end of regex so it isn't treated as a range like 0-9

        String s2 = s.replaceAll(regex, "");

        return s2;
    }
}
