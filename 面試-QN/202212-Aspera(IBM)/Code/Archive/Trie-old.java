//import java.lang.*;

class Node
{
  char ch;
  Node[] next;
  int endOfWord; 
  Node prev;
  
  //public Node(Node node, String left, String right) 
  public Node(char letter)
  {
    ch = letter;
    next = new Node[26]; // next level of tree 
    
    endOfWord = 0; 	 // Not a leaf
    prev = null; // Reference to parent node
  }
}

class Trie
{
  private Node root = null;

  public void insertWord(String str, Node node)
  //public void insertWord(String str)
  {
    Node tmpNode = node;
    int treeLevel = 0; // root level (1st char of str): 0
    int arrIndex = 0; // index of Node.next['a']: 0

    // If this branch is null
    if (tmpNode == null) {
      // Convert char to index number by substracting ASCII number ('a'=97)
      arrIndex = (int)str.charAt(treeLevel) - 97;

      // Move tmpNode to point to the newly made node
      root = new Node(str.charAt(treeLevel)); 
      tmpNode = root;
      printNode("0-tmpNode:", treeLevel, arrIndex);

/*      treeLevel++;
      arrIndex = (int)str.charAt(treeLevel) - 97;
      printNode("1-tmpNode:", treeLevel, arrIndex);
*/
    }

    // If this branch NOT null
    // While current treeLevel's tmpNode char matches char of str[treeLevel],
    // traverse until such matches not happens
    while (tmpNode != null) { 

     if (tmpNode.ch == str.charAt(treeLevel)) {
      treeLevel++;
      arrIndex = (int)str.charAt(treeLevel) - 97;
      printNode("1-tmpNode:", treeLevel, arrIndex);
      
      // Move tmpNode to point to the correct index of next level node
      tmpNode = tmpNode.next[arrIndex];
     }
     else {
	System.out.println("len: "+ (str.length()-1) );

      // Handle the rest of the word not added to Trie
      while ((str.length() - 1) >= treeLevel)  {
        tmpNode = new Node(str.charAt(treeLevel));
        printNode("2-tmpNode:", treeLevel, arrIndex);
        treeLevel++;
      }
      break;
     } // End of if-else

    }

      // Handle the rest of the word not added to Trie
      while ((str.length() - 1) >= treeLevel)  {
        tmpNode = new Node(str.charAt(treeLevel));
        printNode("3-tmpNode:", treeLevel, arrIndex);
        treeLevel++;
      }
    System.out.println("---------------");
  } // End of insertWord

  public void printNode(String s, int i, int j)
  {
    System.out.println(s+" treeLevel:"+i+" arrIndex:"+j);
  }

  // Return index of the 1st char of the str deducted by prefix
  public int indexOfSubStr(String str, String prefix)
  {
    // If prefix exists
    if (prefix != null) {
      if (str.startsWith(prefix)) {
	int len = prefix.length();
	System.out.println("length: "+len);

	return len-1;
      }
    }

    // If prefix NOT exists, return -1
    System.out.println("length: -1");
    return -1;
  }

}   

