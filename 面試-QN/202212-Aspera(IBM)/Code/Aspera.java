import java.io.*;

public class Aspera
{
  public Aspera()  {}

  public static void main(String args[]) throws IOException
  {
    Aspera asp = new Aspera();

    Tree tree = new Tree(); 
    //Node[] node = new Node[26]; // Create 26 branches for 26 English letters
    //TrieNode root = new TrieNode();

    String inputFile, word = "";
    int index;
    
    // Read input text file from cmd
    //System.out.println("Enter input file name: ");
    //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    //inputFile=br.readLine(); 
    inputFile = "t.txt";
 
    // Read line-by-line from the input text file
    BufferedReader br2 = new BufferedReader(new FileReader(inputFile));
    for (String line; (line=br2.readLine()) != null; ) {
	
      // Identify the branch (Node[] index) started w/ the 1st char of the str
      // Convert char to index number by substracting ASCII number ('a'=97)
      index = (int)line.charAt(0) - 97;

      tree.insertWord(line);
/*      if (tree != null) {
	System.out.println("node-1:"+tree.ch);
      }
      if (tree == null) {
	System.out.println("node-1 null");
      }
*/
System.out.println("--------------------");

      //root.insertString(root, line);
      //root.printSorted(root, "");
	 	
    } // End of for

    
    word = tree.printWords();
    System.out.println("word:"+word);


  } // End of main()

}
