//import java.lang.*;

class Node
{
  char ch;
  Node[] next;
  int endOfWord; 
  Node prev;
  
  //public Node(Node node, String left, String right) 
  public Node(char letter)
  {
    ch = letter;
    next = new Node[26]; // next level of tree 
    
    endOfWord = 0; 	 // Not a leaf
    prev = null; // Reference to parent node
  }
}

class Tree
{
  private Node root = null; 

  public void insertWord(String str)
  {
    Node tmpNode;
    int treeLevel = 0; // root level (1st char of str): 0
    int arrIndex = 0; // index of Node.next['a']: 0

    // If this branch is null
    if (root == null) {
      arrIndex = (int)str.charAt(treeLevel) - 97;

      // Move tmpNode to point to the newly made node
      root = new Node(str.charAt(treeLevel)); 
      tmpNode = root;
      printNode(tmpNode.ch, treeLevel, arrIndex);

      // Move tmpNode to point to the next level of node
      treeLevel++;
      arrIndex = (int)str.charAt(treeLevel) - 97;
      tmpNode = tmpNode.next[arrIndex];
    }

    // While current treeLevel's tmpNode char matches char of str[treeLevel],
    // traverse until such matches not happens
/*    while (tmpNode != null) { 
     if (tmpNode.ch == str.charAt(treeLevel)) {
      treeLevel++;
      arrIndex = (int)str.charAt(treeLevel) - 97;
      
      // Move tmpNode to point to the correct index of next level node
      tmpNode = tmpNode.next[arrIndex];
     }
     else {
      System.out.println("len: "+ (str.length()-1) );
      addNodes(tmpNode, str, treeLevel, arrIndex);
      //break;
      return;
     } // End of if-else
    }
    addNodes(tmpNode, str, treeLevel, arrIndex);
*/
  } // End of insertWord

  public String printWords()
  {    
    Node tmpNode = root;
    String s = new String();

    if (tmpNode != null) {
      //s = Character.toString(tmpNode.ch);
      //str.concat(s);
      System.out.println("tmp.ch:("+tmpNode.ch+")");
    } 
    else { return "nuuuull"; }
/* 
    if (tmpNode == null) { return null; }
    if (tmpNode.endOfWord == 1)	{ 
      //start a new word to concat()  
      return str; 
    }
    for (int i = 0; i < 26; i++) {
      printWords(tmpNode.next[i], str);
      i++;
    } 
*/
    return "nuuuull-2";
  }

  // Handle the rest of the word not added to Tree 
  public void addNodes(Node tmpNode, String str, int tLevel, int aIndex) 
  {
    int level = tLevel;
    //int index = aIndex;

    while ((str.length() - 1) >= level)  {
      tmpNode = new Node(str.charAt(level));
      printNode(tmpNode.ch, level, aIndex);
      level++;
    }
 
    tmpNode.endOfWord = 1; // Mark as endof word
  }

  public void printNode(char c, int i, int j)
  {
    System.out.println(c+"  treeLevel:"+i+"  arrIndex:"+j);
  }

  // Return index of the 1st char of the str deducted by prefix
  public int indexOfSubStr(String str, String prefix)
  {
    // If prefix exists
    if (prefix != null) {
      if (str.startsWith(prefix)) {
	return prefix.length() - 1;
      }
    }

    // If prefix NOT exists, return -1
    return -1;
  }

}   

