/*
Interview Company: Zayo Group


Similar problem: 11. Container With Most Water

Approach: Two Pointers
*/

const numberOfCrates = (h) => {
  // No need to check if b/c ln-15 the while loop's condition takes care of it
  //if (h.length <= 2) return 0; 
    
  let maxCrates = 0, curCrates = 0;
  let l = 0, r = h.length - 1;

  // When two trees don't overlap, keep looping
  while (r > l) {
    // Number of crates to store is affected by the lower side of the tree
    let minH = Math.min(h[l], h[r]);
    
    // Sum of space when no trees are between l and r ptrs  
    let sum_without_trees = minH * (r - l - 1);

    // Sum of tree heights between l and r ptrs
    let sum_of_trees = h.slice(l+1, r).reduce((acc, val) => {
      val = Math.min(minH, val);
      return acc + val;
    }, 0);

    // Space available to store crates
    curCrates = sum_without_trees - sum_of_trees;
            
    maxCrates = Math.max(maxCrates, curCrates);
           
    if (h[l] < h[r])    l++;
    else r--;
  }
  return maxCrates;    
}

//-----Test Cases-----
let TreeHeights = [2,2,1];  // 0
//let TreeHeights = [6,5,4,3,2,1,5];  // 10
//let TreeHeights = [3,2,1,2];  // 1
//let TreeHeights = [6,5,1,1,6,1,5]; // 12

let res = numberOfCrates(TreeHeights);
console.log("\nres:", res);


