/*
Interview Company: Zayo Group
*/

function minimumEdits(s) {
  let map = {};
  let pre = s.charAt(0);

  for (let i = 0; i < s.length; i++) {
    let c = s.charAt(i);

    // If a char NOT exists in map{}, init its val to 1, otherwise, increment
    // its val by 1
    //
    /*** Number of ways to check if a property exist in obj ***/
    //if (!map.hasOwnProperty(c))   // 法一
    //if (map[c] == undefined)      // 法二
    if ( !(c in map) )              // 法三
      map[c] = 1;
    else  map[c] += 1;
  }
 
  // If there are less than 3 discinct types of letters, return 0 
  let len = Object.keys(map).length;
  if (len < 3)    return 0;
    
  // Get the smallest val in map{}
  let min = 999;
  for (let val of Object.values(map)) {
    if (min > val)  min = val;
  }
  return min;
}

//-----Test Cases-----
//let s = 'aaa';  // 0
//let s = 'aaabcc'; //  Return 1 b/c we remove 'b' once
//let s = 'aaaabbbbbeee';  // 3, remove 'e' 3 times
//let s = 'abbbcc';  // 1, remove 'a' once
//let s = 'aaabbbccc';  // 3, remove 'a' or 'b' or 'c' 3 times

//let s = 'aabbcd';  // 1
//---> Anna made the case, Does Stephan think the returned val is correct?

let s = 'abbcd';
// 1 ---> Anna made the case, Does Stephan think the returned val is correct?

let res = minimumEdits(s);
console.log("\nres:", res);

//-----HackerRank Driver-----
function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);
    const s = readLine();
    const result = minimumEdits(s);
    ws.write(result + '\n');
    ws.end();
}

