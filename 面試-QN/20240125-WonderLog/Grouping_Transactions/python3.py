#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'groupTransactions' function below.
#
# The function is expected to return a STRING_ARRAY.
# The function accepts STRING_ARRAY transactions as parameter.
#

from collections import Counter
import operator

def groupTransactions(transactions):
    # Write your code here

    # - - - - - - - - - - - - # - - - - - - - - - - - - 
    # Step #1:
    
    # Counter()
    # - a subclass of dict designed for counting hashable objects.
    # - It's a dictionary that stores objects as keys and counts as values. 
    c = Counter(transactions)
    #print("c:", c)

    # Convert from a Counter to a dict
    d = dict(c)
    #print("d:", d)
   
    # - - - - - - - - - - - - # - - - - - - - - - - - - 
    '''
    Step #2:
    - Sort the array descending by transaction count, then ascending 
    alphabetically by item name for items with matching transaction counts

    "Sort Stability and Complex Sorts" Section in Sorting HOW TO on 
    python3 docs: 
        https://docs.python.org/3/howto/sorting.html#sort-stability-and-complex-sorts
    '''

    # dict.items()
    # - return each item in a dictionary as tuples in a list.
    l = d.items()
    #print("l.items():", l, "\n")

    # Sort the list of tuples w/ index=1, which is the val of original dict
    sorted_l = sorted(l, key=operator.itemgetter(0)) 
    #print("sorted_l:", sorted_l)

    sorted_l2 = sorted(sorted_l, key=operator.itemgetter(1), reverse=True)
    #print("sorted_l2:", sorted_l2)

    # - - - - - - - - - - - - # - - - - - - - - - - - - 
    '''
    Step #3:
    - Return an array in the following format:
        ['mouse 2', 'notebook 2', 'keyboard 1']    
    '''
    res = []
    for (transaction, count) in sorted_l2:
        tmp = transaction + " " + str(count)
        #print("tmp:", tmp)
        res.append(tmp)

    #print("res:", res)
    return res

if __name__ == '__main__':
    fptr = sys.stdout

    transactions_count = int(input().strip())

    transactions = []

    for _ in range(transactions_count):
        transactions_item = input()
        transactions.append(transactions_item)

    result = groupTransactions(transactions)

    fptr.write('\n'.join(result))
    fptr.write('\n')

    fptr.close()
