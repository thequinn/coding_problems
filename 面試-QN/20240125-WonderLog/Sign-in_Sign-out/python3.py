#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'processLogs' function below.
#
# The function is expected to return a STRING_ARRAY.
# The function accepts following parameters:
#  1. STRING_ARRAY logs
#  2. INTEGER maxSpan
#

def processLogs(logs, maxSpan):
    # Write your code here
    
    #print("maxSpan:", maxSpan)

    d = {}
    res = []
    
    for log in logs:

        [user_id, timestamp, action] = log.split()
        #print("\nuser_id:", user_id, \
        #      ", timestamp:", timestamp, \
        #      ", action:", action)
        
        if user_id not in d: 
            d[user_id] = int(timestamp)
        else:
            duration = abs(int(timestamp) - d[user_id])
            if duration <= maxSpan:
                res.append(user_id)

    res.sort(key=int)
    #print("res:", res)
    return res


if __name__ == '__main__':
    fptr = sys.stdout

    logs_count = int(input().strip())

    logs = []

    for _ in range(logs_count):
        logs_item = input()
        logs.append(logs_item)

    maxSpan = int(input().strip())

    result = processLogs(logs, maxSpan)

    fptr.write('\n'.join(result))
    fptr.write('\n')

    fptr.close()
