
def decode(msg_file):
    dct = {}

    with open(msg_file) as file:
        lines = file.readlines()

    # Process each line in the file
    for line in lines:
        num, word = line.split(' ', 1)
        num = int(num)
        dct[num] = word

    count = 0
    res = []
    for i in range(len(lines)):
        tmp = []
        for j in range(i, count+1): 
            print("dct[j])", dct[j])
            tmp.append(dct[j])
        print("tmp:", tmp)
        count += 1
        i += count
        res.append(tmp)
    print("res:", res)


msg_file = "./encoded_msg_1.txt"
decoded_msg = decode(msg_file)
print(decoded_msg)
