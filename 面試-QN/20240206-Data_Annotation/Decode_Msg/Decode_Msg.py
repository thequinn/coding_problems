'''
Develop a function named decode(message_file). This function should read an encoded message from a .txt file and return its decoded version as a string.

Your function must be able to process an input file with the following format:

3 love
6 computers
2 dogs
4 cats
1 I
5 you

In this file, each line contains a number followed by a word. The task is to decode a hidden message based on the arrangement of these numbers into a "pyramid" structure. The numbers are placed into the pyramid in ascending order, with each line of the pyramid having one more number than the line above it. The smallest number is 1, and the numbers increase consecutively, like so:

  1
 2 3
4 5 6

The key to decoding the message is to use the words corresponding to the numbers at the end of each pyramid line (in this example, 1, 3, and 6). You should ignore all the other words. So for the example input file above, the message words are:

1: I
3: love
6: computers

and your function should return the string "I love computers".

'''

def decode(message_file):
    # Read the contents of the file
    with open(message_file, 'r') as file:
        lines = file.readlines()

    # Create a dictionary to store the words corresponding to each number
    word_dict = {}
    
    # Process each line in the file
    for line in lines:
        # Split the line into number and word
        num, word = line.strip().split(' ', 1)
        num = int(num)
        
        # Store the word in the dictionary with the corresponding number
        word_dict[num] = word

    # Construct decoded msg by picking words based on the pyramid structure
    decoded_message = []
    line_length = 1
    current_num = 1
    
    while current_num in word_dict:
        # Append the word corresponding to the current number
        decoded_message.append(word_dict[current_num])
        
        # Move to the next number in the pyramid
        current_num += line_length + 1
        line_length += 1

    # Join the decoded words into a single string
    decoded_string = ' '.join(decoded_message)
    
    return decoded_string



# Replace with the path to your message file
message_file = "./encoded_msg_1.txt"  
#message_file = "./encoded_msg_2.txt"  
decoded_message = decode(message_file)
print(decoded_message)

