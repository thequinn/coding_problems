import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';

const rowStyle = {
  display: 'flex'
}

const squareStyle = {
  'width':'60px',
  'height':'60px',
  'backgroundColor': '#ddd',
  'margin': '4px',
  'display': 'flex',
  'justifyContent': 'center',
  'alignItems': 'center',
  'fontSize': '20px',
  'color': 'blue'
}

const boardStyle = {
  'backgroundColor': '#eee',
  'width': '208px',
  'alignItems': 'center',
  'justifyContent': 'center',
  'display': 'flex',
  'flexDirection': 'column',
  'border': '3px #eee solid'
}

const containerStyle = {
  'display': 'flex',
  'alignItems': 'center',
  'flexDirection': 'column'
}

const instructionsStyle = {
  'marginTop': '5px',
  'marginBottom': '5px',
  'fontWeight': 'bold',
  'fontSize': '16px',
}

const buttonStyle = {
  'marginTop': '15px',
  'marginBottom': '16px',
  'width': '80px',
  'height': '40px',
  'backgroundColor': '#8acaca',
  'color': 'white',
  'fontSize': '16px',
}

const Square = ({value, onSquareClick}) => {
  return (
    <div className="square" style={squareStyle} onClick={onSquareClick}>
      {value}
    </div>
  );
}

const Board = ({xIsNext, squares, onPlay, onReset}) => {
  const [nextPlayer, setNextPlayer] = useState();

  const handleClick = (i) => {
    let nextPlayer = !xIsNext ? "X" : "O";
    setNextPlayer(nextPlayer);

    if (calcWinner(squares) || squares[i]) {  
      return
    }
    
    const nextSquares = squares.slice()
    if (xIsNext)  nextSquares[i] = 'X'
    else nextSquares[i] = 'O'

    onPlay(nextSquares);
  }

  const winner = calcWinner(squares);
  let res = "";
  if (winner)   res = winner;

  return (
    <div style={containerStyle} className="gameBoard">
      <div id="statusArea" className="status" style={instructionsStyle}>Next player: <span>{nextPlayer}</span></div>
      <div id="winnerArea" className="winner" style={instructionsStyle}>Winner: <span>{res}</span></div>
      <button style={buttonStyle} onClick={onReset}>Reset</button>
      <div style={boardStyle}>
        <div className="board-row" style={rowStyle}>
          <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
          <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
          <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
        </div>
        <div className="board-row" style={rowStyle}>
          <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
          <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
          <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
        </div>
        <div className="board-row" style={rowStyle}>
          <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
          <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
          <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
        </div>
      </div>
    </div>
  );
}

function Game() {
  const [history, setHistory] = useState( [Array(9).fill(null)] );
  const [curMove, setCurMove] = useState(0);
  const xIsNext = curMove % 2 === 0;
  const curSquares = history[curMove]

  const handlePlay = (nextSquares) => {
    const nextHistory = [...history.slice(0, curMove+1), nextSquares]  
    setHistory(nextHistory);
    setCurMove(nextHistory.length - 1)
  }

  const handleReset = () => {
    const emptyHistory = [Array(9).fill(null)];
    setHistory(emptyHistory);
    setCurMove(0)
  }

  return (
    <div className="game">
      <div className="game-board">
        <Board xIsNext={xIsNext} squares={curSquares} onPlay={handlePlay} onReset={handleReset} />
      </div>
    </div>
  );
}

const calcWinner = (squares) => {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      console.log("inside winner(), return squares[a]:", squares[a])
        return squares[a];
      }
    }
    return null;
  }

const container = document.getElementById('root');
const root = createRoot(container);
root.render(<Game />);
