/*
Convert Infix to Postfix
- Sol & Explanation:
    https://www.geeksforgeeks.org/convert-infix-expression-to-postfix-expression/

思路:
// Traverse the input str
//  if scanned c is '(':
//      Append to stack
//  if scanned c is operand (oprd):
//      Append to output result
//  if scanned c is ')':
//      >Note: In Python, needs to check if a list, stack[] here, is empty before it makes any calls
//      Pop the chunk of chars before ')', and append them to output result.
//      Pop the last scanned c, which is a ')'
//  if scanned c is operator (oprt):
//      >Note: In Python, needs to check if a list, stack[] here, is empty before it makes any calls
//      Pop the chunk of chars before an optr char that has a higher precedence than the scanned c.
//      Append the last scanned c, which is an optr

*/

//Function to return precedence of operators
function prec(c) {
    if(c == '^')
        return 3;
    else if(c == '/' || c=='*')
        return 2;
    else if(c == '+' || c == '-')
        return 1;
    else
        return -1;
}

function infixToPostfix(s) {
    let st = []; // For stack operations, we are using JavaScript built in stack
    let res = "";

    for(let i = 0; i < s.length; i++) {
        let c = s[i];

        // If the scanned char is an operand (oprd), append it to output result.
        if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
            result += c;

        // If the scanned char is an open bracket, push it to the stack.
        else if(c == '(')
            st.push('(');

        // If the scanned char is an close bracket,
            // - Concat the end elem in the stack to output result and pop the stack.  Repeat the 2 ops
            //   until we encounters a matching open bracket.
            // - Now we pop the '(' after we encounter it.
        else if(c == ')') {
            while(st[st.length - 1] != '(') {
                result += st[st.length - 1];
                st.pop();
            }
            st.pop();
        }

        // If the scanned char is an operator (oprt),
            // - Concat the end elem in the stack to output result and pop the stack.  Repeat the 2 ops
            //   if (1) the stack is not empty, and (2) the precedence of the scanned char <=
            //   the precedence of the elem in the end of the stack.
            // - Now, push the scanned char to stack
        else {
            while(st.length != 0 && prec(s[i]) <= prec(st[st.length - 1])) {
                result += st[st.length - 1];
                st.pop();
            }
            st.push(c);
        }
    }


    // Pop all the remaining elements from the stack
    while(st.length != 0) {
        result += st[st.length - 1];
        st.pop();
    }
    return result; // abcd^e-fgh*+^*+i-
}

let exp = "a+b*(c^d-e)^(f+g*h)-i";
let res = infixToPostfix(exp);
console.log("res:", res); // abcd^e-fgh*+^*+i-

