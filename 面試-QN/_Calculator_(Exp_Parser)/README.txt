
s2. Valid Math Exp
- See comments in js version




// - - - - - - - - - - - - - // - - - - - - - - - - - - - //
s1. LeetCode 20. Valid Parentheses



// - - - - - - - - - - - - - //
s2. Valid Math Exp
- 法1: Based on the method in Valid Parentheses
        -> More complicated to implement...skip
- 法2: Using a tree to
        (1) Convert Infix to Prefix  -> Evaluation of Prefix Exp
        (2) Convert Infix to Postfix -> Evaluation of Postfix Exp
  - Tutorial: https://www.geeksforgeeks.org/evaluation-prefix-expressions/



Problem #1:
- https://www.glassdoor.com/Interview/Given-a-string-of-an-arithmetic-expression-composed-of-numbers-and-calculate-the-result-e-g-2-4-5-7-QTN_855511.htm

- Q: Return True if a math expression is valid. The string contains the following set of parentheses ‘(‘, ’)’, ’[’, ’]’, ’{’, ’}’, numbers from 0 to 9 and following operators ‘+’, ’-’ and ‘*’.
ex. "{(1+2)*3}+4" -> T
ex. "((1+2)*3*)"  -> F



// - - - - - - - - - - - - - //
s3.  Calc result of Arithmetic Exp

Method #1:
LeetCode 224. Basic Calculator

Method #2:
https://www.geeksforgeeks.org/expression-evaluation/?ref=rp
- Note:
    1st learn "Arithmetic Expression Evaluation"
        https://www.geeksforgeeks.org/arithmetic-expression-evalution/?ref=rp


Given a string of an arithmetic expression, composed of numbers, '+' and '*', calculate the result.  e.g. "2+4*5*7"
- From Meta (Facebook)
    https://www.interviewkickstart.com/problems/valid-parentheses



